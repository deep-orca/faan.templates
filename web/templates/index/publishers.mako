# -*- coding: utf-8 -*-
<%inherit file="layout/main.mako" />
<%def name="title()">
Разработчикам
</%def>

<%def name="description()">Заработок в приложениях iOS и Android. Качественная монетизация мобильных ресурсов</%def>

<%def name="page_js()">
    <script src="//vjs.zencdn.net/4.9/video.js"></script>
    <script type="text/javascript" src="/global/static/plugins/fancybox/source/jquery.fancybox.pack.js"></script>
    <script type="text/javascript">
        $(".servicepad").hover(function(){$(this).toggleClass("welled").find(".iconBig").toggleClass("hovered")});

        $(document).ready(function(){
            $(".fbox").fancybox();
            var getVideoContent = function (filename, w, h) {
                var content = '' +
                    '<video id="video-box" class="video-js vjs-default-skin"' +
                    'controls preload="auto" width="{w}" height="{h}"' +
                    'poster="http://video-js.zencoder.com/oceans-clip.png">' +
                    '<source src="{filename}" type="video/mp4" />' +
                    '<p class="vjs-no-js">To view this video please enable JavaScript, and consider upgrading to a web browser that <a href="http://videojs.com/html5-video-support/" target="_blank">supports HTML5 video</a></p>' +
                    '</video>';

                content = content.replace('{filename}', filename).replace('{w}', w || 720).replace('{h}', h || 457);

                return content
            };
            $('.fbox_video').on('click', function (event) {
                event.preventDefault();
                var filename = $(this).attr('href');
                $.fancybox.open([filename], {
                    content: getVideoContent(filename),
                    afterShow: function () {
                        $('#video-box source').attr('src', filename);
                        videojs(document.getElementById('video-box'), {}, function () {
                            this.load().play();
                        });
                    },
                    beforeClose: function () {
                        videojs('video-box', {}, function () {
                            this.pause();
                        });
                    }
                });
            });
        })
    </script>
</%def>

<%def name="page_css()">
    <link rel="stylesheet" href="/global/static/plugins/fancybox/source/jquery.fancybox.css" type="text/css" media="screen" />
    <link href="//vjs.zencdn.net/4.9/video-js.css" rel="stylesheet">
    <style>
        .fancybox-inner {
            overflow: hidden !important;
        }
    </style>
</%def>


        <section id="page">
            <div class="col-md-12 text-center pt30 pb30">
                <h1>Инструменты для монетизации</h1>
                <p>Чтобы начать монетизировать Ваши приложения с помощью Vidiger, Вам необходимо пройти 3 простых шага</p>
            </div>

            <section id="content">

                <!-- news -->
                <section class="pt30">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-4">
                                <img src="/static/home_assets/images/publishers/1.jpg" class="img-responsive imgBorder mb15" alt="Добавить реклму в мобильное приложение">

                                <h3>1. Добавить приложение в систему</h3>
                                <p>
                                    Вы добавляете ваше приложение в систему, указывая при этом рекламу каких категорий вы хотите показывать
                                </p>

                            </div>

                            <div class="col-md-4">
                                <img src="/static/home_assets/images/publishers/2.jpg" class="img-responsive imgBorder mb15" alt="Добавить рекламную зону в мобильное приложение">

                                <h3>2. Добавить рекламную зону</h3>
                                <p>
                                    Далее необходимо создать рекламную зону и настроить в ней типы и параметры рекламых материалов, которые будут отображаться в конкретной зоне приложения
                                </p>

                            </div>

                            <div class="col-md-4">
                                <img src="/static/home_assets/images/publishers/3.jpg" class="img-responsive imgBorder mb15" alt="Добавить рекламный SDK в мобильное приложение">

                                <h3>3. Добавить наш SDK в Ваше приложение</h3>
                                <p>
                                    Вы добавляете 3 строки кода в ваше приложение и начинаете зарабатывать. За каждый просмотр рекламы вы будете получать выплату
                                </p>

                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12 mb40 mt30">
                                <hr class="lineLines">
                            </div>
                        </div>



                    </div>

                </section>
                <!-- news -->

                <!-- services -->
                <section class="pb30">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-4">
                                <article class="iconBlocHeading">
                                    <i class="icon-thumbs-up iconRounded"></i>
                                    <h2>Качественная реклама</h2>

                                    <p>Вашим пользователям будет отображаться только качественная реклама, категории и показ которой вы можете настроить сами.</p>

                                </article>
                            </div>
                            <div class="col-md-4">
                                <article class="iconBlocHeading">
                                    <i class="icon-cog iconRounded"></i>
                                    <h2>Легкая настройка</h2>
                                    <p>Настройка и добавление рекламы в проект предельно простое, при этом позволяет вам ограничить показ рекламы по времени, время задержки перед пропуском рекламы, лимиты показов и многое другое.</p>
                                </article>
                            </div>
                            <div class="col-md-4">
                                <article class="iconBlocHeading">
                                    <i class="icon-chart-bar iconRounded"></i>
                                    <h2>Полная статистика</h2>
                                    <p>Мы предоставляем максимально полную и прозрачную статистику по вашим приложениям и заработкам, а также помогаем интегрировать статистику с внешними инструментами анализа	.</p>
                                </article>
                            </div>
                        </div>
                        <div class="row mt40">
                            <div class="col-md-4">
                                <article class="iconBlocHeading">
                                    <i class="icon-dollar iconRounded"></i>
                                    <h2>Удобный вывод средств</h2>

                                    <p>Вывести заработанные финансы можно во все популярные платежные системы.</p>

                                </article>
                            </div>
                            <div class="col-md-4">
                                <article class="iconBlocHeading">
                                    <i class="icon-rocket iconRounded"></i>
                                    <h2>Никаких тормозов</h2>
                                    <p>Рекламные материалы больших размеров динамически подгружаются еще до вызова рекламного блока, что обеспечивает отображение рекламы пользователю сразу, без ожидания и "тормозов".</p>
                                </article>
                            </div>
                            <div class="col-md-4">
                                <article class="iconBlocHeading">
                                    <i class="icon-star iconRounded"></i>
                                    <h2>Молниеносная поддержка</h2>
                                    <p>С вами будет работать персональный менеджер, а также техподдержка, готовая помочь 24/7</p>
                                </article>
                            </div>

                        </div>

                        <div class="row">
                            <div class="col-md-12 mb40 mt30">
                                <hr class="lineLines">
                            </div>
                        </div>

                    </div>
                </section>
                <!-- services -->

                <!-- tabs & list -->
                <section class="pb40">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-12 text-center pt30 pb30">
                                <h1>Форматы рекламы</h2>
                                    <p>Выберите наиболее подходящий для вашего приложения формат</p>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <ul class="nav nav-tabs mt30" id="myTab">
                        <li class="active"><a href="#interstitial" data-toggle="tab">Видео</a></li>
                        <li><a href="#banners" data-toggle="tab">Баннеры</a></li>
                        <li><a href="#v4vc" data-toggle="tab">За получение контента</a></li>
                        <li><a href="#native" data-toggle="tab">Встроенная реклама</a></li>
                    </ul>

                    <div class="tab-content">
                        <!-- tab 1 -->
                        <div class="tab-pane fade active in" id="interstitial">
                            <div class="row">
                                <div class="col-md-12">
                                    <h3>Полноэкранное видео</h3>

                                    <p>
                                        Рекламное видео отображается в том месте приложения, куда его установит разработчик приложения. Например, при переходе в новом уровне в игре, при запуске утилиты, при открытии определенных разделов приложения. В видео возможно добавление оверлея и лендинга.
                                    </p>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="thumbnail">
                                        <a href="/static/home_assets/images/adformats/interstitial.jpg" class="fbox"><img src="/static/home_assets/images/adformats/interstitial_small.jpg" alt="Полноэкранная мобильная реклама"></a>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <pre><h3 style="margin:0"><a href="/static/home_assets/video/interstitial_360.mp4" class="fbox_video text-primary"><i class="icon-play"></i>Смотреть пример</a></h3></pre>
                                </div>
                            </div>
                        </div>

                        <div class="tab-pane fade" id="banners">
                            <div class="row">
                                <div class="col-md-12">
                                    <h3>Стандартные и Rich-media баннеры</h3>

                                    <p>
                                        Пользователю показывается баннер внутри приложения, который может быть статичным, анимированным или Rich-media баннером, поддерживающим дополнительные возможности анимации и использования расширенного функционала смартфона.
                                    </p>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="thumbnail">
                                        <a href="/static/home_assets/images/adformats/banner_video.png" class="fbox"><img src="/static/home_assets/images/adformats/banner_video_small.png" alt="Баннерная реклама"></a>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <pre><h3 style="margin:0"><a href="/static/home_assets/video/banner_video.mp4" class="fbox_video text-primary"><i class="icon-play"></i>Смотреть пример</a></h3></pre>
                                </div>
                            </div>
                        </div>


                        <!-- tab 2 -->
                        <div class="tab-pane fade" id="v4vc">
                            <div class="row">
                                <div class="col-md-12">
                                    <h3>Реклама за получение контента</h3>

                                    <p>
                                        Данный тип рекламы гарантирует рекламодателю полный и внимательный просмотр рекламного ролика, так как пользователь сам запрашивает рекламу у разработчика, потому что за ее просмотр он получает определенный условно-бесплатный контент в приложении (например, игровую валюту). При таком типе отображения реклама теряет такую порой свойственную ей вещь, как назойливость, оставляя при этом всех в выигрыше - пользователь получает игровую валюту, рекламодатель - потенциального клиента, а разработчик - свою выплату.
                                    </p>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="thumbnail">
                                        <a href="/static/home_assets/images/adformats/v4vc.jpg" class="fbox"><img src="/static/home_assets/images/adformats/v4vc_small.jpg" alt="Реклама за виртуальную валюту"></a>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <pre><h3 style="margin:0"><a href="/static/home_assets/video/v4vc_360.mp4" class="fbox_video text-primary"><i class="icon-play"></i>Смотреть пример</a></h3></pre>
                                </div>
                            </div>
                        </div>

                        <div class="tab-pane fade" id="native">
                            <div class="row">
                                <div class="col-md-5">
                                    <div class="thumbnail">
                                        <a href="/static/home_assets/presentation/native-ads.png" class="fbox">
                                            <img src="/static/home_assets/presentation/native-ads.png" alt="Нативная реклама">
                                        </a>
                                    </div>
                                </div>
                                <div class="col-md-7">
                                    <h3>Встроенная реклама</h3>

                                    <p>
                                        Рекламный баннер или видео стилизуются под общий стиль приложения, в котором они отображаются. Это гарантирует высокий CTR и привлечение внимания пользователей, привыкших к стандартной рекламе, а следовательно и высокий доход разработчика.
                                    </p>


                                </div>

                            </div>
                        </div>



<!--
                        <div class="tab-pane fade" id="offerwall">
                            <div class="row">
                                <div class="col-md-5 thumbnail">
                                    <img src="/static/home_assets/images/adformats/offerwall_small.png">
                                </div>
                                <div class="col-md-7">
                                    <h3>Выполнение действия</h3>

                                    <p>
                                        При таком типе рекламы пользователю в списке офферов может отображаться как рекламное видео, которое пользователь должен будет посмотреть для получения контента, так и ссылку, по которой пользователь должен будет выполнить какое-то дейстие для получения контента (например, зарегистрироваться на сайте, сделать репост, установить приложение).
                                    </p>
                                    <pre><h3 style="margin:0"><a href="/static/home_assets/video/offerwall_360.mp4" class="fbox_video text-primary"><i class="icon-play"></i>Смотреть видео-пример</a></h3></pre>
                                </div>
                            </div>

                            </div>

-->


                        </div>

                            </div>
                            <div class="col-md-6">
                                <h2>Преимущества данных форматов</h2>
                                <p>В итоге, вы получаете качественную монетизацию без потери лояльности пользователей в случае показа рекламы за предоставление условно-платного контента. Пользователь сам захочет посмотреть нашу рекламу, потому что за это он что-то в итоге получит (внутреннюю валюту, открытие разделов, ограниченный контент).
                                </p>
                                <div class="row">
                                    <div class="col-md-6">
                                        <ul class="list-unstyled iconList">
                                            <li>Динамическая предзагрузка</li>
                                            <li>Сохранение лояльности пользователей</li>
                                            <li>Высокий доход от рекламы</li>

                                        </ul>
                                    </div>
                                    <div class="col-md-6">
                                        <ul class="list-unstyled iconList">

                                            <li>Качественный рекламный контент</li>
                                            <li>Полное регулирование показов рекламы</li>
                                            <li>Поддержка всех популярных платформ</li>
                                        </ul>
                                    </div>


                                </div>
                            </div>
                        </div>
                    </div>

                </section>

                <!-- tabs & list -->

                <section class="color2 pb40 pt40">
                    <div class="ctaBox ctaBoxFullwidth">
                        <div class="container">
                            <div class="row">
                                <div class="col-md-8">
                                    <h1 style="font-weight: 300; font-size: 34px;">
                                        Чтобы начать размещать вашу рекламу в сети Vidiger, зарегистрируйтесь на сайте
                                    </h1>
                                </div>
                                <div class="col-md-4">
                                    <a class="btn btn-lg btn-border" title="" href="/sign/register?publisher">
                                        <i class="icon-down-open-big"></i> Зарегистрироваться
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>




            </section>
        </section>