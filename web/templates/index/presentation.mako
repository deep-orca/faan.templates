<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=1024" />
    <meta name="apple-mobile-web-app-capable" content="yes" />
    <title>Vidiger - презентация проекта</title>
    
    <meta name="description" content="Презентация проекта Vidiger" />
    <meta name="author" content="Vidiger" />

<!--
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:400italic,700italic,400,700&subset=latin,cyrillic' rel='stylesheet' type='text/css'>

    <link href='http://fonts.googleapis.com/css?family=PT+Sans:400,700,400italic,700italic&subset=latin,cyrillic' rel='stylesheet' type='text/css'>
-->
    <link href='http://fonts.googleapis.com/css?family=Roboto+Condensed:300italic,400italic,700italic,400,300,700&subset=latin,cyrillic' rel='stylesheet' type='text/css'>
    <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet">

  
    <link href="/static/home_assets/presentation/css/impress-demo.css" rel="stylesheet" />

    <link rel="stylesheet" href="/global/static/plugins/fancybox/source/jquery.fancybox.css" type="text/css" media="screen" />
    
    <link rel="shortcut icon" href="/static/home_assets/images/favicon.png" />
    <link rel="apple-touch-icon" href="/static/home_assets/presentation/apple-touch-icon.png" />
    <script src="//code.jquery.com/jquery-1.11.0.min.js"></script>


<!-- For Image gallery -->
    <link rel="stylesheet" type="text/css" href="/static/home_assets/presentation/js/jquery-collagePlus/support/examples.css" media="all" />
    <link rel="stylesheet" type="text/css" href="/static/home_assets/presentation/js/jquery-collagePlus/css/transitions.css" media="all" />

    <script src="/static/home_assets/presentation/js/jquery-collagePlus/jquery.collagePlus.js"></script>
    <script src="/static/home_assets/presentation/js/jquery-collagePlus/extras/jquery.removeWhitespace.js"></script>
    <script src="/static/home_assets/presentation/js/jquery-collagePlus/extras/jquery.collageCaption.js"></script>

    <link href="/global/static/plugins/video-js/video-js.min.css" rel="stylesheet">
    <script src="/global/static/plugins/video-js/video.js"></script>
    <script>
      videojs.options.flash.swf = "/global/static/plugins/video-js/video-js.swf"
    </script>
  
    <script type="text/javascript">

    // All images need to be loaded for this plugin to work so
    // we end up waiting for the whole window to load in this example
    $(window).load(function () {
        $(document).ready(function(){
            collage();
            $('.Collage').collageCaption();
        });
    });


    // Here we apply the actual CollagePlus plugin
    function collage() {
        $('.Collage').removeWhitespace().collagePlus(
            {
                'fadeSpeed'     : 2000,
                'targetHeight'  : 200,
                'effect'        : 'effect-1',
                'direction'     : 'vertical'
            }
        );
    };

    // This is just for the case that the browser window is resized
    var resizeTimer = null;
    $(window).bind('resize', function() {
        // hide all the images until we resize them
        $('.Collage .Image_Wrapper').css("opacity", 0);
        // set a timer to re-apply the plugin
        if (resizeTimer) clearTimeout(resizeTimer);
        resizeTimer = setTimeout(collage, 2000);
    });

    </script>
</head>


<body class="impress-not-supported">
<p style="position:fixed;width:100%;height:100%;background-color:rgba(255, 255, 255, 1);z-index: 9999;text-align:center;vertical-align:middle;padding-top: 200px;" id="boverlay">
    <span style="color: grey;" id="oloading">
        <img src="/static/home_assets/presentation/gears-animation.gif"><br>
        <small>Загрузка...</small>
    </span>
</p>
<!--
    For example this fallback message is only visible when there is `impress-not-supported` class on body.
-->
<div class="fallback-message">
    <p>Ваш браузер не поддерживает функцию полноценного просмотра презентации. Вы просматриваете презентацию в упрощенном режиме.</p>
    <p>Чтобы полноценно просматривать презентацию, используйте браузер <b>Chrome</b>, <b>Safari</b> или <b>Firefox</b>.</p>
</div>


<div id="impress">

    <div id="title" class="step" data-x="0" data-y="0" data-scale="4">
        <span class="try">Презентация</span>
        <h1>Vidiger<sup>*</sup></h1>
        <span class="footnote"><sup>*</sup> сеть интерактивной мобильной рекламы</span>
    </div>

    <div id="about" class="step" data-x="850" data-y="3000" data-rotate="90" data-scale="3">
        <h2>О проекте</h2><br/>
        <p><strong>VIDIGER</strong> - проект, реализующий размещение видеорекламы, баннерной рекламы, а также интерактивной Rich media рекламы в любых мобильных приложениях, работающих на платформах iOS и Android (в ближайшей перспективе - Windows), а также на мобильных сайтах </p>
        <p><i class="fa fa-apple fapadding"></i><i class="fa fa-android fapadding"></i><i class="fa fa-globe"></i></p>
    </div>

    <div id="more" class="step" data-x="-1050" data-y="3000" data-rotate="90" data-scale="1">
        <p style="font-size: 40px;"><i class="fa fa-cogs fapadding"></i>Рекламодатель может самостоятельно настраивать рекламные кампании любой сложности и продолжительности в зависимости от конкретных задач, а также использовать стандартные пакетные размещения рекламы</p><br>
        <p style="font-size: 40px;"><i class="fa fa-play-circle fapadding"></i>Гибкие настройки платформы позволяют откручивать на мобильных площадках не только обычную баннерную и видеорекламу, но и расширенную технологию <strong>MRAID</strong>, которая позволяет создавать рекламу, напрямую взаимодействующую с пользователем </p>
    </div>

    

    <div id="ing" class="step" data-x="3500" data-y="-850" data-rotate="270" data-scale="3" style="font-size: 36px">
        <p><i class="fa fa-bar-chart-o fapadding"></i> Количество пользователей мобильного Интернета стремительно <b class="rotating">растет</b>, при этом такой формат, как видеореклама и интерактивная реклама, практически <b class="scaling">отсутствует</b> в мобильном Рунете</p>
    </div>

    <div id="stats" class="step" data-x="3500" data-y="-3250" data-rotate="270" data-scale="3" style="font-size: 30px">
        <h3>Прирост заметен на графике</h3>
        <img src="http://company.yandex.ru/i/researches/2014/internet_regions/mobile_internet_penetration_by_years.png" class="imgshadow">
        <small style="color:grey;font-size: 20px; padding-left: 80px;">Проникновение мобильного Интернета в крупных городах России</small>
    </div>

    <div id="aim" class="step" data-x="3800" data-y="2100" data-rotate="180" data-scale="6">
        <p>попадите <b style="font-size: 150px;">т<i class="fa fa-crosshairs" style="padding-top:20px"></i>чно</b> <span class="thoughts">в цель</span></p>
    </div>

   
    <div id="instruments" class="step" data-x="4425" data-y="2485" data-z="-6500" data-rotate="300" data-scale="1">
        <p>с помощью инструментов <br><b>расширенного таргетинга</b> по местоположению, социальной информации и интересам пользователя</p>
        <i class="fa fa-male fapadding"></i><i class="fa fa-female fapadding"></i><i class="fa fa-users fapadding"></i><i class="fa fa-heart-o fapadding"></i><i class="fa fa-comments fapadding"></i>
    </div>

    <div id="benefits" class="step" data-x="6700" data-y="-300" data-scale="6">
        <p>о наших <b class="benefits">конкурентных преимуществах</b></p>
    </div>

    <div id="vidiger" class="step" data-x="6300" data-y="2000" data-rotate="20" data-scale="4" style="font-size:40px;">
        <q>Vidiger</q> - это <strong>профессиональный сервис</strong>, реализующий возможность интеграции Rich media рекламы в мобильные приложения и сайты
    </div>

    <div id="targetting" class="step" data-x="6000" data-y="4000" data-scale="2">
        <q><i class="fa fa-male "></i><i class="fa fa-female fapadding"></i>Мы предоставляем</q>
        <p><strong>точный таргетинг ЦА</strong> (география, пол, возраст, интересы, семейное положение и т.д.)</p>
    </div>


    <div id="model" class="step" data-x="6200" data-y="4600" data-z="-100" data-rotate-x="-40" data-rotate-y="10" data-scale="2">
        <p>
            <span class="have"><i class="fa fa-usd fapadding"></i>Эффективная</span> <span class="you">модель</span> <span class="noticed">показов</span><br> 
        </p>
        <span class="footnote" style="color:black">в зависимости от выбранного формата рекламы </span>
    </div>

    <div id="loyality" class="step" data-x="6300" data-y="5000" data-z="-500" data-rotate-x="-70" data-rotate-y="10" data-scale="2">
        <p>
            <span class="have"><i class="fa fa-thumbs-o-up fapadding"></i>Высокая</span> <span class="you">лояльность</span> <span class="noticed">пользователя</span><br> 
        </p>
        <span class="footnote" style="color:black">к новым форматам качественной рекламы, которую можно пропустить или просмотреть за вознаграждение</span>
    </div>



    <div id="services" class="step" data-x="8850" data-y="-2000" data-rotate="90" data-scale="3">
        <h2>Возможности сервиса</h2><br/>
        <table class="tlist" style="font-size:30px">
            <tr><td class="cbullet"><i class="fa fa-usd "></i></td><td>Настройка рекламного бюджета</td></tr>
            <tr><td class="cbullet"><i class="fa fa-clock-o "></i></td><td>Настройка показа рекламы по датам начала и конца кампании, по времени суток и дням недели</td></tr>
            <tr><td class="cbullet"><i class="fa fa-flash "></i></td><td>Уникальная система динамической предзагрузки позволяет показывать рекламу без задержек</td></tr>
            <tr><td class="cbullet"><i class="fa fa-cogs "></i></td><td>Настройка длительности показа, времени до пропуска рекламы, частоты показа рекламы пользователю</td></tr>
        </table>
        
    </div>

    <div id="formats" class="step" data-x="6850" data-y="-4700" data-scale="6" data-rotate="0">
        <p>о наших <b class="imagination">рекламных форматах</b></p>
    </div>

    <div id="interstitial" class="step" data-x="13000" data-y="-3700" data-scale="6" data-rotate-x="-10" data-rotate-y="10">
        <h3>Промежуточный ролик</h3>
        <p class="tlist">Рекламное видео отображается при переходе на новый уровень в игре, при запуске утилиты, при открытии определенных разделов приложения и т.д. В видео можно добавить оверлей и лендинг</p>
        <img src="/static/home_assets/presentation/interstitial.png" width="900">
        <a href="/static/home_assets/video/interstitial_360.mp4" class="fbox_video" style="font-size:26px"><i class="fa fa-play-circle"></i> Смотреть пример</a>
    </div>

    <div id="vfvc" class="step" data-x="13900" data-y="0" data-z="-400" data-rotate-x="-30" data-rotate-y="10" data-scale="4">
        <h3>Реклама за вознаграждение</h3>
        <p class="tlist">Этот тип рекламы гарантирует рекламодателю полный и внимательный просмотр рекламного ролика, так как пользователь получает за ее просмотр условно-бесплатный контент в приложении (например, игровую валюту). Реклама при этом не кажется назойливой</p>
        <img src="/static/home_assets/presentation/v4vc.png" width="900">
        <a href="/static/home_assets/video/v4vc_360.mp4" class="fbox_video" style="font-size:26px"><i class="fa fa-play-circle"></i> Смотреть пример</a>
    </div>

<!--
    <div id="offerwall" class="step" data-x="14900" data-y="3000" data-z="-800" data-rotate-x="0" data-rotate-y="10" data-scale="4">
        <h3>Список предложений</h3>
        <table class="tlist">
            <tr><td><img src="http://vidiger.com/static/home_assets/images/adformats/offerwall_small.png" width="300"></td>
                <td style="vertical-align:top; padding-left: 30px">При этом типе рекламы пользователь выполняет определенное действие для получения вознаграждения. Это может быть просмотр рекламного видео, установка приложения, регистрация на сайте или любое другое действие из списка предложений </td>
            </tr>
        </table>
       

        <a href="/static/home_assets/video/offerwall_360.mp4" class="fbox_video" style="font-size:26px"><i class="fa fa-play-circle"></i> Смотреть пример</a>
    </div>
-->

    <div id="banner_video" class="step" data-x="14900" data-y="3000" data-z="-800" data-rotate-x="0" data-rotate-y="10" data-scale="4">
        <h3>Rich-media баннер + видео с бекграундом</h3>
        <p class="tlist">Пользователю демонстрируется баннер, при нажатии на который, проигрывается видеореклама.</p>
        <img src="/static/home_assets/presentation/banner_video.png" width="900">
        <a href="/static/home_assets/video/banner_video.mp4" class="fbox_video" style="font-size:26px"><i class="fa fa-play-circle"></i> Смотреть пример</a>
    </div>
<!--
    <div id="native" class="step" data-x="15900" data-y="6000" data-z="-1600" data-rotate-x="0" data-rotate-y="10" data-scale="4">
        <h3>Стилизованная реклама</h3>
        <p class="tlist">Рекламный баннер или видео стилизуются под общий стиль приложения, в котором они отображаются. Это гарантирует высокий CTR и привлечение внимания пользователей, привыкших к стандартной рекламе.</p>
        <div style="width:100%;text-align:center;"><img src="/static/home_assets/presentation/native-ads.png" height="500"></div>
    </div>
-->

    <div id="extended" class="step" data-x="12900" data-y="9000" data-z="-5400" data-rotate-x="0" data-rotate-y="10" data-scale="4">
        <h3>Расширенная реклама</h3>
        <p class="tlist">При использовании технологий Vidiger вы можете создать ЛЮБОЙ тип рекламы, напрямую взаимодействующий с пользователем. При этом вы можете использовать все возможности смартфона: камеру, акселерометр, гироскоп, компас, жестикуляцию и др. При использовании такого типа рекламы может быть создано <b>мини-приложение</b>, в котором максимально подробно описывается рекламируемая услуга прямо внутри приложения пользователя. Примеры использования технологии интерактивной Rich media рекламы:</p>


     <section class="Collage effect-parent">
        <div class="Image_Wrapper" data-caption="<b>Мини-игра</b><br>Пользователю отображается мини-игра и дополнительные рекламные опции">
            <a href="/static/home_assets/video/extended_ads/game.m4v" class="fbox_video">
                <img src="http://www.celtra.com/media/uploads/gallery/hotel_trans_215_opt1_p157.jpg">
            </a>
        </div>
        <div class="Image_Wrapper" data-caption="<b>Свайп и акселерометр</b><br>В рекламе используется жестикуляция и акселерометр телефона">
            <a href="/static/home_assets/video/extended_ads/ebay.m4v" class="fbox_video">
                <img src="http://www.celtra.com/media/uploads/gallery/ebay_sc_p138.png">
            </a>
        </div>
       <div class="Image_Wrapper" data-caption="<b>Панорамный обзор</b><br>Используется панорамный обзор продукта, видео и дополнительные опции">
            <a href="/static/home_assets/video/extended_ads/infinity.m4v" class="fbox_video">
                <img src="http://www.celtra.com/media/uploads/gallery/infiniti_215_p194.jpg">
            </a>
        </div>
        <div class="Image_Wrapper" data-caption="<b>Промо-коды и карта</b><br>В рекламе используется жестикуляция и акселерометр телефона">
            <a href="/static/home_assets/video/extended_ads/adidas.m4v" class="fbox_video">
                <img src="http://www.celtra.com/media/uploads/gallery/adidas_215_v1_p207.jpg">
            </a>
        </div>

        <div class="Image_Wrapper" data-caption="<b>Click-to-call и click-to-sms</b><br>Пользователь может позвонить или отправить смс для заказа с помощью рекламы">
            <a href="/static/home_assets/video/extended_ads/pizza.m4v" class="fbox_video">
                <img src="http://bonzai.ad/images/video_thumbs/DOMINOS.jpg">
            </a>
        </div>
        <div class="Image_Wrapper" data-caption="<b>Перетаскивание</b><br>Чтобы приготовить завтрак пользователь должен собрать все ингредиенты">
            <a href="/static/home_assets/video/extended_ads/meal.m4v" class="fbox_video">
                <img src="http://www.celtra.com/media/uploads/gallery/flora_p197.jpg">
            </a>
        </div>

        <div class="Image_Wrapper" data-caption="<b>Использование микрофона</b><br>Голос пользователя сканируется и определяется, является ли он пришельцем">
            <a href="/static/home_assets/video/extended_ads/mib.m4v" class="fbox_video">
                <img src="http://www.celtra.com/media/uploads/gallery/thumb_mib3_01_p100.png">
            </a>
        </div>

         <div class="Image_Wrapper" data-caption="<b>Игра свайпом</b><br>Пользователю нужно забросить батончик Сникерс как можно дальше">
            <a href="/static/home_assets/video/extended_ads/snickers.m4v" class="fbox_video">
                <img src="http://www.celtra.com/media/uploads/gallery/snickers_p180.jpg">
            </a>
        </div>

        <div class="Image_Wrapper" data-caption="<b>Рисование</b><br>Пользователю нужно нарисовать свой дизайн стаканчика Starbucks">
            <a href="/static/home_assets/video/extended_ads/starbucks.m4v" class="fbox_video">
                <img src="http://www.celtra.com/media/uploads/gallery/starbucks_p28.png">
            </a>
        </div>
        
    </section>
    </div>


    <div id="conclusion" class="step" data-x="1200" data-y="10000" data-z="-800" data-scale="4">
        <h3>Заключение</h3>
        <p class="tlist" style="font-size:28px">
            Платформа <strong>VIDIGER</strong> позволяет реализовывать самые разнообразные идеи и проводить рекламные кампании любой сложности. При помощи нашего сервиса можно интегрировать в мобильные приложения и сайты баннерную, видеорекламу, серии <strong>интерактивных</strong> видеороликов, мини-приложения. Последовательность событий и рекламных сообщений в них зависит от поведения пользователя или положения мобильного устройства. </p><br>
            <p class="tlist" style="font-size:28px">Интерактивная реклама воспринимается пользователем как элемент игры, становясь наиболее эффективным видом коммуникации. Гибкие настройки платформы под конкретные задачи позволяют использовать любой вид рекламы, в том числе в сочетании друг с другом.
        </p>
        <strong class="tlist"  style="font-size:28px">
            <i>Свяжитесь с нами для старта Вашей блестящей рекламной кампании</i>
        </strong>
        
    </div>

    <div id="contacts" class="step" data-x="1200" data-y="10000" data-z="25800" data-scale="1">
        <h3>Контакты</h3>
        
        <address style="font-size:36px">
            <p>
                <i class="fa fa-location-arrow w40"></i>Москва, 4-й Сыромятнический переулок 3/5 <br>
                <i class="fa fa-phone w40 w40"></i>+7 (495) 964-14-56  <br>
                <i class="fa fa-mobile-phone w40 w40"></i>+7 (916) 876-28-62  <br>
                <i class="fa fa-skype w40 w40"></i><a href="callto:vidiger.official">vidiger.official</a> <br>
                <i class="fa fa-envelope-o w40"></i><a href="mailto:contact@vidiger.com">contact@vidiger.com</a>
            </p>
        </address>
        
    </div>

  
    <div id="overview" class="step" data-x="3000" data-y="1500" data-scale="10">
    </div>

</div>


<div class="hint">
    <p>Используйте стрелки на клавиатуре или клавишу "пробел" для навигации.</p>
</div>
<script>
if ("ontouchstart" in document.documentElement) { 
    document.querySelector(".hint").innerHTML = "<p>Нажимайте на правую и левую часть экрана для навигации</p>";
}
</script>


    <script type="text/javascript" src="/global/static/plugins/fancybox/source/jquery.fancybox.pack.js"></script>
    <script type="text/javascript">
        $(".servicepad").hover(function(){$(this).toggleClass("welled").find(".iconBig").toggleClass("hovered")});

        $(document).ready(function(){
            
            $(".fbox").fancybox();

            $(".fbox_video").fancybox({
            'type' : 'image',
            padding: 0,
            content: '<video id="example_video" class="video-js vjs-default-skin"  controls preload="auto" width="800" height="495"> <source src="#" type="video/mp4" /> <p class="vjs-no-js">Ваш браузер не поддерживает проигрывание видео</p></video>',
            afterShow: function(){
                $("source").attr("src", $(this).attr("href"));
                videojs(document.getElementById('example_video'), {}, function() {
                    this.load().play();
                    
                });

            }
            });

            $(".fbox_iframe").fancybox();
        })
    </script>

<script src="/static/home_assets/presentation/js/impress.js"></script>
<script>impress().init();</script>

<script type="text/javascript">
$(window).load(function () {
    $("body").css("background", "url(/static/home_assets/presentation/bg2.jpg) no-repeat");
    $("body").css( "background-size", "100%");
    $("#oloading").fadeOut(1000, function() {
        $("#boverlay").fadeOut(1000);
  });
});
</script>
</body>
</html>