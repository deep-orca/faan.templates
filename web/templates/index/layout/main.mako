## -*- coding: utf-8 -*-
<%def name="title()"></%def>
<%def name="description()"></%def>
<%def name="page_js()"></%def>
<%def name="page_css()"></%def>
<%
menu = [{'name': u'Главная', 'href': '/'},
        {'name': u'Рекламодателям', 'href': '/advertisers/'},
        {'name': u'Разработчикам', 'href': '/publishers/'},
        {'name': u'Агентствам и брендам', 'href': '/brands/'},
        {'name': u'Контакты', 'href': '/contacts/'}
        ]
def isActiveItem(i):
    if i.get('href') == request.path:
        return "active"
    return " "
%>
<!DOCTYPE html>
<!--[if lt IE 7 ]><html class="ie ie6" lang="en"> <![endif]-->
<!--[if IE 7 ]><html class="ie ie7" lang="en"> <![endif]-->
<!--[if IE 8 ]><html class="ie ie8" lang="en"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!-->
<html lang="en">
<!--<![endif]-->

<meta http-equiv="content-type" content="text/html;charset=utf-8" />
<head>

<!-- Basic Page Needs
    ================================================== -->
<meta charset="utf-8">
<title>Vidiger -  Это Мобильная Реклама | ${self.title()} | ${self.description()}</title>
<meta name="description" content="Vidiger - первая русская сеть мобильной видео-рекламы. Свежие форматы, подробная аналитика, индивидуальные решения">
    <meta name="author" content="Vidiger Team">
<!-- Mobile Specific Metas
    ================================================== -->
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<!-- CSS
    ================================================== -->
    <!-- Bootstrap  -->
    <link type="text/css" rel="stylesheet" href="/static/home_assets/bootstrap/css/bootstrap.min.css">
    <!-- web font  -->
    <link href='http://fonts.googleapis.com/css?family=Roboto:400,100,300,500&subset=latin,cyrillic' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Roboto+Condensed:400,300,700&subset=latin,cyrillic' rel='stylesheet' type='text/css'>
    <!-- plugin css  -->
    <link rel="stylesheet" type="text/css" href="/static/home_assets/js-plugin/animation-framework/animate.css" />
    <!-- Pop up-->
    <link rel="stylesheet" type="text/css" href="/static/home_assets/js-plugin/magnific-popup/magnific-popup.css" />
    <!-- Revolution Slider -->
    <link rel="stylesheet" type="text/css" href="/static/home_assets/js-plugin/revolution-slider/rs-plugin/css/navstylechange.css" media="screen" />
    <link rel="stylesheet" type="text/css" href="/static/home_assets/js-plugin/revolution-slider/rs-plugin/css/settings-hideo.css" media="screen" />

    <!-- nekoAnim-->
    <link rel="stylesheet" type="text/css" href="/static/home_assets/js-plugin/appear/nekoAnim.css" />
    
    <!-- Owl carousel-->
    <link rel="stylesheet" href="/static/home_assets/js-plugin/owl.carousel/owl-carousel/owl.carousel.css">
    <link rel="stylesheet" href="/static/home_assets/js-plugin/owl.carousel/owl-carousel/owl.transitions.css">
    <link rel="stylesheet" href="/static/home_assets/js-plugin/owl.carousel/owl-carousel/owl.theme.css">
    <!-- icon fonts -->
    <link type="text/css" rel="stylesheet" href="/static/home_assets/font-icons/custom-icons/css/custom-icons.css">
    <link type="text/css" rel="stylesheet" href="/static/home_assets/font-icons/custom-icons/css/custom-icons-ie7.css">
    <!-- Custom css -->
    <link type="text/css" rel="stylesheet" href="/static/home_assets/css/layout.css">
    <link type="text/css" id="colors" rel="stylesheet" href="/static/home_assets/css/colors.css">
    <link type="text/css" rel="stylesheet" href="/static/home_assets/css/custom.css">

    <link href="//netdna.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet">

    <!--[if lt IE 9]><script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script> <![endif]-->
    <script src="/static/home_assets/js/modernizr-2.6.1.min.js"></script>
    <!-- Favicons
    ================================================== -->
    <link rel="shortcut icon" href="/static/home_assets/images/favicon.png">
    <link rel="apple-touch-icon" href="/static/home_assets/images/apple-touch-icon.png">
    <link rel="apple-touch-icon" sizes="72x72" href="/static/home_assets/images/apple-touch-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="114x114" href="/static/home_assets/images/apple-touch-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="144x144" href="/static/home_assets/images/apple-touch-icon-144x144.png">

${self.page_css()}

    <script>
      (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
      (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
      m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
      })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
    
      ga('create', 'UA-51823390-1', 'auto');
      ga('send', 'pageview');
    
    </script>
</head>
<body class="activateAppearAnimation">
<!-- Primary Page Layout 
    ================================================== -->
    <!-- globalWrapper -->
    <div id="globalWrapper" class="localscroll">
        <header class="navbar-fixed-top">
            <!-- header -->
            <div id="mainHeader" role="banner">
                <div class="container">
                    <nav class="navbar navbar-default scrollMenu" role="navigation">
                        <div class="navbar-header">
                            <!-- responsive navigation -->
                            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                                <span class="sr-only">Переключить навигацию</span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </button>
                            <!-- Logo -->
                            <a class="navbar-brand" href="/"><img src="/static/home_assets/images/main-logo.png" alt="Сеть мобильной рекламы Vidiger"/></a>
                        </div>
                        <div class="collapse navbar-collapse" id="mainMenu">
                            <!-- Main navigation -->
                            <ul class="nav navbar-nav pull-right">
                                % for m in menu:
                                <li class="primary">
                                    <a href="${m.get('href')}" class="firstLevel ${isActiveItem(m)} ${u'hasSubMenu' if m.get('href') == '/advertisers/' else ''}" >${m.get('name')}</a>
                                    %if m.get('href') == '/advertisers/':
                                    <ul class="subMenu">
                                        <li><a href="/advertisers/">Основное</a></li>
                                        <li><a href="/advertisers/#adformats">Форматы рекламы</a></li>
                                    </ul>
                                    %endif
                                    ${u'<li class="sep"></li>' if m.get("href") != "/contacts/" else ""}
                                </li>
                                % endfor

                                <li class="sep-middle"></li>
                                <li class="sep-middle"></li>
                                <li class="sep-middle"></li>
                                <li id="login" class="primary"><a href="/sign/in" class="firstLevel last dark">Вход </a></li>
                                <li class="sep-last"></li>
                                <li id="register" class="last"><a href="/sign/register" class="firstLevel last dark"><span class="badge badge-primary">Регистрация</span></a></li>
                            </ul>
                            <!-- End main navigation -->
                        </div>
                    </nav>
                </div>
            </div>
        </header>
        <!-- header -->
        <!-- ======================================= content ======================================= -->

                ${ next.body() }
            <!-- footer -->
<footer id="footerWrapper">
                <section id="mainFooter">
                    <div class="container">
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="footerWidget">
                                    <img src="/static/home_assets/images/main-logo.png" id="footerLogo">
                                    <p><a href="/" title="">Vidiger</a> - сеть мобильной рекламы. </p>
                                </div>
                            </div>

                            <div class="col-sm-3">
                                <div class="footerWidget">

                                    <h3>VIDIGER</h3>
                                    <address>
                                        <p>
                                            <i class="icon-location"></i>&nbsp;Москва<br>
                                            4 Сыромятнический пер., 3/5 <br>
                                            <i class="icon-phone"></i>&nbsp;+7 (495) 964-14-56 <br>
                                            <i class="icon-skype"></i>&nbsp;<a href="callto:vidiger.official">vidiger.official</a> <br>
                                            <i class="icon-mail-alt"></i>&nbsp;<a href="mailto:contact@vidiger.com">contact@vidiger.com</a>
                                        </p>
                                    </address>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="footerWidget">
                                    <h3>Мы в социальных сетях</h3>
                                    <ul class="socialNetwork">
                                        <li><a href="http://vk.com/vidiger" class="tips" title="Присоединяйтесь к нам в VK" target="_blank"><i class="icon-vkontakte iconRounded"></i></a></li>
                                        <li><a href="https://www.facebook.com/vidiger.official" class="tips" title="Присоединяйтесь к нам на Facebook" target="_blank"><i class="icon-facebook-1 iconRounded"></i></a></li>
                                        <li><a href="https://twitter.com/vidiger" class="tips" title="Присоединяйтесь к нам в Twitter" target="_blank"><i class="icon-twitter-bird iconRounded"></i></a></li>
                                        <li><a href="https://plus.google.com/+VidigerOfficial" class="tips" title="Присоединяйтесь к нам на Google+" target="_blank"><i class="icon-gplus-1 iconRounded"></i></a></li>
                                        <li><a href="#" class="tips" title="Присоединяйтесь к нам в Linkedin" target="_blank"><i class="icon-linkedin-1 iconRounded"></i></a></li>
                                        <li><a href="#" class="tips" title="Присоединяйтесь к нам в Dribble" target="_blank"><i class="icon-dribbble iconRounded"></i></a></li>
                                    </ul>     
                                </div>
                            </div>

                        </div>
                    </div>
                </section>

                <section  id="footerRights">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-12">
                                <p>Copyright © 2014 <a href="http://vidiger.com">Vidiger</a> / Все права защищены.</p>
                            </div>

                        </div>
                    </div>
                </section>
            </footer>
<!-- End footer -->
</div>
<!-- global wrapper -->
<!-- End Document 
    ================================================== -->
    <script type="text/javascript" src="/static/home_assets/js-plugin/respond/respond.min.js"></script>
    <script type="text/javascript" src="/static/home_assets/js-plugin/jquery/jquery-1.10.2.min.js"></script>
    <script type="text/javascript" src="/static/home_assets/js-plugin/jquery-ui/jquery-ui-1.8.23.custom.min.js"></script>
    <!-- third party plugins  -->
    <script type="text/javascript" src="/static/home_assets/bootstrap/js/bootstrap.js"></script>
    <script type="text/javascript" src="/static/home_assets/js-plugin/easing/jquery.easing.1.3.js"></script>
    <!-- carousel -->
    <script type="text/javascript" src="/static/home_assets/js-plugin/owl.carousel/owl-carousel/owl.carousel.min.js"></script>
    <!-- pop up -->
    <script type="text/javascript" src="/static/home_assets/js-plugin/magnific-popup/jquery.magnific-popup.min.js"></script>
    <!-- Revolution slider -->
    <script type="text/javascript" src="/static/home_assets/js-plugin/revolution-slider/rs-plugin/js/jquery.themepunch.plugins.min.js"></script>
    <script type="text/javascript" src="/static/home_assets/js-plugin/revolution-slider/rs-plugin/js/jquery.themepunch.revolution.min.js"></script>
    <!-- isotope -->
    <script type="text/javascript" src="/static/home_assets/js-plugin/isotope/jquery.isotope.min.js"></script>
    <!-- form -->
    <script type="text/javascript" src="/static/home_assets/js-plugin/neko-contact-ajax-plugin/js/jquery.form.js"></script>
    <script type="text/javascript" src="/static/home_assets/js-plugin/neko-contact-ajax-plugin/js/jquery.validate.min.js"></script>
    <!-- parallax -->
    <script type="text/javascript" src="/static/home_assets/js-plugin/parallax/js/jquery.stellar.min.js"></script>
    <!-- appear -->
    <script type="text/javascript" src="/static/home_assets/js-plugin/appear/jquery.appear.js"></script>

    <!-- toucheeffect -->
    <script type="text/javascript" src="/static/home_assets/js-plugin/toucheeffect/toucheffects.js"></script>

    <!-- sharrre -->
    <script type="text/javascript" src="/static/home_assets/js-plugin/jquery.sharrre-1.3.4/jquery.sharrre-1.3.4.min.js"></script>

    <!-- Custom  -->
    <script type="text/javascript" src="/static/home_assets/js/custom.js"></script>
    ${self.page_js()}

<!-- Yandex.Metrika counter -->
<script type="text/javascript">
(function (d, w, c) {
    (w[c] = w[c] || []).push(function() {
        try {
            w.yaCounter25223717 = new Ya.Metrika({id:25223717,
                    webvisor:true,
                    clickmap:true,
                    trackLinks:true,
                    accurateTrackBounce:true});
        } catch(e) { }
    });

    var n = d.getElementsByTagName("script")[0],
        s = d.createElement("script"),
        f = function () { n.parentNode.insertBefore(s, n); };
    s.type = "text/javascript";
    s.async = true;
    s.src = (d.location.protocol == "https:" ? "https:" : "http:") + "//mc.yandex.ru/metrika/watch.js";

    if (w.opera == "[object Opera]") {
        d.addEventListener("DOMContentLoaded", f, false);
    } else { f(); }
})(document, window, "yandex_metrika_callbacks");
</script>
<noscript><div><img src="//mc.yandex.ru/watch/25223717" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
<!-- /Yandex.Metrika counter -->
</body>
</html>

