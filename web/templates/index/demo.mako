<!DOCTYPE html>
<!--[if lt IE 7 ]><html class="ie ie6" lang="en"> <![endif]-->
<!--[if IE 7 ]><html class="ie ie7" lang="en"> <![endif]-->
<!--[if IE 8 ]><html class="ie ie8" lang="en"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!-->
<html lang="en">
<!--<![endif]-->

<meta http-equiv="content-type" content="text/html;charset=utf-8" />
<head>

<!-- Basic Page Needs
    ================================================== -->
<meta charset="utf-8">
<title>Vidiger -  Демонстрация рекламных форматов</title>
<meta name="description" content="Vidiger - первая русская сеть мобильной видео-рекламы. Свежие форматы, подробная аналитика, индивидуальные решения">
    <meta name="author" content="Vidiger Team">
<!-- Mobile Specific Metas
    ================================================== -->
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<!-- CSS
    ================================================== -->
    <!-- Bootstrap  -->
    <link type="text/css" rel="stylesheet" href="/static/home_assets/bootstrap/css/bootstrap.min.css">
    <!-- web font  -->
    <link href='http://fonts.googleapis.com/css?family=Roboto:400,100,300,500&subset=latin,cyrillic' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Roboto+Condensed:400,300,700&subset=latin,cyrillic' rel='stylesheet' type='text/css'>

    <link href="//netdna.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet">
<style type="text/css">
body {
    font-family: 'Roboto', sans-serif;
    padding-top: 60px;

}
.navbar-fixed-top{padding:10px;background-color: white;}
.pt10{padding-top:10px}
</style>
</head>
<body>

    <header class="navbar-fixed-top text-center">
        <img src="/static/home_assets/images/main-logo.png" alt="Vidiger logo">
    </header>
    <section>
        <div class="container">
            <div class="col-xs-12">
                <p>Демонстрация рекламных форматов</p>
                <small>Нажмите на название формата, чтобы посмотреть пример страницы с ним</small>
            </div>
            <div class="col-xs-12 pt10">
                <a href="/static/demo-formats/banner/demo.html" class="thumbnail">Статический баннер</a>
            </div>
            <div class="col-xs-12">
                <a href="/static/demo-formats/rm-banner/demo.html" class="thumbnail">Rich-media баннер</a>
            </div>
            <div class="col-xs-12">
                <a href="/static/demo-formats/rm-banner-expand/demo.html" class="thumbnail">Rich-media баннер c расхлопом</a>
            </div>
            <div class="col-xs-12 ">
                <a href="/static/demo-formats/interstitial/demo.html" class="thumbnail">Полноэкранный статический баннер</a>
            </div>
            <div class="col-xs-12 ">
                <a href="/static/demo-formats/rm-interstitial/demo.html" class="thumbnail">Полноэкранный Rich-media баннер</a>
            </div>
            <div class="col-xs-12 ">
                <a href="/static/demo-formats/banner-responsive/demo.html" class="thumbnail">Резиновый баннер, реагирующий на положение смартфона</a>
            </div>
        </div>
    </section>
    <footer>
        <section>
        <div class="container text-center">
            <small>Copyright © 2014 Vidiger / Все права защищены</small>
        </div>
        </section>
    </footer>


<script type="text/javascript" src="/static/home_assets/js-plugin/jquery/jquery-1.10.2.min.js"></script>
<script type="text/javascript" src="/static/home_assets/bootstrap/js/bootstrap.js"></script>
</body>