## -*- coding: utf-8 -*-
<%inherit file="layout/main.mako" />
<%def name="title()">
Главная страница
</%def>

<%def name="description()">Монетизация мобильных приложений и сайтов, размещение рекламы, продвижение бренда</%def>

<%def name="page_css()">
<style>
.btn-border{
	border: 1px dashed rgba(0, 0, 0, 0.15);
}

.color1{
	padding-bottom: 50px;
}

.btn-slider{
	color: #111!important;
	border: 1px dashed rgba(0, 0, 0, 0.15);
	background: white;
	font-size: 20px!important;
	line-height: 22px!important;
}
</style>
</%def>

<!-- slider -->
		<section id="rsDemoWrapper">

			<div class="tp-banner-container">
				<div class="tp-banner" >
					<ul>	<!-- SLIDE  -->
						<li data-transition="fade" data-slotamount="7" data-masterspeed="1500" >
							<!-- MAIN IMAGE -->
							<img src="/static/home_assets/images/slider/rs-slider/slider-1/bg1.jpg"  alt="slidebg1"  data-bgfit="cover" data-bgposition="left top" data-bgrepeat="no-repeat">
							<!-- LAYERS -->
							<!-- LAYER NR. 1 -->
							<div class="tp-caption tp-resizeme"
							data-x="center" data-hoffset="0"
							data-y="150"
							data-speed="600"
							data-start="1400"
							data-endspeed="600"
							style=""
							>
							<img src="/static/home_assets/images/slider/rs-slider/slider-1/logo-big.png" alt="Vidiger Logo" />

						</div>

						<!-- LAYER NR. 2 -->
						<div class="tp-caption large_bold_white customin customout tp-resizeme "
						data-x="center" data-hoffset="0"
						data-y="250"
						data-customin="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:5;scaleY:5;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
						data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
						data-speed="600"
						data-start="2400"
						data-easing="Power4.easeOut"
						data-endspeed="600"
						data-endeasing="Power0.easeIn"
						style="z-index: 3;">Рекламная эволюция

					</div>

					<!-- LAYER NR. 3 -->
					<div class="tp-caption medium_light_white customin customout tp-resizeme"
					data-x="center" data-hoffset="0"
					data-y="bottom" data-voffset="-140"
					data-customin="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:5;scaleY:5;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
					data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
					data-speed="600"
					data-start="2700"
					data-easing="Power4.easeOut"
					data-endspeed="600"
					data-endeasing="Power0.easeIn"
					style="z-index: 4; ">Свежие форматы, точный таргетинг и высокий заработок
				</div>

			</li>

			

<li data-transition="fade" data-slotamount="7" data-masterspeed="1500" >
	<!-- MAIN IMAGE -->
	<img src="/static/home_assets/images/slider/rs-slider/slider-1/bg3.jpg"  alt="slidebg3"  data-bgfit="cover" data-bgposition="left top" data-bgrepeat="no-repeat">
	<!-- LAYERS -->

	<!-- LAYER NR. 2 -->
	<div class="tp-caption largeblackbg skewfromleft tp-resizeme"
	data-x="600" data-hoffset="0"
	data-y="100"
	data-speed="600"
	data-start="1400"
	data-easing="Power4.easeOut"
	data-endspeed="600"
	data-endeasing="Power0.easeIn"
	style="z-index: 3">Рекламодателям

</div>

<div class="tp-caption medium_light_white skewfromleft tp-resizeme"
			data-x="600" data-hoffset="1"
			data-y="180"
			data-speed="600"
			data-start="1800"
			data-easing="Power4.easeOut"
			data-endspeed="600"
			data-endeasing="Power0.easeIn"
			style="z-index: 3; color:rgb(73, 73, 73);"><i class="icon-right-open-mini"></i> Точный таргетинг аудитории

		</div>

		<!-- LAYER NR. 4 -->
		<div class="tp-caption medium_light_white skewfromleft tp-resizeme"
		data-x="600" data-hoffset="0"
		data-y="220"
		data-speed="600"
		data-start="2000"
		data-easing="Power4.easeOut"
		data-endspeed="600"
		data-endeasing="Power0.easeIn"
		style="z-index: 3; color:rgb(73, 73, 73);"><i class="icon-right-open-mini"></i> Новые форматы рекламы

	</div>
	<div class="tp-caption medium_light_white skewfromleft tp-resizeme"
		data-x="600" data-hoffset="0"
		data-y="260"
		data-speed="600"
		data-start="2200"
		data-easing="Power4.easeOut"
		data-endspeed="600"
		data-endeasing="Power0.easeIn"
		style="z-index: 3; color:rgb(73, 73, 73);"><i class="icon-right-open-mini"></i> Расширенная аналитика

	</div>

<div class="tp-caption medium_light_white skewfromleft tp-resizeme"
		data-x="600" data-hoffset="0"
		data-y="300"
		data-speed="600"
		data-start="2800"
		>
		<a class="btn btn-lg btn-border btn-slider" title="" href="/advertisers/">
			<i class="icon-down-open-big"></i> Подробнее
		</a>

	</div>
</li>

<li data-transition="fade" data-slotamount="7" data-masterspeed="1500" >
				<!-- MAIN IMAGE -->
				<img src="/static/home_assets/images/slider/rs-slider/slider-1/bg2.jpg"  alt="slidebg1"  data-bgfit="cover" data-bgposition="left top" data-bgrepeat="no-repeat">
				<!-- LAYERS -->
				<!-- LAYER NR. 2 -->
				<div class="tp-caption largeblackbg skewfromleft tp-resizeme"
				data-x="20" data-hoffset="0"
				data-y="200"
				data-speed="600"
				data-start="1400"
				data-easing="Power4.easeOut"
				data-endspeed="600"
				data-endeasing="Power0.easeIn"
				style="z-index: 3">Разработчикам

			</div>

			<!-- LAYER NR. 3 -->
			<div class="tp-caption medium_light_white skewfromleft tp-resizeme"
			data-x="20" data-hoffset="0"
			data-y="280"
			data-speed="600"
			data-start="1800"
			data-easing="Power4.easeOut"
			data-endspeed="600"
			data-endeasing="Power0.easeIn"
			style="z-index: 3"><i class="icon-right-open-mini"></i> Простая установка

		</div>

		<!-- LAYER NR. 4 -->
		<div class="tp-caption medium_light_white skewfromleft tp-resizeme"
		data-x="20" data-hoffset="0"
		data-y="320"
		data-speed="600"
		data-start="2000"
		data-easing="Power4.easeOut"
		data-endspeed="600"
		data-endeasing="Power0.easeIn"
		style="z-index: 3"><i class="icon-right-open-mini"></i> Высокий доход

	</div>
	<div class="tp-caption medium_light_white skewfromleft tp-resizeme"
		data-x="20" data-hoffset="0"
		data-y="360"
		data-speed="600"
		data-start="2200"
		data-easing="Power4.easeOut"
		data-endspeed="600"
		data-endeasing="Power0.easeIn"
		style="z-index: 3"><i class="icon-right-open-mini"></i> Реклама без потери лояльности

	</div>
	<div class="tp-caption medium_light_white skewfromleft tp-resizeme"
		data-x="20" data-hoffset="0"
		data-y="400"
		data-speed="600"
		data-start="2600"
		>
		<a class="btn btn-lg btn-border btn-slider" title="" href="/publishers/">
			<i class="icon-down-open-big"></i> Подробнее
		</a>

	</div>

</li>

<li data-transition="fade" data-slotamount="7" data-masterspeed="1500" >
	<!-- MAIN IMAGE -->
	<img src="/static/home_assets/images/slider/rs-slider/slider-1/bg4.jpg"  alt="slidebg3"  data-bgfit="cover" data-bgposition="left top" data-bgrepeat="no-repeat">
	<!-- LAYERS -->

	<!-- LAYER NR. 2 -->
	<div class="tp-caption largeblackbg skewfromleft tp-resizeme"
	data-x="20" data-hoffset="0"
	data-y="200"
	data-speed="600"
	data-start="1400"
	data-easing="Power4.easeOut"
	data-endspeed="600"
	data-endeasing="Power0.easeIn"
	style="z-index: 3">Агентствам и брендам

</div>

<div class="tp-caption medium_light_white skewfromleft tp-resizeme"
			data-x="20" data-hoffset="0"
			data-y="280"
			data-speed="600"
			data-start="1800"
			data-easing="Power4.easeOut"
			data-endspeed="600"
			data-endeasing="Power0.easeIn"
			style="z-index: 3; background-color: #3d3d3d;padding-right: 15px;"><i class="icon-right-open-mini"></i> Индивидуальный подход

		</div>

		<!-- LAYER NR. 4 -->
		<div class="tp-caption medium_light_white skewfromleft tp-resizeme"
		data-x="20" data-hoffset="0"
		data-y="320"
		data-speed="600"
		data-start="2000"
		data-easing="Power4.easeOut"
		data-endspeed="600"
		data-endeasing="Power0.easeIn"
		style="z-index: 3; background-color: #3d3d3d;padding-right: 15px;"><i class="icon-right-open-mini"></i> Внушительные объемы

	</div>
	<div class="tp-caption medium_light_white skewfromleft tp-resizeme"
		data-x="20" data-hoffset="0"
		data-y="360"
		data-speed="600"
		data-start="2200"
		data-easing="Power4.easeOut"
		data-endspeed="600"
		data-endeasing="Power0.easeIn"
		style="z-index: 3; background-color: #3d3d3d;padding-right: 15px;"><i class="icon-right-open-mini"></i> Достижение целей рекламных кампаний

	</div>

<div class="tp-caption medium_light_white skewfromleft tp-resizeme"
		data-x="20" data-hoffset="0"
		data-y="400"
		data-speed="600"
		data-start="2800"

		>
		<a class="btn btn-lg btn-border btn-slider" title="" href="/brands/">
			<i class="icon-down-open-big"></i> Подробнее
		</a>

	</div>
</li>
</ul>
</div>
</div>

</section>
<!-- slider -->


<section id="content">

					<!-- section 1 -->
					<section>

						<div class="container">
							<div class="row">
								<div class="col-md-12"><h1 style="text-align: center; padding-top: 50px;">Мобильная реклама нового поколения</h1></div>
								
								<div class="col-md-6 pt40" data-nekoanim="fadeInLeftBig" data-nekodelay="200">
									<img src="/static/home_assets/images/theme-pics/flat-mobile.png" alt="Свежие форматы мобильной рекламы" class="img-responsive" />
								</div>

								<div class="col-md-5 col-md-offset-1 pt40 mt40">
									<h2 class="unsh2">Свежие форматы : Видео/Rich Media реклама</h2>
									<p>
										Представляем вам самые свежие для Рунета решения – видео, а также интерактивную rich media рекламу для мобильных устройств. Используя возможности Vidiger как <span style="background-color: rgb(61, 61, 61);color:white">разработчик</span>, вы сможете показывать современную и качественную рекламу в своих мобильных приложениях. Вы сами выбираете категорию и место, в котором ее увидит ваш пользователь: при начальной загрузке приложения, при загрузке очередного уровня и так далее. За просмотр рекламы вы можете предоставлять пользователям игровую валюту, открывать доступ к определенному контенту. 
										Продвигайте свой бренд как <span style="background-color: rgb(61, 61, 61);color:white">рекламодатель</span> с помощью новых форматов рекламы, достигая максимального эффекта среди пользователей мобильных приложений посредством полного их вовлечения в рекламную игру
									</p>
									<a class="btn btn-lg btn-border" title="" href="/advertisers/#adformats">
										<i class="icon-right-open-big"></i> Смотреть
									</a>
								</div>
							</div>
						</div>

					</section>
					<!-- section 1 -->

					<!-- section 2 -->
					<section class="color1">
						<div class="container">
							<div class="row">
								<div class="col-md-5 pt40 mt40">
									<h2>Простой и эффективный мобильный маркетинг</h2>
									<p>Встроить наш SDK в ваше приложение максимально просто - нужно лишь добавить пару строк кода. Создать рекламную кампанию также несложно, однако при необходимости, вам всегда поможет персональный менеджер. Для рекламодателей мы разработали инструмент исключительно точного таргетинга, который позволяет показывать рекламу по широкому спектру критериев: возраст, интересы, семейное положение, местоположение и многое другое. Одновременно сервис предоставляет обширную аналитику показов. Vidiger - это все инструменты для профессионального мобильного маркетинга
									</p>
									<a class="btn btn-lg btn-border" title="" href="/publishers/">
										<i class="icon-right-open-big"></i> Смотреть
									</a>
								</div>
								<div class="col-md-6 col-md-offset-1 pt40 pb40" data-nekoanim="fadeInRightBig" data-nekodelay="300">
									<img src="/static/home_assets/images/theme-pics/flat-laptop.png" alt="Простота установки SDK" class="img-responsive"/>
								</div>
							</div>
						</div>
						
					</section>
					<!-- section 2 -->

					<!-- team -->

					


							</div>
						</div>

					</section>

					<!-- team -->
					
					<!-- content -->
				</section>
<!-- content -->
<!-- clients -->
	<section id="clients" class="pt30 pb30" data-nekoanim="fadeInUp" data-nekodelay="10">
		<div class="container">
			<div class="row">
				<div class="col-md-2  col-sm-4 col-xs-6">
					<img src="/static/home_assets/images/clients/logo_1.png" class="img-responsive" alt="Клиенты" />
				</div>

				<div class="col-md-2  col-sm-4 col-xs-6">
					<img src="/static/home_assets/images/clients/logo_2.png" class="img-responsive" alt="Клиенты"/>
				</div>

				<div class="col-md-2 col-sm-4 col-xs-6">
					<img src="/static/home_assets/images/clients/logo_3.png" class="img-responsive" alt="Клиенты"/>
				</div>

				<div class="col-md-2  col-sm-4 col-xs-6">
					<img src="/static/home_assets/images/clients/logo_4_oops.png" class="img-responsive" alt="Клиенты"/>
				</div>

				<div class="col-md-2  col-sm-4 col-xs-6">
					<img src="/static/home_assets/images/clients/logo_5.png" class="img-responsive" alt="Клиенты"/>
				</div>

				<div class="col-md-2  col-sm-4 col-xs-6">
					<img src="/static/home_assets/images/clients/logo_6.png" class="img-responsive" alt="Клиенты"/>
				</div>
			</div>
		</div>
	</section>
	<!-- clients -->

<!-- call to action -->