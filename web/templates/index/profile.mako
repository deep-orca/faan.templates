## -*- coding: utf-8 -*-
<%namespace name="bcrumb" file="/layout/bcrumb.mako" />
<%namespace name="fabutton" file="/layout/fa-button.mako" />

<%inherit file="/layout/main.mako" />
<%def name="page_css()">
<link href="/global/static/css/pages/profile.css" rel="stylesheet" type="text/css">
<style>
.form-section{
    margin-bottom: 10px;
}

.hdescription{
    padding-bottom: 20px;
}

.form-actions{
    background-color: inherit
}
</style>
</%def>


<%def name="title()">Профиль</%def>
<%def name="description()"></%def>
${ bcrumb.h(self) }

<%!
    import time
    import datetime
    from faan.core.model.security.account import Account
    from faan.core.model.security.account_extra import AccountExtraCommunication as Communication, AccountPurchasingInfo

    account_type = { Account.Groups.ADV: u'Рекламодатель',
                     Account.Groups.PUB: u'Разработчик'
                     }

    communication_type = { Communication.Account_communication_types.SKYPE: u'Skype',
                           Communication.Account_communication_types.PHONE: u'Телефон',
                           Communication.Account_communication_types.ICQ: u'ICQ',
                           Communication.Account_communication_types.JABBER: u'Jabber',
                           Communication.Account_communication_types.MAIL_RU: u'Mail.ru',
                           Communication.Account_communication_types.WHATSAPP: u'Whatsapp',
                           Communication.Account_communication_types.SITE: u'Сайт',
                          }
    communication_icon = { Communication.Account_communication_types.SKYPE: '<i class="fa fa-skype"></i>',
                           Communication.Account_communication_types.PHONE: '<i class="fa fa-phone"></i>',
                           Communication.Account_communication_types.ICQ: '<i style="margin-top: 5px;"><img src="/global/static/img/social/icq_b.png"></i>',
                           Communication.Account_communication_types.JABBER: '<i class="fa fa-lightbulb-o"></i>',
                           Communication.Account_communication_types.MAIL_RU: '<i style="margin-top: 5px;"><img src="/global/static/img/social/mru_b.png"></i>',
                           Communication.Account_communication_types.WHATSAPP: '<i style="margin-top: 5px;"><img src="/global/static/img/social/whapp.png"></i>',
                           Communication.Account_communication_types.SITE: '<i class="fa fa-globe"></i>',
                          }
    purchase_dict = AccountPurchasingInfo.Type.get_dict_type()


    def get_account_type(atype):
        try:
            return account_type[atype]
        except KeyError:
            return u'Менеджер'

    def get_communication_label(ctype):
        try:
            return communication_type[ctype]
        except KeyError:
            return u'Не найдено контактной информации'
    def get_communication_icon(ctype):
        try:
            return communication_icon[ctype]
        except KeyError:
            return u'Не найдено контактной информации'


   # def get_financial_info(stats, days, account_group):
   #     money = 0
   #     month_before = int(time.time()) - days*24*60*60
   #     if stats:
   #         for stat in stats:
   #             if stat.ts_spawn < month_before:
   #                 break
   #             if account_group == Account.Groups.PUB:
   #                 money += stat.revenue
   #             if account_group == Account.Groups.ADV:
   #                 money += stat.cost
   #     return money


%>
<%
    ex_name = ''
    ex_surname = ''

    if g.extra:
        ex_name = g.extra.name
        ex_surname = g.extra.surname
%>
<div class="row profile">
<div class="col-md-12">
<!--BEGIN TABS-->
<div class="portlet">
<div class="portlet-title"><div class="caption"><i class="fa fa-user"></i>Профиль пользователя ${g.account.username}</div></div>
<div class="portlet-body">

    <div class="row profile-account">
        <div class="col-md-3">
            <ul class="ver-inline-menu tabbable margin-bottom-10">
                <li class="active">
                    <a data-toggle="tab" href="#overview">
                        <i class="fa fa-cog"></i>Обзор</a>
                                    <span class="after">
                                    </span>
                </li>
                <li>
                    <a data-toggle="tab" href="#personal-info"><i class="fa fa-cog"></i>Персональная информация </a>
                </li>
                <li>
                    <a data-toggle="tab" href="#contact-form"><i class="fa fa-picture-o"></i>Контактная информация</a>
                </li>
                <li>
                    <a data-toggle="tab" href="#pay-info"><i class="fa fa-picture-o"></i>Платежная информация</a>
                </li>
                <li>
                    <a data-toggle="tab" href="#change_pass"><i class="fa fa-lock"></i>Смена пароля</a>
                </li>
            </ul>
        </div>

        <div class="col-md-9">
            <div class="tab-content">
                <div id="overview" class="tab-pane active">
                        <div class="row">
                        <div class="col-md-12">
                            <div class="row">

                                <div class="col-md-12 profile-info" style="line-height: 30px;">
                                    <div class="col-md-12">
                                        <h3 class="form-section"> ${ex_name or ''} ${ex_surname or ''}</h3>
                                        <p class="hdescription">
                                            ${get_account_type(g.account.groups)}
                                                %if g.account.date_registration:
                                                   , зарегистрирован ${g.account.date_registration.strftime("%d.%m.%Y")}
                                                %endif
                                           
                                        </p>
                                    </div>
                                    <div class="col-md-6">
                                        <h4 class="form-section">Контактная информация</h4>
                                        <div class="row">
                                            <b class="col-md-4">
                                                <i class="fa fa-envelope"></i> e-mail:
                                            </b>

                                            <div class="col-md-8">
                                                ${g.account.username}
                                            </div>
                                        </div>
                                        %if g.communications:
                                            %for com in g.communications.keys():
                                                %if g.communications[com] != '':
                                                <div class="row">
                                                    <b class="col-md-4">
                                                        ${get_communication_icon(com)} ${get_communication_label(com)}
                                                    </b>
                                                    <div class="col-md-8">
                                                        ${g.communications[com]}
                                                    </div>
                                                </div>
                                                %endif
                                            %endfor
                                        %endif
                                    </div>
                                    <div class="col-md-6">
                                        <h4 class="form-section">Платежная информация</h4>
                                        <div class="row">
                                            <p class="col-md-12">
                                               <b class="potype">
                                               %if purchase_dict.get(g.active_purchase):
                                                   ${purchase_dict.get(g.active_purchase)}
                                               %endif
                                               </b>
                                                %if  g.purchases.get(g.active_purchase):
                                                   , счёт ${g.purchases.get(g.active_purchase)}
                                                %endif
                                            </p>
                                        </div>
                                    </div>
                                </div>
                                <!--end col-md-8-->
                                
                            </div>
                            <!--end row-->
                            
                        </div>
                    </div>

                </div>

                <div id="personal-info" class="tab-pane">
                    <div class="col-md-12">
                        <h3 class="form-section">Персональная информация</h3>
                        <p class="hdescription">Персональная информация Вашего аккаунта</p>
                    
                    <form role="form" method="POST" action="../profile/edit">
                        <div class="form-group">
                            <label class="control-label">Имя</label>
                            <input name="name" type="text" placeholder="Иван" class="form-control" value="${ex_name or ''}"/>
                        </div>
                        <div class="form-group">
                            <label class="control-label">Фамилия</label>
                            <input name="surname" type="text" placeholder="Иванов" class="form-control"
                                   value="${ex_surname or ''}"/>
                        </div>
                        <div class="form-group">
                            <label class="control-label">Email</label>
                            <input type="text" placeholder="example@example.com" class="tooltips form-control"
                                   value="${g.account.username}" readonly="readonly"
                                   data-placement="top" data-original-title="Для изменения e-mail обратитесь в поддержку"/>
                        </div>
                        <div class="form-actions">
                            <button type="submit" class="btn btn-success">Сохранить</button>
                            <a href="#" class="btn btn-default">Отмена</a>
                        </div>
                        <input type="hidden" name="extra" value="${g.extra.id}">

                        <input type="hidden" class="form-control" name="post_form" value="personal_data">
                    </form>
                    </div>
                </div>

                <div id="contact-form" class="tab-pane">
                    <form role="form" method="POST" action="../profile/edit">
                        <div class="col-md-12">
                            <h3 class="form-section">Контактная информация</h3>
                            <p class="hdescription">Добавьте контактную информацию о себе, чтобы нам было удобнее с Вами связаться</p>
                        </div>
                        %for ct in communication_type.keys():
                            <div class="form-group col-md-6">
                            <label class="control-label col-md-4" style="padding-left: 0;">${get_communication_icon(ct)} ${communication_type[ct]}</label>
                                <input type="text" name="communication_${ct}" class="form-control col-md-3"
                                       placeholder="${communication_type[ct]}" value="${g.communications[ct]}" >
                            </div>
                        %endfor
                        <div class="col-md-12 form-actions">
                            <button type="submit" class="btn btn-success">Сохранить</button>

                        </div>
                        <input type="hidden" name="extra" value="${g.extra.id}">

                        <input type="hidden" class="form-control" name="post_form" value="communication_data">
                    </form>
                </div>

                <div id="pay-info" class="tab-pane">
                    <h3 class="form-section">Платежная информация</h3>
                    <p class="hdescription">Введите платежную информацию для вывода средств с Вашего аккаунта</p>
                    <form role="form" method="POST" action="../profile/edit">
                       <div class="portlet-body">
                            <div class="tabbable tabs-left">
                                <ul class="nav nav-tabs">
                                    <li class="active">
                                        <a href="#webmoney" data-toggle="tab" id="awebmoney">
                                            <img src="/global/static/img/balance/wm.png" width="150px" alt="Webmoney">
                                        </a>
                                    </li>
                                    <li class="">
                                        <a href="#yamoney" data-toggle="tab" id="ayamoney">
                                            <img src="/global/static/img/balance/ym.png" width="130px"
                                                 alt="Яндекс.Деньги">
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#qiwi" data-toggle="tab" id="aqiwi">
                                            <img src="/global/static/img/balance/qiwi.png" width="130px"
                                                 alt="QIWI кошелек">
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#paypal" data-toggle="tab" id="apaypal">
                                            <img src="/global/static/img/balance/pp.png" width="130px" alt="Paypal">
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#card" data-toggle="tab" id="acard">
                                            <img src="/global/static/img/balance/cards.png" width="130px"
                                                 alt="Кредитная карта">
                                        </a>
                                    </li>
                                </ul>
                                <div class="tab-content">
                                    <div class="tab-pane active col-md-6" id="webmoney">
                                        <div class="col-md-12">
                                            <h3>WebMoney</h3>
                                            <p><input name="purchase_1" type="text" placeholder="R-кошелек Webmoney" class="form-control" value="${g.purchases[1]}"/></p>
                                            <p><input type="checkbox" name="type_1" class="p_type">  использовать по умолчанию.</p>
                                        </div>
                                    </div>
                                    <div class="tab-pane fade col-md-6" id="yamoney">
                                        <div class="col-md-12">
                                            <h3>Я.деньги</h3>
                                            <p>
                                            <input name="purchase_2" type="text" placeholder="Номер счета Яндекс.Деньги" class="form-control"  value="${g.purchases[2]}"/>
                                            </p>
                                            <p><input type="checkbox" name="type_2" class="p_type">  использовать по умолчанию.</p>
                                        
                                        </div>
                                    </div>
                                    <div class="tab-pane fade col-md-6" id="qiwi">
                                        <div class="col-md-12">
                                            <h3>QIWI</h3>
                                            <p>
                                            <input name="purchase_5" type="text" placeholder="Кошелек QIWI" class="form-control"  value="${g.purchases[5]}"/>
                                            </p>
                                            <p><input type="checkbox" name="type_5" class="p_type">  использовать по умолчанию.</p>
                                        </div>
                                    </div>
                                    <div class="tab-pane fade col-md-6" id="paypal">
                                        <div class="col-md-12">
                                            <h3>PayPal</h3>
                                            <p>
                                                <input name="purchase_3" type="text" placeholder="Аккаунт PayPal" class="form-control"  value="${g.purchases[3]}"/>
                                            </p>
                                            <p><input type="checkbox" name="type_3" class="p_type"> использовать по умолчанию.</p>
                                        </div>
                                    </div>
                                    <div class="tab-pane fade col-md-6" id="card">

                                        <div class="col-md-12">
                                            <h3>Кредитная карта</h3>
                                            <p>
                                            <input name="purchase_4" type="text" placeholder="Номер карты..." class="form-control"  value="${g.purchases[4]}"/>
                                            </p>
                                            <p><input type="checkbox" name="type_4" class="p_type">  использовать по умолчанию.</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-offset-3 col-md-8 form-actions">
                            <button type="submit" class="btn btn-success">Сохранить</button>
                        </div>
                        <input type="hidden" name="extra" value="${g.extra.id}">

                        <input type="hidden" class="form-control" name="post_form" value="purchase_data">
                    </form>
                </div>

                <div id="change_pass" class="tab-pane">
                    
                    <form role="form" method="POST" action="../profile/edit">
                        <div class="col-md-12">
                            <h3 class="form-section">Изменить пароль</h3>
                            <p class="hdescription">Введите текущий и новый пароли в форму ниже</p>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label">Текущий пароль</label>
                                <input name="old" type="password" class="form-control" value=""/>
                            </div>
                            <div class="form-group">
                                <label class="control-label">Новый пароль</label>
                                <input name="pass0" type="password" class="form-control"/>
                            </div>
                            <div class="form-group">
                                <label class="control-label">Повторите пароль</label>
                                <input name="pass1" type="password" class="form-control"/>
                            </div>
                            </div>
                        <div class="form-actions col-md-12">
                            <button type="submit" class="btn btn-success">Сохранить</button>
                            <a href="#" class="btn btn-default">Отмена</a>
                        </div>
                        
                        <input type="hidden" class="form-control" name="post_form" value="password_data">
                    </form>
                    
                </div>
            </div>
        </div>
        <!--end col-md-9-->
    </div>

</div>
</div>
<!--end tab-pane-->


<!--END TABS-->
</div>
</div>
<!-- END PAGE CONTENT-->

<script>
$(function(){
    $('[name=type_${g.active_purchase}').prop('checked',true).uniform();
    $('#atab_${g.active_purchase}').click();
    if (document.URL.split('/')[5] != ''){
        $('body').animate({ scrollTop: 0 }, "slow");
    }
});
$('.p_type').click(function(){
      $('input[type=checkbox]').prop('checked',false).uniform();
       $(this).prop('checked',true).uniform();
});

</script>