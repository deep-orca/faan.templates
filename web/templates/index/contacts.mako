# -*- coding: utf-8 -*-
<%inherit file="layout/main.mako" />
<%def name="title()">
Контакты
</%def>

<%def name="description()">Где купить мобильный трафик. Как монетизировать приложение. Как заработать на мобильном приложении</%def>

<%def name="page_js()">
    <script type="text/javascript">
        $(document).on("ready", function() {
            var q = window.location.search.replace('?', '').split('=');
            var success = (q[0] == 'success' && q[1] == 'true');
            console.debug(success);

            if (success) {
                $("#success").modal();
            }
        });
    </script>
</%def>

<%def name="page_css()">
<style>
textarea#comments{
    height: 77px;
}
</style>
</%def>

        <section id="page">
            <header class="page-header">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-6">
                            <h1>Напишите нам</h1>
                        </div>

                    </div>
                </div>
            </header>
            <section id="content" class="mt30">
                <div class="container">

                    <div class="row">
                        <div class="col-sm-4">
                            <h4>Адрес:</h4>
                            <address>
                                <p>
                                    <i class="icon-location"></i>&nbsp;Москва<br>
                                    4 Сыромятнический переулок 3/5 <br>
                                    <i class="icon-phone"></i>&nbsp; +7 (495) 964-14-56  <br>
                                    <i class="icon-skype"></i>&nbsp;<a href="callto:vidiger.official">vidiger.official</a> <br>
                                    <i class="icon-mail-alt"></i>&nbsp;<a href="mailto:contact@vidiger.com">contact@vidiger.com</a>
                                </p>
                            </address>
                            <h4>Мы в социальных сетях</h4>
                            <ul class="socialNetwork">
                                <li><a href="http://vk.com/vidiger" class="tips" title="Присоединяйтесь к нам в VK" target="_blank"><i class="icon-vkontakte iconRounded"></i></a></li>
                                <li><a href="https://www.facebook.com/vidiger.official" class="tips" title="Присоединяйтесь к нам на Facebook" target="_blank"><i class="icon-facebook-1 iconRounded"></i></a></li>
                                <li><a href="https://twitter.com/vidiger" class="tips" title="Присоединяйтесь к нам в Twitter" target="_blank"><i class="icon-twitter-bird iconRounded"></i></a></li>
                                <li><a href="https://plus.google.com/+VidigerOfficial" class="tips" title="Присоединяйтесь к нам на Google+" target="_blank"><i class="icon-gplus-1 iconRounded"></i></a></li>
                                <li><a href="#" class="tips" title="Присоединяйтесь к нам в Linkedin" target="_blank"><i class="icon-linkedin-1 iconRounded"></i></a></li>
                                <li><a href="#" class="tips" title="Присоединяйтесь к нам в Dribble" target="_blank"><i class="icon-dribbble iconRounded"></i></a></li>
                            </ul>    
                        </div>
                          <script type="text/javascript">
                            var RecaptchaOptions = {
                               theme : 'white'
                            };
                            </script>
                        <form method="post" action="/contacts/" id="contactfrm">
                            ${form.csrf_token}
                            <div class="col-sm-4">
                                <div class="form-group ${'has-error text-danger' if form.name.errors else ''}">
                                    <label for="name">Имя</label>
                                    <input type="text" class="form-control" name="name" id="name" placeholder="Ваше имя"  title="Пожалуйста, введите Ваше имя" value="${form.name._value()}"/>
                                    %if form.name.errors:
                                        <div class="x-form-error">${form.name.errors[0]}</div>
                                    %endif
                                </div>
                                <div class="form-group ${'has-error text-danger' if form.email.errors else ''}">
                                    <label for="email">Email</label>
                                    <input type="email" class="form-control" name="email" id="email" placeholder="Ваш email" title="Пожалуйста, введите действительный адрес e-mail"
                                           value="${form.email._value()}"/>
                                    %if form.email.errors:
                                        <div class="x-form-error">${form.email.errors[0]}</div>
                                    %endif
                                </div>
                                <div class="form-group ${'has-error text-danger' if form.phone.errors else ''}">
                                    <label for="phone">Телефон</label>
                                    <input name="phone" class="form-control required digits" type="tel" id="phone" size="30" placeholder="Контатный телефон" title="Пожалуйста, введите номер телефона вида 79991234567"
                                           value="${form.phone._value()}">
                                    %if form.phone.errors:
                                        <div class="x-form-error">${form.phone.errors[0]}</div>
                                    %endif
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group ${'has-error text-danger' if form.message.errors else ''}">
                                    <label for="comments">Сообщение</label>
                                    <textarea
                                            name="message"
                                            class="form-control"
                                            id="comments"
                                            cols="3" rows="5"
                                            placeholder="Текст Вашего сообщения"
                                            title="Пожалуйста, введите текст сообщения (не менее 10 символов)">${form.message._value()}</textarea>
                                    %if form.message.errors:
                                        <div class="x-form-error">${form.message.errors[0]}</div>
                                    %endif
                                </div>
                                <fieldset class="clearfix securityCheck">

                                    <div class="form-group ${'has-error text-danger' if form.captcha.errors else ''}">
                                        <label>Проверка</label>
                                        ${form.captcha}
                                        %if form.captcha.errors:
                                            <div class="x-form-error">${form.captcha.errors[0]}</div>
                                        %endif
                                    </div>
                                </fieldset>
                            </div>
                            <div class="col-md-8 col-md-offset-4">
                                <input type="submit" class="btn btn-lg" id="submit" value="Отправить">
                            </div>

                            <div id="success" class="modal fade">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-body">
                                            <h3>Сообщение отправлено!</h3>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-success" data-dismiss="modal">OK
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <div id="mapWrapper" class="mt30"></div>
            </section>
        </section>

