# -*- coding: utf-8 -*-
<%inherit file="layout/main.mako" />
<%def name="title()">
    Агентствам и брендам
</%def>

<%def name="description()">Брендовая реклама в ios и android приложениях. Покупка и создание рекламы для мобильных ресурсов</%def>

<%def name="page_js()">
    <script type="text/javascript">
        $(document).on("ready", function () {
            var q = window.location.search.replace('?', '').split('=');
            var success = (q[0] == 'success' && q[1] == 'true');
            console.debug(success);

            if (success) {
                $("#success").modal();
            }
        });
    </script>
</%def>

<%def name="page_css()">
    <style>
        .page-header.bigHeader {
            background-image: url('/static/home_assets/images/theme-pics/brands_bg.jpg');
        }

        .page-header.bigHeader h1 {
            font-family: 'Roboto Condensed', sans-serif;
            color: white;
            background-color: rgba(0, 0, 0, 0.66);
            padding: 10px 20px 10px 20px;
        }

        .page-header.bigHeader p {
            padding: 10px 20px 10px 20px;
            background: #fff;
        }

        .mb15 {
            padding-left: 20px;
            padding-right: 20px;
        }

        textarea#comments {
            height: 77px;
        }
    </style>


</%def>


<section id="page">
    <header class="page-header bigHeader">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <h1>Индивидуальные рекламные решения</h1>

                    <p>Агентствам и брендам зачастую требуются нестандартные рекламные решения. Для таких случаев
                        Vidiger готов создать для Вас уникальные рекламные кампании, которые полностью будут
                        удовлетворять Вашим запросам.</p>
                    <a href="/contacts" class="btn btn-lg btn-primary mb15 btn-border">Связаться с нами</a>
                </div>

            </div>
        </div>
    </header>

    <!--
    <section id="content" class="pb30">


        <section class="largeQuote pb40 pt40">
            <div class="container">
                <div class="row">
                    <div class="span12 text-center">
                        <h1>Рекламные решения любой сложности от <strong>Vidiger</strong><br><small>Напишите нам и для начала блестящих рекламных кампаний</small></h1>

                    </div>
                </div>
            </div>
        </section>
    -->

    <!-- contact -->
    <section id="contact">
        <div class="col-md-12 text-center pt30 pb30">
            <h1>Напишите нам</h1>

            <h2 class="subTitle">Мы готовы к любому виду сотрудничества и обеспечим Вам блестящие рекламные
                кампании</h2>
        </div>


    </section>

    <section id="content" class="mt30">
        <div class="container">

            <div class="row">
                <div class="col-sm-4">
                    <h4>Адрес:</h4>
                    <address>
                        <p>
                            <i class="icon-location"></i>&nbsp;Москва<br>
                            4 Сыромятнический переулок 3/5 <br>
                            <i class="icon-phone"></i>&nbsp; +7 (495) 964-14-56 <br>
                            <i class="icon-mail-alt"></i>&nbsp;<a
                                href="mailto:contact@vidiger.com">contact@vidiger.com</a>
                        </p>
                    </address>
                </div>
                <script type="text/javascript">
                    var RecaptchaOptions = {
                        theme: 'white'
                    };
                </script>
                <form method="post" action="/brands/" id="contactfrm">
                    ${form.csrf_token}
                    <div class="col-sm-4">
                        <div class="form-group ${'has-error text-danger' if form.name.errors else ''}">
                            <label for="name">Имя</label>
                            <input type="text" class="form-control" name="name" id="name" placeholder="Ваше имя"
                                   title="Пожалуйста, введите Ваше имя" value="${form.name._value()}"/>
                            %if form.name.errors:
                                <div class="x-form-error">${form.name.errors[0]}</div>
                            %endif
                        </div>
                        <div class="form-group ${'has-error text-danger' if form.email.errors else ''}">
                            <label for="email">Email</label>
                            <input type="email" class="form-control" name="email" id="email"
                                   placeholder="Ваш email"
                                   title="Пожалуйста, введите действительный адрес e-mail"
                                   value="${form.email._value()}"/>
                            %if form.email.errors:
                                <div class="x-form-error">${form.email.errors[0]}</div>
                            %endif
                        </div>
                        <div class="form-group ${'has-error text-danger' if form.phone.errors else ''}">
                            <label for="phone">Телефон</label>
                            <input name="phone" class="form-control required digits" type="tel" id="phone"
                                   size="30" placeholder="Контатный телефон"
                                   title="Пожалуйста, введите номер телефона вида 79991234567"
                                   value="${form.phone._value()}">
                            %if form.phone.errors:
                                <div class="x-form-error">${form.phone.errors[0]}</div>
                            %endif
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group ${'has-error text-danger' if form.message.errors else ''}">
                            <label for="comments">Сообщение</label>
                            <textarea
                                    name="message"
                                    class="form-control"
                                    id="comments"
                                    cols="3" rows="5"
                                    placeholder="Текст Вашего сообщения"
                                    title="Пожалуйста, введите текст сообщения (не менее 10 символов)">${form.message._value()}</textarea>
                            %if form.message.errors:
                                <div class="x-form-error">${form.message.errors[0]}</div>
                            %endif
                        </div>
                        <fieldset class="clearfix securityCheck">

                            <div class="form-group ${'has-error text-danger' if form.captcha.errors else ''}">
                                <label>Проверка</label>
                                ${form.captcha}
                                %if form.captcha.errors:
                                    <div class="x-form-error">${form.captcha.errors[0]}</div>
                                %endif
                            </div>
                        </fieldset>
                    </div>
                    <div class="col-md-8 col-md-offset-4">
                        <input type="submit" class="btn btn-lg" id="submit" value="Отправить">
                    </div>

                    <div id="success" class="modal fade">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-body">
                                    <h3>Сообщение отправлено!</h3>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-success" data-dismiss="modal">OK
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <div id="mapWrapper" class="mt30"></div>
    </section>
</section>

<section class="color2 pb40 pt40">
    <div class="ctaBox ctaBoxFullwidth">
        <div class="container">
            <div class="row">
                <div class="col-md-8">
                    <h1>
                        <strong>Зарегистрируйтесь в сети Vidiger</strong><br> если Вам требуется стандартное размещение
                    </h1>
                </div>
                <div class="col-md-4">
                    <a class="btn btn-lg btn-border" title="" href="/sign/register" target="blank">
                        <i class="icon-down-open-big"></i> Зарегистрироваться
                    </a>
                </div>
            </div>
        </div>
    </div>
</section>