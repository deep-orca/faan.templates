# -*- coding: utf-8 -*-
<%inherit file="layout/main.mako" />
<%def name="title()">
Рекламодателям
</%def>

<%def name="description()">Купите мобильный трафик на сайтах и в приложениях с помощью технологии интерактивной рекламы</%def>

<%def name="page_js()">
    <script src="//vjs.zencdn.net/4.9/video.js"></script>
    <script type="text/javascript" src="/global/static/plugins/fancybox/source/jquery.fancybox.pack.js"></script>
    <script type="text/javascript">
        $(".servicepad").hover(function(){$(this).toggleClass("welled").find(".iconBig").toggleClass("hovered")});

        $(document).ready(function(){
            $(".fbox").fancybox();
            var getVideoContent = function (filename, w, h) {
                var content = '' +
                    '<video id="video-box" class="video-js vjs-default-skin"' +
                    'controls preload="auto" width="{w}" height="{h}"' +
                    'poster="http://video-js.zencoder.com/oceans-clip.png">' +
                    '<source src="{filename}" type="video/mp4" />' +
                    '<p class="vjs-no-js">To view this video please enable JavaScript, and consider upgrading to a web browser that <a href="http://videojs.com/html5-video-support/" target="_blank">supports HTML5 video</a></p>' +
                    '</video>';

                content = content.replace('{filename}', filename).replace('{w}', w || 720).replace('{h}', h || 457);

                return content
            };
            $('.fbox_video').on('click', function (event) {
                event.preventDefault();
                var filename = $(this).attr('href');
                $.fancybox.open([filename], {
                    content: getVideoContent(filename),
                    afterShow: function () {
                        $('#video-box source').attr('src', filename);
                        videojs(document.getElementById('video-box'), {}, function () {
                            this.load().play();
                        });
                    },
                    beforeClose: function () {
                        videojs('video-box', {}, function () {
                            this.pause();
                        });
                    }
                });
            });
        })
    </script>
</%def>

<%def name="page_css()">
<link rel="stylesheet" href="/global/static/plugins/fancybox/source/jquery.fancybox.css" type="text/css" media="screen" />
##<link href="//example.com/path/to/video-js.css" rel="stylesheet">

<link href="//vjs.zencdn.net/4.9/video-js.css" rel="stylesheet">


<style type="text/css">
.servicepad{
    height: 200px;
    padding: 20px;
    margin-bottom: 20px;
}

.welled{
    padding: 19px;
    margin-bottom: 19px;
    background-color: #f5f5f5;
    border: 1px solid #e3e3e3;
    border-radius: 4px;
    -webkit-box-shadow: inset 0 1px 1px rgba(0,0,0,.05);
    box-shadow: inset 0 1px 1px rgba(0,0,0,.05);
}

.iconBig{
    color:white;
}

.iconBig:hover{

    background-color: #3d3d3d!important;
}

.hovered{
    color: #fff;
    text-shadow: 0 0 5px #FFF;
    border-style: solid;
    background-color: #3d3d3d!important;
}
    
.fancybox-inner {
    overflow: hidden !important;
}
</style>
</%def>
<section id="content">

    <!-- services -->
    <section id="services" class=" color1">
        <div class="col-md-12 text-center pt30 pb30">
            <h1>Наши услуги</h1>
            <p>Широкий набор инструментов для рекламщика</p>
        </div>
        <div class="container pb30 pt30 ">

            <div class="row">
                <div class="col-md-6 servicepad">
                    <div class="boxIconServices posLeft clearfix">
                        <i class="icon-heart iconBig iconRounded" style="background-color: #00aa3a;"></i>
                        <div class="boxContent">
                            <h2>Точный таргетинг</h2>
                            <p>Вы можете выбрать именно ту аудиторию, которая вам нужна по широкому списку критериев - от точного георасположения пользователя, до его интересов, семейного положения и социальных параметров</p>
                        </div>
                    </div>
                </div>

                <div class="col-md-6 servicepad">
                    <div class="boxIconServices clearfix">
                        <i class="icon-edit iconBig iconRounded" style="background-color: #b91617;"></i>
                        <div class="boxContent">
                            <h2>Расширенная статистика и аналитика</h2>
                            <p>Vidiger предоставляет возможно получать статистику по любым возможным срезам, выгружать ее в удобном формате, а так же интегрировать во внешние системы статистики, поддерживая при этом трекинг трафика</p>
                        </div>
                    </div>
                </div>

            </div>

            <div class="row">
                <div class="col-md-6 servicepad">
                    <div class="boxIconServices posLeft clearfix">
                        <i class="icon-docs iconBig iconRounded" style="background-color: #003760;"></i>
                        <div class="boxContent">
                            <h2>Рекламные материалы</h2>
                            <p>Ваша реклама будет использовать все возможности смартфона от камеры до компаса. Кроме стандартных рекламных видео-роликов вы можете добавить лендинговые страницы, оверлеи и любое другое рекламное решение, которое мы выполним для вас в индивидуальном порядке. Кроме того, мы создадим для вас рекламные материалы, если у Вас нет таковых</p>
                        </div>
                    </div>
                </div>

                <div class="col-md-6 servicepad">
                    <div class="boxIconServices clearfix">
                        <i class="icon-cog iconBig iconRounded" style="background-color: #fd7e0d;"></i>
                        <div class="boxContent">
                            <h2>Удобство и поддержка</h2>
                            <p>Вам будет выделен персональный менеджер, который поможет создать и настроить кампании, а так же молниеносная техподдержка</p>
                        </div>
                    </div>
                </div>

            </div>

        </div>
    </section>
    <!-- services -->







<section class="pt40 pb40" id="adformats">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <img src="/static/home_assets/images/theme-pics/flat-mobile.png" alt="services" class="Форматы мобильной рекламы" data-nekoanim="fadeInUp" data-nekodelay="10">
                </div>
                <div class="col-md-6">
                    <h1>Наши рекламные форматы</h1>
                    <p>Рынок мобильных технологий активно развивается. Не стоит на месте и мобильная реклама, использующая для своих целей расширенные возможности смартфонов и планшетов. Стоит отметить, что в рунете отсутствует такой формат как видео и интерактивная реклама на мобильных устройствах. Кроме того, существующие форматы рекламы довольно назойливые и неудобные для пользователя.
                    </p>
                    <p>Мы представляем новые рекламные форматы, более качественные наглядные для аудитории, нежели устаревшие баннерные и текстовые решения и, следовательно, эффективнее доносящие рекламный посыл.</p>
                    <strong>Ниже вы можете ознакомиться с нашими самыми популярными рекламными форматами.</strong>

                    <ul class="nav nav-tabs mt30" id="myTab">
                        <li class="active"><a href="#interstitial" data-toggle="tab">Видео</a></li>
                        <li><a href="#banners" data-toggle="tab">Баннеры</a></li>
                        <li><a href="#native" data-toggle="tab">Встроенная реклама</a></li>
                        <li><a href="#rich" data-toggle="tab">Интерактивная реклама</a></li>
                    </ul>

                    <div class="tab-content">
                        <!-- tab 1 -->
                        <div class="tab-pane fade active in" id="interstitial">
                            <div class="row">
                                <div class="col-md-12">
                                    <h3>Промежуточное видео</h3>

                                    <p>
                                        Рекламное видео отображается, например, при переходе в новом уровне в игре, при запуске утилиты, при открытии определенных разделов приложения. В видео возможно добавление оверлея и лендинга.
                                    </p>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="thumbnail">
                                        <a href="/static/home_assets/images/adformats/interstitial.jpg" class="fbox"><img src="/static/home_assets/images/adformats/interstitial_small.jpg" alt="Полноэкранная мобильная реклама"></a>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <pre><h3 style="margin:0"><a href="/static/home_assets/video/interstitial_360.mp4" class="fbox_video text-primary"><i class="icon-play"></i>Смотреть пример</a></h3></pre>
                                </div>
                            </div>
                        </div>



                        <!-- tab 2 -->
                        <div class="tab-pane fade" id="banners">
                            <div class="row">
                                <div class="col-md-12">
                                    <h3>Стандартные и Rich-media баннеры</h3>

                                    <p>
                                        Пользователю показывается баннер внутри приложения, который может быть статичным, анимированным или Rich-media баннером, поддерживающим дополнительные возможности анимации и использования расширенного функционала смартфона.
                                    </p>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="thumbnail">
                                        <a href="/static/home_assets/images/adformats/banner_video.png" class="fbox"><img src="/static/home_assets/images/adformats/banner_video_small.png" alt="Мобильные баннеры"></a>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <pre><h3 style="margin:0"><a href="/static/home_assets/video/banner_video.mp4" class="fbox_video text-primary"><i class="icon-play"></i>Смотреть пример</a></h3></pre>
                                </div>
                            </div>
                        </div>



                        <div class="tab-pane fade" id="native">
                            <div class="row">
                                <div class="col-md-5">
                                    <div class="thumbnail">
                                        <a href="/static/home_assets/presentation/native-ads.png" class="fbox">
                                            <img src="/static/home_assets/presentation/native-ads.png" alt="Нативная реклама">
                                        </a>
                                    </div>
                                </div>
                                <div class="col-md-7">
                                    <h3>Встроенная реклама</h3>

                                    <p>
                                        Рекламный баннер или видео стилизуются под общий стиль приложения, в котором они отображаются. Это гарантирует высокий CTR и привлечение внимания пользователей, привыкших к стандартной рекламе.
                                    </p>


                                </div>

                            </div>
                        </div>

                        <div class="tab-pane fade" id="rich">
                            <div class="row">
                                <div class="col-md-12">
                                    <h3>Интерактивная реклама</h3>

                                    <p>
                                        Реклама напрямую взаимодействует с пользователем, достигая своего максимального эффекта засчет нестандартного подхода. Вы можете использовать все возможности смартфона: камеру, акселерометр, гироскоп, компас, жестикуляцию и др. При использовании такого типа рекламы может быть создано мини-приложение, в котором максимально подробно описывается рекламируемая услуга прямо внутри приложения пользователя.
                                    </p>
                                    <b>Такой тип рекламы согласовывается индивидуально с каждым рекламодателем. Рекламные материалы также создаются под нужды каждой рекламной кампании в индивидуальном порядке. <a href="/contacts">Свяжитесь с нами</a>, чтобы узнать подробности.</b><br>
                                </div>
                            </div>
                            <div class="row" style="margin-top: 20px;">
                                <div class="col-md-12">
                                    <pre><h3 style="margin:0"><a href="/static/home_assets/video/extended_ads/ebay.m4v" class="fbox_video text-primary"><i class="icon-play"></i>Смотреть пример</a></h3></pre>
                                </div>
                            </div>
                        </div>



                        </div>



                    </div>



                </div>

            </div>
        </div>
    </section>

<!-- clients -->
    <section id="clients" class="pt30 pb30 color1" data-nekoanim="fadeInUp" data-nekodelay="10">
        <div class="container">
            <div class="row text-center">
                <h1>Наши клиенты</h1>
            </div>
            <div class="row">
                <div class="col-md-2  col-sm-4 col-xs-6">
                    <img src="/static/home_assets/images/clients/logo_1.png" class="img-responsive" alt="Клиенты" />
                </div>

                <div class="col-md-2  col-sm-4 col-xs-6">
                    <img src="/static/home_assets/images/clients/logo_2.png" class="img-responsive" alt="Клиенты"/>
                </div>

                <div class="col-md-2 col-sm-4 col-xs-6">
                    <img src="/static/home_assets/images/clients/logo_3.png" class="img-responsive" alt="Клиенты"/>
                </div>

                <div class="col-md-2  col-sm-4 col-xs-6">
                    <img src="/static/home_assets/images/clients/logo_4_oops.png" class="img-responsive" alt="Клиенты"/>
                </div>

                <div class="col-md-2  col-sm-4 col-xs-6">
                    <img src="/static/home_assets/images/clients/logo_5.png" class="img-responsive" alt="Клиенты"/>
                </div>

                <div class="col-md-2  col-sm-4 col-xs-6">
                    <img src="/static/home_assets/images/clients/logo_6.png" class="img-responsive" alt="Клиенты"/>
                </div>
            </div>
        </div>
    </section>
    <!-- clients -->
    <!--Call to action-->
    <section class="color2 pb40 pt40">
        <div class="ctaBox ctaBoxFullwidth">
            <div class="container">
                <div class="row">
                    <div class="col-md-8">
                        <h1 style="font-weight: 300; font-size: 34px;">
                            Чтобы начать размещать Вашу рекламу в сети Vidiger, зарегистрируйтесь на сайте
                        </h1>
                    </div>
                    <div class="col-md-4">
                        <a class="btn btn-lg btn-border" title="" href="/sign/register?advertiser">
                            <i class="icon-down-open-big"></i> Зарегистрироваться
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--End Call to action-->
        <!-- contact -->
        <section id="contact">
            <div class="col-md-12 text-center pt30 pb30">
                <h1>Напишите нам</h1>
                <h2 class="subTitle">Мы готовы к любому виде сотрудничества и обеспечим вам блестящие рекламные кампании</h2>
            </div>
            <div class="container">
                <div class="row pb30">
                    <div class="col-md-12 mb15">
                        <!-- map -->
                        <div id="mapWrapper" class="mt30"></div>
                        <!-- map -->
                    </div>
                    <!-- contact intro -->
                    <div class="col-md-3">
                        <h3>Контактная информация</h3>
                        <div class="divider"><span></span></div>
                        <p>Офис компании Vidiger находится неподалеку от станции метро Курская московского метрополитена. Мы готовы встретиться с нашими клиентами по вопросам сотрудничества в любое время по будним дням. Свяжитесь с нами с помощью контактов, указанных далее.</p>
                    </div>
                    <!-- contact intro -->
                    <div class="col-md-3 col-sm-4">
                        <h4>Офис Vidiger</h4>
                        <div class="divider"><span></span></div>
                        <address>
                            <i class="icon-location"></i>4 Сыромятнический пер., 3/5<br>
                            Москва, Российская Федерация <br>
                            <br>
                            <i class="icon-mobile"></i>+7 (495) 964-14-56<br>
                            <i class="icon-mail"></i><a href="mailto:contact@vidiger.com">contact@vidiger.com</a>
                        </address>
                    </div>
                    <!-- address bloc -->
                    <div class="col-md-3 col-sm-4">
                        <h4></h4>
                        <div class="divider"><span></span></div>
                        <address>


                        </address>
                    </div>
                    <!-- address bloc -->
                    <!-- address bloc -->
                    <div class="col-md-3 col-sm-4">
                        <h4></h4>
                        <div class="divider"><span></span></div>
                        <address>


                        </address>
                    </div>
                    <!-- address bloc -->
                    <!-- address bloc -->

                    <!-- address bloc -->
                </div>
            </div>


        </section>



    </section>