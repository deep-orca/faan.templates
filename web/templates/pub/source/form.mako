## -*- coding: utf-8 -*-
<%namespace name="bcrumb" file="/layout/bcrumb.mako" />
<%inherit file="/layout/main.mako" />

<%
    from faan.core.model.pub.unit import Unit
%>

<%def name="title()">
    % if g.source.id:
        Источник ${g.source.name}
    % else:
        Создание источника
    %endif
</%def>
<%def name="description()">Настройки источника</%def>

${ bcrumb.h(self) }
${ bcrumb.bc([
    {
        'name' : u'Площадки',
        'href' : '/pub/application/',
        'icon' : 'rocket'
    },
    {
        'name' : u"%s<sup>[%s]</sup>" % (g.app.name, g.app.id),
        'href' : '/pub/application/%s/unit' % g.app.id
    },
    {
        'name' : u'%s<sup>[%s]</sup>' % (g.unit.name, g.unit.id),
        'href' : '/pub/application/%s/unit/%s/source' % (g.app.id, g.unit.id)
    },
    {
        'name' : (u'%s<sup>[%s]</sup>' % (g.source.name, g.source.id)) if g.source.id else u'Добавление нового источника'
    }
]) }

<%def name="page_css()">
    <link href="/global/static/plugins/ion.rangeslider/css/ion.rangeSlider.css" rel="stylesheet" type="text/css"/>
    <link href="/global/static/plugins/ion.rangeslider/css/ion.rangeSlider.Conquer.css" rel="stylesheet"
          type="text/css"/>
    <style>
        .xform-error {
            padding: 3px;
            font-weight: bold !important;
        }

        .btn-tall {
            height: 60px;
        }

        .btn-wide {
            width: 180px;
        }

        .zone-preview {
            position: absolute;
            top: 10px;
            right: 10px;
        }
    </style>
</%def>

<%def name="page_js()">
    <script src="/global/static/plugins/ion.rangeslider/js/ion-rangeSlider/ion.rangeSlider.min.js"></script>
</%def>


<!-- BEGIN PAGE CONTENT-->
<div class="row">
    <div class="col-md-12">
##<pre>
##Source class:           ${g.source.__class__.__name__}
##Form class:             ${form.__class__.__name__}
##Form template path:     ${form.template}
##Errors:                 ${form.errors or u'No errors'}
##Message:                ${g.message}
##</pre>
        %if g.message:
            <div class="alert alert-${g.message[1]}">
                <button type="button" class="close" data-dismiss="alert">
                    <span aria-hidden="true">&times;</span>
                </button>
                <i class="fa fa-fw fa-1x fa-info"></i> ${g.message[0]}
            </div>
        %endif
        <div class="portlet">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-sitemap"></i>
                    %if not g.source.id:
                        Добавление источника
                    %else:
                        Источник ${g.source.name}<sup>[${g.source.id}]</sup>
                    %endif
                </div>
            </div>
            <div class="portlet-body" style="position: relative;">
                <div class="row">
                    <%include file="${partial}" />
                </div>
            </div>
        </div>
    </div>
</div>

<!-- END PAGE CONTENT-->
<script type="text/javascript">
    $(document).on('ready', function () {
                $("#duration-slide")
                    .ionRangeSlider({
                        min: 0,
                        max: 45,
                        from: ${form.data.get('min_duration') or 0},
                        to: ${form.data.get('max_duration') or 15},
                        type: 'double',
                        step: 1,
                        postfix: " сек.",
                        prettify: false,
                        hasGrid: true,
                        onChange: function(obj) {
                            $("#min_duration").val(obj.fromNumber);
                            $("#max_duration").val(obj.toNumber);
                        }
                    });

                $("#closable-slide")
                    .ionRangeSlider({
                        min: 0,
                        max: 45,
                        from: ${form.data.get('closable') or 0},
                        type: 'single',
                        step: 1,
                        postfix: " сек.",
                        prettify: false,
                        hasGrid: true,
                        onChange: function (obj) {
                            $("#closable").val(obj.fromNumber);
                        }
                    });
##
##        var updatePreview = function () {
##            var imgClass = '.zone-preview-' + $('#device_type').val() + $('#type').val();
##            $('.zone-preview-img').hide();
##            $(imgClass).show();
##        };
##
##        var updateShowVideo = function (element) {
##            if (element.is(':checked')) {
##                $('*[data-if-show-video]').show();
##            } else {
##                $('*[data-if-show-video]').hide();
##            }
##        };
##
##        var isFloat = function (a) {
##            return a == +a && +a !== (+a | 0)
##        };
##        var isInt = function (a) {
##            return a == +a && +a === (+a | 0)
##        };
##
##        ##        var checkboxShowVideo = $('#show_video');
##
##        ##        updateShowVideo(checkboxShowVideo);
##
##        ##        checkboxShowVideo.on('change', function () {
##        ##            updateShowVideo($(this));
##        ##        });
##
##        $('a.type').on('click', function () {
##            $('a.type').removeClass('active');
##            $(this).addClass('active');
##            $('*[data-if-type]').hide();
##            $('*[data-if-type="' + $(this).data('type') + '"]').show();
##            $('#type').val($(this).data('type'));
##            if (+$(this).data('type') == ${Unit.Type.BANNER}) {
##                $('#show_video').prop('checked', false);
##                $.uniform.update();
##                ##                updateShowVideo(checkboxShowVideo);
##                            }
##            //updatePreview();
##        });
##        $('a.type[data-type="1"]').trigger('click');
##
##        var phoneSizes = [
##            $('#size > option[value="${';'.join((str(v) for v in Unit.Size.BANNER()))}"]').clone(),
##            $('#size > option[value="${';'.join((str(v) for v in Unit.Size.LARGE_BANNER()))}"]').clone(),
##            $('#size > option[value="${';'.join((str(v) for v in Unit.Size.MEDIUM_RECTANGLE()))}"]').clone()
##        ];
##        var tabletSizes = $('#size > option').clone();
##
##        $('a.device-type').on('click', function () {
##            $('a.device-type').removeClass('active');
##            $(this).addClass('active');
##            $('#device_type').val($(this).data('device-type'));
##            $('#size')
##                    .empty()
##                    .append(($(this).data('device-type') == ${Unit.DeviceType.PHONE}) ? phoneSizes : tabletSizes)
##                    .trigger('change')
##                    .select2();
##            //updatePreview();
##        });
##        $('a.device-type[data-device-type="1').trigger('click');
##
        $('button[type="submit"]').on('click', function () {
            console.log();
            $(this).parents('form').submit();
        });
##
##        $('#size').on('change', function () {
##            $('#width').val($(this).val().split(';')[0]);
##            $('#height').val($(this).val().split(';')[1]);
##        });
##        $('#size').val(zoneSize).select2();
##
##        $('.input-spinner').each(function () {
##            var element = $(this), _prevValue;
##            var min = +element.attr('min') || 0,
##                    max = +element.attr('max') || Infinity,
##                    step = +element.attr('step') || 1,
##                    forceFloat = +element.attr('decimal') || false;
##
##            var increaseButton = $('<span class="input-group-btn"><button class="btn btn-default"><i class="fa fa-fw fa-plus"></i></button></span>')
##                            .on('click', function () {
##                                var value = +element.val();
##                                if (value + step <= max) {
##                                    if (forceFloat || isFloat(value + step))
##                                        element.val((value + step).toFixed(2));
##                                    else
##                                        element.val(value + step);
##                                }
##                            }),
##                    decreaseButton = $('<span class="input-group-btn"><button class="btn btn-default"><i class="fa fa-fw fa-minus"></i></button></span>')
##                            .on('click', function () {
##                                var value = +element.val();
##                                if (value - step >= min) {
##                                    if (forceFloat || isFloat(value - step))
##                                        element.val((value - step).toFixed(2));
##                                    else
##                                        element.val(value - step);
##                                }
##                            });
##            element
##                    .on('keydown', function () {
##                        var value = +element.val();
##                        if (!(isNaN(value) || value > max || value < min)) {
##                            _prevValue = value;
##                        }
##                    })
##                    .on('keyup', function () {
##                        var value = +element.val();
##                        if (isNaN(value) || value > max || value < min) {
##                            element.val(_prevValue);
##                        }
##                    });
##            if (!element.parent().hasClass('input-group')) {
##                element.wrap($('<div class="input-group"></div>'));
##            }
##            element.after(increaseButton);
##            element.before(decreaseButton);
##        });
    });
</script>