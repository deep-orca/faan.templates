<%
    from wtforms.widgets import HiddenInput, ListWidget
%>
<form class="form-horizontal col-md-12" action="" method="post" role="form">
    %for field in form:
        %if isinstance(field.widget, HiddenInput):
            %if field.name == 'closable':
                <div class="form-group">
                    ${field.label(class_="control-label col-md-4")}
                    <div class="col-md-4">
                        ${field(class_="form-control")}
                        <input id="closable-slide" type="text" class="form-control"/>
                        %if field.description:
                            <small class="help-block">
                                ${field.description}
                            </small>
                        %endif
                        %if field.errors:
                            <ul class="list-unstyled text-danger">
                                %for e in field.errors:
                                    <li>${e}</li>
                                %endfor
                            </ul>
                        %endif
                    </div>
                </div>
            %elif field.name == 'min_duration':
                <div class="form-group">
                    ${field.label(class_="control-label col-md-4")}
                    <div class="col-md-4">
                        ${field(class_="form-control")}
                        <input id="duration-slide" type="text" class="form-control"/>
                        %if field.description:
                            <small class="help-block">
                                ${field.description}
                            </small>
                        %endif
                        %if field.errors:
                            <ul class="list-unstyled text-danger">
                                %for e in field.errors:
                                    <li>${e}</li>
                                %endfor
                            </ul>
                        %endif
                    </div>
                </div>
            %else:
                ${field}
            %endif
        %elif isinstance(field.widget, ListWidget):
            <div class="form-group">
                ${field.label(class_="control-label col-md-4")}
                <div class="col-md-4">
                    ${field(class_="list-unstyled", style="margin-top: 6px;")}
                    %if field.errors:
                        <ul class="list-unstyled text-danger">
                            %for e in field.errors:
                                <li>${e}</li>
                            %endfor
                        </ul>
                    %endif
                </div>
            </div>
        %else:
            <div class="form-group">
                ${field.label(class_="control-label col-md-4")}
                <div class="col-md-4">
                    ${field(class_="form-control")}
                    %if field.description:
                        <small class="help-block">
                            ${field.description}
                        </small>
                    %endif
                    %if field.errors:
                        <ul class="list-unstyled text-danger">
                            %for e in field.errors:
                                <li>${e}</li>
                            %endfor
                        </ul>
                    %endif
                </div>
            </div>
        %endif
    %endfor
    <hr/>
    <div class="text-center">
        <button type="submit" class="btn btn-success btn-tall btn-wide">
            <i class="fa fa-fw fa-save"></i> Сохранить
        </button>
    </div>
</form>