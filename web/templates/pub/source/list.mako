## -*- coding: utf-8 -*-
<%namespace name="bcrumb" file="/layout/bcrumb.mako" />
<%namespace name="fabutton" file="/layout/fa-button.mako" />
<%inherit file="/layout/main.mako" />

<%def name="title()">Источники</%def>
<%def name="description()">Источники блока ${g.unit.name}</%def>

<%
    import datetime
    from faan.core.model.pub.source.source import Source
%>

${ bcrumb.h(self) }
${ bcrumb.bc([
    {
        'name' : u'Площадки',
        'href' : '/pub/application/',
        'icon' : 'rocket'
    },
    {
        'name' : u"%s<sup>[%s]</sup>" % (g.application.name, g.application.id),
        'href' : '/pub/application/%s/unit' % g.application.id
    },
    {
        'name' : u'%s<sup>[%s]</sup>' % (g.unit.name, g.unit.id),
        'href' : '/pub/application/%s/unit/%s/source' % (g.application.id, g.unit.id)
    },
    {
        'name' : u'Источники'
    },
]) }

<!-- BEGIN PAGE CONTENT-->
<div class="row">
    <div class="col-md-12">
        <div class="portlet">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-sitemap"></i>
                    Источники блока ${g.unit.name}<sup>[${g.unit.id}]</sup>
                </div>
                <div class="actions">
                    <div class="btn-group">
                        <a href="#" class="btn btn-success dropdown-toggle ${'disabled' if g.has_source_type(Source.Type.VIDIGER) and g.has_source_type(Source.Type.ADMOB) else ''}" data-toggle="dropdown">
                            <i class="fa fa-plus"></i>
                            Добавить новый
                            <i class="fa fa-angle-down"></i>
                        </a>
                        <ul class="dropdown-menu pull-right">
                            %if not g.has_source_type(Source.Type.VIDIGER):
                                <li>
                                    <a href="/pub/application/${g.application.id}/unit/${g.unit.id}/source/new?type=VIDIGER">
                                        Источник Vidiger
                                    </a>
                                </li>
                            %endif
                            %if not g.has_source_type(Source.Type.ADMOB):
                                <li>
                                    <a href="/pub/application/${g.application.id}/unit/${g.unit.id}/source/new?type=ADMOB">
                                        Источник AdMob
                                    </a>
                                </li>
                            %endif
                            %if g.has_source_type(Source.Type.VIDIGER) and g.has_source_type(Source.Type.ADMOB):
                                <li>

                                </li>
                            %endif
                        </ul>
                    </div>
                </div>
            </div>

            <div class="portlet-body">
                <table class="table table-striped table-bordered table-hover" id="source_table">
                    <thead>
                    <tr>
                        <th>Название</th>
                        <th class="text-center">Тип источника</th>
                        <th class="text-center">Статус</th>
                    </tr>
                    </thead>
                    <tbody>
                        % for s in g.sources:
                            <tr>
                                <td>
                                    <h4 style="margin-top:0;">
                                        <span class="badge badge-default">#${s.id}</span>
                                        <a class="title" href="${s.id}">
                                            ${s.name}
                                        </a>
                                    </h4>

                                    <p class="text-left">
                                        <%
                                            today = datetime.date.today()
                                            monthago = today - datetime.timedelta(days=30)
                                        %>
                                        ${ fabutton.fab([{ 'text' : u'Активировать',  'href' : '/pub/application/%d/unit/%d/source/%d/activate'%(g.application.id, g.unit.id, s.id), 'icon' : 'play' }]) if s.state == Source.State.DISABLED else fabutton.fab([{ 'text' : u'Остановить',  'href' : '/pub/application/%d/unit/%d/source/%d/deactivate'%(g.application.id, g.unit.id, s.id), 'icon' : 'pause' }])}
                                        ${fabutton.fab([
                                            { 'text' : u'Редактировать',  'href' : s.id, 'icon' : 'pencil' },
                                            { 'text' : u'Статистика',  'href' : '/pub/stat/date#?from=%s&to=%s&application=%d&unit=%d&source=%s'%(unicode(monthago.strftime('%d.%m.%Y')), unicode(today.strftime('%d.%m.%Y')), g.application.id, g.unit.id, s.id), 'icon' : 'bar-chart-o' },
                                            {
                                                'text': u'Удалить',
                                                'href': '#delete',
                                                'icon': 'times',
                                                'class': 'actionSourceDelete'
                                            }
                                        ])}
                                    </p>

                                </td>
                                <td class="text-center">
                                    %if s.type == Source.Type.VIDIGER:
                                        Vidiger
                                    %elif s.type == Source.Type.ADFOX:
                                        AdFox
                                    %elif s.type == Source.Type.ADMOB:
                                        AdMob
                                    %elif s.type == Source.Type.BACKFILL:
                                        Backfill
                                    %else:
                                        Custom
                                    %endif
                                </td>
                                <td class="text-center">
                                    %if s.state == Source.State.ACTIVE:
                                        <span class="badge badge-success">
                                            Активен
                                        </span>
                                    %elif s.state == Source.State.DISABLED:
                                        <span class="badge badge-warning">
                                            Неактивен
                                        </span>
                                    %else:
                                        <span class="badge badge-danger">
                                            Выключен
                                        </span>
                                    %endif
                                </td>
                            </tr>
                        % endfor
                    </tbody>
                </table>
            </div>
        </div>

    </div>
</div>

<div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog" style="width: 25%">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel">Удалить источники?</h4>
            </div>
            <div class="modal-body">
                <div class="pasta form-group">
                    Действительно удалить источник <span id="del_list"></span> ?
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
                <button type="button" id="delete" class="btn btn-danger group_actions">Удалить</button>
            </div>
        </div>
    </div>
</div>
<!-- END PAGE CONTENT-->

##<script src="/global/static/scripts/zones_table.js"></script>
<script type="text/javascript">
    $(document).on('ready', function() {
        var deleteModal = $('#deleteModal');
        $('.actionSourceDelete').on('click', function () {
            var sourceID = $(this).parents('tr').find('a.title').attr('href');
            var sourceName = $(this).parents('tr').find('a.title').text();
            $('#delete').one('click', function () {

                window.location = window.location.pathname + sourceID + '/delete';
            });
            if (sourceID) {
                deleteModal.modal();
                $('#del_list').html(sourceName + '<sup>[' + sourceID + ']</sup>');
            }
        });

        $('#source_table').dataTable({
            "aoColumns": [
                { "bSearchable": true },
                { "bSortable": false, "bSearchable": false },
                { "bSortable": false, "bSearchable": false }
            ],
            "aLengthMenu": [
                [10, 20, 50, -1],
                [10, 20, 50, "Все"]  // change per page values here
            ],
            "iDisplayLength": 10,
            "sPaginationType": "bootstrap",
            "oLanguage": {
                "sLengthMenu": "_MENU_ источников",
                "oPaginate": {
                    "sPrevious": "Назад",
                    "sNext": "Вперед"
                },
                "sSearch": "Поиск: ",
                "sInfo": "Показаны с _START_ по _END_ из _TOTAL_ источников",
                "sInfoempty": "У Вас пока не добавлено ни одного источника"
            },
            "aoColumnDefs": [
                {
                    'bSortable': false,
                    'aTargets': [0]
                }
            ]
        });

        $('.group-checkable').on('change', function () {
            var set = $(this).attr("data-set");
            var checked = $(this).is(":checked");
            $(set).each(function () {
                if (checked) {
                    $(this).attr("checked", true);
                } else {
                    $(this).attr("checked", false);
                }
            });
            $.uniform.update(set);
        });

        $('.dataTables_filter input').addClass("form-control input-small");
        $('.dataTables_length select').addClass("form-control input-xsmall").select2();
    });
</script>
