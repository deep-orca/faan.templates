## -*- coding: utf-8 -*-
<%namespace name="bcrumb" file="/layout/bcrumb.mako" />
<%namespace name="fabutton" file="/layout/fa-button.mako" />
<%inherit file="/layout/main.mako" />
<%def name="title()">Площадки</%def>
<%def name="description()">Список всех добавленных площадок</%def>
<%def name="page_css()">

<%! from faan.core.model.pub.application import Application %>

<style>
.osfilter>.btn:hover, .osfilter-vertical>.btn:hover, .osfilter>.btn:focus, .osfilter-vertical>.btn:focus, .osfilter>.btn:active, .osfilter-vertical>.btn:active, .osfilter>.btn.active, .osfilter-vertical>.btn.active{
    border-color: #00A800;
}
</style>
</%def>
<% import datetime %>

${ bcrumb.h(self) }
${ bcrumb.bc([ { 'name' : u'Площадки', 'href' : '/pub/application/', 'icon' : 'rocket' }
               ]) }

<div class="row">                                                               <!-- BEGIN PAGE CONTENT-->
<div class="col-md-12">                 
    <div class="portlet">                                                       <!--BEGIN TABLE-->
        <div class="portlet-title">
            <div class="caption">
                <i class="fa fa-rocket"></i>Ваши площадки
            </div>
            <div class="actions">
                <a href="/pub/application/new" class="btn btn-success"><i class="fa fa-plus"></i> Добавить новую</a>
            </div>
        </div>
        
        <div class="portlet-body">
            <table class="table table-striped table-bordered table-hover" id="applications_table">
            <thead>
            <tr>
                <th style="width:15px;"><input type="checkbox" class="group-checkable" data-set="#applications_table .checkboxes"/></th>
                <th>ID</th>
                <th>Платформа</th>
                <th></th>
                <th>Название</th>
                <th class="text-center">Статус</th>
                <th class="text-center">ОС</th>
                <th class="text-center">Зоны</th>
##                <th class="text-right">Показы</th>
##                <th class="text-right">Завершенные</th>
##                <th class="text-right">Рейтинг просмотров</th>
##                <th class="text-right">V4VC расход</th>
##                <th class="text-right">Доход</th>
                
            </tr>
            </thead>
            <tbody>
            % for a in g.applications:
            <tr class="odd gradeX">                                                             <!-- TODO: add actual columns -->
                <td><input type="checkbox" class="checkboxes" value="${a.id}"/></td>
                <td>${a.id}</td>
                <td>
                    %if a.os == 1:
                        ios
                    %elif a.os == 2:
                        android
                    %elif a.os == 3:
                        windows
                    %else:
                        web
                    %endif

                </td>
                <td>
                    <img height="48" src="${a.icon_url}" alt=""
                         style="border:0;border-radius: 6px"/>
                </td>
                <td>
                    <h4 style="margin-top:0;">
                        <span class="badge badge-primary">#${a.id}</span>

                        <a href="/pub/application/${a.id}/unit/">${h.limitstr(a.name, 15)}</a>
                    </h4>

                    <p class="text-muted text-left">
                        <small style="width:100px;">
                            <i class="fa fa-tags"></i>
                            ${",<br>".join([unicode(g.categories_dict.get(cid)) for cid in a.category_groups])}
                        </small>
                    </p>
                    <p class="text-left">
                        <%
                            today = datetime.date.today()
                            monthago = today - datetime.timedelta(days=30)
                        %>
                        %if a.state != 0:
                            ${fabutton.fab([{ 'text' : u'Активировать',  'href' : '/pub/application/%d/state/1'%(a.id), 'icon' : 'play' }]) if a.state == 2 else fabutton.fab([{ 'text' : u'Остановить',  'href' : '/pub/application/%d/state/2'%(a.id), 'icon' : 'pause' }])}
                        %endif

                        ${ fabutton.fab([
                            { 'text' : u'Добавить блок',  'href' : '/pub/application/%d/unit/new'%(a.id), 'icon' : 'plus' },
                            { 'text' : u'Редактировать',  'href' : '/pub/application/%d'%(a.id), 'icon' : 'pencil' },
                            { 'text' : u'Статистика',  'href' : '/pub/stat/date#?from=%s&to=%s&application=%d'%(unicode(monthago.strftime('%d.%m.%Y')), unicode(today.strftime('%d.%m.%Y')), a.id), 'icon' : 'bar-chart-o' },
                            {
                                'text' : u'Удалить',
                                'href' : '#delete',
                                'icon' : 'times',
                                'data': {
                                    'aid': '%d' % a.id,
                                },
                                'class': 'delete_app'
                            }
                        ])}
                    </p>
##                    <div class="row">
##                        <div class="col-md-8">
##
##                        </div>
##
##                    </div>
                </td>
                <td class="text-center">
                    <span class="hidden">${a.state}</span>
                    %if a.state == Application.State.ACTIVE:
                        <span class="badge badge-success">Активный</span>
                    %elif a.state == Application.State.DISABLED:
                        <span class="badge badge-warning">Остановленный</span>
                    %elif a.state == Application.State.NONE:
                        <span class="badge badge-info">Новый</span>
                    %else:
                        <span class="badge badge-danger">Заблокированный</span>
                    %endif
                </td>
                <td class="text-center" style="color: #0d638f;padding-top: 6px;">
                    %if a.os == 1:
                        <i class="fa fa-apple fa-fw fa-2x"></i>
                    %elif a.os == 2:
                        <i class="fa fa-android fa-fw fa-2x"></i>
                    %elif a.os == 3:
                        <i class="fa fa-windows fa-fw fa-2x"></i>
                    %elif a.os == 4:
                        <i class="fa fa-globe fa-fw fa-2x"></i>
                    %endif
                </td>
                <td class="text-center">
                    <span class="badge badge-success tooltips" data-placement="top" data-original-title="Активные">${a.zones.get("active", 0)}</span>
                    <span class="badge badge-info tooltips" data-placement="top" data-original-title="Остановленные">${a.zones.get("inactive", 0)}</span>
                </td>
##                <td class="text-right">${a.raw}</td>
##                <td class="text-right">${a.impressions}</td>
##                <td class="text-right">
##                    ${"%.2f" %(100 * h.divbyz(a.impressions, a.raw))}
##                </td>
##                <td class="text-right">${a.v4vc_cost}</td>
##                <td class="text-right">${a.revenue}</td>
            </tr>
            % endfor
            </tbody>
            </table>
        </div>
    </div>              <!--END TABLE-->
 </div>
</div>

## Delete confirmation modal

<div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog" style="width: 25%">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="myModalLabel">Удалить площадки?</h4>
      </div>
      <div class="modal-body">
        <div class="pasta form-group">
            Действительно удалить площадки <span id="del_list"></span> ?

        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
        <button type="button" id="delete" class="btn btn-danger group_actions">Удалить</button>
      </div>
    </div>
  </div>
</div>


<!-- END PAGE CONTENT-->
<script type="text/javascript" src="/global/static/scripts/apps_table.js"></script>
<script type="text/javascript">
    var deleteModal = $('#deleteModal');
    $('.delete_app').on('click', function() {
        var aid = $(this).data('aid');
        $('#delete').one('click', function() {
            window.location = '/pub/application/' + aid + '/delete';
        });
        if (aid) {
            deleteModal.modal();
            $('#del_list').html('').html(aid);
        }
        console.debug('# Deleting application', $(this).data('aid'));
    });
TableManaged.init();
</script>
