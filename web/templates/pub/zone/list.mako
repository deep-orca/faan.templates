## -*- coding: utf-8 -*-
<%namespace name="bcrumb" file="/layout/bcrumb.mako" />
<%namespace name="fabutton" file="/layout/fa-button.mako" />
<%inherit file="/layout/main.mako" />
<%def name="title()">Зоны</%def>
<%def name="description()">Зоны площадки [${g.application.id}] ${g.application.name}</%def>
<% import datetime %>

${ bcrumb.h(self) }
${ bcrumb.bc([ { 'name' : u'Площадки',  'href' : '/pub/application/', 'icon' : 'rocket' }
             , { 'name' : u"[%s] %s" % (g.application.id, g.application.name), 'href' : '/pub/application/%s' % (g.application.id) }
             , { 'name' : u'Зоны',      'href' : '/pub/application/%s/zone' % (g.application.id) }
             ]) }

<!-- BEGIN PAGE CONTENT-->
<div class="row">
    <div class="col-md-12">

    <div class="portlet">
        <div class="portlet-title">
            <div class="caption"><i class="fa fa-sitemap"></i>${self.description()}</div>
            <div class="actions">
                <a href="/pub/application/${g.application.id}/zone/new" class="btn btn-success"><i class="fa fa-plus"></i> Добавить новую</a>
            </div>
        </div>
        
        <div class="portlet-body">
            <table class="table table-striped table-bordered table-hover" id="zones_table">
            <thead>
            <tr>
                <th style="width:15px;"><input type="checkbox" class="group-checkable" data-set="#zones_table .checkboxes"/></th>
                <th>Название</th>
                <th class="text-center">Тип зоны</th>
                <th class="text-center">Тип устройства</th>
                <th class="text-center">Статус</th>
##                <th class="text-center">Показы</th>
##                <th class="text-center">Доход</th>
            </tr>
            </thead>
            <tbody>
            % for z in g.zones:
            <tr class="odd gradeX">                                                             <!-- TODO: add actual columns -->
                <td><input type="checkbox" class="checkboxes" value="${z.id}"/></td>
                <td>
                    <div class="row">
                        <div class="col-md-8">
                            
                            <h4 style="margin-top:0;">
                                <span class="badge badge-default">#${z.id}</span>
                                <a href="/pub/application/${g.application.id}/zone/${z.id}">${z.name}</a>
                            </h4>
                            <p class="text-left">
                                <%
                                today = datetime.date.today()
                                monthago = datetime.date(day=today.day, month=today.month - 1, year=today.year)
                                %>
                                ${ fabutton.fab([{ 'text' : u'Активировать',  'href' : '/pub/application/%d/zone/%d/state/1'%(g.application.id, z.id), 'icon' : 'play' }]) if z.state == 2 else fabutton.fab([{ 'text' : u'Остановить',  'href' : '/pub/application/%d/zone/%d/state/2'%(g.application.id, z.id), 'icon' : 'pause' }])
                                }
                                ${ fabutton.fab([ 
                                    { 'text' : u'Редактировать',  'href' : '/pub/application/%d/zone/%d'%(g.application.id, z.id), 'icon' : 'pencil' },
                                    { 'text' : u'Статистика',  'href' : '/pub/stat/date?date=%s+-+%s&application=%d&zone=%d'%(unicode(monthago.strftime('%d.%m.%Y')), unicode(today.strftime('%d.%m.%Y')), g.application.id, z.id), 'icon' : 'bar-chart-o' },
                                    {
                                        'text': u'Удалить',
                                        'href': '#delete', 
                                        'icon': 'times',
                                        'data': {
                                            'aid': '%d' % g.application.id,
                                            'zid': '%d' % z.id
                                        },
                                        'class': 'delete_zone'
                                    }
                                ]) 
                                }
                            </p>
                        </div>
                        %if z.type == z.Type.INTERSTITIAL and z.show_video == 1:
                            <div class="col-md-4 text-right">
                                <p class="text-muted">
                                    <small><i class="fa fa-clock-o"></i> ${z.min_duration} - ${z.max_duration} сек.</small>
                                </p>
                                <p class="text-muted">
                                    <small><i class="fa fa-times-circle"></i> ${z.closable} сек.</small>
                                </p>
                            </div>
                        %endif
                    </div>
                </td>

                <td>
                    %if z.type == z.Type.BANNER:
                        Баннер
                    %endif
                    %if z.type == z.Type.INTERSTITIAL:
                        Полноэкранная
                    %endif
                </td>
                <td class="text-center">
                    %if z.device_type == z.DeviceType.PHONE:
                        <i class="fa fa-fw fa-2x fa-mobile-phone"></i>
                    %endif
                    %if z.device_type == z.DeviceType.TABLET:
                        <i class="fa fa-fw fa-2x fa-tablet"></i>
                    %endif
                </td>

                <td class="text-center">
                    ${fabutton.fas([{'status': z.state, 'type': 'zone'}])}
                </td>
##                <td class="text-right">${z.raw}</td>
##                <td class="text-right">${z.revenue}</td>
            </tr>
            % endfor
            </tbody>
            </table>
        </div>
    </div>
 
</div>
</div>

<div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog" style="width: 25%">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="myModalLabel">Удалить зоны?</h4>
      </div>
      <div class="modal-body">
        <div class="pasta form-group">
            Действительно удалить зоны <span id="del_list"></span> ?

        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
        <button type="button" id="delete" class="btn btn-danger group_actions">Удалить</button>
      </div>
    </div>
  </div>
</div>
<!-- END PAGE CONTENT-->

<script src="/global/static/scripts/zones_table.js"></script>
<script type="text/javascript">
    var deleteModal = $('#deleteModal');
    $('.delete_zone').on('click', function() {
        var aid = $(this).data('aid'),
            zid = $(this).data('zid');
        $('#delete').one('click', function() {
            window.location = '/pub/application/' + aid + '/zone/' + zid + '/delete';
        });
        if (zid) {
            deleteModal.modal();
            $('#del_list').html('').html(zid);
        }
    });
TableManaged.init();
</script>
