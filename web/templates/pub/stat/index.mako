## -*- coding: utf-8 -*-
<%namespace name="bcrumb" file="/layout/bcrumb.mako" />
<%inherit file="/layout/main.mako" />

<%def name="title()">Вебмастер</%def>
<%def name="description()">Статистика</%def>
<%def name="page_css()">
    <link rel="stylesheet" type="text/css"
          href="/global/static/plugins/bootstrap-daterangepicker/daterangepicker-bs3.css"/>
    <style>
        th {
            vertical-align: middle !important;
            text-align: center;
        }

        #defaultrange input {
            cursor: pointer !important;
        }

        tfoot th {
            text-align: left !important;
        }

        .ng-table-pager {
            overflow: hidden;
        }

        .pagination {
            margin: 0;
        }
    </style>
</%def>

${ bcrumb.h(self) }

<div class="row">
    <div class="col-md-12">
        <div class="portlet">
            <div class="portlet-title">
                <div class="caption">
                    % if   g.group == 'date':
                        <i class="fa fa-calendar"></i> По дате
                    % elif g.group == 'campaign':
                        <i class="fa fa-bullhorn"></i> По кампаниям
                    % elif g.group == 'media':
                        <i class="fa fa-video-camera"></i> По медиа
                    % endif
                </div>
            </div>

            <div class="portlet-body" ng-controller="TableController as table">
                <div ng-include="'/static/angular-templates/pub/table-filter.html'"></div>

                <table ng-table="table.config" class="table table-striped table-bordered table-hover"
                       id="campaings_table">
                    <thead>
                        <tr role="row">
                            <th ng-if="table.control.isActive(['date'])"
                                ng-click="table.config.sorting({'ts_spawn' : table.config.isSortBy('ts_spawn', 'asc') ? 'desc' : 'asc'})">
                                Дата
                                <i class="fa fa-unsorted"
                                   ng-class="{'fa-sort-asc': table.config.isSortBy('ts_spawn', 'asc'),'fa-sort-desc': table.config.isSortBy('ts_spawn', 'desc')}"></i>
                            </th>
                            <th ng-if="table.control.isActive(['application'])"
                                ng-click="table.config.sorting({'application' : table.config.isSortBy('application', 'asc') ? 'desc' : 'asc'})">
                                Площадка
                                <i class="fa fa-unsorted"
                                   ng-class="{'fa-sort-asc': table.config.isSortBy('application', 'asc'),'fa-sort-desc': table.config.isSortBy('application', 'desc')}"></i>
                            </th>
                            <th ng-if="table.control.isActive(['unit'])"
                                ng-click="table.config.sorting({'unit' : table.config.isSortBy('unit', 'asc') ? 'desc' : 'asc'})">
                                Блок
                                <i class="fa fa-unsorted"
                                   ng-class="{'fa-sort-asc': table.config.isSortBy('unit', 'asc'),'fa-sort-desc': table.config.isSortBy('unit', 'desc')}"></i>
                            </th>
                            <th ng-if="table.control.isActive(['source'])"
                                ng-click="table.config.sorting({'source' : table.config.isSortBy('source', 'asc') ? 'desc' : 'asc'})">
                                Источник
                                <i class="fa fa-unsorted"
                                   ng-class="{'fa-sort-asc': table.config.isSortBy('source', 'asc'),'fa-sort-desc': table.config.isSortBy('source', 'desc')}"></i>
                            </th>
                            <th ng-if="table.control.isActive(['country'])"
                                ng-click="table.config.sorting({'country' : table.config.isSortBy('country', 'asc') ? 'desc' : 'asc'})">
                                Страна
                                <i class="fa fa-unsorted"
                                   ng-class="{'fa-sort-asc': table.config.isSortBy('country', 'asc'),'fa-sort-desc': table.config.isSortBy('country', 'desc')}"></i>
                            </th>
                            <th ng-if="table.control.isActive(['region'])"
                                ng-click="table.config.sorting({'region' : table.config.isSortBy('region', 'asc') ? 'desc' : 'asc'})">
                                Регион
                                <i class="fa fa-unsorted"
                                   ng-class="{'fa-sort-asc': table.config.isSortBy('region', 'asc'),'fa-sort-desc': table.config.isSortBy('region', 'desc')}"></i>
                            </th>
                            <th ng-if="table.control.isActive(['city'])"
                                ng-click="table.config.sorting({'city' : table.config.isSortBy('city', 'asc') ? 'desc' : 'asc'})">
                                Город
                                <i class="fa fa-unsorted"
                                   ng-class="{'fa-sort-asc': table.config.isSortBy('city', 'asc'),'fa-sort-desc': table.config.isSortBy('city', 'desc')}"></i>
                            </th>
                            <th ng-click="table.config.sorting({'impressions' : table.config.isSortBy('impressions', 'asc') ? 'desc' : 'asc'})">
                                Показы
                                <i class="fa fa-unsorted"
                                   ng-class="{'fa-sort-asc': table.config.isSortBy('impressions', 'asc'),'fa-sort-desc': table.config.isSortBy('impressions', 'desc')}"></i>
                            </th>
                            <th ng-click="table.config.sorting({'ctr' : table.config.isSortBy('ctr', 'asc') ? 'desc' : 'asc'})">
                                CPM
                                <i class="fa fa-unsorted"
                                   ng-class="{'fa-sort-asc': table.config.isSortBy('ctr', 'asc'),'fa-sort-desc': table.config.isSortBy('ctr', 'desc')}"></i>
                            </th>
                            <th ng-click="table.config.sorting({'cost' : table.config.isSortBy('cost', 'asc') ? 'desc' : 'asc'})">
                                Деньги
                                <i class="fa fa-unsorted"
                                   ng-class="{'fa-sort-asc': table.config.isSortBy('cost', 'asc'),'fa-sort-desc': table.config.isSortBy('cost', 'desc')}"></i>
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr ng-if="$data.length == 0">
                            <td colspan="12" class="text-center">Нет данных за выбранный период</td>
                        </tr>
                        <tr ng-repeat="row in $data">
                            <td ng-if="table.control.isActive(['date'])">
                                <a ng-if="!table.options.hourly"
                                   ng-click="table.columnDate({from: (row.ts_spawn | x_date:null:false), to: (row.ts_spawn | x_date:null:false)})"
                                   style="cursor:pointer;">
                                    {{row.ts_spawn | x_date:table.filterData.timezone:table.options.hourly}}
                                </a>
                                <span ng-if="table.options.hourly">
                                    {{row.ts_spawn | x_date:table.filterData.timezone:table.options.hourly}}
                                </span>
                            </td>
                            <td ng-if="table.control.isActive(['application'])">
                                <a ng-href="{{table.columnApplication({application: row.application, from: (table.options.data.startDate.format('DD.MM.YYYY')), to: (table.options.data.endDate.format('DD.MM.YYYY'))})}}">
                                    {{row.application_name}}
                                </a>
                            </td>
                            <td ng-if="table.control.isActive(['unit'])">
                                <a ng-href="{{table.columnUnit({unit: row.unit, from: (table.options.data.startDate.format('DD.MM.YYYY')), to: (table.options.data.endDate.format('DD.MM.YYYY'))})}}">
                                    {{row.unit_name}}
                                </a>
                            </td>
                            <td ng-if="table.control.isActive(['source'])">
                                {{row.source_name}}
                            </td>
                            <td ng-if="table.control.isActive(['country'])">
                                <a ng-href="{{table.columnCountry({country: row.country, from: (table.options.data.startDate.format('DD.MM.YYYY')), to: (table.options.data.endDate.format('DD.MM.YYYY'))})}}">
                                    {{row.country_name}}
                                </a>
                            </td>
                            <td ng-if="table.control.isActive(['region'])">
                                <a ng-href="{{table.columnRegion({region: row.region, from: (table.options.data.startDate.format('DD.MM.YYYY')), to: (table.options.data.endDate.format('DD.MM.YYYY'))})}}">
                                    {{row.region_name}}
                                </a>
                            </td>
                            <td ng-if="table.control.isActive(['city'])">{{row.city_name}}</td>
                            <td>{{row.impressions | x_number:0 }}</td>
                            <td>{{row.cpm | x_number:2}} руб</td>
                            <td>{{row.cost | x_number }} руб</td>
                        </tr>
                    </tbody>
                    <tfoot ng-if="$data.length > 0">
                        <tr>
                            <th class="text-left">Всего</th>
                            <th class="text-left">{{table.summary.impressions | x_number:0 }}</th>
                            <th class="text-left">{{table.summary.cpm | x_number:2 }} руб</th>
                            <th class="text-left">{{table.summary.cost | x_number:2 }} руб</th>
                        </tr>
                    </tfoot>
                </table>
            </div>
        </div>
    </div>
</div>