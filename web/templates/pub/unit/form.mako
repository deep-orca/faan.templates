## -*- coding: utf-8 -*-
<%namespace name="bcrumb" file="/layout/bcrumb.mako" />
<%inherit file="/layout/main.mako" />

<%
    from faan.core.model.pub.unit import Unit
    from faan.core.model.pub.application import Application
%>

<%def name="title()">
    % if g.unit.id:
        Блок ${g.unit.name}
    % else:
        Создание блока
    %endif
</%def>
<%def name="description()">Настройки блока</%def>

${ bcrumb.h(self) }
${ bcrumb.bc([ { 'name' : u'Приложения', 'href' : '/pub/application/', 'icon' : 'rocket' }
             , { 'name' : u"%s" % (g.app.name), 'href' : '/pub/application/%s/unit'% (g.app.id)}
             , { 'name' : u"Блок %s<sup>[%s]</sup>" % (g.unit.name, g.unit.id) if g.unit.id else u'Новый блок' }
             ]) }

<%def name="page_css()">
    <link href="/global/static/plugins/ion.rangeslider/css/ion.rangeSlider.css" rel="stylesheet" type="text/css"/>
    <link href="/global/static/plugins/ion.rangeslider/css/ion.rangeSlider.Conquer.css" rel="stylesheet" type="text/css"/>
    <style>
        .xform-error {
            padding: 3px;
            font-weight: bold !important;
        }

        .btn-tall {
            height: 60px;
        }

        .btn-wide {
            width: 180px;
        }

        .zone-preview {
            position: absolute;
            top: 10px;
            right: 10px;
        }
        .select2-result-unselectable,
        .select2-disabled {
            background: #FFFFFF;
            color: #AAAAAA;
        }
    </style>
</%def>

<%def name="page_js()">
    <script src="/global/static/plugins/ion.rangeslider/js/ion-rangeSlider/ion.rangeSlider.min.js"></script>
</%def>


<!-- BEGIN PAGE CONTENT-->
<div class="row">
    <div class="col-md-12">
        <div class="portlet">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-sitemap"></i>
                    %if not g.unit.id:
                        Добавление блока
                    %else:
                        Блок ${g.unit.name}<sup>[${g.unit.id}]</sup>
                    %endif
                </div>
            </div>
            <div class="portlet-body" style="position: relative;">
                <div class="row">
##                    <div class="zone-preview hidden-xs hidden-sm col-md-4">
##                        <img class="zone-preview-img zone-preview-11" height="100" src="/static/img/zone/00.png" alt=""/>
##                        <img class="zone-preview-img zone-preview-12" height="100" src="/static/img/zone/01.png" alt=""/>
##                        <img class="zone-preview-img zone-preview-21" height="100" src="/static/img/zone/10.png" alt=""/>
##                        <img class="zone-preview-img zone-preview-22" height="100" src="/static/img/zone/11.png" alt=""/>
##                    </div>
##                    <pre>${form.errors}</pre>
                    <form id="zone-form" class="form-horizontal col-md-6 col-md-offset-2" method="post" role="form">
                        ${form.csrf_token}
                        <div class="form-group ${'hidden' if g.unit.id else ''}">
                            <label class="control-label col-md-4">Тип блока</label>
                            <div class="col-md-8">
                                <div class="btn-group btn-group-justified">
                                    <a class="btn btn-default type ${'active' if form.type.data == Unit.Type.BANNER else ''}" data-type="${Unit.Type.BANNER}">
                                        Баннер
                                    </a>
                                    <a class="btn btn-default type ${'active' if form.type.data == Unit.Type.INTERSTITIAL else ''}" data-type="${Unit.Type.INTERSTITIAL}">
                                        Полноэкранный
                                    </a>
                                </div>
                            </div>
                            ${form.type()}
                        </div>

                        <div class="form-group ${'hidden' if g.unit.id else ''}">
                            <label class="control-label col-md-4">Тип устройства</label>
                            <div class="col-md-8">
                                <div class="btn-group btn-group-justified">
                                    <a class="btn btn-default device-type ${'active' if form.device_type.data == Unit.DeviceType.PHONE else ''}" data-device-type="${Unit.DeviceType.PHONE}">
                                        Телефон
                                    </a>
                                    <a class="btn btn-default device-type ${'active' if form.device_type.data == Unit.DeviceType.TABLET else ''}" data-device-type="${Unit.DeviceType.TABLET}">
                                        Планшет
                                    </a>
                                </div>
                            </div>
                            ${form.device_type()}
                        </div>

                        %if g.unit.id:
                            <div class="form-group">
                                <label class="control-label col-md-4"><b>ID блока</b></label>
                                <div class="col-md-8">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <label class="control-label"> ${g.unit.id}-${g.unit.uuid}</label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            %if g.app.os == Application.Os.MOBILE_WEB:
                                <div class="form-group">
                                    <label class="control-label col-md-4"><b>Код вставки</b></label>
                                    <div class="col-md-8">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <pre>&lt;script src="http://api.vidiger.com/static/q.js?unit=${g.unit.id}-${g.unit.uuid}"&gt;&lt;/script&gt;</pre>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            %endif
                        %endif

                        <div class="form-group ${'has-error' if form.name.errors else ''}">
                            ${form.name.label(class_="col-md-4 control-label")}
                            <div class="col-md-8">
                                <div class="row">
                                    <div class="col-md-12">
                                        ${form.name(class_="form-control")}
                                    </div>
                                </div>
                                <div class="row">
                                    <span class="text-danger col-md-12">
                                        ${", ".join(form.name.errors)}
                                    </span>
                                </div>
                            </div>
                        </div>

                        <div class="form-group ${'has-error' if form.mod_factor.errors else ''}">
                            ${form.mod_factor.label(class_="col-md-4 control-label")}
                            <div class="col-md-8">
                                <div class="row">
                                    <div class="col-md-7">
                                        ${form.mod_factor(class_="form-control input-spinner")}
                                    </div>
                                </div>
                                <div class="row">
                                    <span class="text-danger col-md-12">
                                        ${", ".join(form.mod_factor.errors)}
                                    </span>
                                </div>
                                <div class="row">
                                    <div class="help-block col-md-12">
                                        ${form.mod_factor.description}
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="form-group ${'has-error' if form.session_limit.errors else ''}">
                            ${form.session_limit.label(class_="col-md-4 control-label")}
                            <div class="col-md-8">
                                <div class="row">
                                    <div class="col-md-7">
                                        ${form.session_limit(class_="form-control input-spinner")}
                                    </div>
                                </div>
                                <div class="row">
                                    <span class="col-md-12 text-danger">
                                        ${", ".join(form.session_limit.errors)}
                                    </span>
                                </div>
                                <div class="row">
                                    <span class="help-block col-md-12">
                                        ${form.session_limit.description}
                                    </span>
                                </div>
                            </div>
                        </div>

                        <div class="form-group ${'has-error' if form.daily_limit.errors else ''}">
                            ${form.daily_limit.label(class_="col-md-4 control-label")}
                            <div class="col-md-8">
                                <div class="row">
                                    <div class="col-md-7">
                                        ${form.daily_limit(class_="form-control input-spinner")}
                                    </div>
                                </div>
                                <div class="row">
                                    <span class="col-md-12 text-danger">
                                        ${", ".join(form.daily_limit.errors)}
                                    </span>
                                </div>
                                <div class="row">
                                    <span class="help-block col-md-12">
                                        ${form.daily_limit.description}
                                    </span>
                                </div>
                            </div>

                        </div>

                        <div class="form-group ${'has-error' if form.refresh_rate.errors else ''}" data-if-type="${Unit.Type.BANNER}">
                            ${form.refresh_rate.label(class_="col-md-4 control-label")}
                            <div class="col-md-8">
                                <div class="row">
                                    <div class="col-md-7">
                                        ${form.refresh_rate(class_="form-control input-spinner")}
                                    </div>
                                </div>
                                <div class="row">
                                    <span class="text-danger">
                                        ${", ".join(form.refresh_rate.errors)}
                                    </span>
                                </div>
                                <div class="row">
                                    <span class="help-block col-md-12">
                                        ${form.refresh_rate.description}
                                    </span>
                                </div>
                            </div>
                        </div>
                        <div class="form-group ${'has-error' if form.width.errors or form.height.errors else ''}" data-if-type="${Unit.Type.BANNER}">
                            <label class="col-md-4 control-label" for="size">Размер</label>
                            <div class="col-md-8">
                                <div class="row">
                                    <div class="col-md-7">
                                        <select class="form-control" name="size" id="size">
                                            %for size in form.SIZES:
                                                <option value="${size[0].width};${size[0].height}">${size[1]}</option>
                                            %endfor
                                        </select>

                                    </div>
                                </div>
                                <div class="row">
                                    <span class="text-danger col-md-12">
                                        ${", ".join(form.width.errors + form.height.errors)}
                                    </span>
                                </div>
                            </div>
                        </div>
                        <div class="form-group custom-size hidden ${'has-error' if form.width.errors or form.height.errors else ''}" data-if-type="${Unit.Type.BANNER}">
                            ${form.width.label(class_="col-md-4 control-label")}
                            <div class="col-md-4">
                                ${form.width(class_="form-control")}
                            </div>
                        </div>

                        <div class="form-group custom-size hidden ${'has-error' if form.width.errors or form.height.errors else ''}" data-if-type="${Unit.Type.BANNER}">
                            ${form.height.label(class_="col-md-4 control-label")}
                            <div class="col-md-4">
                                ${form.height(class_="form-control")}
                            </div>
                        </div>

##                        <div class="form-group ${'has-error' if form.v4vc_cpv.errors else ''}" data-if-type="${Unit.Type.INTERSTITIAL}">
##                            ${form.v4vc_cpv.label(class_="col-md-4 control-label")}
##                            <div class="col-md-8">
##                                <div class="row">
##                                    <div class="col-md-7">
##                                        ${form.v4vc_cpv(class_="form-control input-spinner")}
##                                    </div>
##                                </div>
##                                <div class="row">
##                                    <span class="text-danger col-md-12">
##                                        ${", ".join(form.v4vc_cpv.errors)}
##                                    </span>
##                                </div>
##
##                            </div>
##                        </div>

##                        <div class="form-group ${'has-error' if form.show_video.errors else ''}" data-if-type="${Unit.Type.INTERSTITIAL}">
##                            ${form.show_video.label(class_="col-md-4 control-label")}
##                            <div class="col-md-8">
##                                <div class="row">
##                                    <div class="col-md-2" style="padding-top: 8px;">
##                                        ${form.show_video(class_="form-control")}
##                                    </div>
##                                </div>
##                                <div class="row">
##                                    <span class="text-danger col-md-12">
##                                        ${", ".join(form.show_video.errors)}
##                                    </span>
##                                </div>
##
##                            </div>
##                        </div>
##
##                        <div class="form-group ${'has-error' if form.min_duration.errors or form.max_duration.errors else ''}"
##                             data-if-show-video>
##                            <label class="control-label col-md-4">Показывать ролики длиной</label>
##                            <div class="col-md-8">
##                                <div class="row">
##                                    <div class="col-md-12">
##                                        ${form.min_duration()}
##                                        ${form.max_duration()}
##                                        <input id="duration-slide" type="text" class="form-control"/>
##                                    </div>
##                                </div>
##                                <div class="row">
##                                    <span class="text-danger col-md-12">
##                                        ${", ".join(form.min_duration.errors + form.max_duration.errors)}
##                                    </span>
##                                </div>
##                                <div class="row">
##                                    <div class="help-block col-md-12">
##                                        Выберите диапазон минимальной и максимальной длительности роликов
##                                    </div>
##                                </div>
##                            </div>
##                        </div>
##
##                        <div class="form-group ${'has-error' if form.closable.errors else ''}" data-if-show-video>
##                            ${form.closable.label(class_="col-md-4 control-label")}
##                            <div class="col-md-8">
##                                <div class="row">
##                                    <div class="col-md-12">
##                                        ${form.closable()}
##                                        <input id="closable-slide" type="text" class="form-control"/>
##                                    </div>
##                                </div>
##                                <div class="row">
##                                    <span class="text-danger col-md-12">
##                                        ${", ".join(form.closable.errors)}
##                                    </span>
##                                </div>
##                                <div class="row">
##                                    <div class="help-block col-md-12">
##                                        ${form.closable.description}
##                                    </div>
##                                </div>
##                            </div>
##                        </div>
                    </form>
                </div>
                <div class="row">
                    <div class="col-md-12 text-center">
                        <hr/>
                        <button type="submit" class="btn btn-success btn-tall btn-wide">
                            <i class="fa fa-fw fa-save"></i> Сохранить
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<!-- END PAGE CONTENT-->
<script type="text/javascript">
    $(document).on('ready', function() {
        var zoneSize = '${form.width.data};${form.height.data}';
        var zoneSizes = ${['%s;%s' % (s.width, s.height) for s in Unit.Size.values()] + ['0;0']};

        var isNew = "${g.unit.id}" == "None";

        var isCustom = !_.contains(zoneSizes, zoneSize);

        console.debug(isCustom, isNew);

##        $("#duration-slide")
##            .ionRangeSlider({
##                min: 0,
##                max: 45,
##                from: ${form.data.get('min_duration') or 0},
##                to: ${form.data.get('max_duration') or 15},
##                type: 'double',
##                step: 1,
##                postfix: " сек.",
##                prettify: false,
##                hasGrid: true,
##                onChange: function(obj) {
##                    $("#min_duration").val(obj.fromNumber);
##                    $("#max_duration").val(obj.toNumber);
##                }
##            });
##
##        $("#closable-slide")
##            .ionRangeSlider({
##                min: 0,
##                max: 45,
##                from: ${form.data.get('closable') or 0},
##                type: 'single',
##                step: 1,
##                postfix: " сек.",
##                prettify: false,
##                hasGrid: true,
##                onChange: function (obj) {
##                    $("#closable").val(obj.fromNumber);
##                }
##            });

        var updatePreview = function() {
            var imgClass = '.zone-preview-' + $('#device_type').val() + $('#type').val();
            $('.zone-preview-img').hide();
            $(imgClass).show();
        };

        var updateShowVideo = function (element) {
            if (element.is(':checked')) {
                $('*[data-if-show-video]').show();
            } else {
                $('*[data-if-show-video]').hide();
            }
        };

        var isFloat = function(a) { return a == +a && +a !== (+a | 0) };
        var isInt = function (a) { return a == +a && +a === (+a | 0) };

##        var checkboxShowVideo = $('#show_video');

##        updateShowVideo(checkboxShowVideo);

##        checkboxShowVideo.on('change', function () {
##            updateShowVideo($(this));
##        });

        $('a.type').on('click', function () {
            $('a.type').removeClass('active');
            $(this).addClass('active');
            $('*[data-if-type]').hide();
            $('*[data-if-type="'+$(this).data('type')+'"]').show();
            $('#type').val($(this).data('type'));
            if (+$(this).data('type') == ${Unit.Type.BANNER}) {
                $('#show_video').prop('checked', false);
                $.uniform.update();
##                updateShowVideo(checkboxShowVideo);
            }
            //updatePreview();
        });
        $('a.type[data-type="${g.unit.type or 1}"]').trigger('click');

##        var phoneSizes = [
##            $('#size > option[value="${';'.join((str(v) for v in Unit.Size.CUSTOM()))}"]').clone(),
##            $('#size > option[value="${';'.join((str(v) for v in Unit.Size.BANNER()))}"]').clone(),
##            $('#size > option[value="${';'.join((str(v) for v in Unit.Size.LARGE_BANNER()))}"]').clone(),
##            $('#size > option[value="${';'.join((str(v) for v in Unit.Size.MEDIUM_RECTANGLE()))}"]').clone()
##        ];
##        var tabletSizes = $('#size > option').clone();

        var toggleSizeChoices = function(deviceType) {
            $('#size').select2('val', '320;50');
            switch(deviceType) {
                case ${Unit.DeviceType.PHONE}:
                    $('option[value="${';'.join((str(v) for v in Unit.Size.FULL_BANNER()))}"]').prop('disabled', true);
                    $('option[value="${';'.join((str(v) for v in Unit.Size.LEADERBOARD()))}"]').prop('disabled', true);
                    break;
                case ${Unit.DeviceType.TABLET}:
                    $('option[value="${';'.join((str(v) for v in Unit.Size.FULL_BANNER()))}"]').removeProp('disabled');
                    $('option[value="${';'.join((str(v) for v in Unit.Size.LEADERBOARD()))}"]').removeProp('disabled');
            }
        };

        $('a.device-type').on('click', function () {
            $('a.device-type').removeClass('active');
            $(this).addClass('active');
            $('#device_type').val($(this).data('device-type'));
            toggleSizeChoices(+$(this).data('device-type'));
##            $('#size')
##                    .empty()
##                    .append(($(this).data('device-type') == ${Unit.DeviceType.PHONE}) ? phoneSizes : tabletSizes)
##                    .trigger('change')
##                    .select2();
##            //updatePreview();
        });
        $('a.device-type[data-device-type="${g.unit.device_type or 1}"]').trigger('click');

        $('button[type="submit"]').on('click', function() {
            $('#zone-form').submit();
        });

        $('#size').on('change', function () {
            var value = $(this).val() ? $(this).val().split(';') : ['', ''];
            if (value[0] == "" && value[1] == "") {
                $('.custom-size').removeClass('hidden');
##                $('#width').val('');
##                $('#height').val('');
            } else {
                $('.custom-size').addClass('hidden');
                $('#width').val(value[0]);
                $('#height').val(value[1]);
            }
        });
        $('#size').val(isCustom ? ";" : (isNew ? "320;50" : zoneSize)).select2().trigger('change');

        $('.input-spinner').each(function() {
            var element = $(this), _prevValue;
            var min = +element.attr('min') || 0,
                max = +element.attr('max') || Infinity,
                step = +element.attr('step') || 1,
                forceFloat = +element.attr('decimal') || false;

            var increaseButton = $('<span class="input-group-btn"><button class="btn btn-default"><i class="fa fa-fw fa-plus"></i></button></span>')
                    .on('click', function() {
                        var value = +element.val();
                        if (value + step <= max) {
                            if (forceFloat || isFloat(value + step))
                                element.val((value + step).toFixed(2));
                            else
                                element.val(value + step);
                        }
                    }),
                decreaseButton = $('<span class="input-group-btn"><button class="btn btn-default"><i class="fa fa-fw fa-minus"></i></button></span>')
                    .on('click', function () {
                        var value = +element.val();
                        if (value - step >= min) {
                            if (forceFloat || isFloat(value - step))
                                element.val((value - step).toFixed(2));
                            else
                                element.val(value - step);
                        }
                    });
            element
                .on('keydown', function () {
                    var value = +element.val();
                    if (!(isNaN(value) || value > max || value < min)) {
                        _prevValue = value;
                    }
                })
                .on('keyup', function() {
                    var value = +element.val();
                    if (isNaN(value) || value > max || value < min) {
                        element.val(_prevValue);
                    }
                });
            if (!element.parent().hasClass('input-group')) {
                element.wrap($('<div class="input-group"></div>'));
            }
            element.after(increaseButton);
            element.before(decreaseButton);
        });
    });
</script>