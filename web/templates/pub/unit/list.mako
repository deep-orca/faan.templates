## -*- coding: utf-8 -*-
<%namespace name="bcrumb" file="/layout/bcrumb.mako" />
<%namespace name="fabutton" file="/layout/fa-button.mako" />
<%inherit file="/layout/main.mako" />
<%def name="title()">Блоки</%def>
<%def name="description()">Блоки площадки [${g.application.id}] ${g.application.name}</%def>
<%
    import datetime
    from faan.core.model.pub.unit import Unit
%>

${ bcrumb.h(self) }
${ bcrumb.bc([ { 'name' : u'Площадки',  'href' : '/pub/application/', 'icon' : 'rocket' }
             , { 'name' : u"%s<sup>[%s]</sup>" % (g.application.name, g.application.id), 'href' : '/pub/application/%s/unit' % (g.application.id) }
             , { 'name' : u'Блоки' }
             ]) }

<!-- BEGIN PAGE CONTENT-->
<div class="row">
    <div class="col-md-12">

    <div class="portlet">
        <div class="portlet-title">
            <div class="caption"><i class="fa fa-sitemap"></i>Блоки площадки ${g.application.name}<sup>[${g.application.id}]</sup></div>
            <div class="actions">
                <a href="/pub/application/${g.application.id}/unit/new" class="btn btn-success"><i class="fa fa-plus"></i> Добавить новый</a>
            </div>
        </div>
        
        <div class="portlet-body">
            <table class="table table-striped table-bordered table-hover" id="zones_table">
            <thead>
            <tr>
                <th style="width:15px;"><input type="checkbox" class="group-checkable" data-set="#zones_table .checkboxes"/></th>
                <th>Название</th>
                <th class="text-center">Тип блока</th>
##                <th class="text-center">Тип устройства</th>ё
                <th class="text-center">Статус</th>
            </tr>
            </thead>
            <tbody>
            % for u in g.units:
            <tr class="odd gradeX">
                <td><input type="checkbox" class="checkboxes" value="${u.id}"/></td>
                <td>
                    <div class="row">
                        <div class="col-md-8">

                            <h4 style="margin-top:0;">
                                <span class="badge badge-default">#${u.id}</span>
                                <a href="${u.id}/source">${u.name}</a>
                            </h4>
                            <p class="text-left">
                                <%
                                today = datetime.date.today()
                                monthago = today - datetime.timedelta(days=30)
                                %>
                                ${ fabutton.fab([{ 'text' : u'Активировать',  'href' : '/pub/application/%d/unit/%d/activate'%(g.application.id, u.id), 'icon' : 'play' }]) if u.state == Unit.State.DISABLED else fabutton.fab([{ 'text' : u'Остановить',  'href' : '/pub/application/%d/unit/%d/deactivate'%(g.application.id, u.id), 'icon' : 'pause' }])
                                }
                                ${ fabutton.fab([
                                    { 'text' : u'Редактировать',  'href' : '/pub/application/%d/unit/%d'%(g.application.id, u.id), 'icon' : 'pencil' },
                                    { 'text' : u'Статистика',  'href' : '/pub/stat/date#?from=%s&to=%s&application=%d&unit=%d'%(unicode(monthago.strftime('%d.%m.%Y')), unicode(today.strftime('%d.%m.%Y')), g.application.id, u.id), 'icon' : 'bar-chart-o' },
                                    {
                                        'text': u'Удалить',
                                        'href': '#delete',
                                        'icon': 'times',
                                        'data': {
                                            'aid': '%d' % g.application.id,
                                            'zid': '%d' % u.id
                                        },
                                        'class': 'delete_zone'
                                    }
                                ])
                                }
                            </p>
                        </div>
                    </div>
                </td>

                <td>
                    %if u.type == u.Type.BANNER:
                        Баннер (${u.width} &times; ${u.height})
                    %endif
                    %if u.type == u.Type.INTERSTITIAL:
                        Полноэкранная (${u.width} &times; ${u.height})
                    %endif
                </td>
                <td class="text-center">
                    <span class="hidden">${u.state}</span>
                    ${fabutton.fas([{'status': u.state, 'type': 'zone'}])}
                </td>
            </tr>
            % endfor
            </tbody>
            </table>
        </div>
    </div>
 
</div>
</div>

<div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog" style="width: 25%">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="myModalLabel">Удалить зоны?</h4>
      </div>
      <div class="modal-body">
        <div class="pasta form-group">
            Действительно удалить блоки <span id="del_list"></span> ?

        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
        <button type="button" id="delete" class="btn btn-danger group_actions">Удалить</button>
      </div>
    </div>
  </div>
</div>
<!-- END PAGE CONTENT-->

<script src="/global/static/scripts/zones_table.js"></script>
<script type="text/javascript">
    var deleteModal = $('#deleteModal');
    $('.delete_zone').on('click', function() {
        var aid = $(this).data('aid'),
            zid = $(this).data('zid');
        $('#delete').one('click', function() {
            window.location = '/pub/application/' + aid + '/unit/' + zid + '/delete';
        });
        if (zid) {
            deleteModal.modal();
            $('#del_list').html('').html(zid);
        }
    });
TableManaged.init();
</script>
