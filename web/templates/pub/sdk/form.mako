## -*- coding: utf-8 -*-
<%namespace name="bcrumb" file="/layout/bcrumb.mako" />
<%inherit file="/layout/main.mako" />

<%def name="title()">
Скачать SDK
</%def>

<%def name="description()">Выбор платформы</%def>
<%def name="page_css()">
<link href="/static/css/sdk.css" rel="stylesheet" type="text/css"/>
</%def>

${ bcrumb.h(self) }
${ bcrumb.bc([ { 'name' : u'Приложения', 'href' : '/pub/application/', 'icon' : 'rocket' }
			 , { 'name' : u'Скачать SDK', 	'href' : '/pub/application/sdk' }
			 ]) }

<div class="row">																<!-- BEGIN PAGE CONTENT-->
<div class="col-md-12">
	<div class="portlet">
        <div class="portlet-title"><div class="caption"><i class="fa fa-download"></i>${self.title()}</div></div>
	
		<div class="portlet-body">
			<ul class="nav nav-pills">
				<li class="active">
					<a href='#ios' data-toggle="tab">iOS </a>
				</li>
				<li class="">
					<a href='#android' data-toggle="tab"> Android</a>
				</li>
				<li class="">
					<a href='#windows' data-toggle="tab"> Windows </a>
				</li>
			</ul>
			<div class="tab-content">
				<div class="tab-pane fade active in" id="ios">
					<div class="row">
						<div class="col-md-3 thumbnail sdk_tab_content">
							<div class="col-md-1 sdk_logo">	
									<i class="fa fa-apple downsdk"></i>
							</div>
	
								<div class="col-md-1 sdk_desc">
									<h2 class="h2_big"> iOS SDK</h2>
	
									<span class="help-block">Версия 1.2.9</span>
								</div>
	
						</div>
						<div class="clearfix"></div>
						<div class="col-md-4">
							<a href="#" class="btn btn btn-success"><i class="fa fa-download"></i> Скачать</a>
							<a href="https://docs.google.com/document/d/1IBJnjP-5b9-oOekxYvjxpsUqlC-3-CwaBotX6uxgutM/edit?usp=sharing" class="btn btn btn-info" target="_blank"><i class="fa fa-code"></i> Инструкции</a>
						</div>
					</div>
				</div>
				<div class="tab-pane fade" id="android">
					
					<div class="row">
						<div class="col-md-4 thumbnail sdk_tab_content">
							<div class="col-md-1 sdk_logo">	
									<i class="fa fa-android downsdk"></i>
							</div>
	
								<div class="col-md-1 sdk_desc">
									<h2 class="h2_big"> Android SDK</h2>
	
									<span class="help-block">Версия 2.0.0</span>
								</div>
	
						</div>
						<div class="clearfix"></div>
						<div class="col-md-4">
							<a href="/static/sdk/android/vidiger-sdk-2.0.0.jar" class="btn btn btn-success"><i class="fa fa-download"></i> Скачать</a>
							<a href="https://docs.google.com/document/d/1qnlBgDueE-YKp9aOjmoS6C7mWxTNDtuYRDv1PDFBBHQ/edit?usp=sharing" class="btn btn btn-info" target="_blank"><i class="fa fa-code"></i> Инструкции</a>
						</div>
					</div>
				</div>
				<div class="tab-pane fade" id="windows">
					<div class="row">
						<div class="col-md-4 thumbnail sdk_tab_content">
							<div class="col-md-1 sdk_logo" style="padding-left: 20px;">	
									<i class="fa fa-windows downsdk"></i>
							</div>
	
								<div class="col-md-1 sdk_desc">
									<h2 class="h2_big"> Windows SDK</h2>
	
									<span class="help-block">Версия 2.19.3</span>
								</div>
	
						</div>
						<div class="clearfix"></div>
						<div class="col-md-4">
							<a href="#" class="btn btn btn-success"><i class="fa fa-download"></i> Скачать</a>
							<a href="#" class="btn btn btn-info"><i class="fa fa-code"></i> Инструкции</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
</div>
<!--END TABLE-->
<!-- END PAGE CONTENT-->

<script>

</script>
