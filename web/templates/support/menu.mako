<div class="col-md-3">
    <div class="list-group margin-left-10 x-actions-list">
        <a href="/${account_type}/support/new"
           class="list-group-item bg-success sactive margin-bottom-10 round-bottom ${'active' if g.view_type == 'new' else ''}">
            Создать новый
        </a>
        <a href="/${account_type}/support/open"
           class="list-group-item round-top ${'active' if g.view_type == 'open' else ''}">
            Открытые
            <span class="badge">${g.total_new_replies.get(0, "")}</span>
        </a>
        <a href="/${account_type}/support/resolved"
           class="list-group-item ${'active' if g.view_type == 'resolved' else ''}">
            Закрытые
            <span class="badge">${g.total_new_replies.get(1, "")}</span>
        </a>
    </div>
</div>