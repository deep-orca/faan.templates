## -*- coding: utf-8 -*-

<%namespace name="bcrumb" file="/layout/bcrumb.mako" />

<%inherit file="/layout/main.mako" />

## Page JS
<%def name="page_js()">
    <script src="/global/static/plugins/moment-with-langs.min.js" type="text/javascript"></script>
</%def>
## END

## Page CSS
<%def name="page_css()">
    <link href="//netdna.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet">
    ##    <link href="/global/static/css/pages/inbox.css" rel="stylesheet" type="text/css"/>

    <style>
        .margin-left-10 {
            margin-left: 10px;
        }

        .round-top {
            -moz-border-top-left-radius: 4px;
            -webkit-border-top-left-radius: 4px;
            border-top-left-radius: 4px;
            -moz-border-top-right-radius: 4px;
            -webkit-border-top-right-radius: 4px;
            border-top-right-radius: 4px;
        }

        .round-bottom {
            -moz-border-bottom-left-radius: 4px;
            -webkit-border-bottom-left-radius: 4px;
            border-bottom-left-radius: 4px;
            -moz-border-bottom-right-radius: 4px;
            -webkit-border-bottom-right-radius: 4px;
            border-bottom-right-radius: 4px;
        }
    </style>
</%def>
## END

<%def name="title()">Поддержка</%def>
<%def name="description()"></%def>

${bcrumb.h(self)}

${bcrumb.bc(
[
{'name': u'Поддержка', 'href' : '../support/', 'icon' : 'life-ring'},
{'name': u'Новый запрос', 'href' : '../support/new', 'icon' : ''}
],
[]
)}

<div class="row">
    <div class="row inbox">
        <%include file="menu.mako"/>
        <div class="col-md-9">
            <div class="portlet">
                <div class="portlet-title">
                    <div class="caption" id="portlet-title">Новый запрос</div>
                </div>
                <div class="portlet-body" id="portlet-render">
                    ${form.html(form_id="ticket_form", label_width=3, form_class=form, message={"rows": 7, "placeholder": u"Опишите проблему"}, submit={"class_": "text-center", "has_label": False})}
                </div>
            </div>
        </div>
    </div>
</div>


