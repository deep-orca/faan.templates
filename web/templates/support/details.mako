## -*- coding: utf-8 -*-

<%namespace name="bcrumb" file="/layout/bcrumb.mako" />

<%
    import time
    from datetime import datetime
    from faan.web.app import account
%>

<%inherit file="/layout/main.mako" />

## Page JS
<%def name="page_js()">
    <script src="/global/static/plugins/moment-with-langs.min.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(document).on("ready", function() {
            $("#resolve").on("click", function() {})
        });
    </script>
</%def>
## END

## Page CSS
<%def name="page_css()">
    <link href="//netdna.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet">
    ##    <link href="/global/static/css/pages/inbox.css" rel="stylesheet" type="text/css"/>

    <style>
        .margin-left-10 {
            margin-left: 10px;
        }

        .round-top {
            -moz-border-top-left-radius: 4px;
            -webkit-border-top-left-radius: 4px;
            border-top-left-radius: 4px;
            -moz-border-top-right-radius: 4px;
            -webkit-border-top-right-radius: 4px;
            border-top-right-radius: 4px;
        }

        .round-bottom {
            -moz-border-bottom-left-radius: 4px;
            -webkit-border-bottom-left-radius: 4px;
            border-bottom-left-radius: 4px;
            -moz-border-bottom-right-radius: 4px;
            -webkit-border-bottom-right-radius: 4px;
            border-bottom-right-radius: 4px;
        }

        .reply {
            width: 60%;
            -moz-border-radius: 4px;
            -webkit-border-radius: 4px;
            border-radius: 4px;
            padding: 10px;
            margin: 5px auto;
            -webkit-box-shadow: 1px 1px 10px 1px #ddd;
            -moz-box-shadow: 1px 1px 10px 1px #ddd;
            box-shadow: 1px 1px 10px 1px #ddd;
        }

        .reply.owner {
            background-color: #e5f9ff
        }

        .reply.support {
            background-color: #faffd2;
        }

        .reply .info {
            font-size: 10px;
            color: #888;
            margin-top: 10px;
            font-style: italic;
        }

        .resolved {
            padding: 10px;
            width: 100%;
            color: #888;
            font-style: italic;
            border-top: 1px dashed #ddd;
            margin-top: 10px;
            font-size: 10px;
        }
    </style>
</%def>
## END

<%def name="title()">Поддержка</%def>
<%def name="description()"></%def>

${bcrumb.h(self)}

${bcrumb.bc(
[
{
'name': u'Поддержка',
'href' : '/support/',
'icon' : 'life-ring'
}
],
[]
)}

<div class="row">
    <div class="row inbox">
        <%include file="menu.mako"/>
        <div class="col-md-9">
            <div class="portlet">
                <div class="portlet-title">
                    <div class="caption" id="portlet-title">${g.ticket.title | h}</div>
                    <div class="actions">
                        %if g.ticket.state != g.ticket.State.RESOLVED:
                            <a href="/${account_type}/support/${g.ticket.id}/resolve" class="btn btn-success btn-sm">
                                <i class="fa fa-check"></i> &nbsp;
                                Закрыть запрос
                            </a>
                        %endif
                    </div>

                </div>
                <div class="portlet-body" id="portlet-render">
                    <div class="pull-left reply owner">
                        ${g.ticket.message | h}
                        <div class="info">Вы (${datetime.fromtimestamp(g.ticket.ts_created-time.timezone).strftime("%H:%M:%S %d %B %Y")})</div>
                    </div>
                    <div class="clearfix"></div>

                    %for reply in g.ticket.replies:
                        <div class="${'pull-left owner' if reply.account_id == account.id else 'pull-right support'} reply ">
                            ${reply.message | h}
                            <div class="info">
                                ${u'Вы' if reply.account_id == account.id else u'Сотрудник техподдержки' | h}
                                (${datetime.fromtimestamp(reply.ts_created-time.timezone).strftime("%H:%M:%S %d %B %Y")})
                            </div>
                        </div>
                        <div class="clearfix"></div>
                    %endfor

                    %if g.ticket.state == g.ticket.State.RESOLVED:
                        <div class="text-center resolved">Запрос закрыт</div>
                    %endif


                    %if g.ticket.state != g.ticket.State.RESOLVED:
                        <hr/>
                        ${form.html(message={"has_label": False, "placeholder": u"Ответ"}, submit={"has_label": False, "class": "btn-block", "title": u"Ответить"})}
                    %endif
                </div>
            </div>
        </div>
    </div>
</div>


