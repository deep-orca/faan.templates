## -*- coding: utf-8 -*-

<%namespace name="bcrumb" file="/layout/bcrumb.mako" />

<%
    import time
    from datetime import datetime
    from faan.web.app import account
    from faan.core.model.ticket import Ticket

    # Categories hr mapping
    _category_mapping = {
        0: u"Общие вопросы",
        1: u"Технические вопросы",
        2: u"Финансовые вопросы",
    }

    # Urgency hr mapping
    _urgency_mapping = {
        0: u"Низкая",
        1: u"Средняя",
        2: u"Высокая",
        3: u"Критическая",
    }
%>

<%inherit file="/layout/main.mako" />

## Page JS
<%def name="page_js()">
    <script src="/global/static/plugins/moment-with-langs.min.js" type="text/javascript"></script>
</%def>
## END

## Page CSS
<%def name="page_css()">
    <link href="//netdna.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet">
##    <link href="/global/static/css/pages/inbox.css" rel="stylesheet" type="text/css"/>
    <style>
        .margin-left-10 {
            margin-left: 10px;
        }

        .round-top {
            -moz-border-top-left-radius: 4px;
            -webkit-border-top-left-radius: 4px;
            border-top-left-radius: 4px;
            -moz-border-top-right-radius: 4px;
            -webkit-border-top-right-radius: 4px;
            border-top-right-radius: 4px;
        }        
        .round-bottom {
            -moz-border-bottom-left-radius: 4px;
            -webkit-border-bottom-left-radius: 4px;
            border-bottom-left-radius: 4px;
            -moz-border-bottom-right-radius: 4px;
            -webkit-border-bottom-right-radius: 4px;
            border-bottom-right-radius: 4px;
        }
    </style>
</%def>
## END

<%def name="title()">Поддержка</%def>
<%def name="description()"></%def>

${bcrumb.h(self)}

${bcrumb.bc(
[
{'name': u'Поддержка', 'href' : '../support/', 'icon' : 'life-ring'},
],
[]
)}

<div class="row">
    <div class="row inbox">
        <%include file="menu.mako"/>

        <div class="col-md-9">
            <div class="portlet">
                <div class="portlet-title">
                    <div class="caption" id="portlet-title">${u'Открытые' if g.view_type == 'open' else u'Закрытые'}</div>
                </div>
                <div class="portlet-body" id="portlet-render">
                    %if g.tickets:
                        <table class="table table-striped table-hover">
                            <thead>
                                <tr>
                                    <th>
                                        &nbsp;
                                    </th>
                                    <th>
                                        Создан
                                    </th>
                                    <th>
                                        Тема
                                    </th>
                                    <th>
                                        Тип
                                    </th>
                                    <th>
                                        Срочность
                                    </th>
                                    <th>
                                        Ответов
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                                %for ticket in g.tickets:
                                <tr>
                                    <td>
                                        ${"<i class='fa fa-comment' style='color: #888;'></i>" if ticket.new_replies_owner[0] else ""}
                                         ${ticket.new_replies_owner[1] if ticket.new_replies_owner[1] else ""}
                                    </td>
                                    <td>
                                        ${datetime.fromtimestamp(ticket.ts_created-time.timezone).strftime("%H:%M:%S   %d %B %Y")}
                                    </td>
                                    <td>
                                        <a href="/${account_type}/support/${ticket.id}">${ticket.title | h}</a>
                                    </td>
                                    <td>
                                        ${_category_mapping.get(ticket.category)}
                                    </td>
                                    <td>
                                        ${_urgency_mapping.get(ticket.urgency)}
                                    </td>
                                    <td>
                                        ${len(ticket.replies)}
                                    </td>
                                </tr>
                                %endfor
                            </tbody>
                        </table>
                    %else:
                        <div class="text-center well">Нет запросов</div>
                    %endif
                </div>
            </div>
        </div>
    </div>
</div>



