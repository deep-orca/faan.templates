## -*- coding: utf-8 -*-

<%namespace name="bcrumb" file="/layout/bcrumb.mako" />

<%inherit file="/layout/main.mako" />

## Page JS
<%def name="page_js()">
    <script type="text/javascript" src="/global/static/plugins/underscore.min.js"></script>
    <script src="/global/static/plugins/moment-with-langs.min.js" type="text/javascript"></script>
    <script src="/global/static/plugins/bootstrap-daterangepicker/daterangepicker.js" type="text/javascript"></script>
    <script src="/global/static/plugins/flot/jquery.flot.js" type="text/javascript"></script>
    <script src="/global/static/plugins/flot/jquery.flot.resize.js" type="text/javascript"></script>
    <script src="/global/static/plugins/flot/jquery.flot.time.js" type="text/javascript"></script>
    <script src="/global/static/plugins/toolbox/lime.tables.js" type="text/javascript"></script>
    <script src="/global/static/scripts/adv_dashboard.js" type="text/javascript"></script>
</%def>
## END

## Page CSS
<%def name="page_css()">
    <link href="/global/static/css/plugins.css" rel="stylesheet" type="text/css"/>
    <link href="/global/static/css/pages/dashboard.css" rel="stylesheet" type="text/css"/>
    <link href="/global/static/plugins/fullcalendar/fullcalendar/fullcalendar.css" rel="stylesheet" type="text/css"/>
    <link href="/global/static/plugins/bootstrap-daterangepicker/daterangepicker-bs3.css" rel="stylesheet"
          type="text/css"/>
    <style>
        .created {
            font-style: oblique;
            color: #aaa;
            margin: 0 10px 0 8px;
        }

        .news-item .list-inline li a {
            color: #eee;
        }

        .news-item .list-inline li:first-child {
            padding-left: 5px;
        }

        .news-item .list-inline {
            display: inline;
        }

        .news-item .panel-footer {
            font-size: 10px;
            padding: 5px;
            color: #888;
        }

        .news-item p {
            margin: 0;
        }

        .news-item h3 {
            margin-bottom: 10px;
        }
    </style>
</%def>
## END

<%def name="title()">Новости</%def>
<%def name="description()">${g.news_item.title}</%def>

${bcrumb.h(self)}

${bcrumb.bc(
[
{
'name': u'Назад к списку новостей',
'href' : '/{account_group}/news'.format(account_group=g.account_group),
'icon' : 'inbox'
}
],
[]
)}

<div class="clearfix">
</div>
<div class="row">
    <div class="col-md-12">
        <div class="row">
            <div class="col-md-12 col-sm-12">
                <div id="news-${g.news_item.id}"
                     class="news-item panel panel-${g.priorities.get(g.news_item.priority)[1]} bg-danger">
                    <div class="panel-heading">
                        <h3 class="panel-title"><strong>${g.news_item.title}</strong>
                        </h3>

                        <p>${g.news_item.message}</p>
                    </div>
                    <div class="panel-footer">
                        <span class="created" data-timestamp="${g.news_item.ts_created * 1000}"></span>
                        <ul class="list-inline">
                            <li><i class="fa fa-tags"></i></li>
                            %for tag in g.news_item.tags:
                                <li class="label label-default"><a
                                        href="/${g.account_group}/news/?tag=${tag}">${g.tags.get(tag).name}</a>
                                </li>
                            %endfor
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(document).on("ready", function () {
        var currentTimezone = +(moment().format("ZZ")) * 36000;
        $("*[data-timestamp]").each(function () {
            var self = $(this);
            self.text(moment(self.data("timestamp") + currentTimezone).format("DD MMMM YYYY HH:mm:ss"))
        });
    });
</script>
