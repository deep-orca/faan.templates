## -*- coding: utf-8 -*-

<%namespace name="bcrumb" file="/layout/bcrumb.mako" />

<%inherit file="/layout/main.mako" />

## Page JS
<%def name="page_js()">
    <script type="text/javascript" src="/global/static/plugins/underscore.min.js"></script>
    <script src="/global/static/plugins/moment-with-langs.min.js" type="text/javascript"></script>
    <script src="/global/static/plugins/bootstrap-daterangepicker/daterangepicker.js" type="text/javascript"></script>
    <script src="/global/static/plugins/flot/jquery.flot.js" type="text/javascript"></script>
    <script src="/global/static/plugins/flot/jquery.flot.resize.js" type="text/javascript"></script>
    <script src="/global/static/plugins/flot/jquery.flot.time.js" type="text/javascript"></script>
    <script src="/global/static/plugins/toolbox/lime.tables.js" type="text/javascript"></script>
    <script src="/global/static/scripts/adv_dashboard.js" type="text/javascript"></script>
</%def>
## END

## Page CSS
<%def name="page_css()">
    <link href="/global/static/css/plugins.css" rel="stylesheet" type="text/css"/>
    <link href="/global/static/css/pages/dashboard.css" rel="stylesheet" type="text/css"/>
    <link href="/global/static/plugins/fullcalendar/fullcalendar/fullcalendar.css" rel="stylesheet" type="text/css"/>
    <link href="/global/static/plugins/bootstrap-daterangepicker/daterangepicker-bs3.css" rel="stylesheet"
          type="text/css"/>
    <style>
        .created {
            font-style: oblique;
            color: #aaa;
            margin-right: 10px;
        }

        .tags-cloud .list-inline li a {
            color: #eee;
        }

        .tags-cloud .list-inline li:first-child {
            padding-left: 5px;
        }

        .news-item {
            margin: 15px 0;
        }

        .news-item .list-inline li a {
            color: #eee;
        }

        .news-item .list-inline li:first-child {
            padding-left: 5px;
        }

        .news-item .list-inline {
            display: inline;
        }

        .news-item .panel-footer {
            font-size: 10px;
            padding: 5px;
            color: #888;
        }

        .news-item p {
            margin: 0;
        }

        .news-item h3 {
            margin-bottom: 10px;
        }
    </style>
</%def>
## END

<%def name="title()">Новости</%def>
<%def name="description()">Список новостей</%def>

${bcrumb.h(self)}

${bcrumb.bc(
[
{
'name': u'Новости',
'href' : '/{account_group}/news'.format(account_group=g.account_group),
'icon' : 'inbox'
}
],
[]
)}

<div class="clearfix">
</div>
<div class="row">
    <div class="col-md-12">
        <div class="row">
            <div class="tags-cloud col-md-12 col-sm-12">
                <ul class="list-inline">
                    <li>Теги:</li>
                    <li class="label ${'label-primary' if not g.current_tag else 'label-default'}"
                        id="tag-item-0"><a href="?tag=all">Все</a></li>
                    %for tag_id, tag_data in g.tags.iteritems():
                        <li class="label ${'label-primary' if (g.current_tag and g.current_tag == tag_id) else 'label-default'}"
                            id="tag-item-${tag_id}"><a href="?tag=${tag_id}">${tag_data.name}</a></li>
                    %endfor
                </ul>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12 col-sm-12">
                %if g.news:
                    %for news_item in g.news:
                        <div id="news-${news_item.id}" class="news-item panel panel-${g.priorities.get(news_item.priority)[1]} bg-danger">
                            <div class="panel-heading">
                                <h3 class="panel-title"><a
                                        href="/${g.account_group}/news/${news_item.id}"><strong>${news_item.title}</strong></a>
                                </h3>

                                <p>${h.limitstr(news_item.message, 80)}</p>
                            </div>
                            <div class="panel-footer">
                                <span class="created" data-timestamp="${news_item.ts_created * 1000}"></span>
                                <ul class="list-inline">
                                    <li><i class="fa fa-tags"></i></li>

                                    %if news_item.tags:
                                        %for tag in news_item.tags:
                                            <li class="label label-default"><a
                                                    href="/${g.account_group}/news/?tag=${tag}">${g.tags.get(tag).name}</a></li>
                                        %endfor
                                    %else:
                                        <li>Нет тегов</li>
                                    %endif
                                </ul>
                            </div>
                        </div>
                    %endfor
                %else:
                    <div class="text-center">
                        <h4>Нет новостей</h4>
                        <a href="/">Вернуться на главную</a>
                    </div>
                %endif
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(document).on("ready", function () {
        var currentTimezone = +(moment().format("ZZ")) * 36000;
        $("*[data-timestamp]").each(function () {
            var self = $(this);
            self.text(moment(self.data("timestamp") + currentTimezone).format("DD MMMM YYYY HH:mm:ss"))
        });
    });
</script>
