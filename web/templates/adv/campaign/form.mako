## -*- coding: utf-8 -*-
<%namespace name="bcrumb" file="/layout/bcrumb.mako" />
<%inherit file="/layout/main.mako" />

<%def name="title()">
    % if g.campaign.id:
        Кампания ${g.campaign.name} [${g.campaign.id}]
    % else:
        Новая кампания
    % endif
</%def>

<%def name="description()">Форма редактирования</%def>

<%def name="page_js()">
    <script type="text/javascript" src="/global/static/plugins/jquery.validate.min.js"></script>
    <script type="text/javascript" src="/global/static/plugins/jquery-validation/localization/messages_ru.js"></script>
    <script type="text/javascript"
            src="/global/static/plugins/bootstrap-switch/js/bootstrap-switch.min.js"></script>
    <script type="text/javascript"
            src="/global/static/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
    <script type="text/javascript"
            src="/global/static/plugins/bootstrap-datepicker/js/locales/bootstrap-datepicker.ru.js"></script>
    <script type="text/javascript" src="/global/static/plugins/fancytree/jquery.fancytree.min.js"></script>

</%def>

<%def name="page_css()">
    <link rel="stylesheet" type="text/css"
          href="/global/static/plugins/bootstrap-daterangepicker/daterangepicker-bs3.css"/>
    <link rel="stylesheet" type="text/css" href="/global/static/plugins/bootstrap-datepicker/css/datepicker.css"/>
    <link rel="stylesheet" type="text/css"
          href="/global/static/plugins/bootstrap-switch/css/bootstrap3/bootstrap-switch.min.css"/>
    <link rel="stylesheet" type="text/css" href="/global/static/plugins/fancytree/skin-lion/ui.fancytree.min.css"/>

    <style>
        #daterange input {
            cursor: pointer !important;
        }

        .xform-error {
            padding: 3px;
            font-weight: bold !important;
        }

        .form-group:last-child {
            margin-bottom: 0;
        }

        #payment_type input[type='radio'] {
            float: none !important;
            margin: 0 !important;
        }
    </style>
</%def>

${ bcrumb.h(self) }
${ bcrumb.bc([ { 'name' : u'Кампании',  'href' : '/adv/campaign/', 'icon' : 'bullhorn' }
, { 'name' : u"%s<sup>[%s]</sup>" % (g.campaign.name, g.campaign.id) if g.campaign.id else u'Новая кампания', 'href': '/adv/campaign/%s/media/' % (g.campaign.id) if g.campaign.id else "javascript:void(0);" }
]) }

<%

    from faan.core.model.adv.campaign import Campaign
    from faan.core.model.pub.application import Application

    platform_dict = { u'iOS'      : Application.Os.IOS
                , u'Android'  : Application.Os.ANDROID
                , u'WinPhone' : Application.Os.WINPHONE
                }

    devclass_dict = { u'Телефон'  : Campaign.DevClass.PHONE
                , u'Планшет'  : Campaign.DevClass.TABLET
                }


%>

<!-- BEGIN PAGE CONTENT-->

<div class="row">
    <form id="campaign_form" method="POST" class="form-horizontal">
        ${form.csrf_token}
        <div class="col-md-9">
            <div class="row">
                ## <pre>${form.errors}</pre>
                ## Name + category
                ## START
                <div class="col-md-12">
                    <h3>Общие настройки</h3>

                    <hr/>

                    <div class="portlet">
                        <div class="portlet-title">
                            <div class="caption">Основное</div>
                        </div>
                        <div class="portlet-body">
                            <div class="form-group ${'has-error' if form.name.errors else ''}">
                                ${ form.name.label(class_="col-sm-3 control-label") }
                                <div class="col-sm-9">
                                    ${ form.name(class_="form-control", placeholder=u"Имя") }
                                    <div class="xform-error text-danger text-center">${"<br>".join(form.name.errors)}</div>
                                </div>
                            </div>

                            <div class="form-group ${'has-error' if form.category.errors else ''}">
                                ${ form.category.label(class_="col-sm-3 control-label") }
                                <div class="col-sm-9">
                                    ${ form.category(class_="form-control") }
                                    <div class="xform-error text-danger text-center">${"<br>".join(form.category.errors)}</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                ## END
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="portlet">
                        <div class="portlet-title">
                            <div class="caption">Ограничения расходов</div>
                        </div>
                        <div class="portlet-body">
                            <div class="form-group ${'has-error' if form.limit_cost_total.errors or form.limit_cost_daily.errors else ''}">
                                <label for="limit-total" class="col-sm-3 control-label">Общие</label>

                                <div class="col-sm-3">
                                    <input name="limit_cost_total" type="text" class="form-control" id="limit_total"
                                           value="${form.limit_cost_total.data or 0}"/>
                                    <span class="help-block"><small>0 - без ограничений</small></span>
                                </div>
                                <label for="limit-daily" class="col-sm-3 control-label">В сутки</label>

                                <div class="col-sm-3">
                                    <input type="text" name="limit_cost_daily" class="form-control" id="limit_daily"
                                           value="${form.limit_cost_daily.data or 0}"/>
                                </div>
                            </div>
                            <div class="form-group ${'has-error' if form.ts_start.errors or form.ts_end.errors else ''} form-inline">
                                <label for="date-start" class="col-sm-3 control-label">Начало показа</label>

                                <div class="col-sm-3">
                                    <div id="date-start">
                                        <input type="text" name="ts_start" class="form-control" placeholder="" readonly
                                               value=""/>
                                    </div>
                                </div>
                                <label for="date-end" class="col-sm-3 control-label">Конец показа</label>

                                <div class="col-sm-3">
                                    <div id="date-end">
                                        <input type="text" name="ts_end" class="form-control" placeholder="" readonly
                                               value=""/>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <h3>Настройки площадок</h3>
                    <hr/>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="portlet">
                        <div class="portlet-title">
                            <div class="caption">Таргетинг: Категории</div>
                            <div class="pull-right">
                                <input type="checkbox"
                                       class="bs-switch toggle"
                                       id="category_targeting"
                                       name="targeting"
                                       value="${Campaign.Targeting.CATEGORY}"
                                       data-on-color="success" data-off-color="danger" data-size="small"
                                       ${'checked' if Campaign.Targeting.CATEGORY & g.campaign.targeting or Campaign.Targeting.CATEGORY in (form.targeting.data or []) else ''}
                                />
                            </div>
                        </div>
                        <div class="portlet-body">
                            <div class="form-group"
                                 id="camp-target-categories">

                                <label class="col-md-12 text-center">
                                    Категории приложений для показа рекламы
                                </label>

                                <div class="xform-error text-danger text-center">${"<br>".join(form.target_categories_groups.errors)}</div>
                            </div>
                            <div class="btn-group-vertical" style="width: 100%;">
                                <button type="button" class="btn btn-default check_all"
                                        style="font-size:12px;">Выделить
                                    все
                                </button>
                                <button type="button" class="btn btn-default uncheck_all"
                                        style="font-size:12px;">Снять
                                    выделение
                                </button>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-md-6">
                    <div class="portlet">
                        <div class="portlet-title">
                            <div class="caption">Таргетинг: Возраст</div>
                            <div class="pull-right">
                                <input type="checkbox"
                                       class="bs-switch toggle"
                                       id="category_targeting"
                                       name="targeting"
                                       value="${Campaign.Targeting.CONTENT_RATING}"
                                       data-on-color="success" data-off-color="danger" data-size="small"
                                    ${'checked' if Campaign.Targeting.CONTENT_RATING & g.campaign.targeting or Campaign.Targeting.CONTENT_RATING in (form.targeting.data or []) else ''}
                                        />
                            </div>
                        </div>
                        <div class="portlet-body">
                            <div class="form-group ${'has-error' if form.target_content_rating.errors else ''}"
                                 id="camp-target-content-rating">

                                <label class="col-md-12 text-center">
                                    Возрастные ограничения приложений
                                </label>

                                <ul class="col-md-12 text-left list-unstyled">
                                    <li>
                                        <label>
                                            <input type="checkbox" class="toggle checkbox-uniform"
                                                   id="app_age_targeting"
                                                   name="target_content_rating"
                                                   value="1" ${'checked' if 1 in form.data.get('target_content_rating') else ''} />
                                            Младшая
                                        </label>
                                    </li>
                                    <li>
                                        <label>
                                            <input type="checkbox" class="toggle checkbox-uniform"
                                                   id="app_age_targeting"
                                                   name="target_content_rating"
                                                   value="2" ${'checked' if 2 in form.data.get('target_content_rating') else ''}/>
                                            Средняя
                                        </label>
                                    </li>
                                    <li>
                                        <label>
                                            <input type="checkbox" class="toggle checkbox-uniform"
                                                   id="app_age_targeting"
                                                   name="target_content_rating"
                                                   value="3" ${'checked' if 3 in form.data.get('target_content_rating') else ''}/>
                                            Старшая
                                        </label>
                                    </li>
                                    <li>
                                        <label>
                                            <input type="checkbox" class="toggle checkbox-uniform"
                                                   id="app_age_targeting"
                                                   name="target_content_rating"
                                                   value="0" ${'checked' if 0 in form.data.get('target_content_rating') else ''}/>
                                            Без ограничений
                                        </label>
                                    </li>
                                </ul>

                                <div class="xform-error text-danger text-center">${"<br>".join(form.target_content_rating.errors)}</div>
                            </div>
                            <div class="btn-group-vertical" style="width: 100%;">
                                <button type="button" class="btn btn-default check_all"
                                        style="font-size:12px;">Выделить
                                    все
                                </button>
                                <button type="button" class="btn btn-default uncheck_all"
                                        style="font-size:12px;">Снять
                                    выделение
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <h3>Настройки пользователя</h3>
                    <hr/>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="portlet">                                               <!------------------ PLATFORMS TARGETING  -->
                        <div class="portlet-title">
                            <div class="caption">Таргетинг: Платформы</div>
                            <div class="make-switch switch-mini pull-right" data-on="success" data-off="danger">
                                <input type="checkbox"
                                       class="bs-switch toggle"
                                       id="category_targeting"
                                       name="targeting"
                                       value="${Campaign.Targeting.PLATFORM}"
                                       data-on-color="success" data-off-color="danger" data-size="small"
                                    ${'checked' if Campaign.Targeting.PLATFORM & g.campaign.targeting or Campaign.Targeting.PLATFORM in (form.targeting.data or []) else ''}
                                        />
                            </div>
                        </div>
                        <div class="portlet-body">
                            <div class="form-group ${'has-error' if form.target_platforms.errors else ''}">
                                <!-- CAMPAIGN.PLATFORMS -->
##                                <label class="col-sm-4 control-label">Разрешенные платформы</label>

                                <div class="col-sm-12">
                                    % for (pk, pv) in platform_dict.items():
                                        <div class="checkbox"><label>
                                            <input name="target_platforms" type="checkbox"
                                                   value="${pv}" ${'checked' if pv in form.data.get('target_platforms', []) else ''}>
                                            ${pk}
                                        </label></div>
                                    % endfor
                                    <div class="xform-error text-danger text-center">${"<br>".join(form.target_platforms.errors)}</div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="portlet">
                        <div class="portlet-title">
                            <div class="caption">Таргетинг: Типы устроиств</div>
                            <div class="make-switch switch-mini pull-right" data-on="success" data-off="danger">
                                <input type="checkbox"
                                       class="bs-switch toggle"
                                       id="category_targeting"
                                       name="targeting"
                                       value="${Campaign.Targeting.DEVCLASS}"
                                       data-on-color="success" data-off-color="danger" data-size="small"
                                    ${'checked' if Campaign.Targeting.DEVCLASS & g.campaign.targeting or Campaign.Targeting.DEVCLASS in (form.targeting.data or []) else ''}
                                        />
                            </div>
                        </div>
                        <div class="portlet-body">

                            <div class="form-group ${'has-error' if form.target_devclasses.errors else ''}">
                                <!-- CAMPAIGN.DEV_CLASSES -->
##                                <label class="col-sm-4 control-label">Разрешенные типы устройств</label>

                                <div class="col-sm-12">
                                    % for (dck, dcv) in devclass_dict.items():
                                        <div class="checkbox"><label>
                                            <input name="target_devclasses" type="checkbox"
                                                   value="${dcv}" ${'checked' if dcv in form.data.get('target_devclasses', []) else ''}>
                                            ${dck}
                                        </label></div>
                                    % endfor
                                    <div class="xform-error text-danger text-center">${"<br>".join(form.target_devclasses.errors)}</div>
                                </div>
                            </div>


                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-6">
                    <div class="portlet">
                        <div class="portlet-title">
                            <div class="caption">
                                Таргетинг: Пол
                            </div>
                            <div class="pull-right">
                                <input type="checkbox"
                                       class="bs-switch toggle"
                                       id="category_targeting"
                                       name="targeting"
                                       value="${Campaign.Targeting.GENDER}"
                                       data-on-color="success" data-off-color="danger" data-size="small"
                                    ${'checked' if Campaign.Targeting.GENDER & g.campaign.targeting or Campaign.Targeting.GENDER in (form.targeting.data or []) else ''}
                                        />
                            </div>
                        </div>
                        <div class="portlet-body">
                            ${form.target_gender(class_='form-control')}
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="portlet">
                        <div class="portlet-title">
                            <div class="caption">
                                Таргетинг: Возраст
                            </div>
                            <div class="pull-right">
                                <input type="checkbox"
                                       class="bs-switch toggle"
                                       id="category_targeting"
                                       name="targeting"
                                       value="${Campaign.Targeting.AGE}"
                                       data-on-color="success" data-off-color="danger" data-size="small"
                                    ${'checked' if Campaign.Targeting.AGE & g.campaign.targeting or Campaign.Targeting.AGE in (form.targeting.data or []) else ''}
                                        />
                            </div>
                        </div>
                        <div class="portlet-body">
                            ${form.target_age(class_='form-control')}
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <div class="portlet">                                               <!------------------ GEO TARGETING  -->
                        <div class="portlet-title">
                            <div class="caption">Таргетинг: Гео</div>
                            <div class="pull-right switch" data-on="success" data-off="danger" data-size="small">
                                <input type="checkbox"
                                       class="bs-switch toggle"
                                       id="geo_targeting"
                                       name="targeting"
                                       value="${Campaign.Targeting.GEO}"
                                       data-on-color="success" data-off-color="danger" data-size="small"
                                       data-state="${'true' if Campaign.Targeting.GEO & g.campaign.targeting or Campaign.Targeting.GEO in (form.targeting.data or []) else 'false'}"
                                    ${'checked' if Campaign.Targeting.GEO & g.campaign.targeting or Campaign.Targeting.GEO in (form.targeting.data or []) else ''}
                                        />
                            </div>
                        </div>
                        <div class="portlet-body">                                                            <!-- CAMPAIGN.GEO -->
                            <div id="geo_tree"></div>
                            <div class="xform-error text-danger text-center">${"<br>".join(form.target_geo.errors)}</div>
                            <input type="hidden" name="target_geo" id="target_geo">
                            <script type="text/javascript">

                                var fancytree;
                                var selectedNodes = ${map(int, form.data.get('target_geo', []))};

                                $(function () {
                                    fancytree = $("#geo_tree").fancytree({ checkbox: true, selectMode: 3, keyboard: false, clickFolderMode: 2, icons: false, source: ${g.nodes}
                                        , select: function (event, data) {
                                            var snodes = data.tree.getSelectedNodes(true);
                                            var skeys = [];

                                            for (var i in snodes) {
                                                skeys.push(snodes[i].key);
                                            }

                                            $("#target_geo").val(skeys);
                                        }
                                    });
                                    for (i in selectedNodes) {
                                        $("#geo_tree").fancytree("getNodeByKey", selectedNodes[i]).setSelected(true);
                                    }
                                });
                            </script>
                        </div>
                    </div>
                </div>
            </div>


            <div class="row">
                <div class="col-md-12">
                    <div class="portlet">
                        <div class="portlet-title">
                            <div class="caption">
                                Расширенные настройки
                            </div>
                            <div class="pull-right">
                                <input type="checkbox"
                                       class="bs-switch toggle"
                                       id="advanced_settings"
                                       name=""
                                       data-on-color="success" data-off-color="danger" data-size="small"
                                       data-on-text="Скрыть" data-off-text="Показать"
                                       data-state="${'true' if g.campaign.target_applications else 'false'}"
                                    ${'checked' if g.campaign.target_applications else ''}
                                        />
                            </div>
                        </div>
                        <div class="portlet-body">
                            <div class="row">
                                <div class="col-md-6">
                                    <p>
                                        <strong class="text-danger">
                                            Не указывайте в этом поле ничего, если не знаете, что здесь указывать!
                                        </strong>
                                    </p>

                                    <p>
                                        ID площадок для показа рекламы
                                    </p>
                                    ${form.target_applications(class_='form-control')}
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>

            <input type="hidden" name="total_cost_limit" value="10">
        </div>


        <div class="col-md-3">
            <div class="portlet fixed">
                <div class="portlet-title">
                    <div class="caption">Биллинг</div>
                </div>
                <div class="portlet-body ">
                    <div style="margin-bottom: 10px;">
                        ${form.payment_type()}

                    </div>

                    <div class="input-group ${'has-error' if form.bid.errors else ''}">
                        <span class="input-group-addon payment">Ставка</span>
                        <input type="text" class="form-control" name="bid" value="${form.data.get('bid') or '1'}">

                    </div>
                    <div class="help text-muted text-center" style="margin:5px 0;"></div>
                    <div class="xform-error text-danger">${"<br>".join(form.bid.errors)}</div>

                    <hr style="margin:10px 0;">

                    <div id="xdata">
                        <dl class="dl-horizontal">
                            <dt>Минимальная</dt>
                            <dd id="min_bid">--</dd>
                        </dl>
                        <dl class="dl-horizontal">
                            <dt>Рекомендованая</dt>
                            <dd id="rec_bid">--</dd>
                        </dl>
                        <dl class="dl-horizontal">
                            <dt>Прогноз</dt>
                            <dd id="forecast">--</dd>
                        </dl>
                    </div>

                </div>
                <div class="form-actions fluid" style="margin-top:0">
                    <div class="text-center">
                        <button type="submit" class="btn btn-success">Сохранить</button>
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>

<script type="text/javascript">
Index = function () {
    return {
        init: function (options) {
            var self = this;   // DO NOT EDIT OR REMOVE THIS LINE
            moment.lang("ru"); // DO NOT EDIT OR REMOVE THIS LINE

            // Default options (as it was a new app)
            self.options = {
                campId: 0,
                campCategory: 0,
                campTargetCategories: [],
                campTSStart: 0,
                campTSEnd: 0,
                dateRangePickerConf: function (from, to) {
                    return {
                        opens: (App.isRTL() ? 'right' : 'left'),
                        minDate: '01.01.2012',
                        maxDate: moment().add('years', 5).format('DD.MM.YYYY'),
                        ##                            dateLimit: {
                        ##                                days: 60
                        ##                            },
                                                    showDropdowns: false,
                        showWeekNumbers: true,
                        dateRange: false,
                        timePicker: false,
                        timePickerIncrement: 1,
                        timePicker12Hour: false,
                        buttonClasses: ['btn', 'btn-small'],
                        applyClass: 'btn-success',
                        cancelClass: 'btn-default',
                        format: 'DD.MM.YYYY',
                        separator: ' to ',
                        locale: {
                            applyLabel: 'ОК',
                            cancelLabel: 'Отмена',
                            fromLabel: 'С',
                            toLabel: 'По',
                            customRangeLabel: 'Вручную',
                            daysOfWeek: ['Вс', 'Пн', 'Вт', 'Ср', 'Чт', 'Пт', 'Сб'],
                            monthNames: ['Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь', 'Июль', 'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь'],
                            firstDay: 1
                        }
                    }
                }
            };

            // Applying recieved options
            $.extend(self.options, options);

            // Init camp category select2
            self.loadCategories = function () {
                $("#category").empty();
                $.ajax({
                    url: "/adv/ajax/category",
                    data: {
                        type: 0,
                        platform: 0,
                        q: ""
                    },
                    success: function (response) {
                        $.each(response.objects, function () {
                            $("<option value='" + this.id + "'>" + this.name + "</option>").appendTo($("#category"));
                        });
                        if (self.options.campCategory) {
                            $("#category").select2("val", self.options.campCategory);
                        }
                    }
                })
            };

            self.loadCategories();

            $("#category").select2({});


            var startDatepicker,
                    endDatepicker,
                    onStartChangeDate = function (e) {
                        endDatepicker.datepicker("setStartDate", e.date);
                    },
                    onEndChangeDate = function (e) {
                        startDatepicker.datepicker("setEndDate", e.date);
                    };

            if (self.options.campTSStart != 0) {
                startDatepicker = Index.initDatepicker({
                    el: "#date-start",
                    date: moment(self.options.campTSStart)
                }).datepicker("setEndDate", moment(self.options.campTSEnd).format("l")).on("changeDate", onStartChangeDate);
            } else {
                startDatepicker = Index.initDatepicker({
                    el: "#date-start"
                }).on("changeDate", onStartChangeDate);
            }

            if (self.options.campTSEnd != 0) {
                endDatepicker = Index.initDatepicker({
                    el: "#date-end",
                    date: moment(self.options.campTSEnd)
                }).datepicker("setStartDate", moment(self.options.campTSStart).format("l")).on("changeDate", onEndChangeDate);
            } else {
                endDatepicker = Index.initDatepicker({
                    el: "#date-end"
                }).on("changeDate", onEndChangeDate);
            }


            // Loading categories
            $.ajax({
                url: "/pub/ajax/category",
                data: {
                    q: "",
                    type: 3
                },
                success: function (response) {
                    var i = 0, list;
                    $.each(response.objects, function () {
                        var checked = self.options.campTargetCategories.indexOf(this.id) >= 0 ? "checked" : "";
                        if (i == 0 || i == parseInt(response.objects.length / 2)) {
                            list = $("<div class='col-md-6'><div class='checkbox-list'></div></div>").appendTo($("#camp-target-categories")).find(".checkbox-list");
                        }
                        $("<label><input type='checkbox' class='checkbox-uniform' name='target_categories_groups' value='" + this.id + "' " + checked + ">" + this.name + "</label>").appendTo(list);
                        i++;
                    });

                    $(".checkbox-uniform").uniform();
                }
            });

            $("form").validate({
                highlight: function (el) {
                    $(el).parent().addClass("has-error");
                }
            });

            $("button[type='submit']").on("click", function (evt) {
                evt.preventDefault();
                if ($("form").valid()) {
                    $("form").submit()
                }
            });

            $(".select2-choices").addClass("form-control");

            $("#target_gender").select2();
            $("#target_age").select2();

            $("#target_applications").select2({
                tags: [],
                tokenSeparators: [',']
            });
        },
        initDatepicker: function (options) {
            var self = this;
            var defaults = {
                el: null,
                date: null,
                datepicker: {
                    language: "ru",
                    format: "dd.mm.yyyy"
                }
            };

            $.extend(true, defaults, options);

            var updateDatePlaceholders = function (date) {
                if (date) {
                    var dateString = date.format('DD.MM.YYYY');
                    $(defaults.el + " input").val(dateString);
                }
            };

            updateDatePlaceholders(defaults.date);
            return $(defaults.el + " input").datepicker(defaults.datepicker); //.data("datepicker");
        }
    }
}();

var queryForecast = function () {
    $.post("/adv/campaign/forecast",
            $("#campaign_form").serialize(),
            function (data) {
##                $("input[name='bid']").val(data.min);
                $("#min_bid").text(data.min);
                $("#rec_bid").text(data.recommended);
                $("#forecast").text(data.forecast);
            }
    );
};

$(document).on("ready", function () {
    <%
        ts_start = form.data.get('ts_start') or 0
        ts_end =form.data.get('ts_end') or 0
    %>
    Index.init({
        campId: ${g.campaign.id or 0},
        campCategory: ${form.data.get('category') or 0},
        campTargetCategories: ${form.target_categories_groups.data or []},
        campTSStart: ${int("%s000" % ts_start)},
        campTSEnd: ${int("%s000" % ts_end)}
    });

    $('.bs-switch').bootstrapSwitch({
        size: 'mini',
        onColor: 'success',
        offColor: 'danger',
        onText: 'ВКЛ',
        offText: 'ВЫКЛ',
        onInit: function(event, state) {
            if (state === undefined) {
                state = $(this).is(':checked');
            }

            if (state) {
                $(this).parents('.portlet').find('.portlet-body').show();
            } else {
                $(this).parents('.portlet').find('.portlet-body').hide();
            }
        },
        onSwitchChange: function(event, state) {
            console.debug(state);
            queryForecast();
            if (state) {
                $(this).parents('.portlet').find('.portlet-body').slideDown(500);
            } else {
                $(this).parents('.portlet').find('.portlet-body').slideUp(500);
            }
        }
    });

    $(".btn-payment_type-action").on("click", function(evt) {
        var help = $(".help");
        switch(+$(this).data("value")) {
            case 1: help.text("Цена за 1000 показов"); break;
            case 2: help.text("Цена за клик"); break;
            case 3: help.text("Цена за действие"); break;
        }
    });
    $(".btn-payment_type-action[data-value='${form.payment_type.data}']").trigger("click");

    $("#campaign_form input").on('change', queryForecast);
    queryForecast();
});

//Fixed forecast block
$(function () {
    var offset = $(".fixed").offset();
    var topPadding = 65;
    $(window).scroll(function () {
        if ($(window).scrollTop() > offset.top - 35) {
            $(".fixed").stop().animate({marginTop: $(window).scrollTop() - offset.top + topPadding}, 400, 'linear');
        }
        else {
            $(".fixed").stop().animate({marginTop: 0}, 400, 'linear');
        }
        ;
    });
});

$(".check_all").on("click", function () {
    var self = $(this);
    $.each($(self.parents()[1]).find("input[type='checkbox']"), function () {
        if (!$(this).prop("checked")) $(this).trigger("click");
    });
});

$(".uncheck_all").on("click", function () {
    var self = $(this);
    $.each($(self.parents()[1]).find("input[type='checkbox']"), function () {
        if ($(this).prop("checked")) $(this).trigger("click");
    });
});
</script>
