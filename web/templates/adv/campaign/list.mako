## -*- coding: utf-8 -*-
<%namespace name="bcrumb" file="/layout/bcrumb.mako" />
<%namespace name="fabutton" file="/layout/fa-button.mako" />
<%inherit file="/layout/main.mako" />
<%def name="title()">Кампании</%def>
<%def name="description()">Список всех ваших кампаний</%def>
<% import datetime %>
<%
    from faan.core.model.adv.campaign import Campaign
    from faan.core.model.pub.application import Application
%>

${ bcrumb.h(self) }
${ bcrumb.bc([ { 'name' : u'Кампании',  'href' : '/adv/campaign/', 'icon' : 'bullhorn' }]) }

<div class="row">                                                               <!-- BEGIN PAGE CONTENT-->
<div class="col-md-12">                 
    <div class="portlet">                                                       <!--BEGIN TABLE-->
        <div class="portlet-title">
            <div class="caption"><i class="fa fa-bullhorn"></i>Ваши кампании</div>
            <div class="actions">
                <a href="/adv/campaign/new" class="btn btn-success"><i class="fa fa-plus"></i> Добавить новую</a>
                <div class="btn-group">
                    <a class="btn btn-info dropdown-toggle" href="#" data-toggle="dropdown">
                        <i class="fa fa-cogs"></i> Действие <i class="fa fa-angle-down"></i>
                    </a>
                    <ul class="dropdown-menu pull-right">
                        <li><a href="#" id="2" class="group_actions"><i class="fa fa-pause"></i> Остановить</a></li>
                        <li><a href="#" id="1" class="group_actions"><i class="fa fa-play"></i> Активировать</a></li>
                        <li><a href="#" id="delete_campaigns"><i class="fa fa-trash-o"></i> Удалить</a></li>
                    </ul>
                </div>

            </div>
        </div>
        
        <div class="portlet-body">
            <table class="table table-striped table-bordered table-hover" id="campaigns_table">
            <thead>
            <tr>
                <th style="width:15px;" class="text-center"><input type="checkbox" class="group-checkable" data-set="#campaigns_table .checkboxes"/></th>
                <th class="text-center">ID</th>
                <th class="text-center">Название</th>
                <th class="text-center">Платформа</th>
                <th class="text-center">Медиа</th>
                <th class="text-center">Статус</th>
                <th class="text-center">Показы</th>
                <th class="text-center">Клики</th>
                <th class="text-center">CTR</th>
                <th class="text-center">Расход</th>
            </tr>
            </thead>
            <tbody>
            % for c in g.campaigns:
            <tr class="odd gradeX" id="tr_${c.id}">                                                             <!-- TODO: add actual columns -->
                <td class="text-center"><input type="checkbox" class="checkboxes" value="${c.id}"/></td>
                <td>
                    ${c.id}
                </td>
                <td class="text-left">
##                    ${fabutton.campaign(c)}
##                    <div style="font-size: 25px; font-weight: bold; float: right;">
##                        ${c.bid}<i style="font-size: 23px;" class="fa fa-1x fa-ruble"></i>
##                    </div>
##                    <div class="title" style="float: left; margin-right: 90px;">
##                        <h3 style="margin: 0 0 10px 0;" class="tooltips" data-original-title="${c.name}">
##                            <a href="/adv/campaign/${c.id}/media/">${h.limitstr(c.name, 15)}</a>
##                            <sup>
##                                <small class="text-muted">#${c.id}</small>
##                            </sup>
##                        </h3>
##
##                        <ul class="list-inline text-muted">
##                            <li>
##                                <small><i class="fa fa-tags"></i> ${g.categories_dict[c.category].name}</small>
##                            </li>
##                        </ul>
##
##                        <ul class="list-inline">
##                            <li>
##                                <i class="fa fa-fw fa-apple listfaicon tooltips ${'text-info' if Application.Os.IOS in c.target_platforms else ''}"
##                                   data-placement="top"
##                                   data-original-title="iOS"></i>
##                            </li>
##                            <li>
##                                <i class="fa fa-fw fa-android listfaicon tooltips ${'text-info' if Application.Os.ANDROID in c.target_platforms else ''}"
##                                   data-placement="top"
##                                   data-original-title="Android"></i>
##                            </li>
##                            <li>
##                                <i class="fa fa-fw fa-windows listfaicon tooltips ${'text-info' if Application.Os.WINPHONE in c.target_platforms else ''}"
##                                   data-placement="top"
##                                   data-original-title="Windows Phone"></i>
##                            </li>
##                        </ul>
##
##                        <ul class="list-inline">
##                            <li>
##                                <i class="fa fa-fw fa-mobile-phone listfaicon tooltips ${'text-info' if Campaign.DevClass.PHONE in c.target_devclasses else ''}"
##                                   data-placement="top"
##                                   data-original-title="Смартфоны"></i>
##                            </li>
##                            <li>
##                                <i class="fa fa-fw fa-tablet listfaicon tooltips ${'text-info' if Campaign.DevClass.TABLET in c.target_devclasses else ''}"
##                                   data-placement="top"
##                                   data-original-title="Планшеты"></i>
##                            </li>
##                        </ul>

##                        <ul class="list-unstyled">
##                            %if m.uri != '':
##                                <li>
##                                    <a href="${m.uri}" target="_blank" title="m.uri">
##                                        <i class="fa fa-external-link-square"
##                                           style="font-size: 13px"></i> ${h.limitstr(m.uri, 21)}
##                                    </a>
##                                </li>
##                            %endif
##
##                            %if m.type == Media.Type.VIDEO:
##                                <li>
##                                    <i class="fa fa-1x fa-fw fa-clock-o text-muted"></i> ${m.duration} сек.
##                                </li>
##                                <li>
##                                    <i class="fa fa-1x fa-fw fa-times-circle text-muted"></i> ${m.closable} сек.
##                                </li>
##                                <li>
##                                    <i class="fa fa-1x fa-fw fa-money text-muted"></i>
##                                    <%
##                                        total = 0
##                                        if m.endcard: total += .5
##                                        if m.overlay: total += .3
##                                        if m.closable: total += m.closable*.3
##                                    %>
##                                    +${total} р.
##                                </li>
##                            %endif
##                        </ul>
##                    </div>
                    <div class="row">
                        <div class="col-md-8">
                        <h4 style="margin-top:0;">
                            <span class="badge badge-success">#${c.id} </span>
                            <a href="/adv/campaign/${c.id}/media/" style="clear:both;margin-left: 5px;">
                                ${h.limitstr(c.name, 30)}
                            </a>
                        </h4>
                        <p class="text-muted" style="line-height: 8px;"><small><i class="fa fa-tags"></i> ${g.categories_dict[c.category].name}</small></p>
                        <p class="text-left">
                            %if c.state == g.campaign_states.ACTIVE:
                                ${ fabutton.fab([
                                    { 'text' : u'Остановить',  'href' : '/adv/campaign/%d/deactivate'%(c.id), 'icon' :   'pause' }
                                ])}
                            %elif c.state == g.campaign_states.DISABLED:
                               ${ fabutton.fab([
                                { 'text' : u'Запустить',  'href' : '/adv/campaign/%d/activate'%(c.id), 'icon' : 'play' }
                                    ])
                                }
                            %endif

                            <%
                                today = datetime.date.today()
                                ## monthago = datetime.date(day=today.day, month=today.month - 1, year=today.year)
                                monthago = today - datetime.timedelta(days=30)
                            %>
                            ${fabutton.fab([
                                { 'text' : u'Добавить медиа',  'href' : '/adv/campaign/%d/media/new'%(c.id), 'icon' : 'plus' },
                                { 'text' : u'Редактировать',  'href' : '/adv/campaign/%d'%(c.id), 'icon' : 'pencil' },
                                { 'text' : u'Статистика',  'href' : '/adv/stat/date?date=%s+-+%s&campaign=%d'%(unicode(monthago.strftime('%d.%m.%Y')), unicode(today.strftime('%d.%m.%Y')), c.id), 'icon' : 'bar-chart-o' },
                                { 'text' : u'Удалить',  'href' : '/adv/campaign/%d/delete'%(c.id), 'icon' : 'times' }
                            ])}
                        </p>
                        </div>
                        <div class="col-md-4 text-right">
                            <div class="text-muted text-right">
                                <i class="fa fa-apple listfaicon tooltips ${'text-info' if Application.Os.IOS in c.target_platforms else ''}" data-placement="top"
                                   data-original-title="iOS"></i>
                                <i class="fa fa-android listfaicon tooltips ${'text-info' if Application.Os.ANDROID in c.target_platforms else ''}" data-placement="top"
                                   data-original-title="Android"></i>
                                <i class="fa fa-windows listfaicon tooltips ${'text-info' if Application.Os.WINPHONE in c.target_platforms else ''}" data-placement="top"
                                   data-original-title="Windows Phone"></i>

                                <br>

                                <i class="fa fa-mobile-phone listfaicon tooltips ${'text-info' if Campaign.DevClass.PHONE in c.target_devclasses else ''}" data-placement="top"
                                   data-original-title="Смартфоны"></i>
                                <i class="fa fa-tablet listfaicon tooltips ${'text-info' if Campaign.DevClass.TABLET in c.target_devclasses else ''}" data-placement="top"
                                   data-original-title="Планшеты"></i>
                            </div>

                            <br/>

                            <p class="text-muted" style="line-height: 8px;"><small><i style="margin-top: 5px;"><img src="/global/static/img/icons/coins_l.png"></i> ${c.bid} руб.</small></p>
                        </div>
                    </div>
                </td>
                <td>
                    % if c.targeting & Campaign.Targeting.PLATFORM:
                        % for p in c.target_platforms:
                            %if p == 1:
                                ios
                            %elif p == 2:
                                android
                            %else:
                                windows
                            %endif
                        % endfor
                    %else:
                        ios android windows
                    %endif
                </td>

                <td class="text-center">
                    <span class="badge badge-success tooltips" data-placement="top" data-original-title="Активные">${c.media.get("active", 0)}</span>
                    <span class="badge badge-warning tooltips" data-placement="top" data-original-title="На модерации">${c.media.get("new", 0)}</span>
                    <span class="badge badge-danger tooltips" data-placement="top" data-original-title="Забаненные">${c.media.get("banned", 0)}</span>
                    <span class="badge badge-info tooltips" data-placement="top" data-original-title="Остановленные">${c.media.get("inactive", 0)}</span>
                </td>
                <td class="text-center campaign-status"><span class="hidden">${c.state}</span>${u"<span class='badge badge-success'>Активна</span>" if c.state == 1 else u"<span class='badge badge-info'>Остановлена</span>"} </td></td>


                <td class="text-right">${c.raw}</td>
                <td class="text-right">${c.clicks}</td>
                <td class="text-right">${"%.2f%%" % (100 * h.divbyz(c.clicks, c.impressions))}</td>
                <td class="text-right">${c.cost} руб.</td>
            </tr>
            % endfor
            </tbody>
            </table>
        </div>
    </div>              <!--END TABLE-->
 </div>
</div>
<div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog" style="width: 25%">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="myModalLabel">Удалить компании?</h4>
      </div>
      <div class="modal-body">
        <div class="pasta form-group">
            Действительно удалить кампании <span id="del_list"></span> ?

        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
        <button type="button" id="delete" class="btn btn-danger group_actions">Удалить</button>
      </div>
    </div>
  </div>
</div>


<script src="/global/static/scripts/campaign_table.js"></script>
<script>
    var $delete_modal = $('#deleteModal');
    $('.delete_campaign').on('click', function() {
        var cid = $(this).data('cid');
        var cname = $(this).data('cname');
        $('#delete').one('click', function() {
            window.location = '/adv/campaign/' + cid + '/delete';
        });
        if (cid && cname) {
            $delete_modal.modal();
            $('#del_list').html('').html(cname + '<sup>' + cid + '</sup>');
        }
        console.debug('# Deleting campaign', $(this).data('cid'));
    });
    $('#delete_campaigns').on('click',function(){
        var objects = $('.checkboxes:checked');
        var objects_ids = '';
        if (objects.length > 0){
                $delete_modal.modal();
                for (var i=0; i < objects.length; i++){
                    objects_ids += objects[i].value + ', ';
            }
            $('#del_list').empty().html(objects_ids.slice(0, -1));
        }
   });

   $('.group_actions').on('click', function(){
    var objects = $('.checkboxes:checked');
    var objects_ids = '';
    if (objects.length > 0){
        for (var i=0; i < objects.length; i++){
                objects_ids += objects[i].value + ',';
        }
        $.post('/adv/campaign/group_actions',{objects_list:objects_ids, action:this.id}, after_load);
    }
});
function after_load(data){
        if (data.error  == 0){
            var action_html = '';
            var object_str = JSON.stringify(data.campaign_list).replace('[','').replace(']','');
            if (data.action == 1){
                action_html = 'Активна';
                toastr.success('ID: '+ object_str , 'кампании запущены' );
            }
            else if (data.action == 2){
                action_html = 'Остановлена';
                toastr.success('ID: '+ object_str , 'кампании остановлены ');
            }
            else if(data.action == 'delete'){
                for(var i = 0; i < data.campaign_list.length; i++){
                    $('#tr_'+data.campaign_list[i]).remove();
                }
                $delete_modal.modal('hide');
                toastr.success('ID: ' + object_str, 'Удалены media');
                return;
            }

            for(var i = 0; i < data.campaign_list.length; i++){
                 if (action_html == 'Активна')
                    $('#tr_'+data.campaign_list[i]+'  .campaign-status .badge').removeClass('badge-info').addClass('badge-success').html(action_html);
                else 
                    $('#tr_'+data.campaign_list[i]+' .campaign-status .badge').removeClass('badge-success').addClass('badge-info').html(action_html);
            }
        }
}
TableManaged.init();
</script>
<!-- END PAGE CONTENT-->