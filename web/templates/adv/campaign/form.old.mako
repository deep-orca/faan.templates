## -*- coding: utf-8 -*-
<%namespace name="bcrumb" file="/layout/bcrumb.mako" />
<%inherit file="/layout/main.mako" />

<%def name="title()">
% if g.campaign.id:
Кампания [${g.campaign.id}] ${g.campaign.name}
% else:
Новая кампания
% endif
</%def>
<%def name="description()">Форма редактирования</%def>

${ bcrumb.h(self) }
${ bcrumb.bc([ { 'name' : u'Кампании', 	'href' : '/adv/campaign/', 'icon' : 'rocket' }
			 , { 'name' : u'Список', 	'href' : '/adv/campaign/' }
			 , { 'name' : u"[%s] %s" % (g.campaign.id, g.campaign.name) if g.campaign.id else u'Новая кампания' }
			 ]) }

<%

from faan.core.model.adv.campaign import Campaign
from faan.core.model.pub.application import Application

platform_dict = { u'iOS' 	  : Application.Os.IOS
 				, u'Android'  : Application.Os.ANDROID
 				, u'WinPhone' : Application.Os.WINPHONE
 				}
 				
devclass_dict = { u'Телефон'  : Campaign.DevClass.PHONE
 				, u'Планшет'  : Campaign.DevClass.TABLET
 				}
 				
 				

%>

<!-- BEGIN PAGE CONTENT-->
<div class="row">
	<div class="col-md-12">

	<!--BEGIN TABLE-->
    <div class="portlet">
        <div class="portlet-title"><div class="caption"><i class="fa fa-rocket"></i>${self.title()}</div></div>
        
        <div class="portlet-body">
        	<form method="POST" class="form-horizontal">

        		<div class="form-group">													<!-- CAMPAIGN.NAME -->
    				<label for="campaignName" class="col-sm-2 control-label">Имя</label>
    				<div class="col-sm-10"><input name="name" type="text" class="form-control" id="campaignName" placeholder="Имя" value="${g.campaign.name}"></div>
  				</div>

        		<div class="form-group">													<!-- CAMPAIGN.CATEGORY -->
    				<label for="campaignCategory" class="col-sm-2 control-label">Категория</label>
    				<div class="col-sm-10">
    					<select name="category" id="campaignCategory" class="form-control">	<!-- TODO: use helping generators here -->
    						% for c in g.categories:
    						<option value="${c.id}" ${"selected" if c.id == g.campaign.category else ""}>${c.name}</option>
    						% endfor
						</select>
    				</div>
  				</div>
							
        		<div class="form-group">													<!-- CAMPAIGN.target_categories -->
    				<label for="campaignCategories" class="col-sm-2 control-label">Разрешенные категории</label>
    				<div class="col-sm-10">
    					<select name="target_categories" id="campaignCategories" class="form-control" multiple>	<!-- TODO: use helping generators here -->
    						% for c in g.categories:
    						<option value="${c.id}" ${"selected" if c.id in g.campaign.target_categories else ""}>${c.name}</option>
    						% endfor
						</select>
    				</div>
  				</div>							

        		<div class="form-group">													<!-- CAMPAIGN.PLATFORMS -->
    				<label class="col-sm-2 control-label">Разрешенные платформы</label>
    				<div class="col-sm-10">
    				% for (pk, pv) in platform_dict.items():
    					<div class="checkbox"><label>
    						<input name="platforms" type="checkbox" value="${pv}" ${'checked' if pv in g.campaign.platforms else ''}>
    						${pk}
    					</label></div>
    				% endfor 
    				</div>
  				</div>							

        		<div class="form-group">													<!-- CAMPAIGN.DEV_CLASSES -->
    				<label class="col-sm-2 control-label">Разрешенные типы устройств</label>
    				<div class="col-sm-10">
    				% for (dck, dcv) in devclass_dict.items():
    					<div class="checkbox"><label>
    						<input name="dev_classes" type="checkbox" value="${dcv}" ${'checked' if dcv in g.campaign.dev_classes else ''}>
    						${dck}
    					</label></div>
    				% endfor 
    				</div>
  				</div>							
																							
				<!-- TODO: add actual fields -->
				<input type="hidden" name="total_cost_limit" value="10">
				
				<div class="form-group">													<!-- ==== SUBMIT ==== --->
    				<div class="col-sm-offset-2 col-sm-10">
      					<button type="submit" class="btn btn-default">Сохранить</button>
    				</div>
  				</div>
        	</form>
        </div>
    </div>
 <!--END TABLE-->
</div>
</div>
<!-- END PAGE CONTENT-->