## -*- coding: utf-8 -*-
<%namespace name="bcrumb" file="/layout/bcrumb.mako" />
<%namespace name="balance" file="/layout/balance.mako" />
<%inherit file="/layout/main.mako" />
<%def name="title()">Баланс</%def>
<%def name="description()">Отображение и пополнение баланса рекламодателя</%def>
<% import datetime %>
<%def name="page_css()">
    <link href="/global/static/css/pages/balance.css" rel="stylesheet" type="text/css"/>
</%def>

${ bcrumb.h(self) }
${ bcrumb.bc(
[
    { 'name': u'Рекламодатель', 'href': '/adv', 'icon' :'bullhorn' },
    { 'name': u'Финансы', 'href': '/adv/balance/', 'icon' :'ruble' },
    { 'name': u'Пополнение баланса', 'href' : '/adv/balance/', 'icon' : 'plus-square' }
]
)}

<%
    hostname = request.environ['SERVER_NAME'] + (":" + request.environ['SERVER_PORT'] if request.environ['SERVER_PORT'] != '80' else "" )
%>

<!--PAGE CONTENT-->
<div class="row">
    <div class="col-md-8">
        <div class="portlet">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-ruble"></i>Пополнить баланс
                </div>
            </div>
            <div class="portlet-body">
                <div class="tabbable tabs-left">
                    <ul class="nav nav-tabs">
                        <li class="active">
                            <a href="#tab_wm" data-toggle="tab">
                                <img src="/global/static/img/balance/wm.png" width="150px" alt="Webmoney">
                            </a>
                        </li>
                        <li class="">
                            <a href="#tab_yd" data-toggle="tab">
                                <img src="/global/static/img/balance/ym.png" width="130px" alt="Яндекс.Деньги">
                            </a>
                        </li>
                        <li>
                            <a href="#tab_qiwi" data-toggle="tab">
                                <img src="/global/static/img/balance/qiwi.png" width="130px" alt="QIWI кошелек">
                            </a>
                        </li>
                        <li>
                            <a href="#tab_pp" data-toggle="tab">
                                <img src="/global/static/img/balance/pp.png" width="130px" alt="Paypal">
                            </a>
                        </li>
                        <li>
                            <a href="#tab_card" data-toggle="tab">
                                <img src="/global/static/img/balance/cards.png" width="130px" alt="Кредитная карта">
                            </a>
                        </li>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane active" id="tab_wm">
                            ${ balance.ps({'name': u'Webmoney', 'href': 'http://webmoney.ru'})}
                        </div>
                        <div class="tab-pane fade" id="tab_yd">
                            ${ balance.ps({'name': u'Яндекс.Деньги', 'href': 'http://money.yandex.ru'})}
                        </div>
                        <div class="tab-pane fade" id="tab_qiwi">
                            ${ balance.ps({'name': u'QIWI', 'href': 'http://qiwi.ru'})}
                        </div>
                        <div class="tab-pane fade" id="tab_pp">
                            ${ balance.ps({'name': u'PayPal', 'href': 'http://paypal.com'})}
                        </div>
                        <div class="tab-pane fade" id="tab_card">
                            ${ balance.ps({'name': u'кредитной карты', 'href': 'http://robokassa.ru'})}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-4">
        <div class="portlet sale-summary">
            <div class="portlet-title">
                <div class="caption">
                    Баланс
                </div>
            </div>
            <div class="portlet-body">
                <ul class="list-unstyled">
                    <li>
                        <span class="sale-info">
                             Текущий баланс
                        </span>
                        <span class="sale-num" id="bal-amount">
                             <!-- 12 358 -->
                            ${g.account.balance} <i class="fa fa-rub"></i>
                        </span>
                    </li>
                    <li class="deposit-quickview">
                        <div style="overflow: hidden;">
                            <span style="float: right;">
                                 <a href="#"
                                    data-limetoggle-state="hidden"
                                    data-limetoggle-label-show="Посмотреть"
                                    data-limetoggle-label-hide="Скрыть"
                                    data-limetoggle-bind=".deposit-quickview ul">Посмотреть</a>
                            </span>
                            <span class="sale-info">
                                 История пополнений
                            </span>

                        </div>
                        <div>
                            <ul class="list-group" data-type="0">
                                ##                                <li class="list-group-item">
##                                    12.05.2014 - Yandex.Money  <span class="badge badge-success">+200</span>
##                                </li>
                            </ul>
                        </div>
                    </li>
                    <li class="withdraw-quickview">
                        <div style="overflow: hidden;">
                            <span style="float: right;">
                                 <a href="#"
                                    data-limetoggle-state="hidden"
                                    data-limetoggle-label-show="Посмотреть"
                                    data-limetoggle-label-hide="Скрыть"
                                    data-limetoggle-bind=".withdraw-quickview ul">Посмотреть</a>
                            </span>
                            <span class="sale-info">
                                 История списаний
                            </span>
                        </div>

                        <div>
                            <ul class="list-group" data-type="1">
                                ##                                <li class="list-group-item">
##                                    12.05.2014 - Yandex.Money  <span class="badge badge-danger">-200</span>
##                                </li>
                            </ul>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>

<%def name="page_js()">
    <script type="text/javascript" src="/global/static/plugins/moment-with-langs.min.js"></script>
    <script src="/global/static/plugins/jquery.validate.min.js" type="text/javascript"></script>
    <script type="text/javascript" src="/global/static/plugins/jquery-validation/localization/messages_ru.js"></script>
    <script type="text/javascript" src="/global/static/plugins/toolbox/lime.templated.js"></script>
    <script type="text/javascript" src="/global/static/plugins/toolbox/lime.toggle.js"></script>
    ##    <script type="text/javascript" src="/global/static/plugins/toolbox/lime.templated.js"></script>

    <script type="text/javascript" src="/global/static/scripts/adv_balance.js"></script>
</%def>

