    ## -*- coding: utf-8 -*-
<%namespace name="bcrumb" file="/layout/bcrumb.mako" />
<%inherit file="/layout/main.mako" />
<%def name="title()">Media</%def>
<%def name="description()">Media кампании [${g.campaign.id}] ${g.campaign.name}</%def>

${ bcrumb.h(self) }
${ bcrumb.bc([ { 'name' : u'Кампании', 	'href' : '/adv/campaign/', 'icon' : 'rocket' }
			 , { 'name' : u"[%s] %s" % (g.campaign.id, g.campaign.name), 'href' : '/adv/campaign/%s/media' % (g.campaign.id) }
             , { 'name' : u'%s' % (g.media.name) if g.media.id else u"Новое медиа"}
			 ]) }

<%
hostname = request.environ['SERVER_NAME'] + (":" + request.environ['SERVER_PORT'] if request.environ['SERVER_PORT'] != '80' else "" )
%>
<%def name="page_css()">
    <link href="/global/static/plugins/ion.rangeslider/css/ion.rangeSlider.css" rel="stylesheet" type="text/css"/>
    <link href="/global/static/plugins/ion.rangeslider/css/ion.rangeSlider.Conquer.css" rel="stylesheet" type="text/css"/>
    <link rel="stylesheet" type="text/css" href="/global/static/plugins/bootstrap-fileupload/bootstrap-fileupload.css"/>
    <style>
        .xform-error {
            padding: 3px;
            font-weight: bold !important;
        }

        ## .btn-squared {
        ##     height: 100px;
        ##     width: 100px;
        ## }

        .form-group .btn-group > .btn {
            height: 60px;
        }

        .jwplayer {
            margin: 0 auto !important;
        }
        </style>
</%def>

<%def name="page_js()">
    ## <script src="http://jwpsrv.com/library/MQkkJAm8EeO9QiIACusDuQ.js"></script>
        ​
    <script src="/global/static/plugins/jwplayer/jwplayer.js"></script>
    <script>jwplayer.key = "BNMMP5zePmaUScqhl2go8j3xvQ34ZUTwrG1xTA==";</script>
    <script src="/global/static/plugins/ion.rangeslider/js/ion-rangeSlider/ion.rangeSlider.min.js"></script>
    <script>
        var mediaTemp = ${g.media_json};
        if (mediaTemp.objects.length > 0) {
            media = mediaTemp.objects[0];
        } else {
            media = null;
        }
    </script>
</%def>

<!-- BEGIN PAGE CONTENT-->
<div class="row" ng-controller="MediaController as mediaCtrl">
    <div class="col-md-8">
        <div class="portlet">
            <div class="portlet-title">
                <div class="caption">Новый баннер</div>
            </div>
            <div class="portlet-body">

            </div>
        </div>
    </div>
    <div class="col-md-4">
        <div class="portlet">
            <div class="portlet-title">
                <div class="caption">{{'caption'}}</div>
            </div>
            <div class="portlet-body">

            </div>
        </div>
    </div>
</div>

##	<div class="col-md-12">
##        <div class="portlet">
##            <div class="portlet-title">
##                <div class="caption">
##                    <i class="fa fa-video-camera"></i>
##                    % if g.media.id:
##                    [${g.media.id}] ${g.media.name}
##                    % else:
##                    Новое media
##                    % endif
##                </div>
##            </div>
##            <div class="portlet-body">
##                <div class="row">
##                <form method="POST" class="form-horizontal" id="add_media_form">
##                ${form.csrf_token}
##                <div class="panel-group accordion col-md-8" id="media_accordion">
##                    <div class="panel panel-default">
##                        <div class="panel-heading">
##                            <h4 class="panel-title">
##                            <a class="accordion-toggle" data-toggle="collapse" data-parent="#media_accordion" href="#collapse_1">
##                            1. Название и ссылка </a>
##                            </h4>
##                        </div>
##                        <div id="collapse_1" class="panel-collapse in">
##                            <div class="panel-body">
##                                <div class="form-group">
##                                    <label for="mediaName" class="col-md-2 control-label">Название</label>
##                                    <div class="col-md-5">
##                                        <input name="name" type="text" class="form-control" id="mediaName" placeholder="Название" value="${form.data.get('name')}">
##                                        <div style="display: none;" class="xform-error">Это обязательное поле</div>
##                                    </div>
##                                </div>
##                                <div class="form-group">
##                                    <label for="mediaURI" class="col-md-2 control-label">Ссылка</label>
##                                    <div class="col-md-10">
##                                        <input name="uri" type="text" class="form-control" id="mediaURI" placeholder="Landing URI" value="${form.data.get('uri')}">
##                                        <div style="display: none;" class="xform-error">Это обязательное поле</div>
##                                    </div>
##                                </div>
##                                % if not g.media.id:
##                                <div class="form-group">
##                                    <div class="col-md-8">
##                                        <a class="btn btn-success but1" href="javascript:void(0)">Далее</a>
##                                    </div>
##                                </div>
##                                %endif
##                            </div>
##                        </div>
##                    </div>
##                    <div class="panel panel-default">
##                        <div class="panel-heading">
##                            <h4 class="panel-title">
##                            <a class="accordion-toggle" data-toggle="collapse" data-parent="#media_accordion" href="#collapse_2">
##                            2. Видео </a>
##                            </h4>
##                        </div>
##                        <div id="collapse_2" class="panel-collapse collapse ${u'in' if g.media.id else ''}">
##                            <div class="panel-body">
##                                %if g.media.video:
##                                <div class="form-group">                                                    <!-- MEDIA.VIDEO -->
##                                    <label class="col-md-2 control-label">Текущее Видео</label>
##                                    <div class="col-md-4">
##                                        <div class="thumbnail ma" style="width: 330px; margin-bottom: 0;">
##                                            <div id="preview" style="float:right"></div>
##                                            <script type="text/javascript">
##                                                jwplayer("preview").setup({
##                                                    file: "${form.data.get('video')}.mp4",
##                                                    height: 180,
##                                                    skin: 'bekle',
##                                                    width: 320
##                                                });
##                                            </script>
##                                        </div>
##                                    </div>
##                                </div>
##                                %endif
##                                <div class="form-group">
##                                    <label class="col-sm-2 control-label">Новое видео</label>
##                                    <div class="col-md-4">
##                                        <div class="thumbnail vidthumb" style="width: 330px; display: none;">
##                                            <div id="preview"></div>
##                                        </div>
##                                        <div class="fileupload fileupload-new" data-provides="fileupload">
##                                            <span class="btn btn-default btn-file">
##                                                <span class="fileupload-new">
##                                                    Выберите файл
##                                                </span>
##                                                <span class="fileupload-exists">
##                                                    <i class="fa fa-undo"></i> Изменить
##                                                </span>
##                                                <input type="file" class="default" name="data-video" id="data-video"/>
##                                            </span>
##
##                                            <a href="javascript:void(0)" class="close fileupload-exists" data-dismiss="fileupload" style="float: none; margin-left:5px;"></a>
##                                            <div class="progress"></div>
##                                        </div>
##                                        <div style="display: none;" class="xform-error xv">Это обязательное поле</div>
##                                    </div>
##                                </div>
##                                % if not g.media.id:
##                                 <div class="form-group">
##                                    <div class="col-md-8">
##                                        <a class="btn btn-info butb1" href="javascript:void(0)">Назад</a>
##                                        <a class="btn btn-success but2" href="javascript:void(0)">Далее</a>
##                                    </div>
##                                </div>
##                                % endif
##                            </div>
##                        </div>
##                    </div>
##                    <div class="panel panel-default">
##                        <div class="panel-heading">
##                            <h4 class="panel-title">
##                            <a class="accordion-toggle" data-toggle="collapse" data-parent="#media_accordion" href="#collapse_3">
##                            3. Лендинг </a>
##                            </h4>
##                        </div>
##                        <div id="collapse_3" class="panel-collapse collapse ${u'in' if g.media.id else ''}">
##                            <div class="panel-body">
##                                <div class="form-group">                                                    <!-- MEDIA.ENDCARD -->
##                                    <label class="col-md-2 control-label">Текущий EndCard</label>
##                                    <div class="col-md-6">
##                                        <div class="thumbnail" style="width: 330px;">
##                                            <img id="new_endcard" src="http://${hostname}${form.data.get('endcard') or '/global/static/img/default-endcard.png'}" style="max-width: 320px; max-height: 240px;">
##                                        </div>
##                                    </div>
##                                </div>
##
##                                <div class="form-group">
##                                    <label class="col-sm-2 control-label">Изменить EndCard</label>
##                                    <div class="col-md-4">
##                                        <div class="fileupload fileupload-new" data-provides="fileupload">
##                                            <span class="btn btn-default btn-file">
##                                                <span class="fileupload-new">
##                                                    Выберите файл
##                                                </span>
##                                                <span class="fileupload-exists">
##                                                    <i class="fa fa-undo"></i> Изменить
##                                                </span>
##                                                <input type="file" class="default" name="data-endcard" id="data-endcard"/>
##                                            </span>
##                                            <span class="fileupload-preview" style="margin-left:5px;">
##                                            </span>
##                                            <a href="javascript:void(0)" class="close fileupload-exists" data-dismiss="fileupload" style="float: none; margin-left:5px;"></a>
##                                            <div class="progress"></div>
##                                        </div>
##                                    </div>
##                                </div>
##                                % if not g.media.id:
##                                <div class="form-group">
##                                    <div class="col-md-8">
##                                        <a class="btn btn-info butb2" href="javascript:void(0)">Назад</a>
##                                        <a class="btn btn-success but3" href="javascript:void(0)">Далее</a>
##                                    </div>
##                                </div>
##                                % endif
##                            </div>
##                        </div>
##                    </div>
##                    <div class="panel panel-default">
##                        <div class="panel-heading">
##                            <h4 class="panel-title">
##                            <a class="accordion-toggle" data-toggle="collapse" data-parent="#media_accordion" href="#collapse_4">
##                            4. Оверлей </a>
##                            </h4>
##                        </div>
##                        <div id="collapse_4" class="panel-collapse collapse ${u'in' if g.media.id else ''}">
##                            <div class="panel-body">
##
##                                <div class="form-group olay" style="display: ${u'block' if form.data.get('overlay') else u'none'};">                                                    <!-- MEDIA.OVERLAY -->
##                                    <label class="col-md-2 control-label">Текущий Overlay</label>
##                                    <div class="col-md-3">
##                                        <div class="thumbnail" style="width:320px">
##                                            <img id="olay" src="http://${hostname}${form.data.get('overlay')}" style="max-width: 320px; max-height: 140px; border: 1px solid gray;">
##                                        </div>
##                                    </div>
##                                </div>
##
##
##                                <div class="form-group">
##                                    <label class="col-sm-2 control-label">Новый Overlay</label>
##
##                                    <div class="col-md-4">
##                                        <div class="fileupload fileupload-new" data-provides="fileupload">
##                                            <span class="btn btn-default btn-file">
##                                                <span class="fileupload-new">
##                                                    Выберите файл
##                                                </span>
##                                                <span class="fileupload-exists">
##                                                    <i class="fa fa-undo"></i> Изменить
##                                                </span>
##                                                <input type="file" class="default" name="data-overlay" id="data-overlay"/>
##                                            </span>
##                                            <span class="fileupload-preview" style="margin-left:5px;">
##                                            </span>
##                                            <a href="javascript:void(0)" class="close fileupload-exists" data-dismiss="fileupload" style="float: none; margin-left:5px;"></a>
##                                            <div class="progress"></div>
##                                        </div>
##                                    </div>
##
##
##                                </div>
##                                % if not g.media.id:
##                                <div class="form-group">
##                                    <div class="col-md-8">
##                                        <a class="btn btn-info butb3" href="javascript:void(0)">Назад</a>
##                                        <a class="btn btn-success but4" href="javascript:void(0)">Далее</a>
##                                    </div>
##                                </div>
##                                % endif
##                            </div>
##                        </div>
##                    </div>
##                    <div class="panel panel-default">
##                        <div class="panel-heading">
##                            <h4 class="panel-title">
##                            <a class="accordion-toggle a5" data-toggle="collapse" data-parent="#media_accordion" href="#collapse_5">
##                            5. Время до закрытия </a>
##                            </h4>
##                        </div>
##                        <div id="collapse_5" class="panel-collapse collapse ${u'in' if g.media.id else ''}">
##                            <div class="panel-body">
##                                <div class="form-group">
##                                    <label class="col-md-4 control-label">Время до закрытия</label>
##                                    <div class="col-md-6">
##                                       <input id="range_close" type="text" name="range_close" value=""/>
##                                       <span class="help-block">
##                                        Время, после которого у пользователя появится возможность пропустить видео
##                                        </span>
##                                    </div>
##
##                                </div>
##                                % if not g.media.id:
##                                <div class="form-group">
##                                    <div class="col-md-8">
##                                        <a class="btn btn-info butb4" href="javascript:void(0)">Назад</a>
##                                        <a class="btn btn-success but5" href="javascript:void(0)">Далее</a>
##                                    </div>
##                                </div>
##                                % else:
##                                    <script type="text/javascript">
##                                    $( document ).ready(function() {
##                                        ioninit(${form.data.get('closable')});
##                                    });
##                                    </script>
##                                % endif
##                            </div>
##                        </div>
##                    </div>
##
##                    <div class="panel panel-default">
##                        <div class="panel-heading">
##                            <h4 class="panel-title">
##                            <a class="accordion-toggle a6" data-toggle="collapse" data-parent="#media_accordion" href="#collapse_6">
##                            6. Ограничения </a>
##                            </h4>
##                        </div>
##                        <div id="collapse_6" class="panel-collapse collapse ${u'in' if g.media.id else ''}">
##                            <div class="panel-body">
##                                <div class="form-group">
##                                    <label class="col-md-4 control-label">Показов на пользователя за сессию</label>
##                                    <div class="col-md-8">
##                                        <div id="spinner1">
##                                            <div class="input-group input-small">
##                                                <input type="text" class="spinner-input form-control" maxlength="3" id="session_limit" name="session_limit" value="${form.data.get('session_limit')}">
##                                                <div class="spinner-buttons input-group-btn btn-group-vertical">
##                                                    <button type="button" class="btn spinner-up btn-xs btn-info">
##                                                    <i class="fa fa-angle-up"></i>
##                                                    </button>
##                                                    <button type="button" class="btn spinner-down btn-xs btn-info">
##                                                    <i class="fa fa-angle-down"></i>
##                                                    </button>
##                                                </div>
##                                            </div>
##                                        </div>
##                                        <span class="help-block">
##                                             Сколько раз видео может быть показано пользователю за один запуск приложения. <br>
##                                             0 - без ограничений.
##                                        </span>
##                                    </div>
##
##                                </div>
##                                <div class="form-group">
##                                    <label class="col-md-4 control-label">Показов на пользователя в сутки</label>
##                                    <div class="col-md-8">
##                                        <div id="spinner1">
##                                            <div class="input-group input-small">
##                                                <input type="text" class="spinner-input form-control" maxlength="3" id="daily_limit" name="daily_limit" value="${form.data.get('daily_limit')}">
##                                                <div class="spinner-buttons input-group-btn btn-group-vertical">
##                                                    <button type="button" class="btn spinner-up btn-xs btn-info">
##                                                    <i class="fa fa-angle-up"></i>
##                                                    </button>
##                                                    <button type="button" class="btn spinner-down btn-xs btn-info">
##                                                    <i class="fa fa-angle-down"></i>
##                                                    </button>
##                                                </div>
##                                            </div>
##                                        </div>
##                                        <span class="help-block">
##                                             Сколько раз видео может быть показано пользователю всего за сутки. <br>
##                                             0 - без ограничений.
##                                        </span>
##                                    </div>
##
##                                </div>
##                                <input type="hidden" name="id"   id="id"   value="${g.media.id}">
##                                <input type="hidden" name="video"   id="video"   value="">
##                                <input type="hidden" name="endcard" id="endcard" value="">
##                                <input type="hidden" name="overlay" id="overlay" value="">
##                                <input type="hidden" name="closable" id="closable" value="${form.data.get('closable')}">
##                                <input type="hidden" name="cost" id="cost" value="${g.media.cost}">
##                                <div class="form-group col-md-12 text-center">
##                                    % if not g.media.id:
##                                    <a class="btn btn-info butb5" href="javascript:void(0)">Назад</a>
##                                    % endif
##                                    <button type="button" class="btn btn-lg btn-primary" id="save_form_btn">Сохранить</button>
##                                </div>
##                            </div>
##                        </div>
##                    </div>
##                </div>
##                <div class="col-md-4">
##                    <div class="portlet fixed">
##                        <div class="portlet-title">
##                            <div class="caption">
##                                <i style="margin-top: 0;"><img src="/global/static/img/icons/coins.png"></i>
##                                Наценка
##                            </div>
##                        </div>
##                        <div class="portlet-body">
##                            <p>Цена за просмотр ролика <span id="campaign_cost">${g.campaign.bid}</span> руб.</p>
##                            <ul>
##                                <li>Лендинг + <span id="endcard_cost">${u"0.50" if form.data.get('endcard') else u"0.0"}</span> руб.</li>
##                                <li>Оверлей + <span id="overlay_cost">${u"0.30" if form.data.get('overlay') else u"0.0"}</span> руб.</li>
##                                <li>Закрытие + <span id="closable_cost">${form.data.get('closable', 0)*.3}</span> руб.</li>
##                            </ul>
##                            <h5>Итого: <span class="label label-info total_cost">
##                                <%
##                                total = 0
##                                if g.media.endcard: total += .5
##                                if g.media.overlay: total += .3
##                                if g.media.closable: total += g.media.closable*.3
##                                %>
##                                ${total} руб.</span> <i class="fa fa-asterisk tooltips text-danger" data-placement="top" data-original-title="При полном просмотре рекламного видео и лендинга после него."></i>
##                            </h5>
##                        </div>
##                    </div>
##                </div>
##            </form>
##        </div>
##            </div>
##        </div>
##
##</div>




<script type="text/javascript" src="/global/static/plugins/jquery-file-upload/js/jquery.fileupload.js"></script>
<script type="text/javascript" src="/global/static/plugins/jquery-file-upload/js/vendor/jquery.ui.widget.js"></script>
<script type="text/javascript" src="/global/static/plugins/jquery-file-upload/js/jquery.iframe-transport.js"></script>
<script type="text/javascript" src="/global/static/plugins/fuelux/js/spinner.min.js"></script>


<script type="text/javascript" src="/global/static/scripts/media_form.js"></script>
<!-- END PAGE CONTENT-->