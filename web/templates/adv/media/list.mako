## -*- coding: utf-8 -*-
<%namespace name="bcrumb" file="/layout/bcrumb.mako" />
<%namespace name="fabutton" file="/layout/fa-button.mako" />
<%inherit file="/layout/main.mako" />
<%def name="title()">Media</%def>
<%def name="description()">Media кампании [${g.campaign.id}] ${g.campaign.name}</%def>
<% import datetime %>
<%!
    import requests
    from faan.core.model.adv.media import Media
    from faan.core.X.helpers.thumbnails import thumbnail
%>


${ bcrumb.h(self) }
${ bcrumb.bc([ { 'name' : u'Кампании',  'href' : '/adv/campaign/', 'icon' : 'bullhorn' }
             , { 'name' : u"[%s] %s" % (g.campaign.id, g.campaign.name), 'href' : '/adv/campaign/%s' % (g.campaign.id) }
             , { 'name' : u'Список медиа',         'href' : '/adv/campaign/%s/media' % (g.campaign.id) }
             ]) }
<%
hostname = request.environ['SERVER_NAME'] + (":" + request.environ['SERVER_PORT'] if request.environ['SERVER_PORT'] != '80' else "" )
%>

<%def name="page_css()">
    <link rel="stylesheet" href="/global/static/plugins/fancybox/source/jquery.fancybox.css" />
    <style>
        .playbutton{
            font-size: 36px;
            position: absolute;
            margin-top: -60px;
            margin-left: -15px;
            color: #5AB5FD;
        }
        .playbutton:hover{
            color:#fff;
        }

        ##.text-muted:hover{
        ##    color: #4889BD;
        ##}

        .name-wrapper {
            width: 100px;
            overflow: hidden;
            float: left;
        }


        .vthumb {
            margin: 0 auto !important;
        }
    </style>
    <link href="//vjs.zencdn.net/4.9/video-js.css" rel="stylesheet">
</%def>

<%def name="page_js()">
    ##<script src="http://jwpsrv.com/library/MQkkJAm8EeO9QiIACusDuQ.js"></script>
    <script src="/global/static/plugins/fancybox/source/jquery.fancybox.pack.js"></script>
    <script src="//vjs.zencdn.net/4.9/video.js"></script>
</%def>


<!-- BEGIN PAGE CONTENT-->
<div class="row">
    <div class="col-md-12">
    <div class="portlet">
        <div class="portlet-title">
            <div class="caption"><i class="fa fa-video-camera"></i>Media кампании [${g.campaign.id}] ${g.campaign.name}</div>
            <div class="actions">
                <a href="/adv/campaign/${g.campaign.id}/media/new" class="btn btn-success"><i class="fa fa-plus"></i> Добавить новое</a>
                <div class="btn-group">
                    <a class="btn btn-info dropdown-toggle" href="#" data-toggle="dropdown">
                        <i class="fa fa-cogs"></i> Действие <i class="fa fa-angle-down"></i>
                    </a>
                <ul class="dropdown-menu pull-right">
                    <li><a href="#" id="2" class="group_actions"><i class="fa fa-pause"></i> Остановить</a></li>
                    <li><a href="#" id="1" class="group_actions"><i class="fa fa-play"></i> Активировать</a></li>
                    <li><a href="#" id="eurl_link"><i class="fa fa fa-pencil"></i> Изменить URI</a></li>
                    <li><a href="#" id="delete_media"><i class="fa fa-trash-o"></i> Удалить</a></li>
                </ul>
                </div>
            </div>
        </div>

        <div class="portlet-body">
            <table class="table table-striped table-bordered table-hover" id="media_table">
            <thead>
            <tr>
                <th style="width:10px;">
                    <input type="checkbox" class="group-checkable" data-set="#media_table .checkboxes"/>
                </th>
                <th class="text-center">Название</th>
                <th class="text-center">Тип</th>
                <th class="text-center">Статус</th>
                <th class="text-center">Предпросмотр</th>
##                <th class="text-center">Показы</th>
##                <th class="text-center">Клики</th>
##                <th class="text-center">CTR</th>
##                <th class="text-center">Деньги</th>
            </tr>
            </thead>
            <tbody>
            % for m in g.medias:
            <tr class="odd gradeX" id="tr_${m.id}">
                <!-- Checkbox -->
                <td>
                    <input type="checkbox" class="checkboxes" value="${m.id}"/>
                </td>

                <!-- Name -->
                <td class="text-left">
##                    ${fabutton.media(m)}
##                    <div class="title" style="float: left;">
##                        <h3 style="margin: 0 0 10px 0;" class="tooltips" data-original-title="${m.name}">
##                            <a href="/adv/campaign/${m.campaign}/media/${m.id}">${h.limitstr(m.name, 25)}</a>
##                            <sup>
##                                <small class="text-muted">#${m.id}</small>
##                            </sup>
##                        </h3>
##
##                        <ul class="list-unstyled">
####                            %if m.uri != '':
##                                <li>
##                                    <i class="fa fa-1x fa-fw fa-external-link-square text-muted" style="font-size: 13px"></i>
##                                    <a href="${m.uri}" target="_blank" title="m.uri">
##                                        ${h.limitstr(m.uri, 51)}
##                                    </a>
##                                </li>
####                            %endif
##
####                            %if m.type == Media.Type.VIDEO:
##                                <li>
##                                    <i class="fa fa-1x fa-fw fa-clock-o text-muted"></i> ${m.duration} сек.
##                                </li>
##                                <li>
##                                    <i class="fa fa-1x fa-fw fa-times-circle text-muted"></i> ${m.closable} сек.
##                                </li>
##                                <li>
##                                    <i class="fa fa-1x fa-fw fa-money text-muted"></i>
##                                    <%
##                                        total = 0
##                                        if m.endcard: total += .5
##                                        if m.overlay: total += .3
##                                        if m.closable: total += m.closable*.3
##                                    %>
##                                    +${total} р.
##                                </li>
####                            %endif
##                        </ul>

##                    </div>

                    <div class="row">
                        <div class="col-md-8">
                            <h4 style="margin-top:0;">
                                <span class="badge badge-default">#${m.id}</span>
                                <a style="clear:both;margin-left: 5px;" href="/adv/campaign/${g.campaign.id}/media/${m.id}">
                                    ${h.limitstr(m.name, 25)}
                                </a>
                            </h4>

                            %if m.uri != '':
                                <p>
                                    <small>
                                        <a href="${m.uri}" target="_blank">
                                            <i class="fa fa-external-link-square" style="font-size: 13px"></i> ${h.limitstr(m.uri, 31)}
                                        </a>
                                    </small>
                                </p>
                            %endif


                            <p class="text-left">
                                <%
                                    today = datetime.date.today()
                                    ## monthago = datetime.date(day=today.day, month=today.month - 1, year=today.year)
                                    monthago = today - datetime.timedelta(days=30)
                                %>


                                %if m.state == Media.State.ACTIVE:
                                                                      ${ fabutton.fab([
                                                                      { 'text' : u'Остановить',  'href' : '/adv/campaign/%d/media/%d/deactivate'%(g.campaign.id, m.id), 'icon' : 'pause' },
                                                                      {
                                                                      'text' : u'Редактировать',
                                                                      'href' : '/adv/campaign/%d/media/%d'%(g.campaign.id, m.id),
                                                                      'icon' : 'pencil'
                                                                      }
                                                                      ]) }
                                %elif m.state == Media.State.DISABLED:
                                                                      ${ fabutton.fab([
                                                                      { 'text' : u'Запустить',  'href' : '/adv/campaign/%d/media/%d/activate'%(g.campaign.id, m.id), 'icon' : 'play' },
                                                                      {
                                                                      'text' : u'Редактировать',
                                                                      'href' : '/adv/campaign/%d/media/%d'%(g.campaign.id, m.id),
                                                                      'icon' : 'pencil'
                                                                      },
                                                                      ]) }
                                %endif

                                ${ fabutton.fab([
                                    {
                                    'text' : u'Статистика',
                                    'href' : '/adv/stat/date#?from=%s&to=%s&campaign=%d&media=%d' % (unicode(monthago.strftime('%d.%m.%Y')), unicode(today.strftime('%d.%m.%Y')), g.campaign.id, m.id),
                                    'icon' : 'bar-chart-o'
                                    },
                                    {
                                    'text' : u'Удалить',
                                    'data': {
                                    'cid': '%d' % g.campaign.id,
                                    'mid': '%d' % m.id
                                    },
                                    'href' : '#delete',
                                    'icon' : 'times',
                                    'class': 'delete_media'
                                    }
                                ]) }
                            </p>
                        </div>

                        %if m.type == Media.Type.VIDEO:
                            <div class="col-md-4 text-left">
                                <p class="text-muted"><small><i class="fa fa-clock-o"></i> ${m.duration} сек.</small></p>
                                <p class="text-muted"><small><i class="fa fa-times-circle"></i> ${m.closable} сек.</small></p>
                                <p class="text-muted"><small><i style="margin-top: 5px;"><img src="/global/static/img/icons/coins_l.png"></i>
                                    <%
                                        total = 0
                                        if m.endcard: total += .5
                                        if m.overlay: total += .3
                                        if m.closable: total += m.closable*.3
                                    %>
                                    +${total} р.</small></p>
                            </div>
                        %endif
                    </div>
                </td>
                <td class="text-center media-status">
                    %if m.type == Media.Type.BANNER:
                        <i class="fa fa-2x fa-fw fa-file-picture-o"></i>
                        <br/>
                        <small class="text-muted">Баннер</small>
                    %elif m.type == Media.Type.VIDEO:
                        <i class="fa fa-2x fa-fw fa-file-video-o"></i>
                        <br/>
                        <small class="text-muted">Видео</small>
                    %elif m.type == Media.Type.MRAID:
                        <i class="fa fa-2x fa-fw fa-file-zip-o"></i>
                        <br/>
                        <small class="text-muted">MRAID</small>
                    %endif
                </td>
                <td class="text-center media-status" >
                    <span class="hidden">${m.state}</span>
                    %if m.state == Media.State.ACTIVE:
                        <span class="badge badge-success">Активный</span>
                    %elif m.state == Media.State.SUSPENDED:
                        <span class="badge badge-danger">Заблокированный</span>
                    %elif m.state == Media.State.NONE:
                        <span class="badge badge-info">Новый</span>
                    %else:
                        <span class="badge badge-warning">Остановленный</span>
                    %endif
                </td>
                <td class="text-center">
                    %if m.type == Media.Type.BANNER:
                        <a class="fancyboxpic" href="${m.banner}">
                            <img src="${m.banner}" alt="" width="100"/>
                        </a>
                        <div style="margin-top: 2px; display: block;">${m.width} &times; ${m.height}</div>
                    %elif m.type == Media.Type.VIDEO:
                        <a class="fancyboxvid" href="${m.video}" onmouseleave="endPreview(event, this)"
                           onmouseenter="startPreview(event, this)">
                            <div class="thumbnail vthumb" style="width: 130px; height:100px; margin-bottom: 0;">
                                ## Get the preview frame.
                                <%
                                    preview_frame = m.video.replace('source', 'frames/preview.png')
                                    ## Does the preview frame exists?
                                    r = requests.head('http://vidiger.com' + preview_frame)
                                    if not r.status_code == requests.codes.ok:
                                        preview_frame = '/global/static/img/icons/video_preview.png'
                                %>
                                <img src="${preview_frame}" class="vpreview">
                                <i class="fa fa-play-circle playbutton"></i>
                            </div>
                        </a>
                    %elif m.type == Media.Type.MRAID:
                        <small class="text-muted">Предпросмотр <br/>недоступен</small>
                        <div style="margin-top: 2px; display: block;">${m.width} &times; ${m.height}</div>
                    %endif
                </td>
##                <td width="150" class="text-center">
##                    <div class="thumbnail" style="width: 150px; height:100px; margin-bottom: 0;">
##                    %if m.endcard:
##                        <a class="fancyboxpic" href="${m.endcard}">
##                            <img src="${m.endcard}" rel="group" style="height:90px">
##                        </a>
##                    %else:
##                        <a class="fancyboxpic" href="/global/static/img/default-endcard.png">
##                            <img src="/global/static/img/default-endcard.png" rel="group"  style="height:90px">
##                        </a>
##                    %endif
##                    </div>
##                </td>

##                <td class="text-right">${m.impressions} </td>
##                <td class="text-right">${m.clicks}</td>
##                <td class="text-right">${"%.2f%%" % (100 * h.divbyz(m.clicks, m.impressions))}</td>
##                <td class="text-right">${m.cost} руб.</td>
            </tr>
            % endfor
            </tbody>
            </table>
        </div>
    </div>

</div>
</div>
<div class="modal fade" id="uriModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="myModalLabel">Изменить URI</h4>
      </div>
      <div class="modal-body">
        <div class="pasta form-group">                                                  <!-- MEDIA.URI -->
            <label for="mediaURI" class="col-sm-2 control-label"></label>
                <div class="col-sm-10"><input name="uri" type="text" class="form-control" placeholder="Landing URI" value=""></div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
        <button type="button" id="edit_url" class="btn btn-primary group_actions">Сохранить</button>
      </div>
    </div>
  </div>
</div>
<div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog" style="width: 25%">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="myModalLabel">Удалить медиа?</h4>
      </div>
      <div class="modal-body">
        <div class="pasta form-group">
            Действительно удалить медиа <span id="del_list"></span> ?

        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
        <button type="button" id="delete" class="btn btn-danger group_actions">Удалить</button>
      </div>
    </div>
  </div>
</div>
<script src="/global/static/scripts/media_table.js"></script>
<script>
    var $delete_modal = $('#deleteModal');
    $('.delete_media').on('click', function() {
        var cid = $(this).data('cid'),
            mid = $(this).data('mid');

        $('#delete').one('click', function() {
            window.location = '/adv/campaign/' + cid + '/media/' + mid + '/delete';
        });
        if (cid && mid) {
            $delete_modal.modal();
            $('#del_list').html('').html(cid);
        }
        console.debug('# Deleting media', mid);
    });
    $('#delete_media').on('click',function(){
        var objects = $('.checkboxes:checked');
        var objects_ids = '';
        if (objects.length > 0){
                $delete_modal.modal();
                for (var i=0; i < objects.length; i++){
                    objects_ids += objects[i].value + ',';
            }
            $('#del_list').html('');
            $('#del_list').html(objects_ids.slice(0, -1));
        }
    });



    var $uri_modal = $('#uriModal');
    var post_url = document.URL.split('media/')[0] + 'media/group_actions';

    $uri_modal.on('hide.bs.modal', function(e){
            $('[name=uri]').val('');
    });

    $('#eurl_link').on('click',function(){
        var media = $('.checkboxes:checked');
        if (media.length > 0){
                $uri_modal.modal();
        }
   });
   $('.group_actions').on('click', function(){
        var media = $('.checkboxes:checked');
        var media_ids = '';
        if (media.length > 0){
            for (var i=0; i < media.length; i++){
                    media_ids += media[i].value + ',';
            }
            $.post(post_url,{media_list:media_ids, action:this.id, url:$('[name=uri]').val()}, after_load);
        }
    });

    function after_load(data){
        if (data.error  == 0){
            var action_html = '';
            var media_str = JSON.stringify(data.media_list).replace('[','').replace(']','');
            if (data.action == 1){
                action_html = 'Активен';
                toastr.success('ID: '+media_str, 'Media запущены' );
            }
            else if (data.action == 2){
                action_html = 'Остановлен';
                toastr.success('ID: '+media_str, 'Media остановлены ');
            }
            else if(data.action == 'delete'){
                for(var i = 0; i < data.media_list.length; i++){
                    $('#tr_'+data.media_list[i]).remove();
                }
                $delete_modal.modal('hide');
                toastr.success('ID: '+media_str, 'Удалены media');
                return;
            }
            else if (data.action == 'edit_url'){
                $('.media-uri a').attr('href', $('[name=uri]').val()).html($('[name=uri]').val());
                $uri_modal.modal('hide');
                toastr.success('ID: '+media_str, 'Uri изменены');
                return;
            }
            for(var i = 0; i < data.media_list.length; i++){
                if (action_html == 'Активен')
                    $('#tr_'+data.media_list[i]+' .media-status .badge').removeClass('badge-info').addClass('badge-success').html(action_html);
                else
                    $('#tr_'+data.media_list[i]+' .media-status .badge').removeClass('badge-success').addClass('badge-info').html(action_html);
            }
        }
    }
TableManaged.init();
</script>
<!-- END PAGE CONTENT-->
<script type="text/javascript">
    $(document).ready(function () {
        $(".fbox").fancybox();
        var getVideoContent = function (filename, w, h) {
            var content = '' +
                    '<video id="video-box" class="video-js vjs-default-skin"' +
                    'controls preload="auto" width="{w}" height="{h}">' +
                    '<source src="{filename}" type="video/mp4" />' +
                    '<p class="vjs-no-js">To view this video please enable JavaScript, and consider upgrading to a web browser that <a href="http://videojs.com/html5-video-support/" target="_blank">supports HTML5 video</a></p>' +
                    '</video>';

            content = content.replace('{filename}', filename).replace('{w}', w || 720).replace('{h}', h || 457);

            return content
        };
        $(".fancyboxvid").on('click', function (event) {
            event.preventDefault();
            var filename = $(this).attr('href');
            $.fancybox.open([filename], {
                content: getVideoContent(filename, 600, 350),
                afterShow: function () {
                    videojs(document.getElementById('video-box'), {}, function () {
                        this.load().play();
                    });
                },
                beforeClose: function () {
                    videojs('video-box', {}, function () {
                        this.pause();
                    });
                }
            });
        });

        $(".fancyboxpic").fancybox({
            'type' : 'image',
            padding: 0,
            openEffect : 'elastic',
            openSpeed  : 250,
            closeEffect : 'elastic',
            closeSpeed  : 250,
            closeClick : true,
            helpers : {
                overlay : null
            }
            });

##        $(".fancyboxvid").fancybox({
##            'type' : 'image',
##            padding: 0,
##            openEffect : 'elastic',
##            openSpeed  : 250,
##            closeEffect : 'elastic',
##            closeSpeed  : 250,
##            closeClick : false,
##            helpers : {
##                overlay : null
##            },
##            content: '<div id="video_box"></div>',
##            afterShow: function(){
##                //$(".fancybox-error").remove();
####                //$(".fancybox-inner").append('<div id="preview_${m.id}" style="float:right"></div>');
##                 jwplayer("video_box").setup({
##                                file: $(this).attr("href"),
##                                height: 350,
##                                skin: 'bekle',
##                                width: 600,
##                                wmode: 'window'
##                            }).play();
##            }
##            });
    });

    $(".vpreview").hover(
        function(){$(this).next(".playbutton").css("color", "#fff");},
        function(){$(this).next(".playbutton").css("color", "#5AB5FD");}
        );

    function startPreview(e, self) {
        var preview = self.children[0].children[0];
        timerChangeFrame = setInterval(function(){changeFrame(preview)}, 500);
    }
    function endPreview(e, self) {
        var preview = self.children[0].children[0];
        var src = preview.src.split("/");

        switch(src[src.length - 1]) {
            case "video_preview.png":
                break;
            default:
                src.splice(src.length - 1, 1);
                src.splice(0, 3);
                src = src.join("\/").replace(",", "/");
                src += "/preview.png";
                preview.src = "\/" + src;
                break;
        }

        clearInterval(timerChangeFrame)
    }
    function changeFrame(preview) {
        var src = preview.src.split("/");
        switch(src[src.length - 1]) {
            case "video_preview.png":
                break;
            case "preview.png":
                src.splice(src.length - 1, 1);
                src.splice(0, 3);
                src = src.join("\/").replace(",", "/");
                src += "/preview1.png";
                preview.src = "\/" + src;
                break;
            default:
                var frame = Number(src[src.length - 1].split("preview")[1].split(".png")[0]);
                if (frame == 10) {
                    frame = 1;
                } else {
                    frame += 1;
                }
                frame = "preview" + frame;
                src.splice(src.length - 1, 1);
                src.splice(0, 3);
                src = src.join("\/").replace(",", "/");
                src += "/" + frame + ".png";
                preview.src = "/" + src;
                break;
        }

    }


</script>