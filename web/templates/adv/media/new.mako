## -*- coding: utf-8 -*-
<%namespace name="bcrumb" file="/layout/bcrumb.mako" />
<%inherit file="/layout/main.mako" />
<%def name="title()">Media</%def>
<%def name="description()">Media</%def>

${ bcrumb.h(self) }
${ bcrumb.bc([
{ 'name' : u'Кампании',    'href' : '/adv/campaign/', 'icon' : 'rocket' }
]) }

<%def name="page_css()">
    <link href="/global/static/plugins/ion.rangeslider/css/ion.rangeSlider.css" rel="stylesheet" type="text/css"/>
    <link href="/global/static/plugins/ion.rangeslider/css/ion.rangeSlider.Conquer.css" rel="stylesheet"
          type="text/css"/>
    <link rel="stylesheet" type="text/css" href="/global/static/plugins/bootstrap-fileupload/bootstrap-fileupload.css"/>
    <style>
        .xform-error {
            padding: 3px;
            font-weight: bold !important;
        }

            ## .btn-squared {
            ##     height: 100px;
        ##     width: 100px;
        ## }

        .form-group .btn-group > .btn {
            height: 60px;
        }

        .jwplayer {
            margin: 0 auto !important;
        }
    </style>
</%def>

<%def name="page_js()">
    ## <script src="http://jwpsrv.com/library/MQkkJAm8EeO9QiIACusDuQ.js"></script>
        ​
    <script src="/global/static/plugins/jwplayer/jwplayer.js"></script>
    <script>jwplayer.key = "BNMMP5zePmaUScqhl2go8j3xvQ34ZUTwrG1xTA==";</script>
    <script src="/global/static/plugins/ion.rangeslider/js/ion-rangeSlider/ion.rangeSlider.min.js"></script>
</%def>

<!-- BEGIN PAGE CONTENT-->

<div class="col-md-12">
    <div class="portlet">
        <div class="portlet-title">
            <div class="caption">
                Выберите тип медиа
            </div>
        </div>
        <div class="portlet-body">
            <div class="btn-group btn-group-justified">
                <a class="btn btn-default btn-squared" href="new/banner">Баннер</a>
                <a class="btn btn-default btn-squared" href="new/video">Видео</a>
                <a class="btn btn-default btn-squared" href="new/mraid">MRAID</a>
            </div>
        </div>
    </div>
</div>





<script type="text/javascript" src="/global/static/plugins/jquery-file-upload/js/jquery.fileupload.js"></script>
<script type="text/javascript" src="/global/static/plugins/jquery-file-upload/js/vendor/jquery.ui.widget.js"></script>
<script type="text/javascript" src="/global/static/plugins/jquery-file-upload/js/jquery.iframe-transport.js"></script>
<script type="text/javascript" src="/global/static/plugins/fuelux/js/spinner.min.js"></script>


<script type="text/javascript" src="/global/static/scripts/media_form.js"></script>
<!-- END PAGE CONTENT-->