## -*- coding: utf-8 -*-

<% from faan.core.model.adv.media import Media %>

<%namespace name="bcrumb" file="/layout/bcrumb.mako" />
<%inherit file="/layout/main.mako" />
<%def name="title()">Креативы</%def>
<%def name="description()">Креатив для кампании ${g.campaign.name} [${g.campaign.id}]</%def>

${ bcrumb.h(self) }
${ bcrumb.bc([ { 'name' : u'Кампании', 	'href' : '/adv/campaign/', 'icon' : 'rocket' }
			 , { 'name' : u"%s<sup>[%s]</sup>" % (g.campaign.name, g.campaign.id), 'href' : '/adv/campaign/%s/media' % (g.campaign.id) }
             , { 'name' : u'%s' % (g.media.name if g.media.id else u"Новый креатив")}
			 ]) }

<%
hostname = request.environ['SERVER_NAME'] + (":" + request.environ['SERVER_PORT'] if request.environ['SERVER_PORT'] != '80' else "" )
%>
<%def name="page_css()">
    <link href="/global/static/plugins/ion.rangeslider/css/ion.rangeSlider.css" rel="stylesheet" type="text/css"/>
    <link href="/global/static/plugins/ion.rangeslider/css/ion.rangeSlider.Conquer.css" rel="stylesheet" type="text/css"/>
    <link rel="stylesheet" type="text/css" href="/global/static/plugins/bootstrap-fileupload/bootstrap-fileupload.css"/>
    <link href="//vjs.zencdn.net/4.9/video-js.css" rel="stylesheet">
    <style>
        .xform-error {
            padding: 3px;
            font-weight: bold !important;
        }

        .btn-tall {
            height: 60px;
        }

        .btn-wide {
            width: 180px;
        }

        .fa.in-progress {
            display: none;
        }

        .fa.error {
            display: none;
        }

        .fa.done {
            display: inline;
        }

        #mraid_html {
            font-family: Monaco, Menlo, Consolas, "Courier New", monospace;
            font-size: 12px;
        }

        .jwplayer {
            margin: 0 auto !important;
        }

        .vjs-big-play-button {
            width: 70px !important;
            height: 70px !important;
            border-radius: 100% !important;
            top: 43% !important;
            left: 44% !important;
        }

        .vjs-big-play-button:before {
            left: 2px !important;
            line-height: 70px !important;
        }
        </style>
</%def>

<%def name="page_js()">
##    <script src="http://jwpsrv.com/library/MQkkJAm8EeO9QiIACusDuQ.js"></script>

    <script src="//vjs.zencdn.net/4.9/video.js"></script>
    <script src="/global/static/plugins/ion.rangeslider/js/ion-rangeSlider/ion.rangeSlider.min.js"></script>
</%def>

<!-- BEGIN PAGE CONTENT-->

<div class="row">
    <div class="col-md-12">
        <div class="portlet">
            <div class="portlet-title">
                <div class="caption">${('[%s] %s' % (g.media.Type.inverse().get(g.media.type), g.media.name)) if g.media.id else u'Новый креатив'}</div>
            </div>
            <div class="portlet-body">
                <div class="type_switch">
                    <div class="btn-group btn-group-justified">
                        <a class="btn btn-primary btn-tall type" data-type="${Media.Type.BANNER}">
                            <i class="fa fa-fw fa-file-picture-o"></i>
                            Баннер
                        </a>
                        <a class="btn btn-primary btn-tall type" data-type="${Media.Type.VIDEO}">
                            <i class="fa fa-fw fa-file-video-o"></i>
                            Видео
                        </a>
                        <a class="btn btn-primary btn-tall type" data-type="${Media.Type.MRAID}">
                            <i class="fa fa-fw fa-file-zip-o"></i>
                            MRAID
                        </a>
                    </div>

                    <hr/>
                </div>

                <!-- BANNER -->
                <%include file="partials/banner.mako" />

                <!-- VIDEO -->
                <%include file="partials/video.mako" />

                <!-- MRAID -->
                <%include file="partials/mraid.mako" />
            </div>
        </div>
    </div>
##    <!-- <div class="col-md-4">
##        <div class="portlet fixed">
##            <div class="portlet-title">
##                <div class="caption">Наценка</div>
##            </div>
##            <div class="portlet-body">
##
##                    <div class="label label-success text-center" style="display:block;">
##                        <h5>Цена за просмотр ролика</h5>
##                        <p>
##                            <span class="base">1.0</span> руб.
##                        </p>
##                    </div>
##                    <div class="text-center" style="display:block;">
##                        <i class="fa fa-plus"></i>
##                    </div>
##                    <div class="label label-default text-center" style="display:block;">
##                        <h5>Лендинг</h5>
##                        <p>
##                            <span class="endcard">0.0</span> руб.
##                        </p>
##                    </div>
##                    <div class="text-center" style="display:block;">
##                        <i class="fa fa-plus"></i>
##                    </div>
##                    <div class="label label-default text-center" style="display:block;">
##                        <h5>Оверлей</h5>
##                        <p>
##                            <span class="overlay">0.0</span> руб.
##                        </p>
##                    </div>
##                    <div class="text-center" style="display:block;">
##                        <i class="fa fa-plus"></i>
##                    </div>
##                    <div class="label label-default text-center" style="display:block;">
##                        <h5>Закрытие</h5>
##                        <p>
##                            <span class="closable">0.0</span> руб.
##                        </p>
##                    </div>
##                    <div class="text-center" style="display:block;">
##                        <i class="fa fa-arrow-down"></i>
##                    </div>
##                    <div class="label label-default text-center" style="display:block;">
##                        <h5>Итого</h5>
##                        <p>
##                            <span class="total">0.0</span> руб.
##                        </p>
##                    </div>
##            </div>
##        </div>
##    </div> -->
</div>

<div class="modal mraid fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title">Modal title</h4>
            </div>
            <div class="modal-body">
                <form role="form" class="form" method="post" action="/mraid-order" id="mraid_form" style="display: block;">
                    ${mraid_order_form.csrf_token}
                    <div class="form-group ${'has-error text-danger' if mraid_order_form.message.errors else ''}">
                        <label for="comments">Краткое описание требований к креативу</label>
                        <textarea
                                name="message"
                                class="form-control"
                                id="comments"
                                cols="3" rows="5"
                                placeholder=""
                                title="Пожалуйста, введите текст сообщения (не менее 10 символов)"></textarea>
                        %if mraid_order_form.message.errors:
                            <div class="x-form-error">${mraid_order_form.message.errors[0]}</div>
                        %endif
                    </div>
                    <input type="submit" class="btn btn-success" id="submit" value="Отправить заказ">
                </form>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<div class="modal success fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                Заявка успешно отправлена!
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">OK</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<div class="modal error fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                Не удалось отправить заявку, попробуйте позже
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">OK</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->


<script type="text/javascript" src="/global/static/plugins/jquery-file-upload/js/jquery.fileupload.js"></script>
<script type="text/javascript" src="/global/static/plugins/jquery-file-upload/js/vendor/jquery.ui.widget.js"></script>
<script type="text/javascript" src="/global/static/plugins/jquery-file-upload/js/jquery.iframe-transport.js"></script>
<script type="text/javascript" src="/global/static/plugins/fuelux/js/spinner.min.js"></script>

<%
    def get_form_field_data(form, field):
        try:
            data = getattr(form, field).data
        except AttributeError, e:
            data = ''

        return data
%>

<script type="text/javascript">
    $(document).on('ready', function () {
        var Type = {
            BANNER: ${Media.Type.BANNER},
            VIDEO: ${Media.Type.VIDEO},
            MRAID: ${Media.Type.MRAID}
        };

        var MRAIDType = {
            FILE: ${Media.MRAID_Type.FILE},
            URL: ${Media.MRAID_Type.URL},
            HTML: ${Media.MRAID_Type.HTML}
        };

        var uploadURL = document.URL.split('/media')[0] + '/media/upload/';
        var mediaId = ${g.media.id or 0};
        var mediaType = ${current_form.type.data};
        var mediaVideo = '${get_form_field_data(current_form, "video")}';
        var mediaEndcard = '${get_form_field_data(current_form, "endcard")}';
        var mediaOverlay = '${get_form_field_data(current_form, "overlay")}';
        var mediaBanner = '${get_form_field_data(current_form, "banner")}';
        var mediaActionEnd = '${get_form_field_data(current_form, "action_end")}';
        var mediaBannerSize = '${get_form_field_data(current_form, "width")};${get_form_field_data(current_form, "height")}';
        var mediaMRAIDType = '${get_form_field_data(current_form, "mraid_type")}' || ${Media.MRAID_Type.FILE};

        var formHasErrors = ${0 if not current_form.errors else 1};

        var bannerVideoData = $('.form.banner #video_data');
        var bannerEndcardData = $('.form.banner #endcard_data');
        var bannerOverlayData = $('.form.banner #overlay_data');
        var videoVideoData = $('.form.video #video_data');
        var videoEndcardData = $('.form.video #endcard_data');
        var videoOverlayData = $('.form.video #overlay_data');
        var bannerBannerData = $('.form.banner #banner_data');
        var mraidMraidData = $('.form.mraid #mraid_data');

        // Media type switch
        $('a.type').on('click', function () {
            $('form[data-type="' + $(this).data('type') + '"]').show().find('input[name="type"]').val($(this).data('type'));
            $('form[data-type!="' + $(this).data('type') + '"]').hide();
            $('a.type').removeClass('active');
            $(this).addClass('active');
            if ($(this).data('type') == 1)
                $('.form.video #closable').data('value', $('.form.video #closable').val()).removeAttr('value').ionRangeSlider({
                    min: +0,
                    max: +45,
                    from: +($('.form.video #closable').data('value')),
                    type: 'single',
                    step: 1,
                    postfix: " сек.",
                    prettify: false,
                    hasGrid: true
                }).val($('.form.video .closable').data('value'));
        });
        $('a.type[data-type="' + mediaType + '"]').trigger('click');
        // ---

        // File input wrap
        $.each($('input[type="file"]'), function () {
            var element = $(this);
            var target = $(this).data('target');
            var fileInputControl = $('<div class="input-group">\
                        <input type="text" class="form-control holder" readonly style="cursor: pointer;" value=""/>\
                        <span class="input-group-addon">\
                            <i class="done fa fa-fw fa-check"></i>\
                            <i class="in-progress fa fa-fw fa-spinner fa-spin"></i>\
                            <i class="error fa fa-fw fa-times"></i>\
                        </span>\
                        <span class="input-group-btn">\
                            <a class="btn btn-default"><i class="fa fa-paperclip"></i> Выбрать файл</a>\
                        </span>\
                    </div>');
            fileInputControl.on('click', function () {
                element.trigger('click');
            });
            element.hide().before(fileInputControl).on('update', function (evt, value) {
                console.debug(evt, value);
                if (value === undefined) {
                    try { value = element[0].files[0].name; } catch (e) { value = '' }
                }
                fileInputControl.find('input[type="text"]').val(value);
            });
        });
        // ---

        // VideoJS wrapper

        var videoPlayer = function(el, filename) {
##            filename = filename.replace('.mp4', '');
            var content = $('<video id="video-box" class="video-js vjs-default-skin"' +
            'controls preload="auto" width="600" height="320">' +
            '<source src="' + filename + '" type="video/mp4" />' +
            '<p class="vjs-no-js">To view this video please enable JavaScript, and consider upgrading to a web browser that <a href="http://videojs.com/html5-video-support/" target="_blank">supports HTML5 video</a></p>' +
            '</video>');
            el.empty().append(content);
            videojs(document.getElementById('video-box'), {}, function () {
                this.load();
            });
        };

        // ---

        // Banner click action
        $('.form.banner #action_click').on('change', function () {
            $('.form.banner .action').hide();
            switch (+$(this).val()) {
                case 1:
                    $('.form.banner .action.call').show();
                    break;
                case 2:
                    $('.form.banner .action.sms').show();
                    break;
                case 3:
                    $('.form.banner .action.url').show();
                    break;
                case 4:
                    $('.form.banner .action.video.select').show();
                    break;
            }
        }).trigger('change').select2();
        // ---

        // Video click action
        $('.form.video #action_click').on('change', function () {
            $('.form.video .action').hide();
            switch (+$(this).val()) {
                case 1:
                    $('.form.video .action.call').show();
                    break;
                case 2:
                    $('.form.video .action.sms').show();
                    break;
                case 3:
                    $('.form.video .action.url').show();
                    break;
            }
        }).trigger('change').select2();
        // ---

        // Video type select
        $('.btn.video').on('click', function () {
            $('.action.video.new').hide();
            $('.action.video.existing').hide();
            $('.btn.video').removeClass('active');
            $(this).addClass('active');
            if ($(this).hasClass('new')) {
                $('.action.video.new').show();
                $('.form.banner .action.url').show();
                $('.form.banner #closable').data('value', $('.form.banner #closable').val()).removeAttr('value').ionRangeSlider({
                    min: +0,
                    max: +45,
                    from: +($('.form.banner #closable').data('value')),
                    type: 'single',
                    step: 1,
                    postfix: " сек.",
                    prettify: false,
                    hasGrid: true
                }).val($('#closable').data('value'));
            } else {
                $('.action.video.existing').show();
                $('.form.banner .action.url').hide();
            }
        });

        // ---

        // Init Select2
        $('#existing_video').select2({
            matcher: function (term, text, opt) {
                return text.toUpperCase().indexOf(term.toUpperCase()) >= 0 || opt.parent("optgroup").attr("label").toUpperCase().indexOf(term.toUpperCase()) >= 0
            }
        });

        $('.form.banner #banner_size').on('change', function () {
            $('.form.banner #banner').val('');
            bannerBannerData.parent().find('img').hide();
            bannerBannerData.val('');
            bannerBannerData.trigger('update');
            if ($(this).val()) {
                $('.form.banner #width').val($(this).val().split(';')[0]);
                $('.form.banner #height').val($(this).val().split(';')[1]);
            }

            if ($('.form.banner #banner_size option:selected').data('is-custom') == 'True') {
                $('.form.banner .size-manual').removeClass('hidden');
                $('.form.banner #width').val('${g.media.width if g.media.id else ''}');
                $('.form.banner #height').val('${g.media.height if g.media.id else ''}');
            } else {
                $('.form.banner .size-manual').addClass('hidden');
            }
        }).val(mediaBannerSize).trigger('change').select2();

        $('.form.mraid #banner_size').on('change', function () {
            if ($(this).val()) {
                $('.form.mraid #width').val($(this).val().split(';')[0]);
                $('.form.mraid #height').val($(this).val().split(';')[1]);
            }

            if ($('.form.mraid #banner_size option:selected').data('is-custom') == 'True') {
                $('.form.mraid .size-manual').removeClass('hidden');
                $('.form.mraid #width').val('${g.media.width if g.media.id else ''}');
                $('.form.mraid #height').val('${g.media.height if g.media.id else ''}');
            } else {
                $('.form.mraid .size-manual').addClass('hidden');
            }
        }).val(mediaBannerSize).trigger('change').select2();
        // ---

        var onSend = function(element) {
            return function() {
                $('.form button[type="submit"]').addClass('disabled');
                element.parent().find('input.holder').val('Подождите, идет загрузка...');
                element.parent().find('.error').hide();
                element.parent().find('.done').hide();
                element.parent().find('.in-progress').show();
            }
        };

        var onDone = function(element, callback, handleCustom) {
            return function (e, data) {
                $('.form button[type="submit"]').removeClass('disabled');
                element.parent().find('input.holder').val('');
                if (data.result.error) {
                    element.parent().find('.error').show();
                    if (!handleCustom) { return }
                } else {
                    element.parent().find('.done').show();
                }
                element.parent().find('.in-progress').hide();
                callback(e, data);
                element.trigger('update');
            }
        };

        // Init fileupload

        bannerVideoData.fileupload({
            dataType: 'json',
            url: uploadURL + 'video',
            replaceFileInput: false,
            send: onSend(bannerVideoData),
            done: onDone(bannerVideoData, function(e, data) {
                $(".form.banner #video").val('/global/upload/temp/' + data.result.name);
                // jwplayer('player_banner').setup({ file: '/global/upload/temp/' + data.result.name, width: '100%', skin: 'bekle' });
                videoPlayer($('#player_banner'), '/global/upload/temp/' + data.result.name);
            })
        });

        bannerEndcardData.fileupload({
            dataType: 'json',
            url: uploadURL + 'endcard',
            replaceFileInput: false,
            send: onSend(bannerEndcardData),
            done: onDone(bannerEndcardData, function (e, data) {
                $(".form.banner #endcard").val('/global/upload/temp/' + data.result.name);
                $(".form.banner #endcard-preview").find("img").attr('src', '/global/upload/temp/' + data.result.name).show();
            })
        });


        bannerOverlayData.fileupload({
            dataType: 'json',
            url: uploadURL + 'overlay',
            replaceFileInput: false,
            send: onSend(bannerOverlayData),
            done: onDone(bannerOverlayData, function (e, data) {
                $(".form.banner #overlay").val('/global/upload/temp/' + data.result.name);
                $(".form.banner #overlay-preview").find("img").attr('src', '/global/upload/temp/' + data.result.name).show();
            })
        });


        videoVideoData.fileupload({
            dataType: 'json',
            url: uploadURL + 'video',
            replaceFileInput: false,
            send: onSend(videoVideoData),
            done: onDone(videoVideoData, function (e, data) {
                $(".form.video #video").val('/global/upload/temp/' + data.result.name);
                // jwplayer('player_video').setup({ file: '/global/upload/temp/' + data.result.name, width: '100%', skin: 'bekle' });
                videoPlayer($('#player_video'), '/global/upload/temp/' + data.result.name);
            })
        });


        videoEndcardData.fileupload({
            dataType: 'json',
            url: uploadURL + 'endcard',
            replaceFileInput: false,
            send: onSend(videoEndcardData),
            done: onDone(videoEndcardData, function (e, data) {
                $(".form.video #endcard").val('/global/upload/temp/' + data.result.name);
                $(".form.video #endcard-preview").find("img").attr('src', '/global/upload/temp/' + data.result.name).show();
            })
        });


        videoOverlayData.fileupload({
            dataType: 'json',
            url: uploadURL + 'overlay',
            replaceFileInput: false,
            send: onSend(videoOverlayData),
            done: onDone(videoOverlayData, function (e, data) {
                $(".form.video #overlay").val('/global/upload/temp/' + data.result.name);
                $(".form.video #overlay-preview").find("img").attr('src', '/global/upload/temp/' + data.result.name).show();
            })
        });


        bannerBannerData.fileupload({
            dataType: 'json',
            url: uploadURL + 'banner',
            replaceFileInput: false,
            send: onSend(bannerBannerData),
            done: onDone(bannerBannerData, function (e, data) {
                bannerBannerData.parent().find('span.text-danger').text('');
                if (data.result.error) {
                    if (data.result.msg == 'dimensions_mismatch') {
                        bannerBannerData.parent().find('span.text-danger').text('Ширина или высота баннера не соответствуют указанному размеру');
                        bannerBannerData.parent().find('img').hide();
                        bannerBannerData.val('')
                    }
                    if (data.result.msg == 'size_mismatch') {
                        bannerBannerData.parent().find('span.text-danger').text('Размер баннера не должен превышать 400Кб');
                        bannerBannerData.parent().find('img').hide();
                        bannerBannerData.val('')
                    }
                    return
                }
                $(".form.banner #banner").val('/global/upload/temp/' + data.result.name);
                $("#banner-preview").find("img").attr('src', '/global/upload/temp/' + data.result.name).show();
            }, true)
        });


        mraidMraidData.fileupload({
            dataType: 'json',
            url: uploadURL + 'mraid',
            replaceFileInput: false,
            send: onSend(mraidMraidData),
            done: onDone(mraidMraidData, function (e, data) {
                $(".form.mraid #mraid_file").val('/global/upload/temp/' + data.result.name);
            })
        });
        // ---

        $('.form.video .uri_target_switch > a').on('click', function () {
            $('.form.video .uri_target_switch > a').removeClass('active');
            $(this).addClass('active');
            $('.form.video #uri_target').val($(this).data('value'));
        });

        $('.form.banner .uri_target_switch > a').on('click', function () {
            $('.form.banner .uri_target_switch > a').removeClass('active');
            $(this).addClass('active');
            $('.form.banner #uri_target').val($(this).data('value'));
        });

        $('.form.video .action_end_switch > a').on('click', function () {
            $('.form.video .action_end_switch > a').removeClass('active');
            $(this).addClass('active');
            $('.form.video #action_end').val($(this).data('value'));
            if ($(this).data('value') == 1) {
                $('.form.video .action.video.endcard').slideDown(500);
            } else {
                $('.form.video .action.video.endcard').slideUp(500);
            }
        });

        $('.form.banner .action_end_switch > a').on('click', function () {
            $('.form.banner .action_end_switch > a').removeClass('active');
            $(this).addClass('active');
            $('.form.banner #action_end').val($(this).data('value'));
            if ($(this).data('value') == 1) {
                $('.form.banner .action.video.endcard').slideDown(500);
            } else {
                $('.form.banner .action.video.endcard').slideUp(500);
            }
        });

        $('.form.mraid .mraid_type_switch > a').on('click', function () {
            $('.form.mraid .mraid_type_switch > a').removeClass('active');
            $(this).addClass('active');
            $('.form.mraid #mraid_type').val($(this).data('value'));

            $('.form.mraid *[data-if-mraid-type]').hide();
            $('.form.mraid *[data-if-mraid-type="'+ $(this).data('value') + '"]').show();
        });

        $('.form button[type="submit"]').on('click', function () {
            if (!$(this).hasClass('disabled')) {
                $('.form.' + $(this).data('form')).submit();
            }
        });

        $('.form input[type="submit"]').on('click', function () {
            $('.form.' + $(this).data('form')).submit();
        });

        $('#mraid_order').on('click', function () {
            $('.modal form').show();
            $('.modal.mraid').modal();
        });

        $('.form.mraid .mraid_type_switch > a[data-value="' + mediaMRAIDType + '"]').trigger('click');

        var mraidManual = $('#mraid_manual');

        mraidManual.on('click', function () {
            $('.form.mraid .manual').show();
            $('.form.mraid #mraid_switch').hide();
        });

        $(".modal.mraid form").on('submit', function (e) {
            e.preventDefault();
            var form = $(".modal form");
            $.ajax({
                url: document.URL.split('/new')[0] + form.attr('action'),
                data: form.serialize(),
                type: 'POST',
                success: function () {
                    $('.modal.mraid').modal('hide');
                    $('.modal.success').modal();
                },
                error: function () {
                    $('.modal.mraid').modal('hide');
                    $('.modal.error').modal();
                }
            });
        });

        if (mediaId > 0) {
            $('.type_switch').hide();
            if (mediaType == Type.VIDEO && mediaVideo != '')
                $('.btn.video.new').trigger('click');
            if (mediaType == Type.MRAID)
                mraidManual.trigger('click');
        }

        if (mediaVideo != '') {
            if (mediaType == Type.VIDEO)
                // jwplayer('player_video').setup({ file: mediaVideo + '.mp4', width: '100%', skin: 'bekle' });
                videoPlayer($('#player_video'), mediaVideo + '.mp4');
            if (mediaType == Type.BANNER)
                // jwplayer('player_banner').setup({ file: mediaVideo + '.mp4', width: '100%', skin: 'bekle' });
                videoPlayer($('#player_banner'), mediaVideo + '.mp4');
        }

        if (mediaBanner != '') {
            if (mediaType == Type.BANNER)
                $("#banner-preview").find("img").attr('src', mediaBanner).show();
        }

        if (mediaOverlay != '') {
            if (mediaType == Type.BANNER)
                $(".form.banner #overlay-preview").find("img").attr('src', mediaOverlay).show();
            if (mediaType == Type.VIDEO)
                $(".form.video #overlay-preview").find("img").attr('src', mediaOverlay).show();
        }

        if (mediaEndcard != '') {
            if (mediaType == Type.BANNER)
                $(".form.banner #endcard-preview").find("img").attr('src', mediaEndcard).show();
            if (mediaType == Type.VIDEO)
                $(".form.video #endcard-preview").find("img").attr('src', mediaEndcard).show();
        }

        if (mediaActionEnd == 1) {
            if (mediaType == Type.BANNER)
                $('.form.banner .action_end_switch > a[data-value="1"]').trigger('click');
            if (mediaType == Type.VIDEO)
                $('.form.video .action_end_switch > a[data-value="1"]').trigger('click');
        }

        if (formHasErrors && mediaType == Type.MRAID)
            mraidManual.trigger('click');
    });
</script>

##<script type="text/javascript" src="/global/static/scripts/media_form.js"></script>
<!-- END PAGE CONTENT-->