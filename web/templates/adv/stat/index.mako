## -*- coding: utf-8 -*-
<%namespace name="bcrumb" file="/layout/bcrumb.mako" />
<%inherit file="/layout/main.mako" />

<%def name="title()">Рекламодатель</%def>
<%def name="description()">Статистика</%def>
<%def name="page_css()">
    <link rel="stylesheet" type="text/css"
          href="/global/static/plugins/bootstrap-daterangepicker/daterangepicker-bs3.css"/>
    <style>
        th {
            vertical-align: middle!important;
            text-align: center;
        }

        #defaultrange input {
            cursor: pointer !important;
        }

        tfoot th {
            text-align: left !important;
        }

        .ng-table-pager {
            overflow: hidden;
        }

        .pagination {
            margin: 0;
        }
    </style>
</%def>

${ bcrumb.h(self) }

<div class="row">
    <div class="col-md-12">
        <div class="portlet">
            <div class="portlet-title">
                <div class="caption">
                    % if   g.group == 'date':
                        <i class="fa fa-calendar"></i> По дате
                    % elif g.group == 'campaign':
                        <i class="fa fa-bullhorn"></i> По кампаниям
                    % elif g.group == 'media':
                        <i class="fa fa-video-camera"></i> По медиа
                    % endif
                </div>
            </div>

            <div class="portlet-body" ng-controller="TableController as table">
                <div ng-include="'/static/angular-templates/adv/table-filter.html'"></div>

                <table ng-table="table.config" class="table table-striped table-bordered table-hover" id="campaings_table">
                    <thead>
                        <tr role="row">
                            <th rowspan="2"
                                ng-if="table.control.isActive(['date'])"
                                ng-click="table.config.sorting({'ts_spawn' : table.config.isSortBy('ts_spawn', 'asc') ? 'desc' : 'asc'})">
                                Дата
                                <i class="fa fa-unsorted"
                                   ng-class="{'fa-sort-asc': table.config.isSortBy('ts_spawn', 'asc'),'fa-sort-desc': table.config.isSortBy('ts_spawn', 'desc')}"></i>
                            </th>
                            <th rowspan="2"
                                ng-if="table.control.isActive(['campaign'])"
                                ng-click="table.config.sorting({'campaign' : table.config.isSortBy('campaign', 'asc') ? 'desc' : 'asc'})">
                                Кампания
                                <i class="fa fa-unsorted"
                                   ng-class="{'fa-sort-asc': table.config.isSortBy('campaign', 'asc'),'fa-sort-desc': table.config.isSortBy('campaign', 'desc')}"></i>
                            </th>
                            <th rowspan="2"
                                ng-if="table.control.isActive(['media'])"
                                ng-click="table.config.sorting({'media' : table.config.isSortBy('media', 'asc') ? 'desc' : 'asc'})">
                                Креатив
                                <i class="fa fa-unsorted"
                                   ng-class="{'fa-sort-asc': table.config.isSortBy('media', 'asc'),'fa-sort-desc': table.config.isSortBy('media', 'desc')}"></i>
                            </th>
                            <th rowspan="2"
                                ng-if="table.control.isActive(['country'])"
                                ng-click="table.config.sorting({'country' : table.config.isSortBy('country', 'asc') ? 'desc' : 'asc'})">
                                Страна
                                <i class="fa fa-unsorted"
                                   ng-class="{'fa-sort-asc': table.config.isSortBy('country', 'asc'),'fa-sort-desc': table.config.isSortBy('country', 'desc')}"></i>
                            </th>
                            <th rowspan="2"
                                ng-if="table.control.isActive(['region'])"
                                ng-click="table.config.sorting({'region' : table.config.isSortBy('region', 'asc') ? 'desc' : 'asc'})">
                                Регион
                                <i class="fa fa-unsorted"
                                   ng-class="{'fa-sort-asc': table.config.isSortBy('region', 'asc'),'fa-sort-desc': table.config.isSortBy('region', 'desc')}"></i>
                            </th>
                            <th rowspan="2"
                                ng-if="table.control.isActive(['city'])"
                                ng-click="table.config.sorting({'city' : table.config.isSortBy('city', 'asc') ? 'desc' : 'asc'})">
                                Город
                                <i class="fa fa-unsorted"
                                   ng-class="{'fa-sort-asc': table.config.isSortBy('city', 'asc'),'fa-sort-desc': table.config.isSortBy('city', 'desc')}"></i>
                            </th>
                            <th colspan="1" rowspan="2"
                                ng-click="table.config.sorting({'impressions' : table.config.isSortBy('impressions', 'asc') ? 'desc' : 'asc'})">
                                Показы
                                <i class="fa fa-unsorted"
                                   ng-class="{'fa-sort-asc': table.config.isSortBy('impressions', 'asc'),'fa-sort-desc': table.config.isSortBy('impressions', 'desc')}"></i>
                            </th>
                            <th colspan="1" rowspan="2"
                                ng-click="table.config.sorting({'clicks' : table.config.isSortBy('clicks', 'asc') ? 'desc' : 'asc'})">
                                Клики
                                <i class="fa fa-unsorted"
                                   ng-class="{'fa-sort-asc': table.config.isSortBy('clicks', 'asc'),'fa-sort-desc': table.config.isSortBy('clicks', 'desc')}"></i>
                            </th>
                            <th colspan="1" rowspan="2"
                                ng-click="table.config.sorting({'ctr' : table.config.isSortBy('ctr', 'asc') ? 'desc' : 'asc'})">
                                CTR
                                <i class="fa fa-unsorted"
                                   ng-class="{'fa-sort-asc': table.config.isSortBy('ctr', 'asc'),'fa-sort-desc': table.config.isSortBy('ctr', 'desc')}"></i>
                            </th>
                            <th colspan="1" rowspan="2"
                                ng-click="table.config.sorting({'cost' : table.config.isSortBy('cost', 'asc') ? 'desc' : 'asc'})">
                                Деньги
                                <i class="fa fa-unsorted"
                                   ng-class="{'fa-sort-asc': table.config.isSortBy('cost', 'asc'),'fa-sort-desc': table.config.isSortBy('cost', 'desc')}"></i>
                            </th>
                            <th colspan="1" rowspan="2"
                                ng-click="table.config.sorting({'cpm' : table.config.isSortBy('cpm', 'asc') ? 'desc' : 'asc'})">
                                CPM
                                <i class="fa fa-unsorted"
                                   ng-class="{'fa-sort-asc': table.config.isSortBy('cpm', 'asc'),'fa-sort-desc': table.config.isSortBy('cpm', 'desc')}"></i>
                            </th>
                            <th colspan="2" rowspan="1">Оверлей</th>
                            <th colspan="2" rowspan="1">Лендинг</th>
                        </tr>
                        <tr role="row">
                            <th colspan="1" rowspan="1"
                                ng-click="table.config.sorting({'overlay_cost' : table.config.isSortBy('overlay_cost', 'asc') ? 'desc' : 'asc'})">
                                <i class="fa fa-rub"></i>
                                <i class="fa fa-unsorted"
                                   ng-class="{'fa-sort-asc': table.config.isSortBy('overlay_cost', 'asc'),'fa-sort-desc': table.config.isSortBy('overlay_cost', 'desc')}"></i>
                            </th>
                            <th colspan="1" rowspan="1"
                                ng-click="table.config.sorting({'overlays' : table.config.isSortBy('overlays', 'asc') ? 'desc' : 'asc'})">
                                <i class="fa fa-eye"></i>
                                <i class="fa fa-unsorted"
                                   ng-class="{'fa-sort-asc': table.config.isSortBy('overlays', 'asc'),'fa-sort-desc': table.config.isSortBy('overlays', 'desc')}"></i>
                            </th>
                            <th colspan="1" rowspan="1"
                                ng-click="table.config.sorting({'endcard_cost' : table.config.isSortBy('endcard_cost', 'asc') ? 'desc' : 'asc'})">
                                <i class="fa fa-rub"></i>
                                <i class="fa fa-unsorted"
                                   ng-class="{'fa-sort-asc': table.config.isSortBy('endcard_cost', 'asc'),'fa-sort-desc': table.config.isSortBy('endcard_cost', 'desc')}"></i>
                            </th>
                            <th colspan="1" rowspan="1"
                                ng-click="table.config.sorting({'endcards' : table.config.isSortBy('endcards', 'asc') ? 'desc' : 'asc'})">
                                <i class="fa fa-eye"></i>
                                <i class="fa fa-unsorted"
                                   ng-class="{'fa-sort-asc': table.config.isSortBy('endcards', 'asc'),'fa-sort-desc': table.config.isSortBy('endcards', 'desc')}"></i>
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr ng-if="$data.length == 0">
                            <td colspan="12" class="text-center">Нет данных за выбранный период</td>
                        </tr>
                        <tr ng-repeat="row in $data">

                            <td ng-if="table.control.isActive(['date'])">
                                <a ng-if="!table.options.hourly" ng-click="table.columnDate({from: (row.ts_spawn | x_date:null:false), to: (row.ts_spawn | x_date:null:false)})" style="cursor:pointer;">
                                    {{row.ts_spawn | x_date:table.filterData.timezone:table.options.hourly}}
                                </a>
                                <span ng-if="table.options.hourly">
                                    {{row.ts_spawn | x_date:table.filterData.timezone:table.options.hourly}}
                                </span>
                            </td>
                            <td ng-if="table.control.isActive(['campaign'])">
                                <a ng-href="{{table.columnCampaign({campaign: row.campaign, from: (table.options.data.startDate.format('DD.MM.YYYY')), to: (table.options.data.endDate.format('DD.MM.YYYY'))})}}">
                                    {{row.campaign_name}}
                                </a>
                            </td>
                            <td ng-if="table.control.isActive(['media'])">{{row.media_name}}</td>
                            <td ng-if="table.control.isActive(['country'])">
                                <a ng-href="{{table.columnCountry({country: row.country, from: (table.options.data.startDate.format('DD.MM.YYYY')), to: (table.options.data.endDate.format('DD.MM.YYYY'))})}}">
                                    {{row.country_name}}
                                </a>
                            </td>
                            <td ng-if="table.control.isActive(['region'])">
                                <a ng-href="{{table.columnRegion({region: row.region, from: (table.options.data.startDate.format('DD.MM.YYYY')), to: (table.options.data.endDate.format('DD.MM.YYYY'))})}}">
                                    {{row.region_name}}
                                </a>
                            </td>
                            <td ng-if="table.control.isActive(['city'])">{{row.city_name}}</td>
                            <td>{{row.impressions | x_number:0 }}</td>
                            <td>{{row.clicks | x_number:0 }}</td>
                            <td>{{row.ctr | x_number:2}}%</td>
                            <td>{{row.cost | x_number }} руб</td>
                            <td>{{row.cpm | x_number:2}} руб</td>
                            <td>{{row.overlay_cost | x_number:2}} руб</td>
                            <td>{{row.overlays | x_number:0 }}</td>
                            <td>{{row.endcard_cost | x_number:2}} руб</td>
                            <td>{{row.endcards | x_number:0 }}</td>
                        </tr>
                    </tbody>
                    <tfoot ng-if="$data.length > 0">
                        <tr>
                            <th class="text-left">Всего</th>
                            <th class="text-left">{{table.summary.impressions | x_number:0 }}</th>
                            <th class="text-left">{{table.summary.clicks | x_number:0 }}</th>
                            <th class="text-left">{{table.summary.ctr | x_number:2 }}%</th>
                            <th class="text-left">{{table.summary.cost | x_number:2 }} руб</th>
                            <th class="text-left">{{table.summary.cpm | x_number:2 }} руб</th>
                            <th class="text-left">{{table.summary.overlay_cost | x_number:2 }} руб</th>
                            <th class="text-left">{{table.summary.overlays | x_number:0 }}</th>
                            <th class="text-left">{{table.summary.endcard_cost | x_number:2 }} руб</th>
                            <th class="text-left">{{table.summary.endcards | x_number:0 }}</th>
                        </tr>
                    </tfoot>
                </table>
            </div>
        </div>
    </div>
</div>
