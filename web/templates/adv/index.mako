## -*- coding: utf-8 -*-

<%namespace name="bcrumb" file="/layout/bcrumb.mako" />

<%inherit file="/layout/main.mako" />

## Page JS
<%def name="page_js()">
    <script type="text/javascript" src="/global/static/plugins/underscore.min.js"></script>
    <script src="/global/static/plugins/moment-with-langs.min.js" type="text/javascript"></script>
    <script src="/global/static/plugins/bootstrap-daterangepicker/daterangepicker.js" type="text/javascript"></script>
    <script src="/global/static/plugins/flot/jquery.flot.js" type="text/javascript"></script>
    <script src="/global/static/plugins/flot/jquery.flot.resize.js" type="text/javascript"></script>
    <script src="/global/static/plugins/flot/jquery.flot.time.js" type="text/javascript"></script>
    <script src="/global/static/plugins/toolbox/lime.tables.js" type="text/javascript"></script>
    <script src="/global/static/scripts/adv_dashboard.js" type="text/javascript"></script>
</%def>
## END

## Page CSS
<%def name="page_css()">
    <link href="/global/static/css/plugins.css" rel="stylesheet" type="text/css"/>
    <link href="/global/static/css/pages/dashboard.css" rel="stylesheet" type="text/css"/>
    <link href="/global/static/plugins/fullcalendar/fullcalendar/fullcalendar.css" rel="stylesheet" type="text/css"/>
    <link href="/global/static/plugins/bootstrap-daterangepicker/daterangepicker-bs3.css" rel="stylesheet" type="text/css"/>
    <style>
        .created {
            font-style: oblique;
            color: #aaa;
            margin-right: 10px;
        }

        .news-item {
            margin: 8px;
        }

        .news-item .list-inline li a {
            color: #eee;
        }

        .news-item .list-inline li:first-child {
            padding-left: 5px;
        }

        .news-item .list-inline {
            display: inline;
        }

        .news-item .panel-footer {
            font-size: 10px; padding: 5px;
            color: #888;
        }

        .news-item p {
            margin: 0;
        }

        .news-item h3 {
            margin-bottom: 10px;
        }
    </style>
</%def>
## END

<%def name="title()">Рекламодатель</%def>
<%def name="description()">Главная страница рекламодателя</%def>

${bcrumb.h(self)}

${bcrumb.bc(
    [
        {
            'name': u'Главная страница',
            'href' : '/adv/',
            'icon' : 'home'
        }
    ],
    [
        {
            'type': 'datepicker',
            'title': u'Выберите дату'
        },
        {
            'type': 'button',
            'class': ['btn-default', 'btn-bcrumb', 'btn-success'],
            'href': '/adv/campaign/new',
            'title': u'Добавить кампанию',
            'icon': 'plus'
        }
    ]
)}

<div class="clearfix">
</div>
<div class="row">
    <div class="col-md-12">
        <div class="row">
            <div class="col-md-12 col-sm-12">
                <div class="portlet">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="fa fa-bar-chart"></i>Новости
                        </div>
                        <div class="actions">
                            <a class="btn btn-primary" href="/adv/news/"><i class="fa fa-external-link"></i> Все новости</a>
                        </div>
                    </div>
                    <div class="portlet-body">
                        %if g.news:
                            %for news_item in g.news:
                                <div class="news-item panel panel-${g.priorities.get(news_item.priority)[1]}">
                                    <div class="panel-heading">
                                        <h3 class="panel-title"><a href="/adv/news/${news_item.id}"><strong>${news_item.title}</strong></a></h3>
                                        <p>${h.limitstr(news_item.message, 80)}</p>
                                    </div>
                                    <div class="panel-footer">
                                        <span class="created" data-timestamp="${news_item.ts_created * 1000}"></span>
                                        <ul class="list-inline">
                                            <li><i class="fa fa-tags"></i></li>
                                            %if news_item.tags:
                                                %for tag in news_item.tags:
                                                    <li class="label label-default"><a
                                                            href="/adv/news/?tag=${tag}">${g.tags.get(tag).name}</a></li>
                                                %endfor
                                            %else:
                                                <li>Нет тегов</li>
                                            %endif
                                        </ul>
                                    </div>
                                </div>
                            %endfor
                        %else:
                            <div class="text-center">
                                <h4>Нет новостей</h4>
                            </div>
                        %endif
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            ## Overview graphs

            <div class="col-md-6 col-sm-12">
                <div class="portlet">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="fa fa-bar-chart"></i>Динамика показов за сегодня
                        </div>
                    </div>
                    <div class="portlet-body">
                        <div id="views_dynamics_daily_loading">
                            <img src="/global/static/img/loading.gif" alt="loading"/>
                        </div>
                        <div id="views_dynamics_daily_content" class="display-none">
                            <div id="views_dynamics_daily" class="chart">
                            </div>
                            <div class="legend_holder"></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6 col-sm-12">
                <div class="portlet">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="fa fa-bar-chart"></i>Динамика показов за <span class="date_placeholder"></span>
                        </div>
                    </div>
                    <div class="portlet-body">
                        <div id="views_dynamics_loading">
                            <img src="/global/static/img/loading.gif" alt="loading"/>
                        </div>
                        <div id="views_dynamics_content" class="display-none">
                            <div id="views_dynamics" class="chart">
                            </div>
                            <div class="legend_holder"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6 col-sm-12">
                <div class="portlet">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="fa fa-bar-chart"></i>Затраты по кампаниям за сегодня
                        </div>
                    </div>
                    <div class="portlet-body">
                        <div id="cost_dynamics_daily_loading">
                            <img src="/global/static/img/loading.gif" alt="loading"/>
                        </div>
                        <div id="cost_dynamics_daily_content" class="display-none">
                            <div id="cost_dynamics_daily" class="chart">
                            </div>
                            <div class="legend_holder"></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6 col-sm-12">
                <div class="portlet">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="fa fa-bar-chart"></i>Затраты по кампаниям за <span
                                class="date_placeholder"></span>
                        </div>
                    </div>
                    <div class="portlet-body">
                        <div id="cost_dynamics_loading">
                            <img src="/global/static/img/loading.gif" alt="loading"/>
                        </div>
                        <div id="cost_dynamics_content" class="display-none">
                            <div id="cost_dynamics" class="chart">
                            </div>
                            <div class="legend_holder"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12 col-sm-12">
                <div class="portlet">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="fa fa-bar-chart"></i>Показы по кампаниям за <span class="date_placeholder"></span>
                        </div>
                    </div>
                    <div class="portlet-body">
                        <div class="table-responsive">
                            <table id="apps_table" class="table table-hover">
                                <thead>
                                <tr>
                                    <th>
                                        Кампания
                                    </th>
##                                    <th>
##                                        Всего просмотров
##                                    </th>
                                    <th>
                                        Показы
                                    </th>
                                    <th>
                                        Кликов
                                    </th>
                                    <th>
                                        CTR
                                    </th>
                                    <th>
                                        Затраты
                                    </th>
                                </tr>
                                </thead>
                                <tbody>
                                    ## Here be dynamically created table
                                </tbody>
                                <tfoot>
                                <tr>
                                    <th class="text-left">Всего</th>
                                    <th class="text-left"></th>
                                    <th class="text-left"></th>
                                    <th class="text-left"></th>
                                    <th class="text-left"></th>
##                                    <th class="text-left"></th>
                                </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


##    <div class="col-md-12 col-sm-12">
##        <div class="portlet">
##            <div class="portlet-title">
##                <div class="caption">
##                    <i class="fa fa-bar-chart"></i>Новости
##                </div>
##            </div>
##            <div class="portlet-body">
##                %for news_item in g.news:
##                    <div class="news-item panel panel-danger">
##                        <ul class="list-inline">
##                            <li class="title">${news_item.title}</li>
##                            <li class="short-message">${h.limitstr(news_item.message, 50)}</li>
##                        </ul>
##
##                    </div>
##                %endfor
##            </div>
##        </div>
##    </div>

    
    ## Cost graphs

</div>

<script type="text/javascript">
    jQuery(document).ready(function() {
        Index.init({
            dateFrom: moment().subtract('days', 6),
            dateTo: moment()
        });
        Index.initTables();
        Index.initDashboardDaterange();
        Index.initDashboard();

        var currentTimezone = +(moment().format("ZZ")) * 36000;
        $("*[data-timestamp]").each(function () {
            var self = $(this);
            self.text(moment(self.data("timestamp") + currentTimezone).format("DD MMMM YYYY HH:mm:ss"))
        });
    });
</script>
