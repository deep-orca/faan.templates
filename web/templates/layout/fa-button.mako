
<%def name="media(m)">
    <%
        import datetime
        from faan.core.model.adv.media import Media
        today = datetime.date.today()
        monthago = today - datetime.timedelta(days=30)
    %>
    <div class="btn-group" style="">
        ## Activate/Deactivate button
        %if m.state == Media.State.ACTIVE:
            <a class="btn btn-default tooltips"
               data-placement="top" data-original-title="Остановить" data-container="body"
               href="${'/adv/campaign/{campaign_id}/media/{media_id}/deactivate'.format(campaign_id=m.campaign, media_id=m.id)}">
                <i class="fa fa-fw fa-1x fa-pause"></i>
            </a>
        %elif m.state == Media.State.DISABLED:
            <a class="btn btn-default tooltips"
               data-placement="top" data-original-title="Запустить" data-container="body"
               href="${'/adv/campaign/{campaign_id}/media/{media_id}/activate'.format(campaign_id=m.campaign, media_id=m.id)}">
                <i class="fa fa-fw fa-1x fa-play"></i>
            </a>
        %else:
            <a class="btn btn-default disabled">
                <i class="fa fa-fw fa-1x fa-play" style="color: #CCCCCC"></i>
            </a>
        %endif

        ## Edit
        %if m.state in [Media.State.ACTIVE, Media.State.DISABLED]:
            <a class="btn btn-default tooltips"
               data-placement="top" data-original-title="Редактировать" data-container="body"
               href="${'/adv/campaign/{campaign_id}/media/{media_id}'.format(campaign_id=m.campaign, media_id=m.id)}">
                <i class="fa fa-fw fa-1x fa-pencil"></i>
            </a>
        %else:
            <a class="btn btn-default disabled">
                <i class="fa fa-fw fa-1x fa-pencil" style="color: #CCCCCC"></i>
            </a>
        %endif

        <a class="btn btn-default tooltips" target="_blank"
           data-placement="top" data-original-title="Статистика" data-container="body"
           href="${'/adv/stat/date#?from={from_date}&to={to_date}&campaign={campaign_id}&media={media_id}'.format(campaign_id=m.campaign, media_id=m.id, from_date=monthago.strftime("%d.%m.%Y"), to_date=today.strftime("%d.%m.%Y"))}">
            <i class="fa fa-fw fa-1x fa-bar-chart-o"></i>
        </a>

        <a class="btn btn-danger delete_media tooltips"
           data-placement="top" data-original-title="Удалить" data-container="body"
           href="#delete" data-mid="${m.id}" data-cid="${m.campaign}">
            <i class="fa fa-fw fa-1x fa-times"></i>
        </a>
    </div>
</%def>

<%def name="campaign(c)">
    <%
        import datetime
        from faan.core.model.adv.campaign import Campaign
        today = datetime.date.today()
        monthago = today - datetime.timedelta(days=30)
    %>
    <div class="btn-group" style="float: right; margin-left: 10px;">
        ## Activate/Deactivate button
        %if c.state == Campaign.State.ACTIVE:
            <a class="btn btn-default tooltips"
               data-placement="left" data-original-title="Остановить" data-container="body"
               href="${'/adv/campaign/{campaign_id}/deactivate'.format(campaign_id=c.id)}">
                <i class="fa fa-fw fa-1x fa-pause"></i>
            </a>
        %elif c.state == Campaign.State.DISABLED:
            <a class="btn btn-default tooltips"
               data-placement="left" data-original-title="Запустить" data-container="body"
               href="${'/adv/campaign/{campaign_id}/activate'.format(campaign_id=c.id)}">
                <i class="fa fa-fw fa-1x fa-play"></i>
            </a>
        %else:
            <a class="btn btn-default disabled">
                <i class="fa fa-fw fa-1x fa-play" style="color: #CCCCCC"></i>
            </a>
        %endif

        <a class="btn btn-info tooltips"
           data-placement="left" data-original-title="Добавить новый креатив" data-container="body"
           href="${'/adv/campaign/{campaign_id}/media/new'.format(campaign_id=c.id)}">
            <i class="fa fa-fw fa-plus"></i>
        </a>

        ## Edit
        %if c.state in [Campaign.State.ACTIVE, Campaign.State.DISABLED]:
            <a class="btn btn-default tooltips"
               data-placement="left" data-original-title="Редактировать" data-container="body"
               href="${'/adv/campaign/{campaign_id}'.format(campaign_id=c.id)}">
                <i class="fa fa-fw fa-1x fa-pencil"></i>
            </a>
        %else:
            <a class="btn btn-default disabled">
                <i class="fa fa-fw fa-1x fa-pencil" style="color: #CCCCCC"></i>
            </a>
        %endif

        <a class="btn btn-default tooltips" target="_blank"
           data-placement="left" data-original-title="Статистика" data-container="body"
           href="${'/adv/stat/date#?from={from_date}&to={to_date}&campaign={campaign_id}'.format(campaign_id=c.id, from_date=monthago.strftime("%d.%m.%Y"), to_date=today.strftime("%d.%m.%Y"))}">
            <i class="fa fa-fw fa-1x fa-bar-chart-o"></i>
        </a>

        <a class="btn btn-danger delete_campaign tooltips"
           data-placement="left" data-original-title="Удалить" data-container="body"
           href="#delete" data-cid="${c.id}" data-cname="${c.name}">
            <i class="fa fa-fw fa-1x fa-times"></i>
        </a>
    </div>
</%def>

<%def name="unit(u)">
    <%
        import datetime
        from faan.core.model.pub.unit import Unit
        today = datetime.date.today()
        monthago = today - datetime.timedelta(days=30)
    %>
    <div class="btn-group" style="">
        ## Activate/Deactivate button
        %if u.state == Unit.State.ACTIVE:
            <a class="btn btn-default tooltips"
               data-placement="top" data-original-title="Остановить" data-container="body"
               href="${'{unit_id}/deactivate'.format(unit_id=u.id)}">
                <i class="fa fa-fw fa-1x fa-pause"></i>
            </a>
        %elif u.state == Unit.State.DISABLED:
            <a class="btn btn-default tooltips"
               data-placement="top" data-original-title="Запустить" data-container="body"
               href="${'{unit_id}/activate'.format(unit_id=u.id)}">
                <i class="fa fa-fw fa-1x fa-play"></i>
            </a>
        %else:
            <a class="btn btn-default disabled">
                <i class="fa fa-fw fa-1x fa-play" style="color: #CCCCCC"></i>
            </a>
        %endif

        <a class="btn btn-default tooltips dropdown-toggle"
           data-toggle="dropdown"
           data-placement="top" data-original-title="Добавить источник" data-container="body"
           href="">
            <i class="fa fa-fw fa-plus"></i>
        </a>
        <ul class="dropdown-menu" role="menu">
            <li>1</li>
            <li>2</li>
        </ul>

        ## Edit
        %if u.state in [Unit.State.ACTIVE, Unit.State.DISABLED]:
            <a class="btn btn-default tooltips"
               data-placement="top" data-original-title="Редактировать" data-container="body"
               href="${'{unit_id}'.format(unit_id=u.id)}">
                <i class="fa fa-fw fa-1x fa-pencil"></i>
            </a>
        %else:
            <a class="btn btn-default disabled">
                <i class="fa fa-fw fa-1x fa-pencil" style="color: #CCCCCC"></i>
            </a>
        %endif

        <a class="btn btn-default tooltips" target="_blank"
           data-placement="top" data-original-title="Статистика" data-container="body"
           href="${'/pub/stat/date#?from={from_date}&to={to_date}&application={application_id}&unit={unit_id}'.format(application_id=u.application, unit_id=u.id, from_date=monthago.strftime("%d.%m.%Y"), to_date=today.strftime("%d.%m.%Y"))}">
            <i class="fa fa-fw fa-1x fa-bar-chart-o"></i>
        </a>

        <a class="btn btn-default delete_zone actionDelete tooltips"
           data-placement="top" data-original-title="Удалить" data-container="body"
           href="#delete">
            <i class="fa fa-fw fa-1x fa-times"></i>
        </a>
    </div>
</%def>

<%def name="fab(bps)">

    % for p in bps:
        <a href="${p.get('href', '#')}" id="${p.get('id', 'newid')}" class="${p.get('class', 'newclass')}"
            %for dk, dv in p.get('data', {}).iteritems():
           data-${'%s' % dk}="${'%s' % dv}"
            %endfor
                >
            <i class="fa fa-${p.get('icon', 'question')} fabutton tooltips" data-placement="top"
               data-original-title="${p.get('text', 'NewText')}"></i>
        </a>
    % endfor

</%def>

<%def name="fas(bps)">
    % for p in bps:
        %if p.get('type') == 'app':
        %if p.get('status') == 1:
            <span class="label label-success fastatus tooltips" data-placement="top" data-original-title="Активно"><i
                    class="fa fa-play fastatusi">
        %elif p.get('status') == 2:
            <span class="label label-default fastatus tooltips" data-placement="top" data-original-title="Остановлено">
            <i class="fa fa-pause fastatusi">
        %elif p.get('status') == 0:
            <span class="label label-warning fastatus tooltips" data-placement="top" data-original-title="На модерации">
            <i class="fa fa-clock-o fastatusi">
##        %elif p.get('status') == 3:
##            <span class="label label-danger fastatus tooltips" data-placement="top" data-original-title="Заблокировано">
##            <i class="fa fa-ban fastatusi">
        %endif
        </i>
            <div style="display:none">${p.get('status')}</div>
        </span>
        %endif

        %if p.get('type') == 'zone':
        %if p.get('status') == 1:
            <span class="label label-success fastatus tooltips" data-placement="top" data-original-title="Активно"><i
                    class="fa fa-play fastatusi">
        %elif p.get('status') == 2:
            <span class="label label-default fastatus tooltips" data-placement="top" data-original-title="Остановлено">
            <i class="fa fa-pause fastatusi">
        %elif p.get('status') == 3:
            <span class="label label-warning fastatus tooltips" data-placement="top" data-original-title="На модерации">
            <i class="fa fa-clock-o fastatusi">
        %elif p.get('status') == 4:
            <span class="label label-danger fastatus tooltips" data-placement="top" data-original-title="Заблокировано">
            <i class="fa fa-ban fastatusi">
        %endif
        </i>
            <div style="display:none">${p.get('status')}</div>
        </span>
        %endif
    % endfor

</%def>