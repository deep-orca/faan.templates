## -*- coding: utf-8 -*-
<div class="footer">															<!-- BEGIN FOOTER -->
    <div class="footer-inner">
         &copy; VIDIGER 2014. Все права защищены
    </div>
    <div class="footer-tools">
        <span class="go-top">
            <i class="fa fa-angle-up"></i>
        </span>
    </div>
</div>																			<!-- END FOOTER -->