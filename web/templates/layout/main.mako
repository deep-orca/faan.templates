## -*- coding: utf-8 -*-
<%def name="title()"></%def>
<%def name="description()"></%def>
<%def name="page_js()"></%def>
<%def name="page_css()"></%def>
<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en" class="no-js">
<!--<![endif]-->
<head>
    <meta charset="utf-8"/>
    <title>Vidiger - ${self.title()} - ${self.description()}</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <meta name="description" content="${description}"/>
    <meta name="author" content=""/>
    <meta name="MobileOptimized" content="320">

    ## JS
    <%include file="core_css.mako" />
    ${self.page_css()}

    ## CSS
    <%include file="core_js.mako" />
    ${self.page_js()}
</head>
<!-- END HEAD -->
<body class="page-header-fixed page-sidebar-fixed" ng-app="VidigerApp">
    <%include file="header.mako"/>
    <div class="clearfix"></div>
    <div class="page-container">
        <%include file="/layout/sidebar.mako"/>
        <div class="page-content-wrapper">
            <div class="page-content" style="min-height:1009px !important">
                ${ next.body() }
            </div>
        </div>
    </div>
    <%include file="footer.mako" />

    <script type="text/javascript">
        $(document).on("ready", function () {
            var quickListFormatter = function (row) {
                var tz = +moment(row["ts_spawn"]).format("ZZ") / 100;
                return {
                    date: moment(row["ts_spawn"]).add("hours", tz).format("L HH:mm"),
                    icon: (+row["amount"] > 0 ? "<i class='fa fa-plus'></i>" : "<i class='fa fa-minus'></i>"),
                    amount: Math.abs(+row["amount"]),
                    css: (+row["amount"] > 0 ? "success" : "danger")
                }
            };

            var quickListTemplate = "<li>\
                    <a href='#'>\
                        <span class='label label-sm label-icon label-{css}'>{icon}</span>  {amount}\
                        <i class='fa fa-rub'></i>\
                        <span class='time'>{date}</span>\
                    </a>\
                </li>";

            var onLoad = function (el, response) {
                $(el).empty();
                $.each(response.objects.slice(0, 5), function () {
                    var row = this;
                    $(quickListTemplate).templated(quickListFormatter(row)).appendTo(el);
                });
            };

            var loadData = function () {
                $("#transactions-quick").loadList(onLoad);
            };

            loadData();
        });
    </script>
</body>
</html>

