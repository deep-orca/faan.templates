<%def name="h(gctx)">
<div class="row">
    <div class="col-md-12">
        <!-- BEGIN PAGE TITLE & BREADCRUMB-->
        <h3 class="page-title">${gctx.title()} <small>${gctx.description()}</small></h3>
        <!-- END PAGE TITLE & BREADCRUMB-->
    </div>
</div>
</%def>

<%def name="bc(bcs, controls=[])">
<div class="row">
    <div class="col-md-12">
		<ul class="page-breadcrumb breadcrumb">
			% for b in bcs:
		    <li>
		    	% if b.get('icon'):
		        <i class="fa fa-${b.get('icon')}"></i>
		        % endif
		        <a href="${b.get('href', '#')}">${b.get('name')}</a>
		        % if not loop.last:
		        <i class="fa fa-angle-right"></i>
		        % endif
		    </li>
		    % endfor
            %for ctrl in controls:
                % if ctrl.get("type") == "datepicker" :
                    <li class="pull-right">
                        <div id="dashboard-report-range" class="dashboard-date-range tooltips" data-placement="top" data-original-title="${ctrl.get('title')}">
                            <i class="fa fa-calendar"></i>
                            <span></span>
                            <i class="fa fa-angle-down"></i>
                        </div>
                    </li>
                %elif ctrl.get("type") == "button" :
                    <li class="pull-right">
                        <a href="${ctrl.get('href', '#')}" class="btn ${' '.join(ctrl.get('class', []))}">
                            ${ctrl.get('title', 'Button')}
                            %if ctrl.get('icon'):
                                <i class="fa fa-${ctrl.get('icon', 'question')}"></i>
                            %endif
                        </a>
                    </li>
                % endif
            %endfor
		</ul>
    </div>
</div>
</%def>

<!--
<div class="note note-info">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
    <h4 class="block">Добро пожаловать</h4>
    <p>Чтобы добавить новую кампанию, нажмите на кнопку ниже.</p>
    <a href="/adv/campaign/new" class="btn btn-success"><i class="fa fa-plus"></i> Добавить кампанию</a>
</div>
-->
