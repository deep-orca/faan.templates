<%def name="ps(bps)">

	<p>
		 <h3>Пополнение счета с помощью ${bps.get('name')}</h3>
	</p>
	<p>
		Введите сумму платежа и нажмите "Пополнить". Вы будете перенаправлены на сайт платежной системы для завершения платежа.
	</p>
	<h5>
		Сумма пополнения:
	</h5>
	<form action="${bps.get('href')}"  class="form-horizontal adv_balance">
		<div class="form-body">
			<div class="alert alert-danger display-hide col-md-8">
				<button class="close" data-close="alert"></button>
				Введите сумму не менее 1000 рублей.
			</div>
			<div class="form-group">
				<div class="col-md-2 amount">
					<div class="input-icon right">
						<i class="fa"></i>
						<input type="text" class="form-control" name="amount"/>
					</div>
				</div>
				<div class="col-md-2">
					<button type="submit" class="btn btn-success">Пополнить</button>
				</div>
			</div>
		</div>
	</form>

</%def>