## -*- coding: utf-8 -*-
<%
##from faan.core.model.security import Account
##
##account = Account.Get(request.environ['REMOTE_USER'])
from faan.web.app import account


%>
<li class="dropdown" id="head_balance_bar">										<!-- BEGIN BALANCE DROPDOWN -->
    <a href="#" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true" id="head_balance_bar">
    	&nbsp;&nbsp;&nbsp;${'%.2f' % account.balance} <i class="fa fa-rub" id="head_balance_bar"></i>
    </a>
    <ul class="dropdown-menu extended notification">
        %if account.groups == 2:
            <li><p class="text-center"><a href="/adv/balance" id="header_addfunds"><i class="fa fa-plus"></i> Пополнить баланс</a></p></li>
        %endif

        <li>
            <ul class="dropdown-menu-list scroller" id="transactions-quick" style="height: 170px;">
##                <li>
##                    <a href="#">
##	                    <span class="label label-sm label-icon label-success"><i class="fa fa-plus"></i></span>
##	                    1 356 <i class="fa fa-rub"></i>
##	                    <span class="time">Сегодня</span>
##                    </a>
##                </li>
            </ul>
        </li>
        <li class="external">
            <a href="/${'pub' if account.groups == 1 else 'adv'}/balance/history">История баланса <i class="fa fa-angle-right"></i></a>
        </li>
    </ul>
</li>																			<!-- END BALANCE DROPDOWN -->