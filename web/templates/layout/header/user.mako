## -*- coding: utf-8 -*-
<%
from faan.core.model.security import Account

account = Account.Get(request.environ['REMOTE_USER'])

url = ''
if request.path.startswith('/pub'):
    url = '/pub/profile'
elif request.path.startswith('/adv'):
    url = '/adv/profile'
else:
    if h.get_user_group(request.environ.get('REMOTE_USER')) == Account.Groups.PUB:
        url = '/pub/profile'
    elif h.get_user_group(request.environ.get('REMOTE_USER')) == Account.Groups.ADV:
        url = '/adv/profile'


%>
<li class="dropdown user">															<!-- BEGIN USER LOGIN DROPDOWN -->
    <a href="#" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
	    <span class="username">${account.username}</span>
	    <i class="fa fa-angle-down"></i>
    </a>
    <ul class="dropdown-menu">
        <li><a href="${url}"		><i class="fa fa-user"	></i> Профайл</a></li>
        <li><a href="/general/settings"		><i class="fa fa-gear"	></i> Настройки</a></li>
        <li class="divider"></li>
        <li><a href="/sign/out"				><i class="fa fa-key"	></i> Выход</a></li>
    </ul>
</li>																				<!-- END USER LOGIN DROPDOWN -->