## -*- coding: utf-8 -*-
<%
from faan.core.model.security.account import Account
group = h.get_user_group(request.environ['REMOTE_USER'])

%>
<div class="header navbar navbar-inverse navbar-fixed-top">						<!-- BEGIN HEADER -->
	<div class="header-inner">													<!-- BEGIN TOP NAVIGATION BAR -->
		<a class="navbar-brand" href="/">										<!-- BEGIN LOGO -->
		    <img src="/global/static/img/logo.png" alt="logo" class="img-responsive"/>
		</a>																	<!-- END LOGO -->
		
		<div class="btn-group headerifswitch">
			<a href="/pub" id="pubswitch" class="btn btn-default headerifswitchlabel ${'active' if request.path.startswith('/pub') else ''}"><i class="fa fa-mobile-phone menu"></i> Разработчик</a>
			<a href="/adv" id="advswitch" class="btn btn-default headerifswitchlabel ${'active' if request.path.startswith('/adv') else ''}"><i class="fa fa-desktop menu"></i> Рекламодатель</a>
		</div>
		
		<a href="#" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">		<!-- BEGIN RESPONSIVE MENU TOGGLER -->
		    <img src="/global/static/img/menu-toggler.png" alt=""/>
		</a>																							<!-- END RESPONSIVE MENU TOGGLER -->
		
		<ul class="nav navbar-nav pull-right">									<!-- BEGIN TOP NAVIGATION MENU -->
            <%include file="header/notification.mako" />
            <%include file="header/inbox.mako" />
			<li class="devider">&nbsp;</li>
            <%include file="header/balance.mako"/>
			<li class="devider">&nbsp;</li>
            <%include file="header/user.mako"/>
	    </ul>																	<!-- END TOP NAVIGATION MENU -->
	</div>																		<!-- END TOP NAVIGATION BAR -->
</div>																			<!-- END HEADER -->
