## -*- coding: utf-8 -*-
<%
    from faan.core.model.security.account import Account
    if request.path.startswith('/pub'): #  or h.get_user_group(request.environ.get('REMOTE_USER')) == Account.Groups.PUB:
        g.menu = h.pub_menu
    elif request.path.startswith('/adv'): # or h.get_user_group(request.environ.get('REMOTE_USER')) == Account.Groups.ADV:
        g.menu = h.adv_menu
    else:
        if h.get_user_group(request.environ.get('REMOTE_USER')) == Account.Groups.PUB:
            g.menu = h.pub_menu
        if h.get_user_group(request.environ.get('REMOTE_USER')) == Account.Groups.ADV:
            g.menu = h.adv_menu
%>


<div class="page-sidebar-wrapper">												<!-- BEGIN SIDEBAR -->
	<div class="page-sidebar navbar-collapse collapse">
		<ul class="page-sidebar-menu">											<!-- BEGIN SIDEBAR MENU -->
			<li class="sidebar-toggler-wrapper">
				<div class="sidebar-toggler"></div>								<!-- SIDEBAR TOGGLER BUTTON -->
				<div class="clearfix"></div>									
			</li>
			
##			% for m in g.menu:
##			${renderItem(m, 'start' if loop.first else '')}
##			% endfor
            ${g.menu.render()}
##            <script type="text/javascript">
##                $(document).on("ready", function () {
##                    var new_replies = "${g.sidebar_new_replies}";
##                    $("li .support-badge").text(new_replies);
##                });
##            </script>
			<!--
			<li class="start ">
				<a href="../index.html">
					<i class="fa fa-home"></i>
					<span class="title">Главная</span>
				</a>
			</li>

			<li class="active ">
				<a href="#">
					<i class="fa fa-rocket"></i>
					<span class="title">Приложения</span>
					<span class="selected"></span>
					<span class="arrow open"></span>
				</a>
				<ul class="sub-menu">
					<li class="active">
						<a href="/pub/apps/"><i class="fa fa-list"></i>Список приложений</a>
					</li>
					<li>
						<a href="/pub/apps/new"><i class="fa fa-plus"></i> Добавить приложение</a>
					</li>
					<li>
						<a href=""><i class="fa fa-download"></i>Скачать SDK</a>
					</li>
				</ul>
			</li>
			-->
		</ul>
		<!-- END SIDEBAR MENU -->
	</div>

</div>																			<!-- END SIDEBAR -->
