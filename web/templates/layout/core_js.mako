## -*- coding: utf-8 -*-
<!--[if lt IE 9]>
<script src="/global/static/plugins/respond.min.js"></script>
<script src="/global/static/plugins/excanvas.min.js"></script>
<![endif]-->

<script src="/global/static/plugins/jquery-1.10.2.min.js" type="text/javascript"></script>
<script src="/global/static/plugins/jquery-migrate-1.2.1.min.js" type="text/javascript"></script>
<!-- IMPORTANT! Load jquery-ui-1.10.3.custom.min.js before bootstrap.min.js to fix bootstrap tooltip conflict with jquery ui tooltip -->
<script src="/global/static/plugins/jquery-ui/jquery-ui-1.10.3.custom.min.js" type="text/javascript"></script>
<script src="/global/static/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<script src="/global/static/plugins/bootstrap-hover-dropdown/twitter-bootstrap-hover-dropdown.min.js" type="text/javascript"></script>
<script src="/global/static/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
<script src="/global/static/plugins/jquery.blockui.min.js" type="text/javascript"></script>
<script src="/global/static/plugins/jquery.cokie.min.js" type="text/javascript"></script>
<script src="/global/static/plugins/uniform/jquery.uniform.min.js" type="text/javascript"></script>
<script type="text/javascript" src="/global/static/plugins/moment-with-langs.min.js" ></script>
<script type="text/javascript" src="/global/static/plugins/select2/select2.min.js"></script>
<script type="text/javascript" src="/global/static/plugins/data-tables/jquery.dataTables.patched.min.js"></script>
<script type="text/javascript" src="/global/static/plugins/data-tables/DT_bootstrap.js"></script>
<script type="text/javascript" src="/global/static/plugins/bootstrap-toastr/toastr.min.js"></script>
<script src="/global/static/scripts/app.js"></script>
<script type="text/javascript" src="/global/static/plugins/toolbox/lime.templated.js"></script>
<script type="text/javascript" src="/global/static/plugins/toolbox/lime.toggle.js"></script>

<script type="text/javascript" src="/global/static/plugins/angular.min.js"></script>

<script src="/global/static/plugins/moment-with-langs.min.js" type="text/javascript"></script>
<script type="text/javascript" src="/global/static/plugins/underscore.min.js"></script>
<script type="text/javascript" src="/global/static/plugins/bootstrap-daterangepicker/daterangepicker.js"></script>

<script type="text/javascript" src="/global/static/plugins/ng-table.min.js"></script>
<script type="text/javascript" src="/global/static/plugins/angular.ui.select2.js"></script>
<script type="text/javascript" src="/global/static/plugins/angular.bs-daterangepicker.js"></script>

<script type="text/javascript" src="/global/static/scripts/angular/app.js"></script>
<script type="text/javascript" src="/global/static/scripts/angular/stat.js"></script>
<script type="text/javascript" src="/global/static/scripts/angular/media.js"></script>

<script>
    $(document).ready(function() {
        moment.lang("ru");
        App.init();
    });
</script>