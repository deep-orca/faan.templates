<%namespace name="bcrumb" file="/layout/bcrumb.mako" />

<%inherit file="/layout/main.mako" />

## Page JS
<%def name="page_js()">
    <script src="/global/static/plugins/moment-with-langs.min.js" type="text/javascript"></script>
    <script src="/global/static/plugins/bootstrap-daterangepicker/daterangepicker.js" type="text/javascript"></script>
    <script src="/global/static/plugins/flot/jquery.flot.js" type="text/javascript"></script>
    <script src="/global/static/plugins/flot/jquery.flot.resize.js" type="text/javascript"></script>
    <script src="/global/static/plugins/flot/jquery.flot.time.js" type="text/javascript"></script>
    <script src="/global/static/scripts/adv_dashboard.js" type="text/javascript"></script>
</%def>
## END

## Page CSS
<%def name="page_css()">
    <link href="/global/static/css/plugins.css" rel="stylesheet" type="text/css"/>
    <link href="/global/static/css/pages/error.css" rel="stylesheet" type="text/css"/>

    <link href="/global/static/css/pages/dashboard.css" rel="stylesheet" type="text/css"/>
    <link href="/global/static/plugins/fullcalendar/fullcalendar/fullcalendar.css" rel="stylesheet" type="text/css"/>
    <link href="/global/static/plugins/bootstrap-daterangepicker/daterangepicker-bs3.css" rel="stylesheet" type="text/css"/>
</%def>
## ENDs

<%def name="title()">Error!</%def>

			<!-- BEGIN PAGE CONTENT-->
			<div class="row">
				<div class="col-md-12 page-500">
					<div class=" number">
						 500
					</div>
					<div class=" details">
						<h3>Oops! Something went wrong.</h3>
						<p>
							 We are fixing it!<br/>
							Please come back in a while.<br/><br/>
						</p>
					</div>
				</div>
			</div>
			<!-- END PAGE CONTENT-->
		</div>
		<!-- BEGIN CONTENT -->
		<div class="page-content-wrapper">
			<div class="page-content-wrapper">
			</div>
			<!-- END CONTAINER -->
			<!-- BEGIN FOOTER -->
			<div class="footer">
				<div class="footer-inner">
 				</div>
				<div class="footer-tools">
					<span class="go-top">
						<i class="fa fa-angle-up"></i>
					</span>
				</div>
			</div>
        </div>
			<!-- END FOOTER -->
			<!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->
			<!-- BEGIN CORE PLUGINS -->
			<!--[if lt IE 9]>
<script src="/global/static/plugins/respond.min.js"></script>
<script src="/global/static/plugins/excanvas.min.js"></script> 
<![endif]-->
			<script src="/global/static/plugins/jquery-1.10.2.min.js" type="text/javascript"></script>
			<script src="/global/static/plugins/jquery-migrate-1.2.1.min.js" type="text/javascript"></script>
			<script src="/global/static/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
			<script src="/global/static/plugins/bootstrap-hover-dropdown/twitter-bootstrap-hover-dropdown.min.js" type="text/javascript"></script>
			<script src="/global/static/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
			<script src="/global/static/plugins/jquery.blockui.min.js" type="text/javascript"></script>
			<script src="/global/static/plugins/jquery.cokie.min.js" type="text/javascript"></script>
			<script src="/global/static/plugins/uniform/jquery.uniform.min.js" type="text/javascript"></script>
			<!-- END CORE PLUGINS -->
			<script src="/global/static/scripts/app.js"></script>
			<script>
jQuery(document).ready(function() {    
   App.init();
});
			</script>
			<!-- END JAVASCRIPTS -->
			</body>
			<!-- END BODY -->
			</html>
