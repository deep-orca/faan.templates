<%inherit file="base.mako" />
<%def name="title()">Востановление пароля</%def>

<form class="forget-form" action="/sign/restore" method="post">
    <h3>Забыли пароль ?</h3>
    <p>
         Введите Ваш e-mail в поле ниже, чтобы восстановить пароль.
    </p>
    <div class="form-group">
        <div class="input-icon">
            <i class="fa fa-envelope"></i>
            <input class="form-control placeholder-no-fix" type="text" autocomplete="off" placeholder="Адрес e-mail" name="email"/>
        </div>
    </div>
    <div class="form-actions">
        <a href="/sign/in" class="btn btn-default">
            <i class="m-icon-swapleft"></i>Назад
        </a>
        <button type="submit" class="btn btn-info pull-right">
        Восстановить пароль </button>
    </div>
    ${form.csrf_token()}
</form>

<%def name="custom_js()">
    <script type="text/javascript">
        $(document).on("ready", function() {
            App.init();
            Login.init();
        });
    </script>
</%def>