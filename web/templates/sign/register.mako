<%inherit file="base.mako" />
<%def name="title()">Регистрация</%def>

<form class="register-form" action="/sign/register" method="post">
    <h3>Регистрация</h3>
    <h5 class="text-center form-section" style="margin-top:20px;">Выберите цель регистрации</h5>

    <div class="form-group row">
        <div class="btn-group regtype col-md-12" data-toggle="buttons">
            <label for="pub-group" class="btn btn-default usrtypebutton col-md-6" data-group="1">
                <input id="pub-group" name="groups" type="radio" class="toggle" value="1">
                <i class="fa fa-mobile-phone acctypeicon"></i>
                <h4 class="usrtypelabel">
                     Монетизировать<br>свои приложения
                </h4>
            </label>
            <label for="adv-group" class="btn btn-default usrtypebutton col-md-6" data-group="2">
            <input id="adv-group" name="groups" type="radio" class="toggle" value="2">
                <i class="fa fa-desktop acctypeicon"></i>
                <h4 class="usrtypelabel">
                     Размещать рекламу<br>в приложениях
                </h4>
            </label>
        </div>
    </div>

    <div class="register-fields" id="">
        <div class="form-group row">
            <div class="col-md-7 ${'has-error' if form.fullname.errors else ''}">
                <label class="control-label visible-ie8 visible-ie9">Полное имя</label>
                <div class="input-icon">
                    <i class="fa fa-font"></i>
                    <input class="form-control placeholder-no-fix" type="text" placeholder="Ваше имя и фамилия" name="fullname" value="${form.fullname._value()}"/>
                    <div class="xform-error">${"<br>".join(form.fullname.errors)}</div>
                </div>
            </div>
            <div class="col-md-5 ${'has-error' if form.company.errors else ''}">
                <label class="control-label visible-ie8 visible-ie9">Компания</label>
                <div class="input-icon">
                    <i class="fa fa-briefcase"></i>
                    <input class="form-control placeholder-no-fix" type="text" placeholder="Компания" name="company" value="${form.company._value()}"/>
                    <div class="xform-error">${"<br>".join(form.company.errors)}</div>
                </div>
            </div>
        </div>
        <div class="form-group row">
            <div class="col-md-7 ${'has-error' if form.email.errors else ''}">
                <label class="control-label visible-ie8 visible-ie9">Адрес e-mail</label>
                <div class="input-icon">
                    <i class="fa fa-envelope"></i>
                    <input class="form-control placeholder-no-fix" type="text" placeholder="Адрес e-mail" name="email" value="${form.email._value()}"/>
                    <div class="xform-error">${"<br>".join(form.email.errors)}</div>
                </div>
            </div>
            <div class="col-md-5 ${'has-error' if form.phone.errors else ''}">
                <label class="control-label visible-ie8 visible-ie9">Телефон</label>
                <div class="input-icon">
                    <i class="fa fa-phone"></i>
                    <input id="phonenum" class="form-control placeholder-no-fix" type="text" placeholder="Номер телефона" name="phone" value="${form.phone._value()}"/>
                    <div class="xform-error">${"<br>".join(form.phone.errors)}</div>
                </div>
            </div>
        </div>
        <div class="form-group row">
            <!--ie8, ie9 does not support html5 placeholder, so we just show field title for that-->
            <div class="col-md-7 ${'has-error' if form.skype.errors else ''}">
                <label class="control-label visible-ie8 visible-ie9">Skype</label>
                <div class="input-icon">
                    <i class="fa fa-skype"></i>
                    <input class="form-control placeholder-no-fix" type="text" placeholder="Skype" name="skype" value="${form.skype._value()}"/>
                    <div class="xform-error">${"<br>".join(form.skype.errors)}</div>
                </div>
            </div>
            <div class="col-md-5 ${'has-error' if form.icq.errors else ''}">
                <label class="control-label visible-ie8 visible-ie9">ICQ</label>
                <div class="input-icon">
                    <i style="margin-top: 5px;"><img src="/global/static/img/social/icq.png"></i>
                    <input class="form-control placeholder-no-fix" type="text" placeholder="ICQ" name="icq" value="${form.icq._value()}"/>
                    <div class="xform-error">${"<br>".join(form.icq.errors)}</div>
                </div>
            </div>

        </div>

        <div class="form-group">
            <label class="control-label visible-ie8 visible-ie9"></label>
            <div class="input-icon">
                <textarea placeholder="Расскажите о себе. Какие приложения Вы хотите монетизировать?" name="description" class="form-control placeholder-no-fix"></textarea>
            </div>
        </div>

        <div class="form-group text-center">
            <label class="control-label visible-ie8 visible-ie9"></label>
            ${form.recaptcha}
        </div>

        <div class="form-actions">
            <a href="/sign/in" class="btn btn-default">
                <i class="m-icon-swapleft"></i>Назад
            </a>
            <button type="submit" id="register-submit-btn" class="btn btn-success pull-right">
                Подать заявку
                <i class="m-icon-swapright m-icon-white"></i>
            </button>
        </div>
    </div>
    ${form.csrf_token()}
</form>

<style>
    .register-fields{
        display: none;
    }
</style>


<%def name="custom_js()">
    <script type="text/javascript">
        $(document).on("ready", function() {
            $(".content").css("width", 460);
            App.init();
            Login.init();

            $(".usrtypebutton[data-group='${form.groups._value() if form.groups._value() else 0}']").trigger("click");
            $('#phonenum').mask('+7 (000) 000-00-00');
        });

    </script>
%if 'publisher' in request.args:
    <script type="text/javascript">
        $(document).on("ready", function() {
            $("label[for='pub-group']").click();
            console.log('pub');
        });
    </script>
%elif  'advertiser'  in request.args:
    <script type="text/javascript">
        $(document).on("ready", function() {
            $("label[for='adv-group']").click();
            console.log('adv');
        });
    </script>

%endif
</%def>
