mboxes = {};
mbox_cntr = 0;

function dump_form(f) {
	$(f).find('input[type="checkbox"]').each(function() {
		$(this).attr('managed_initial_value', $(this).prop('checked').toString()); 
	});
	
	$(f).find('input[type="groupbox"]').each(function() {
		$(this).attr('managed_initial_value', $(this).prop('selected').toString()); 
	});

	$(f).find('input[type="text"],select,textarea').each(function() {
		$(this).attr('managed_initial_value', $(this).val()); 
	});
	
	$(f).find('*[contenteditable]').each(function() {
		$(this).attr('managed_initial_value', $(this).html()); 
	});
	
}

function managed_apply(ev) {
	mbox = mboxes[$(this).attr('m_box')];
	if(mbox['active'] || !mbox['dirty'])
		return false;

	

	form = mbox['form'];
	if(typeof(mbox['onsubmit']) == "function" && mbox['onsubmit'](ev) === false)
		return false;
		
	uri 	= $(form).attr('action');
	method 	= $(form).attr('method');
	data 	= $(form).serialize();

	$(mbox['box']).find('p[contenteditable], span[contenteditable]').each(function() {
		if(data.length > 0)
			data += "&";
		
		data += encodeURIComponent($(this).attr('name')) + '=' + encodeURIComponent($(this).html());
	});

	$(mboxes[$(this).attr('m_box')]['box']).find(mbox['overlay_class']).show();
	
	$.ajax({
	  	url		: uri,
	  	dataType: 'json',
	  	data	: data,
	  	type	: method.toUpperCase(),
	  	success : function(data) {
	  				$(mbox['box']).find(mbox['overlay_class']).hide();
	  	
					if(typeof(mbox['onreceive']) == "function" && mbox['onreceive'](data) === false)
						return;
					
					dump_form(form);
					mbox['active'] = false;
					$(mbox['box']).find(mbox['apply'] + ',' + mbox['cancel']).hide();	
				  },
		error	: function(data) {
					$(mbox['box']).find(mbox['overlay_class']).hide();
					
					if(typeof(mbox['onerror']) == "function")
						mbox['onerror'](data);
					
					mbox['active'] = false;
				  }
	});
	
	return false;
}

function managed_cancel() {
	mbox = mboxes[$(this).attr('m_box')];
	if(mbox['active'])
		return;

	f = mbox['form'];
	
	$(f).find('input[type="checkbox"]').each(function() {
		$(this).prop('checked', $(this).attr('managed_initial_value'));
	});
	
	$(f).find('input[type="groupbox"]').each(function() {
		$(this).prop('selected', $(this).attr('managed_initial_value'));
	});

	$(f).find('input[type="text"],select,textarea').each(function() {
		$(this).val($(this).attr('managed_initial_value')); 
	});
	
	$(f).find('*[contenteditable]').each(function() {
		$(this).html($(this).attr('managed_initial_value')); 
	});
	
	
	
	$(mbox['box']).find(mbox['apply'] + ',' + mbox['cancel']).hide();	
	
	if(mbox['oncancel'])
		mbox['oncancel']();
}

function managed_activity() {
	mbox = mboxes[$(this).attr('m_box')]; 
	f = mboxes[$(this).attr('m_box')]['form'];
	
	changed = false;
	
	$(f).find('input[type="checkbox"]').each(function() {
		if($(this).attr('managed_initial_value') != $(this).prop('checked').toString())
			changed = true;
	});
	
	if(!changed)
		$(f).find('input[type="groupbox"]').each(function() {
			if($(this).attr('managed_initial_value') != $(this).prop('selected').toString())
				changed = true; 
		});

	if(!changed)
		$(f).find('input[type="text"],select,textarea').each(function() {
			if($(this).attr('managed_initial_value') != $(this).val())
				changed = true; 
		});
		
	if(!changed)
		$(f).find('*[contenteditable]').each(function() {
			if($(this).attr('managed_initial_value') != $(this).html())
				changed = true; 
		});		
	
	mbox['dirty'] = changed;
	
	if(changed) {
		$(mbox['box']).find(mbox['apply'] + ',' + mbox['cancel']).show();
	} else {
		$(mbox['box']).find(mbox['apply'] + ',' + mbox['cancel']).hide();
	}
	
	return true;
}

function manageBoxes(container, apply, cancel, overlay_class) {
	$(container).each(function() {
		mbox = { };
		
		mbox['box'] = this;
		mbox['active'] = false;
		mbox['dirty'] = false;
		mbox['container'] = container;
		mbox['apply'] = apply;
		mbox['cancel'] = cancel;
		mbox['overlay_class'] = overlay_class;
		mbox['form'] = $(this).find('form')[0];
		
		$(this).find(apply +',' + cancel + ', form, form input, form select, form textarea, p[contenteditable], span[contenteditable]').attr('m_box', mbox_cntr);
		
		dump_form(mbox['form'], mbox_cntr);
		
		var onsubmit = $(mbox['form']).attr("onsubmit");
		var onreceive = $(mbox['form']).attr("onreceive");
		var onerror = $(mbox['form']).attr("onerror");
		var oncancel = $(mbox['form']).attr("oncancel");
		
		if(onsubmit != null) {
			$(mbox['form']).removeAttr('onsubmit');
			mbox['onsubmit'] = new Function('event', onsubmit);
		} else {
			mbox['onsubmit'] = null;
		}
		
		if(onreceive != null) {
			$(mbox['form']).removeAttr('onreceive'); 
			mbox['onreceive'] = new Function('data', onreceive);
		} else {
			mbox['onreceive'] = null;
		}

		if(onerror != null) {
			$(mbox['form']).removeAttr('onerror'); 
			mbox['onerror'] = new Function('data', onerror);
		} else {
			mbox['onerror'] = null;
		}

		if(oncancel != null) {
			$(mbox['form']).removeAttr('oncancel'); 
			mbox['oncancel'] = new Function('', oncancel);
		} else {
			mbox['oncancel'] = null;
		}

		
	
		$(mbox['form']).removeAttr('onSubmit').submit(managed_apply);
				
		$(this).find(apply).click(managed_apply).hide();
		$(this).find(cancel).click(managed_cancel).hide();
		$(this).find('form input, form select, form textarea, *[contenteditable]').change(managed_activity)
														 						  .keyup(managed_activity);
		
		$(this).append($("<div class='" + overlay_class + "' style='display: none;'></div>"));
		
		mboxes[mbox_cntr++] = mbox;
	});
}

$(function() {
	manageBoxes(".managed_box", ".managed_apply_btn", ".managed_cancel_btn", ".managed_overlay");
});

