//
//
//

var filterBox = function() {
    var box;
    // Constructor for filter object.
    var filter = function(init) {
        var format = function(item) { return item.name };
        // Check if there are mandatory fields
        // ...
        this.id = init.id;
        this.name = init.name;
        this.header = init.header;
        this.placeholder = init.placeholder;
        if (init.format)
            this.format = init.format;

        var selecting = function(e) {
            var flag = false;
            box[init.name] = e.val != null ? e.val : "";
            for (var i = 0; i < box.filters.length; i++) {
                if (flag) {
                box[box.filters[i].name] = "";
                    $("#" + box.filters[i].id).select2("data", {id: "", text: "Все"});
                    $("#s2id_"+ box.filters[i].id + " a").addClass("select2-default");
                    $("#s2id_"+ box.filters[i].id + " a span:first").html(box.filters[i].header);
                }
                if (box.filters[i].name == init.name)
                    flag = true;
            }
        };

        if (init.url) {
            this.f = $("#" + this.id).select2({
                placeholder: init.placeholder || "",
                ajax: {
                    url: function() { return init.url + "?query=" + init.name + "&values=" + box.getValues() + box.getExtraParameters(); },
                    type: "GET",
                    results: function(data, page) {
                        for (var name, i = 0; i < data.objects.length; i++)
                            if (data.objects[i].id == null)
                                data.objects[i].name = "Все - " + init.header;
                        return {results: data.objects, text: 'name' };
                    },
                    data: function(term, page) { return { q: term } }
                },
                formatSelection: format,
                formatResult: format
            }).on("select2-selecting", function(e) { selecting(e); });
        } else if (init.data) {
            this.f = $("#" + this.id).select2({
                placeholder: init.placeholder || "",
                data: init.data
            }).on("select2-selecting", function(e) { selecting(e); });
        } else
            throw Error("There aren't the required data to perform initial.\
                         You have to declare 'url' or 'data' attribute!");
    };

    // Initializing container.
    box = {
        filters: [],
        push: function(f) {
            this.filters.push(new filter(f));
            this[f.name] = f.value != null ? f.value : "";
        },
        getValues: function() {
            for (var i = 0, s = ""; i < box.filters.length; i++)
                // Apply a special format output if it exists.
                if (box.filters[i].format)
                    s += box.filters[i].format(box.filters[i], box[box.filters[i].name]) + ",";
                else
                    s += box.filters[i].name + ":" + box[box.filters[i].name] + ",";
            return s.substring(0, s.length - 1);
        },
        getParameters: function() { return this.getValues() },
        getParametersHTTP: function() {
            var s = "";
            for (var i = 0; i < this.filters.length; i++)
                s += "&" + this.filters[i].name + "=" + this[this.filters[i].name];
            return s;
        },
        getExtraParameters: function() {
            var s = "";
            if (this.extraUrlParameters)
                for (var k in this.extraUrlParameters)
                    s += "&" + k + "=" + this.extraUrlParameters[k];
            return s;
        }
    };
    return box;
}();
