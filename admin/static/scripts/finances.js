//
//  JS fo finances table.
//

$(document).on("ready", function() {

    // Init table.
    var tableUrl = '/finances/ajax/table';

    var formater = function(data, isFloat){
        if (isFloat)
            return data.toFixed(2).toString().replace(/\B(?=(\d{3})+(?!\d))/g, " ");
        else
            return data.toString().replace(/\B(?=(\d{3})+(?!\d))/g, " ");
    }

    var tableColumns = [
        { data: 'date', render: function(date){
            var tz = +moment(date, 'X').format('ZZ') * 36;
            return moment(date - tz, 'X').format('DD.MM.YYYY');
        } },
        { data: 'raws', render: function(data){
            return formater(data);
        } },
        { data: 'revenue', render: function(data){
            return formater(data, true);
        } },
        { data: 'charges', render: function(data){
            return formater(data, true);
        } },
        { data: 'margin', render: function(data){
            return formater(data, true);
        } },
        { data: 'ecpm', render: function(ecpm){
            if (+ecpm.raws != 0)
                return formater(+ecpm.margin / +ecpm.raws, true);
            else
                return "0";
        } }
    ];

    var tableFooter = function ( row, data, start, end, display ) {
        var api = this.api();

        if (start == end) {
            for (var j = 1; j < 5; j++)
                $(api.column(j).footer()).html("");
            return;
        }
        var didFormat = false;
        // Remove the formatting to get integer data for summation
        var intVal = function ( i ) {
            return typeof i === 'string' ?
                i.replace(/[' ']/g, '').replace(/[\$,]/g, '') * 1:
                typeof i === 'number' ?
                    i : 0;
        };
        // Remove the formatting to get float data for summation
        var floatVal = function ( i ) {
            didFormat = true;
            return typeof i === 'string' ?
                i.replace(/['руб']/g, '').replace(/[' ']/g, '').replace(/[\$,]/g, '') * 1.0:
                typeof i === 'number' ?
                    i : 0;
        };
        // Impressions
        var impressions = api.column(1).data().reduce( function (a, b) { return intVal(a) + intVal(b);});
        // Revenue
        var revenue = api.column(2).data().reduce( function (a, b) { return floatVal(a) + floatVal(b); });
        // charges
        var charges = api.column(3).data().reduce( function (a, b) { return floatVal(a) + floatVal(b); });
        // margin
        var margin = api.column(4).data().reduce( function (a, b) { return floatVal(a) + floatVal(b); });
        //ECPM
        var ecmp = 0;
        try {
            if (impressions != 0)
                ecmp = +margin / +impressions;
        } catch(e) { }
        // Impressions
        $(api.column(1).footer()).html(formater(impressions));
        $(api.column(2).footer()).html(formater(revenue, true));
        $(api.column(3).footer()).html(formater(charges, true));
        $(api.column(4).footer()).html(formater(margin, true));
        $(api.column(5).footer()).html(formater(ecmp, true));
    }

    var loadTable = function(parameters) {
        // Destroys the table with old data.
        try { $("#financesTable").DataTable().destroy(); } catch (e) {}

        // Initial a new table
        var tableOptions = {
            processing: true,
            ajax: {
                url: tableUrl,
                data: parameters,
                dataSrc: function(response) { return response.data; }
            },
            columns: tableColumns,
            order: [],
            footerCallback: tableFooter
        };

        $("#financesTable").dataTable(tableOptions);

        $('#financesTable_wrapper select').select2();
        $('#financesTable_wrapper input[type="search"]').addClass('form-control input-medium');

    }

    //Index.addTable('financesTable', '/finances/ajax/table', tableColumns, tableFooter);
    //Index.tableLoadData('financesTable', {dateRange: getDateTimestamps(), timezone: +(moment().format("ZZ")) * 36});

    // Initial date range.
    var setupDate = function(from, to){
        var d = "{from} - {to}"
            .replace('{from}', from.format('LL'))
            .replace('{to}', to.format('LL'))
            .replace(/ ([0-9]{1,2}) /g, ' $1, ');
        $('#daterange span').html(d);
    }
    // Return current date range in timestamp format.
    // The date already considers the client timezone.
    var getDateTimestamps = function(){
        var dateRangeTimestamps = [], tz;
        var dateRange = $('#daterange span').html().split(' - ');
        for(var i = 0; i < dateRange.length; i++) {
            //tz = moment(dateRange[i], 'LL').format('ZZ');
            //dateRangeTimestamps.push(+moment(dateRange[i], 'LL').format('X') + tz);
            dateRangeTimestamps.push(+moment(dateRange[i], 'LL').format('X'));
        }
        return dateRangeTimestamps.join('-');
    }
    setupDate(moment().add('days', -6), moment());
    $('#daterange').daterangepicker(
        {
          format: 'YYYY-MM-DD',
          startDate: moment().add('days', -6).format('YYYY-MM-DD'),
          endDate: moment().format('YYYY-MM-DD')
        },
        function(start, end, label) {
            // Change date representation on the page.
            setupDate(start, end)

            // Reload table.
            var date = {
                // The date is passed to the server in timestamp format.
                dateRange: getDateTimestamps(),
                timezone: +(moment().format("ZZ")) * 36
            }
            //Index.tableLoadData('financesTable', date);
            loadTable(date)
        }
    );

    loadTable({dateRange: getDateTimestamps(), timezone: +(moment().format("ZZ")) * 36});
})
