## -*- coding: utf-8 -*-
<%namespace name="bcrumb" file="/layout/bcrumb.mako" />
<%inherit file="/layout/main.mako" />

<%!
    import json
    from faan.core.model.pub.source import Source
%>
<%
    source_type_dict = {
        Source.Type.VIDIGER: 'VIDIGER',
        Source.Type.ADMOB: 'ADMOB',
    }
%>

<%def name="title()">
% if g.source.id:
Источник ${g.source.name}
% else:
Создание источника
%endif
</%def>
<%def name="description()">Настройки источника</%def>

${ bcrumb.h(self) }
${ bcrumb.bc([ { 'name' : u'Блок', 'href' : '/unit/', 'icon' : 'cube' }
             , { 'name' : u"%s" % (g.unit.name), 'href' : '/unit/%s/source'% (g.unit.id)}
             , { 'name' : u"Источник %s" % (g.source.name) if g.source.id else u'Новый источник' }
             ]) }

<%def name="page_css()">
<link href="/global/static/plugins/ion.rangeslider/css/ion.rangeSlider.css" rel="stylesheet" type="text/css"/>
<link href="/global/static/plugins/ion.rangeslider/css/ion.rangeSlider.Conquer.css" rel="stylesheet" type="text/css"/>
<style>
    .xform-error {
        padding: 3px;
        font-weight: bold !important;
    }

    .btn-tall {
        height: 60px;
    }

    .btn-wide {
        width: 180px;
    }

    .zone-preview {
        position: absolute;
        top: 10px;
        right: 10px;
    }
</style>
</%def>
<%def name="page_js()">
<script src="/global/static/plugins/ion.rangeslider/js/ion-rangeSlider/ion.rangeSlider.min.js"></script>
</%def>


<!-- BEGIN PAGE CONTENT-->
<div class="row">
    <div class="col-md-12">
        <div class="portlet">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-plus"></i> Добавление источника
                </div>
            </div>
            <div class="portlet-body" style="position: relative;">
                <div class="row">
                    <div class="zone-preview hidden-xs hidden-sm col-md-4">
                    </div>
                    <form id="source-form" class="form-horizontal col-md-6 col-md-offset-2" method="post" role="form">
                        ${form.csrf_token}

                        %if g.source.id:
                            <div class="form-group">
                                <label class="control-label col-md-4"><b>ID источника</b></label>
                                <div class="col-md-8">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <label class="control-label"> ${g.source.id}</label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        %endif

                        <div class="form-group" id="type_label">
                            <label class="control-label col-md-4"><b>Тип</b></label>
                            <div class="col-md-8">
                                <div class="row">
                                    <div class="col-md-12">
                                        <label class="control-label">
                                        ${source_type_dict.get(int(source_type))}
                                        </lable>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="form-group ${'has-error' if form.name.errors else ''}">
                            ${form.name.label(class_="col-md-4 control-label")}
                            <div class="col-md-8">
                                <div class="row">
                                    <div class="col-md-12">
                                        ${form.name(class_="form-control")}
                                    </div>
                                </div>
                                <div class="row">
                                    <span class="text-danger col-md-12">
                                        ${", ".join(form.name.errors)}
                                    </span>
                                </div>
                            </div>
                        </div>

                        <div class="form-group ${'has-error' if form.ecpm.errors else ''}">
                            ${form.ecpm.label(class_="col-md-4 control-label")}
                            <div class="col-md-8">
                                <div class="row">
                                    <div class="col-md-12">
                                        ${form.ecpm(class_="form-control")}
                                    </div>
                                </div>
                                <div class="row">
                                    <span class="text-danger col-md-12">
                                        ${", ".join(form.ecpm.errors)}
                                    </span>
                                </div>
                            </div>
                        </div>

                        <%include file="/source/sources/source_${source_type}.mako" />

                        <div class="form-group ${'has-error' if form.type.errors else ''}">
                            <div class="col-md-8">
                                <div class="row">
                                    <div class="col-md-12">
                                        ${form.type(class_="form-control", value=source_type)}
                                    </div>
                                </div>
                                <div class="row">
                                    <span class="text-danger col-md-12">
                                        ${", ".join(form.type.errors)}
                                    </span>
                                </div>
                            </div>
                        </div>

                    </form>
                </div>
                <div class="row">
                    <div class="col-md-12 text-center">
                        <hr/>
                        <button type="submit" data-form="banner" class="btn btn-success btn-tall btn-wide">
                            <i class="fa fa-fw fa-save"></i> Сохранить
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<!-- END PAGE CONTENT-->
<script type="text/javascript">
    (function($, undefined){
        $(function(){
            $('button[type="submit"]').on('click', function() {
                $('#source-form').submit();
            });
        });
    })(jQuery);
</script>