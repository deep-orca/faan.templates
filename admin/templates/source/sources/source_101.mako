
<div class="form-group ${'has-error' if form.unit.errors else ''}">
    ${form.unit.label(class_="col-md-4 control-label")}
    <div class="col-md-8">
        <div class="row">
            <div class="col-md-12">
                ${form.unit(class_="form-control")}
            </div>
        </div>
        <div class="row">
            <span class="text-danger col-md-12">
                ${", ".join(form.unit.errors)}
            </span>
        </div>
    </div>
</div>