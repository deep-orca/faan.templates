<div class="form-group ${'has-error' if form.banner.errors else ''}">
    <label class="control-label col-md-4">Типы креативов</label>
    <div class="col-md-8">
        <div class="row">
            <div class="col-md-4">
                <ul class="list-unstyled">
                    <li>${form.banner(class_="form-control")} <span>${form.banner.label()}<span></li>
                    <li>${form.video(class_="form-control")} <span>${form.video.label()}<span></li>
                    <li>${form.mraid(class_="form-control")} <span>${form.mraid.label()}<span></li>
                </ul>
            </div>
        </div>
        <div class="row">
            <span class="text-danger col-md-12">
                ${", ".join(form.banner.errors)}
            </span>
        </div>
    </div>
</div>
