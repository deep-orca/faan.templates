<%namespace name="bcrumb" file="/layout/bcrumb.mako" />
<%namespace name="fabutton" file="/layout/fa-button.mako" />
<%inherit file="/layout/main.mako" />

<%!
    from faan.core.model.security.account import Account
%>

<%def name="title()">Профиль</%def>
<%def name="description()">
%if not g.input_error:
    Создать нового пользователя
%else:
    Ошибка при создании пользователя: ${g.input_error}
%endif
</%def>

${ bcrumb.h(self) }


<!-- BEGIN PAGE CONTENT-->
<div class="row profile">
	<div class="col-md-12">
			<div class="row profile-account">
				<div class="col-md-3">
					<ul class="ver-inline-menu tabbable margin-bottom-10">
						<li class="active">
							<a data-toggle="tab" href="#tab_1-1">
							<i class="fa fa-cog"></i>Персональная информация </a>
							<span class="after">
							</span>
						</li>
					</ul>
				</div>
				<div class="col-md-9">
					<div class="tab-content">
						<div id="tab_1-1" class="tab-pane active" >

						<div class="portlet">
						<div class="portlet-title">
							<div class="caption">
								<i class="fa fa-plus"></i> Создать пользователя
							</div>
						</div>
						<div class="portlet-body form">
							<form role="form" method="POST" name="create_user">
							${form.csrf_token}
							    <div class="form-body">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group ${'has-error' if form.email.errors else ''}">
                                                <label>Почтовый адрес</label> <span class="required" style="color: red;"> * </span>
                                                <div class="input-group">
                                                    <span class="input-group-addon">
                                                        <i class="fa fa-envelope"></i>
                                                    </span>
                                                    ${form.email(class_="form-control")}
                                                </div>
                                                <span class="help-block">
                                                        %if form.email.errors:
                                                            ${unicode(form.email.errors)}
                                                        %else:
                                                            Введите почтовый адрес пользователя, например contacts@vidiger.com
                                                        %endif
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group ${'has-error' if form.password.errors else ''}">
                                                <label for="password">Пароль</label> <span class="required" style="color: red;"> * </span>
                                                ${form.password(class_="form-control")}
                                                <span class="help-block">
                                                    Введите пароль пользователя
                                                </span>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group ${'has-error' if form.password_confirm.errors else ''}">
                                                <label for="password_confirm">Повторите пароль</label> <span class="required" style="color: red;"> * </span>
                                                ${form.password_confirm(class_="form-control")}
                                                <span class="help-block">
                                                    Повторите пароль пользователя
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group ${'has-error' if form.name.errors else ''}">
                                                <label for="name">Имя</label>
                                                ${form.name(class_="form-control")}
                                                <span class="help-block">
                                                    Введите имя пользователя, например Иван
                                                </span>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group ${'has-error' if form.surname.errors else ''}">
                                                <label for="surname">Фамилия</label>
                                                ${form.surname(class_="form-control")}
                                            </div>
                                            <span class="help-block">
                                                Введите фамилию пользователя, например Иванов
                                            </span>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group ${'has-error' if form.type.errors else ''}">
                                                <label for="type">Тип</label>
                                                ${form.type(class_="form-control")}
                                                <span class="help-block">
                                                    Выбирите тип пользователя
                                                </span>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group ${'has-error' if form.group.errors else ''}">
                                                <label for="group">Группа</label>
                                                ${form.group(class_="form-control")}
                                            </div>
                                            <span class="help-block">
                                                Введите группу для пользователя
                                            </span>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group ${'has-error' if form.campaign.errors else ''}">
                                                <label for="campaign">Кампания</label>
                                                ${form.campaign(class_="form-control")}
                                                <span class="help-block">
                                                    Укажите кампанию пользователя
                                                </span>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group ${'has-error' if form.balance.errors else ''}" id="balanceGroup">
                                                <label for="balance">Баланс</label>
                                                <div class="input-group">
                                                    <span class="input-group-btn">
                                                        <button type="button" class="btn btn-success bootstrap-touchspin-down">-</button>
                                                    </span>
                                                    <span class="input-group-addon bootstrap-touchspin-prefix"><i class="fa fa-rub"></i></span>
                                                    ${form.balance(class_="form-control")}
                                                    <span class="input-group-addon bootstrap-touchspin-postfix"></span>
                                                    <span class="input-group-btn">
                                                        <button type="button" class="btn btn-success bootstrap-touchspin-up">+</button>
                                                    </span>
                                                </div>
                                            </div>
                                            <span class="help-block">
                                                %if form.balance.errors:
                                                    <span style="color: red;">${unicode(form.balance.errors)}</span>
                                                %else:
                                                    Укажите первоначальный баланс пользователя
                                                %endif
                                            </span>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group ${'has-error' if form.description.errors else ''}">
                                                <label for="description">Описание</label>
                                                ${form.description(class_="form-control")}
                                            </div>
                                            <span class="help-block">
                                                Описание пользователя
                                            </span>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group ${'has-error' if form.send_reg_msg.errors else ''}">
                                                ${form.send_reg_msg(class_="form-control")}
                                                <label for="send_reg_msg">Отправить сообщение пользователю</label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">

                                    </div>
								</div>
								<div class="form-actions">
									<button class="btn btn-success" type="submit">Создать</button>
									<button class="btn btn-default" type="button">Отменить</button>
								</div>
							</form>
						</div>
					    </div>

						</div>
					</div>
				</div>
				<!--end col-md-9-->
			</div>
		</div>
</div>
<!-- END PAGE CONTENT-->

<script type="text/javascript">
    $(document).on('ready', function() {
        // Balance operations.
        var defaultBalanceStep = +100;
        $('#balance').val(0);
        $('#balanceGroup button:first').on('click', function() {
            if (+$('#balance').val() - defaultBalanceStep < 0)
                $('#balance').val(0);
            else
                $('#balance').val(+$('#balance').val() - defaultBalanceStep);
        })
        $('#balanceGroup button:last').on('click', function() {
            $('#balance').val(+$('#balance').val() + defaultBalanceStep);
        })
    })
</script>