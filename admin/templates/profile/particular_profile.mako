<%namespace name="bcrumb" file="/layout/bcrumb.mako" />
<%namespace name="fabutton" file="/layout/fa-button.mako" />
<%inherit file="/layout/main.mako" />

<%!
    import time
    from faan.core.model.security.account import Account
    from faan.core.model.security.message import Message
    from faan.core.model.security.account_extra import AccountExtraCommunication as Communication
    from faan.core.model.security.account_extra import AccountPurchasingInfo as Purchase

    account_type = { Account.Type.USER: u'Пользователь',
                     Account.Type.PARTNER: u'Партнер',
                     Account.Type.MANAGER: u'Менеджер',
                     Account.Type.FINANCIAL_MANAGER: u'Финансовый менеджер',
                     Account.Type.ADMINISTRATOR: u'Администратор',
                     Account.Type.DEVELOPER: u'Разработчик',
                     Account.Type.GOD: u'Супер пользователь',
                     }

    account_status = { Account.State.NEW: u'Новый',
                      Account.State.ACTIVE: u'Активный',
                      Account.State.BANNED: u'Заблокированный',
                      Account.State.SUSPENDED: u'Приостановленный',
                     }

    account_group = { Account.Groups.NONE: u'Без группы',
                      Account.Groups.PUB: u'Разработчик',
                      Account.Groups.ADV: u'Рекламодатель',
                     }

    communication_type = { Communication.Account_communication_types.SKYPE: u'Skype',
                           Communication.Account_communication_types.PHONE: u'Телефон',
                           Communication.Account_communication_types.ICQ: u'ICQ',
                           Communication.Account_communication_types.JABBER: u'Jabber',
                           Communication.Account_communication_types.MAIL_RU: u'Mail.ru',
                           Communication.Account_communication_types.WHATSAPP: u'Whatsapp',
                           Communication.Account_communication_types.SITE: u'Сайт',
                          }

    def get_financial_info(stats, days, account_group):
        money = 0
        month_before = int(time.time()) - days * 24 * 60 * 60
        for stat in stats:
            if stat.ts_spawn < month_before:
                break
            if account_group == Account.Groups.PUB:
                money += stat.revenue
            if account_group == Account.Groups.ADV:
                money += stat.cost
        # format
        return '{:,.2f}'.format(money).replace(',', ' ');
%>

<%def name="title()">Профиль</%def>
<%def name="description()">Профиль пользователя ${g.account.username}</%def>

${ bcrumb.h(self) }
${ bcrumb.bc([ { 'name' : u'Пользователи', 	'href' : '/profile/list', 'icon' : 'group' }
             , { 'name' : u'%s'% (g.account.username), 	'href' : '/profile/%s' % g.account.id, 'icon' : 'user' }
             ]) }


<!-- BEGIN THEME STYLES -->
<link href="/global/static/css/style-conquer.css" rel="stylesheet" type="text/css"/>
<link href="/global/static/css/style.css" rel="stylesheet" type="text/css"/>
<link href="/global/static/css/style-responsive.css" rel="stylesheet" type="text/css"/>
<link href="/global/static/css/plugins.css" rel="stylesheet" type="text/css"/>
<link href="/global/static/css/themes/default.css" rel="stylesheet" type="text/css" id="style_color"/>
<link href="/global/static/css/pages/profile.css" rel="stylesheet" type="text/css"/>
<link href="/global/static/css/custom.css" rel="stylesheet" type="text/css"/>
<!-- END THEME STYLES -->

<link href="/global/static/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
<link href="/global/static/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
<link href="/global/static/plugins/uniform/css/uniform.default.css" rel="stylesheet" type="text/css"/>
<!-- END GLOBAL MANDATORY STYLES -->
<!-- BEGIN PAGE LEVEL STYLES -->
<link href="/global/static/plugins/bootstrap-modal/css/bootstrap-modal-bs3patch.css" rel="stylesheet" type="text/css"/>
<link href="/global/static/plugins/bootstrap-modal/css/bootstrap-modal.css" rel="stylesheet" type="text/css"/>
<!-- END PAGE LEVEL STYLES -->
<!-- BEGIN THEME STYLES -->
<link href="/global/static/css/style-conquer.css" rel="stylesheet" type="text/css"/>
<link href="/global/static/css/style.css" rel="stylesheet" type="text/css"/>
<link href="/global/static/css/style-responsive.css" rel="stylesheet" type="text/css"/>
<link href="/global/static/css/plugins.css" rel="stylesheet" type="text/css"/>
<link href="/global/static/css/themes/default.css" rel="stylesheet" type="text/css" id="style_color"/>
<link href="/global/static/css/custom.css" rel="stylesheet" type="text/css"/>


<!-- BEGIN PAGE CONTENT-->
<div class="row profile">
    <div class="col-md-12">
        <!--BEGIN TABS-->
        <div class="tabbable tabbable-custom">
            <ul class="nav nav-tabs">
                <li class="active">
                    <a href="#tab_1_1" data-toggle="tab">Обзор</a>
                </li>
                <li>
                    <a href="#tab_1_3" data-toggle="tab">Настройки</a>
                </li>
            </ul>
            <div class="tab-content">
                <div class="tab-pane active" id="tab_1_1">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="row">

                                <div class="col-md-8 profile-info" style="line-height: 30px;">
                                    <div class="col-md-6">
                                        <h3> Аккаунт: ${g.account.username} </h3>
                                        <h4 class="form-section">Персональная информация</h4>
                                        <div class="row">
                                            <b class="col-md-5">
                                                Имя
                                            </b>
                                            <div class="col-md-7">
                                                %if g.extra:
                                                    ${g.extra.name or ''}
                                                    ${g.extra.surname or ''}
                                                %endif
                                            </div>
                                        </div>
                                        <div class="row">
                                            <b class="col-md-5">
                                                Тип аккаунта
                                            </b>
                                            <div class="col-md-7">
                                                ${account_type.get(g.account.type, 'Unknown')}
                                            </div>
                                        </div>
                                        <div class="row">
                                            <b class="col-md-5">
                                                Группа аккаунта:
                                            </b>
                                            <div class="col-md-7">
                                                ${account_group.get(g.account.groups, 'Unknown')}
                                            </div>
                                        </div>
                                        <div class="row">
                                            <b class="col-md-5">
                                                Зарегистрирован:
                                            </b>
                                            <div class="col-md-7">
                                                %if g.account.date_registration:
                                                    ${g.account.date_registration.date()}
                                                %endif
                                            </div>
                                        </div>
                                        <div class="row">
                                            <b class="col-md-5">
                                                О себе:
                                            </b>
                                            <div class="col-md-7">
                                                ${g.extra.description or ""}
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <div class="row" style="margin-top: 16px;">
                                            <div class="col-md-3">
                                                <a data-toggle="modal" href="#change_account_status"><span class='badge badge-${u"success" if g.account.state==1 else u"danger"}'>${account_status.get(g.account.state, 'Unknown')}</span></a>
                                            </div>
                                            <div class="col-md-9">
                                                %if g.account.type == Account.Type.PARTNER:
                                                    %if g.account.partner_group_id:
                                                        %if g.account.groups == Account.Groups.PUB:
                                                        <span class='badge badge-primary'><a href="/partners/pub/?group=${g.partner_group.id}&tab=all" style="color:#fff">Группа ${g.partner_group.name}</a></span>
                                                        %elif g.account.groups == Account.Groups.ADV:
                                                        <span class='badge badge-primary'><a href="/partners/adv/?group=${g.partner_group.id}&tab=all" style="color:#fff">Группа ${g.partner_group.name}</a></span>
                                                        %endif
                                                        <small></small>
                                                        <small><a data-toggle="modal" href="#responsive">Сменить</a></small>
                                                    %else:
                                                        <span class='badge badge-warning'>Без группы</span><small><a  data-toggle="modal" href="#responsive">Назначить</a></small>
                                                    %endif
                                                %endif
                                            </div>
                                            <div class="col-md-3">
                                                <a data-toggle="modal" href="#delete_account"><span class='badge badge-danger'>Удалить</span></a>
                                            </div>
                                        </div>

                                        <h4 class="form-section">Контактная информация</h4>
                                        <div class="row">
                                            <b class="col-md-5">
                                                e-mail:
                                            </b>
                                            <div class="col-md-7">
                                                ${g.account.email}
                                            </div>
                                        </div>
                                        %for com in g.communications:
                                        <div class="row">
                                            <b class="col-md-5">
                                                ${communication_type.get(com.type, 'Unknown')}
                                            </b>
                                            <div class="col-md-7">
                                                ${com.value}
                                            </div>
                                        </div>
                                        %endfor
                                    </div>
                                </div>
                                <!--end col-md-8-->
                                <div class="col-md-4">
                                    <div class="portlet sale-summary">
                                        <div class="portlet-title">
                                            <div class="caption">
                                                Платежная информация
                                            </div>
                                            <div class="tools">
                                                <a class="reload" href="javascript:;"></a>
                                            </div>
                                        </div>
                                        <div class="portlet-body">
                                            <ul class="list-unstyled">
                                                <li>
                                                    <span class="sale-info">
                                                         Баланс
                                                    </span>
                                                    <span class="sale-num">
                                                         ${'{:,.2f}'.format(g.account.balance).replace(',', ' ')} <i class="fa fa-rub"></i>
                                                    </span>
                                                </li>
                                                %if g.account.groups == Account.Groups.PUB:
                                                    <li>
                                                        <span class="sale-info">
                                                             Доход за неделю
                                                        </span>
                                                        <span class="sale-num">
                                                             ${get_financial_info(g.stats, 6, Account.Groups.PUB)} <i class="fa fa-rub"></i>
                                                        </span>
                                                    </li>
                                                    <li>
                                                        <span class="sale-info">
                                                             Доход за месяц
                                                        </span>
                                                        <span class="sale-num">
                                                             ${get_financial_info(g.stats, 30, Account.Groups.PUB)}  <i class="fa fa-rub"></i>
                                                        </span>
                                                    </li>
                                                %elif g.account.groups == Account.Groups.ADV:
                                                    <li>
                                                        <span class="sale-info">
                                                             Расход за неделю
                                                        </span>
                                                        <span class="sale-num">
                                                             ${get_financial_info(g.stats, 6, Account.Groups.ADV)} <i class="fa fa-rub"></i>
                                                        </span>
                                                    </li>
                                                    <li>
                                                        <span class="sale-info">
                                                             Расход за месяц
                                                        </span>
                                                        <span class="sale-num">
                                                             ${get_financial_info(g.stats, 30, Account.Groups.ADV)} <i class="fa fa-rub"></i>
                                                        </span>
                                                    </li>
                                                %endif
                                                <li>
                                                    <span class="sale-info">
                                                          <a href="/balance/?account=${g.account.id}">История платежей</a><i class="fa fa-img-up"></i>
                                                    </span>
                                                </li>
                                                <li>
                                                    <span class="sale-info">
                                                        <a data-toggle="modal" href="#change_balance" class="btn btn-info">Изменить баланс</a>
                                                    </span>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <!--end col-md-4-->
                            </div>
                            <!--end row-->
                            <div class="row">
                                <div class="col-md-8" style="padding-left: 30px;padding-right: 30px;">
                                    <h4 class="form-section">Коментарии</h4>
                                    <ul class="feeds">
                                        %for comment in g.comments:
                                        <li>
                                            <div class="col1">
                                                <div class="cont">
                                                    <div class="cont-col1">
                                                        <div class="label label-success">
                                                            <i class="fa fa-bell"></i>
                                                        </div>
                                                    </div>
                                                    <div class="cont-col2">
                                                        <div class="desc">
                                                             ${comment.text}
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col2" style="width: 95px;margin-left: -95px;">
                                                <div class="date">
                                                     ${comment.date.date()}
                                                </div>
                                            </div>
                                            </a>
                                        </li>
                                        %endfor
                                    </ul>

                                            <!--tab-pane-->

                                    <div class="form-actions" style="background-color: #fff;">

                                            <a href="/profile/create_comment?consumer_id=${g.account.id}&back=/profile/${g.account.id}" class="btn btn-info" style="border:0;color:#fff;">
                                                        Добавить комментарий
                                            </a>

                                    </div>

                                </div>
                                <div class="col-md-4">
                                    <ul class="list-unstyled profile-nav">
                                        <li>
                                            <a href="/app/?tab=all&account=${g.account.id}">Приложения</a>
                                        </li>
                                        <li>
                                            <a href="/media/list/?tab=2&account=${g.account.id}">Креативы</a>
                                        </li>
                                        <li>
                                            %if g.account.groups == Account.Groups.PUB:
                                                <a href="/stats/pub/days/?account=${g.account.id}">Статистика</a>
                                            %elif g.account.groups == Account.Groups.ADV:
                                                <a href="/stats/adv/days/?account=${g.account.id}">Статистика</a>
                                            %else:
                                                <a href="#">Статистика</a>
                                            %endif
                                        </li>
                                        <li>
                                            <a href="#">
                                                Сообщения
                                                %if g.messages > 0:
                                                    <span>
                                                    ${g.messages}
                                                    </span>
                                                %endif
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">

                        </div>
                    </div>
                </div>
                <!--tab_1_2-->
                <div class="tab-pane" id="tab_1_3">
                    <div class="row profile-account">
                        <div class="col-md-3">
                            <ul class="ver-inline-menu tabbable margin-bottom-10">
                                <li class="active">
                                    <a data-toggle="tab" href="#tab_1-1">
                                    <i class="fa fa-cog"></i>Персональная информация </a>
                                    <span class="after">
                                    </span>
                                </li>
                                <li>
                                    <a data-toggle="tab" href="#tab_4-4"><i class="fa fa-picture-o"></i>Контактная информация</a>
                                </li>
                                <li>
                                    <a data-toggle="tab" href="#tab_2-2"><i class="fa fa-picture-o"></i>Платежная информация</a>
                                </li>
                                <li>
                                    <a data-toggle="tab" href="#tab_3-3"><i class="fa fa-lock"></i>Смена пароля</a>
                                </li>
                            </ul>
                        </div>
                        <div class="col-md-9">
                            <div class="tab-content">
                                <div id="tab_1-1" class="tab-pane active">
                                    <form role="form" method="POST" action="/profile/${g.account.id}/_profile_post_settings">
                                        <div class="form-group">
                                            <label class="control-label">Имя</label>
                                            <input name="name" type="text" placeholder="Иван" class="form-control" value="${g.extra.name}"/>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label">Фамилия</label>
                                            <input name="surname" type="text" placeholder="Иванов" class="form-control" value="${g.extra.surname}"/>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label">Email</label>
                                            <input name="email" type="text" placeholder="example@example.com" class="form-control" value="${g.account.email}"/>
                                        </div>
                                        ## TODO: replace with calendar
                                        <div class="form-group">
                                            <label class="control-label">Дата регистрации</label>
                                            %if g.account.date_registration:
                                                <input disabled name="registration" type="text" class="form-control" value="${g.account.date_registration.date()}"/>
                                            %else:
                                                <input name="registration" type="text" class="form-control" value=""/>
                                            %endif
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label">Тип аккаунта</label>
                                            <select name="type" class="form-control input-medium select2me" data-placeholder="">
                                                <option value="${g.account.type}">${'%s' % account_type.get(g.account.type, 'Unknown')}</option>
                                                <%
                                                    types = account_type.items()
                                                    types.remove((g.account.type, account_type.get(g.account.type, 'Unknonw')))
                                                %>
                                                % for key, value in types:
                                                <option value="${key}">${value}</option>
                                                % endfor
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label">Статус</label>
                                            <select name="state" class="form-control input-medium select2me" data-placeholder="">
                                                %if g.account.state:
                                                   <option value="${g.account.state}">${account_status.get(g.account.state, 'Unknown')}</option>
                                                %endif
                                                <%
                                                    statuses = account_status.items()
                                                    if g.account.state:
                                                        statuses.remove((g.account.state, account_status.get(g.account.state, 'Unknown')))
                                                %>
                                                % for key, value in statuses:
                                                <option value="${key}">${value}</option>
                                                % endfor
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label">Группа</label>
                                            <select name="group" class="form-control input-medium select2me" data-placeholder="">
                                                %if g.account.groups:
                                                    <option value="${g.account.groups}">${account_group.get(g.account.groups, 'Unknown')}</option>
                                                %endif
                                                <%
                                                    groups = account_group.items()
                                                    if g.account.groups:
                                                        groups.remove((g.account.groups, account_group.get(g.account.groups, 'Unknown')))
                                                %>
                                                % for key, value in groups:
                                                <option value="${key}">${value}</option>
                                                % endfor
                                            </select>
                                        </div>
                                        <!--
                                        %if g.account.type == Account.Type.PARTNER:
                                            <div class="form-group">
                                                <label class="control-label">Группа партнера</label>
                                                <select name="partner_group_id" class="form-control input-medium select2me" data-placeholder="">
                                                    %if g.extra.partner_group_id:
                                                        <option value="${g.partner_group.id}">${g.partner_group.name}</option>
                                                        <%
                                                            pgroups = [(pgroup.id, pgroup.name) for pgroup in g.partner_groups if pgroup.id != g.partner_group.id]
                                                        %>
                                                    %else:
                                                        <%
                                                            pgroups = [(pgroup.id, pgroup.name) for pgroup in g.partner_groups]
                                                        %>
                                                    %endif
                                                    % for key, value in pgroups:
                                                    <option value="${key}">${value}</option>
                                                    % endfor
                                                </select>
                                            </div>
                                        %endif
                                        --!>
                                        <div class="form-group">
                                            <label class="control-label">Процент отчислений</label>
                                            <input name="rate" type="text" class="form-control" value="${g.account.rate or ''}" style="width:240px"/>
                                        </div>
                                        <div class="form-group">
                                            ##<input id="request_rate_checkbox" name="send_email" type="checkbox" class="form-control" value=""/>
                                            <label class="control-label">Установить цену за запрос</label>
                                            <input id="request_rate" name="request_cost" type="text" class="form-control" value="${g.account.request_cost or ''}" style="width:240px"/>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label">Установить цену за overlay</label>
                                            <input name="overlay_cost" type="text" class="form-control" value="${g.account.overlay_cost or ''}" style="width:240px"/>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label">Установить цену за endcard</label>
                                            <input name="endcard_cost" type="text" class="form-control" value="${g.account.endcard_cost or ''}" style="width:240px"/>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label">Просмотренно после %</label>
                                            <input name="completion_ratio" type="text" class="form-control" value="${g.account.completion_ratio or ''}" style="width:240px"/>
                                        </div>

                                        <div class="margiv-top-10">
                                            <button type="submit"  class="btn btn-success">Сохранить</button>
                                            <a href="/profile/${g.account.id}" class="btn btn-default">Отмена</a>
                                        </div>
                                        <input type="hidden" class="form-control" name="post_form" value="_profile_post_settings">
                                        <input type="hidden" class="form-control" name="back" value="/profile/${g.account.id}/#tab_1-1">
                                    </form>
                                </div>
                                <div id="tab_4-4" class="tab-pane">
                                    <form role="form" method="POST" action="/profile/${g.account.id}/_profile_post_communication">
                                        <div class="form-group">
                                            <label class="control-label">Основной e-mail</label>
                                            <input name="email" type="text" placeholder="" class="form-control" value="${g.account.email}"/>
                                        </div>
                                        %for com in g.communications:
                                        <div class="form-group">
                                            <a href="/profile/delete_communication/${com.id}?back=/profile/${g.account.id}/#tab_4-4"><span class='badge badge-danger'>--</span></a>
                                            <label class="control-label">${communication_type.get(com.type, 'Unknown')}</label>

                                            <input name="communication_${com.id}" type="text" placeholder="" class="form-control" value="${com.value}"/>
                                        </div>
                                        %endfor

                                        <div class="margiv-top-10">
                                            <a data-toggle="modal" href="#add_communication" class="btn btn-info">Добавить</a>
                                            <button type="submit"  class="btn btn-success">Сохранить</button>
                                            <a href="/profile/${g.account.id}" class="btn btn-default">Отмена</a>
                                        </div>
                                        <input type="hidden" class="form-control" name="post_form" value="_profile_post_communication">
                                        <input type="hidden" class="form-control" name="account_extra_id" value="${g.extra.id}">
                                        <input type="hidden" class="form-control" name="back" value="/profile/${g.account.id}/#tab_4-4">
                                    </form>
                                </div>
                                <div id="tab_2-2" class="tab-pane">
                                    <form role="form" method="POST" action="/profile/${g.account.id}/_profile_post_purchase">

                                        %if g.purchases:
                                            %for purchase in g.purchases:
                                            <div class="form-group">
                                                %if purchase.state == Purchase.State.ACTIVE:
                                                    <span class='badge badge-success'>Активный</span>
                                                %else:
                                                    <a href="/profile/active_purchase/${g.extra.id}/${purchase.id}?back=/profile/${g.account.id}/#tab_2-2"><span class='badge badge-info'>Активировать</span></a>
                                                %endif
                                                <a href="/profile/delete_purchase/${purchase.id}?back=/profile/${g.account.id}/#tab_2-2"><span class='badge badge-danger'>Удалить</span></a>
                                                <label class="control-label">${Purchase.Type.get_label_by_type(purchase.type)}</label>
                                                <input name="purchase_${purchase.id}" type="text" placeholder="" class="form-control" value="${purchase.value}"/>
                                            </div>
                                            %endfor
                                        %endif

                                        <div class="margiv-top-10">
                                            <a data-toggle="modal" href="#add_purchase" class="btn btn-info">Добавить</a>
                                            <button type="submit"  class="btn btn-success">Сохранить</button>
                                            <a href="/profile/${g.account.id}" class="btn btn-default">Отмена</a>
                                        </div>
                                        <input type="hidden" class="form-control" name="post_form" value="_profile_post_purchase">
                                        <input type="hidden" class="form-control" name="account_extra_id" value="${g.extra.id}">
                                        <input type="hidden" class="form-control" name="back" value="/profile/${g.account.id}/#tab_2-2">
                                    </form>
                                </div>

                                <div id="tab_3-3" class="tab-pane">
                                    <form role="form" method="POST" action="/profile/${g.account.id}/_profile_post_password">
##									    <div class="form-group">
##											<label class="control-label">Текущий пароль</label>
##											<input name="old" type="password" class="form-control" value=""/>
##										</div>
                                        <div class="form-group">
                                            <label class="control-label">Новый пароль</label>
                                            <input name="pass0" type="password" class="form-control"/>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label">Повторите пароль</label>
                                            <input name="pass1" type="password" class="form-control"/>
                                        </div>

                                        <div class="margiv-top-10">
                                            <button type="submit"  class="btn btn-success">Сохранить</button>
                                            <a href="/profile/${g.account.id}" class="btn btn-default">Отмена</a>
                                        </div>
                                        <input type="hidden" class="form-control" name="post_form" value="_profile_post_password">
                                        <input type="hidden" class="form-control" name="back" value="/profile/${g.account.id}/#tab_3-3">
                                    </form>

                                </div>
                            </div>
                        </div>
                        <!--end col-md-9-->
                    </div>
                </div>
                <!--end tab-pane-->
            </div>
        </div>
        <!--END TABS-->
    </div>
</div>
<!-- END PAGE CONTENT-->

%if g.account.type == Account.Type.PARTNER:
<!-- responsive -->
<div id="responsive" class="modal fade" tabindex="-1" data-width="760" style="top:25%">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
        <h4 class="modal-title">Назначить группу</h4>
    </div>
    <form method="POST" class="form-horizontal" action="/profile/${g.account.id}/_profile_post_group">
        <div class="modal-body">
            <div class="row">
                <div class="col-md-6">
                    <h4>Группы</h4>
                    <p>
                        <select name="assign_group_id" class="form-control input-medium select2me" data-placeholder="">
                            % for group in g.partner_groups:
                            <option value="${group.id}">${'%s - %s%%' % (group.name , group.rate or 0)}</option>
                            % endfor
                        </select>
                    </p>
                    <input type="hidden" class="form-control" name="post_form" value="_profile_post_group">
                    <input type="hidden" class="form-control" name="extra_id" value="${g.extra.id}">
                    <input type="hidden" class="form-control" name="back" value="/profile/${g.account.id}/#tab_1_1">
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" data-dismiss="modal" class="btn btn-default">Отменить</button>
            <button type="submit" id="sample_editable_1_new" class="btn btn-success">Сохранить</button>
        </div>
    </form>
</div>
%endif

<!-- change_account_status -->
<div id="change_account_status" class="modal fade" tabindex="-1" data-width="760" style="top:25%">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
        <h4 class="modal-title">Изменить статус</h4>
    </div>
    <form method="POST" class="form-horizontal" action="/profile/${g.account.id}/_profile_post_status">
        <div class="modal-body">
            <div class="row">
                <div class="col-md-6">
                    <h4>Статус</h4>
                    <p>
                        <select name="assigned_status" class="form-control input-medium select2me" data-placeholder="${account_status.get(g.account.state, 'Unknown')}">
                            % for status_id, status_value in account_status.items():
                            <option value="${status_id}">${'%s' % (status_value)}</option>
                            % endfor
                        </select>
                    </p>
                    <input type="hidden" class="form-control" name="post_form" value="_profile_post_status">
                    <input type="hidden" class="form-control" name="account_id" value="${g.account.id}">
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" data-dismiss="modal" class="btn btn-default">Отменить</button>
            <button type="submit" id="sample_editable_11_new" class="btn btn-success">Сохранить</button>
            <input type="hidden" class="form-control" name="back" value="/profile/${g.account.id}/#tab_1_1">
        </div>
    </form>
</div>

<!-- add_communication -->
<div id="add_communication" class="modal fade" tabindex="-1" data-width="760" style="top:25%">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
        <h4 class="modal-title">Добавить контактную информацию</h4>
    </div>
    <form method="POST" class="form-horizontal" action="/profile/${g.account.id}/_profile_post_add_communication">
        <div class="modal-body">
            <div class="row">
                <div class="col-md-6">
                    <h4>Тип</h4>
                    <p>
                        <select name="type" class="form-control input-medium select2me" data-placeholder="">
                            % for id, value in communication_type.items():
                            <option value="${id}">${'%s' % (value)}</option>
                            % endfor
                        </select>
                    </p>
                </div>
                <div class="col-md-6">
                    <h4>Значение</h4>
                    <p>
                        <input name="value" type="text" placeholder="" class="form-control" value=""/>
                    </p>
                </div>
            </div>
            <input type="hidden" class="form-control" name="post_form" value="_profile_post_add_communication">
            <input type="hidden" class="form-control" name="extra_id" value="${g.extra.id}">
            <input type="hidden" class="form-control" name="back" value="/profile/${g.account.id}/#tab_4-4">
        </div>
        <div class="modal-footer">
            <button type="button" data-dismiss="modal" class="btn btn-default">Отменить</button>
            <button type="submit" id="sample_editable_111_new" class="btn btn-success">Сохранить</button>
        </div>
    </form>
</div>

<!-- add_purchase -->
<div id="add_purchase" class="modal fade" tabindex="-1" data-width="760" style="top:25%">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
        <h4 class="modal-title">Добавить Платежную информацию</h4>
    </div>
    <form method="POST" class="form-horizontal" action="/profile/${g.account.id}/_profile_post_add_purchase">
        <div class="modal-body">
            <div class="row">
                <div class="col-md-6">
                    <h4>Тип</h4>
                    <p>
                        <select name="type" class="form-control input-medium select2me" data-placeholder="">
                            % for id, value in Purchase.Type.get_dict_type().items():
                            <option value="${id}">${'%s' % (value)}</option>
                            % endfor
                        </select>
                    </p>
                </div>
                <div class="col-md-6">
                    <h4>Значение</h4>
                    <p>
                        <input name="value" type="text" placeholder="" class="form-control" value=""/>
                    </p>
                </div>
            </div>
            <input type="hidden" class="form-control" name="post_form" value="_profile_post_add_purchase">
            <input type="hidden" class="form-control" name="extra_id" value="${g.extra.id}">
            <input type="hidden" class="form-control" name="back" value="/profile/${g.account.id}/#tab_2-2">
        </div>
        <div class="modal-footer">
            <button type="button" data-dismiss="modal" class="btn btn-default">Отменить</button>
            <button type="submit" id="sample_editable_1111_new" class="btn btn-success">Сохранить</button>
        </div>
    </form>
</div>

<!-- Change balance -->
<div id="change_balance" class="modal fade" tabindex="-1" data-width="460" style="top:25%">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
        <h4 class="modal-title">Изменить Баланс</h4>
    </div>
    <form method="POST" class="form-horizontal" action="/profile/${g.account.id}/_profile_post_change_balance">
        <div class="modal-body">
            <div class="row">
                <div class="col-md-12">
                    <h4>Баланс</h4>
                    <p>
                        <input name="value" type="text" placeholder="" class="form-control" value="0"/>
                    </p>
                    <p>
                        <textarea class="form-control" name="comment" id="" cols="30" rows="2" placeholder="Комментарий"></textarea>
                        <small>По умолчанию - "Ручное изменение баланса"</small>
                    </p>
                </div>
            </div>
            <input type="hidden" class="form-control" name="post_form" value="_profile_post_change_balance">
            <input type="hidden" class="form-control" name="back" value="/profile/${g.account.id}/#tab_1_1">
        </div>
        <div class="modal-footer">
            <button type="button" data-dismiss="modal" class="btn btn-default">Отменить</button>
            <button type="submit" id="sample_editable_1111_new" class="btn btn-success">Изменить</button>
        </div>
    </form>
</div>


<div id="delete_account" class="modal fade" tabindex="-1" data-width="760" style="top:25%">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
        <h4 class="modal-title">Удалить аккаунт</h4>
    </div>
    <form method="POST" class="form-horizontal" action="/profile/${g.account.id}/_profile_post_delele_account">
        <div class="modal-body">
            <div class="row">
                <div class="col-md-10">
                    <p>
                        Вы действительно хотите удалить аккаунт <b>${g.account.email}</b> ?
                    </p>
                    <input type="hidden" class="form-control" name="post_form" value="_profile_post_delele_account">
                    <input type="hidden" class="form-control" name="back" value="/profile/list/">
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" data-dismiss="modal" class="btn btn-default">Отменить</button>
            <button type="submit" id="sample_editable_111_new" class="btn btn-danger">Удалить</button>
        </div>
    </form>
</div>


<script type="text/javascript">
$('#request_rate_checkbox').click(function() {
    var $this = $(this);
    if ($this.is(':checked')) {
        $this.val("1");
        $('#request_rate').removeAttr('disabled');
    } else {
        $this.val("");
        $('#request_rate').attr('disabled','disabled');
        $('#request_rate').val('');
    }
});
</script>

<script type="text/javascript">
    $(document).on("ready", function() {
       ## $('#request_rate').attr('disabled','disabled');
    });
</script>