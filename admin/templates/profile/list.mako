<%namespace name="bcrumb" file="/layout/bcrumb.mako" />
<%namespace name="fabutton" file="/layout/fa-button.mako" />
<%inherit file="/layout/main.mako" />

<%!
    from faan.core.model.security.account import Account
%>

<%
    for s in g.selects:
        if s.get('name') == 'group':
            s.update({'header': u'Группа'})
%>

<%def name="title()">Список пользователей</%def>
<%def name="description()"></%def>

<script type="text/javascript" src="/static/scripts/filters.js"></script>

<link href="/global/static/plugins/bootstrap-modal/css/bootstrap-modal-bs3patch.css" rel="stylesheet" type="text/css"/>
<link href="/global/static/plugins/bootstrap-modal/css/bootstrap-modal.css" rel="stylesheet" type="text/css"/>

${ bcrumb.h(self) }
${ bcrumb.bc([ { 'name' : u'Пользователи',     'href' : '/profile/list/', 'icon' : 'bullhorn' }
             ]) }

<%def name="show_select_item(select)">
    <div class="form-group">
        <label for="${select['name']}" class="control-label col-md-3">${select['header']}</label>
        <div class="col-md-3">
                <input name="${select['name']}" type="hidden" id="${select['id']}" style="width:324px"/>
        </div>
    </div>
</%def>

<%def name="render_table(id)">
 <!--START TABLE-->
 <table class="table table-striped table-bordered table-hover" id="${id}">
 <thead>
 <tr>
     <th>ID</th>
     <th>Логин</th>
     <th>Статус</th>
     <th>Баланс</th>
     <th>Группа партнера</th>
     <th>Действия</th>
 </tr>
 </thead>
 <tbody>

${caller.body()}

 </tbody>
 </table>
 <!--END TABLE-->

</%def>

<%def name="show_filters(selects)">
    %for select in selects:
        ${self.show_select_item(select)}
    %endfor
    <!-- ==== SUBMIT ==== --->
    <label class="control-label col-md-3"></label>
    <div class="form-group">
        <div class="col-md-2">
            <a href="#" id="apply_changes" class="btn btn-success">Применить</a>
        </div>
	    <div class="col-md-3">
          <a href="/profile/create" class="btn btn-info">Создать нового пользователя</a>
		</div>
	</div>

</%def>

<!-- BEGIN PAGE CONTENT-->
<div class="row">
<div class="col-md-12">
    <div class="portlet">														<!--BEGIN TABLE-->
        <div class="portlet-title">
            <div class="caption"><i class="fa fa-rocket"></i>
            	Пользователи

            </div>
        </div>

        <div class="portlet-body">
        	<form method="GET" class="form-horizontal">
                ${self.show_filters(g.selects)}
			</form>
                <ul class="nav nav-pills">
                    %if getattr(g, 'tab', '') and g.tab == 'new':
                    <li class="active">
                    %else:
                    <li class="">
                    %endif
                        <a href="#tab_2_1" data-toggle="tab" id="tab_new">Новые</a>
                    </li>
                    %if getattr(g, 'tab', '') and g.tab == 'all':
                    <li class="active">
                    %else:
                    <li class="">
                    %endif
                        <a href="#tab_2_2" data-toggle="tab" id="tab_all">Активные</a>
                    </li>
                    %if getattr(g, 'tab', '') and g.tab == 'banned':
                    <li class="active">
                    %else:
                    <li class="">
                    %endif
                        <a href="#tab_2_3" data-toggle="tab" id="tab_banned">Заблокированные</a>
                    </li>
                </ul>
                <div class="tab-content">
                    %if getattr(g, 'tab', '') and g.tab == 'new':
                    <div class="tab-pane fade active in" id="tab_2_1">
                    %else:
                    <div class="tab-pane fade" id="tab_2_1">
                    %endif
                        <p>
                        <%self:render_table id="stats_table_new">

                        </%self:render_table>
                        </p>
                        <div class="row">
                            <div class="col-md-6 col-sm-12"></div>
                            <div class="col-md-6 col-sm-12">
                                <div id="log_table_filter_banned" class="dataTables_filter">
                                    <label>
                                    &nbsp
                                        <ul class="pagination">
                                            <li class="prev disabled"><a title="Prev" href="#"><i class="fa fa-angle-left"></i></a></li>
                                            <li class="next"><a title="Next" href="#"><i class="fa fa-angle-right"></i></a></li>
                                        </ul>
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>
                    %if getattr(g, 'tab', '') and g.tab == 'all':
                    <div class="tab-pane fade active in" id="tab_2_2">
                    %else:
                    <div class="tab-pane fade" id="tab_2_2">
                    %endif
                        <p>
                        <%self:render_table id="stats_table_all">

                        </%self:render_table>
                        </p>
                        <div class="row">
                            <div class="col-md-6 col-sm-12"></div>
                            <div class="col-md-6 col-sm-12">
                                <div id="log_table_filter_banned" class="dataTables_filter">
                                    <label>
                                    &nbsp
                                        <ul class="pagination">
                                            <li class="prev disabled"><a title="Prev" href="#"><i class="fa fa-angle-left"></i></a></li>
                                            <li class="next"><a title="Next" href="#"><i class="fa fa-angle-right"></i></a></li>
                                        </ul>
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>
                    %if getattr(g, 'tab', '') and g.tab == 'banned':
                    <div class="tab-pane fade active in" id="tab_2_3">
                    %else:
                    <div class="tab-pane fade" id="tab_2_3">
                    %endif
                        <p>
                        <%self:render_table id="stats_table_banned">

                        </%self:render_table>
                        </p>
                        <div class="row">
                            <div class="col-md-6 col-sm-12"></div>
                            <div class="col-md-6 col-sm-12">
                                <div id="log_table_filter_banned" class="dataTables_filter">
                                    <label>
                                    &nbsp
                                        <ul class="pagination">
                                            <li class="prev disabled"><a title="Prev" href="#"><i class="fa fa-angle-left"></i></a></li>
                                            <li class="next"><a title="Next" href="#"><i class="fa fa-angle-right"></i></a></li>
                                        </ul>
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
        </div>
    </div>
</div>
</div>
<!-- END PAGE CONTENT-->

<div id="delete_user" class="modal fade" tabindex="-1" data-width="760" style="top:25%">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
        <h4 class="modal-title">Удалить пользователя</h4>
    </div>
    <form method="GET" class="form-horizontal">
        <div class="modal-body">
            <div class="row">
                <label class="control-label col-md-2"></label>
                <div>
                    <p>
                        Вы действительно хотите удалить пользователя ?
                    </p>
                    ##<input type="hidden" class="form-control" name="back" value="/profile/${g.account.id}/#tab_1_1">
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" data-dismiss="modal" class="btn btn-default">Отменить</button>
            <button type="submit" id="button_delete_user" class="btn btn-danger">Удалить</button>
        </div>
    </form>
</div>


<!-- SCRIPTS -->
<script type="text/javascript">
Index = function() {
    return {

        tab: null,

        // Pagination options
        pag_size: +50,
        pag_start: 0,

        // Some table attributes
        data: null,
        total: +0,

        init: function() {
            $("#apply_changes").on("click", function(e) {
                Index["loadData_" + Index.tab]()
            });

            %for tab in ('all', 'new', 'banned',):
            $("#tab_${tab}").on("click", function(e) {
                Index.tab = "${tab}";
                Index.pag_size = + 50;
                Index.pag_start = + 0;
                Index.loadData_${tab}();
            });
            %endfor

            %if getattr(g, 'tab', ''):
                this.tab = "${g.tab}";
            %else:
                this.tab = "new";
            %endif
        },

        %for tab in ('all', 'new', 'banned',):
        loadData_${tab}: function(start, count, search) {
            try { $("#stats_table_${tab}").DataTable().destroy(); } catch (e) {}

            start = start ? start : 0;
            count = count ? count : Index.pag_size;
            search = search ? search : "";

            var url = "/profile/ajax/table?tab=${tab}" + filterBox.getParametersHTTP() + "&start=" + start + "&count=" + count + "&search=" + search;

            c = [
               { data: 'id' },
               { data: 'name' },
               { data: 'state' },
               { data: 'balance' },
               { data: 'group' },
               { data: 'action' }
            ];

            $("#stats_table_${tab}").dataTable({
                processing: true,
                ajax: {
                    url: url,
                    dataSrc: function(response) {
                        Index.total = response.meta.total;
                        return response.data;
                    }
                },
                columns: c,
                lengthChange: true,
                "aLengthMenu": [
                    [25, 50, 100, -1],
                    [25, 50, 100, "All"] // change per page values here
                ],
                // set the initial value
                "iDisplayLength": 50,
                "sPaginationType": "bootstrap",
                "oLanguage": {
                    "sLengthMenu": "_MENU_ records",
                    "oPaginate": {
                        "sPrevious": "Prev",
                        "sNext": "Next"
                    }
                },
                "aoColumnDefs": [{
                        'bSortable': false,
                        'aTargets': [0]
                    }
                ],
                order: [],
                paging: false,
                bFilter: false,
                bInfo: false
            });

            // Add select items.
            $("#stats_table_${tab}_wrapper .row")[0].children[0].innerHTML = '<div class="dataTables_length" id="stats_table_${tab}_length"><label><select name="stats_table_${tab}_length" id="_stats_table_${tab}_length" aria-controls="stats_table_${tab}" class="form-control input-xsmall"><option value="50">50</option><option value="100">100</option><option value="150">150</option></select> records</label></div>'
            $('#stats_table_${tab}_wrapper .dataTables_length select').select2().on("select2-selecting", function(e) {
                Index.pag_size = + e.val;
                Index.pag_start = + 0;
                Index.loadData_${tab}(0, Index.pag_size);
                $('#stats_table_${tab}_wrapper .dataTables_length select').select2("val", e.val.toString());
            });

            //Add search item.
            $("#stats_table_${tab}_wrapper .row")[0].children[1].innerHTML = '<div id="stats_table_new_filter" class="dataTables_filter"><label>Search:<input type="search" class="form-control input-medium" aria-controls="stats_table_new"></label></div>'
            $('#stats_table_${tab}_wrapper .dataTables_filter input').on("change", function(){
                var text = $(this).val();
                Index.pag_start = + 0;
                Index.loadData_${tab}(Index.pag_start, Index.pag_size, text);
                $('#stats_table_${tab}_wrapper .dataTables_length select').select2("val", Index.pag_size);
                $('#stats_table_${tab}_wrapper .dataTables_filter input').val(text);
            });

            // Parsing parameters for the table.
            $("#stats_table_${tab}").on('order.dt',  function () {
                console.log("show!");
                var rows = $("#stats_table_${tab}").dataTable().fnGetNodes();

                if (rows.length < Index.pag_size)
                    $(".next").addClass("disabled");
                else
                    $(".next").removeClass("disabled");

                for (var i = 0; i < rows.length; i++) {
                    // Login
                    rows[i].cells[1].innerHTML = '<a href="/profile/' + rows[i].cells[0].innerHTML + '">' + rows[i].cells[1].innerHTML +'</a>';
                    // Groups
                    try {
                        var gr = rows[i].cells[4].innerHTML, name;
                        gr = JSON.parse(gr);
                        name = gr.name.split(' - ');
                        name[0] = account_group[name[0]];
                        gr.name = name.join(' - ');
                        rows[i].cells[4].innerHTML = '<a href="/profile/list/?tab=' +  Index.tab  + '&group=' + gr.id + '">' + gr.name + '</a>';
                    } catch (e) { }
                    // Actions
                    try {
                        var state = rows[i].cells[2].innerHTML;
                        state = JSON.parse(state);
                        if (state.id == ${Account.State.NEW}) {
                            var url = "/profile/control/{account}/${Account.State.ACTIVE}?tab=${tab}"
                                .replace('{account}', rows[i].cells[0].innerHTML);
                            rows[i].cells[5].innerHTML = '<a href="' + url + '" class="newclass"><i class="fa fa-check fabutton tooltips" data-placement="top" data-original-title="Активировать"></i></a>'
                            url = "/profile/control/{account}/${Account.State.BANNED}?tab=${tab}"
                                .replace('{account}', rows[i].cells[0].innerHTML);
                            rows[i].cells[5].innerHTML += '<a href="' + url + '" class="newclass"><i class="fa fa-times fabutton tooltips" data-placement="top" data-original-title="Заблокировать"></i></a>'
                        } else if (state.id == ${Account.State.ACTIVE}) {
                            var url = "/profile/control/{account}/${Account.State.BANNED}?tab=${tab}"
                                .replace('{account}', rows[i].cells[0].innerHTML);
                            rows[i].cells[5].innerHTML = '<a href="' + url + '" class="newclass"><i class="fa fa-times fabutton tooltips" data-placement="top" data-original-title="Заблокировать"></i></a>'
                            rows[i].cells[5].innerHTML += '<a href="#delete_user" class="newclass" data-toggle="modal"><i class="fa fa-trash-o fabutton tooltips" data-placement="top" data-original-title="Удалить"></i></a>'

                        } else  {
                            var url = "/profile/control/{account}/${Account.State.ACTIVE}?tab=${tab}"
                                .replace('{account}', rows[i].cells[0].innerHTML);

                            rows[i].cells[5].innerHTML = '<a href="' + url + '" class="newclass"><i class="fa fa-check fabutton tooltips" data-placement="top" data-original-title="Активировать"></i></a>'
                            var url = "/profile/delete/{account}?tab=${tab}".replace('{account}', rows[i].cells[0].innerHTML);
                            rows[i].cells[5].innerHTML += '<a href="' + url + '" class="newclass"><i class="fa fa-trash-o fabutton tooltips" data-placement="top" data-original-title="Удалить"></i></a>'
                        }
                    } catch(e) { }

                    // Status
                    try {
                        var stat = rows[i].cells[2].innerHTML;
                        stat = JSON.parse(stat);
                        var stat_class = "";
                        if (stat.id == "${Account.State.ACTIVE}"){
                            stat_class = "badge-success";
                        } else if (stat.id == "${Account.State.SUSPENDED}") {
                            stat_class = "badge-warning";
                        } else if (stat.id == "${Account.State.NEW}") {
                            stat_class = "badge-info";
                        } else {
                            stat_class = "badge-danger";
                        }
                        rows[i].cells[2].innerHTML = '<span class="badge {stat_class}">{stat_value}</span>'
                            .replace("{stat_value}", account_state[stat.id])
                            .replace("{stat_class}", stat_class);
                    } catch(e) { console.log("status error"); }

                    //Balance
                    try {
                        var balance = rows[i].cells[3].innerHTML;
                        balance = JSON.parse(balance);
                        var cost = +balance.balance;
                        rows[i].cells[3].innerHTML = cost.toFixed(2).toString().replace(/\B(?=(\d{3})+(?!\d))/g, " ") + " руб"
                    } catch(e) { }
                }

                // Add summary.
                var start = +Index.pag_start + 1, end;
                if (!Index.total)
                    start = end = 0;
                else if ((+Index.pag_start + Index.pag_size) > +Index.total)
                    end = Index.total;
                else
                    end = + Index.pag_start + Index.pag_size;
                $("#stats_table_${tab}_wrapper .row")[1].children[0].innerHTML = 'С {start} по {end}, всего {total}'
                    .replace("{total}", Index.total)
                    .replace("{start}", start)
                    .replace("{end}", end);
            });
        },
        %endfor

        fake: null
    }
}();
</script>

<script type="text/javascript">
    $(document).on("ready", function() {

        account_group = {
            "${Account.Groups.NONE}": 'Без группы',
            "${Account.Groups.PUB}": 'Разработчик',
            "${Account.Groups.ADV}": 'Рекламодатель'
        }

        account_state = {
            "${Account.State.NEW}": 'Новый',
            "${Account.State.ACTIVE}": 'Активный',
            "${Account.State.BANNED}": 'Заблокированный',
            "${Account.State.SUSPENDED}": 'Приостановленный'
        }

        Index.init();
        // Init filters.
        %for select in g.selects:
            <%
                _name = 'account' if select['name'] in ('advertiser', 'publisher', ) else select['name']
                placeholder = getattr(g, str(_name) + '_name', select['header'])
            %>
            filterBox.push({
                id: "${select['id']}",
                name: "${select['name']}",
                header: "${select['header']}",
                placeholder: "${placeholder}",
                value: "${getattr(g, str(select['name']), '')}",
                url: "/profile/ajax/select"
            });
        %endfor
        %if getattr(g, 'tab', ''):
            Index.loadData_${g.tab}();
        %else:
            Index.loadData_new();
        %endif

        // Pagination sctipt
        $(".prev").on("click", function() {
            if ($(".prev").attr("class").indexOf("disabled") > 0) {
               return;
            }

            Index.pag_start -= Number(Index.pag_size);
            if (+Index.pag_start < 0) {
               Index.pag_start = +0;
               $(".prev").addClass("disabled");
               return;
            }
            $(".next").removeClass("disabled");

            Index["loadData_" + Index.tab](Index.pag_start, Index.pag_size);

        });
        $(".next").on("click", function() {
            console.log("next!");
            if ($(".next").attr("class").indexOf("disabled") > 0) {
               return;
            }

            $(".prev").removeClass("disabled");

            Index.pag_start = Number(Index.pag_start) + Number(Index.pag_size);
            Index["loadData_" + Index.tab](Number(Index.pag_start), Number(Index.pag_size));
        });

    });
</script>