<%namespace name="bcrumb" file="/layout/bcrumb.mako" />
<%namespace name="fabutton" file="/layout/fa-button.mako" />
<%inherit file="/layout/main.mako" />

<%
    import datetime

%>

<%def name="title()">Добавить комментарий</%def>
<%def name="description()"></%def>

${ bcrumb.h(self) }


<!-- BEGIN PAGE CONTENT-->
<div class="row">
<div class="col-md-12">
    <div class="portlet">
        <div class="portlet-title">
            <div class="caption"><i class="fa fa-rocket"></i>
                %if True:
            	    Добавить комментарий
            	%else:

            	%endif
            </div>
        </div>

        <div class="portlet-body">
        	<form method="POST" class="form-horizontal">
                <div class="form-group">
					<label class="control-label col-md-4">Комментарий  </label>
					<div class="col-md-4">
						<input type="text" class="form-control" name="text" value="">
					</div>
				</div>

				<input type="hidden" class="form-control" name="consumer" value="${g.consumer_id}">

				<!-- ==== SUBMIT ==== --->

                    <div class="form-group">
                        <div class="col-sm-offset-2 col-sm-10">
                            <button type="submit" id="sample_editable_1_new" class="btn btn-success">
                                Добавить
                            </button>
                        </div>
                    </div>
            </form>
        </div>
    </div>
</div>
</div>
<!-- END PAGE CONTENT-->