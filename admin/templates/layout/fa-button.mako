<%def name="fab(bps)">

	% for p in bps:
		<a href="${p.get('href', '#')}" id="${p.get('id', 'newid')}" class="${p.get('class', 'newclass')}"><i class="fa fa-${p.get('icon', 'question')} fabutton tooltips" data-placement="top" data-original-title="${p.get('text', 'NewText')}"></i></a>
	% endfor

</%def>

<%def name="fas(bps)">
% for p in bps:
	%if p.get('type') == 'app':		
		%if p.get('status') == 1:
			<span class="label label-success fastatus tooltips" data-placement="top" data-original-title="Активно"><i class="fa fa-play fastatusi" >
		%elif p.get('status') == 0:
			<span class="label label-default fastatus tooltips" data-placement="top" data-original-title="Остановлено"><i class="fa fa-pause fastatusi" >
		%elif p.get('status') == 2:
			<span class="label label-warning fastatus tooltips" data-placement="top" data-original-title="На модерации"><i class="fa fa-clock-o fastatusi" >
		%elif p.get('status') == 3:
			<span class="label label-danger fastatus tooltips" data-placement="top" data-original-title="Заблокировано"><i class="fa fa-ban fastatusi" >
		%endif
		    </i><div style="display:none">${p.get('status')}</div>
        </span>
    %endif

    %if p.get('type') == 'zone':		
		%if p.get('status') == 1:
			<span class="label label-success fastatus tooltips" data-placement="top" data-original-title="Активно"><i class="fa fa-play fastatusi" >
		%elif p.get('status') == 2:
			<span class="label label-default fastatus tooltips" data-placement="top" data-original-title="Остановлено"><i class="fa fa-pause fastatusi" >
		%elif p.get('status') == 3:
			<span class="label label-warning fastatus tooltips" data-placement="top" data-original-title="На модерации"><i class="fa fa-clock-o fastatusi" >
		%elif p.get('status') == 4:
			<span class="label label-danger fastatus tooltips" data-placement="top" data-original-title="Заблокировано"><i class="fa fa-ban fastatusi" >
		%endif
		    </i><div style="display:none">${p.get('status')}</div>
        </span>
    %endif

    %if p.get('type') == 'unit':
		%if p.get('status') == 1:
			<span class="label label-success fastatus tooltips" data-placement="top" data-original-title="Активно"><i class="fa fa-play fastatusi" >
		%elif p.get('status') == 2:
			<span class="label label-default fastatus tooltips" data-placement="top" data-original-title="Остановлено"><i class="fa fa-pause fastatusi" >
		%elif p.get('status') == 3:
			<span class="label label-warning fastatus tooltips" data-placement="top" data-original-title="На модерации"><i class="fa fa-clock-o fastatusi" >
		%elif p.get('status') == 4:
			<span class="label label-danger fastatus tooltips" data-placement="top" data-original-title="Заблокировано"><i class="fa fa-ban fastatusi" >
		%endif
		    </i><div style="display:none">${p.get('status')}</div>
        </span>
    %endif

    %if p.get('type') == 'media':		
		%if p.get('status') == 1:
			<span class="label label-success fastatus tooltips" data-placement="top" data-original-title="Активно"><i class="fa fa-play fastatusi" >
		%elif p.get('status') == 0:
			<span class="label label-default fastatus tooltips" data-placement="top" data-original-title="Остановлено"><i class="fa fa-pause fastatusi" >
		%elif p.get('status') == 2:
			<span class="label label-warning fastatus tooltips" data-placement="top" data-original-title="На модерации"><i class="fa fa-clock-o fastatusi" >
		%elif p.get('status') == 3:
			<span class="label label-danger fastatus tooltips" data-placement="top" data-original-title="Заблокировано"><i class="fa fa-ban fastatusi" >
		%endif
		    </i><div style="display:none">${p.get('status')}</div>
        </span>
    %endif
% endfor	

</%def>