## -*- coding: utf-8 -*-
<div class="footer">															<!-- BEGIN FOOTER -->
    <div class="footer-inner">
         2013 &copy; FAAN.
    </div>
    <div class="footer-tools">
        <span class="go-top">
            <i class="fa fa-angle-up"></i>
        </span>
    </div>
</div>																			<!-- END FOOTER -->