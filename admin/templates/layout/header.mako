## -*- coding: utf-8 -*-

<div class="header navbar navbar-inverse navbar-fixed-top">						<!-- BEGIN HEADER -->
	<div class="header-inner">													<!-- BEGIN TOP NAVIGATION BAR -->
		<a class="navbar-brand" href="/">										<!-- BEGIN LOGO -->
		    <img src="/global/static/img/logo.png" alt="logo" class="img-responsive"/>
		</a>																	<!-- END LOGO -->
		
		
		<a href="#" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">		<!-- BEGIN RESPONSIVE MENU TOGGLER -->
		    <img src="/global/static/img/menu-toggler.png" alt=""/>
		</a>																							<!-- END RESPONSIVE MENU TOGGLER -->
		
		<ul class="nav navbar-nav pull-right">									<!-- BEGIN TOP NAVIGATION MENU -->
            <%include file="header/notification.mako" />
            <%include file="header/inbox.mako" />
##			<li class="devider">&nbsp;</li>
##            <%include file="header/balance.mako"/>
			<li class="devider">&nbsp;</li>
            <%include file="header/user.mako"/>
	    </ul>																	<!-- END TOP NAVIGATION MENU -->
	</div>																		<!-- END TOP NAVIGATION BAR -->
</div>																			<!-- END HEADER -->
