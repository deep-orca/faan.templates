## -*- coding: utf-8 -*-
<%
from faan.core.model.security import Account

account = Account.Get(request.environ['REMOTE_USER'])
%>
<li class="dropdown user">															<!-- BEGIN USER LOGIN DROPDOWN -->
    <a href="#" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
	    <span class="username">${account.username}</span>
	    <i class="fa fa-angle-down"></i>
    </a>
    <ul class="dropdown-menu">
        <li><a href="/general/profile"		><i class="fa fa-user"	></i> Профайл</a></li>
        <li><a href="/general/settings"		><i class="fa fa-gear"	></i> Настройки</a></li>
        <li class="divider"></li>
        <li><a href="/sign/out"				><i class="fa fa-key"	></i> Выход</a></li>
    </ul>
</li>																				<!-- END USER LOGIN DROPDOWN -->