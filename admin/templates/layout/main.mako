## -*- coding: utf-8 -*-
<%def name="title()"></%def>
<%def name="description()"></%def>
<%def name="page_js()"></%def>
<%def name="page_css()"></%def>
<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en" class="no-js">
<!--<![endif]-->
<head>                                                                            <!-- BEGIN HEAD -->
    <meta charset="utf-8"/>
    <META NAME="ROBOTS" CONTENT="NOINDEX, NOFOLLOW">
    <title>Vidiger Admin - ${self.title()} - ${self.description()}</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <meta name="description" content="${description}"/>
    <meta name="author" content=""/>
    <meta name="MobileOptimized" content="320">


    <%include file="core_js.mako" />
    ${self.page_js()}
    <!-- BEGIN GLOBAL MANDATORY STYLES -->
    ##<link href="/global/static/plugins/font-awesome/css/font-awesome.min.css" 	rel="stylesheet" type="text/css" />

    <link href="//netdna.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet">
    <link href="/global/static/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
    <link href="/global/static/plugins/uniform/css/uniform.default.css" rel="stylesheet" type="text/css"/>
    <!-- END GLOBAL MANDATORY STYLES -->
    <!-- BEGIN PAGE LEVEL STYLES -->
    <link href="/global/static/plugins/select2/select2_conquer.css" rel="stylesheet" type="text/css"/>
    <link href="/global/static/plugins/data-tables/DT_bootstrap.css" rel="stylesheet" type="text/css"/>
    <!-- END PAGE LEVEL STYLES -->
    <!-- BEGIN THEME STYLES -->
    <link href="/global/static/css/style-conquer.css" rel="stylesheet" type="text/css"/>
    <link href="/global/static/css/style.css" rel="stylesheet" type="text/css"/>
    <link href="/global/static/css/style-responsive.css" rel="stylesheet" type="text/css"/>
    <link href="/global/static/css/plugins.css" rel="stylesheet" type="text/css"/>
    <link href="/global/static/css/themes/default.css" id="style_color" rel="stylesheet" type="text/css"/>
    <link href="/global/static/css/custom.css" rel="stylesheet" type="text/css"/>
    <link href="/global/static/plugins/bootstrap-toastr/toastr.min.css" rel="stylesheet" type="text/css"/>
    ${self.page_css()}

    <!-- END THEME STYLES -->
    <link rel="shortcut icon" href="/static/home_assets/images/favicon.png"/>
</head>
<!-- END HEAD -->

<!-- BEGIN BODY -->
<body class="page-header-fixed page-sidebar-fixed">
    <%include file="header.mako"/>
    <div class="clearfix"></div>
    <!-- BEGIN CONTAINER -->
    <div class="page-container">
        <%include file="/layout/sidebar.mako"/>
        <!-- BEGIN CONTENT -->
        <div class="page-content-wrapper">
            <div class="page-content" style="min-height:1009px !important">
                ${ next.body() }
            </div>
        </div>
        <!-- END CONTENT -->
    </div>
    <!-- END CONTAINER -->
    <%include file="footer.mako" />
</body>
<!-- END BODY -->
</html>

