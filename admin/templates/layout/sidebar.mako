<div class="page-sidebar-wrapper">
    <div class="page-sidebar navbar-collapse collapse">
        <ul class="page-sidebar-menu">
            <li class="sidebar-toggler-wrapper">
                <div class="sidebar-toggler"></div>
                <div class="clearfix"></div>
            </li>

            ${h.admin_menu.render()}
        </ul>
    </div>
</div>
