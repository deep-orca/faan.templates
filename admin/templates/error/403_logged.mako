<%namespace name="bcrumb" file="/layout/bcrumb.mako" />

<%inherit file="/layout/main.mako" />

## Page JS
<%def name="page_js()">
    <script src="/global/static/plugins/moment-with-langs.min.js" type="text/javascript"></script>
    <script src="/global/static/plugins/bootstrap-daterangepicker/daterangepicker.js" type="text/javascript"></script>
    <script src="/global/static/plugins/flot/jquery.flot.js" type="text/javascript"></script>
    <script src="/global/static/plugins/flot/jquery.flot.resize.js" type="text/javascript"></script>
    <script src="/global/static/plugins/flot/jquery.flot.time.js" type="text/javascript"></script>
    <script src="/global/static/scripts/adv_dashboard.js" type="text/javascript"></script>
</%def>
## END

## Page CSS
<%def name="page_css()">
    <link href="/global/static/css/plugins.css" rel="stylesheet" type="text/css"/>
    <link href="/global/static/css/pages/error.css" rel="stylesheet" type="text/css"/>
    <link href="/global/static/css/pages/dashboard.css" rel="stylesheet" type="text/css"/>
    <link href="/global/static/plugins/fullcalendar/fullcalendar/fullcalendar.css" rel="stylesheet" type="text/css"/>
    <link href="/global/static/plugins/bootstrap-daterangepicker/daterangepicker-bs3.css" rel="stylesheet" type="text/css"/>
</%def>
## ENDs

<%def name="title()">404 Тут ничего нет.</%def>
<!-- BEGIN PAGE CONTENT-->
<div class="row">
    <div class="col-md-12 page-404">
        <div class="number">
             403
        </div>
        <div class="details">

            <h3>Oops! Вам сюда нельзя.</h3>
            <p>
                Вы не можете просматривать страницу, которую Вы запрашивали.<br>
                Попробуйте <a href="/">вернуться на главную</a>.
            </p>
        </div>
    </div>
</div>
