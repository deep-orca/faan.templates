<!DOCTYPE html>
<!-- 
Template Name: Conquer - Responsive Admin Dashboard Template build with Twitter Bootstrap 3.0.3
Version: 1.5.2
Author: KeenThemes
Website: http://www.keenthemes.com/
Purchase: http://themeforest.net/item/conquer-responsive-admin-dashboard-template/3716838?ref=keenthemes
-->
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en" class="no-js">
<!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
<meta charset="utf-8"/>
<title>404</title>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta content="width=device-width, initial-scale=1.0" name="viewport"/>
<meta content="" name="description"/>
<meta content="" name="author"/>
<meta name="MobileOptimized" content="320">
<!-- BEGIN GLOBAL MANDATORY STYLES -->
<link href="/global/static/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
<link href="/global/static/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
<link href="/global/static/plugins/uniform/css/uniform.default.css" rel="stylesheet" type="text/css"/>
<!-- END GLOBAL MANDATORY STYLES -->
<!-- BEGIN THEME STYLES -->
<link href="/global/static/css/style-conquer.css" rel="stylesheet" type="text/css"/>
<link href="/global/static/css/style.css" rel="stylesheet" type="text/css"/>
<link href="/global/static/css/style-responsive.css" rel="stylesheet" type="text/css"/>
<link href="/global/static/css/plugins.css" rel="stylesheet" type="text/css"/>
<link href="/global/static/css/themes/default.css" rel="stylesheet" type="text/css" id="style_color"/>
<link href="/global/static/css/pages/error.css" rel="stylesheet" type="text/css"/>
<link href="/global/static/css/custom.css" rel="stylesheet" type="text/css"/>
<!-- END THEME STYLES -->
<link rel="shortcut icon" href="favicon.ico"/>
</head>
<!-- END HEAD -->
<!-- BEGIN BODY -->
<body class="page-404-full-page">
<div class="row">
	<div class="col-md-12 page-404">
		<div class="number">
			 403
		</div>
		<div class="details">

			<h3>Oops! Вы потерялись.</h3>
			<p>
				<a href="/sign/in">Войти</a>.
			</p>
		</div>
	</div>
</div>
<!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->
<!-- BEGIN CORE PLUGINS -->
<!--[if lt IE 9]>
<script src="assets/plugins/respond.min.js"></script>
<script src="assets/plugins/excanvas.min.js"></script> 
<![endif]-->
<script src="/global/static/plugins/jquery-1.10.2.min.js" type="text/javascript"></script>
<script src="/global/static/plugins/jquery-migrate-1.2.1.min.js" type="text/javascript"></script>
<script src="/global/static/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<script src="/global/static/plugins/bootstrap-hover-dropdown/twitter-bootstrap-hover-dropdown.min.js" type="text/javascript"></script>
<script src="/global/static/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
<script src="/global/static/plugins/jquery.blockui.min.js" type="text/javascript"></script>
<script src="/global/static/plugins/jquery.cokie.min.js" type="text/javascript"></script>
<script src="/global/static/plugins/uniform/jquery.uniform.min.js" type="text/javascript"></script>
<script type="text/javascript" src="/global/static/plugins/bootstrap-toastr/toastr.min.js"></script>

<!-- END CORE PLUGINS -->
<script src="/global/static/scripts/app.js"></script>
<script>
jQuery(document).ready(function() {    
   App.init();
});
</script>
<!-- END JAVASCRIPTS -->
</body>
<!-- END BODY -->
</html>