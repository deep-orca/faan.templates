## -*- coding: utf-8 -*-

<% from faan.core.model.pub.application import Application %>

<%namespace name="bcrumb" file="/layout/bcrumb.mako" />
<%inherit file="/layout/main.mako" />

<%def name="title()">
% if form.data.get('id'):
Площадка [${g.application.id}] ${g.application.name}
% else:
Новая площадка
% endif
</%def>

<%def name="page_css()">
    <link rel="stylesheet" href="/global/static/plugins/jquery-ui/jquery-ui-1.10.3.custom.min.css"/>
    <style>
        .ui-autocomplete {
            max-height: 370px;
            overflow-y: auto;
            /* prevent horizontal scrollbar */
            overflow-x: hidden;
            width: 315px;
        }

        /*
         * IE 6 doesn't support max-height
         * we use height instead, but this forces the menu to always be this tall
         */
        * html .ui-autocomplete {
            height: 370px;
            width: 315px;
        }
        .ui-menu .ui-menu-item a {
            padding: 2px !important;
        }

        .ui-menu .ui-menu-item a img {
            margin-right: 10px;
        }

        .search-spinner {
            background-image: url('/global/static/plugins/select2/select2-spinner.gif');
            background-position: center;
            background-repeat: no-repeat;
            height: 21px;
            width: 21px;
            margin-left: -30px;
            margin-right: 6px;
            display: inline-block;
        }

        .search-spinner.hidden {
            display: none;
        }

        #app-name {
            height: 0;
            visibility: hidden;
            padding: 0;
            margin: 0;
        }

        #app-name-search {
            display: inline-block;
            border-bottom-right-radius: 0;
            border-top-right-radius: 0;
##            width: 247px;
        }

        #app-name-search[disabled] {
            cursor: auto !important;
        }

        .ui-state-focus {
            border: 1px solid #ddd !important;
            background: #eee !important;
        }

        .ui-state-focus {
            color: #000 !important;
        }

        .ui-menu-item a {
            color: #666;
        }

##        #app-search {
##            display: inline-block;
##            border-top-left-radius: 0;
##            border-bottom-left-radius: 0;
##            margin-left: -3px;
##        }
        .ui-corner-all{
            font-family: 'Open Sans', sans-serif;
        }

        .xform-error {
            padding: 3px;
            font-weight: bold !important;
        }
    </style>
</%def>

<%def name="page_js()">
    <script type="text/javascript" src="/global/static/plugins/jquery.validate.min.js"></script>
    <script type="text/javascript" src="/global/static/plugins/jquery-validation/localization/messages_ru.js"></script>
</%def>

<%def name="description()">Форма редактирования</%def>

${ bcrumb.h(self) }
${ bcrumb.bc([
    { 'name' : u'Площадки', 'href' : '/app/', 'icon' : 'rocket' },
    { 'name' : u"[%s] %s" % (g.application.id, g.application.name) if g.application.id else u'Новая площадка', 'href' : u"/app/%s/zone" % (form.data.get('id')) if form.data.get('id') else "#"},
], [])}

<div class="row">
<div class="col-md-12">
    <div class="portlet">
        <div class="portlet-title"><div class="caption"><i class="fa fa-rocket"></i>${self.title()}</div></div>

        <div class="portlet-body form">
            <form class="form-horizontal form-row-sepe" method="post" id="app-form">
                ${form.csrf_token}

                %if g.application.id:
                    <div class="form-group">
                        <p></p>

                        <label class="control-label col-md-4"><b>ID площадки</b></label>
                        <div class="col-md-4">
                            <label class="control-label"> ${g.application.uuid}</label>
                        </div>
                    </div>
                %endif

                <div class="form-group ${'has-error' if form.os.errors else ''}">
                    <p></p>

                    <label class="control-label col-md-4">Выберите платформу</label>
                    <div class="col-md-6" id="app_type_radio">
                        <div class="btn-group" data-toggle="buttons">
                            <label class="btn btn-default apptypebutton" data-app-id="${Application.Os.IOS}" id="app-os-${Application.Os.IOS}">
                                <input type="radio" class="toggle">
                                <i class="fa fa-apple apptypeicon"></i>
                                <div class="usrtype">iOS</div>
                            </label>
                            <label class="btn btn-default apptypebutton" data-app-id="${Application.Os.ANDROID}" id="app-os-${Application.Os.ANDROID}">
                                <input type="radio" class="toggle">
                                <i class="fa fa-android apptypeicon"></i>
                                <div class="usrtype">Android</div>
                            </label>
                            <label class="btn btn-default apptypebutton disabled" data-app-id="${Application.Os.WINPHONE}" id="app-os-${Application.Os.WINPHONE}">
                                <input type="radio" class="toggle" disabled>
                                <i class="fa fa-windows apptypeicon"></i>
                                <div class="usrtype">Windows</div>
                            </label>
                            <label class="btn btn-default apptypebutton"
                                   data-app-id="${Application.Os.MOBILE_WEB}" id="app-os-${Application.Os.MOBILE_WEB}">
                                <input type="radio" class="toggle">
                                <i class="fa fa-globe apptypeicon"></i>

                                <div class="usrtype">Web</div>
                            </label>
                        </div>
                    </div>
                    <input type="hidden" name="os" id="apptype" value="${form.data.get('os')}">
                </div>

                

                <div class="form-group has-feedback if-application">
                    <label for="app-country" class="control-label col-md-4">Страна приложения</label>
                    <div class="col-md-4">
                        <select name="app-country" id="app-country" class="form-control">
                            <option value=""> Международный </option>
                            <option value="RU"> Российская Федерация </option>
                            <option value="UA"> Украина </option>
                            <option value="BY"> Беларусь </option>
                            <option value="KZ"> Казахстан </option>
                            <option value="AU"> Австралия </option>
                            <option value="AT"> Австрия </option>
                            <option value="AZ"> Азербайджан </option>
                            <option value="AL"> Албания </option>
                            <option value="DZ"> Алжир </option>
                            <option value="AS"> Американское Самоа </option>
                            <option value="AI"> Ангилья </option>
                            <option value="AO"> Ангола </option>
                            <option value="AD"> Андорра </option>
                            <option value="AR"> Аргентина </option>
                            <option value="AM"> Армения </option>
                            <option value="AW"> Аруба </option>
                            <option value="AF"> Афганистан </option>
                            <option value="BS"> Багамы </option>
                            <option value="BD"> Бангладеш </option>
                            <option value="BB"> Барбадос </option>
                            <option value="BH"> Бахрейн </option>
                            <option value="BY"> Беларусь </option>
                            <option value="BZ"> Белиз </option>
                            <option value="BE"> Бельгия </option>
                            <option value="BJ"> Бенин </option>
                            <option value="BM"> Бермуды </option>
                            <option value="BG"> Болгария </option>
                            <option value="BO"> Боливия </option>
                            <option value="BA"> Босния и Герцеговина </option>
                            <option value="BW"> Ботсвана </option>
                            <option value="BR"> Бразилия </option>
                            <option value="IO"> Британская территория Индийского океана </option>
                            <option value="BN"> Бруней-Даруссалам </option>
                            <option value="BV"> Буве </option>
                            <option value="BF"> Буркина-Фасо </option>
                            <option value="BI"> Бурунди </option>
                            <option value="BT"> Бутан </option>
                            <option value="VU"> Вануату </option>
                            <option value="GB"> Великобритания </option>
                            <option value="HU"> Венгрия </option>
                            <option value="VE"> Венесуэла </option>
                            <option value="VG"> Виргинские острова (Британские) </option>
                            <option value="VI"> Виргинские острова (США ) </option>
                            <option value="VN"> Вьетнам </option>
                            <option value="GA"> Габон </option>
                            <option value="GY"> Гайана </option>
                            <option value="HT"> Гаити </option>
                            <option value="GM"> Гамбия </option>
                            <option value="GH"> Гана </option>
                            <option value="GP"> Гваделупа </option>
                            <option value="GT"> Гватемала </option>
                            <option value="GN"> Гвинея </option>
                            <option value="GW"> Гвинея-Бисау </option>
                            <option value="DE"> Германия </option>
                            <option value="GI"> Гибралтар </option>
                            <option value="HN"> Гондурас </option>
                            <option value="HK"> Гонконг </option>
                            <option value="GD"> Гренада </option>
                            <option value="GL"> Гренландия </option>
                            <option value="GR"> Греция </option>
                            <option value="GE"> Грузия </option>
                            <option value="GU"> Гуам </option>
                            <option value="DK"> Дания </option>
                            <option value="DJ"> Джибути </option>
                            <option value="DM"> Доминика </option>
                            <option value="DO"> Доминиканская Республика </option>
                            <option value="EG"> Египет </option>
                            <option value="ZM"> Замбия </option>
                            <option value="EH"> Западная Сахара </option>
                            <option value="ZW"> Зимбабве </option>
                            <option value="YE"> Йемен </option>
                            <option value="IL"> Израиль </option>
                            <option value="IN"> Индия </option>
                            <option value="ID"> Индонезия </option>
                            <option value="JO"> Иордания </option>
                            <option value="IQ"> Ирак </option>
                            <option value="IR"> Иран (Исламская Республика) </option>
                            <option value="IE"> Ирландия </option>
                            <option value="IS"> Исландия </option>
                            <option value="ES"> Испания </option>
                            <option value="IT"> Италия </option>
                            <option value="CV"> Кабо-Верде </option>
                            <option value="KY"> Каймановы острова </option>
                            <option value="KH"> Камбоджа </option>
                            <option value="CM"> Камерун </option>
                            <option value="CA"> Канада </option>
                            <option value="QA"> Катар </option>
                            <option value="KE"> Кения </option>
                            <option value="CY"> Кипр </option>
                            <option value="KI"> Кирибати </option>
                            <option value="CN"> Китай </option>
                            <option value="CC"> Кокосовые (Килинг) острова </option>
                            <option value="CO"> Колумбия </option>
                            <option value="KM"> Коморские Острова </option>
                            <option value="CG"> Конго </option>
                            <option value="CD"> Конго, Демократическая Республика </option>
                            <option value="KP"> Корея , Корейская Народно-Демократическая Республика </option>
                            <option value="CR"> Коста-Рика </option>
                            <option value="CI"> Кот-д'Ивуар </option>
                            <option value="CU"> Куба </option>
                            <option value="KW"> Кувейт </option>
                            <option value="KG"> Кыргызстан </option>
                            <option value="LA"> Лаосская Народно-Демократическая Республика </option>
                            <option value="LV"> Латвия </option>
                            <option value="LS"> Лесото </option>
                            <option value="LR"> Либерия </option>
                            <option value="LB"> Ливан </option>
                            <option value="LY"> Ливийская Арабская Джамахирия </option>
                            <option value="LT"> Литва </option>
                            <option value="LI"> Лихтенштейн </option>
                            <option value="LU"> Люксембург </option>
                            <option value="MU"> Маврикий </option>
                            <option value="MR"> Мавритания </option>
                            <option value="MG"> Мадагаскар </option>
                            <option value="YT"> Майотта </option>
                            <option value="MO"> Макао </option>
                            <option value="MK"> Македония, бывшая Югославская Республика </option>
                            <option value="MW"> Малави </option>
                            <option value="MY"> Малайзия </option>
                            <option value="ML"> Мали </option>
                            <option value="MV"> Мальдивские о-ва </option>
                            <option value="MT"> Мальта </option>
                            <option value="MA"> Марокко </option>
                            <option value="MQ"> Мартиника </option>
                            <option value="MH"> Маршалловы Острова </option>
                            <option value="MX"> Мексика </option>
                            <option value="FM"> Микронезия, Федеративные Штаты </option>
                            <option value="MZ"> Мозамбик </option>
                            <option value="MD"> Молдова </option>
                            <option value="MC"> Монако </option>
                            <option value="MN"> Монголия </option>
                            <option value="MS"> Монтсеррат </option>
                            <option value="MM"> Мьянма </option>
                            <option value="NA"> Намибия </option>
                            <option value="NR"> Науру </option>
                            <option value="NP"> Непал </option>
                            <option value="NE"> Нигер </option>
                            <option value="NG"> Нигерия </option>
                            <option value="AN"> Нидерландские Антильские острова </option>
                            <option value="NL"> Нидерланды </option>
                            <option value="NI"> Никарагуа </option>
                            <option value="NU"> Ниуэ </option>
                            <option value="NZ"> Новая Зеландия </option>
                            <option value="NC"> Новая Каледония </option>
                            <option value="NO"> Норвегия </option>
                            <option value="AE"> Объединенные Арабские Эмираты </option>
                            <option value="OM"> Оман </option>
                            <option value="NF"> Остров Норфолк </option>
                            <option value="CX"> Остров Рождества </option>
                            <option value="CK"> Острова Кука </option>
                            <option value="PK"> Пакистан </option>
                            <option value="PW"> Палау </option>
                            <option value="PA"> Панама </option>
                            <option value="PG"> Папуа-Новая Гвинея </option>
                            <option value="PY"> Парагвай </option>
                            <option value="PE"> Перу </option>
                            <option value="PN"> Питкэрн </option>
                            <option value="PL"> Польша </option>
                            <option value="PT"> Португалия </option>
                            <option value="PR"> Пуэрто-Рико </option>
                            <option value="KR"> Республика Корея </option>
                            <option value="RE"> Реюньон </option>

                            <option value="RW"> Руанда </option>
                            <option value="RO"> Румыния </option>
                            <option value="SV"> Сальвадор </option>
                            <option value="WS"> Самоа </option>
                            <option value="SH"> Санкт Елена </option>
                            <option value="PM"> Санкт Пьер и Микелон </option>
                            <option value="SM"> Сан-Марино </option>
                            <option value="ST"> Сан-Томе и Принсипи </option>
                            <option value="SA"> Саудовская Аравия </option>
                            <option value="SZ"> Свазиленд </option>
                            <option value="VA"> Святой Престол (Ватикан) </option>
                            <option value="MP"> Северные Марианские острова </option>
                            <option value="SC"> Сейшельские острова </option>
                            <option value="SN"> Сенегал </option>
                            <option value="VC"> Сент-Винсент и Гренадины </option>
                            <option value="KN"> Сент-Китс и Невис </option>
                            <option value="LC"> Сент-Люсия </option>
                            <option value="SG"> Сингапур </option>
                            <option value="SY"> Сирийская Арабская Республика </option>
                            <option value="SK"> Словакия (Республика Словакия) </option>
                            <option value="SI"> Словения </option>
                            <option value="SB"> Соломоновых островов </option>
                            <option value="SO"> Сомали </option>
                            <option value="SD"> Судан </option>
                            <option value="SR"> Суринам </option>
                            <option value="US"> США </option>
                            <option value="UM"> США Внешние малые острова </option>
                            <option value="SL"> Сьерра-Леоне </option>
                            <option value="TJ"> Таджикистан </option>
                            <option value="TW"> Тайвань, провинция Китая </option>
                            <option value="TH"> Таиланд </option>
                            <option value="TZ"> Танзания, Объединенная Республика </option>
                            <option value="TC"> Теркс и Кайкос </option>
                            <option value="TG"> Того </option>
                            <option value="TK"> Токелау </option>
                            <option value="TO"> Тонга </option>
                            <option value="TT"> Тринидад и Тобаго </option>
                            <option value="TV"> Тувалу </option>
                            <option value="TN"> Тунис </option>
                            <option value="TM"> Туркменистан </option>
                            <option value="TR"> Турция </option>
                            <option value="UG"> Уганда </option>
                            <option value="UZ"> Узбекистан </option>

                            <option value="WF"> Уоллис и Футуна </option>
                            <option value="UY"> Уругвай </option>
                            <option value="FO"> Фарерские острова </option>
                            <option value="FJ"> Фиджи </option>
                            <option value="PH"> Филиппины </option>
                            <option value="FI"> Финляндия </option>
                            <option value="FK"> Фолклендские (Мальвинские) острова </option>
                            <option value="FR"> Франция </option>
                            <option value="GF"> Французская Гвиана </option>
                            <option value="PF"> Французская Полинезия </option>
                            <option value="TF"> Французские Южные Территории </option>
                            <option value="HM"> Херд и Мак Дональд острова </option>
                            <option value="HR"> Хорватия</option>
                            <option value="CF"> Центральноафриканская Республика </option>
                            <option value="TD"> Чад </option>
                            <option value="CZ"> Чехия </option>
                            <option value="CL"> Чили </option>
                            <option value="CH"> Швейцария </option>
                            <option value="SE"> Швеция </option>
                            <option value="SJ"> Шпицберген и Ян-Майен острова </option>
                            <option value="LK"> Шри-Ланка </option>
                            <option value="EC"> Эквадор </option>
                            <option value="GQ"> Экваториальная Гвинея </option>
                            <option value="ER"> Эритрея </option>
                            <option value="EE"> Эстония </option>
                            <option value="ET"> Эфиопия </option>
                            <option value="ZA"> Южная Африка </option>
                            <option value="GS"> Южная Георгия и Южные Сандвичевы острова </option>
                            <option value="JM"> Ямайка </option>
                            <option value="JP"> Япония </option>
                        </select>
                    </div>
                </div>

                <div class="form-group if-application ${'has-error' if form.name.errors else ''}">
                    <label for="app-name-search" class="control-label col-md-4">Название приложения</label>
                    <div class="col-md-4" id="app-search-ctrl">
                        <div class="input-group">
                            <span class="input-group-addon" style="padding:0;border:0;">
                                <img width="34" height="34" src="${form.data.get('icon_url', '')}" alt="" id="app-icon-preview" style="border:0;border-top-left-radius: 4px;border-bottom-left-radius: 4px"/>
                            </span>
                            <input type="text" class="form-control" style="" name="name-search" value="${form.data.get('name', '')}" id="app-name-search">
                            <span class="form-control-feedback search-spinner hidden">&nbsp;</span>
                            <span class="input-group-btn">
                                <button type="button" id="app-search" style="" class="btn btn-default">Искать</button>
                            </span>
                        </div>
                        <div class="xform-error text-danger">${", ".join(form.name.errors)}</div>
                        <input type="text" class="form-control" name="name" value="${form.data.get('name', '')}" id="app-name">
                        <input type="hidden" name="icon_url" id="app-icon" class="form-control" value="${form.data.get('icon_url', '')}"/>
                        <label for="app-name-search" class="error help-text" style="display: none;"></label>
                    </div>
                </div>

                <div class="form-group if-web ${'has-error' if form.name.errors else ''}" style="display:none;">
                    ${form.name.label(class_="col-sm-4 col-md-4 control-label")}
                    <div class="col-md-4 col-sm-4">
                        ${form.name(class_="form-control")}

                        <div class="xform-error text-danger">${", ".join(form.name.errors)}</div>
                    </div>
                </div>

                <div class="form-group ${'has-error' if form.category_groups.errors else ''}">
                    <label for="app-category" class="col-sm-4 control-label">Категория</label>
                    <div class="col-sm-4">
                        <select name="category_groups" multiple id="app-category" class="form-control"></select>

                        <div class="xform-error text-danger">${", ".join(form.category_groups.errors)}</div>
                    </div>
                </div>

                <div class="form-group ${'has-error' if form.store_url.errors else ''}">
                    <label for="app-store-url" class="col-sm-4 control-label">URL</label>
                    <div class="col-sm-4">
                        <input type="text" name="store_url" id="app-store-url" class="form-control" value="${form.data.get('store_url', '')}" />

                        <div class="xform-error text-danger">${", ".join(form.store_url.errors)}</div>
                    </div>
                </div>

                <div class="form-group ${'has-error' if form.content_rating.errors else ''}">
                    <label for="content-rating" class="col-sm-4 control-label">Возрастная категория</label>
                    <div class="col-sm-4">
                        <select name="content_rating"  id="content-rating" class="form-control">
                            <option value="0">Без ограничения</option>
                            <option value="1">Младшая</option>
                            <option value="2">Средняя</option>
                            <option value="3">Старшая</option>
                        </select>

                        <div class="xform-error text-danger">${", ".join(form.content_rating.errors)}</div>
                    </div>

                </div>

                <div class="form-group col-md-12">
                    <h4 class="text-center">Категории рекламы</h4>
                    <span class="help-block text-center">
                        Выберите категории рекламных материалов, которые разрешены к показу на Вашей площадке.
                    </span>


                </div>

                <div class="form-group ${'has-error' if form.target_categories.errors else ''}" id="app-target-categories">
                    <div class="xform-error text-center text-danger col-md-12">${", ".join(form.target_categories.errors)}</div>
                    <div class="control-label col-md-3">
                        <div class="btn-group-vertical margin-right-10">
                            <button type="button" class="btn btn-default" id="check_all">Выделить все</button>
                            <button type="button" class="btn btn-default" id="uncheck_all">Снять выделение</button>
                        </div>
                    </div>
                </div>

                <div class="form-actions fluid">
                    <div class="col-md-offset-4 col-md-8">
                        <button type="submit" class="btn btn-lg btn-success">Сохранить</button>
                        <button type="button" class="btn btn-lg btn-default" onClick="javascript:window.location = '/app/'">Отмена</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
</div>
<!--END TABLE-->
<!-- END PAGE CONTENT-->

<script type="text/javascript">
    Index = function() {
    return {
            init: function(options) {
                var self = this; // DO NOT EDIT OR REMOVE THIS LINE

                // Default options (as it was a new app)
                self.options = {
                    appId: 0,
                    appOs: 0,
                    appCategories: [],
                    appTargetCategories: [],
                    appContentRating: -1,
                    appStoreURL: "",
                    searchCountry: ""
                };

                // Applying recieved options
                $.extend(self.options, options);

                // Init app category select2

                self.loadCategories = function() {
                    $("#app-category").empty();
                    $.ajax({
                        url: "/app/ajax/category",
                        data: {
                            platform: self.options.appOs == 4 ? 0 : self.options.appOs,
                            type: self.options.appOs == 4 ? 3 : "",
                            q: ""
                        },
                        success: function(response) {
                            $.each(response.objects, function() {
                                $("<option value='" + this.id + "'>" + this.name + "</option>").appendTo($("#app-category"));
                            });
                            if (self.options.appCategories.length) {
                                $("#app-category").select2("val", self.options.appCategories);
                            }
                        }
                    })
                };

                var formatCountry = function(item) {
                    var id = (item.id == "" ? "INT" : item.id).toLowerCase(),
                        name = item.text;
                    return "<img class='flag' src='/global/static/img/flags/{flag}.png' />{name}"
                            .replace("{flag}", id)
                            .replace("{name}", name);
                };

                $("#app-country").select2({
                    formatResult: formatCountry,
                    formatSelection: formatCountry,
                    escapeMarkup: function(m) { return m; }
                }).on("change", function() { self.options.searchCountry = $(this).select2("val"); });

                $("#app-category").val(self.appCategories).select2({});
                $("#content-rating").select2({});

                // Init app name autocomplete

                $("#app-name").autocomplete({
                    delay: 500,
                    source: function(request, response)  {
                        $.ajax({
                            url: "/app/ajax/store/search",
                            data: {
                                q: request.term,
                                platform: self.options.appOs,
                                lang: self.options.searchCountry
                            },
                            beforeSend: function() {
                                $(".search-spinner").removeClass('hidden');
                            },
                            success: function(r) {
                                $(".search-spinner").addClass('hidden');
                                response(r.objects.slice(0,10));
                                $(window).one("click", function() { $("#app-name").autocomplete("close"); })
                            },
                            error: function() {
                                $(".search-spinner").addClass('hidden');
                            }
                        })
                    },
                    select: function(evt, ui) {
                        self.options.appCategories = ui.item.genres;
                        self.options.appContentRating = ui.item.rating;
                        self.options.appStoreURL = ui.item.url;
                        self.options.appStoreIcon = ui.item.icon;
                        $("#app-name").val(ui.item.name);
                        $("#app-icon").val(self.options.appStoreIcon);
                        $("#app-icon-preview").attr("src", self.options.appStoreIcon);
                        $("#app-store-url").val(self.options.appStoreURL);
                        $("#app-name-search").val(ui.item.name);
                        $("#app-category").select2("val", self.options.appCategories);
                        $("#content-rating").select2("val", self.options.appContentRating);
                        return false;
                    }
                }).data("ui-autocomplete")._renderItem = function(ul, item) {
                    var cut = item.name.length > 30 ? "..." : "",
                        name = item.name.slice(0,30),
                        icon = item.icon;
                    return $("<li><a><img src='" + icon + "' height=32 width=32 />" + name + cut + "</a></li>").appendTo(ul);
                };

                $("#app-name-search").on("change", function() { $("#app-name").val($(this).val()) });

                $("#app-search").on("click", function() {
                    $("#app-name").autocomplete("search", $("#app-name-search").val());
                });



                // Loading categories
                $.ajax({
                    url: "/app/ajax/category",
                    data: {
                        q: "",
                        type: 0,
                        platform: 0
                    },
                    success: function(response) {
                        var i = 0, list;
                        $.each(response.objects, function() {
                            var checked = self.options.appTargetCategories.indexOf(this.id) >= 0 ? "checked" : "";
                            if (i == 0 || i == parseInt(response.objects.length / 2) + 1) {
                                list = $("<div class='col-md-4'><div class='checkbox-list'></div></div>").appendTo($("#app-target-categories")).find(".checkbox-list");
                            }
                            $("<label><input type='checkbox' name='target_categories' value='" + this.id + "' " + checked + ">" + this.name +"</label>").appendTo(list);
                            i++;
                        });

                        $("input[type='checkbox']").uniform();

                        if (!self.options.appId) self.events.onCheckAllClick();
                    }
                });

                // Binding events
                $(".apptypebutton").on("click", self.events.onAppTypeClick);
                $("#check_all").on("click", self.events.onCheckAllClick);
                $("#uncheck_all").on("click", self.events.onUncheckAllClick);

                // Some after init behaviour
                if (!self.options.appOs) {
                    $("#app-category").select2("enable", false);
                    $("#content-rating").select2("enable", false).select2("val", -1);
                    $("#app-name").autocomplete("disable");
                    $("#app-name-search").prop("disabled", true);
                    $("#app-search").prop("disabled", true);
                } else {
                    $("#app-os-{appOs}".replace("{appOs}", self.options.appOs)).trigger('click');
                    $("#content-rating").select2("enable", true).select2("val", self.options.appContentRating);
                    $("#app-name").autocomplete("enable");
                    $("#app-name-search").prop("disabled", false);
                    $("#app-search").prop("disabled", false);
                    self.loadCategories();
                }


                $("form").validate({
                    highlight: function(el) {
                        $(el).parent().addClass("has-error");
                    }
                });

                $("button[type='submit']").on("click", function(evt) {
                    evt.preventDefault();
                    if (self.options.appOs && $("form").valid()) {$("form").submit()}
                });

                $(".select2-choices").addClass("form-control");
            },

            // Events
            events: {
                // Check all target categories
                onCheckAllClick: function() {
                    $.each($("input[type='checkbox']"), function() { if (!$(this).prop("checked")) $(this).trigger("click"); });
                },

                // Uncheck all target categories
                onUncheckAllClick: function() {
                    $.each($("input[type='checkbox']"), function() { if ($(this).prop("checked")) $(this).trigger("click"); });
                },

                // Select app type
                onAppTypeClick: function() {
                    if (Index.options.appOs != $(this).data("app-id")) {
                        Index.options.appOs = $(this).data("app-id");
                        Index.loadCategories();
                        $("#apptype").val(Index.options.appOs);
                        $("#app-category").select2("val", null);
                    }

                    if (+$(this).data('app-id') == ${Application.Os.MOBILE_WEB}) {
                        $('.if-application').hide();
                        $('.if-web').show();
                        $('#app-name').attr('name', 'name-app');
                    } else {
                        $('.if-web').hide();
                        $('.if-application').show();
                        $('#app-name').attr('name', 'name');
                    }

                    $("#app-category").select2("enable", true);
                    $("#content-rating").select2("enable", true);
                    $("#app-name").autocomplete("enable");
                    $("#app-name-search").prop("disabled", false);
                    $("#app-search").prop("disabled", false);
                }
            }
        }
    }();

    $(document).on("ready", function() {
        Index.init({
            appId: ${g.application.id or 0},
            appOs: ${form.os.data or 0},
            appCategories: ${form.data.get('category_groups', [])},
            appStoreURL: "${form.data.get('store_url')}",
            appTargetCategories: ${form.data.get('target_categories', [])},
            appContentRating: ${form.data.get('content_rating')}
        });

    });
</script>