## -*- coding: utf-8 -*-
<%namespace name="bcrumb" file="/layout/bcrumb.mako" />
<%namespace name="fabutton" file="/layout/fa-button.mako" />
<%inherit file="/layout/main.mako" />

<%def name="title()">Блоки</%def>
<%def name="description()">Блоки приложения [${g.application.id}] ${g.application.name}</%def>
<% import datetime %>

${ bcrumb.h(self) }
${ bcrumb.bc([ { 'name' : u'Приложения',  'href' : '/app/', 'icon' : 'rocket' }
             , { 'name' : u"[%s] %s" % (g.application.id, g.application.name), 'href' : '/app/%s/unit' % (g.application.id) }
             , { 'name' : u'Блоки',      'href' : '/app/%s/unit' % (g.application.id) }
             ]) }

<!-- BEGIN PAGE CONTENT-->
<div class="row">
    <div class="col-md-12">

    <div class="portlet">
        <div class="portlet-title">
            <div class="caption"><i class="fa fa-sitemap"></i>${self.description()}</div>
            <div class="actions">
                <a href="/unit/${g.application.id}/new" class="btn btn-success"><i class="fa fa-plus"></i> Добавить новый</a>
            </div>
        </div>
        
        <div class="portlet-body">
            <table class="table table-striped table-bordered table-hover" id="units_table">
            <thead>
            <tr>
                <th style="width:15px;"><input type="checkbox" class="group-checkable" data-set="#units_table .checkboxes"/></th>
                <th>Название</th>
                <th class="text-center">Тип блока</th>
                <th class="text-center">Статус</th>
            </tr>
            </thead>
            <tbody>
            % for z in g.units:
            <tr class="odd gradeX">                                                             <!-- TODO: add actual columns -->
                <td><input type="checkbox" class="checkboxes" value="${z.id}"/></td>
                <td>
                    <div class="row">
                        <div class="col-md-8">
                            
                            <h4 style="margin-top:0;">
                                <span class="badge badge-default">#${z.id}</span>
                                <a href="/unit/${z.id}/source">${z.name}</a>
                            </h4>
                            <p class="text-left">
                                <%
                                    today = datetime.date.today()
                                    monthago = today - datetime.timedelta(days=30)
                                %>
                                ${ fabutton.fab([{ 'text' : u'Активировать',  'href' : '/unit/%d/%d/state/1'%(g.application.id, z.id), 'icon' : 'play' }]) if z.state == 2 else fabutton.fab([{ 'text' : u'Остановить',  'href' : '/unit/%d/%d/state/2'%(g.application.id, z.id), 'icon' : 'pause' }])
                                }
                                ${ fabutton.fab([ 
                                    { 'text' : u'Редактировать',  'href' : '/unit/%d/%d'%(g.application.id, z.id), 'icon' : 'pencil' },
                                    { 'text' : u'Статистика',  'href' : '/stats/pub/days/?date=%s+-+%s&application=%d&unit=%d'%(unicode(monthago.strftime('%d.%m.%Y')), unicode(today.strftime('%d.%m.%Y')), g.application.id, z.id), 'icon' : 'bar-chart-o' },
                                    {
                                        'text': u'Удалить',
                                        'href': '/unit/%d/%d/delete' % (g.application.id, z.id),
                                        'icon': 'trash-o',
                                        'data': {
                                            'aid': '%d' % g.application.id,
                                            'zid': '%d' % z.id
                                        },
                                        'class': 'delete_unit'
                                    }
                                ]) 
                                }
                            </p>
                        </div>
                        <div class="col-md-4 text-right">
                            %if z.type == z.Type.BANNER:
                            <p class="text-muted">
                                <small><i class="fa fa-arrows-alt"></i> ${z.width}x${z.height}</small>
                            </p>
                            %endif
                        </div>
                    </div>
                </td>

                <td>
                    %if z.type == z.Type.BANNER:
                        Баннер
                    %endif
                    %if z.type == z.Type.INTERSTITIAL:
                        Полноэкранный
                    %endif
                </td>

                <td class="text-center">
                    ${fabutton.fas([{'status': z.state, 'type': 'unit'}])}
                </td>
            </tr>
            % endfor
            </tbody>
            </table>
        </div>
    </div>
 
</div>
</div>

<div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog" style="width: 25%">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="myModalLabel">Удалить блоки?</h4>
      </div>
      <div class="modal-body">
        <div class="pasta form-group">
            Действительно удалить блоки <span id="del_list"></span> ?

        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
        <button type="button" id="delete" class="btn btn-danger group_actions">Удалить</button>
      </div>
    </div>
  </div>
</div>
<!-- END PAGE CONTENT-->

<script type="text/javascript">
    var deleteModal = $('#deleteModal');
    $('.delete_unit').on('click', function() {
        var aid = $(this).data('aid'),
            zid = $(this).data('zid');
        $('#delete').one('click', function() {
            window.location = '/app/' + aid + '/unit/' + zid + '/delete';
        });
        if (zid) {
            deleteModal.modal();
            $('#del_list').html('').html(zid);
        }
    });

    $('#units_table').dataTable({
         "aoColumns": [
          { "bSortable": false, "bSearchable": false },
          { "bSearchable": true },
          { "bSortable": false, "bSearchable": false },
          { "bSearchable": false }
//            { "bSearchable": false }
        ],
        "aLengthMenu": [
            [10, 20, 50, -1],
            [10, 20, 50, "Все"]  // change per page values here
        ],
        // set the initial value
        "iDisplayLength": 10,
        "sPaginationType": "bootstrap",
        "oLanguage": {
            "sLengthMenu": "_MENU_ блоков",
            "oPaginate": {
                "sPrevious": "Назад",
                "sNext": "Вперед"
            },
            "sSearch": "Поиск: ",
            "sInfo" : "Показано с _START_ по _END_ из _TOTAL_ блоков",
            "sInfoempty" : "У Вас пока не добавлено ни одного блока"
        },
        "aoColumnDefs": [{
                'bSortable': false,
                'aTargets': [0]
            }
        ]
    });

    jQuery('#units_table .group-checkable').change(function () {
        var set = jQuery(this).attr("data-set");
        var checked = jQuery(this).is(":checked");
        jQuery(set).each(function () {
            if (checked) {
                $(this).attr("checked", true);
            } else {
                $(this).attr("checked", false);
            }
        });
        jQuery.uniform.update(set);
    });

    jQuery('#units_table_wrapper .dataTables_filter input').addClass("form-control input-small"); // modify table search input
    jQuery('#units_table_wrapper .dataTables_length select').addClass("form-control input-xsmall"); // modify table per page dropdown
    jQuery('#units_table_wrapper .dataTables_length select').select2(); // initialize select2 dropdown
</script>
