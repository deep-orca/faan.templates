<%namespace name="bcrumb" file="/layout/bcrumb.mako" />
<%namespace name="fabutton" file="/layout/fa-button.mako" />
<%inherit file="/layout/main.mako" />

<%!
    from faan.core.model.pub.application import Application
    from faan.core.model.security.account import Account
%>

<%
    for s in g.selects:
        if s.get('name') == 'account':
            s.update({'header': u'Аккаунт'})
        if s.get('name') == 'group':
            s.update({'header': u'Группа'})
%>

<%def name="title()">Площадки</%def>
<%def name="description()"></%def>


<script src="http://jwpsrv.com/library/MQkkJAm8EeO9QiIACusDuQ.js"></script>
<script src="/global/static/plugins/fancybox/source/jquery.fancybox.js"></script>
<script type="text/javascript" src="/static/scripts/filters.js"></script>

<link rel="stylesheet" href="/global/static/plugins/fancybox/source/jquery.fancybox.css"/>
<link href="/global/static/plugins/bootstrap-modal/css/bootstrap-modal-bs3patch.css" rel="stylesheet" type="text/css"/>
<link href="/global/static/plugins/bootstrap-modal/css/bootstrap-modal.css" rel="stylesheet" type="text/css"/>


${ bcrumb.h(self) }

<%def name="show_select_item(select)">
    <div class="form-group">
        <label for="${select['name']}" class="control-label col-md-3">${select['header']}</label>
        <div class="col-md-3">
                <input name="${select['name']}" type="hidden" id="${select['id']}" style="width:324px"/>
        </div>
    </div>
</%def>

<%def name="render_table(id)">
 <!--START TABLE-->
 <table class="table table-striped table-bordered table-hover" id="${id}">
 <thead>
 <tr>
     <th class="table-checkbox">
         <input type="checkbox" class="group-checkable" data-set="#${id} .checkboxes"/>
     </th>
     <th>id</th>
     <th>Название</th>
     <th class="text-center" >Статус</th>
     <th class="text-center" >ОС</th>
     <th class="text-center" >Страна</th>
     <th>Пользователь</th>
     <th class="text-center">Возрастная категория</th>
 </tr>
 </thead>
 <tbody>

${caller.body()}

 </tbody>
 </table>
 <!--END TABLE-->
</%def>

<%def name="show_filters(selects)">
    %for select in selects:
        ${self.show_select_item(select)}
    %endfor
    <!-- ==== SUBMIT ==== --->
    <div class="form-group">
    <label class="control-label col-md-3"></label>
        <div class="col-sm-1">
            <a href="#" id="apply_changes" class="btn btn-success">Применить</a>
        </div>
        <div class="col-sm-3">
            <div style="padding-left: 50px;" data-toggle="buttons" class="btn-group osfilter" id="platform_select">
                <label id="os_type_all" class="btn btn-default btn-sm filteros active">
                <input type="radio" class="toggle"> Все </label>
                <label id="os_type_ios" class="btn btn-default btn-sm filteros">
                <input type="radio" class="toggle"> <i class="fa fa-apple"></i></label>
                <label id="os_type_android" class="btn btn-default btn-sm filteros">
                <input type="radio" class="toggle"> <i class="fa fa-android"></i></label>
                <label id="os_type_windows" class="btn btn-default btn-sm filteros">
                <input type="radio" class="toggle"> <i class="fa fa-windows"></i></label>
                <label id="os_type_mobile_site" class="btn btn-default btn-sm filteros">
                <input type="radio" class="toggle"> <i class="fa fa-globe"></i></label>
            </div>
        </div>
    </div>

</%def>

<!-- BEGIN PAGE CONTENT-->
<div class="row">
<div class="col-md-12">
    <div class="portlet">
        <div class="portlet-title">

        <div class="caption"><i class="fa fa-rocket"></i>Площадки</div>
            <div class="actions">
                <div class="btn-group">
                    <a class="btn btn-info" href="#" data-toggle="dropdown">
                        <i class="fa fa-cogs"></i> Действие <i class="fa fa-angle-down"></i>
                    </a>
                    <ul class="dropdown-menu pull-right">
                        <li><a href="#group_action_approve" data-toggle="modal" id="disable" class="group_actions" ><i class="fa fa-times"></i> Заблокировать</a></li>
                        <li><a href="#group_action_approve" data-toggle="modal" id="active" class="group_actions" ><i class="fa fa-play"></i> Активировать</a></li>
                        <li><a href="#group_action_approve" data-toggle="modal" id="delete" class="group_actions" ><i class="fa fa-trash-o"></i> Удалить</a></li>
                    </ul>
                </div>
            </div>
        </div>

        <div class="portlet-body">

                <form method="GET" class="form-horizontal">
                    ${self.show_filters(g.selects)}
			    </form>

                <ul class="nav nav-pills">
                    %if getattr(g, 'tab', '') and g.tab == 'new':
                    <li class="active">
                    %else:
                    <li class="">
                    %endif
                        <a href="#tab_2_1" data-toggle="tab" id="tab_new">Новые</a>
                    </li>
                    %if getattr(g, 'tab', '') and g.tab == 'all':
                    <li class="active">
                    %else:
                    <li class="">
                    %endif
                        <a href="#tab_2_2" data-toggle="tab" id="tab_all">Активные</a>
                    </li>
                    %if getattr(g, 'tab', '') and g.tab == 'banned':
                    <li class="active">
                    %else:
                    <li class="">
                    %endif
                        <a href="#tab_2_3" data-toggle="tab" id="tab_banned">Заблокированные</a>
                    </li>
                </ul>
                <div class="tab-content">
                    %if getattr(g, 'tab', '') and g.tab == 'new':
                    <div class="tab-pane fade active in" id="tab_2_1">
                    %else:
                    <div class="tab-pane fade" id="tab_2_1">
                    %endif
                        <p>
                        <%self:render_table id="stats_table_new">

                        </%self:render_table>
                        </p>
                        <div class="row">
                            <div class="col-md-6 col-sm-12"></div>
                            <div class="col-md-6 col-sm-12">
                                <div id="log_table_filter_banned" class="dataTables_filter">
                                    <label>
                                    &nbsp
                                        <ul class="pagination">
                                            <li class="prev disabled"><a title="Prev" href="#"><i class="fa fa-angle-left"></i></a></li>
                                            <li class="next"><a title="Next" href="#"><i class="fa fa-angle-right"></i></a></li>
                                        </ul>
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>
                    %if getattr(g, 'tab', '') and g.tab == 'all':
                    <div class="tab-pane fade active in" id="tab_2_2">
                    %else:
                    <div class="tab-pane fade" id="tab_2_2">
                    %endif
                        <p>
                        <%self:render_table id="stats_table_all">

                        </%self:render_table>
                        </p>
                        <div class="row">
                            <div class="col-md-6 col-sm-12"></div>
                            <div class="col-md-6 col-sm-12">
                                <div id="log_table_filter_banned" class="dataTables_filter">
                                    <label>
                                    &nbsp
                                        <ul class="pagination">
                                            <li class="prev disabled"><a title="Prev" href="#"><i class="fa fa-angle-left"></i></a></li>
                                            <li class="next"><a title="Next" href="#"><i class="fa fa-angle-right"></i></a></li>
                                        </ul>
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>
                    %if getattr(g, 'tab', '') and g.tab == 'banned':
                    <div class="tab-pane fade active in" id="tab_2_3">
                    %else:
                    <div class="tab-pane fade" id="tab_2_3">
                    %endif
                        <p>
                        <%self:render_table id="stats_table_banned">

                        </%self:render_table>
                        </p>
                        <div class="row">
                            <div class="col-md-6 col-sm-12"></div>
                            <div class="col-md-6 col-sm-12">
                                <div id="log_table_filter_banned" class="dataTables_filter">
                                    <label>
                                    &nbsp
                                        <ul class="pagination">
                                            <li class="prev disabled"><a title="Prev" href="#"><i class="fa fa-angle-left"></i></a></li>
                                            <li class="next"><a title="Next" href="#"><i class="fa fa-angle-right"></i></a></li>
                                        </ul>
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
        </div>
    </div>
</div>
</div>
<!-- END PAGE CONTENT-->

<!-- change_account_status -->
<div id="group_action_approve" class="modal fade" tabindex="-1" data-width="760" style="top:10%">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
        <h4 class="modal-title"><span id="approve_header"></span></h4>
    </div>
    <form method="POST" class="form-horizontal">
        <div class="modal-body">
            <div class="row">
                <div class="col-md-10">
                    <p>
                        Вы действительно хотите <span id='approve_chose_action'></span> приложения <b><span id="approve_elements_list"></span></b> ?
                    </p>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" data-dismiss="modal" class="btn btn-default">Отменить</button>
            <a href="#" data-dismiss="modal" id="button_group_action_approve" class="btn btn-warning">Подтвердить</a>
        </div>
    </form>
</div>



<!-- SCRIPTS INDEX -->
<script type="text/javascript">
Index = function() {
    return {

        // Some table attributes
        data: null,
        total: +0,

        // Pagination options
        pag_size: +50,
        pag_start: 0,

        //OS attributes
        os: "all",

        tab: null,

        init: function() {
            $("#apply_changes").on("click", function(e) {
                Index["loadData_" + Index.tab]()
            });

            %for tab in ('all', 'new', 'banned',):
            $("#tab_${tab}").on("click", function(e) {
                Index.tab = "${tab}";
                Index.pag_size = + 50;
                Index.pag_start = + 0;
                Index.loadData_${tab}();
            });
            %endfor

            %if getattr(g, 'tab', ''):
                this.tab = "${g.tab}";
            %else:
                this.tab = "new";
            %endif

            this.first = true;
        },

        %for tab in ('all', 'new', 'banned'):
        loadData_${tab}: function(os, start, count, search) {
            try { $("#stats_table_${tab}").DataTable().destroy(); } catch (e) {}

            start = start ? start : 0;
            count = count ? count : Index.pag_size;
            search = search ? search : "";
            os = os ? os : "";

            var url = "/app/ajax/table?tab=${tab}" + "&values=" + filterBox.getValues() + "&os=" + os + "&start=" + start + "&count=" + count + "&search=" + search;

            c = [
               { data: 'checkbox' },
               { data: 'id' },
               { data: 'application' },
               { data: 'state' },
               { data: 'os' },
               { data: 'country' },
               { data: 'user' },
               { data: 'age' }
            ];

            $("#stats_table_${tab}").dataTable({
                processing: true,
                ajax: {
                    url: url,
                    dataSrc: function(response) {
                        Index.total = response.meta.total;
                        return response.data;
                    }
                },
                columns: c,
                lengthChange: true,
                "aLengthMenu": [
                    [25, 50, 100, -1],
                    [25, 50, 100, "All"] // change per page values here
                ],
                // set the initial value
                "iDisplayLength": 50,
                "sPaginationType": "bootstrap",
                "oLanguage": {
                    "sLengthMenu": "_MENU_ records",
                    "oPaginate": {
                        "sPrevious": "Prev",
                        "sNext": "Next"
                    }
                },
                "aoColumnDefs": [{
                        'bSortable': false,
                        'aTargets': [0]
                    }
                ],
                order: [],
                paging: false,
                bFilter: false,
                bInfo: false
            });

            $("#stats_table_${tab} .group-checkable").change(function () {
                var set = jQuery(this).attr("data-set");
                var checked = jQuery(this).is(":checked");
                jQuery(set).each(function () {
                    if (checked) {
                        $(this).attr("checked", true);
                        $(this).parents('tr').addClass("active");
                        $(this).parents('span').addClass("checked");
                        console.log($(this));
                    } else {
                        $(this).attr("checked", false);
                        $(this).parents('tr').removeClass("active");
                        $(this).parents('span').removeClass("checked");
                    }
                });
                jQuery.uniform.update(set);

            });

            // Add select items.
            $("#stats_table_${tab}_wrapper .row")[0].children[0].innerHTML = '<div class="dataTables_length" id="stats_table_${tab}_length"><label><select name="stats_table_${tab}_length" id="_stats_table_${tab}_length" aria-controls="stats_table_${tab}" class="form-control input-xsmall"><option value="50">50</option><option value="100">100</option><option value="150">150</option></select> records</label></div>'
            $('#stats_table_${tab}_wrapper .dataTables_length select').select2().on("select2-selecting", function(e) {
                Index.pag_size = + e.val;
                Index.pag_start = + 0;
                Index.loadData_${tab}(Index.os, 0, Index.pag_size);
                $('#stats_table_${tab}_wrapper .dataTables_length select').select2("val", e.val.toString());
            });

            //Add search item.
            $("#stats_table_${tab}_wrapper .row")[0].children[1].innerHTML = '<div id="stats_table_new_filter" class="dataTables_filter"><label>Search:<input type="search" class="form-control input-medium" aria-controls="stats_table_new"></label></div>'
            $('#stats_table_${tab}_wrapper .dataTables_filter input').on("change", function(){
                var text = $(this).val();
                Index.pag_start = + 0;
                Index.loadData_${tab}(Index.os, Index.pag_start, Index.pag_size, text);
                $('#stats_table_${tab}_wrapper .dataTables_length select').select2("val", Index.pag_size);
                $('#stats_table_${tab}_wrapper .dataTables_filter input').val(text);
            });

            // Parsing parameters for the table.
            $("#stats_table_${tab}").on('order.dt',  function () {
                var rows = $("#stats_table_${tab}").dataTable().fnGetNodes();

                if (rows.length < Index.pag_size)
                    $(".next").addClass("disabled");
                else
                    $(".next").removeClass("disabled");

                for (var i = 0; i < rows.length; i++) {
                    // Application
                    try {
                        var categories = "";
                        var app = rows[i].cells[2].innerHTML;
                        app = JSON.parse(app);
                        for (var j = 0, k = 0; j < app.categories.length; j++){
                            categories += app.categories[j] + ", ";
                            k += app.categories[j].length;
                            if (k > 20){
                                categories += "<br>";
                                k = 0;
                            }
                        }
                        rows[i].cells[2].innerHTML = ' \
                            <h4 style="margin-top:0;"> \
                                <a href="{store_url}">{app_name}</a> \
                            </h4> \
                            <p class="text-muted text-left"> \
                                <small> \
                                    <i class="fa fa-tags"></i> \
                                       {categories}  \
                                     \
                                </small> \
                            </p>'
                        .replace("{app_name}", app.name)
                        .replace("{store_url}", "/app/{app_id}/unit/".replace("{app_id}", app.id))
                        .replace("{categories}", categories);

                        //Actions
                        try {
                            var state = rows[i].cells[3].innerHTML;
                            var app_id = rows[i].cells[1].innerHTML;
                            state = JSON.parse(state);
                            if (state.id == ${Application.State.NONE}) {
                                var url = "/app/active/{app_id}?tab=${tab}".replace('{app_id}', app_id);
                                rows[i].cells[2].innerHTML += '<a href="' + url + '" class="newclass"><i class="fa fa-check fabutton tooltips" data-placement="top" data-original-title="Активировать"></i></a>' + "&nbsp"
                                var url = "/app/disable/{app_id}?tab=${tab}".replace('{app_id}', app_id);
                                rows[i].cells[2].innerHTML += '<a href="' + url + '" class="newclass"><i class="fa fa-times fabutton tooltips" data-placement="top" data-original-title="Заблокировать"></i></a>' + "&nbsp"
                                var url = "/app/edit/{app_id}?tab=${tab}".replace('{app_id}', app_id);
                                rows[i].cells[2].innerHTML += '<a href="' + url + '" class="newclass"><i class="fa fa-pencil fabutton tooltips" data-placement="top" data-original-title="Редактировать"></i></a>'+ "&nbsp"
                                var url = "/stats/pub/app/?application={app_id}".replace('{app_id}', app_id);
                                rows[i].cells[2].innerHTML += '<a href="' + url + '" class="newclass"><i class="fa fa-bar-chart-o fabutton tooltips" data-placement="top" data-original-title="Редактировать"></i></a>'+ "&nbsp"
                            } else if (state.id == ${Application.State.ACTIVE}) {
                                var url = "/app/disable/{app_id}?tab=${tab}".replace('{app_id}', app_id);
                                rows[i].cells[2].innerHTML += '<a href="' + url + '" class="newclass"><i class="fa fa-times fabutton tooltips" data-placement="top" data-original-title="Заблокировать"></i></a>'+ "&nbsp"
                                var url = "/app/stop/{app_id}?tab=${tab}".replace('{app_id}', app_id);
                                rows[i].cells[2].innerHTML += '<a href="' + url + '" class="newclass"><i class="fa fa-pause fabutton tooltips" data-placement="top" data-original-title="Приостановить"></i></a>'+ "&nbsp"
                                var url = "/app/edit/{app_id}?tab=${tab}".replace('{app_id}', app_id);
                                rows[i].cells[2].innerHTML += '<a href="' + url + '" class="newclass"><i class="fa fa-pencil fabutton tooltips" data-placement="top" data-original-title="Редактировать"></i></a>'+ "&nbsp"
                                var url = "/stats/pub/app/?application={app_id}".replace('{app_id}', app_id);
                                rows[i].cells[2].innerHTML += '<a href="' + url + '" class="newclass"><i class="fa fa-bar-chart-o fabutton tooltips" data-placement="top" data-original-title="Редактировать"></i></a>'+ "&nbsp"
                            } else if (state.id == ${Application.State.DISABLED}) {
                                var url = "/app/disable/{app_id}?tab=${tab}".replace('{app_id}', app_id);
                                rows[i].cells[2].innerHTML += '<a href="' + url + '" class="newclass"><i class="fa fa-times fabutton tooltips" data-placement="top" data-original-title="Заблокировать"></i></a>'+ "&nbsp"
                                var url = "/app/active/{app_id}?tab=${tab}".replace('{app_id}', app_id);
                                rows[i].cells[2].innerHTML += '<a href="' + url + '" class="newclass"><i class="fa fa-play fabutton tooltips" data-placement="top" data-original-title="Активировать"></i></a>'+ "&nbsp"
                                var url = "/app/edit/{app_id}?tab=${tab}".replace('{app_id}', app_id);
                                rows[i].cells[2].innerHTML += '<a href="' + url + '" class="newclass"><i class="fa fa-pencil fabutton tooltips" data-placement="top" data-original-title="Редактировать"></i></a>'+ "&nbsp"
                                var url = "/stats/pub/app/?application={app_id}".replace('{app_id}', app_id);
                                rows[i].cells[2].innerHTML += '<a href="' + url + '" class="newclass"><i class="fa fa-bar-chart-o fabutton tooltips" data-placement="top" data-original-title="Редактировать"></i></a>'+ "&nbsp"
                            } else  {
                                var url = "/app/active/{app_id}?tab=${tab}".replace('{app_id}', app_id);
                                rows[i].cells[2].innerHTML += '<a href="' + url + '" class="newclass"><i class="fa fa-check fabutton tooltips" data-placement="top" data-original-title="Активировать"></i></a>'+ "&nbsp"
                                var url = "/app/edit/{app_id}?tab=${tab}".replace('{app_id}', app_id);
                                rows[i].cells[2].innerHTML += '<a href="' + url + '" class="newclass"><i class="fa fa-pencil fabutton tooltips" data-placement="top" data-original-title="Редактировать"></i></a>'+ "&nbsp"
                            }
                        } catch(e) { }


                    } catch(e) { }

                    //OS
                    var os = rows[i].cells[4].innerHTML;
                    if (! isNaN(Number(os))) {
                        switch(os){
                            case "${Application.Os.IOS}":
                                rows[i].cells[4].innerHTML = '<i class="fa fa-apple" style="font-size:22px;line-height: 22px;"></i><div style="display:none">iOS</div>'
                                break;
                            case "${Application.Os.ANDROID}":
                                rows[i].cells[4].innerHTML = '<i class="fa fa-android" style="font-size:22px;line-height: 22px;"></i><div style="display:none">Android</div>'
                                break;
                            case "${Application.Os.WINPHONE}":
                                rows[i].cells[4].innerHTML = '<i class="fa fa-windows" style="font-size:22px;line-height: 22px;"></i><div style="display:none">Windows</div>'
                                break;
                            case "${Application.Os.MOBILE_WEB}":
                                rows[i].cells[4].innerHTML = '<i class="fa fa-globe" style="font-size:22px;line-height: 22px;"></i><div style="display:none">Mobile Site</div>'
                                break;
                            default:
                                rows[i].cells[4].innerHTML = '<i class="fa fa-times" style="font-size:22px;line-height: 22px;"></i><div style="display:none">Unknown</div>'
                                break;
                        }
                    }
                    // Checkbox
                    var app_id = rows[i].cells[1].innerHTML;
                    rows[i].cells[0].innerHTML = '<input type="checkbox" class="checkboxes" value="{app_id}"/>'.replace('{app_id}', app_id);

                    // User
                    try {
                        var user = rows[i].cells[6].innerHTML;
                        user = JSON.parse(user);
                        rows[i].cells[6].innerHTML = '<a href="/app/?tab={ctab}&account={user_id}">{user_name}</a>'
                            .replace("{user_id}", user.id)
                            .replace("{ctab}", Index.tab)
                            .replace("{user_name}", user.name);
                    } catch(e) { }

                    // Category
                    var category = rows[i].cells[7].innerHTML;
                    if (! isNaN(Number(category)))
                        rows[i].cells[7].innerHTML = app_content_rating[category];

                    // Status
                    try {
                        var stat = rows[i].cells[3].innerHTML;
                        stat = JSON.parse(stat);
                        var stat_class = "";
                        if (stat.id == "${Application.State.ACTIVE}"){
                            stat_class = "badge-success";
                        } else if (stat.id == "${Application.State.DISABLED}") {
                            stat_class = "badge-warning";
                        } else if (stat.id == "${Application.State.NONE}") {
                            stat_class = "badge-info";
                        } else {
                            stat_class = "badge-danger";
                        }
                        rows[i].cells[3].innerHTML = '<span class="badge {stat_class}">{stat_value}</span>'
                            .replace("{stat_value}", app_state[stat.id])
                            .replace("{stat_class}", stat_class);
                    } catch(e) { console.log("status error"); }


                }

            // Add summary.
            var start = +Index.pag_start + 1, end;
            if (!Index.total)
                start = end = 0;
            else if ((+Index.pag_start + Index.pag_size) > +Index.total)
                end = Index.total;
            else
                end = + Index.pag_start + Index.pag_size;
            $("#stats_table_${tab}_wrapper .row")[1].children[0].innerHTML = 'С {start} по {end}, всего {total}'
                .replace("{total}", Index.total)
                .replace("{start}", start)
                .replace("{end}", end);

            });
        },
        %endfor

        fake: null
    }
}();
</script>
<!-- SCRIPTS INDEX END-->


<script type="text/javascript">
    $(document).on("ready", function() {

        account_group = {
            "${Account.Groups.NONE}": 'Без группы',
            "${Account.Groups.PUB}": 'Разработчик',
            "${Account.Groups.ADV}": 'Рекламодатель'
        }

        app_content_rating = {
            "${Application.ContentRating.NONE}": 'Незвестно',
            "${Application.ContentRating.EVERYONE}": 'Без ограничений',
            "${Application.ContentRating.LOW}": 'Младшая',
            "${Application.ContentRating.MEDIUM}": 'Средняя',
            "${Application.ContentRating.HIGH}": 'Старшая'
        }

        app_state = {
            "${Application.State.NONE}": 'Новый',
            "${Application.State.ACTIVE}": 'Активный',
            "${Application.State.DISABLED}": 'На паузе',
            "${Application.State.SUSPENDED}": 'Заблокированный'
        }

        $(".fancybox").fancybox({"type" : "image"});

        // Init filters.
        %for select in g.selects:
            <%
                _name = 'account' if select['name'] in ('advertiser', 'publisher', ) else select['name']
                placeholder = getattr(g, str(_name) + '_name', select['header'])
            %>
            filterBox.push({
                id: "${select['id']}",
                name: "${select['name']}",
                header: "${select['header']}",
                placeholder: "${placeholder}",
                value: "${getattr(g, str(select['name']), '')}",
                url: "/app/ajax/select"
            });
        %endfor

        Index.init();
        %if getattr(g, 'tab', ''):
            Index.loadData_${g.tab}();
        %else:
            Index.loadData_new();
        %endif

        // Pagination sctipt
        $(".prev").on("click", function() {
            if ($(".prev").attr("class").indexOf("disabled") > 0) {
               return;
            }

            Index.pag_start -= Number(Index.pag_size);
            if (+Index.pag_start < 0) {
               Index.pag_start = +0;
               $(".prev").addClass("disabled");
               return;
            }
            $(".next").removeClass("disabled");

            Index["loadData_" + Index.tab](Index.os, Index.pag_start, Index.pag_size);

        });
        $(".next").on("click", function() {
            console.log("next!");
            if ($(".next").attr("class").indexOf("disabled") > 0) {
               return;
            }

            $(".prev").removeClass("disabled");

            Index.pag_start = Number(Index.pag_start) + Number(Index.pag_size);
            Index["loadData_" + Index.tab](Index.os, Index.pag_start, Index.pag_size);
        });
    });
</script>

<script type="text/javascript">

    action_assistant = {
        objects: null,
        objects_ids: null,
        action: null
    };

   $('.group_actions').on('click', function(){
        var objects = $('.checkboxes:checked');
        var objects_ids = '';
        if (objects.length > 0){
            for (var i = 0; i < objects.length; i++){
                if (i == objects.length - 1){
                    objects_ids += objects[i].value;
                    continue;
                }
                objects_ids += objects[i].value + ',';
            }

            var group_checkbox = $("#stats_table_{tab} .group-checkable".replace("{tab}", Index.tab));
            group_checkbox[0].parentElement.className = "";

            action_assistant.action = this.id
            action_assistant.objects = objects
            action_assistant.objects_ids = objects_ids

            switch(this.id) {
                case 'active':
                    $('#approve_chose_action').html("активировать");
                    $('#approve_header').html("Активирование приложений");
                    break;
                case 'disable':
                    $('#approve_chose_action').html("заблокировать");
                    $('#approve_header').html("Блокирование приложений");
                    break;
                case 'delete':
                    $('#approve_chose_action').html("удалить");
                    $('#approve_header').html("Удаление приложений");
                    break;
            }

            $('#approve_elements_list').html("[" + objects_ids + "]");
        }
   });

   $("#button_group_action_approve").on('click', function(){
        console.log(action_assistant);
        $.post('/app/group_actions', {objects_list: action_assistant.objects_ids, action: action_assistant.action}, after_load);

        objects = action_assistant.objects

        for (var i = 0; i < objects.length; i++){
            tr = objects[i].parentElement.parentElement;
            tr.className = tr.className.replace("active", "");
            objects[i].parentElement.className = "";
            objects[i].checked = false;
            tr.innerHTML = "";
        }
   });

   function after_load(data){ }
</script>

<script type="text/javascript">
    function os_filter(os) {
        Index["loadData_" + Index.tab](os)
    }

    $("#os_type_all").on("click", function() {
        Index.os = "all";
        os_filter("all");
    });
    $("#os_type_ios").on("click", function() {
        Index.os = "ios";
        os_filter("ios");
    });
    $("#os_type_android").on("click", function() {
        Index.os = "android";
        os_filter("android");
    });
    $("#os_type_windows").on("click", function() {
        Index.os = "windows";
        os_filter("windows");
    });
    $("#os_type_mobile_site").on("click", function() {
        Index.os = "mobile_site";
        os_filter("mobile_site");
    });
</script>