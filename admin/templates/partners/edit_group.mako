<%namespace name="bcrumb" file="/layout/bcrumb.mako" />
<%namespace name="fabutton" file="/layout/fa-button.mako" />
<%inherit file="/layout/main.mako" />

<%
    import datetime

%>

<%def name="title()">Группы</%def>
<%def name="description()"></%def>

${ bcrumb.h(self) }


<!-- BEGIN PAGE CONTENT-->
<div class="row">
<div class="col-md-12">
    <div class="portlet">
        <div class="portlet-title">
            <div class="caption"><i class="fa fa-rocket"></i>
                Редактировать группу
            </div>
        </div>

        <div class="portlet-body">
        	<form method="POST" class="form-horizontal" name="edit_group" onsubmit="return validateForm()">
                <div class="form-group">
					<label class="control-label col-md-4">Название группы  </label>
					<div class="col-md-4">
						<input type="text" class="form-control" name="name" value="${g.group_name}">
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-md-4">Процент отчислений  </label>
					<div class="col-md-4">
						<input type="text" class="form-control" name="rate" value="${g.group_rate}">
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-md-4">Цена за запрос  </label>
					<div class="col-md-4">
						<input type="text" class="form-control" name="request" value="${g.group_request}">
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-md-4">Цена за overlay  </label>
					<div class="col-md-4">
						<input type="text" class="form-control" name="overlay" value="${g.group_overlay}">
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-md-4">Цена за endcard  </label>
					<div class="col-md-4">
						<input type="text" class="form-control" name="endcard" value="${g.group_endcard}">
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-md-4">Считать просмотренным после %</label>
					<div class="col-md-4">
						<input type="text" class="form-control" name="ration" value="${g.group_ration}">
					</div>
				</div>

				<input type="hidden" class="form-control" name="account" value="${g.account_type}">

				<!-- ==== SUBMIT ==== --->
                <div class="form-group">
                    <div class="col-sm-offset-2 col-sm-10">
                        <button type="submit" id="sample_editable_1_new" class="btn btn-success">
                            Редактировать
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
</div>
<!-- END PAGE CONTENT-->

<script type="text/javascript">
    // Validate the form
    function validateForm() {
        var gr_name = document.forms['edit_group']['name'].value;

        // Make a form object.
        var _form = {
            rate: document.forms['edit_group']['rate'].value,
            request: document.forms['edit_group']['request'].value,
            overlay: document.forms['edit_group']['overlay'].value,
            endcard: document.forms['edit_group']['endcard'].value,
            ration: document.forms['edit_group']['ration'].value
        }

        if (gr_name == null || gr_name == "") {
            alert("The group name must not be empty!");
            return false;
        }
        for (var key in _form) {
            var attr = _form[key];
            if ( attr != "" && isNaN(Number(attr))) {
                alert("The group " + key + " must be digital!");
                return false;
            }
        }
        return true;
    }
</script>