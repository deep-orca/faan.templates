<%namespace name="bcrumb" file="/layout/bcrumb.mako" />
<%namespace name="fabutton" file="/layout/fa-button.mako" />
<%inherit file="/layout/main.mako" />

<%!
    import datetime
    from faan.core.model.security.account import Account
%>

<%def name="title()">Группы</%def>
<%def name="description()"></%def>

${ bcrumb.h(self) }

<link href="/global/static/plugins/bootstrap-modal/css/bootstrap-modal-bs3patch.css" rel="stylesheet" type="text/css"/>
<link href="/global/static/plugins/bootstrap-modal/css/bootstrap-modal.css" rel="stylesheet" type="text/css"/>

<!-- FUNCTION BEGIN --!>

<%def name="render_table(id)">
 <!--START TABLE-->
 <table class="table table-striped table-bordered table-hover" id="${id}">
 <thead>
 <tr>
     <th>Название</th>
     <th>Процент отчислений</th>
     <th>Стоимость запроса</th>
     <th>Стоимость overlay</th>
     <th>Стоимость endcard</th>
     <th>Ration</th>
     <th>Действия</th>
 </tr>
 </thead>
 <tbody>

${caller.body()}

 </tbody>
 </table>


 <script type="text/javascript">
                $('#${id}').dataTable({
                "aoColumns": [
                  //{ "bSortable": false },
                  null,
                  null,
                  null,
                  null,
                  null,
                  null,
                  null
                 ## { "bSortable": false }
                ],
                "aLengthMenu": [
                    [25, 50, 100, -1],
                    [25, 50, 100, "All"] // change per page values here
                ],
                // set the initial value
                "iDisplayLength": 50,
                "sPaginationType": "bootstrap",
                "oLanguage": {
                    "sLengthMenu": "_MENU_ records",
                    "oPaginate": {
                        "sPrevious": "Prev",
                        "sNext": "Next"
                    }
                },
                "aoColumnDefs": [{
                        'bSortable': false,
                        'aTargets': [0]
                    }
                ]
            });


            jQuery('#${id}_wrapper .dataTables_filter input').addClass("form-control input-medium"); // modify table search input
            jQuery('#${id}_wrapper .dataTables_length select').addClass("form-control input-xsmall"); // modify table per page dropdown
            jQuery('#${id}_wrapper .dataTables_length select').select2();
</script>

<!--END TABLE-->
</%def>

<%def name="show_column(group)">
    <tr>
        <td><a href="/partners/group/edit/${group.id}/?group_name=${group.name}&group_rate=${group.rate or ""}&group_request=${group.request_cost or ""}&group_overlay=${group.overlay_cost or ""}&group_endcard=${group.endcard_cost or ""}&group_ration=${group.completion_ratio or ""}&account_type=${group.account_type}">${group.name}</a></td>
        <td>${group.rate}</td>
        <td>${group.request_cost}</td>
        <td>${group.overlay_cost}</td>
        <td>${group.endcard_cost}</td>
        <td>${group.completion_ratio}</td>
        <td>
        ${ fabutton.fab([
             { 'text' : u'Редактировать',
                'href' : '/partners/group/edit/%s/?group_name=%s&group_rate=%s&group_request=%s&group_overlay=%s&group_endcard=%s&group_ration=%s&account_type=%s' % (group.id, group.name, group.rate or '', group.request_cost or '', group.overlay_cost or '', group.endcard_cost or '', group.completion_ratio or '', group.account_type),
                'icon' : 'edit' },
             { 'text' : u'Удалить',
                'href' : '/partners/group/delete/%d?account_type=%s' % (group.id, group.account_type),
                'icon' : 'trash-o' },
         ])
        }
        </td>
    </tr>
</%def>


<%def name="render_group(partner_type='', account_group=0, id='')">
<div class="row">
<div class="col-md-12">
    <div class="portlet">
        <div class="portlet-title">
            <div class="caption"><i class="fa fa-rocket"></i>Существующие группы: ${partner_type}</div>
            <div class="actions">
                <div class="btn-group">
                    <a data-toggle="modal" href="#create_group" class="btn btn-success"> <i class="fa fa-plus"></i> Создать группу</a>
                </div>
            </div>
        </div>
        <div class="portlet-body">
        	<%self:render_table id="${id}">
        	    %for group in g.partner_groups:
        	        %if account_group == group.account_type:
        	            ${show_column(group)}
        	        %endif
        	    %endfor
        	</%self:render_table>
        </div>
    </div>
</div>
</div>
</%def>
<!-- FUNCTION END --!>



<!-- BEGIN PAGE CONTENT-->
<ul class="nav nav-pills">
    %if getattr(g, 'tab', '') and g.tab == 'pub':
    <li class="active">
    %else:
    <li class="">
    %endif
        <a href="#tab_2_1" data-toggle="tab" id="tab_pub">Вебмастера</a>
    </li>
    %if getattr(g, 'tab', '') and g.tab == 'adv':
    <li class="active">
    %else:
    <li class="">
    %endif
        <a href="#tab_2_2" data-toggle="tab" id="tab_adv">Рекламодатели</a>
    </li>
</ul>

<div class="tab-content">
     %if getattr(g, 'tab', '') and g.tab == 'pub':
     <div class="tab-pane fade active in" id="tab_2_1">
     %else:
     <div class="tab-pane fade" id="tab_2_1">
     %endif
         <p>
          <% partner_type = u'Вебмастера' %>
          ${render_group(partner_type=partner_type, account_group=Account.Groups.PUB, id='stats_table_pub')}
         </p>
     </div>
     %if getattr(g, 'tab', '') and g.tab == 'adv':
     <div class="tab-pane fade active in" id="tab_2_2">
     %else:
     <div class="tab-pane fade" id="tab_2_2">
     %endif
         <p>
          <% partner_type = u'Рекламодатели' %>
          ${render_group(partner_type=partner_type, account_group=Account.Groups.ADV, id='stats_table_adv')}
         </p>
     </div>
</div>
<!-- END PAGE CONTENT-->

<!-- change_account_status -->
<div id="create_group" class="modal fade" tabindex="-1" data-width="760" style="top:25%">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
        <h4 class="modal-title">Создать новую группу</h4>
    </div>
    <form name="create_group_form" method="POST" class="form-horizontal" onsubmit="return validateForm()">

        <div class="modal-body">
            <p>
                <div class="row">
                    <div class="col-md-6">
                        <label class="control-label">Название</label>
                    </div>
                    <div class="col-md-6">
                        <input type="text" class="form-control" name="group_name" value="">
                    </div>
                </div>
            </p>
            <hr>
            <p>
                <div class="row">
                    <div class="col-md-6">
                        <label class="control-label">% отчисления</label>
                    </div>
                    <div class="col-md-6">
                        <input type="text" class="form-control" name="group_rate" value="">
                    </div>
                </div>
            </p>
            <p>
                <div class="row">
                    <div class="col-md-6">
                        <label class="control-label">За запрос &nbsp <i class="fa fa-rub"></i></label>
                    </div>
                    <div class="col-md-6">
                        <input type="text" class="form-control" name="group_request" value="">
                    </div>
                </div>
            </p>
            <p>
                <div class="row">
                    <div class="col-md-6">
                        <label class="control-label">За overlay  &nbsp <i class="fa fa-rub"></i></label>
                    </div>
                    <div class="col-md-6">
                        <input type="text" class="form-control" name="group_overlay" value="">
                    </div>
                </div>
            </p>
            <p>
                <div class="row">
                    <div class="col-md-6">
                        <label class="control-label">За endcard  &nbsp <i class="fa fa-rub"></i></label>
                    </div>
                    <div class="col-md-6">
                        <input type="text" class="form-control" name="group_endcard" value="">
                    </div>
                </div>
            </p>
            <p>
                <div class="row">
                    <div class="col-md-6">
                        <label class="control-label">Просмотренно после %</label>
                    </div>
                    <div class="col-md-6">
                        <input type="text" class="form-control" name="group_ration" value="">
                    </div>
                </div>
            </p>
        </div>
        <input id="account_type_id" type="hidden" name="account_type" value="${g.tab}">
        <div class="modal-footer">
            <button type="button" data-dismiss="modal" class="btn btn-default">Отменить</button>
            <button type="submit" id="sample_editable_11_new" class="btn btn-success" value="Submit">Сохранить</button>
        </div>
    </form>
</div>

<script type="text/javascript">
    $(document).on("ready", function() {
            Index = function () { };
            %for tab in ('pub', 'adv', ):
                $("#tab_${tab}").on("click", function(e) {
                    Index.tab = "${tab}";
                    $("#account_type_id").val("${tab}");
                });
            %endfor
            Index.tab = "${g.tab}";
    });

    // Validate the form
    function validateForm() {
        var gr_name = document.forms['create_group_form']['group_name'].value;

        // Make a form object.
        var _form = {
            rate: document.forms['create_group_form']['group_rate'].value,
            request: document.forms['create_group_form']['group_request'].value,
            overlay: document.forms['create_group_form']['group_overlay'].value,
            endcard: document.forms['create_group_form']['group_endcard'].value,
            ration: document.forms['create_group_form']['group_ration'].value
        }

        if (gr_name == null || gr_name == "") {
            alert("The group name must not be empty!");
            return false;
        }
        for (var key in _form) {
            var attr = _form[key];
            if ( attr != "" && isNaN(Number(attr))) {
                alert("The group " + key + " must be digital!");
                return false;
            }
        }
        return true;
    }
</script>