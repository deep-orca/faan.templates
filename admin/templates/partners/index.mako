<%namespace name="bcrumb" file="/layout/bcrumb.mako" />
<%namespace name="fabutton" file="/layout/fa-button.mako" />
<%inherit file="/layout/main.mako" />

<%!
    from faan.core.model.security.account import Account
%>

<%def name="title()">Партнеры</%def>
<%def name="description()"></%def>

${ bcrumb.h(self) }

<%def name="show_select_item(select)">
    <div class="form-group">
        <label for="${select['name']}" class="control-label col-md-3">${select['header']}</label>
        <div class="col-md-3">
                <input name="${select['name']}" type="hidden" id="${select['id']}" style="width:324px"/>
            <span class="help-block">
                 ${select['help_string']}
            </span>
        </div>
    </div>
</%def>

<%def name="render_table(id)">
 <!--START TABLE-->
 <table class="table table-striped table-bordered table-hover" id="${id}">
 <thead>
 <tr>
     <th>ID</th>
     <th>Логин</th>
     <th>Статус</th>
     <th>Баланс</th>
     <th>Группа</th>
     <th>Действия</th>
 </tr>
 </thead>
 <tbody>

${caller.body()}

 </tbody>
 </table>
 <!--END TABLE-->
</%def>


<%def name="show_filters(selects)">
    %for select in selects:
        ${self.show_select_item(select)}
    %endfor
    <!-- ==== SUBMIT ==== --->
    <div class="form-group">
        <div class="col-sm-offset-2 col-sm-10">
            <a href="#" id="apply_changes" class="btn btn-success">Применить</a>
        </div>
    </div>
</%def>

<!-- BEGIN PAGE CONTENT-->
<div class="row">
<div class="col-md-12">
    <div class="portlet">														<!--BEGIN TABLE-->
        <div class="portlet-title">
            <div class="caption"><i class="fa fa-rocket"></i>
            	Партнеры
            </div>
        </div>

        <div class="portlet-body">
            <form method="GET" class="form-horizontal">
                ${self.show_filters(g.selects)}
			</form>
            <ul class="nav nav-pills">
                %if getattr(g, 'tab', '') and g.tab == 'new':
                <li class="active">
                %else:
                <li class="">
                %endif
                    <a href="#tab_2_1" data-toggle="tab" id="tab_new">Новые</a>
                </li>
                %if getattr(g, 'tab', '') and g.tab == 'all':
                <li class="active">
                %else:
                <li class="">
                %endif
                    <a href="#tab_2_2" data-toggle="tab" id="tab_all">Активные</a>
                </li>
                %if getattr(g, 'tab', '') and g.tab == 'banned':
                <li class="active">
                %else:
                <li class="">
                %endif
                    <a href="#tab_2_3" data-toggle="tab" id="tab_banned">Заблокированные</a>
                </li>
            </ul>
            <div class="tab-content">
                %if getattr(g, 'tab', '') and g.tab == 'new':
                <div class="tab-pane fade active in" id="tab_2_1">
                %else:
                <div class="tab-pane fade" id="tab_2_1">
                %endif
                    <p>
                    <%self:render_table id="stats_table_new">

                    </%self:render_table>
                    </p>
                    <div class="row">
                        <div class="col-md-6 col-sm-12"></div>
                        <div class="col-md-6 col-sm-12">
                            <div id="log_table_filter_banned" class="dataTables_filter">
                                <label>
                                &nbsp
                                    <ul class="pagination">
                                        <li class="prev disabled"><a title="Prev" href="#"><i class="fa fa-angle-left"></i></a></li>
                                        <li class="next"><a title="Next" href="#"><i class="fa fa-angle-right"></i></a></li>
                                    </ul>
                                </label>
                            </div>
                        </div>
                    </div>
                </div>
                %if getattr(g, 'tab', '') and g.tab == 'all':
                <div class="tab-pane fade active in" id="tab_2_2">
                %else:
                <div class="tab-pane fade" id="tab_2_2">
                %endif
                    <p>
                    <%self:render_table id="stats_table_all">

                    </%self:render_table>
                    </p>
                    <div class="row">
                        <div class="col-md-6 col-sm-12"></div>
                        <div class="col-md-6 col-sm-12">
                            <div id="log_table_filter_banned" class="dataTables_filter">
                                <label>
                                &nbsp
                                    <ul class="pagination">
                                        <li class="prev disabled"><a title="Prev" href="#"><i class="fa fa-angle-left"></i></a></li>
                                        <li class="next"><a title="Next" href="#"><i class="fa fa-angle-right"></i></a></li>
                                    </ul>
                                </label>
                            </div>
                        </div>
                    </div>
                </div>
                %if getattr(g, 'tab', '') and g.tab == 'banned':
                <div class="tab-pane fade active in" id="tab_2_3">
                %else:
                <div class="tab-pane fade" id="tab_2_3">
                %endif
                    <p>
                    <%self:render_table id="stats_table_banned">

                    </%self:render_table>
                    </p>
                    <div class="row">
                        <div class="col-md-6 col-sm-12"></div>
                        <div class="col-md-6 col-sm-12">
                            <div id="log_table_filter_banned" class="dataTables_filter">
                                <label>
                                &nbsp
                                    <ul class="pagination">
                                        <li class="prev disabled"><a title="Prev" href="#"><i class="fa fa-angle-left"></i></a></li>
                                        <li class="next"><a title="Next" href="#"><i class="fa fa-angle-right"></i></a></li>
                                    </ul>
                                </label>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
<!-- END PAGE CONTENT-->

<!--------------- STATS TABLE BEGIN -----------------!>

<!--------------- STATS TABLE END -----------------!>
<script type="text/javascript">
Index = function() {
    return {

        tab: null,

        // Some table attributes
        data: null,
        total: +0,

        // Pagination options
        pag_size: +50,
        pag_start: 0,

        init: function() {
            $("#apply_changes").on("click", function(e) {
                Index["loadData_" + Index.tab]()
            });

            %for tab in ('all', 'new', 'banned',):
            $("#tab_${tab}").on("click", function(e) {
                Index.tab = "${tab}";
                Index.pag_size = + 50;
                Index.pag_start = + 0;
                Index.loadData_${tab}();
            });
            %endfor

            %if getattr(g, 'tab', ''):
                this.tab = "${g.tab}";
            %else:
                this.tab = "new";
            %endif
        },

        initSelect2: function() {
            var self = this;
            var format = function(item) { return item.name };

            %for select in g.selects:
                <%
                    _name = 'account' if select['name'] in ('advertiser', 'publisher', ) else select['name']
                    try:
                        placeholder = getattr(g, str(_name) + '_name')
                    except StandardError:
                        placeholder = select['header']
                %>

                $("#${select['id']}").select2({
                    placeholder: "${placeholder}",
                    ajax: {
                        url: function() { return "/partners/ajax/partners/select?account_group=${g.a_group}" },
                        type: "GET",
                        results: function(data, page) {
                            for (var name, i = 0; i < data.objects.length; i++)
                                if (data.objects[i].id == null)
                                    data.objects[i].name = "Все";
                            return {results: data.objects, text: 'name' };
                         },
                        data: function(term, page) { return { q: term } }
                    },
                    formatSelection: format,
                    formatResult: format
                }).on("select2-selecting", function(e) {
                    select_list["${select['name']}"] = e.val;
                    <% flag = False %>
                    %for s in g.selects:
                        %if flag:
                            select_list["${s['name']}"] = '';
                            $("#${s['id']}").select2("data", {id: "", text: "Все"});
                        %endif
                        <%
                            if s['name'] == select['name']:
                                flag = True
                        %>
                    %endfor

                });
            %endfor

        },

        %for tab in ('all', 'new', 'banned',):
        loadData_${tab}: function(start, count, search) {
            try { $("#stats_table_${tab}").DataTable().destroy(); } catch (e) {}

            start = start ? start : 0;
            count = count ? count : Index.pag_size;
            search = search ? search : "";

            var url = "/partners/ajax/partners/table?tab=${tab}" + "&group=" + select_list.group + "&account_group=${g.a_group}" + "&start=" + start + "&count=" + count + "&search=" + search;

            c = [
               { data: 'id' },
               { data: 'name' },
               { data: 'state' },
               { data: 'balance' },
               { data: 'group' },
               { data: 'action' }
            ];

            $("#stats_table_${tab}").dataTable({
                processing: true,
                ajax: {
                    url: url,
                    dataSrc: function(response) {
                        Index.total = response.meta.total;
                        return response.data;
                    }
                },
                columns: c,
                lengthChange: true,
                "aLengthMenu": [
                    [25, 50, 100, -1],
                    [25, 50, 100, "All"] // change per page values here
                ],
                // set the initial value
                "iDisplayLength": 50,
                "sPaginationType": "bootstrap",
                "oLanguage": {
                    "sLengthMenu": "_MENU_ records",
                    "oPaginate": {
                        "sPrevious": "Prev",
                        "sNext": "Next"
                    }
                },
                "aoColumnDefs": [{
                        'bSortable': false,
                        'aTargets': [0]
                    }
                ],
                order: [],
                paging: false,
                bFilter: false,
                bInfo: false
            });

            // Add select items.
            $("#stats_table_${tab}_wrapper .row")[0].children[0].innerHTML = '<div class="dataTables_length" id="stats_table_${tab}_length"><label><select name="stats_table_${tab}_length" id="_stats_table_${tab}_length" aria-controls="stats_table_${tab}" class="form-control input-xsmall"><option value="50">50</option><option value="100">100</option><option value="150">150</option></select> records</label></div>'
            $('#stats_table_${tab}_wrapper .dataTables_length select').select2().on("select2-selecting", function(e) {
                Index.pag_size = + e.val;
                Index.pag_start = + 0;
                Index.loadData_${tab}(0, Index.pag_size);
                $('#stats_table_${tab}_wrapper .dataTables_length select').select2("val", e.val.toString());
            });

            //Add search item.
            $("#stats_table_${tab}_wrapper .row")[0].children[1].innerHTML = '<div id="stats_table_new_filter" class="dataTables_filter"><label>Search:<input type="search" class="form-control input-medium" aria-controls="stats_table_new"></label></div>'
            $('#stats_table_${tab}_wrapper .dataTables_filter input').on("change", function(){
                var text = $(this).val();
                Index.pag_start = + 0;
                Index.loadData_${tab}(Index.pag_start, Index.pag_size, text);
                $('#stats_table_${tab}_wrapper .dataTables_length select').select2("val", Index.pag_size);
                $('#stats_table_${tab}_wrapper .dataTables_filter input').val(text);
            });

            // Parsing parameters for the table.
            $("#stats_table_${tab}").on('order.dt',  function () {
                <% acc_gr = 'pub' if g.a_group == Account.Groups.PUB else 'adv' %>
                var account_group = "${acc_gr}";
                var rows = $("#stats_table_${tab}").dataTable().fnGetNodes();

                if (rows.length < Index.pag_size) {
                    $(".next").addClass("disabled");
                } else {
                    $(".next").removeClass("disabled");
                }

                for (var i = 0; i < rows.length; i++) {
                    // Login
                    rows[i].cells[1].innerHTML = '<a href="/profile/' + rows[i].cells[0].innerHTML + '">' + rows[i].cells[1].innerHTML +'</a>';
                    // Groups
                    try {
                        var gr = rows[i].cells[4].innerHTML;
                        gr = JSON.parse(gr);
                        rows[i].cells[4].innerHTML = '<a href="/partners/' + account_group + '/?tab=' + Index.tab + '&group=' + gr.id + '">' + gr.name + '</a>';
                    } catch (e) { }
                    // Actions
                    try {
                        var state = rows[i].cells[2].innerHTML;
                        state = JSON.parse(state);
                        if (state.id == ${Account.State.NEW}) {
                            var url = "/partners/control/{account}/${Account.State.ACTIVE}?back={group}&tab=${tab}"
                                .replace('{account}', rows[i].cells[0].innerHTML)
                                .replace('{group}', account_group);
                            rows[i].cells[5].innerHTML = '<a href="' + url + '" class="newclass"><i class="fa fa-check fabutton tooltips" data-placement="top" data-original-title="Активировать"></i></a>'
                            url = "/partners/control/{account}/${Account.State.BANNED}?back={group}&tab=${tab}"
                                .replace('{account}', rows[i].cells[0].innerHTML)
                                .replace('{group}', account_group);
                            rows[i].cells[5].innerHTML += '<a href="' + url + '" class="newclass"><i class="fa fa-times fabutton tooltips" data-placement="top" data-original-title="Заблокировать"></i></a>'
                        } else if (state.id == ${Account.State.ACTIVE}) {
                            var url = "/partners/control/{account}/${Account.State.BANNED}?back={group}&tab=${tab}"
                                .replace('{account}', rows[i].cells[0].innerHTML)
                                .replace('{group}', account_group);
                            rows[i].cells[5].innerHTML = '<a href="' + url + '" class="newclass"><i class="fa fa-times fabutton tooltips" data-placement="top" data-original-title="Заблокировать"></i></a>'
                        } else  {
                            var url = "/partners/control/{account}/${Account.State.ACTIVE}?back={group}&tab=${tab}"
                                .replace('{account}', rows[i].cells[0].innerHTML)
                                .replace('{group}', account_group);
                            rows[i].cells[5].innerHTML = '<a href="' + url + '" class="newclass"><i class="fa fa-check fabutton tooltips" data-placement="top" data-original-title="Активировать"></i></a>'
                        }
                    } catch(e) { }

                    // Status
                    try {
                        var stat = rows[i].cells[2].innerHTML;
                        stat = JSON.parse(stat);
                        var stat_class = "";
                        if (stat.id == "${Account.State.ACTIVE}"){
                            stat_class = "badge-success";
                        } else if (stat.id == "${Account.State.SUSPENDED}") {
                            stat_class = "badge-warning";
                        } else if (stat.id == "${Account.State.NEW}") {
                            stat_class = "badge-info";
                        } else {
                            stat_class = "badge-danger";
                        }
                        rows[i].cells[2].innerHTML = '<span class="badge {stat_class}">{stat_value}</span>'
                            .replace("{stat_value}", account_state[stat.id])
                            .replace("{stat_class}", stat_class);
                    } catch(e) { console.log("status error"); }

                    //Balance
                    try {
                        var balance = rows[i].cells[3].innerHTML;
                        balance = JSON.parse(balance);
                        var cost = +balance.balance;
                        rows[i].cells[3].innerHTML = cost.toFixed(2).toString().replace(/\B(?=(\d{3})+(?!\d))/g, " ") + " руб"
                    } catch(e) { }

                    // Add summary.
                    var start = +Index.pag_start + 1, end;
                    if (!Index.total)
                        start = end = 0;
                    else if ((+Index.pag_start + Index.pag_size) > +Index.total)
                        end = Index.total;
                    else
                        end = + Index.pag_start + Index.pag_size;
                    $("#stats_table_${tab}_wrapper .row")[1].children[0].innerHTML = 'С {start} по {end}, всего {total}'
                        .replace("{total}", Index.total)
                        .replace("{start}", start)
                        .replace("{end}", end);

                }
            });
        },
        %endfor

        fake: null
    }
}();
</script>

<script type="text/javascript">
    $(document).on("ready", function() {

        account_state = {
            "${Account.State.NEW}": 'Новый',
            "${Account.State.ACTIVE}": 'Активный',
            "${Account.State.BANNED}": 'Заблокированный',
            "${Account.State.SUSPENDED}": 'Приостановленный'
        };

        select_list =  {

            %for select in g.selects:
                <%
                    _name = 'account' if select['name'] in ('advertiser', 'publisher', ) else select['name']
                    try:
                        value = getattr(g, str(_name)) or ''
                    except StandardError:
                        value = ''
                %>
                ${select['name']}: "${value}",
            %endfor

            get_values: function() {
                s = "";
                %for i, select in enumerate(g.selects):
                    %if i != len(g.selects) - 1:
                        s += "${select['name']}:" + this["${select['name']}"] + ",";
                    %else:
                        s += "${select['name']}:" + this["${select['name']}"];
                    %endif
                %endfor
                return s;
            }
        };

        Index.init();
        Index.initSelect2();
        %if getattr(g, 'tab', ''):
            Index.loadData_${g.tab}();
        %else:
            Index.loadData_new();
        %endif

        // Pagination sctipt
        $(".prev").on("click", function() {
            if ($(".prev").attr("class").indexOf("disabled") > 0) {
               return;
            }

            Index.pag_start -= Number(Index.pag_size);
            if (+Index.pag_start < 0) {
               Index.pag_start = +0;
               $(".prev").addClass("disabled");
               return;
            }
            $(".next").removeClass("disabled");

            Index["loadData_" + Index.tab](Index.pag_start, Index.pag_size);

        });
        $(".next").on("click", function() {
            console.log("next!");
            if ($(".next").attr("class").indexOf("disabled") > 0) {
               return;
            }

            $(".prev").removeClass("disabled");

            Index.pag_start = Number(Index.pag_start) + Number(Index.pag_size);
            Index["loadData_" + Index.tab](Number(Index.pag_start), Number(Index.pag_size));
        });

    });
</script>