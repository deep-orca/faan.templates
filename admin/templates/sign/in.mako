<%inherit file="base.mako" />

<form class="login-form" method="post" action="/sign/in">
    <h3 class="form-title">Войдите в Ваш аккаунт</h3>

    %if form.errors.get("auth"):
        <div class="alert ${form.errors.get('auth')['class']}">
            <button class="close" data-close="alert"></button>
            <span>
                 ${form.errors.get('auth')['message']}
            </span>
        </div>
    %endif

    <div class="form-group">
        <!--ie8, ie9 does not support html5 placeholder, so we just show field title for that-->
        <label class="control-label visible-ie8 visible-ie9">Адрес e-mail</label>
        <div class="input-icon">
            <i class="fa fa-envelope"></i>
            <input class="form-control placeholder-no-fix" type="text" autocomplete="off" placeholder="Адрес e-mail" name="email"/>
        </div>
    </div>
    <div class="form-group">
        <label class="control-label visible-ie8 visible-ie9">Пароль</label>
        <div class="input-icon">
            <i class="fa fa-lock"></i>
            <input class="form-control placeholder-no-fix" type="password" autocomplete="off" placeholder="Пароль" name="password"/>
        </div>
    </div>
    %if g.attempts >= 3:
        <div class="form-group">
            <label class="control-label visible-ie8 visible-ie9">Captcha</label>
            ${form.recaptcha}
        </div>
    %endif
    <div class="form-actions" style="border-bottom: 0;">
        <label class="checkbox">
        <input type="checkbox" name="remember" value="1"/> Запомнить меня </label>
        <button type="submit" class="btn btn-info pull-right">
            Войти
        </button>
    </div>
    ${form.csrf_token()}
</form>

<%def name="custom_js()">
    <script type="text/javascript">
        $(document).on("ready", function() {
            App.init();
            Login.init();
        });
    </script>
</%def>