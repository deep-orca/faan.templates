<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en" class="no-js">
<!--<![endif]-->
    <head>
        <meta charset="utf-8"/>
        <META NAME="ROBOTS" CONTENT="NOINDEX, NOFOLLOW">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1.0" name="viewport"/>
        <meta content="" name="description"/>
        <meta content="" name="author"/>
        <meta name="MobileOptimized" content="320">
        <link href="/global/static/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
        <link href="/global/static/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
        <link href="/global/static/plugins/uniform/css/uniform.default.css" rel="stylesheet" type="text/css"/>
        <link rel="stylesheet" type="text/css" href="/global/static/plugins/select2/select2_conquer.css"/>
        <link href="/global/static/css/style-conquer.css" rel="stylesheet" type="text/css"/>
        <link href="/global/static/css/style.css" rel="stylesheet" type="text/css"/>
        <link href="/global/static/css/style-responsive.css" rel="stylesheet" type="text/css"/>
        <link href="/global/static/css/plugins.css" rel="stylesheet" type="text/css"/>
        <link href="/global/static/css/themes/default.css" rel="stylesheet" type="text/css" id="style_color"/>
        <link href="/global/static/css/pages/login.css" rel="stylesheet" type="text/css"/>
        <link href="/global/static/css/custom.css" rel="stylesheet" type="text/css"/>
        <link rel="shortcut icon" href="favicon.ico"/>

        <title>Vidiger | Вход на сайт</title>
    </head>
    <body class="login">
        <div class="logo">
         <img src="/global/static/img/logo_centered.png" alt=""/>
        </div>
        <div class="content">
            ${next.body()}
        </div>
        <div class="copyright">
          2014 &copy; Vidiger. Все права защищены
        </div>

        <!--[if lt IE 9]>
        <script src="/global/static/plugins/respond.min.js"></script>
        <script src="/global/static/plugins/excanvas.min.js"></script>
        <![endif]-->

        <script src="/global/static/plugins/jquery-1.10.2.min.js" type="text/javascript"></script>
        <script src="/global/static/plugins/jquery-migrate-1.2.1.min.js" type="text/javascript"></script>
        <script src="/global/static/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
        <script src="/global/static/plugins/bootstrap-hover-dropdown/twitter-bootstrap-hover-dropdown.min.js" type="text/javascript"></script>
        <script src="/global/static/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
        <script src="/global/static/plugins/jquery.blockui.min.js" type="text/javascript"></script>
        <script src="/global/static/plugins/jquery.cokie.min.js" type="text/javascript"></script>
        <script src="/global/static/plugins/uniform/jquery.uniform.min.js" type="text/javascript"></script>
        <script type="text/javascript" src="/global/static/plugins/bootstrap-toastr/toastr.min.js"></script>
        <script src="/global/static/plugins/jquery.validate.min.js" type="text/javascript"></script>
        <script type="text/javascript" src="/global/static/plugins/select2/select2.min.js"></script>
        <script type="text/javascript" src="/global/static/plugins/jquery-mask/jquery.mask.min.js"></script>
        <script src="/global/static/scripts/app.js" type="text/javascript"></script>
        <script src="/global/static/scripts/login.js" type="text/javascript"></script>

        ${ self.custom_js() }
    </body>
</html>