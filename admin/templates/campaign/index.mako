<%namespace name="bcrumb" file="/layout/bcrumb.mako" />
<%namespace name="fabutton" file="/layout/fa-button.mako" />
<%inherit file="/layout/main.mako" />

<%!
    from faan.core.model.adv.campaign import Campaign
    from faan.core.model.adv.media import Media
    from faan.admin.controllers.campaigns import Tabs
%>

<%

    for s in g.selects:
        if s.get('name') == 'account':
            s.update({'header': u'Аккаунт'})
        if s.get('name') == 'group':
            s.update({'header': u'Группа'})
%>

<%def name="title()">Кампании</%def>
<%def name="description()"></%def>

<script type="text/javascript" src="/static/scripts/filters.js"></script>

<%def name="page_css()">
<link href="/global/static/plugins/bootstrap-modal/css/bootstrap-modal-bs3patch.css" rel="stylesheet" type="text/css"/>
<link href="/global/static/plugins/bootstrap-modal/css/bootstrap-modal.css" rel="stylesheet" type="text/css"/>
</%def>


${ bcrumb.h(self) }

<%def name="render_table(id)">
 <!--START TABLE-->
 <table class="table table-striped table-bordered table-hover" id="${id}">
 <thead>
 <tr>
     <th class="table-checkbox">
         <input type="checkbox" class="group-checkable" data-set="#${id} .checkboxes"/>
     </th>
     <th>Кампания</th>
     <th class="text-center" >Статус</th>
     <th class="text-center" >Креативы</th>
     <th>Тип Оплаты</th>
     <th>Ставка</th>
     <th>Устройства</th>
     <th>Действия</th>
 </tr>
 </thead>
 <tbody>

${caller.body()}

 </tbody>
 </table>
 <!--END TABLE-->
</%def>


<%def name="show_select_item(select)">
    <div class="form-group">
        <label for="${select['name']}" class="control-label col-md-3">${unicode(select['header'])}</label>
        <div class="col-md-3">
                <input name="${select['name']}" type="hidden" id="${select['id']}" style="width:324px"/>
        </div>
    </div>
</%def>

<%def name="show_filters(selects)">
    %for select in selects:
        ${self.show_select_item(select)}
    %endfor
    <!-- ==== SUBMIT ==== --->
    <div class="form-group">
    <label class="control-label col-md-3"></label>
        <div class="col-sm-1">
            <a href="#" id="apply_changes" class="btn btn-success">Применить</a>
        </div>
    </div>
</%def>

<!-- BEGIN PAGE CONTENT-->
<div class="row">
<div class="col-md-12">
    <div class="portlet">
        <div class="portlet-title">

        <div class="caption"><i class="fa fa-rocket"></i>Кампании</div>
            <div class="actions">
                <div class="btn-group">
                    <a class="btn btn-info" href="#" data-toggle="dropdown">
                        <i class="fa fa-cogs"></i> Действие <i class="fa fa-angle-down"></i>
                    </a>
                    <ul class="dropdown-menu pull-right">
                        <li><a href="#group_action_approve" data-toggle="modal" id="disable" class="group_actions" ><i class="fa fa-times"></i> Заблокировать</a></li>
                        <li><a href="#group_action_approve" data-toggle="modal" id="active" class="group_actions" ><i class="fa fa-play"></i> Активировать</a></li>
                        <li><a href="#group_action_approve" data-toggle="modal" id="delete" class="group_actions" ><i class="fa fa-trash-o"></i> Удалить</a></li>
                    </ul>
                </div>
            </div>
        </div>

        <div class="portlet-body">

                <form method="GET" class="form-horizontal">
                    ${self.show_filters(g.selects)}
			    </form>

                <ul class="nav nav-pills">
                    %if getattr(g, 'tab', '') and g.tab == Tabs.ACTIVE:
                    <li class="active">
                    %else:
                    <li class="">
                    %endif
                        <a href="#tab_2_2" data-toggle="tab" id="tab_${Tabs.ACTIVE}">Активные</a>
                    </li>
                    %if getattr(g, 'tab', '') and g.tab == Tabs.BANNED:
                    <li class="active">
                    %else:
                    <li class="">
                    %endif
                        <a href="#tab_2_3" data-toggle="tab" id="tab_${Tabs.BANNED}">Приостановленные</a>
                    </li>
                </ul>
                <div class="tab-content">
                    %if getattr(g, 'tab', '') and g.tab == Tabs.ACTIVE:
                    <div class="tab-pane fade active in" id="tab_2_2">
                    %else:
                    <div class="tab-pane fade" id="tab_2_2">
                    %endif
                        <p>
                        <%self:render_table id="stats_table_${str(Tabs.ACTIVE)}">

                        </%self:render_table>
                        </p>
                        <div class="row">
                            <div class="col-md-6 col-sm-12"></div>
                            <div class="col-md-6 col-sm-12">
                                <div id="log_table_filter_${Tabs.ACTIVE}" class="dataTables_filter">
                                    <label>
                                    &nbsp
                                        <ul class="pagination">
                                            <li class="prev disabled"><a title="Prev" href="#"><i class="fa fa-angle-left"></i></a></li>
                                            <li class="next"><a title="Next" href="#"><i class="fa fa-angle-right"></i></a></li>
                                        </ul>
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>
                    %if getattr(g, 'tab', '') and g.tab == Tabs.BANNED:
                    <div class="tab-pane fade active in" id="tab_2_3">
                    %else:
                    <div class="tab-pane fade" id="tab_2_3">
                    %endif
                        <p>
                        <%self:render_table id="stats_table_${str(Tabs.BANNED)}">

                        </%self:render_table>
                        </p>
                        <div class="row">
                            <div class="col-md-6 col-sm-12"></div>
                            <div class="col-md-6 col-sm-12">
                                <div id="log_table_filter_${Tabs.BANNED}" class="dataTables_filter">
                                    <label>
                                    &nbsp
                                        <ul class="pagination">
                                            <li class="prev disabled"><a title="Prev" href="#"><i class="fa fa-angle-left"></i></a></li>
                                            <li class="next"><a title="Next" href="#"><i class="fa fa-angle-right"></i></a></li>
                                        </ul>
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
        </div>
    </div>
</div>
</div>
<!-- END PAGE CONTENT-->

<!-- change_account_status -->
<div id="group_action_approve" class="modal fade" tabindex="-1" data-width="760" style="top:10%">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
        <h4 class="modal-title"><span id="approve_header"></span></h4>
    </div>
    <form method="POST" class="form-horizontal">
        <div class="modal-body">
            <div class="row">
                <div class="col-md-10">
                    <p>
                        Вы действительно хотите <span id='approve_chose_action'></span> кампании <b><span id="approve_elements_list"></span></b> ?
                    </p>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" data-dismiss="modal" class="btn btn-default">Отменить</button>
            <a href="#" data-dismiss="modal" id="button_group_action_approve" class="btn btn-warning">Подтвердить</a>
        </div>
    </form>
</div>


<!-- SCRIPTS INDEX -->
<script type="text/javascript">
Index = function() {
    return {

        // Some table attributes
        data: null,
        total: +0,

        // Pagination options
        pag_size: +50,
        pag_start: 0,

        tab: null,
        mID: null,
        fmtype: null,

        init: function() {
            $("#apply_changes").on("click", function(e) {
                Index["loadData_" + Index.tab]()
            });

            %for tab in Tabs.tabs():
            $("#tab_${tab}").on("click", function(e) {
                Index.tab = "${tab}";
                Index.pag_size = + 50;
                Index.pag_start = + 0;
                Index.loadData_${tab}();
            });
            %endfor

            %if getattr(g, 'tab', ''):
                this.tab = "${g.tab}";
            %else:
                this.tab = "${Tabs.ACTIVE}";
            %endif
        },

        %for tab in Tabs.tabs():
        loadData_${tab}: function(start, count, search) {
            try { $("#stats_table_${tab}").DataTable().destroy(); } catch (e) { }

            var parameters = {
                start: start ? start : 0,
                count: count ? count : Index.pag_size,
                search: search ? search : ""
            }

            var url = "/campaign/ajax/table?tab=${tab}" + "&values=" + filterBox.getValues();

            for (var p in parameters)
                url += "&" + p + "=" + parameters[p];

            var c = [
                { data: 'checkbox', render: function(data) {
                    return '<input type="checkbox" class="checkboxes" value="{cam_id}"/>'.replace('{cam_id}', data);
                }},
                { data: 'campaign', render: function(data) {
                    var cam = JSON.parse(data);
                    return ' \
                    <h3 style="margin-top:0"> \
                        <span class="badge badge-info">#{cam_id}</span> \
                        <a href="/media/list/?tab={ctab}&campaign={cam_id}">{cam_name}</a> \
                    </h3> \
                    <p class="text-info"> \
                        <i class="fa fa-user"></i> <a href="/campaign/?tab={ctab}&account={a_id}">{a_name}</a> \
                    </p> \
                    <p class="text-info"> \
                        <i class="fa fa-group"></i> <a href="/campaign/?tab={ctab}&group={g_id}">{g_name}</a> \
                    </p>'
                    .replace(/{cam_id}/g, cam.id)
                    .replace(/{cam_name}/g, cam.name)
                    .replace("{a_id}", cam.a_id)
                    .replace("{a_name}", cam.a_name)
                    .replace("{g_id}", cam.g_id)
                    .replace("{g_name}", cam.g_name)
                    .replace(/{ctab}/g, Index.tab);
                }},
                { data: 'state', render: function(data){
                    var stat = data;
                    var stat_class = "";
                    if (stat == "${Campaign.State.ACTIVE}"){
                        stat_class = "badge-success";
                    } else if (stat == "${Campaign.State.DISABLED}") {
                        stat_class = "badge-warning";
                    } else if (stat == "${Campaign.State.NONE}") {
                        stat_class = "badge-info";
                    } else {
                        stat_class = "badge-danger";
                    }
                    return '<span class="badge {stat_class}">{stat_value}</span>'
                        .replace("{stat_value}", cam_state[stat])
                        .replace("{stat_class}", stat_class);
                }},
                { data: 'medias', render: function(data){
                    var mediasDataFull = "", mediasDataShort = "", statePrefix;
                    var medias = JSON.parse(data);
                    try{ uid++; } catch(e) { uid = 0; funcArr = []; }

                    for (var i = 0, stat_class; i < medias.length; i++) {
                        switch(+medias[i].state) {
                            case ${Media.State.NONE}:
                                stat_class = "badge-info";
                                break;
                            case ${Media.State.ACTIVE}:
                                stat_class = "badge-success";
                                break;
                            case ${Media.State.DISABLED}:
                                stat_class = "badge-warning";
                                break;
                            case ${Media.State.SUSPENDED}:
                                stat_class = "badge-danger";
                                break;
                        }
                        statePrefix = '<span class="badge {stat_class}">{stat_value}</span>'
                            .replace("{stat_value}", " &nbsp ")
                            .replace("{stat_class}", stat_class);
                        mediasDataFull += statePrefix + ' &nbsp ' + '<a href="/media/'+ medias[i].id +'">' + medias[i].name + '</a>' + '<br>';
                        if (i < 4) {
                            mediasDataShort += statePrefix + ' &nbsp ' + '<a href="/media/'+ medias[i].id +'">' + medias[i].name + '</a>' + '<br>';
                        } else if (i == 4)
                            mediasDataShort += '<a onClick="funcArr['+uid+'](this, true); return false;" >Подробнее...</a>';
                    }

                    if (i > 3)
                        mediasDataFull += '<a onClick="funcArr['+uid+'](this, false); return false;" >Скрыть...</a>';

                    funcArr[uid] = function(obj, show) {
                        if (show)
                            obj.parentElement.innerHTML = mediasDataFull;
                        else
                            obj.parentElement.innerHTML = mediasDataShort;
                    }
                    return mediasDataShort;
                }},
                { data: 'payment_type', render: function(data){
                    var paymentType = "";
                    switch(data) {
                        case ${Campaign.PaymentType.CPM}:
                            paymentType = 'CPM';
                            break;
                        case ${Campaign.PaymentType.CPC}:
                            paymentType = 'CPC';
                            break;
                        case ${Campaign.PaymentType.CPA}:
                            paymentType = 'CPA';
                            break;
                        default:
                            paymentType = 'Unknown';
                            break;
                    }
                    return paymentType;
                }},
                { data: 'bid', render: function(data){
                    return data + '&nbsp <i class="fa fa-rub"></i>'
                }},
                { data: 'devices', render: function(data) {
                    var devicesData = "";
                    var devices = JSON.parse(data);
                    for (var i = 0; i < devices.length; i++) {
                        switch(+devices[i]) {
                            case ${Campaign.DevClass.PHONE}:
                                devicesData += '<i data-original-title="Смартфоны" data-placement="top" class="fa fa-mobile-phone listfaicon tooltips text-info"></i>';
                                break;
                            case ${Campaign.DevClass.TABLET}:
                                devicesData += '&nbsp <i data-original-title="Планшеты" data-placement="top" class="fa fa-tablet listfaicon tooltips text-info"></i>';
                                break;
                        }
                    }
                    return devicesData;
                }},
                { data: 'actions', render: function(data) {
                    var action_data = "";
                    var action = JSON.parse(data);
                    var id = action.id ,state = action.state
                    var url_edit = "/campaign/{cam_id}".replace('{cam_id}', id);
                    var url_stat = "/stats/adv/cam/?campaign={cam_id}".replace('{cam_id}', id);
                    if (state == ${Campaign.State.NONE}) {
                        action_data += '&nbsp <a onClick="entry_action(event, {action: \'active\', id: \'{cam_id}\'})" ><i class="fa fa-check fabutton tooltips" data-placement="top" data-original-title="Активировать"></i></a>'.replace("{cam_id}", id);
                        action_data += '&nbsp <a onClick="entry_action(event, {action: \'disable\', id: \'{cam_id}\'})" ><i class="fa fa-times fabutton tooltips" data-placement="top" data-original-title="Заблокировать"></i></a>'.replace("{cam_id}", id);
                        action_data += '&nbsp <a href="' + url_edit + '" class="newclass"><i class="fa fa-pencil fabutton tooltips" data-placement="top" data-original-title="Редактировать"></i></a>';
                        action_data += '&nbsp <a href="' + url_stat + '" class="newclass"><i class="fa fa-bar-chart-o fabutton tooltips" data-placement="top" data-original-title="Статистика"></i></a>';
                    } else if (state == ${Campaign.State.ACTIVE}) {
                        action_data += '&nbsp <a onClick="entry_action(event, {action: \'disable\', id: \'{cam_id}\'})" ><i class="fa fa-pause fabutton tooltips" data-placement="top" data-original-title="Приостановить"></i></a>'.replace("{cam_id}", id);
                        action_data += '&nbsp <a href="' + url_edit + '" class="newclass"><i class="fa fa-pencil fabutton tooltips" data-placement="top" data-original-title="Редактировать"></i></a>';
                        action_data += '&nbsp <a href="' + url_stat + '" class="newclass"><i class="fa fa-bar-chart-o fabutton tooltips" data-placement="top" data-original-title="Статистика"></i></a>';
                    } else if (state == ${Campaign.State.DISABLED}) {
                        action_data += '&nbsp <a onClick="entry_action(event, {action: \'active\', id: \'{cam_id}\'})" ><i class="fa fa-check fabutton tooltips" data-placement="top" data-original-title="Активировать"></i></a>'.replace("{cam_id}", id);
                        action_data += '&nbsp <a href="' + url_edit + '" class="newclass"><i class="fa fa-pencil fabutton tooltips" data-placement="top" data-original-title="Редактировать"></i></a>';
                        action_data += '&nbsp <a href="' + url_stat + '" class="newclass"><i class="fa fa-bar-chart-o fabutton tooltips" data-placement="top" data-original-title="Статистика"></i></a>';
                    } else  {
                        action_data += '&nbsp <a onClick="entry_action(event, {action: \'active\', id: \'{cam_id}\'})" ><i class="fa fa-check fabutton tooltips" data-placement="top" data-original-title="Активировать"></i></a>'.replace("{cam_id}", id);
                    }
                    return action_data;
                }}
            ];

            $("#stats_table_${tab}").dataTable({
                processing: true,
                ajax: {
                    url: url,
                    dataSrc: function(response) {
                        Index.total = response.meta.total;
                        return response.data;
                    }
                },
                columns: c,
                lengthChange: true,
                "aLengthMenu": [
                    [25, 50, 100, -1],
                    [25, 50, 100, "All"] // change per page values here
                ],
                // set the initial value
                "iDisplayLength": 50,
                "sPaginationType": "bootstrap",
                "oLanguage": {
                    "sLengthMenu": "_MENU_ records",
                    "oPaginate": {
                        "sPrevious": "Prev",
                        "sNext": "Next"
                    }
                },
                "aoColumnDefs": [{
                        'bSortable': false,
                        'aTargets': [0]
                    }
                ],
                order: [],
                paging: false,
                bFilter: false,
                bInfo: false
            });

            $("#stats_table_${tab} .group-checkable").change(function () {
                var set = jQuery(this).attr("data-set");
                var checked = jQuery(this).is(":checked");
                jQuery(set).each(function () {
                    if (checked) {
                        $(this).attr("checked", true);
                        $(this).parents('tr').addClass("active");
                        $(this).parents('span').addClass("checked");
                    } else {
                        $(this).attr("checked", false);
                        $(this).parents('tr').removeClass("active");
                        $(this).parents('span').removeClass("checked");
                    }
                });
                jQuery.uniform.update(set);

            });

            // Add select items.
            $("#stats_table_${tab}_wrapper .row")[0].children[0].innerHTML = '<div class="dataTables_length" id="stats_table_${tab}_length"><label><select name="stats_table_${tab}_length" id="_stats_table_${tab}_length" aria-controls="stats_table_${tab}" class="form-control input-xsmall"><option value="50">50</option><option value="100">100</option><option value="150">150</option></select> records</label></div>'
            $('#stats_table_${tab}_wrapper .dataTables_length select').select2().on("select2-selecting", function(e) {
                Index.pag_size = + e.val;
                Index.pag_start = + 0;
                Index.loadData_${tab}(0, Index.pag_size);
                $('#stats_table_${tab}_wrapper .dataTables_length select').select2("val", e.val.toString());
            });

            //Add search item.
            $("#stats_table_${tab}_wrapper .row")[0].children[1].innerHTML = '<div id="stats_table_${Tabs.ACTIVE}_filter" class="dataTables_filter"><label>Search:<input type="search" class="form-control input-medium" aria-controls="stats_table_${Tabs.ACTIVE}"></label></div>'
            $('#stats_table_${tab}_wrapper .dataTables_filter input').on("change", function(){
                var text = $(this).val();
                Index.pag_start = + 0;
                Index.loadData_${tab}(Index.pag_start, Index.pag_size, text);
                $('#stats_table_${tab}_wrapper .dataTables_length select').select2("val", Index.pag_size);
                $('#stats_table_${tab}_wrapper .dataTables_filter input').val(text);
            });

            // Parsing parameters for the table.
            $("#stats_table_${tab}").on('order.dt',  function () {

                // Array of the table rows.
                var rows = $("#stats_table_${tab}").dataTable().fnGetNodes();

                if (rows.length < Index.pag_size)
                    $(".next").addClass("disabled");
                else
                    $(".next").removeClass("disabled");

                // Add summary to the table.
                var start = +Index.pag_start + 1, end;
                if (!Index.total)
                    start = end = 0;
                else if ((+Index.pag_start + Index.pag_size) > +Index.total)
                    end = Index.total;
                else
                    end = + Index.pag_start + Index.pag_size;
                $("#stats_table_${tab}_wrapper .row")[1].children[0].innerHTML = 'С {start} по {end}, всего {total}'
                    .replace("{total}", Index.total)
                    .replace("{start}", start)
                    .replace("{end}", end);
            });
        },
        %endfor

        // Reload the table. The pagination settings are dropped.
        reload_table: function(){
            Index["loadData_" + Index.tab]();
        }
    }
}();
</script>
<!-- SCRIPTS INDEX END-->


<script type="text/javascript">
    $(document).on("ready", function() {

        cam_state = {
            "${Campaign.State.NONE}": 'Новый',
            "${Campaign.State.ACTIVE}": 'Активный',
            "${Campaign.State.DISABLED}": 'Заблокированный'
        }

        // Table pagination.
        $(".prev").on("click", function() {
            if ($(".prev").attr("class").indexOf("disabled") > 0)
               return;

            Index.pag_start -= Number(Index.pag_size);
            if (+Index.pag_start < 0) {
               Index.pag_start = +0;
               $(".prev").addClass("disabled");
               return;
            }
            $(".next").removeClass("disabled");

            Index["loadData_" + Index.tab](Index.pag_start, Index.pag_size);

        });
        $(".next").on("click", function() {
            if ($(".next").attr("class").indexOf("disabled") > 0)
               return;

            $(".prev").removeClass("disabled");

            Index.pag_start = Number(Index.pag_start) + Number(Index.pag_size);
            Index["loadData_" + Index.tab](Number(Index.pag_start), Number(Index.pag_size));
        });

        Index.init();
        // Init filters.
        %for select in g.selects:
            <%
                _name = 'account' if select['name'] in ('advertiser', 'publisher', ) else select['name']
                placeholder = getattr(g, str(_name) + '_name', select['header'])
            %>
            filterBox.push({
                id: "${select['id']}",
                name: "${select['name']}",
                header: "${select['header']}",
                placeholder: "${placeholder}",
                value: "${getattr(g, str(select['name']), '')}",
                url: "/campaign/ajax/select"
            });
        %endfor
        %if getattr(g, 'tab', ''):
            Index.loadData_${g.tab}();
        %else:
            Index.loadData_${Tabs.ACTIVE}();
        %endif

    });
</script>


<!--GROUP ACTIONS-->

<script type="text/javascript">

    action_assistant = {
        objects: null,
        objects_ids: null,
        action: null
    };

    $('.group_actions').on('click', function(){
        var objects = $('.checkboxes:checked');
        var objects_ids = '';
        if (objects.length > 0){
            for (var i = 0; i < objects.length; i++){
                if (i == objects.length - 1){
                    objects_ids += objects[i].value;
                    continue;
                }
                objects_ids += objects[i].value + ',';
            }

            var group_checkbox = $("#stats_table_{tab} .group-checkable".replace("{tab}", Index.tab));
            group_checkbox[0].parentElement.className = "";

            action_assistant.action = this.id
            action_assistant.objects = objects
            action_assistant.objects_ids = objects_ids

            switch(this.id) {
                case 'active':
                    $('#approve_chose_action').html("активировать");
                    $('#approve_header').html("Активация кампаний");
                    break;
                case 'disable':
                    $('#approve_chose_action').html("заблокировать");
                    $('#approve_header').html("Блокировка кампаний");
                    break;
                case 'delete':
                    $('#approve_chose_action').html("удалить");
                    $('#approve_header').html("Удаление кампаний");
                    break;
            }

            $('#approve_elements_list').html("[" + objects_ids + "]");
        }
    });

    $("#button_group_action_approve").on('click', function(){
        $.post('/campaign/group_actions', {objects_list: action_assistant.objects_ids, action: action_assistant.action}, after_load);

        objects = action_assistant.objects

        for (var i = 0; i < objects.length; i++){
            tr = objects[i].parentElement.parentElement;
            tr.className = tr.className.replace("active", "");
            objects[i].parentElement.className = "";
            objects[i].checked = false;
            remove_tr(tr);
        }
    });

    function entry_action(event, action_assistant) {
        var tr = event.target.parentElement.parentElement.parentElement;
        var action = action_assistant.action;
        var objects_ids = action_assistant.id;
        var previous_state = "" + action_assistant.state;

        $.post('/campaign/group_actions', {objects_list: objects_ids, action: action}, after_load);

        tr.className = tr.className.replace("active", "");
        remove_tr(tr);
    }

    // That's used to reload the page if there are no rows in the table.
    function remove_tr(tr) {
        var reload = true;
        var trs = tr.parentElement.children;
        tr.innerHTML = "";

        for (var i = 0; i < trs.length; i++) {
            if (trs[i].innerHTML != "") {
                reload = false;
                break;
            }
        }
        if (reload)
            window.location.href = '/campaign/?tab=' + Index.tab;
    }

    function after_load(data) { }
</script>