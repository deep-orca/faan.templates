## -*- coding: utf-8 -*-
<%namespace name="bcrumb" file="/layout/bcrumb.mako" />
<%inherit file="/layout/main.mako" />

<%def name="title()">
    % if g.campaign.id:
        Кампания ${g.campaign.name} [${g.campaign.id}]
    % else:
        Новая кампания
    % endif
</%def>

<%def name="description()">Форма редактирования</%def>

<%def name="page_js()">
    <script type="text/javascript" src="/global/static/plugins/jquery.validate.min.js"></script>
    <script type="text/javascript" src="/global/static/plugins/jquery-validation/localization/messages_ru.js"></script>
    <script type="text/javascript"
            src="/global/static/plugins/bootstrap-switch/static/js/bootstrap-switch.min.js"></script>
    <script type="text/javascript"
            src="/global/static/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
    <script type="text/javascript"
            src="/global/static/plugins/bootstrap-datepicker/js/locales/bootstrap-datepicker.ru.js"></script>
    <script type="text/javascript" src="/global/static/plugins/fancytree/jquery.fancytree.min.js"></script>

</%def>

<%def name="page_css()">
    <link rel="stylesheet" type="text/css"
          href="/global/static/plugins/bootstrap-daterangepicker/daterangepicker-bs3.css"/>
    <link rel="stylesheet" type="text/css" href="/global/static/plugins/bootstrap-datepicker/css/datepicker.css"/>
    <link rel="stylesheet" type="text/css"
          href="/global/static/plugins/bootstrap-switch/static/stylesheets/bootstrap-switch-conquer.css"/>
    <link rel="stylesheet" type="text/css" href="/global/static/plugins/fancytree/skin-lion/ui.fancytree.min.css"/>

    <style>
        #daterange input {
            cursor: pointer !important;
        }

        .xform-error {
            padding: 3px;
            font-weight: bold !important;
        }
    </style>
</%def>

${ bcrumb.h(self) }
${ bcrumb.bc([ { 'name' : u'Кампании',  'href' : '/campaign/', 'icon' : 'bullhorn' }
, { 'name' : u"%s<sup>[%s]</sup>" % (g.campaign.name, g.campaign.id) if g.campaign.id else u'Новая кампания', 'href': '/campaign/%s/media/' % (g.campaign.id) if g.campaign.id else "javascript:void(0);" }
]) }

<%

    from faan.core.model.adv.campaign import Campaign
    from faan.core.model.pub.application import Application

    platform_dict = { u'iOS'      : Application.Os.IOS
                , u'Android'  : Application.Os.ANDROID
                , u'WinPhone' : Application.Os.WINPHONE
                }

    devclass_dict = { u'Телефон'  : Campaign.DevClass.PHONE
                , u'Планшет'  : Campaign.DevClass.TABLET
                }


%>

<!-- BEGIN PAGE CONTENT-->

<div class="row">
    <form id="campaign_form" method="POST" class="form-horizontal">
        ${form.csrf_token}
        <div class="col-md-9">
            <div class="row">
                ## Name + category
                ## START
                <div class="col-md-12">
                    <div class="portlet">
                        <div class="portlet-title">
                            <div class="caption">Общие настройки</div>
                        </div>
                        <div class="portlet-body">
                            <div class="form-group ${'has-error' if form.name.errors else ''}">
                                <label for="campaignName" class="col-sm-2 control-label">Имя</label>

                                <div class="col-sm-10">
                                    <input name="name" type="text" class="form-control" id="campaignName"
                                           placeholder="Имя"
                                           value="${form.data.get('name')}" required/>

                                    <div class="xform-error text-danger text-center">${"<br>".join(form.name.errors)}</div>
                                </div>
                            </div>

                            <div class="form-group ${'has-error' if form.category.errors else ''}">
                                <label for="camp-category" class="col-sm-2 control-label">Категория</label>

                                <div class="col-sm-10">
                                    <select name="category" id="camp-category" class="form-control" required></select>

                                    <div class="xform-error text-danger text-center">${"<br>".join(form.category.errors)}</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                ## END
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="portlet">
                        <div class="portlet-title">
                            <div class="caption">Таргетинг: Категории</div>
                            <div class="make-switch switch-mini pull-right" data-on="success" data-off="danger">

                                <input type="checkbox" class="toggle" id="category_targeting" name="targeting"
                                       value="${Campaign.Targeting.CATEGORY}"
                                    ${'checked' if Campaign.Targeting.CATEGORY & g.campaign.targeting or Campaign.Targeting.CATEGORY in (form.targeting.data or []) else ''}
                                        />
                            </div>
                        </div>
                        <div class="portlet-body">
                            <div class="form-group"
                                 id="camp-target-categories">

                                <label class="col-md-12 text-center">
                                    Категории приложений для показа рекламы
                                </label>

                                <div class="xform-error text-danger text-center">${"<br>".join(form.target_categories_groups.errors)}</div>
                            </div>
                            <div class="btn-group-vertical" style="width: 100%;">
                                <button type="button" class="btn btn-default check_all"
                                        style="font-size:12px;">Выделить
                                    все
                                </button>
                                <button type="button" class="btn btn-default uncheck_all"
                                        style="font-size:12px;">Снять
                                    выделение
                                </button>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-md-6">
                    <div class="portlet">
                        <div class="portlet-title">
                            <div class="caption">Таргетинг: Возраст</div>
                        </div>
                        <div class="portlet-body">
                            <div class="form-group ${'has-error' if form.target_content_rating.errors else ''}"
                                 id="camp-target-content-rating">

                                <label class="col-md-12 text-center">
                                    Возрастные ограничения приложений
                                </label>

                                <ul class="col-md-12 text-left list-unstyled">
                                    <li>
                                        <label>
                                            <input type="checkbox" class="toggle checkbox-uniform"
                                                   id="app_age_targeting"
                                                   name="target_content_rating"
                                                   value="1" ${'checked' if 1 in form.data.get('target_content_rating') else ''} />
                                            Младшая
                                        </label>
                                    </li>
                                    <li>
                                        <label>
                                            <input type="checkbox" class="toggle checkbox-uniform"
                                                   id="app_age_targeting"
                                                   name="target_content_rating"
                                                   value="2" ${'checked' if 2 in form.data.get('target_content_rating') else ''}/>
                                            Средняя
                                        </label>
                                    </li>
                                    <li>
                                        <label>
                                            <input type="checkbox" class="toggle checkbox-uniform"
                                                   id="app_age_targeting"
                                                   name="target_content_rating"
                                                   value="3" ${'checked' if 3 in form.data.get('target_content_rating') else ''}/>
                                            Старшая
                                        </label>
                                    </li>
                                    <li>
                                        <label>
                                            <input type="checkbox" class="toggle checkbox-uniform"
                                                   id="app_age_targeting"
                                                   name="target_content_rating"
                                                   value="0" ${'checked' if 0 in form.data.get('target_content_rating') else ''}/>
                                            Без ограничений
                                        </label>
                                    </li>
                                </ul>

                                <div class="xform-error text-danger text-center">${"<br>".join(form.target_content_rating.errors)}</div>
                            </div>
                            <div class="btn-group-vertical" style="width: 100%;">
                                <button type="button" class="btn btn-default check_all"
                                        style="font-size:12px;">Выделить
                                    все
                                </button>
                                <button type="button" class="btn btn-default uncheck_all"
                                        style="font-size:12px;">Снять
                                    выделение
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="portlet">                                               <!------------------ PLATFORMS TARGETING  -->
                        <div class="portlet-title">
                            <div class="caption">Таргетинг: Платформы</div>
                            <div class="make-switch switch-mini pull-right" data-on="success" data-off="danger">
                                <input type="checkbox" name="targeting" class="toggle" id="platforms_targeting"
                                       value="${Campaign.Targeting.PLATFORM}"
                                    ${'checked' if Campaign.Targeting.PLATFORM & g.campaign.targeting or Campaign.Targeting.PLATFORM in (form.targeting.data or []) else ''}
                                        />
                            </div>
                        </div>
                        <div class="portlet-body">
                            <div class="form-group ${'has-error' if form.target_platforms.errors else ''}">
                                <!-- CAMPAIGN.PLATFORMS -->
##                                <label class="col-sm-4 control-label">Разрешенные платформы</label>

                                <div class="col-sm-12">
                                    % for (pk, pv) in platform_dict.items():
                                        <div class="checkbox"><label>
                                            <input name="target_platforms" type="checkbox"
                                                   value="${pv}" ${'checked' if pv in form.data.get('target_platforms', []) else ''}>
                                            ${pk}
                                        </label></div>
                                    % endfor
                                    <div class="xform-error text-danger text-center">${"<br>".join(form.target_platforms.errors)}</div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="portlet">
                        <div class="portlet-title">
                            <div class="caption">Таргетинг: Типы устроиств</div>
                            <div class="make-switch switch-mini pull-right" data-on="success" data-off="danger">
                                <input type="checkbox" class="toggle" id="devclass_targeting" name="targeting"
                                       value="${Campaign.Targeting.DEVCLASS}"
                                    ${'checked' if Campaign.Targeting.DEVCLASS & g.campaign.targeting or Campaign.Targeting.DEVCLASS in (form.targeting.data or []) else ''}
                                        />
                            </div>
                        </div>
                        <div class="portlet-body">

                            <div class="form-group ${'has-error' if form.target_devclasses.errors else ''}">
                                <!-- CAMPAIGN.DEV_CLASSES -->
##                                <label class="col-sm-4 control-label">Разрешенные типы устройств</label>

                                <div class="col-sm-12">
                                    % for (dck, dcv) in devclass_dict.items():
                                        <div class="checkbox"><label>
                                            <input name="target_devclasses" type="checkbox"
                                                   value="${dcv}" ${'checked' if dcv in form.data.get('target_devclasses', []) else ''}>
                                            ${dck}
                                        </label></div>
                                    % endfor
                                    <div class="xform-error text-danger text-center">${"<br>".join(form.target_devclasses.errors)}</div>
                                </div>
                            </div>


                        </div>
                    </div>
                </div>
            </div>




            <div class="row">
                <div class="col-md-6">
                    <div class="portlet">
                        <div class="portlet-title">
                            <div class="caption">
                                Таргетинг: Пол
                            </div>
                        </div>
                        <div class="portlet-body">
                            ${form.target_gender(class_='form-control')}
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="portlet">
                        <div class="portlet-title">
                            <div class="caption">
                                Таргетинг: Возраст
                            </div>
                        </div>
                        <div class="portlet-body">
                            ${form.target_age(class_='form-control')}
                        </div>
                    </div>
                </div>
            </div>




            <div class="row">
                <div class="col-md-12">
                    <div class="portlet">                                               <!------------------ GEO TARGETING  -->
                        <div class="portlet-title">
                            <div class="caption">Таргетинг: Гео</div>
                            <div class="make-switch switch-mini pull-right" data-on="success" data-off="danger">
                                <input type="checkbox" name="targeting" class="toggle" id="geo_targeting"
                                       value="${Campaign.Targeting.GEO}"
                                    ${'checked' if Campaign.Targeting.GEO & g.campaign.targeting or Campaign.Targeting.GEO in (form.targeting.data or []) else ''}
                                        />
                            </div>
                        </div>
                        <div class="portlet-body">                                                            <!-- CAMPAIGN.GEO -->
                            <div id="geo_tree"></div>
                            <div class="xform-error text-danger text-center">${"<br>".join(form.target_geo.errors)}</div>
                            <input type="hidden" name="target_geo" id="target_geo">
                            <script type="text/javascript">

                                var fancytree;
                                var selectedNodes = ${map(int, form.data.get('target_geo', []))};

                                $(function () {
                                    fancytree = $("#geo_tree").fancytree({ checkbox: true, selectMode: 3, keyboard: false, clickFolderMode: 2, icons: false, source: ${g.nodes}
                                        , select: function (event, data) {
                                            var snodes = data.tree.getSelectedNodes(true);
                                            var skeys = [];

                                            for (var i in snodes) {
                                                skeys.push(snodes[i].key);
                                            }

                                            $("#target_geo").val(skeys);
                                        }
                                    });
                                    for (i in selectedNodes) {
                                        $("#geo_tree").fancytree("getNodeByKey", selectedNodes[i]).setSelected(true);
                                    }
                                });
                            </script>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="portlet">
                        <div class="portlet-title">
                            <div class="caption">Ограничения расходов</div>
                        </div>
                        <div class="portlet-body">
                            <div class="form-group ${'has-error' if form.limit_cost_total.errors or form.limit_cost_daily.errors else ''}">
                                <label for="limit-total" class="col-sm-2 control-label">Общие</label>

                                <div class="col-sm-4">
                                    <input name="limit_cost_total" type="text" class="form-control" id="limit_total"
                                           value="${form.limit_cost_total.data or 0}"/>
                                    <span class="help-block"><small>0 - без ограничений</small></span>
                                </div>
                                <label for="limit-daily" class="col-sm-2 control-label">В сутки</label>

                                <div class="col-sm-4">
                                    <input type="text" name="limit_cost_daily" class="form-control" id="limit_daily"
                                           value="${form.limit_cost_daily.data or 0}"/>
                                </div>
                            </div>
                            <div class="form-group ${'has-error' if form.ts_start.errors or form.ts_end.errors else ''} form-inline">
                                <label for="date-start" class="col-sm-2 control-label">Начало показа</label>

                                <div class="col-sm-4">
                                    <div id="date-start">
                                        <input type="text" name="ts_start" class="form-control" placeholder="" readonly
                                               value=""/>
                                    </div>
                                </div>
                                <label for="date-end" class="col-sm-2 control-label">Конец показа</label>

                                <div class="col-sm-4">
                                    <div id="date-end">
                                        <input type="text" name="ts_end" class="form-control" placeholder="" readonly
                                               value=""/>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

                <!-- TODO: add actual fields -->
            <input type="hidden" name="total_cost_limit" value="10">
        </div>


        <div class="col-md-3">
            <div class="portlet fixed">
                <div class="portlet-title">
                    <div class="caption">Биллинг</div>
                </div>
                <div class="portlet-body ">

                    <div class="input-group ${'has-error' if form.bid.errors else ''}">
                        <span class="input-group-addon">Ставка</span>
                        <input type="text" class="form-control" name="bid" value="${form.data.get('bid') or '0'}">
                    </div>
                    <div class="xform-error text-danger">${"<br>".join(form.bid.errors)}</div>

                    <hr>

                    <div id="xdata">
                        <dl class="dl-horizontal">
                            <dt>Минимальная</dt>
                            <dd id="min_bid">--</dd>
                        </dl>
                        <dl class="dl-horizontal">
                            <dt>Рекомендованая</dt>
                            <dd id="rec_bid">--</dd>
                        </dl>
                        <dl class="dl-horizontal">
                            <dt>Прогноз</dt>
                            <dd id="forecast">--</dd>
                        </dl>
                    </div>

                </div>
                <div class="form-actions fluid" style="margin-top:0">
                    <div class="text-center">
                        <button type="submit" class="btn btn-success">Сохранить</button>
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>

<script type="text/javascript">
Index = function () {
    return {
        init: function (options) {
            var self = this;   // DO NOT EDIT OR REMOVE THIS LINE
            moment.lang("ru"); // DO NOT EDIT OR REMOVE THIS LINE

            // Default options (as it was a new app)
            self.options = {
                campId: 0,
                campCategory: 0,
                campTargetCategories: [],
                campTSStart: 0,
                campTSEnd: 0,
                dateRangePickerConf: function (from, to) {
                    return {
                        opens: (App.isRTL() ? 'right' : 'left'),
                        minDate: '01.01.2012',
                        maxDate: moment().add('years', 5).format('DD.MM.YYYY'),
                        ##                            dateLimit: {
                        ##                                days: 60
                        ##                            },
                                                    showDropdowns: false,
                        showWeekNumbers: true,
                        dateRange: false,
                        timePicker: false,
                        timePickerIncrement: 1,
                        timePicker12Hour: false,
                        buttonClasses: ['btn', 'btn-small'],
                        applyClass: 'btn-success',
                        cancelClass: 'btn-default',
                        format: 'DD.MM.YYYY',
                        separator: ' to ',
                        locale: {
                            applyLabel: 'ОК',
                            cancelLabel: 'Отмена',
                            fromLabel: 'С',
                            toLabel: 'По',
                            customRangeLabel: 'Вручную',
                            daysOfWeek: ['Вс', 'Пн', 'Вт', 'Ср', 'Чт', 'Пт', 'Сб'],
                            monthNames: ['Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь', 'Июль', 'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь'],
                            firstDay: 1
                        }
                    }
                }
            };

            // Applying recieved options
            $.extend(self.options, options);

            // Init camp category select2
            self.loadCategories = function () {
                $("#camp-category").empty();
                $.ajax({
                    url: "/campaign/category",
                    data: {
                        type: 0,
                        platform: 0,
                        q: ""
                    },
                    success: function (response) {
                        $.each(response.objects, function () {
                            $("<option value='" + this.id + "'>" + this.name + "</option>").appendTo($("#camp-category"));
                        })
                        if (self.options.campCategory) {
                            $("#camp-category").select2("val", self.options.campCategory);
                        }
                    }
                })
            };

            self.loadCategories();

            $("#camp-category").select2({});


            var startDatepicker,
                    endDatepicker,
                    onStartChangeDate = function (e) {
                        endDatepicker.datepicker("setStartDate", e.date);
                    },
                    onEndChangeDate = function (e) {
                        startDatepicker.datepicker("setEndDate", e.date);
                    };

            if (self.options.campTSStart != 0) {
                startDatepicker = Index.initDatepicker({
                    el: "#date-start",
                    date: moment(self.options.campTSStart)
                }).datepicker("setEndDate", moment(self.options.campTSEnd).format("l")).on("changeDate", onStartChangeDate);
            } else {
                startDatepicker = Index.initDatepicker({
                    el: "#date-start"
                }).on("changeDate", onStartChangeDate);
            }

            if (self.options.campTSEnd != 0) {
                endDatepicker = Index.initDatepicker({
                    el: "#date-end",
                    date: moment(self.options.campTSEnd)
                }).datepicker("setStartDate", moment(self.options.campTSStart).format("l")).on("changeDate", onEndChangeDate);
            } else {
                endDatepicker = Index.initDatepicker({
                    el: "#date-end"
                }).on("changeDate", onEndChangeDate);
            }


            // Loading categories
            $.ajax({
                url: "/campaign/category",
                data: {
                    q: "",
                    type: 3
                },
                success: function (response) {
                    var i = 0, list;
                    $.each(response.objects, function () {
                        var checked = self.options.campTargetCategories.indexOf(this.id) >= 0 ? "checked" : "";
                        if (i == 0 || i == parseInt(response.objects.length / 2)) {
                            list = $("<div class='col-md-6'><div class='checkbox-list'></div></div>").appendTo($("#camp-target-categories")).find(".checkbox-list");
                        }
                        $("<label><input type='checkbox' class='checkbox-uniform' name='target_categories_groups' value='" + this.id + "' " + checked + ">" + this.name + "</label>").appendTo(list);
                        i++;
                    });

                    $(".checkbox-uniform").uniform();
                }
            });

            $("form").validate({
                highlight: function (el) {
                    $(el).parent().addClass("has-error");
                }
            });

            $("button[type='submit']").on("click", function (evt) {
                evt.preventDefault();
                if ($("form").valid()) {
                    $("form").submit()
                }
            });

            $(".select2-choices").addClass("form-control");

            $("#target_gender").select2();
            $("#target_age").select2();
        },
        initDatepicker: function (options) {
            var self = this;
            var defaults = {
                el: null,
                date: null,
                datepicker: {
                    language: "ru",
                    format: "dd.mm.yyyy"
                }
            };

            $.extend(true, defaults, options);

            var updateDatePlaceholders = function (date) {
                if (date) {
                    var dateString = date.format('DD.MM.YYYY');
                    $(defaults.el + " input").val(dateString);
                }
            };

            updateDatePlaceholders(defaults.date);
            return $(defaults.el + " input").datepicker(defaults.datepicker); //.data("datepicker");
        }
    }
}();

var queryForecast = function () {
    $.post("/campaign/forecast",
            $("#campaign_form").serialize(),
            function (data) {
                $("#min_bid").text(data.min);
                $("#rec_bid").text(data.recommended);
                $("#forecast").text(data.forecast);
            }
    );
}

$(document).on("ready", function () {
    <%
        ts_start = form.data.get('ts_start') or 0
        ts_end =form.data.get('ts_end') or 0
    %>
    Index.init({
        campId: ${g.campaign.id or 0},
        campCategory: ${form.data.get('category') or 0},
        campTargetCategories: ${form.target_categories_groups.data or []},
        campTSStart: ${int("%s000" % ts_start)},
        campTSEnd: ${int("%s000" % ts_end)}
    });

    $("#campaign_form input").on('change', queryForecast);
    queryForecast();
});

//Fixed forecast block
$(function () {
    var offset = $(".fixed").offset();
    var topPadding = 65;
    $(window).scroll(function () {
        if ($(window).scrollTop() > offset.top - 35) {
            $(".fixed").stop().animate({marginTop: $(window).scrollTop() - offset.top + topPadding}, 400, 'linear');
        }
        else {
            $(".fixed").stop().animate({marginTop: 0}, 400, 'linear');
        }
        ;
    });
});

$(".check_all").on("click", function () {
    var self = $(this);
    $.each($(self.parents()[1]).find("input[type='checkbox']"), function () {
        if (!$(this).prop("checked")) $(this).trigger("click");
    });
});

$(".uncheck_all").on("click", function () {
    var self = $(this);
    $.each($(self.parents()[1]).find("input[type='checkbox']"), function () {
        if ($(this).prop("checked")) $(this).trigger("click");
    });
});
</script>
