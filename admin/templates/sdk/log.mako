<%namespace name="bcrumb" file="/layout/bcrumb.mako" />
<%namespace name="fabutton" file="/layout/fa-button.mako" />
<%inherit file="/layout/main.mako" />
<%!
    import datetime
    from faan.core.model.general.sdklog import SDKLog
    from faan.core.model.pub.application import Application
    from faan.core.model.adv.campaign import Campaign
%>
<%
    error_type = {
        SDKLog.Type.DEBUG: u'DEBUG',
        SDKLog.Type.ERROR: u'ERROR',
        SDKLog.Type.EXCEPTION: u'EXCEPTION',
    }
    error_list = [(0, "ALL")] + [(k, v) for k, v in error_type.items()]
    error = {
        'id': 'id_select_error_type',
        'list': error_list,
        'placeholder': 'Err. type',
    }

    device_class = {
        Campaign.DevClass.PHONE: u'PHONE',
        Campaign.DevClass.TABLET: u'TAB',
    }
    device_list = [(0, "ALL")] + [(k, v) for k, v in device_class.items()]
    device = {
        'id': 'id_select_device_type',
        'list': device_list,
        'placeholder': 'Dev. class',
    }

    platform_type = {
        Application.Os.ANDROID: u'ANDROID',
        Application.Os.IOS: u'IOS',
        Application.Os.WINPHONE: u'WINPHONE',
    }
    platform_list = [(0, "ALL")] + [(k, v) for k, v in platform_type.items()]
    platform = {
        'id': 'id_select_platform_type',
        'list': platform_list,
        'placeholder': 'Platform type',
    }

    def determine_select_item(selects, name):
        for s in selects:
            if s['name'] == name:
                return s
        else:
            raise ValueError

%>

<%def name="title()">SDK Log</%def>
<%def name="description()">SDK Log</%def>

<script type="text/javascript" src="/global/static/plugins/zclip/jquery.zclip.js"></script>

${ bcrumb.h(self) }
${ bcrumb.bc([ { 'name' : u'SDK Log', 'href' : '/sdk/log/', 'icon' : 'bullhorn' }]) }

<%def name="show_select_item(select, i)">
    %if i in (0, -1,):
    <div class="form-group">
    <label class="control-label col-md-3"></label>
    %endif
        <div class="col-md-2">
                <input name="${select['name']}" type="hidden" id="${select['id']}" style="width:150px"/>
            <span class="help-block">
                 ${select['help_string']}
            </span>
        </div>
    %if i in (1, -1,):
    </div>
    %endif
</%def>

<%def name="show_filters(selects)">
    <div class="form-group">
        <label class="control-label col-md-3"></label>
        <div class="col-md-3">
            <div class="input-group" id="defaultrange">
                <input name="date" value="${getattr(g, 'date_period', '')}" type="text" class="form-control">
                <span class="input-group-btn">
                    <button class="btn btn-default date-range-toggle" type="button"><i class="fa fa-calendar"></i></button>
                </span>
            </div>
        </div>
    </div>

    <div class="form-group">
        <label class="control-label col-md-3"></label>

        <div class="col-md-1">
                <input name="select_err_type" type="text" id="id_select_error_type" value="${g.err_type}"/>
            <span class="help-block">
            </span>
        </div>


        <div class="col-md-1">
                <input name="select_device_type" type="text" id="id_select_device_type" value="${g.dev_type}"/>
            <span class="help-block">
            </span>
        </div>

        <div class="col-md-1">
                <input name="select_platform_type" type="text" id="id_select_platform_type" value="${g.platform_type}"/>
            <span class="help-block">
            </span>
        </div>
    </div>

    <% order = ['advertiser', 'publisher', 'campaign', 'application', 'media'] %>
    %for i, name in enumerate(order):
        %for select in selects:
            %if select['name'] == name:
                %if i == len(selects) - 1:
                    ${self.show_select_item(select, -1)}
                %else:
                    ${self.show_select_item(select, i % 2)}
                %endif
            %endif
        %endfor
    %endfor
    <!-- ==== SUBMIT ==== --->
    <div class="form-group">
        <label class="control-label col-md-3"></label>
        <div class="col-md-2">
            <a href="#" id="apply_changes" class="btn btn-success">Применить</a>
        </div>

        <!-- ==== COPY STATISTIC URL ==== --->
        <div class="col-md-2">
            <a href="#" id="copy_log_page" class="btn btn-info">Скопировать лог</a>
        </div>
    </div>

</%def>

<%def name="render_table(id)">
 <!--START TABLE-->
 <table class="table table-striped table-bordered table-hover" id="${id}">
 <thead>
 <tr>
     <th>Date</th>
     <th>ID</th>
     <th>Err. type</th>
     <th>App</th>
     <th>Camp.</th>
     <th>Media</th>
     <th>OS</th>
     <th>Dev type</th>
     <th>Geo</th>
     <th>Version</th>
     <th style="word-wrap: break-word; max-width: 30px;">Msg</th>
     <th>Context</th>
 </tr>
 </thead>
 <tbody>

${caller.body()}

 </tbody>
 </table>
 <!--END TABLE-->
</%def>

<!-- BEGIN PAGE CONTENT-->
<div class="row">
<div class="col-md-12">
    <div class="portlet">
        <div class="portlet-title">
            <div class="caption"><i class="fa fa-rocket"></i>
            	SDK
            </div>
        </div>
        <div class="portlet-body">
        	<form method="GET" class="form-horizontal">
                ${self.show_filters(g.selects)}
			</form>
            <%self:render_table id="log_table">

            </%self:render_table>

            <div class="row">
                <div class="col-md-6 col-sm-12"></div>
                <div class="col-md-6 col-sm-12">
                    <div id="log_table_filter" class="dataTables_filter">
                        <label>
                        &nbsp
                            <ul class="pagination">
                                <li class="prev disabled"><a title="Prev" href="#" id="pag_prev"><i class="fa fa-angle-left"></i></a></li>
                                <li class="next"><a title="Next" href="#" id="pag_next"><i class="fa fa-angle-right"></i></a></li>
                            </ul>
                        </label>
                    </div>
                </div>
            </div>


        </div>
    </div>
</div>
</div>
<!-- END PAGE CONTENT-->

<!--------------- DATA RANGE PICKER BEGIN -----------------!>
<!-- CSS --!>
<link rel="stylesheet" type="text/css" href="/global/static/plugins/bootstrap-daterangepicker/daterangepicker-bs3.css"/>

<!-- SCRIPT --!>
<script type="text/javascript" src="/global/static/plugins/bootstrap-daterangepicker/moment.min.js"></script>
<script type="text/javascript" src="/global/static/plugins/bootstrap-daterangepicker/daterangepicker.js"></script>
<script>
 $('#defaultrange').daterangepicker({
         opens: (App.isRTL() ? 'left' : 'right'),
         format: 'DD.MM.YYYY',
         separator: ' to ',
         startDate: moment().subtract('days', 0),
         endDate: moment(),
         minDate: '01/01/2012',
         maxDate: '12/31/2014',
         locale: { cancelLabel: 'Отмена', applyLabel: 'ОК', fromLabel: 'С', toLabel: 'по', }
     },
     function (start, end) {
         console.log("Callback has been called!");
         $('#defaultrange input').val(start.format('DD.MM.YYYY') + ' - ' + end.format('DD.MM.YYYY'));
     }
 );
</script>
<!--------------- DATA RANGE PICKER END -----------------!>

<!-- SCRIPTS INDEX -->
<script type="text/javascript">
Index = function() {
    return {

        // Some table attributes
        data: null,
        total: +0,

        // Pagination options
        pag_size: +100,
        pag_start: 0,

        init: function() {
            $("#apply_changes").on("click", function(e) {
                Index.loadData()
            });

            this.first = true;
        },

        initSelect2: function() {
            var self = this;
            var format = function(item) { return item.name };


            %for elem in (error, platform, device,):
            $("#${elem['id']}").select2({
                placeholder: "${elem['placeholder']}",
                data: [
                    %for i, (k, v) in enumerate(elem['list']):
                        %if i != len(error_list) - 1:
                        {"id": ${k}, "text": "${v}"},
                        %else:
                        {"id": ${k}, "text": "${v}"}
                        %endif
                    %endfor
                ]
            })
            %endfor

            %for select in g.selects:
                <%
                    _name = 'account' if select['name'] in ('advertiser', 'publisher', ) else select['name']
                    try:
                        placeholder = getattr(g, str(_name) + '_name')
                    except StandardError:
                        placeholder = select['header']
                %>

                $("#${select['id']}").select2({
                    placeholder: "${placeholder}",
                    ajax: {
                        url: function() { return "/sdk/log/ajax/select?query=${select['name']}&values=" + select_list.get_values() },
                        type: "GET",
                        results: function(data, page) {
                            for (var name, i = 0; i < data.objects.length; i++) {
                                if (data.objects[i].id == null) {
                                    data.objects[i].name = "Все"
                                }
                            }
                            return {results: data.objects, text: 'name' };
                        },
                        data: function(term, page) { return { q: term } }
                    },
                    formatSelection: format,
                    formatResult: format
                }).on("select2-selecting", function(e) {
                    select_list["${select['name']}"] = e.val;
                    switch("${select['name']}") {
                        case "advertiser":
                            <% select = determine_select_item(g.selects, 'campaign') %>
                            select_list["${select['name']}"] = '';
                            $("#${select['id']}").select2("data", {id: "", text: "Все"});
                        case "campaign":
                            <% select = determine_select_item(g.selects, 'media') %>
                            select_list["${select['name']}"] = '';
                            $("#${select['id']}").select2("data", {id: "", text: "Все"});
                        case "media":
                            break;
                        case "publisher":
                            <% select = determine_select_item(g.selects, 'application') %>
                            select_list["${select['name']}"] = '';
                            $("#${select['id']}").select2("data", {id: "", text: "Все"});
                            break;
                    }
                });
            %endfor

        },

        loadData: function(start, count, search) {
            try { $("#log_table").DataTable().destroy(); } catch (e) { }

            var date = $("#defaultrange input").val();
            var err_type = $("#id_select_error_type").val();
            var dev_type = $("#id_select_device_type").val();
            var platform_type = $("#id_select_platform_type").val();
            var timezone = +(moment().format("ZZ")) * 36;

            start = start ? start : 0;
            count = count ? count : Index.pag_size;
            search = search ? search : "";

            var url = "/sdk/log/ajax/table?values=" + select_list.get_values() + "&date=" + date + "&error_type=" + err_type + "&dev_type=" + dev_type + "&platform_type=" + platform_type + "&start=" + start + "&count=" + count + "&search=" + search + "&timezone=" + timezone;

            c = [
               { data: 'date' },
               { data: 'id' },
               { data: 'type' },
               { data: 'app' },
               { data: 'campaign' },
               { data: 'media' },
               { data: 'platform' },
               { data: 'device' },
               { data: 'geo' },
               { data: 'version' },
               { data: 'text' },
               { data: 'context' }
            ];

            $("#log_table").dataTable({
                processing: true,
                ajax: {
                    url: url,
                    dataSrc: function(response) {
                        Index.total = response.meta.total;
                        return response.data;
                    }
                },
                columns: c,
                lengthChange: true,
                "aLengthMenu": [
                    [25, 50, 100, -1],
                    [25, 50, 100, "All"] // change per page values here
                ],
                // set the initial value
                "iDisplayLength": 50,
                "sPaginationType": "bootstrap",
                "oLanguage": {
                    "sLengthMenu": "_MENU_ records",
                    "oPaginate": {
                        "sPrevious": "Prev",
                        "sNext": "Next"
                    }
                },
                "aoColumnDefs": [{
                        'bSortable': false,
                        'aTargets': [0]
                    }
                ],
                order: [],
                paging: false,
                bFilter: false,
                bInfo: false
            });

            // Add select items.
            $("#log_table_wrapper .row")[0].children[0].innerHTML = '<div class="dataTables_length" id="log_table_length"><label><select name="log_table_length" id="_log_table_length" aria-controls="log_table" class="form-control input-xsmall"><option value="100">100</option><option value="150">150</option><option value="300">300</option></select> records</label></div>'
            $('#log_table_wrapper .dataTables_length select').select2().on("select2-selecting", function(e) {
                Index.pag_size = + e.val;
                Index.pag_start = + 0;
                Index.loadData(0, Index.pag_size);
                $('#log_table_wrapper .dataTables_length select').select2("val", e.val.toString());
            });

            //Add search item.
            ##$("#log_table_wrapper .row")[0].children[1].innerHTML = '<div id="stats_table_new_filter" class="dataTables_filter"><label>Search:<input type="search" class="form-control input-medium" aria-controls="stats_table_new"></label></div>'
            $("#log_table_wrapper .row")[0].children[1].innerHTML = '<div class="dataTables_filter" id="stats_table_new_filter"><label>Search:<input type="search" aria-controls="stats_table_new" class="form-control input-medium"></label></div>'
            $('#log_table_wrapper .dataTables_filter input').on("change", function(){
                var text = $(this).val();
                Index.pag_start = + 0;
                Index.loadData(Index.pag_start, Index.pag_size, text);
                $('#log_table_wrapper .dataTables_length select').select2("val", Index.pag_size);
                $('#log_table_wrapper .dataTables_filter input').val(text);
            });

            // Parsing parameters for the table.
            $("#log_table").on('order.dt',  function () {
                var rows = $("#log_table").dataTable().fnGetNodes();

                if (rows.length < Index.pag_size)
                    $(".next").addClass("disabled");
                else
                    $(".next").removeClass("disabled");

                for (var i = 0; i < rows.length; i++) {

                    // Type error
                    try {
                        _type = JSON.parse(rows[i].cells[2].innerHTML);
                         switch(_type.id){
                            case ${SDKLog.Type.DEBUG}:
                                rows[i].cells[2].innerHTML = 'DEBUG';
                                break;
                            case ${SDKLog.Type.ERROR}:
                                rows[i].cells[2].innerHTML = 'ERROR';
                                break;
                            case ${SDKLog.Type.EXCEPTION}:
                                rows[i].cells[2].innerHTML = 'EXCEPTION';
                                break;
                            default:
                                rows[i].cells[2].innerHTML = 'UNKNOWN';
                                break;
                        }
                    } catch(e) { }

                    // Platform
                    try {
                        var platform = rows[i].cells[6].innerHTML;
                        platform = JSON.parse(platform);
                        switch(platform.id){
                            case ${Application.Os.IOS}:
                                rows[i].cells[6].innerHTML = '<i class="fa fa-apple" style="font-size:22px;line-height: 22px;"></i><div style="display:none">iOS</div>'
                                break;
                            case ${Application.Os.ANDROID}:
                                rows[i].cells[6].innerHTML = '<i class="fa fa-android" style="font-size:22px;line-height: 22px;"></i><div style="display:none">Android</div>'
                                break;
                            case ${Application.Os.WINPHONE}:
                                rows[i].cells[6].innerHTML = '<i class="fa fa-windows" style="font-size:22px;line-height: 22px;"></i><div style="display:none">Windows</div>'
                                break;
                            default:
                                rows[i].cells[6].innerHTML = '<i class="fa fa-times" style="font-size:22px;line-height: 22px;"></i><div style="display:none">Unknown</div>'
                                break;
                        }
                    } catch(e) { }

                    // Type device
                    try {
                        device = JSON.parse(rows[i].cells[7].innerHTML);
                         switch(device.id){
                            case ${Campaign.DevClass.PHONE}:
                                rows[i].cells[7].innerHTML = 'PHONE';
                                break;
                            case ${Campaign.DevClass.TABLET}:
                                rows[i].cells[7].innerHTML = 'TAB';
                                break;
                            default:
                                rows[i].cells[7].innerHTML = 'UNKNOWN';
                                break;
                        }
                    } catch(e) { }

                    // Message and Context
                    try {
                        var new_line_length = 40;
                        var text = rows[i].cells[10].innerHTML;
                        text = JSON.parse(text);
                        var context = rows[i].cells[11].innerHTML;
                        rows[i].cells[10].innerHTML = addNewlines(text.msg, new_line_length);
                        rows[i].cells[11].innerHTML = addNewlines(context, new_line_length);
                        rows[i].cells[11].innerHTML = '<a iscontexthidden="1" row="' + i + '" cell="11" onclick="contextAction(event)">' + hideText(rows[i].cells[11].innerHTML) + '</a>';
                    } catch (e) { }

                }

                // Add summary.
                var start = +Index.pag_start + 1, end;
                if (!Index.total) {
                    start = 0;
                    end = 0;
                } else if ((+Index.pag_start + Index.pag_size) > +Index.total)
                    end = Index.total;
                else
                    end = + Index.pag_start + Index.pag_size;
                $("#log_table_wrapper .row")[1].children[0].innerHTML = 'С {start} по {end}, всего {total}'
                    .replace("{total}", Index.total)
                    .replace("{start}", start)
                    .replace("{end}", end);
            });
        }
    }
}();

function contextAction(e) {
    var row = e.currentTarget.attributes.row.nodeValue;
    var cell = e.currentTarget.attributes.cell.nodeValue;
    var isContextHidden = Number(e.currentTarget.attributes.iscontexthidden.nodeValue);
    var rows = $("#log_table").dataTable().fnGetNodes();

    // Remove <a> tag
    rows[row].cells[cell].innerHTML = rows[row].cells[cell].innerHTML
        .replace('<a iscontexthidden="' + isContextHidden + '" row="' + row + '" cell="' + cell + '" onclick="contextAction(event)">', '')
        .replace('</a>', '')
        .replace('Скрыть', '');

    if (isContextHidden) {
        isContextHidden = 0;
        // ShowText
        rows[row].cells[cell].innerHTML = showText(rows[row].cells[cell].innerHTML);
        rows[row].cells[cell].innerHTML = rows[row].cells[cell].innerHTML + '<a iscontexthidden="' + isContextHidden + '" row="' + row + '" cell="' + cell + '" onclick="contextAction(event)">' + 'Скрыть' + '</a>';
    } else {
        isContextHidden = 1;
        rows[row].cells[cell].innerHTML = hideText(rows[row].cells[cell].innerHTML);
        rows[row].cells[cell].innerHTML = '<a iscontexthidden="' + isContextHidden + '" row="' + row + '" cell="' + cell + '" onclick="contextAction(event)">' + rows[row].cells[cell].innerHTML + '</a>';
    }
    // Add <a> tag

}

function addNewlines(str, new_line) {
    var result = '';
    while (str.length > 0) {
      result += str.substring(0, new_line) + '<br>';
      str = str.substring(new_line);
    }
    return result;
}

function hideText(text) {
    var header_length = 30;
    var header = text.substring(0, header_length);
    var hidden_text = text.substring(header_length);
    var result = header + '<span style="display:none;">' + hidden_text + '</span>';
    return result;
}

function showText(text) {
    return text.replace('<span style="display:none;">', '').replace('</span>', '');
}

</script>
<!-- SCRIPTS INDEX END-->


<script type="text/javascript">
    $(document).on("ready", function() {

        select_list =  {

            %for select in g.selects:
                <%
                    try:
                        _name = select['name']
                        value = getattr(g, str(_name)) or ''
                    except StandardError:
                        value = ''
                %>
                ${select['name']}: "${value}",
            %endfor

            get_values: function() {
                s = "";
                %for i, select in enumerate(g.selects):
                    %if i != len(g.selects) - 1:
                        s += "${select['name']}:" + this["${select['name']}"] + ",";
                    %else:
                        s += "${select['name']}:" + this["${select['name']}"];
                    %endif
                %endfor
                return s;
            },
             get_values_log: function() {
                s = "";
                %for i, select in enumerate(g.selects):
                    s += "&${select['name']}=" + this["${select['name']}"];
                %endfor
                return s;
            }
        };

        Index.init();
        Index.initSelect2();
        Index.loadData();

         // Pagination sctipt
         $("#pag_prev").on("click", function() {
             if ($(".prev").attr("class").indexOf("disabled") > 0) {
                return;
             }

             Index.pag_start -= Number(Index.pag_size);
             if (+Index.pag_start < 0) {
                Index.pag_start = +0;
                $(".prev").addClass("disabled");
                return;
             }
             $(".next").removeClass("disabled");

             Index.loadData(Index.pag_start, Index.pag_size);

         });
         $("#pag_next").on("click", function() {
             if ($(".next").attr("class").indexOf("disabled") > 0) {
                return;
             }

             $(".prev").removeClass("disabled");

             Index.pag_start = Number(Index.pag_start) + Number(Index.pag_size);
             Index.loadData(Number(Index.pag_start), Number(Index.pag_size));
         });

         // Copy page
         var base_url = "http://admin.vidiger.com/sdk/log/";

         $('a#copy_log_page').zclip({
             path: '/global/static/plugins/zclip/ZeroClipboard.swf',
             copy: function(){
                 var url = "";
                 var date = $("#defaultrange input").val();
                 var err_type = $("#id_select_error_type").val();
                 var dev_type = $("#id_select_device_type").val();
                 var platform_type = $("#id_select_platform_type").val();

                 return base_url + "?date=" + date + select_list.get_values_log() + "&error_type=" + err_type + "&dev_type=" + dev_type + "&platform_type=" + platform_type;
             }
         });
    });
</script>