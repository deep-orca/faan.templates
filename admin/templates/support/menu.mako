<div class="col-md-3">
    <div class="list-group margin-left-10 x-actions-list">
##        <a href="/support/new"
##           class="list-group-item round-top ${'active' if g.view_type == 'new' else ''}">
##            Новые

##        </a>
        <a href="/support/open"
           class="list-group-item ${'active' if g.view_type == 'open' else ''}">
            Открытые
            %if g.total_new_replies.get(0, 0) > 0:
                <span class="badge">${g.total_new_replies.get(0, "")}</span>
            %endif
        </a>
        <a href="/support/resolved"
           class="list-group-item ${'active' if g.view_type == 'resolved' else ''}">
            Закрытые
            %if g.total_new_replies.get(1, 0) > 0:
                <span class="badge">${g.total_new_replies.get(1, "")}</span>
            %endif
        </a>
    </div>
</div>