## -*- coding: utf-8 -*-
<%namespace name="bcrumb" file="/layout/bcrumb.mako" />
##<%namespace name="balance" file="/layout/balance.mako" />

<%inherit file="/layout/main.mako" />

<%def name="title()">Администратор</%def>
<%def name="description()">Финансы</%def>

<%def name="page_css()">
    <link href="/global/static/css/pages/balance.css" rel="stylesheet" type="text/css"/>
</%def>

<%def name="page_js()">
    <script type="text/javascript" src="/global/static/plugins/data-tables/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="/global/static/plugins/moment-with-langs.min.js"></script>
</%def>

${ bcrumb.h(self) }
${ bcrumb.bc(
    [
        {
            'name': u'Администратор',
            'href': '/',
            'icon': 'bullhorn'
        },
        {
            'name': u'Финансы',
            'href': '/balance/',
            'icon': 'ruble'
        }
    ]
)}

<div class="row">
	<div class="col-md-12">
		<div class="portlet">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-cogs"></i>История платежей
                </div>
##                <div class="tools">
##                    <a href="" id="reload-table" class="reload"></a>
##                </div>
            </div>
            <div class="portlet-body">
                <div class="table-responsive">
                    <form id="balance-filter" class="panel" style="overflow:hidden;">
                        <div class="form-group col-md-3">
                            <label for="balance-filter-type">
                                Тип транзакции
                            </label>
                            <select class="form-control" name="type" id="balance-filter-transaction-type">
                                <option value>Все</option>
                                <option value="0">Пополнение</option>
                                <option value="1">Списание</option>
                            </select>
                        </div>

                        <div class="form-group col-md-3">
                            <label for="balance-filter-account-group">
                                Тип аккаунта
                            </label>
                            <select class="form-control" name="agroup" id="balance-filter-account-groups">
                                <option value>Все</option>
                                <option value="1">Разработчик</option>
                                <option value="2">Рекламодатель</option>
                            </select>
                        </div>

                        <div class="form-group col-md-3">
                            <label for="balance-filter-partner">
                                Партнер
                            </label>
                            <select class="form-control" name="uid" id="balance-filter-partner">
                                <option value>Все</option>
                            </select>
                        </div>

                    </form>
                    <table id="history-table" class="table table-hover">
                        <thead>
                            <tr>
                                <th>
                                     Дата
                                </th>
                                <th>
                                     ID
                                </th>
                                <th>
                                     Кому
                                </th>
                                <th>
                                     От кого
                                </th>
                                <th>
                                     Платежная система
                                </th>
                                <th>
                                     Сумма
                                </th>
                                <th>
                                     Реквизиты
                                </th>
                                <th>
                                     Комментарий
                                </th>
                            </tr>
                        </thead>
                        <tbody>

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
	</div>
</div>

<script type="text/javascript">
    Module = function() {
        return {
            options: {
                dataTableColumns: function () {
                    return [
                        {data: "transaction_ts_spawn", render: function(data, type, row) { return moment(row["transaction_ts_spawn"]).format("L HH:mm") }},
                        {data: "transaction_id"},
                        {data: "dst_acc_name", render: function(data, type, row) {
                            return (row["transaction_dst_acc"] != 0) ? "<a target='_blank' href='/profile/{id}'>{name}</a>".replace("{id}", row["transaction_dst_acc"]).replace("{name}", data) : data
                        }},
                        {data: "issuer_acc_name", render: function(data, type, row) {
                            return (row["transaction_issuer_acc"] != 0) ? "<a target='_blank' href='/profile/{id}'>{name}</a>".replace("{id}", row["transaction_issuer_acc"]).replace("{name}", data) : data
                        }},
                        {data: "transaction_ps", render: function(data, type, row) {
                            var psName;
                            switch (+data) {
                                case 0:
                                    psName = "Вручную";
                                    break;
                                case 1:
                                    psName = "WebMoney";
                                    break;
                                case 2:
                                    psName = "Yandex.Money";
                                    break;
                                case 3:
                                    psName = "QIWI";
                                    break;
                                case 4:
                                    psName = "PayPal";
                                    break;
                                case 5:
                                    psName = "Банковская карта";
                                    break;
                                default:
                                    break;
                            }
                            return psName;
                        }},
                        {data: "transaction_amount", render: function(data) {
                            var _data = data.replace(/ /g, ""),
                                data = data.replace("-", ""),
                                _icon = "<i class='fa fa-plus'></i>",
                                _class = "success";
                            if (+_data < 0) {
                                _icon = "<i class='fa fa-minus'></i>";
                                _class = "danger";
                            }
                            return "<span class='label label-{class}'>{icon} {data} руб.</span>".replace("{class}", _class).replace("{icon}", _icon).replace("{data}", data);
                        }},
                        {data: "transaction_wallet"},
                        {data: "transaction_comment"}
                    ]
                },
                dataTableLang: {
                    "oPaginate": {
                        "sPrevious": "Назад",
                        "sNext": "Вперед"
                    },
                    "sInfo" : "Показаны с _START_ по _END_ из _TOTAL_ транцакций",
                    "sInfoEmpty": "",
                    "sEmptyTable" : "Нет данных для отображения",
                    lengthMenu: "_MENU_ &nbsp; транзакций"
                },
                dataTableLengthMenu: [
                    [50, 100, 250, -1],
                    [50, 100, 250, "Все"]
                ]
            },
            init: function(options) {
                var self = this;

                moment.lang("ru");

                $.extend(self.options, options);

                self.loadData();
                $("#balance-filter-account-groups").on("change", Module.onFilterTypeChange);
                $("#balance-filter-account-groups").trigger("change");

                $("#balance-filter select").on("change", function() {
                    $.ajax({
                        url: "/ajax/balance/list",
                        type: "GET",
                        data: $("#balance-filter").serialize(),
                        success: function(response) {
                            self.drawTable("#history-table", response.objects, self.options.dataTableColumns())
                        }
                    })
                }).select2();
            },
            loadData: function() {
                var self = this;

                $.ajax({
                    url: "/ajax/balance/list",
                    type: "GET",
                    success: function(response) {
                        self.drawTable("#history-table", response.objects, self.options.dataTableColumns())
                    }
                })
            },
            onFilterTypeChange: function() {
                $.ajax({
                    url: "/ajax/accounts/list",
                    type: "GET",
                    data: {filter_groups: $("#balance-filter-account-groups").val()},
                    success: function(response) {
                        $("#balance-filter-partner").empty();
                        $("<option value>Все</option>").appendTo($("#balance-filter-partner"));
                        $.each(response.objects, function() {
                            $("<option value='{id}'>{name}</option>".replace("{id}", this.id).replace("{name}", this.username)).appendTo($("#balance-filter-partner"));
                        });

                    }
                });
            },
            drawTable: function (el, data, columns) {
                var self = this;
                try { $(el).DataTable().destroy(); } catch (e) {}

                // Code below used to add thousand whitespace separator
                // --BEGIN
                $.each(columns, function() {
                    var self = this, _render = null;
                    var beforeRender = function(data, type, row) { try { return [data.toString().replace(/\B(?=(\d{3})+(?!\d))/g, " "), type, row] } catch(e) { return [data, type, row] }};
                    if (self.hasOwnProperty("render")) { _render = self.render }
                    self["render"] = function(data, type, row) {
                        var res = beforeRender(data, type, row), _data = res[0], _type = res[1], _row  = res[2];
                        if (_render) { return _render(_data, _type, _row); }
                        return _data;
                    };
                });
                // --END

                $(el).dataTable({ data: data, columns: columns, language: self.options.dataTableLang, lengthChange: true, searching: false, order: [], paging: true });
                var wrap = $(el).parent().parent();
                wrap.find('input, select').addClass("form-control input-xsmall");
                wrap.find('select').select2();
            }
        }
    }();

    $(document).on("ready", function() {
        Module.init();
    })
</script>