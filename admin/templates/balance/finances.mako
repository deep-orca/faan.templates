<%namespace name="bcrumb" file="/layout/bcrumb.mako" />
<%namespace name="fabutton" file="/layout/fa-button.mako" />
<%inherit file="/layout/main.mako" />

<%!
    from faan.core.model.pub.application import Application
    from faan.core.model.security.account import Account
%>

<%def name="title()">Доходы</%def>
<%def name="description()"></%def>

${ bcrumb.h(self) }

## Page JS
<%def name="page_js()">
    <script src="/global/static/plugins/moment-with-langs.min.js" type="text/javascript"></script>
    <script src="/global/static/plugins/bootstrap-daterangepicker/daterangepicker.js" type="text/javascript"></script>
</%def>
## END

## Page CSS
<%def name="page_css()">
    <link href="/global/static/plugins/fullcalendar/fullcalendar/fullcalendar.css" rel="stylesheet" type="text/css"/>
    <link href="/global/static/plugins/bootstrap-daterangepicker/daterangepicker-bs3.css" rel="stylesheet" type="text/css"/>
</%def>
## END


<!-- BEGIN PAGE CONTENT-->
<div class="row">
    <div class="col-md-12">
		<ul class="page-breadcrumb breadcrumb">
		    <li>
		        <i class="fa fa-money"></i>
		        <a href="/">Администратор</a>
		        <i class="fa fa-angle-right"></i>
		    </li>
		    <li>
		        <a href="/">Финансы</a>
		    </li>
		</ul>
    </div>
</div>

<div class="row">
<div class="col-md-12">
    <div class="portlet">
        <div class="portlet-title">

        <div class="caption"><i class="fa fa-money"></i>Доходы</div>
            <div class="actions">
                <div class="btn-group"></div>
            </div>
        </div>
        <div class="portlet-body">
            <div class="row">
                <div class="col-md-4"></div>
                <div class="col-md-4">
                    <div class="btn btn-default" id="daterange">
						<i class="fa fa-calendar"></i>
						    &nbsp;
						    <span></span>
						<b class="fa fa-angle-down"></b>
					</div>
                </div>
                <div class="col-md-4"></div>
            </div>
            <br>
            <table class="table table-striped table-bordered table-hover" id="financesTable">
            <thead>
            <tr>
                <th class="text-center">Дата</th>
                <th class="text-center">Показы</th>
                <th class="text-center">Доход</th>
                <th class="text-center">Расход</th>
                <th class="text-center">Маржа</th>
                <th class="text-center">eCPM</th>
            </tr>
            </thead>
            <tbody>
            </tbody>
            <tfoot>
                <tr>
                    <th>Всего:</th>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th></th>
                </tr>
            </tfoot>
            </table>
            <div class="row">

            </div>
        </div>
    </div>
</div>
</div>
<!-- END PAGE CONTENT-->

<script type="text/javascript" src="/static/scripts/filters.js"></script>
<script type="text/javascript" src="/static/scripts/finances.js"></script>