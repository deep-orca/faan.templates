<%namespace name="bcrumb" file="/layout/bcrumb.mako" />
<%namespace name="fabutton" file="/layout/fa-button.mako" />
<%inherit file="/layout/main.mako" />

<%!
    import datetime
    from faan.core.model.security.account import Account
%>
<%
    for s in g.selects:
        if s.get('header'):
            continue
        h = {
            'publisher': u'Аккаунт',
            'advertiser': u'Аккаунт',
            'group': u'Группа',
            'country': u'Страна',
            'region': u'Регион',
            'city': u'Город',
            'application': u'Приложеине',
            'source': u'Источник',
            'unit': u'Блок',
            'campaign': u'Капания',
            'media': u'Медиа',
        }.get(s.get('name'));
        s.update({'header': h})
%>

<script type="text/javascript" src="/static/scripts/filters.js"></script>
<script type="text/javascript" src="/global/static/plugins/zclip/jquery.zclip.js"></script>

<%def name="title()">Статистика</%def>
<%def name="description()"></%def>

${ bcrumb.h(self) }
${ bcrumb.bc([ { 'name' : u'Статистика', 'href' : '/stats/', 'icon' : 'bullhorn' }]) }

<%def name="show_select_item(select, start_line=False, end_line=False)">

    %if start_line:
    <div class="form-group">
    <label class="control-label col-md-3"></label>
    %endif
        <div class="col-md-2">
                <input name="${select['name']}" type="hidden" id="${select['id']}" style="width:180px"/>
        </div>
    %if end_line:
    </div>
    %endif
</%def>

<%def name="show_table_header()"> </%def>

<%def name="show_table(id)"> </%def>

<%def name="show_first_column(stat)"> </%def>

<%def name="show_filters(selects)">
    <div class="form-group">
        <label class="control-label col-md-3">Период</label>
        <div class="col-md-4">
            <div class="input-group" id="defaultrange">
                <input name="date" value="${getattr(g, 'date_period', '')}" type="text" class="form-control">
                <span class="input-group-btn">
                    <button class="btn btn-default date-range-toggle" type="button"><i class="fa fa-calendar"></i></button>
                </span>
            </div>
        </div>
    </div>

        %for i, select in enumerate(selects):
            %if i % 2:
                ${self.show_select_item(select, end_line=True)}
            %else:
                %if i == len(selects) - 1:
                    ${self.show_select_item(select, start_line=True, end_line=True)}
                %else:
                    ${self.show_select_item(select, start_line=True)}
                %endif
            %endif
        %endfor

    <!-- ==== SUBMIT ==== --->
    <div class="form-group">
        <label class="control-label col-md-3"></label>
        <div class="col-md-2">
            <a href="#" id="apply_changes" class="btn btn-success">Применить</a>
        </div>

        <!-- ==== COPY STATISTIC URL ==== --->
        <div class="col-md-2">
            <a href="#" id="copy_stats" class="btn btn-info">Скопировать статистику</a>
        </div>
    </div>
</%def>

<!-- BEGIN PAGE CONTENT-->
<div class="row">
<div class="col-md-12">
    <div class="portlet">
        <div class="portlet-title">
            <div class="caption"><i class="fa fa-rocket"></i>
                %if g.a_group == Account.Groups.PUB:
                    Разработчики
                %elif g.a_group == Account.Groups.ADV:
                    Рекламодатели
                %else:
                    Unknown
                %endif
            </div>
        </div>
        <div class="portlet-body">
        	<form method="GET" class="form-horizontal">
                ${self.show_filters(g.selects)}
			</form>
            <%self:show_table id="stats_table">

            </%self:show_table>
        </div>
    </div>
</div>
</div>
<!-- END PAGE CONTENT-->

<!--------------- DATA RANGE PICKER BEGIN ----------------->
<!-- CSS -->
<link rel="stylesheet" type="text/css" href="/global/static/plugins/bootstrap-daterangepicker/daterangepicker-bs3.css"/>

<!-- SCRIPT -->
<script type="text/javascript" src="/global/static/plugins/bootstrap-daterangepicker/moment.min.js"></script>
<script type="text/javascript" src="/global/static/plugins/bootstrap-daterangepicker/daterangepicker.js"></script>
<script>
 $('#defaultrange').daterangepicker({
         opens: (App.isRTL() ? 'left' : 'right'),
         format: 'DD.MM.YYYY',
         separator: ' to ',
         startDate: moment().subtract('days', 0),
         endDate: moment(),
         minDate: '01/01/2012',
         maxDate: '12/31/2014',
         locale: { cancelLabel: 'Отмена', applyLabel: 'ОК', fromLabel: 'С', toLabel: 'по', }
     },
     function (start, end) {
         $('#defaultrange input').val(start.format('DD.MM.YYYY') + ' - ' + end.format('DD.MM.YYYY'));
     }
 );
</script>
<!--------------- DATA RANGE PICKER END ----------------->

<script type="text/javascript">
Index = function() {
    return {

        init: function() {
            $("#apply_changes").on("click", function(e) {
                Index.loadData()
            });
        },

        loadData: function() {
            try { $("#stats_table").DataTable().destroy(); } catch (e) {}

            var data;
            var date = $("#defaultrange input").val();
            var timezone = +(moment().format("ZZ")) * 36;

            var url = "/stats/ajax/table?page=${g.page}&account_group=${g.a_group}&date=" + date + "&values=" + filterBox.getValues() + "&timezone=" + timezone;

            $.ajax({
                url: url,
                type: "GET",
                success: function(response) { Index.drawTable(response.data, response.meta); }
            });
        },

        drawTable: function(data, meta) {
            %if g.a_group == Account.Groups.ADV:
                c = [
                   { data: 'first' },
                   %if g.page in ("acc", "cam",):
                   { data: 'second' },
                   %elif g.page in ("media",):
                   { data: 'second' },
                   { data: 'third' },
                   %endif
                   { data: 'impressions' },
                   { data: 'clicks' },
                   { data: 'crt' },
                   { data: 'overlays' },
                   { data: 'overlay_cost' },
                   { data: 'endcards' },
                   { data: 'endcard_cost' },
                   { data: 'cost' }
                ];
            %elif g.a_group == Account.Groups.PUB:
                c = [
                   { data: 'first' },
                   %if g.page in ("acc", "app",):
                   { data: 'second' },
                   %elif g.page in ("unit",):
                   { data: 'second' },
                   { data: 'third' },
                   %elif g.page in ("source",):
                   { data: 'second' },
                   { data: 'third' },
                   { data: 'fourth' },
                   %endif
                   { data: 'impressions' },
                   { data: 'clicks' },
                   { data: 'crt' },
                   { data: 'cpm' },
                   { data: 'v4vc_cost' },
                   { data: 'revenue' }
                ];
            %endif
            $("#stats_table").dataTable({
                processing: true,
                data: data,
                columns: c,

                "aLengthMenu": [
                    [25, 50, 100, -1],
                    [25, 50, 100, "All"] // change per page values here
                ],
                // set the initial value
                "iDisplayLength": 50,
                "sPaginationType": "bootstrap",
                "oLanguage": {
                    "sLengthMenu": "_MENU_ records",
                    "oPaginate": {
                        "sPrevious": "Prev",
                        "sNext": "Next"
                    }
                },
                paging: true,
                footerCallback: function ( row, data, start, end, display ) {
                    var api = this.api();

                    %if g.page in ("acc", "cam", "app",):
                    var offset = +1;
                    %elif g.page in ("media", "unit",):
                    var offset = +2;
                    %elif g.page in ("source",):
                    var offset = +3;
                    %else:
                    var offset = +0;
                    %endif

                    if (start == end) {
                        %if g.a_group == Account.Groups.ADV:
                            var numberColumns = 8;
                        %elif g.a_group == Account.Groups.PUB:
                            var numberColumns = 6;
                        %endif
                        for (var j = 0; j < numberColumns; j++)
                            $(api.column(j + offset).footer()).html(0);
                        return;
                    }

                    var didFormat = false;
                    // Remove the formatting to get integer data for summation
                    var intVal = function ( i ) {
                        return typeof i === 'string' ?
                            i.replace(/[' ']/g, '').replace(/[\$,]/g, '') * 1:
                            typeof i === 'number' ?
                                i : 0;
                    };
                    // Remove the formatting to get float data for summation
                    var floatVal = function ( i ) {
                        didFormat = true;
                        return typeof i === 'string' ?
                            i.replace(/['руб']/g, '').replace(/[' ']/g, '').replace(/[\$,]/g, '') * 1.0:
                            typeof i === 'number' ?
                                i : 0;
                    };

                    %if g.a_group == Account.Groups.ADV:
                        // Impressions
                        var impressions = api.column( 1  + offset).data().reduce( function (a, b) { return intVal(a) + intVal(b);});
                        // Clicks
                        var clicks = api.column( 2  + offset).data().reduce( function (a, b) { return intVal(a) + intVal(b); });
                        // Overlay
                        var overlay = api.column( 4  + offset).data().reduce( function (a, b) { return intVal(a) + intVal(b);});
                        // Overlay_cost
                        var overlay_cost = api.column( 5  + offset).data().reduce( function (a, b) { return intVal(a) + intVal(b);});
                        // EndCard
                        var endcard = api.column( 6  + offset).data().reduce( function (a, b) { return intVal(a) + intVal(b); });
                        // EndCard_cost
                        var endcard_cost = api.column( 7  + offset).data().reduce( function (a, b) { return intVal(a) + intVal(b);});
                        // Cost
                        var cost = api.column( 8  + offset).data().reduce( function (a, b) { return floatVal(a) + floatVal(b); });
                        //CTR
                        var ctr = 0;
                        try {
                            if (impressions != 0)
                                ctr = +clicks / +impressions * 100;
                        } catch(e) { }
                        // Impressions
                        $(api.column(1 + offset).footer()).html(impressions.toString().replace(/\B(?=(\d{3})+(?!\d))/g, " "));
                        // Clicks
                        $(api.column(2 + offset).footer()).html(clicks.toString().replace(/\B(?=(\d{3})+(?!\d))/g, " "));
                        // CTR
                        $(api.column(3 + offset).footer()).html(ctr.toFixed(2).toString().replace(/\B(?=(\d{3})+(?!\d))/g, " "));
                        // Overlay
                        $(api.column(4 + offset).footer()).html(overlay.toString().replace(/\B(?=(\d{3})+(?!\d))/g, " "));
                        // Overlay_cost
                        $(api.column(5 + offset).footer()).html(overlay_cost.toString().replace(/\B(?=(\d{3})+(?!\d))/g, " "));
                        // Endcard
                        $(api.column(6 + offset).footer()).html(endcard.toString().replace(/\B(?=(\d{3})+(?!\d))/g, " "));
                        // Endcard_cost
                        $(api.column(7 + offset).footer()).html(endcard_cost.toString().replace(/\B(?=(\d{3})+(?!\d))/g, " "));
                        // Cost
                        if(didFormat)
                            $(api.column(8 + offset).footer()).html(cost.toFixed(2).toString().replace(/\B(?=(\d{3})+(?!\d))/g, " ") + " руб");
                        else
                            $(api.column(8 + offset).footer()).html(cost + " руб");

                    %elif g.a_group == Account.Groups.PUB:
                        // Impressions
                        var impressions = api.column( 1 + offset ).data().reduce( function (a, b) { return intVal(a) + intVal(b);});
                        // Clicks
                        var clicks = api.column( 2  + offset).data().reduce( function (a, b) { return intVal(a) + intVal(b); });
                        // V4VC
                        var v4vc = api.column( 5 + offset ).data().reduce( function (a, b) {return intVal(a) + intVal(b);});
                        // Cost
                        var cost = api.column( 6 + offset ).data().reduce( function (a, b) { return floatVal(a) + floatVal(b); });
                        //CTR
                        var ctr = 0;
                        try {
                            if (impressions != 0)
                                ctr = +clicks / +impressions * 100;
                        } catch(e) { }
                        //CPM
                        var cpm = 0;
                        console.log('impressions ' + impressions);
                        console.log('cost ' + cost);
                        try {
                            if (impressions != 0)
                                cpm = +cost / +impressions * 1000;
                        } catch(e) { }
                        console.log('cpm ' + cpm)

                        // Impressions
                        $(api.column(1 + offset).footer()).html(impressions.toString().replace(/\B(?=(\d{3})+(?!\d))/g, " "));
                        // Clicks
                        $(api.column(2 + offset).footer()).html(clicks.toString().replace(/\B(?=(\d{3})+(?!\d))/g, " "));
                        // CTR
                        $(api.column(3 + offset).footer()).html(ctr.toFixed(2).toString().replace(/\B(?=(\d{3})+(?!\d))/g, " "));
                        // CPM
                        $(api.column(4 + offset).footer()).html(cpm.toFixed(2).toString().replace(/\B(?=(\d{3})+(?!\d))/g, " "));
                        // V4VC
                        $(api.column(5 + offset).footer()).html(v4vc.toString().replace(/\B(?=(\d{3})+(?!\d))/g, " "));
                        // Cost
                        if(!didFormat)
                            cost = +cost;
                        $(api.column(6 + offset).footer()).html(cost.toFixed(2).toString().replace(/\B(?=(\d{3})+(?!\d))/g, " ") + " руб");

                    %endif
                },
                bSort: false
            });

            jQuery('#stats_table_wrapper .dataTables_filter input').addClass("form-control input-medium"); // modify table search input
            jQuery('#stats_table_wrapper .dataTables_length select').addClass("form-control input-xsmall"); // modify table per page dropdown
            jQuery('#stats_table_wrapper .dataTables_length select').select2();

            // Parsing parameters for the table.
            var rows = $("#stats_table").dataTable().fnGetNodes();
            var date = $("#defaultrange input").val();
            %if g.a_group == Account.Groups.ADV:
                var gr = "adv";
            %elif g.a_group == Account.Groups.PUB:
                var gr = "pub";
            %endif

            for (var i = 0; i < rows.length; i++) {

                //  Make space separators when it's necessary.
                for (var j = 1; j < rows[i].cells.length; j++){
                    var cell = rows[i].cells[j].innerHTML;
                    try {
                        cell = JSON.parse(cell);
                        if (cell instanceof Object) continue;
                    } catch(e) { }

                    // Is float ?
                    if(! (cell % 1 === 0)){
                       rows[i].cells[j].innerHTML = cell.toFixed(2).toString().replace(/\B(?=(\d{3})+(?!\d))/g, " ");
                    } else
                       rows[i].cells[j].innerHTML = cell.toString().replace(/\B(?=(\d{3})+(?!\d))/g, " ");

                    // The money column.
                    if (j == rows[i].cells.length - 1)
                        rows[i].cells[j].innerHTML = cell.toFixed(2).toString().replace(/\B(?=(\d{3})+(?!\d))/g, " ") + " руб";
                }

                %if g.page == "days":
                    var _date = rows[i].cells[0].innerHTML;
                    rows[i].cells[0].innerHTML = '<a href="/stats/{gr}/hours/?date={displayed_d} - {displayed_d}" onmouseenter="updateLink(this)" >{displayed_d}</a>'
                        .replace(/{displayed_d}/g, _date)
                        .replace(/{gr}/g, gr);
                %elif g.page == "acc":
                    try {
                        var account = JSON.parse(rows[i].cells[0].innerHTML);
                        var group = meta.account[account][1]
                        %if g.a_group == Account.Groups.ADV:
                        rows[i].cells[0].innerHTML = '<a href="/stats/adv/cam/?date={date}&account={id}" onmouseenter="updateLink(this)" >{user}</a>'
                        %elif g.a_group == Account.Groups.PUB:
                        rows[i].cells[0].innerHTML = '<a href="/stats/pub/app/?date={date}&account={id}" onmouseenter="updateLink(this)" >{user}</a>'
                        %endif
                            .replace(/{date}/g, date)
                            .replace(/{id}/g, account)
                            .replace(/{user}/g, meta.account[account][0]);

                        rows[i].cells[1].innerHTML = '<a href="/stats/{gr}/{gr}/?date={date}&group={gr_id}" onmouseenter="updateLink(this)" >{gr_name}</a>'
                            .replace(/{gr}/g, gr)
                            .replace(/{date}/g, date)
                            .replace(/{gr_id}/g, group)
                            .replace(/{gr_name}/g, meta.group[group]);
                    } catch(e) { }
                %elif g.page == "cam":
                    try {
                        var campaign = JSON.parse(rows[i].cells[0].innerHTML);
                        var account = meta.campaign[campaign][1];
                        rows[i].cells[0].innerHTML = '<a href="/stats/adv/media/?date={date}&campaign={id}" onmouseenter="updateLink(this)" >{campaign}</a>'
                            .replace(/{date}/g, date)
                            .replace(/{id}/g, campaign)
                            .replace(/{campaign}/g, meta.campaign[campaign][0]);

                        rows[i].cells[1].innerHTML = '<a href="/stats/adv/cam/?date={date}&advertiser={acc_id}" onmouseenter="updateLink(this)" >{acc}</a>'
                            .replace(/{date}/g, date)
                            .replace(/{acc_id}/g, account)
                            .replace(/{acc}/g, meta.account[account]);
                    } catch(e) { }
                %elif g.page == "media":
                    try {
                        var media = JSON.parse(rows[i].cells[0].innerHTML);
                        var campaign = meta.media[media][1];
                        var account = meta.campaign[campaign][1];
                        rows[i].cells[0].innerHTML = meta.media[media][0];
                        rows[i].cells[1].innerHTML = '<a href="/stats/adv/media/?date={date}&campaign={cam_id}" onmouseenter="updateLink(this)" >{cam}</a>'
                            .replace(/{date}/g, date)
                            .replace(/{cam_id}/g, campaign)
                            .replace(/{cam}/g, meta.campaign[campaign][0]);

                        rows[i].cells[2].innerHTML = '<a href="/stats/adv/cam/?date={date}&advertiser={acc_id}" onmouseenter="updateLink(this)" >{acc}</a>'
                            .replace(/{date}/g, date)
                            .replace(/{acc_id}/g, account)
                            .replace(/{acc}/g, meta.account[account]);
                    } catch(e) { }
                %elif g.page == "app":
                    try {
                        var application = JSON.parse(rows[i].cells[0].innerHTML);
                        var account = meta.application[application][1];
                        rows[i].cells[0].innerHTML = '<a href="/stats/pub/unit/?date={date}&application={id}" onmouseenter="updateLink(this)" >{app}</a>'
                            .replace(/{date}/g, date)
                            .replace(/{id}/g, application)
                            .replace(/{app}/g, meta.application[application][0]);

                        rows[i].cells[1].innerHTML = '<a href="/stats/pub/app/?date={date}&publisher={acc_id}" onmouseenter="updateLink(this)" >{acc}</a>'
                            .replace(/{date}/g, date)
                            .replace(/{acc_id}/g, account)
                            .replace(/{acc}/g, meta.account[account]);

                    } catch(e) { }
                %elif g.page == "unit":
                    try {
                        var unit = JSON.parse(rows[i].cells[0].innerHTML);
                        var application = meta.unit[unit][1];
                        var account = meta.application[application][1];
                        rows[i].cells[0].innerHTML = meta.unit[unit][0];
                        rows[i].cells[1].innerHTML = '<a href="/stats/pub/unit/?date={date}&application={app_id}" onmouseenter="updateLink(this)" >{app}</a>'
                            .replace(/{date}/g, date)
                            .replace(/{app_id}/g, application)
                            .replace(/{app}/g, meta.application[application][0]);

                        rows[i].cells[2].innerHTML = '<a href="/stats/pub/app/?date={date}&publisher={acc_id}" onmouseenter="updateLink(this)" >{acc}</a>'
                            .replace(/{date}/g, date)
                            .replace(/{acc_id}/g, account)
                            .replace(/{acc}/g, meta.account[account]);
                    } catch(e) { }
                %elif g.page == "source":
                    try {
                        var source = JSON.parse(rows[i].cells[0].innerHTML);
                        var unit = meta.source[source][1];
                        var application = meta.unit[unit][1];
                        var account = meta.application[application][1];
                        rows[i].cells[0].innerHTML = meta.source[source][0];
                        rows[i].cells[1].innerHTML = meta.unit[unit][0];
                        rows[i].cells[1].innerHTML = '<a href="/stats/pub/source/?date={date}&unit={unit_id}" onmouseenter="updateLink(this)" >{unit}</a>'
                            .replace(/{date}/g, date)
                            .replace(/{unit_id}/g, unit)
                            .replace(/{unit}/g, meta.unit[unit][0]);
                        rows[i].cells[2].innerHTML = '<a href="/stats/pub/unit/?date={date}&application={app_id}" onmouseenter="updateLink(this)" >{app}</a>'
                            .replace(/{date}/g, date)
                            .replace(/{app_id}/g, application)
                            .replace(/{app}/g, meta.application[application][0]);

                        rows[i].cells[3].innerHTML = '<a href="/stats/pub/app/?date={date}&publisher={acc_id}" onmouseenter="updateLink(this)" >{acc}</a>'
                            .replace(/{date}/g, date)
                            .replace(/{acc_id}/g, account)
                            .replace(/{acc}/g, meta.account[account]);
                    } catch(e) { }
                %elif g.page == "country":
                    try {
                        var country = JSON.parse(rows[i].cells[0].innerHTML);
                        rows[i].cells[0].innerHTML = '<a href="/stats/{gr}/geo/region/?date={date}&country={country_id}" onmouseenter="updateLink(this)">{country}</a>'
                            .replace(/{gr}/g, gr)
                            .replace(/{date}/g, date)
                            .replace(/{country_id}/g, country)
                            .replace(/{country}/g, meta.country[country]);
                    } catch(e) { }
                %elif g.page == "region":
                    try {
                        var region = JSON.parse(rows[i].cells[0].innerHTML);
                        rows[i].cells[0].innerHTML = '<a href="/stats/{gr}/geo/city/?date={date}&region={region_id}" onmouseenter="updateLink(this)">{region}</a>'
                            .replace(/{gr}/g, gr)
                            .replace(/{date}/g, date)
                            .replace(/{region_id}/g, region)
                            .replace(/{region}/g, meta.region[region]);
                    } catch(e) { }
                %elif g.page == "city":
                    try {
                        var city = JSON.parse(rows[i].cells[0].innerHTML);
                        rows[i].cells[0].innerHTML = meta.city[city]
                    } catch(e) { }
                %endif
            }
        }
    }
}();
</script>

<script type="text/javascript">
    $(document).on("ready", function() {

        updateLink = function(self) {
            var baseUrl = $(self).attr('baseUrl');
            if (!baseUrl) {
                $(self).attr('baseUrl', $(self).attr('href'));
                baseUrl = $(self).attr('baseUrl');
            }
            console.log(baseUrl);
            $(self).attr('href', baseUrl + filterBox.getParametersHTTP());
        }

        account_group = {
            "${Account.Groups.NONE}": 'Без группы',
            "${Account.Groups.PUB}": 'Разработчик',
            "${Account.Groups.ADV}": 'Рекламодатель'
        }

        var base_url = "http://admin.vidiger.com/stats";
        %if g.a_group == Account.Groups.PUB:
            base_url += "/pub/"
            <% _name = g.page if g.page not in ('acc',) else 'pub' %>
            base_url += "${_name}/"
        %elif g.a_group == Account.Groups.ADV:
            base_url += "/adv/"
            <% _name = g.page if g.page not in ('acc',) else 'adv' %>
            base_url += "${_name}/"
        %endif

        $('a#copy_stats').zclip({
            path: '/global/static/plugins/zclip/ZeroClipboard.swf',
            copy: function(){
                var url = "";
                var date = $("#defaultrange input").val();
                return base_url + "?date=" + date + filterBox.getParametersHTTP();
            }
        });

        Index.init();
        // Init filters.
        filterBox.extraUrlParameters = {'account_group': "${g.a_group}"};
        %for select in g.selects:
            <%
                _name = 'account' if select['name'] in ('advertiser', 'publisher', ) else select['name']
                placeholder = getattr(g, str(_name) + '_name', select['header'])
            %>
            %if select.get("data"):
                filterBox.push({
                    id: "${select['id']}",
                    name: "${select['name']}",
                    header: "${select['header']}",
                    placeholder: "${placeholder}",
                    value: "${getattr(g, str(select['name']), '')}",
                    %if select.get("name") == "zone_size":
                    format: function(obj, value) {
                        try {
                            var size = value.split("x");
                            return "width:" + size[0] + ",height:" + size[1];
                        } catch(e) { return "width:,height:"; }
                    },
                    %endif
                    data: [
                        {"id": "", "text": "Все"},
                        %for i, (k, v) in enumerate(select.get('data').items()):
                            %if i != len(select.get('data')) - 1:
                            {"id": "${k}", "text": "${v}"},
                            %else:
                            {"id": "${k}", "text": "${v}"}
                            %endif
                        %endfor
                    ]
                });
            %else:
                filterBox.push({
                    id: "${select['id']}",
                    name: "${select['name']}",
                    header: "${select['header']}",
                    placeholder: "${placeholder}",
                    value: "${getattr(g, str(select['name']), '')}",
                    url: "/stats/ajax/select"
                });
            %endif
        %endfor
        
        Index.loadData();

        //Remove sorting from first row in table header
        $("#stats_table thead tr .sorting_asc").removeClass("sorting_asc");

        // Sorting the table.
        sort_direction = false;
        sort_column = null;
        $("#stats_table thead").on("click", function(event){
            // Determine column number.
            ths = $("#stats_table thead th");
            for (var i = 0; i < ths.length; i++)
                if (event.target == ths[i])
                    break;
            sort_column = i;
            console.log(sort_column);

            // Hack in advertiser table.
            %if g.a_group == Account.Groups.ADV:
                switch(sort_column){
                    case 8:
                        sort_column = 10;
                        break;
                    case 9:
                        sort_column = 6;
                        break;
                    case 10:
                        sort_column = 7;
                        break;
                    case 12:
                        sort_column = 8;
                        break;
                    case 13:
                        sort_column = 9;
                        break;
                }
            %endif

            if (sort_column == null)
                return;

            var intVal = function (i) {
            return typeof i === 'string' ?
                i.replace(/[' ']/g, '').replace(/[\$,]/g, '').replace(/['руб']/g, '') * 1:
                typeof i === 'number' ?
                    i : 0;
            };

            // Make a sorted array for the chosen column.
            var sorted_elems = [];
            var trs = [], _trs = $("#stats_table tbody tr");
            for(var j = 0; j < _trs.length; j++){
                trs.push(_trs[j].children[sort_column]);
            }

            for (var j = 0; j < trs.length; j++){
                if (sort_column != 0)
                    sorted_elems.push({key: intVal(trs[j].innerHTML), value: trs[j].parentNode});
                else // For strings.
                    sorted_elems.push({key: trs[j].innerHTML, value: trs[j].parentNode});
            }

            if (sort_column != 0)
                sorted_elems.sort(function(a, b){return a.key - b.key;});
            else // For strings
                sorted_elems.sort(function(a, b){return 1 ? a > b : a < b ? -1 : 0;});

            if (sort_direction)
                sorted_elems.reverse();

            sort_direction = !sort_direction;

            // Sorting the rows of the main table.
            // Clear old values.
            rows = $("#stats_table tbody tr");
            // Create a buffer array.
            var buff = [];
            for (var k = 0; k < rows.length; k++)
                buff.push(sorted_elems[k].value.innerHTML);

            for (var k = 0; k < rows.length; k++)
                rows[k].innerHTML = buff[k];

        });
    });
</script>