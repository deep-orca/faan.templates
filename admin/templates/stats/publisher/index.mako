<%namespace name="bcrumb" file="/layout/bcrumb.mako" />
<%namespace name="fabutton" file="/layout/fa-button.mako" />
<%inherit file="/stats/index.mako" />

<%def name="title()">Статистика</%def>
<%def name="description()"></%def>

${ bcrumb.h(self) }
${ bcrumb.bc([ { 'name' : u'Статистика', 'href' : '/stats/', 'icon' : 'bullhorn' }]) }

<%def name="show_table(id)">
 <!--START TABLE-->
 <table class="table table-striped table-bordered table-hover" id="${id}">
 <thead>
    ${self.show_table_header()}
 </thead>

  <tfoot>
    <tr>
        <th>Всего:</th>
        %if g.page in ("acc", "app",):
        <th></th>
        %elif g.page in ("unit",):
        <th></th>
        <th></th>
        %elif g.page in ("source",):
        <th></th>
        <th></th>
        <th></th>
        %endif
        <th></th>
        <th></th>
        <th></th>
        <th></th>
        <th></th>
        <th></th>
    </tr>
  </tfoot>

 <tbody>

${caller.body()}

 </tbody>
 </table>
 <!--END TABLE-->


</%def>

<%def name="show_table_header()">

 <tr>
    <th>
    %if g.page in ("days", "hours"):
       Дата
    %elif g.page == "acc":
       Разработчик
    %elif g.page == "app":
       Приложение
    %elif g.page == "unit":
       Блок
    %elif g.page == "source":
       Источник
    %elif g.page == "country":
        Страна
    %elif g.page == "region":
        Регион
    %elif g.page == "city":
        Город
    %else:
        Unknown
    %endif
    </th>

    %if g.page == "acc":
       <th>Группа</th>
    %elif g.page == "app":
       <th>Аккаунт</th>
    %elif g.page == "unit":
       <th>Приложение</th>
       <th>Аккаунт</th>
    %elif g.page == "source":
       <th>Блок</th>
       <th>Приложение</th>
       <th>Аккаунт</th>
    %endif

    <th>Показы</th>
    <th>Клики</th>
    <th>CTR</th>
    <th>CPM</th>
    <th>V4VC</th>
    <th><small><i class="fa fa-dollar"></i></small>всего</th>
 </tr>

</%def>



