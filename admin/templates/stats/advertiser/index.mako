<%namespace name="bcrumb" file="/layout/bcrumb.mako" />
<%namespace name="fabutton" file="/layout/fa-button.mako" />
<%inherit file="/stats/index.mako" />

<%def name="title()">Статистика</%def>
<%def name="description()"></%def>

${ bcrumb.h(self) }
${ bcrumb.bc([ { 'name' : u'Статистика', 'href' : '/stats/', 'icon' : 'bullhorn' }]) }

<%def name="show_table(id)">
 <!--START TABLE-->
 <table class="table table-striped table-bordered table-hover" id="${id}">
 <thead>
    ${self.show_table_header()}
 </thead>

<tfoot>

      <tr>
         <th>Всего: </th>
         %if g.page in ("acc", "cam",):
         <th></th>
         %elif g.page in ("media",):
         <th></th>
         <th></th>
         %endif
         <th></th>
         <th></th>
         <th></th>
         <th></th>
         <th></th>
         <th></th>
         <th></th>
         <th></th>
     </tr>

</tfoot>

 <tbody>

${caller.body()}

 </tbody>
 </table>
 <!--END TABLE-->

</%def>

<%def name="show_table_header()">

 <tr>
      <tr role="row">
         <th colspan="1" rowspan="2">
         %if g.page in ("days", "hours"):
            Дата
         %elif g.page == "acc":
            Рекламодатель
         %elif g.page == "cam":
            Кампания
         %elif g.page == "media":
            Креатив
         %elif g.page == "country":
            Страна
         %elif g.page == "region":
            Регион
         %elif g.page == "city":
            Город
         %else:
            Unknown
         %endif
         </th>

         %if g.page == "acc":
            <th colspan="1" rowspan="2">Группа</th>
         %elif g.page == "cam":
            <th colspan="1" rowspan="2">Аккаунт</th>
         %elif g.page == "media":
            <th colspan="1" rowspan="2">Кампания</th>
            <th colspan="1" rowspan="2">Аккаунт</th>
         %endif

         <th colspan="1" rowspan="2">Показы</th>
         <th colspan="1" rowspan="2">Клики</th>
         <th colspan="1" rowspan="2">CTR</th>
         <th colspan="2" rowspan="1">Оверлей</th>
         <th colspan="2" rowspan="1">Эндкарт</th>
         <th colspan="1" rowspan="2">Деньги</th>
     </tr>
     <tr role="row">
         <th colspan="1" rowspan="1"><i class="fa fa-eye"></i></th>
         <th colspan="1" rowspan="1"><i class="fa fa-rub"></i></th>
         <th colspan="1" rowspan="1"><i class="fa fa-eye"></i></th>
         <th colspan="1" rowspan="1"><i class="fa fa-rub"></i></th>
     </tr>
 </tr>

</%def>

