## -*- coding: utf-8 -*-
<%namespace name="bcrumb" file="/layout/bcrumb.mako" />

<%inherit file="/layout/main.mako" />

<%def name="title()">Администратор</%def>
<%def name="description()">Настройки</%def>

<%def name="page_css()">
    <link rel="stylesheet" type="text/css" href="/global/static/plugins/fancytree/skin-lion/ui.fancytree.min.css"/>
    <style>
        .modal-loading {
            display: none;
            position: absolute;
            top: 0;
            left: 0;
            width: 100%;
            height: 100%;
            background: rgba(0, 0, 0, 0.25);
            z-index: 1;
        }

        .modal-loading .anchor {
            position: absolute;
            top: 50%;
            left: 50%;
            width: 1px;
            height: 1px;
            overflow: visible;
        }

        .modal-loading .anchor .fa-spin {
            position: relative;
            top: -8px;
            left: -6px;
            color: white;
        }

        .error-message {
            display: none;
            padding: 3px;
            font-weight: bold;
        }
    </style>
</%def>

<%def name="page_js()">
    <script type="text/javascript" src="/global/static/plugins/moment-with-langs.min.js"></script>
    <script type="text/javascript" src="/global/static/plugins/fancytree/jquery.fancytree.min.js"></script>
    <script type="text/javascript">
        $.fn.serializeObject = function () {
            var o = {};
            var a = this.serializeArray();
            $.each(a, function () {
                if (this.value != '') {
                    if (o[this.name] !== undefined) {
                        if (!o[this.name].push) {
                            o[this.name] = [o[this.name]];
                        }
                        o[this.name].push(this.value || '');
                    } else {
                        o[this.name] = this.value || '';
                    }
                }
            });
            return o;
        };

        $(document).on("ready", function () {
            var el = $("#geo_tree"),
                modal = $(".modal"),
                modalLoader = modal.find(".modal-loading"),
                currentNode = null;

            var onNodeCreate = function (evt, data) {
                var el = $(data.node.li).find(".fancytree-title");
                var button = $("<i title='Нажмите сюда, чтобы изменить коэффицент' class='fa fa-pencil' style='cursor:pointer;'></i>");
                var span = $("<span class='multiplier' style='margin-left:10px;'></span>");
                span.text("[" + data.node.data.multiplier.toFixed(2) + "]");
                button.on("click", function () {
                    currentNode = data.node;
                    with (modal) {
                        modal();
                        find("span.geo").text("для \"" + currentNode.title + "\"");
                        find("input#multiplier").val(currentNode.data.multiplier.toFixed(2)).parent().parent().removeClass('has-error');
                        find("input#geo_id").val(currentNode.data.id);
                        find("input#geo_type").val(currentNode.data.type);
                        find(".error-message").hide();
                    }

                });
                el.before(button);
                el.append(span);
            };

            var onNodeUpdate = function (node) {
                var el = $(node.li).find(".fancytree-title");
                el.find("span.multiplier").text("[" + node.data.multiplier.toFixed(2) + "]");
            };

            modal.find("button#btn-save").on("click", function () {
                var data = modal.find("form").serializeObject();
                $.ajax({
                    url: "/settings/multiplier",
                    type: "POST",
                    data: modal.find("form").serializeObject(),
                    beforeSend: function() {
                        modalLoader.show();
                    },
                    success: function (response) {
                        currentNode.data.multiplier = +(modal.find("input#multiplier").val());
                        onNodeUpdate(currentNode);
                        modalLoader.hide();
                        modal.modal('hide');
                    },
                    error: function (response, status, message) {
                        var errors = response.responseJSON.errors;
                        modalLoader.hide();
                        $.each(errors, function (field, error) {
                            var formGroup = modal.find("input[name='" + field + "']").parent().parent();
                            var errorMessage = formGroup.find(".error-message");
                            errorMessage.html(error.join("<br/>")).show();
                            formGroup.addClass("has-error");
                        });
                    }
                })
            });

            el.fancytree({
                checkbox: false,
                selectMode: 1,
                keyboard: false,
                clickFolderMode: 4,
                icons: false,
                source: ${g.nodes},
                createNode: onNodeCreate
            });
        });
    </script>
</%def>

${ bcrumb.h(self) }
${ bcrumb.bc(
[
{
'name': u'Администратор',
'href': '/',
'icon': 'bullhorn'
},
{
'name': u'Настройки',
'href': '/settings/',
'icon': 'gear'
}
]
)}

<div class="row">
    <div class="col-md-6">
        <div class="portlet">
            <div class="portlet-title">
                <div class="caption">Коэффиценты</div>
            </div>
            <div class="portlet-body">
                <div id="geo_tree"></div>
            </div>
        </div>
    </div>


    <div class="modal fade" data-backdrop="static">

        <div class="modal-dialog modal-sm">
            <div class="modal-loading">
                <div class="anchor">
                    <i class="fa fa-spin fa-circle-o-notch"></i>
                </div>
            </div>
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span
                            aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                    <h4 class="modal-title">Изменение коэффицента <span class="geo"></span></h4>
                </div>
                <div class="modal-body">
                    <form action="">
                        <div class="form-group">
                            <div class="input-group">
                                <label for="multiplier" class="input-group-addon">Коэффицент</label>
                                <input id="multiplier" name="multiplier" class="form-control" type="text" value=""/>
                            </div>
                            <div class="error-message text-danger"><!-- Error message --></div>
                        </div>
                        <input id="geo_type" name="geo_type" type="hidden"/>
                        <input id="geo_id" name="geo_id" type="hidden"/>
                        ${form.csrf_token}
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
                    <button type="button" id="btn-save" class="btn btn-primary">Сохранить</button>
                </div>
            </div>
        </div>
    </div>
</div>