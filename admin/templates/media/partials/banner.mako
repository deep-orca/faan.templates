## coding=utf-8
<%
    from faan.core.model.adv.media import Media
    form = forms.get('banner')
%>
<form role="form" class="form form-horizontal banner" action="" method="POST" data-type="${g.media.Type.BANNER}">
    <!-- CSRF -->
        ${form.csrf_token}

    <!-- Type -->
    <input type="hidden" name="type" value="${g.media.Type.BANNER}"/>

    <!-- Name -->
    <div class="form-group ${'has-error' if form.name.errors else ''}">
        ${form.name.label(class_="col-md-3 control-label")}
        <div class="col-md-9">
            ${form.name(class_="form-control")}
            <span class="text-danger">${', '.join(form.name.errors)}</span>
        </div>

    </div>

    <!-- Banner size -->
##    <div class="form-group ${'has-error' if form.banner_size.errors else ''}">
##        ${form.banner_size.label(class_="col-md-3 control-label")}
##        <div class="col-md-9">
##            ${form.banner_size(class_="form-control")}
##            <span class="text-danger">${', '.join(form.banner_size.errors)}</span>
##        </div>
##    </div>

    <div class="form-group ${'has-error' if form.width.errors or form.height.errors else ''}">
        <label class="col-md-3 control-label">Размер</label>

        <div class="col-md-9">
            <select class="form-control" name="" id="banner_size">
                %for size in form.SIZES:
                    %if size[0].custom and g.media.id:
                        <option value="${g.media.width};${g.media.height}" data-is-custom="True">${size[1]}</option>
                    %else:
                        <option value="${size[0].width};${size[0].height}" data-is-custom="${size[0].custom}">${size[1]}</option>
                    %endif

                %endfor
            </select>

            <span class="text-danger">${', '.join(form.width.errors+form.height.errors)}</span>
        </div>
    </div>

    <div class="form-group size-manual hidden ${'has-error' if form.width.errors or form.height.errors else ''}">
        <div class="col-md-3 col-md-offset-3">
            <div class="input-group">
                ${form.width(class_="form-control text-center", placeholder=u"Ш")}
                <span class="input-group-addon">&times;</span>
                ${form.height(class_="form-control text-center", placeholder=u"В")}
            </div>
        </div>
    </div>

    <!-- Banner image -->
    <div class="form-group ${'has-error' if form.banner.errors else ''}">
        ${form.banner.label(class_="col-md-3 control-label")}
        <div class="col-md-9">
            ${form.banner(class_="form-control")}
            <input type="file" name="banner_data" id="banner_data">
            <span class="text-danger">${', '.join(form.banner.errors)}</span>

            <div id="banner-preview">
                <img class="img-thumbnail" src="${g.media.banner if g.media.banner != '' else ''}"
                     alt="" ${'' if g.media.banner != '' else 'style="display: none;"'}/>
            </div>
        </div>
    </div>

    <div class="form-group ${'has-error' if form.session_limit.errors else ''}">
        ${form.session_limit.label(class_="col-md-3 control-label")}
        <div class="col-md-9">
            ${form.session_limit(class_="form-control")}
            <span class="help-block">
                Сколько раз баннер может быть показан пользователю за один запуск приложения. <br>0 - без ограничений.
            </span>
            <span class="text-danger">${', '.join(form.session_limit.errors)}</span>
        </div>
    </div>

    <div class="form-group ${'has-error' if form.daily_limit.errors else ''}">
        ${form.daily_limit.label(class_="col-md-3 control-label")}
        <div class="col-md-9">
            ${form.daily_limit(class_="form-control")}
            <span class="help-block">
                Сколько раз баннер может быть показан пользователю всего за сутки. <br>0 - без ограничений.
            </span>
            <span class="text-danger">${', '.join(form.daily_limit.errors)}</span>
        </div>
    </div>

    <!-- Banner click action -->
    <div class="form-group ${'has-error' if form.action_click.errors else ''}">
        ${form.action_click.label(class_="col-md-3 control-label")}
        <div class="col-md-9">
            ${form.action_click(class_="form-control")}
            <span class="text-danger">${', '.join(form.action_click.errors)}</span>
        </div>
    </div>

    <!-- Phone number (Call/SMS) -->
    <div class="form-group action call sms ${'has-error' if form.phone_number.errors else ''}"
         style="display: none;">
        ${form.phone_number.label(class_="col-md-3 control-label")}
        <div class="col-md-9">
            <div class="input-group">
                <span class="input-group-addon">+</span>
                ${form.phone_number(class_="form-control")}
            </div>
            <span class="text-danger">${', '.join(form.phone_number.errors)}</span>
        </div>
    </div>

    <!-- Video select -->
    <div class="form-group action video select" style="display: none;">
        <div class="col-md-offset-3 col-md-9">
            <div class="btn-group btn-group-justified">
                <a class="btn btn-default video new">Новое видео</a>
                <a class="btn btn-default video existing">Существующее видео</a>
            </div>
        </div>
    </div>

    <!-- Vdeo new -->
    <div class="form-group action video new ${'has-error' if form.video.errors else ''}"
         style="display: none;">
        ${form.video.label(class_="col-md-3 control-label")}
        <div class="col-md-9">
            ${form.video(class_="form-control")}
            <input type="file" name="video_data" id="video_data">
            <span class="text-danger">${', '.join(form.video.errors)}</span>

            <div id="video-preview">
                <div id="player_banner"></div>
            </div>
        </div>
    </div>

    <div class="form-group action video new ${'has-error' if form.action_end.errors else ''}"
         style="display: none;">
        ${form.action_end.label(class_="col-md-3 control-label")}
        <div class="col-md-9">
            <div class="btn-group btn-group-justified action_end_switch">
                <a data-value="${Media.EndActions.NONE}"
                   class="btn btn-default ${'active' if form.action_end.data == Media.EndActions.NONE else ''}">Ничего</a>
                <a data-value="${Media.EndActions.ENDCARD}"
                   class="btn btn-default ${'active' if form.action_end.data == Media.EndActions.ENDCARD else ''}">Показать
                    лендинг</a>
            </div>
            ${form.action_end(class_="list-inline")}
            <span class="text-danger">${', '.join(form.action_end.errors)}</span>
        </div>
    </div>

    <div class="form-group action video endcard ${'has-error' if form.endcard.errors else ''}"
         style="display: none;">
        ${form.endcard.label(class_="col-md-3 control-label")}
        <div class="col-md-9">
            ${form.endcard(class_="form-control")}
            <input type="file" name="endcard_data" id="endcard_data">
            <span class="text-danger">${', '.join(form.endcard.errors)}</span>

            <div id="endcard-preview">
                <img class="img-thumbnail" src="${g.media.endcard if g.media.endcard != '' else ''}"
                     alt="" ${'' if g.media.endcard != '' else 'style="display: none;"'}/>
            </div>
        </div>
    </div>

    <div class="form-group action video new ${'has-error' if form.overlay.errors else ''}"
         style="display: none;">
        ${form.overlay.label(class_="col-md-3 control-label")}
        <div class="col-md-9">
            ${form.overlay(class_="form-control")}
            <input type="file" name="overlay_data" id="overlay_data">
            <span class="text-danger">${', '.join(form.overlay.errors)}</span>

            <div id="overlay-preview">
                <img class="img-thumbnail" src="${g.media.overlay if g.media.overlay != '' else ''}"
                     alt="" style="${'' if g.media.overlay != '' else 'display: none;'}"/>
            </div>
        </div>
    </div>

    <!-- URL (URL) -->
    <div class="form-group action url ${'has-error' if form.uri.errors else ''}" style="display: none;">
        ${form.uri.label(class_="col-md-3 control-label")}
        <div class="col-md-9">
            ${form.uri(class_="form-control")}
            <span class="text-danger">${', '.join(form.uri.errors)}</span>
        </div>
    </div>

    <!-- URL target -->
    <div class="form-group action url ${'has-error' if form.uri_target.errors else ''}"
         style="display: none;">
        ${form.uri_target.label(class_="col-md-3 control-label")}
        <div class="col-md-9">
            <div class="uri_target_switch btn-group btn-group-justified">
                <a data-value="${Media.UriTargets.INAPP}"
                   class="btn btn-default ${'active' if forms.get('video').uri_target.data == Media.UriTargets.INAPP else ''}">Нет</a>
                <a data-value="${Media.UriTargets.STANDALONE}"
                   class="btn btn-default ${'active' if forms.get('video').uri_target.data == Media.UriTargets.STANDALONE else ''}">Да</a>
            </div>
            ${form.uri_target(class_="list-inline")}
            <span class="text-danger">${', '.join(form.uri_target.errors)}</span>
        </div>
    </div>

    <div class="form-group action video new ${'has-error' if form.closable.errors else ''}"
         style="display: none;">
        ${form.closable.label(class_="col-md-3 control-label")}
        <div class="col-md-9">
            ${form.closable(class_="form-control closable")}
            <span class="help-block">
                Время, после которого у пользователя появится возможность пропустить видео
            </span>
            <span class="text-danger">${', '.join(form.closable.errors)}</span>
        </div>
    </div>


    <div class="form-group action video existing ${'has-error' if form.existing_video.errors else ''}"
         style="display: none;">
        ${form.existing_video.label(class_="col-md-3 control-label")}
        <div class="col-md-9">
            <select name="existing_video" id="existing_video" class="form-control">
                <option value=""></option>
                %for campaign in g.videos:
                    <optgroup label="${campaign.get('name')}">
                        %for video in campaign.get('media', []):
                            <option value="${video.get('id')}">${video.get('name')}</option>
                        %endfor
                    </optgroup>
                %endfor
            </select>
            <span class="text-danger">${', '.join(form.existing_video.errors)}</span>
        </div>
    </div>

    <hr/>

    <div class="form-group">
        <div class="col-md-12 text-center">
            <button type="submit" data-form="banner" class="btn btn-success btn-tall btn-wide">
                <i class="fa fa-fw fa-save"></i> Сохранить
            </button>
        </div>
    </div>
</form>