<% 
    from faan.core.model.adv.media import Media 
    form = forms.get('video')
%>
<form role="form" class="form form-horizontal video" action="" method="POST" data-type="${Media.Type.VIDEO}">
    ${form.csrf_token}
    <input type="hidden" name="type" value="${Media.Type.VIDEO}"/>

    <div class="form-group ${'has-error' if form.name.errors else ''}">
        ${form.name.label(class_="col-md-3 control-label")}
        <div class="col-md-9">
            ${form.name(class_="form-control")}
            <span class="text-danger">${', '.join(form.name.errors)}</span>
        </div>
    </div>

    <div class="form-group has-feedback ${'has-error' if form.video.errors else ''}">
        ${form.video.label(class_="col-md-3 control-label")}
        <div class="col-md-9">
            ${form.video(class_="form-control")}
            <input type="file" name="video_data" id="video_data">
            <span class="text-danger">${', '.join(form.video.errors)}</span>

            <div id="video-preview">
                <div id="player_video"></div>
            </div>
        </div>
    </div>

    <div class="form-group ${'has-error' if form.action_end.errors else ''}">
        ${form.action_end.label(class_="col-md-3 control-label")}
        <div class="col-md-9">
            <div class="btn-group btn-group-justified action_end_switch">
                <a data-value="${Media.EndActions.NONE}"
                   class="btn btn-default ${'active' if form.action_end.data == Media.EndActions.NONE else ''}">Ничего</a>
                <a data-value="${Media.EndActions.ENDCARD}"
                   class="btn btn-default ${'active' if form.action_end.data == Media.EndActions.ENDCARD else ''}">Показать
                    лендинг</a>
            </div>
            ${form.action_end}
            <span class="text-danger">${', '.join(form.action_end.errors)}</span>
        </div>
    </div>

    <div class="form-group action video endcard ${'has-error' if form.endcard.errors else ''}"
         style="display: none;">
        ${form.endcard.label(class_="col-md-3 control-label")}
        <div class="col-md-9">
            ${form.endcard(class_="form-control")}
            <input type="file" name="endcard_data" id="endcard_data">
            <span class="text-danger">${', '.join(form.endcard.errors)}</span>

            <div id="endcard-preview">
                <img class="img-thumbnail" src="" alt="" style="display: none;"/>
            </div>
        </div>
    </div>

    <div class="form-group ${'has-error' if form.overlay.errors else ''}">
        ${form.overlay.label(class_="col-md-3 control-label")}
        <div class="col-md-9">
            ${form.overlay(class_="form-control")}
            <input type="file" name="overlay_data" id="overlay_data">
            <span class="text-danger">${', '.join(form.overlay.errors)}</span>

            <div id="overlay-preview">
                <img class="img-thumbnail" src="" alt="" style="display: none;"/>
            </div>
        </div>
    </div>

    <div class="form-group ${'has-error' if form.closable.errors else ''}">
        ${form.closable.label(class_="col-md-3 control-label")}
        <div class="col-md-9">
            ${form.closable(class_="form-control closable")}
            <span class="help-block">
                Время, после которого у пользователя появится возможность пропустить видео
            </span>
            <span class="text-danger">${', '.join(form.closable.errors)}</span>
        </div>
    </div>

    <div class="form-group ${'has-error' if form.session_limit.errors else ''}">
        ${form.session_limit.label(class_="col-md-3 control-label")}
        <div class="col-md-9">
            ${form.session_limit(class_="form-control")}
            <span class="help-block">
                Сколько раз видео может быть показано пользователю за один запуск приложения. <br>0 - без ограничений.
            </span>
            <span class="text-danger">${', '.join(form.session_limit.errors)}</span>
        </div>
    </div>

    <div class="form-group ${'has-error' if form.daily_limit.errors else ''}">
        ${form.daily_limit.label(class_="col-md-3 control-label")}
        <div class="col-md-9">
            ${form.daily_limit(class_="form-control")}
            <span class="help-block">
                Сколько раз видео может быть показано пользователю всего за сутки. <br>0 - без ограничений.
            </span>
            <span class="text-danger">${', '.join(form.daily_limit.errors)}</span>
        </div>
    </div>

    <div class="form-group ${'has-error' if form.action_click.errors else ''}">
        ${form.action_click.label(class_="col-md-3 control-label")}
        <div class="col-md-9">
            ${form.action_click(class_="form-control")}
            <span class="text-danger">${', '.join(form.action_click.errors)}</span>
        </div>
    </div>

    <div class="form-group action url ${'has-error' if form.uri.errors else ''}"
         style="display: none;">
        ${form.uri.label(class_="col-md-3 control-label")}
        <div class="col-md-9">
            ${form.uri(class_="form-control")}
            <span class="text-danger">${', '.join(form.uri.errors)}</span>
        </div>
    </div>

    <div class="form-group action url ${'has-error' if form.uri_target.errors else ''}"
         style="display: none;">
        ${form.uri_target.label(class_="col-md-3 control-label")}
        <div class="col-md-9">
            <div class="uri_target_switch btn-group btn-group-justified">
                <a data-value="${Media.UriTargets.INAPP}"
                   class="btn btn-default ${'active' if form.uri_target.data == Media.UriTargets.INAPP else ''}">Нет</a>
                <a data-value="${Media.UriTargets.STANDALONE}"
                   class="btn btn-default ${'active' if form.uri_target.data == Media.UriTargets.STANDALONE else ''}">Да</a>
            </div>
            ${form.uri_target}
            <span class="text-danger">${', '.join(form.uri_target.errors)}</span>
        </div>
    </div>

    <!-- Phone number (Call/SMS) -->
    <div class="form-group action call sms ${'has-error' if forms.get('banner').phone_number.errors else ''}"
         style="display: none;">
        ${forms.get('banner').phone_number.label(class_="col-md-3 control-label")}
        <div class="col-md-9">
            <div class="input-group">
                <span class="input-group-addon">+</span>
                ${forms.get('banner').phone_number(class_="form-control")}
            </div>
            <span class="text-danger">${', '.join(forms.get('banner').phone_number.errors)}</span>
        </div>
    </div>

    <hr/>

    <div class="form-group">
        <div class="col-md-12 text-center">
            <button type="submit" data-form="video" class="btn btn-success btn-tall btn-wide">
                <i class="fa fa-fw fa-save"></i> Сохранить
            </button>
        </div>
    </div>
</form>