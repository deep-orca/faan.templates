## coding=utf-8
<%
    from faan.core.model.adv.media import Media
    form = forms.get('mraid')
%>
<form role="form" class="form  form-horizontal mraid" action="" method="POST" data-type="${Media.Type.MRAID}">
    <div id="mraid_switch" class="row">
        <div class="col-md-offset-3 col-md-6 text-center">
            <div>
                <a id="mraid_order" class="btn btn-success btn-lg btn-block mraid_order">Заказ креатива</a>
            </div>
            <h4>или</h4>
            <div>
                <a id="mraid_manual" class="btn btn-default manual">Добавить свой</a>
            </div>
        </div>
    </div>

    <div class="manual" style="display: none;">
        ${form.csrf_token}
        <input type="hidden" name="type" value="${Media.Type.MRAID}"/>

        <div class="form-group ${'has-error' if form.name.errors else ''}">
            ${form.name.label(class_="col-md-3 control-label")}

            <div class="col-md-9">
                ${form.name(class_="form-control")}
                <span class="text-danger">${', '.join(form.name.errors)}</span>
            </div>
        </div>

        <!-- Banner size -->
##        <div class="form-group ${'has-error' if form.banner_size.errors else ''}">
##            ${form.banner_size.label(class_="col-md-3 control-label")}
##            <div class="col-md-9">
##                ${form.banner_size(class_="form-control")}
##                <span class="text-danger">${', '.join(form.banner_size.errors)}</span>
##            </div>
##        </div>

        <div class="form-group ${'has-error' if form.width.errors or form.height.errors else ''}">
            <label class="col-md-3 control-label">Размер</label>
            <div class="col-md-9">
                <select class="form-control" name="" id="banner_size">
                    %for size in form.SIZES:
                        %if size[0].custom and g.media.id:
                            <option value="${g.media.width};${g.media.height}" data-is-custom="True">${size[1]}</option>
                        %else:
                            <option value="${size[0].width};${size[0].height}"
                                    data-is-custom="${size[0].custom}">${size[1]}</option>
                        %endif
                    %endfor
                </select>
                <span class="text-danger">${', '.join(form.width.errors+form.height.errors)}</span>
            </div>
        </div>

        <div class="form-group size-manual hidden ${'has-error' if form.width.errors or form.height.errors else ''}">
            <div class="col-md-3 col-md-offset-3">
                <div class="input-group">
                    ${form.width(class_="form-control text-center", placeholder=u"Ш")}
                    <span class="input-group-addon">&times;</span>
                    ${form.height(class_="form-control text-center", placeholder=u"В")}
                </div>
            </div>
        </div>

        <div class="form-group ${'has-error' if form.session_limit.errors else ''}">
            ${form.session_limit.label(class_="col-md-3 control-label")}
            <div class="col-md-9">
                ${form.session_limit(class_="form-control")}
                <span class="help-block">
                Сколько раз креатив может быть показан пользователю за один запуск приложения. <br>0 - без ограничений.
            </span>
                <span class="text-danger">${', '.join(form.session_limit.errors)}</span>
            </div>
        </div>

        <div class="form-group ${'has-error' if form.daily_limit.errors else ''}">
            ${form.daily_limit.label(class_="col-md-3 control-label")}
            <div class="col-md-9">
                ${form.daily_limit(class_="form-control")}
                <span class="help-block">
                Сколько раз креатив может быть показан пользователю всего за сутки. <br>0 - без ограничений.
            </span>
                <span class="text-danger">${', '.join(form.daily_limit.errors)}</span>
            </div>
        </div>

        <div class="form-group ${'has-error' if form.mraid_type.errors else ''}">
            ${form.mraid_type.label(class_="col-md-3 control-label")}
            <div class="col-md-9">
                <div class="btn-group btn-group-justified mraid_type_switch">
                    <a data-value="${Media.MRAID_Type.FILE}"
                       class="btn btn-default ${'active' if form.mraid_type.data == Media.MRAID_Type.FILE else ''}">ZIP</a>
                    <a data-value="${Media.MRAID_Type.URL}"
                       class="btn btn-default ${'active' if form.mraid_type.data == Media.MRAID_Type.URL else ''}">URL</a>
                    <a data-value="${Media.MRAID_Type.HTML}"
                       class="btn btn-default ${'active' if form.mraid_type.data == Media.MRAID_Type.HTML else ''}">HTML</a>
                </div>
                ${form.mraid_type}
                <span class="text-danger">${', '.join(form.mraid_type.errors)}</span>
            </div>
        </div>

        <div class="form-group ${'has-error' if form.mraid_file.errors else ''}" data-if-mraid-type="${Media.MRAID_Type.FILE}">
            ${form.mraid_file.label(class_="col-md-3 control-label")}
            <div class="col-md-9">
                ${form.mraid_file(class_="form-control")}
                <input type="file" name="mraid_data" id="mraid_data">
                <span class="text-danger">${', '.join(form.mraid_file.errors)}</span>
            </div>
        </div>

        <div class="form-group ${'has-error' if form.mraid_url.errors else ''}" data-if-mraid-type="${Media.MRAID_Type.URL}">
            ${form.mraid_url.label(class_="col-md-3 control-label")}
            <div class="col-md-9">
                ${form.mraid_url(class_="form-control")}
                <span class="text-danger">${', '.join(form.mraid_url.errors)}</span>
            </div>
        </div>

        <div class="form-group ${'has-error' if form.mraid_html.errors else ''}" data-if-mraid-type="${Media.MRAID_Type.HTML}">
            ${form.mraid_html.label(class_="col-md-3 control-label")}
            <div class="col-md-9">
                ${form.mraid_html(class_="form-control", rows=15)}
                <span class="text-danger">${', '.join(form.mraid_html.errors)}</span>
            </div>
        </div>

        <hr/>

        <div class="form-group">
            <div class="col-md-12 text-center">
                <button type="submit" data-form="mraid" class="btn btn-success btn-tall btn-wide">
                    <i class="fa fa-fw fa-save"></i> Сохранить
                </button>
            </div>
        </div>
    </div>
</form>