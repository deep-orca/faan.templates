<%namespace name="bcrumb" file="/layout/bcrumb.mako" />
<%namespace name="fabutton" file="/layout/fa-button.mako" />
<%inherit file="/layout/main.mako" />

<%!
    from faan.core.model.adv.media import Media
    from faan.admin.controllers.media import MediaTabs
%>

<%

    for s in g.selects:
        if s.get('name') == 'account':
            s.update({'header': u'Аккаунт'})
        if s.get('name') == 'campaign':
            s.update({'header': u'Кампания'})

    action_on_click = {
        Media.ClickActions.NONE: u'Действие отсутствует',
        Media.ClickActions.CALL: u'Звонок',
        Media.ClickActions.SMS: u'Отправить SMS',
        Media.ClickActions.URL: u'Переход по ссылке',
        Media.ClickActions.VIDEO: u'Показ видео',
    }
    action_on_click_data = [(None, u"Все")] + [(k, v) for k, v in action_on_click.items()]
    on_click = {
        'id': 'id_select_action_on_click',
        'data': action_on_click_data,
        'placeholder': u'Действие по нажатию',
    }
%>

<%def name="title()">Креативы</%def>
<%def name="description()"></%def>

<script src="/global/static/plugins/fancybox/source/jquery.fancybox.js"></script>
<script src="/global/static/plugins/video-js/video.js"></script>

<%def name="page_css()">
<link rel="stylesheet" href="/global/static/plugins/fancybox/source/jquery.fancybox.css"></link>
<link href="/global/static/plugins/bootstrap-modal/css/bootstrap-modal-bs3patch.css" rel="stylesheet" type="text/css"/>
<link href="/global/static/plugins/bootstrap-modal/css/bootstrap-modal.css" rel="stylesheet" type="text/css"/>
<link rel="stylesheet" href="/global/static/plugins/video-js/video-js.min.css"></link>
<style>
.playbutton{
    font-size: 36px;
    position: absolute;
    margin-top: -50px;
    margin-left: -15px;
    color: #5AB5FD;
}
.playbutton:hover{
    color:#fff;
}

.text-muted:hover{
    color: #4889BD;
}

.name-wrapper {
    width: 100px;
    overflow: hidden;

    float: left;
}

</style>
</%def>


${ bcrumb.h(self) }

<%def name="render_table(id)"> </%def>
<%def name="callbacks()"> </%def>
<%def name="document_on_ready_extend()"> </%def>
<%def name="filters_extend()"> </%def>


<%def name="show_select_item(select)">
    <div class="form-group">
        <label for="${select['name']}" class="control-label col-md-3">${unicode(select['header'])}</label>
        <div class="col-md-3">
                <input name="${select['name']}" type="hidden" id="${select['id']}" style="width:324px"/>
        </div>
    </div>
</%def>

<%def name="show_filters(selects)">
    %for select in selects:
        ${self.show_select_item(select)}
    %endfor
    ## TODO: Move the "div" block below into filters_extend function for each child templates.
    %if g.mtype != str(Media.Type.MRAID):
    <div class="form-group">
        <label class="control-label col-md-3">Действие по нажатию</label>
        <div class="col-md-1">
                <input name="action_on_click" type="text" id="id_select_action_on_click" value="${g.media_action}" style="width:324px"/>
            <span class="help-block">
            </span>
        </div>
    </div>
    %endif
    ${self.filters_extend()}
    <div class="form-group hidden" id="media_id">
		<label class="control-label col-md-3">Фильтровать по медиа</label>
		<div class="col-md-4">
			<input type="hidden" id="filter_media_id" class="form-control select2" value="Медиа: ${g.media_id}" style="width:324px">
		</div>
	</div>

    <!-- ==== SUBMIT ==== --->
    <div class="form-group">
    <label class="control-label col-md-3"></label>
        <div class="col-sm-1">
            <a href="#" id="apply_changes" class="btn btn-success">Применить</a>
        </div>
    </div>
</%def>

<!-- BEGIN PAGE CONTENT-->
<div class="row">
<div class="col-md-12">
    <div class="portlet">
        <div class="portlet-title">

        <div class="caption"><i class="fa fa-rocket"></i>Креативы</div>
            <div class="actions">
                <div class="btn-group">
                    <a class="btn btn-info" href="#" data-toggle="dropdown">
                        <i class="fa fa-cogs"></i> Действие <i class="fa fa-angle-down"></i>
                    </a>
                    <ul class="dropdown-menu pull-right">
                        <li><a href="#group_action_approve" data-toggle="modal" id="disable" class="group_actions" ><i class="fa fa-times"></i> Заблокировать</a></li>
                        <li><a href="#group_action_approve" data-toggle="modal" id="active" class="group_actions" ><i class="fa fa-play"></i> Активировать</a></li>
                        <li><a href="#group_action_approve" data-toggle="modal" id="delete" class="group_actions" ><i class="fa fa-trash-o"></i> Удалить</a></li>
                    </ul>
                </div>
            </div>
        </div>

        <div class="portlet-body">

                <form method="GET" class="form-horizontal">
                    ${self.show_filters(g.selects)}
			    </form>

                <ul class="nav nav-pills">
                    %if getattr(g, 'tab', '') and g.tab == MediaTabs.NEW:
                    <li class="active">
                    %else:
                    <li class="">
                    %endif
                        <a href="#tab_2_1" data-toggle="tab" id="tab_${MediaTabs.NEW}">Новые</a>
                    </li>
                    %if getattr(g, 'tab', '') and g.tab == MediaTabs.ACTIVE:
                    <li class="active">
                    %else:
                    <li class="">
                    %endif
                        <a href="#tab_2_2" data-toggle="tab" id="tab_${MediaTabs.ACTIVE}">Активные</a>
                    </li>
                    %if getattr(g, 'tab', '') and g.tab == MediaTabs.BANNED:
                    <li class="active">
                    %else:
                    <li class="">
                    %endif
                        <a href="#tab_2_3" data-toggle="tab" id="tab_${MediaTabs.BANNED}">Заблокированные</a>
                    </li>
                </ul>
                <div class="tab-content">
                    %if getattr(g, 'tab', '') and g.tab == MediaTabs.NEW:
                    <div class="tab-pane fade active in" id="tab_2_1">
                    %else:
                    <div class="tab-pane fade" id="tab_2_1">
                    %endif
                        <p>
                        <%self:render_table id="stats_table_${str(MediaTabs.NEW)}">

                        </%self:render_table>
                        </p>
                        <div class="row">
                            <div class="col-md-6 col-sm-12"></div>
                            <div class="col-md-6 col-sm-12">
                                <div id="log_table_filter_${MediaTabs.NEW}" class="dataTables_filter">
                                    <label>
                                    &nbsp
                                        <ul class="pagination">
                                            <li class="prev disabled"><a title="Prev" href="#"><i class="fa fa-angle-left"></i></a></li>
                                            <li class="next"><a title="Next" href="#"><i class="fa fa-angle-right"></i></a></li>
                                        </ul>
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>
                    %if getattr(g, 'tab', '') and g.tab == MediaTabs.ACTIVE:
                    <div class="tab-pane fade active in" id="tab_2_2">
                    %else:
                    <div class="tab-pane fade" id="tab_2_2">
                    %endif
                        <p>
                        <%self:render_table id="stats_table_${str(MediaTabs.ACTIVE)}">

                        </%self:render_table>
                        </p>
                        <div class="row">
                            <div class="col-md-6 col-sm-12"></div>
                            <div class="col-md-6 col-sm-12">
                                <div id="log_table_filter_${MediaTabs.ACTIVE}" class="dataTables_filter">
                                    <label>
                                    &nbsp
                                        <ul class="pagination">
                                            <li class="prev disabled"><a title="Prev" href="#"><i class="fa fa-angle-left"></i></a></li>
                                            <li class="next"><a title="Next" href="#"><i class="fa fa-angle-right"></i></a></li>
                                        </ul>
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>
                    %if getattr(g, 'tab', '') and g.tab == MediaTabs.BANNED:
                    <div class="tab-pane fade active in" id="tab_2_3">
                    %else:
                    <div class="tab-pane fade" id="tab_2_3">
                    %endif
                        <p>
                        <%self:render_table id="stats_table_${str(MediaTabs.BANNED)}">

                        </%self:render_table>
                        </p>
                        <div class="row">
                            <div class="col-md-6 col-sm-12"></div>
                            <div class="col-md-6 col-sm-12">
                                <div id="log_table_filter_${MediaTabs.BANNED}" class="dataTables_filter">
                                    <label>
                                    &nbsp
                                        <ul class="pagination">
                                            <li class="prev disabled"><a title="Prev" href="#"><i class="fa fa-angle-left"></i></a></li>
                                            <li class="next"><a title="Next" href="#"><i class="fa fa-angle-right"></i></a></li>
                                        </ul>
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
        </div>
    </div>
</div>
</div>
<!-- END PAGE CONTENT-->

<!-- change_account_status -->
<div id="group_action_approve" class="modal fade" tabindex="-1" data-width="760" style="top:10%">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
        <h4 class="modal-title"><span id="approve_header"></span></h4>
    </div>
    <form method="POST" class="form-horizontal">
        <div class="modal-body">
            <div class="row">
                <div class="col-md-10">
                    <p>
                        Вы действительно хотите <span id='approve_chose_action'></span> креативы <b><span id="approve_elements_list"></span></b> ?
                    </p>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" data-dismiss="modal" class="btn btn-default">Отменить</button>
            <a href="#" data-dismiss="modal" id="button_group_action_approve" class="btn btn-warning">Подтвердить</a>
        </div>
    </form>
</div>


<!-- SCRIPTS INDEX -->
<script type="text/javascript">
Index = function() {
    return {

        // Some table attributes
        data: null,
        total: +0,

        // Pagination options
        pag_size: +50,
        pag_start: 0,

        tab: null,
        mID: null,
        fmtype: null,

        init: function() {
            $("#apply_changes").on("click", function(e) {
                Index["loadData_" + Index.tab]()
            });

            %for tab in MediaTabs.tabs():
            $("#tab_${tab}").on("click", function(e) {
                Index.tab = "${tab}";
                Index.pag_size = + 50;
                Index.pag_start = + 0;
                Index.loadData_${tab}();
            });
            %endfor

            %if getattr(g, 'tab', ''):
                this.tab = "${g.tab}";
            %else:
                this.tab = "${MediaTabs.NEW}";
            %endif

            this.first = true;
            this.mID = "${g.media_id}";
            if (this.mID)
                $("#media_id").removeClass("hidden");
            this.fmtype = null;
        },

        initSelect2: function() {
            var self = this;
            var None = null;
            var format = function(item) { return item.name };

            %for elem in (on_click,):
            $("#${elem['id']}").select2({
                placeholder: "${elem['placeholder']}",
                data: [
                    %for i, (k, v) in enumerate(elem['data']):
                        %if i != len(elem['data']) - 1:
                        {"id": ${k}, "text": "${v}"},
                        %else:
                        {"id": ${k}, "text": "${v}"}
                        %endif
                    %endfor
                ]
            })
            %endfor

            %for select in g.selects:
                <%
                    _name = 'account' if select['name'] in ('advertiser', 'publisher', ) else select['name']
                    placeholder = getattr(g, str(_name) + '_name', select['header'])
                %>

                $("#${select['id']}").select2({
                    placeholder: "${placeholder}",
                    ajax: {
                        url: function() { return "/media/ajax/select?query=${select['name']}&values=" + select_list.get_values() },
                        type: "GET",
                        results: function(data, page) {
                            for (var name, i = 0; i < data.objects.length; i++)
                                if (data.objects[i].id == null)
                                    data.objects[i].name = "Все";
                            return {results: data.objects, text: 'name' };
                         },
                        data: function(term, page) { return { q: term } }
                    },
                    formatSelection: format,
                    formatResult: format
                }).on("select2-selecting", function(e) {
                    select_list["${select.get('name')}"] = e.val != null ? e.val : "";
                    <% flag = False %>
                    %for s in g.selects:
                        %if flag:
                            select_list["${s.get('name')}"] = '';
                            $("#${s.get('id')}").select2("data", {id: "", text: "Все"});
                            $("#s2id_${s.get('id')} a").addClass("select2-default");
                            $("#s2id_${s.get('id')} a span:first").html("${s.get('header')}")
                        %endif
                        <%
                            if s.get('name') == select.get('name'):
                                flag = True
                        %>
                    %endfor
                });
            %endfor

        },

        %for tab in MediaTabs.tabs():
        loadData_${tab}: function(start, count, search) {
            try { $("#stats_table_${tab}").DataTable().destroy(); } catch (e) { }

            var parameters = {
                start: start ? start : 0,
                count: count ? count : Index.pag_size,
                search: search ? search : "",
                mtype: ${g.mtype},
                mID: Index.mID,
                fmtype: Index.fmtype,
                mediaAction: $("#id_select_action_on_click").val()
            }

            // Update parameters.
            // This function is defined in child templates.
            try {
                parameters = update_parameters(parameters)
            } catch(e) { }

            var url = "/media/ajax/table?tab=${tab}" + "&values=" + select_list.get_values();

            for (var p in parameters)
                url += "&" + p + "=" + parameters[p];

            // Define columns for each type of the media.
            // This function is defined in children templates.
            var c = define_columns();

            $("#stats_table_${tab}").dataTable({
                processing: true,
                ajax: {
                    url: url,
                    dataSrc: function(response) {
                        Index.total = response.meta.total;
                        return response.data;
                    }
                },
                columns: c,
                lengthChange: true,
                "aLengthMenu": [
                    [25, 50, 100, -1],
                    [25, 50, 100, "All"] // change per page values here
                ],
                // set the initial value
                "iDisplayLength": 50,
                "sPaginationType": "bootstrap",
                "oLanguage": {
                    "sLengthMenu": "_MENU_ records",
                    "oPaginate": {
                        "sPrevious": "Prev",
                        "sNext": "Next"
                    }
                },
                "aoColumnDefs": [{
                        'bSortable': false,
                        'aTargets': [0]
                    }
                ],
                order: [],
                paging: false,
                bFilter: false,
                bInfo: false
            });

            $("#stats_table_${tab} .group-checkable").change(function () {
                var set = jQuery(this).attr("data-set");
                var checked = jQuery(this).is(":checked");
                jQuery(set).each(function () {
                    if (checked) {
                        $(this).attr("checked", true);
                        $(this).parents('tr').addClass("active");
                        $(this).parents('span').addClass("checked");
                    } else {
                        $(this).attr("checked", false);
                        $(this).parents('tr').removeClass("active");
                        $(this).parents('span').removeClass("checked");
                    }
                });
                jQuery.uniform.update(set);

            });

            // Add select items.
            $("#stats_table_${tab}_wrapper .row")[0].children[0].innerHTML = '<div class="dataTables_length" id="stats_table_${tab}_length"><label><select name="stats_table_${tab}_length" id="_stats_table_${tab}_length" aria-controls="stats_table_${tab}" class="form-control input-xsmall"><option value="50">50</option><option value="100">100</option><option value="150">150</option></select> records</label></div>'
            $('#stats_table_${tab}_wrapper .dataTables_length select').select2().on("select2-selecting", function(e) {
                Index.pag_size = + e.val;
                Index.pag_start = + 0;
                Index.loadData_${tab}(0, Index.pag_size);
                $('#stats_table_${tab}_wrapper .dataTables_length select').select2("val", e.val.toString());
            });

            //Add search item.
            $("#stats_table_${tab}_wrapper .row")[0].children[1].innerHTML = '<div id="stats_table_${MediaTabs.NEW}_filter" class="dataTables_filter"><label>Search:<input type="search" class="form-control input-medium" aria-controls="stats_table_${MediaTabs.NEW}"></label></div>'
            $('#stats_table_${tab}_wrapper .dataTables_filter input').on("change", function(){
                var text = $(this).val();
                Index.pag_start = + 0;
                Index.loadData_${tab}(Index.pag_start, Index.pag_size, text);
                $('#stats_table_${tab}_wrapper .dataTables_length select').select2("val", Index.pag_size);
                $('#stats_table_${tab}_wrapper .dataTables_filter input').val(text);
            });

            // Parsing parameters for the table.
            $("#stats_table_${tab}").on('order.dt',  function () {

                // Array of the table rows.
                var rows = $("#stats_table_${tab}").dataTable().fnGetNodes();

                if (rows.length < Index.pag_size)
                    $(".next").addClass("disabled");
                else
                    $(".next").removeClass("disabled");

                // This function is defined in child templates.
                parse_table(rows)

                // Add summary to the table.
                var start = +Index.pag_start + 1, end;
                if (!Index.total)
                    start = end = 0;
                else if ((+Index.pag_start + Index.pag_size) > +Index.total)
                    end = Index.total;
                else
                    end = + Index.pag_start + Index.pag_size;
                $("#stats_table_${tab}_wrapper .row")[1].children[0].innerHTML = 'С {start} по {end}, всего {total}'
                    .replace("{total}", Index.total)
                    .replace("{start}", start)
                    .replace("{end}", end);
            });
        },
        %endfor

        // Reload the table. The pagination settings are dropped.
        reload_table: function(){
            Index["loadData_" + Index.tab]();
        },

        fake: null
    }
}();
</script>
<!-- SCRIPTS INDEX END-->


<script type="text/javascript">
    $(document).on("ready", function() {

        select_list =  {
            %for select in g.selects:
                ${select['name']}: "${getattr(g, str(select['name']), '')}",
            %endfor

            get_values: function() {
                var s = "", items = ${[s.get('name') for s in g.selects]};
                for(var v in this)
                    if (v != "get_values")
                        s += v + ":" + this[v] + ",";
                return s ? s.substring(0, s.length - 1): "";
            }
        };

        // Table pagination.
        $(".prev").on("click", function() {
            if ($(".prev").attr("class").indexOf("disabled") > 0)
               return;

            Index.pag_start -= Number(Index.pag_size);
            if (+Index.pag_start < 0) {
               Index.pag_start = +0;
               $(".prev").addClass("disabled");
               return;
            }
            $(".next").removeClass("disabled");

            Index["loadData_" + Index.tab](Index.pag_start, Index.pag_size);

        });
        $(".next").on("click", function() {
            if ($(".next").attr("class").indexOf("disabled") > 0)
               return;

            $(".prev").removeClass("disabled");

            Index.pag_start = Number(Index.pag_start) + Number(Index.pag_size);
            Index["loadData_" + Index.tab](Number(Index.pag_start), Number(Index.pag_size));
        });

        Index.init();
        Index.initSelect2();
        %if getattr(g, 'tab', ''):
            Index.loadData_${g.tab}();
        %else:
            Index.loadData_${MediaTabs.NEW}();
        %endif

        $("#filter_media_id").select2({tags: ["Медиа: ${g.media_id}"]}).on("change", function(e) {
            Index.mID = null;
            Index.reload_table();
            $("#media_id").addClass("hidden");
        });

        // Environment for fancybox.
        $(".fancybox").fancybox({"type" : "image"});

        $(".fancyboxvid").fancybox({
            type : 'image',
            padding: 0,
            openEffect : 'elastic',
            openSpeed  : 250,
            closeEffect : 'elastic',
            closeSpeed  : 250,
            closeClick : false,
            helpers : {
                overlay : null
            },
            content: '<video id="video_box" class="video-js vjs-default-skin" controls preload="auto" width="800" height="495"> <source src="#" type="video/mp4" /> <p class="vjs-no-js">Ваш браузер не поддерживает проигрывание видео</p></video>',
            afterShow: function(){
                 $("source").attr("src", $(this).attr("href"));
                 videojs(document.getElementById('video_box'), {}, function() {
                    this.load().play();
                 });
            }
        });

        $(".vpreview").hover(
            function(){$(this).next(".playbutton").css("color", "#fff");},
            function(){$(this).next(".playbutton").css("color", "#5AB5FD");}
        );

    <%self:document_on_ready_extend> </%self:document_on_ready_extend>

    });
</script>


<!--GROUP ACTIONS-->

<script type="text/javascript">

    action_assistant = {
        objects: null,
        objects_ids: null,
        action: null
    };

    $('.group_actions').on('click', function(){
        var objects = $('.checkboxes:checked');
        var objects_ids = '';
        if (objects.length > 0){
            for (var i = 0; i < objects.length; i++){
                if (i == objects.length - 1){
                    objects_ids += objects[i].value;
                    continue;
                }
                objects_ids += objects[i].value + ',';
            }

            var group_checkbox = $("#stats_table_{tab} .group-checkable".replace("{tab}", Index.tab));
            group_checkbox[0].parentElement.className = "";

            action_assistant.action = this.id
            action_assistant.objects = objects
            action_assistant.objects_ids = objects_ids

            switch(this.id) {
                case 'active':
                    $('#approve_chose_action').html("активировать");
                    $('#approve_header').html("Активация креативов");
                    break;
                case 'disable':
                    $('#approve_chose_action').html("заблокировать");
                    $('#approve_header').html("Блокировка креативов");
                    break;
                case 'delete':
                    $('#approve_chose_action').html("удалить");
                    $('#approve_header').html("Удаление креативов");
                    break;
            }

            $('#approve_elements_list').html("[" + objects_ids + "]");
        }
    });

    $("#button_group_action_approve").on('click', function(){
        $.post('/media/group_actions', {objects_list: action_assistant.objects_ids, action: action_assistant.action}, after_load);

        objects = action_assistant.objects

        for (var i = 0; i < objects.length; i++){
            tr = objects[i].parentElement.parentElement;
            tr.className = tr.className.replace("active", "");
            objects[i].parentElement.className = "";
            objects[i].checked = false;
            remove_tr(tr);
        }
    });

    function entry_action(event, action_assistant) {
        var tr = event.target.parentElement.parentElement.parentElement;
        var action = action_assistant.action;
        var objects_ids = action_assistant.id;
        var previous_state = "" + action_assistant.state;

        $.post('/media/group_actions', {objects_list: objects_ids, action: action}, after_load);

        if (Index.tab == "${MediaTabs.ACTIVE}") {
            var state, html_actions;
            // Those function are defined in children templates.
            var iStatus = index_status();
            var iAction = index_action();

            switch(action) {
                case "stop":
                    state = '<span class="badge badge-warning">На паузе</span>';
                    html_actions = '<a onClick="entry_action(event, {action: \'disable\', id: \'{media_id}\'})" ><i class="fa fa-times fabutton tooltips" data-placement="top" data-original-title="Заблокировать"></i></a>'.replace("{media_id}", objects_ids);
                    html_actions += '<a onClick="entry_action(event, {action: \'active\', id: \'{media_id}\'})" ><i class="fa fa-check fabutton tooltips" data-placement="top" data-original-title="Активировать"></i></a>'.replace("{media_id}", objects_ids);
                    break;
                case "active":
                    state = '<span class="badge badge-success">Активный</span>';
                    html_actions = '<a onClick="entry_action(event, {action: \'disable\', id: \'{media_id}\'})" ><i class="fa fa-times fabutton tooltips" data-placement="top" data-original-title="Заблокировать"></i></a>'.replace("{media_id}", objects_ids);
                    html_actions += '<a onClick="entry_action(event, {action: \'stop\', id: \'{media_id}\'})" ><i class="fa fa-pause fabutton tooltips" data-placement="top" data-original-title="Приостановить"></i></a>'.replace("{media_id}", objects_ids);
                    break;
                default:
                    tr.className = tr.className.replace("active", "");
                    remove_tr(tr);
                    return;
            }

            tr.children[iStatus].innerHTML = state;
            tr.children[iAction].innerHTML = html_actions;

            return;
        }

        tr.className = tr.className.replace("active", "");
        remove_tr(tr);
    }

    // That's used to reload the page if there are no rows in the table.
    function remove_tr(tr) {
        var reload = true;
        var trs = tr.parentElement.children;
        tr.innerHTML = "";

        for (var i = 0; i < trs.length; i++) {
            if (trs[i].innerHTML != "") {
                reload = false;
                break;
            }
        }
        if (reload)
            window.location.href = '/media/?tab=' + Index.tab;
    }

    function after_load(data) { }
</script>

<%self:callbacks> </%self:callbacks>