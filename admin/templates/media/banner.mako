<%namespace name="bcrumb" file="/layout/bcrumb.mako" />
<%namespace name="fabutton" file="/layout/fa-button.mako" />
<%inherit file="/media/index.mako" />

<%!
    from faan.core.model.adv.media import Media
%>

<%def name="title()">Креативы - Баннеры</%def>

${ bcrumb.h(self) }


<%def name="render_table(id)">
 <!--START TABLE-->
 <table class="table table-striped table-bordered table-hover" id="${id}">
 <thead>
 <tr>
     <th class="table-checkbox">
         <input type="checkbox" class="group-checkable" data-set="#${id} .checkboxes"/>
     </th>
     <th>Название</th>
     <th>Статус</th>
     <th>Баннер</th>
     <th>Размер</th>
     <th>По нажатию</th>
     <th>Действие</th>
 </tr>
 </thead>
 <tbody>

${caller.body()}

 </tbody>
 </table>
 <!--END TABLE-->
</%def>

<%def name="filters_extend()">
    <div class="form-group">
        <label class="control-label col-md-3">Размер баннера</label>
        <div class="col-md-1">
                <input name="banner_size" type="text" id="id_select_banener_size" value="${g.banner_size}" style="width:324px"/>
            <span class="help-block">
            </span>
        </div>
    </div>
</%def>

<!--CALLBACKS-->

<%def name="callbacks()">

<script type="text/javascript">
    //
    // CALLBACKS
    //

    //
    // MANDATORY FUNCTIONS FOR EACH CHILD TEMPLATES:
    //
    // define_columns
    // parse_table
    // index_status
    // index_action
    // update_parameters
    //

    // Define columns for the tables.
    function define_columns() {
        return [
            { data: 'checkbox' },
            { data: 'media' },
            { data: 'state' },
            { data: 'banner' },
            { data: 'banner_size' },
            { data: 'on_click' },
            { data: 'action' }
        ];
    }

    // Parses table parameters.
    // Some values in the cells of the table are required special processing,
    // e.g. they can be a JSON objects or a numeric status constant such
    // constant is required to replace with the status string.
    function parse_table(rows) {
        for (var i = 0; i < rows.length; i++) {
            // Media, Checkbox and Actions
            try {
                var media = JSON.parse(rows[i].cells[1].innerHTML);
                // Checkbox
                rows[i].cells[0].innerHTML = '<input type="checkbox" class="checkboxes" value="{media_id}"/>'
                    .replace("{media_id}", media.id);

                rows[i].cells[1].innerHTML = ' \
                    <h3 style="margin-top:0"> \
                        <span class="badge badge-info">#{media_id}</span> \
                         <a href="/media/{media_id}">{media_name}</a> \
                    </h3> \
                    <p class="text-info"> \
                        <i class="fa fa-user"></i> <a href="/media/banner/?tab={ctab}&account={account_id}">{account_username}</a> \
                    </p> \
                    <p class="text-info"> \
                        <i class="fa fa-bullhorn"></i> <a href="/media/banner/?tab={ctab}&campaign={campaign_id}">{campaign_name}</a> \
                    </p>'
                    .replace(/{media_id}/g, media.id)
                    .replace("{account_id}", media.account_id)
                    .replace("{media_name}", media.name)
                    .replace("{account_username}", media.account)
                    .replace("{campaign_name}", media.campaign)
                    .replace("{campaign_id}", media.campaign_id)
                    .replace(/{ctab}/g, Index.tab);

                    // Show URL if it exists.
                    var media_url_show = media.uri;
                    if (media_url_show && media_url_show.length > 25)
                        media_url_show = media_url_show.substr(0, 22) + "...";
                    if(media_url_show) {
                        rows[i].cells[1].innerHTML += ' \
                            <p class="text-info"> \
                                <i class="fa fa-external-link"></i> <a href="{media_uri}" target="_blank">{media_uri_show}</a> \
                            </p>'
                            .replace(/{media_uri}/g, media.uri)
                            .replace(/{media_uri_show}/g, media_url_show);
                    }

                    // Banner size
                    var banner = JSON.parse(rows[i].cells[3].innerHTML);
                    var maxWidth = 320, maxHeight = 150;
                    var width, height, ratio;

                    if (+banner.width && +banner.height) {
                        ratio = banner.width / banner.height;
                        if (+banner.width > +banner.height) {
                            width = maxWidth;
                            height = maxWidth / ratio;
                        } else if (+banner.width <= +banner.height) {
                            height = maxHeight;
                            width = height * ratio;
                        }

                        // Banner
                        rows[i].cells[3].innerHTML = ' \
                            <div class="thumbnail" style="width: {width}px; height:{height}px; margin-bottom: 0;"> \
                                <a class="fancybox" href="{banner_image}"> \
                                    <img src="{banner_image}" rel="group" style="width: {width}px; height:{height}px"> \
                                </a> \
                            </div> '
                            .replace(/{banner_image}/g, banner.image)
                            .replace(/{width}/g, width)
                            .replace(/{height}/g, height);
                        rows[i].cells[3].outerHTML = rows[i].cells[3].outerHTML.replace('<td>', '<td align="center">');
                    }

                    // ON CLICK
                    var on_click = rows[i].cells[5].innerHTML;
                    switch(on_click) {
                        case "${Media.ClickActions.CALL}":
                            rows[i].cells[5].innerHTML = '<i class="fa fa-phone" ></i> &nbsp '  + media.phone;
                            break;
                        case "${Media.ClickActions.SMS}":
                            rows[i].cells[5].innerHTML = '<i class="fa fa-envelope" ></i> &nbsp ' + media.phone;
                            break;
                        case "${Media.ClickActions.URL}":
                            rows[i].cells[5].innerHTML = '<i class="fa fa-external-link-square"></i> &nbsp <a href="{media_uri}" target="_blank">{media_uri_show}</a>'
                                .replace(/{media_uri}/g, media.uri)
                                .replace(/{media_uri_show}/g, media_url_show);
                            break;
                        case "${Media.ClickActions.VIDEO}":
                            rows[i].cells[5].innerHTML = ' \
                                <a class="fancyboxvid" href="{media_video}.mp4"> \
                                <div class="thumbnail vthumb text-center" style="width: 130px; height:100px; margin-bottom: 0;"> \
                                    <img src="/global/static/img/icons/video_preview.png" class="vpreview"> \
                                    <i class="fa fa-play-circle playbutton"></i> \
                                </div> \
                                </a>'
                                .replace(/{media_video}/g, banner.video);
                            break;
                        default:
                            rows[i].cells[5].innerHTML = "";
                            break;
                    }

                    // Actions
                    var state = rows[i].cells[2].innerHTML;
                    state = JSON.parse(state);
                    if (state.id == ${Media.State.NONE}) {
                        rows[i].cells[6].innerHTML = '<a onClick="entry_action(event, {action: \'active\', id: \'{media_id}\'})" ><i class="fa fa-check fabutton tooltips" data-placement="top" data-original-title="Активировать"></i></a>'.replace("{media_id}", media.id);
                        rows[i].cells[6].innerHTML += '<a onClick="entry_action(event, {action: \'disable\', id: \'{media_id}\'})" ><i class="fa fa-times fabutton tooltips" data-placement="top" data-original-title="Заблокировать"></i></a>'.replace("{media_id}", media.id);
                    } else if (state.id == ${Media.State.ACTIVE}) {
                        rows[i].cells[6].innerHTML = '<a onClick="entry_action(event, {action: \'disable\', id: \'{media_id}\'})" ><i class="fa fa-times fabutton tooltips" data-placement="top" data-original-title="Заблокировать"></i></a>'.replace("{media_id}", media.id);
                        rows[i].cells[6].innerHTML += '<a onClick="entry_action(event, {action: \'stop\', id: \'{media_id}\'})" ><i class="fa fa-pause fabutton tooltips" data-placement="top" data-original-title="Приостановить"></i></a>'.replace("{media_id}", media.id);
                    } else if (state.id == ${Media.State.DISABLED}) {
                        rows[i].cells[6].innerHTML = '<a onClick="entry_action(event, {action: \'disable\', id: \'{media_id}\'})" ><i class="fa fa-times fabutton tooltips" data-placement="top" data-original-title="Заблокировать"></i></a>'.replace("{media_id}", media.id);
                        rows[i].cells[6].innerHTML += '<a onClick="entry_action(event, {action: \'active\', id: \'{media_id}\'})" ><i class="fa fa-check fabutton tooltips" data-placement="top" data-original-title="Активировать"></i></a>'.replace("{media_id}", media.id);
                    } else  {
                        rows[i].cells[6].innerHTML = '<a onClick="entry_action(event, {action: \'active\', id: \'{media_id}\'})" ><i class="fa fa-check fabutton tooltips" data-placement="top" data-original-title="Активировать"></i></a>'.replace("{media_id}", media.id);
                    }
            } catch(e) { }

            // Status
            try {
                var stat = rows[i].cells[2].innerHTML;
                stat = JSON.parse(stat);
                var stat_class = "";
                if (stat.id == "${Media.State.ACTIVE}"){
                    stat_class = "badge-success";
                } else if (stat.id == "${Media.State.DISABLED}") {
                    stat_class = "badge-warning";
                } else if (stat.id == "${Media.State.NONE}") {
                    stat_class = "badge-info";
                } else {
                    stat_class = "badge-danger";
                }
                rows[i].cells[2].innerHTML = '<span class="badge {stat_class}">{stat_value}</span>'
                    .replace("{stat_value}", media_state[stat.id])
                    .replace("{stat_class}", stat_class);
            } catch(e) { }
        }
    }

    // Returns an index of cell with status.
    function index_status() { return 2; }

    // Returns an index of cell with actions.
    function index_action() { return 6; }

    // Updates parameters that will be sent to an ajax request in order to
    // fetch table rows. For instance, the child template has a custom filter
    // the filter's value has to be sent to Controller.
    function update_parameters(params) {
        try {
            var bannerSize = $("#id_select_banener_size").val().split("x");
            params.bannerWidth = bannerSize[0];
            params.bannerHeigth = bannerSize[1];
        } catch(e) {
            params.bannerWidth = "";
            params.bannerHeigth = "";
        }
        return params;
    }

    //
    // SPECIFIC FUNCTIONS.
    //


</script>

${caller.body()}

</%def>


<!--DOCUMENT ON READY EXTEND-->
<!--The code below will be added to "document on ready" function.-->
<!--Therefore you should NOT put the code into "script tags.-->
<%def name="document_on_ready_extend()">
    var None = null;

    media_state = {
        "${Media.State.NONE}": 'Новый',
        "${Media.State.ACTIVE}": 'Активный',
        "${Media.State.DISABLED}": 'На паузе',
        "${Media.State.SUSPENDED}": 'Заблокированный'
    }

<%
        banner_size_dict = {
            "320x50": "320x50",
            "320x100": "320x100",
            "300x250": "300x250",
            "468x60": "468x60",
            "728x90": "728x90",
            "320x480": "320x480",
            "1024x768": "1024x768",
        }
        banner_size_data = [(None, u"Все")] + [(k, v) for k, v in banner_size_dict.items()]
        banner_size = {
            'id': 'id_select_banener_size',
            'data': banner_size_data,
            'placeholder': u'',
        }
%>


    // Init the banner size filter.
    $("#id_select_banener_size").select2({
        data: [
            %for i, (k, v) in enumerate(banner_size['data']):
                %if i != len(banner_size['data']) - 1:
                    {"id": "${k}", "text": "${v}"},
                %else:
                    {"id": "${k}", "text": "${v}"}
                %endif
            %endfor
        ]
    });


${caller.body()}

</%def>