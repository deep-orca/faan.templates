<%namespace name="bcrumb" file="/layout/bcrumb.mako" />
<%namespace name="fabutton" file="/layout/fa-button.mako" />
<%inherit file="/media/index.mako" />

<%!
    from faan.core.model.adv.media import Media
%>

<%def name="title()">Креативы - Видео</%def>

${ bcrumb.h(self) }


<%def name="render_table(id)">
 <!--START TABLE-->
 <table class="table table-striped table-bordered table-hover" id="${id}">
 <thead>
 <tr>
     <th class="table-checkbox">
         <input type="checkbox" class="group-checkable" data-set="#${id} .checkboxes"/>
     </th>
     <th>Название</th>
     <th>Статус</th>
     <th>Тип</th>
     <th>Креатив</th>
     <th>Действие</th>
 </tr>
 </thead>
 <tbody>

${caller.body()}

 </tbody>
 </table>
 <!--END TABLE-->
</%def>

<%def name="show_filters(selects)">
    %for select in selects:
        ${self.show_select_item(select)}
    %endfor
    <div class="form-group hidden" id="media_id">
        <label class="control-label col-md-3"></label>
        <div class="col-md-3">
            <a href="#" id="remove_media_id" class="btn btn-danger" title="Очистить">Медиа: ${g.media_id}</a>
        </div>
    </div>
    <!-- ==== SUBMIT ==== --->
    <div class="form-group">
    <label class="control-label col-md-3"></label>
        <div class="col-sm-1">
            <a href="#" id="apply_changes" class="btn btn-success">Применить</a>
        </div>
        <div class="col-sm-3">
            <div style="padding-left: 50px;" data-toggle="buttons" class="btn-group osfilter" id="mtype_filter">
                <label id="mtype_all" class="btn btn-default btn-sm filteros active">
                <input type="radio" class="toggle"> Все </label>
                <label id="mtype_banner" class="btn btn-default btn-sm filteros">
                <input type="radio" class="toggle"> <i class="fa fa-2x fa-fw fa-file-picture-o"></i></label>
                <label id="mtype_video" class="btn btn-default btn-sm filteros">
                <input type="radio" class="toggle"> <i class="fa fa-2x fa-fw fa-file-video-o"></i></label>
                <label id="mtype_mraid" class="btn btn-default btn-sm filteros">
                <input type="radio" class="toggle"> <i class="fa fa-2x fa-fw fa-file-zip-o"></i></label>
            </div>
        </div>
    </div>
</%def>


<!--CALLBACKS-->

<%def name="callbacks()">

<script type="text/javascript">
    //
    // CALLBACKS
    //

    //
    // MANDATORY FUNCTIONS FOR EACH CHILD TEMPLATES:
    //
    // define_columns
    // parse_table
    // index_status
    // index_action
    //

    // Define columns for the tables.
    function define_columns() {
        return [
            { data: 'checkbox' },
            { data: 'media' },
            { data: 'state' },
            { data: 'type' },
            { data: 'representation' },
            { data: 'action' }
        ];
    }

    // Parses table parameters.
    // Some values in the cells of the table are required special processing,
    // e.g. they can be a JSON objects or a numeric status constant such
    // constant is required to replace with the status string.
    function parse_table(rows) {
        for (var i = 0; i < rows.length; i++) {
            // Media, Video, Checkbox and Actions
            try {
                var media = JSON.parse(rows[i].cells[1].innerHTML);
                // Checkbox
                rows[i].cells[0].innerHTML = '<input type="checkbox" class="checkboxes" value="{media_id}"/>'
                    .replace("{media_id}", media.id);
                //rows[i].cells[0].innerHTML = '<div class="checker"><span><input type="checkbox" value="' + media.id + '" class="checkboxes"></span></div>';

                var media_url_show = media.uri
                if (media_url_show.length > 25) {
                    media_url_show = media_url_show.substr(0, 22) + "...";
                }
                rows[i].cells[1].innerHTML = ' \
                    <h3 style="margin-top:0"> \
                        <span class="badge badge-info">#{media_id}</span> \
                        <a href="/media/{media_id}">{media_name}</a> \
                    </h3> \
                    <p class="text-info"> \
                        <i class="fa fa-user"></i> <a href="/media/list/?tab={ctab}&account={account_id}">{account_username}</a> \
                    </p> \
                    <p class="text-info"> \
                        <i class="fa fa-bullhorn"></i> <a href="/media/list/?tab={ctab}&campaign={campaign_id}">{campaign_name}</a> \
                    </p>'
                    .replace(/{media_id}/g, media.id)
                    .replace("{account_id}", media.account_id)
                    .replace("{media_name}", media.name)
                    .replace("{account_username}", media.account)
                    .replace("{campaign_name}", media.campaign)
                    .replace("{campaign_id}", media.campaign_id)
                    .replace(/{ctab}/g, Index.tab);

                    // Show URL if it exists.
                    var media_url_show = media.uri;
                    if (media_url_show && media_url_show.length > 25)
                        media_url_show = media_url_show.substr(0, 22) + "...";
                    if(media_url_show) {
                        rows[i].cells[1].innerHTML += ' \
                            <p class="text-info"> \
                                <i class="fa fa-external-link"></i> <a href="{media_uri}" target="_blank">{media_uri_show}</a> \
                            </p>'
                            .replace(/{media_uri}/g, media.uri)
                            .replace(/{media_uri_show}/g, media_url_show);
                    }

                    //Type
                    var mtype = rows[i].cells[3].innerHTML;
                    $(rows[i].cells[3]).attr('class', 'text-center media-status');

                    // Representation
                    switch(mtype) {
                        case "${Media.Type.BANNER}":
                            var banner = JSON.parse(rows[i].cells[4].innerHTML);
                            var maxWidth = 320, maxHeight = 150;
                            var width, height, ratio;

                            if (banner.width == 0 && banner.height == 0)
                                break;

                            ratio = banner.width / banner.height;
                            if (+banner.width > +banner.height) {
                                width = maxWidth;
                                height = maxWidth / ratio;
                            } else if (+banner.width <= +banner.height) {
                                height = maxHeight;
                                width = height * ratio;
                            }

                            rows[i].cells[4].innerHTML = ' \
                                <div class="thumbnail" style="width: {width}px; height:{height}px; margin-bottom: 0;"> \
                                    <a class="fancybox" href="{banner_image}"> \
                                        <img src="{banner_image}" rel="group" style="width: {width}px; height:{height}px"> \
                                    </a> \
                                </div> '
                                .replace(/{banner_image}/g, banner.image)
                                .replace(/{width}/g, width)
                                .replace(/{height}/g, height);
                            rows[i].cells[4].outerHTML = rows[i].cells[4].outerHTML.replace('<td>', '<td align="center">');
                            break;
                        case "${Media.Type.VIDEO}":
                            var video = JSON.parse(rows[i].cells[4].innerHTML);
                            rows[i].cells[4].innerHTML = ' \
                                <a class="fancyboxvid" href="{media_video}.mp4" onmouseleave="endPreview(event, this)" onmouseenter="startPreview(event, this)"> \
                                <div class="thumbnail vthumb text-center" style="width: 130px; height:100px; margin-bottom: 0;"> \
                                    <img src="{preview}" class="vpreview"> \
                                    <i class="fa fa-play-circle playbutton"></i> \
                                </div> \
                                </a>'
                                .replace(/{media_video}/g, video.video)
                                .replace(/{preview}/g, video.preview);
                            break;
                        case "${Media.Type.MRAID}":
                            rows[i].cells[4].innerHTML = "";
                            break;
                    }

                    //Actions
                    var state = rows[i].cells[2].innerHTML;
                    state = JSON.parse(state);
                    var mtypeString;
                    switch(mtype) {
                        case "${Media.Type.BANNER}":
                            mtypeString = "banner";
                            break;
                        case "${Media.Type.VIDEO}":
                            mtypeString = "video";
                            break;
                        case "${Media.Type.MRAID}":
                            mtypeString = "mraid";
                            break;
                    }
                    // Open details information about the media.
                    rows[i].cells[5].innerHTML = '<a href="/media/{mtype}/?mID={mID}&tab={tab}"><i class="fa fa-external-link fabutton tooltips" data-placement="top" data-original-title="Перейти"></i></a>'.replace("{media_id}", media.id)
                            .replace(/{mtype}/g, mtypeString)
                            .replace(/{mID}/g, media.id)
                            .replace(/{tab}/g, Index.tab);

                    if (state.id == ${Media.State.NONE}) {
                        rows[i].cells[5].innerHTML += '&nbsp <a onClick="entry_action(event, {action: \'active\', id: \'{media_id}\'})" ><i class="fa fa-check fabutton tooltips" data-placement="top" data-original-title="Активировать"></i></a>'.replace("{media_id}", media.id);
                        rows[i].cells[5].innerHTML += '&nbsp <a onClick="entry_action(event, {action: \'disable\', id: \'{media_id}\'})" ><i class="fa fa-times fabutton tooltips" data-placement="top" data-original-title="Заблокировать"></i></a>'.replace("{media_id}", media.id);
                    } else if (state.id == ${Media.State.ACTIVE}) {
                        rows[i].cells[5].innerHTML += '&nbsp <a onClick="entry_action(event, {action: \'disable\', id: \'{media_id}\'})" ><i class="fa fa-times fabutton tooltips" data-placement="top" data-original-title="Заблокировать"></i></a>'.replace("{media_id}", media.id);
                        rows[i].cells[5].innerHTML += '&nbsp <a onClick="entry_action(event, {action: \'stop\', id: \'{media_id}\'})" ><i class="fa fa-pause fabutton tooltips" data-placement="top" data-original-title="Приостановить"></i></a>'.replace("{media_id}", media.id);
                    } else if (state.id == ${Media.State.DISABLED}) {
                        rows[i].cells[5].innerHTML += '&nbsp <a onClick="entry_action(event, {action: \'disable\', id: \'{media_id}\'})" ><i class="fa fa-times fabutton tooltips" data-placement="top" data-original-title="Заблокировать"></i></a>'.replace("{media_id}", media.id);
                        rows[i].cells[5].innerHTML += '&nbsp <a onClick="entry_action(event, {action: \'active\', id: \'{media_id}\'})" ><i class="fa fa-check fabutton tooltips" data-placement="top" data-original-title="Активировать"></i></a>'.replace("{media_id}", media.id);
                    } else  {
                        rows[i].cells[5].innerHTML += '&nbsp <a onClick="entry_action(event, {action: \'active\', id: \'{media_id}\'})" ><i class="fa fa-check fabutton tooltips" data-placement="top" data-original-title="Активировать"></i></a>'.replace("{media_id}", media.id);
                    }

                    //Type
                    rows[i].cells[3].innerHTML = mediaType[mtype];

            } catch(e) { }

            // Status
            try {
                var stat = rows[i].cells[2].innerHTML;
                stat = JSON.parse(stat);
                var stat_class = "";
                if (stat.id == "${Media.State.ACTIVE}"){
                    stat_class = "badge-success";
                } else if (stat.id == "${Media.State.DISABLED}") {
                    stat_class = "badge-warning";
                } else if (stat.id == "${Media.State.NONE}") {
                    stat_class = "badge-info";
                } else {
                    stat_class = "badge-danger";
                }
                rows[i].cells[2].innerHTML = '<span class="badge {stat_class}">{stat_value}</span>'
                    .replace("{stat_value}", mediaState[stat.id])
                    .replace("{stat_class}", stat_class);
            } catch(e) { }
        }
    }

    // Returns an index of cell with status.
    function index_status() { return 2; }

    // Returns an index of cell with actions.
    function index_action() { return 5; }

    //
    // SPECIFIC FUNCTIONS.
    //

    // Preview functions
    // Starts to show preview.
    function startPreview(e, self) {
        var preview = self.children[0].children[0];
        timerChangeFrame = setInterval(function(){changeFrame(preview)}, 500);
    }

    // Stops the preview showing.
    function endPreview(e, self) {
        var preview = self.children[0].children[0];
        var src = preview.src.split("/");

        switch(src[src.length - 1]) {
            case "video_preview.png":
                break;
            default:
                src.splice(src.length - 1, 1);
                src.splice(0, 3);
                src = src.join("\/").replace(",", "/");
                src += "/preview.png";
                preview.src = "\/" + src;
                break;
        }
        clearInterval(timerChangeFrame)
    }

    // Changes the current preview frame.
    // When the mouse cursor is on a video player, this function changes a
    // frame every N second. N value is 0.5 second by default. You can setup
    // the value in the "startPreview" function.
    function changeFrame(preview) {
        var src = preview.src.split("/");
        switch(src[src.length - 1]) {
            case "video_preview.png":
                break;
            case "preview.png":
                src.splice(src.length - 1, 1);
                src.splice(0, 3);
                src = src.join("\/").replace(",", "/");
                src += "/preview1.png";
                preview.src = "\/" + src;
                break;
            default:
                var frame = Number(src[src.length - 1].split("preview")[1].split(".png")[0]);
                if (frame == 10) {
                    frame = 1;
                } else {
                    frame += 1;
                }
                frame = "preview" + frame;
                src.splice(src.length - 1, 1);
                src.splice(0, 3);
                src = src.join("\/").replace(",", "/");
                src += "/" + frame + ".png";
                preview.src = "/" + src;
                break;
        }

    }

</script>

${caller.body()}

</%def>


<!--DOCUMENT ON READY EXTEND-->
<!--The code below will be added to "document on ready" function.-->
<!--Therefore you should NOT put the code into "script tags.-->
<%def name="document_on_ready_extend()">

    mediaState = {
        "${Media.State.NONE}": 'Новый',
        "${Media.State.ACTIVE}": 'Активный',
        "${Media.State.DISABLED}": 'На паузе',
        "${Media.State.SUSPENDED}": 'Заблокированный'
    };

    mediaType = {
        "${Media.Type.BANNER}": '<i class="fa fa-2x fa-fw fa-file-picture-o"></i><br><small class="text-muted">Баннер</small>',
        "${Media.Type.VIDEO}": '<i class="fa fa-2x fa-fw fa-file-video-o"></i><br><small class="text-muted">Видео</small>',
        "${Media.Type.MRAID}": '<i class="fa fa-2x fa-fw fa-file-zip-o"></i><br><small class="text-muted">MRAID</small>'
    };

    function applyMtypeFilter(mtype){
        Index.fmtype = mtype;
        Index.reload_table();
    }

    $("#mtype_all").on("click", function() { applyMtypeFilter(null); });
    $("#mtype_banner").on("click", function() { applyMtypeFilter("${Media.Type.BANNER}"); });
    $("#mtype_video").on("click", function() { applyMtypeFilter("${Media.Type.VIDEO}"); });
    $("#mtype_mraid").on("click", function() { applyMtypeFilter("${Media.Type.MRAID}"); });

${caller.body()}

</%def>