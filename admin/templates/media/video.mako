<%namespace name="bcrumb" file="/layout/bcrumb.mako" />
<%namespace name="fabutton" file="/layout/fa-button.mako" />
<%inherit file="/media/index.mako" />

<%!
    from faan.core.model.adv.media import Media
%>

<%def name="title()">Креативы - Видео</%def>

${ bcrumb.h(self) }


<%def name="render_table(id)">
 <!--START TABLE-->
 <table class="table table-striped table-bordered table-hover" id="${id}">
 <thead>
 <tr>
     <th class="table-checkbox">
         <input type="checkbox" class="group-checkable" data-set="#${id} .checkboxes"/>
     </th>
     <th>Название</th>
     <th>Статус</th>
     <th>Видео</th>
     <th>Endcard</th>
     <th>Overlay</th>
     <th>По нажатию</th>
     <th>Действие</th>
 </tr>
 </thead>
 <tbody>

${caller.body()}

 </tbody>
 </table>
 <!--END TABLE-->
</%def>


<!--CALLBACKS-->

<%def name="callbacks()">

<script type="text/javascript">
    //
    // CALLBACKS
    //

    //
    // MANDATORY FUNCTIONS FOR EACH CHILD TEMPLATES:
    //
    // define_columns
    // parse_table
    // index_status
    // index_action
    //

    // Define columns for the tables.
    function define_columns() {
        return [
            { data: 'checkbox' },
            { data: 'media' },
            { data: 'state' },
            { data: 'video' },
            { data: 'endcard' },
            { data: 'overlay' },
            { data: 'on_click' },
            { data: 'action' }
        ];
    }

    // Parses table parameters.
    // Some values in the cells of the table are required special processing,
    // e.g. they can be a JSON objects or a numeric status constant such
    // constant is required to replace with the status string.
    function parse_table(rows) {
        for (var i = 0; i < rows.length; i++) {
            // Media, Video, Checkbox and Actions
            try {
                var media = JSON.parse(rows[i].cells[1].innerHTML);
                // Checkbox
                rows[i].cells[0].innerHTML = '<input type="checkbox" class="checkboxes" value="{media_id}"/>'
                    .replace("{media_id}", media.id);
                //rows[i].cells[0].innerHTML = '<div class="checker"><span><input type="checkbox" value="' + media.id + '" class="checkboxes"></span></div>';

                var media_url_show = media.uri
                if (media_url_show.length > 25) {
                    media_url_show = media_url_show.substr(0, 22) + "...";
                }
                rows[i].cells[1].innerHTML = ' \
                    <h3 style="margin-top:0"> \
                        <span class="badge badge-info">#{media_id}</span> \
                         <a href="/media/{media_id}">{media_name}</a> \
                    </h3> \
                    <p class="text-info"> \
                        <i class="fa fa-user"></i> <a href="/media/video/?tab={ctab}&account={account_id}">{account_username}</a> \
                    </p> \
                    <p class="text-info"> \
                        <i class="fa fa-bullhorn"></i> <a href="/media/video/?tab={ctab}&campaign={campaign_id}">{campaign_name}</a> \
                    </p> \
                    <p class="text-info"> \
                        <i class="fa fa-external-link"></i> <a href="{media_uri}" target="_blank">{media_uri_show}</a> \
                    </p> \
                    <p class="text-info"> \
                        <i class="fa fa-clock-o"></i>&nbsp{duration}s \
                    </p>'
                    .replace(/{media_id}/g, media.id)
                    .replace("{account_id}", media.account_id)
                    .replace("{media_name}", media.name)
                    .replace("{account_username}", media.account)
                    .replace("{campaign_name}", media.campaign)
                    .replace("{campaign_id}", media.campaign_id)
                    .replace(/{media_uri}/g, media.uri)
                    .replace("{media_uri_show}", media_url_show)
                    .replace(/{ctab}/g, Index.tab)
                    .replace("{duration}", media.duration);
                    //Video
                    var video = JSON.parse(rows[i].cells[3].innerHTML);
                    var preview = video.preview;
                    rows[i].cells[3].innerHTML = ' \
                        <a class="fancyboxvid" href="{media_video}.mp4" onmouseleave="endPreview(event, this)" onmouseenter="startPreview(event, this)"> \
                        <div class="thumbnail vthumb text-center" style="width: 130px; height:100px; margin-bottom: 0;"> \
                            <img src="{preview}" class="vpreview"> \
                            <i class="fa fa-play-circle playbutton"></i> \
                        </div> \
                        </a>'
                        .replace('{media_video}', video.video)
                        .replace('{preview}', preview);

                    // ON CLICK
                    var on_click = rows[i].cells[6].innerHTML;
                    switch(on_click) {
                        case "${Media.ClickActions.CALL}":
                            rows[i].cells[6].innerHTML = '<i class="fa fa-phone" ></i> &nbsp '  + media.phone;
                            break;
                        case "${Media.ClickActions.SMS}":
                            rows[i].cells[6].innerHTML = '<i class="fa fa-envelope" ></i> &nbsp ' + media.phone;
                            break;
                        case "${Media.ClickActions.URL}":
                            rows[i].cells[6].innerHTML = '<i class="fa fa-external-link-square"></i> &nbsp <a href="{media_uri}" target="_blank">{media_uri_show}</a>'
                                .replace(/{media_uri}/g, media.uri)
                                .replace(/{media_uri_show}/g, media_url_show);
                            break;
                        case "${Media.ClickActions.VIDEO}":
                            rows[i].cells[6].innerHTML = ' \
                                <a class="fancyboxvid" href="{media_video}.mp4"> \
                                <div class="thumbnail vthumb text-center" style="width: 130px; height:100px; margin-bottom: 0;"> \
                                    <img src="/global/static/img/icons/video_preview.png" class="vpreview"> \
                                    <i class="fa fa-play-circle playbutton"></i> \
                                </div> \
                                </a>'
                                .replace(/{media_video}/g, banner.video);
                            break;
                        default:
                            rows[i].cells[6].innerHTML = "";
                            break;
                    }

                    //Actions
                    var state = rows[i].cells[2].innerHTML;
                    state = JSON.parse(state);
                    if (state.id == ${Media.State.NONE}) {
                        rows[i].cells[7].innerHTML = '<a onClick="entry_action(event, {action: \'active\', id: \'{media_id}\'})" ><i class="fa fa-check fabutton tooltips" data-placement="top" data-original-title="Активировать"></i></a>'.replace("{media_id}", media.id);
                        rows[i].cells[7].innerHTML += '<a onClick="entry_action(event, {action: \'disable\', id: \'{media_id}\'})" ><i class="fa fa-times fabutton tooltips" data-placement="top" data-original-title="Заблокировать"></i></a>'.replace("{media_id}", media.id);
                    } else if (state.id == ${Media.State.ACTIVE}) {
                        rows[i].cells[7].innerHTML = '<a onClick="entry_action(event, {action: \'disable\', id: \'{media_id}\'})" ><i class="fa fa-times fabutton tooltips" data-placement="top" data-original-title="Заблокировать"></i></a>'.replace("{media_id}", media.id);
                        rows[i].cells[7].innerHTML += '<a onClick="entry_action(event, {action: \'stop\', id: \'{media_id}\'})" ><i class="fa fa-pause fabutton tooltips" data-placement="top" data-original-title="Приостановить"></i></a>'.replace("{media_id}", media.id);
                    } else if (state.id == ${Media.State.DISABLED}) {
                        rows[i].cells[7].innerHTML = '<a onClick="entry_action(event, {action: \'disable\', id: \'{media_id}\'})" ><i class="fa fa-times fabutton tooltips" data-placement="top" data-original-title="Заблокировать"></i></a>'.replace("{media_id}", media.id);
                        rows[i].cells[7].innerHTML += '<a onClick="entry_action(event, {action: \'active\', id: \'{media_id}\'})" ><i class="fa fa-check fabutton tooltips" data-placement="top" data-original-title="Активировать"></i></a>'.replace("{media_id}", media.id);
                    } else  {
                        rows[i].cells[7].innerHTML = '<a onClick="entry_action(event, {action: \'active\', id: \'{media_id}\'})" ><i class="fa fa-check fabutton tooltips" data-placement="top" data-original-title="Активировать"></i></a>'.replace("{media_id}", media.id);
                    }
                    //Endcard
                    var endcard = rows[i].cells[4].innerHTML;
                    if (endcard) {
                        rows[i].cells[4].innerHTML = ' \
                            <div class="thumbnail" style="width: 150px; height:110px; margin-bottom: 0;"> \
                                <a class="fancybox" href="{media_endcard}"> \
                                    <img src="{media_endcard}" rel="group" style="height:100px"> \
                                </a> \
                            </div> '.replace("{media_endcard}", endcard).replace("{media_endcard}", endcard);
                    } else {
                        rows[i].cells[4].innerHTML = ' \
                            <div class="thumbnail" style="width: 150px; height:110px; margin-bottom: 0;"> \
                                <a class="fancybox" href="/global/static/img/default-endcard.png"> \
                                    <img src="/global/static/img/default-endcard.png" rel="group" style="height:100px"> \
                                </a> \
                            </div> '
                    }
                    //Overlay
                    var overlay = rows[i].cells[5].innerHTML;
                    if (overlay) {
                        rows[i].cells[5].innerHTML = ' \
                        <div class="thumbnail" style="width: 150px; height:110px; margin-bottom: 0;"> \
                            <img src="{media_overlay}" style="height:100px"> \
                        </div> '
                        .replace('{media_overlay}', overlay);
                    } else {
                        rows[i].cells[5].innerHTML = "Отсутствует";
                    }

            } catch(e) { }

            // Status
            try {
                var stat = rows[i].cells[2].innerHTML;
                stat = JSON.parse(stat);
                var stat_class = "";
                if (stat.id == "${Media.State.ACTIVE}"){
                    stat_class = "badge-success";
                } else if (stat.id == "${Media.State.DISABLED}") {
                    stat_class = "badge-warning";
                } else if (stat.id == "${Media.State.NONE}") {
                    stat_class = "badge-info";
                } else {
                    stat_class = "badge-danger";
                }
                rows[i].cells[2].innerHTML = '<span class="badge {stat_class}">{stat_value}</span>'
                    .replace("{stat_value}", media_state[stat.id])
                    .replace("{stat_class}", stat_class);
            } catch(e) { }
        }
    }

    // Returns an index of cell with status.
    function index_status() { return 2; }

    // Returns an index of cell with actions.
    function index_action() { return 7; }

    //
    // SPECIFIC FUNCTIONS.
    //

    // Preview functions
    // Starts to show preview.
    function startPreview(e, self) {
        var preview = self.children[0].children[0];
        timerChangeFrame = setInterval(function(){changeFrame(preview)}, 500);
    }

    // Stops the preview showing.
    function endPreview(e, self) {
        var preview = self.children[0].children[0];
        var src = preview.src.split("/");

        switch(src[src.length - 1]) {
            case "video_preview.png":
                break;
            default:
                src.splice(src.length - 1, 1);
                src.splice(0, 3);
                src = src.join("\/").replace(",", "/");
                src += "/preview.png";
                preview.src = "\/" + src;
                break;
        }
        clearInterval(timerChangeFrame)
    }

    // Changes the current preview frame.
    // When the mouse cursor is on a video player, this function changes a
    // frame every N second. N value is 0.5 second by default. You can setup
    // the value in the "startPreview" function.
    function changeFrame(preview) {
        var src = preview.src.split("/");
        switch(src[src.length - 1]) {
            case "video_preview.png":
                break;
            case "preview.png":
                src.splice(src.length - 1, 1);
                src.splice(0, 3);
                src = src.join("\/").replace(",", "/");
                src += "/preview1.png";
                preview.src = "\/" + src;
                break;
            default:
                var frame = Number(src[src.length - 1].split("preview")[1].split(".png")[0]);
                if (frame == 10) {
                    frame = 1;
                } else {
                    frame += 1;
                }
                frame = "preview" + frame;
                src.splice(src.length - 1, 1);
                src.splice(0, 3);
                src = src.join("\/").replace(",", "/");
                src += "/" + frame + ".png";
                preview.src = "/" + src;
                break;
        }

    }

</script>

${caller.body()}

</%def>


<!--DOCUMENT ON READY EXTEND-->
<!--The code below will be added to "document on ready" function.-->
<!--Therefore you should NOT put the code into "script tags.-->
<%def name="document_on_ready_extend()">

    media_state = {
        "${Media.State.NONE}": 'Новый',
        "${Media.State.ACTIVE}": 'Активный',
        "${Media.State.DISABLED}": 'На паузе',
        "${Media.State.SUSPENDED}": 'Заблокированный'
    }

${caller.body()}

</%def>