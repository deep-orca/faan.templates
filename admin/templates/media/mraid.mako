<%namespace name="bcrumb" file="/layout/bcrumb.mako" />
<%namespace name="fabutton" file="/layout/fa-button.mako" />
<%inherit file="/media/index.mako" />

<%!
    from faan.core.model.adv.media import Media
%>

<%def name="title()">Креативы - MRAID</%def>

${ bcrumb.h(self) }


<%def name="render_table(id)">
 <!--START TABLE-->
 <table class="table table-striped table-bordered table-hover" id="${id}">
 <thead>
 <tr>
     <th class="table-checkbox">
         <input type="checkbox" class="group-checkable" data-set="#${id} .checkboxes"/>
     </th>
     <th>Название</th>
     <th>Статус</th>
     <th>Действие</th>
 </tr>
 </thead>
 <tbody>

${caller.body()}

 </tbody>
 </table>
 <!--END TABLE-->
</%def>


<!--CALLBACKS-->

<%def name="callbacks()">

<script type="text/javascript">
    //
    // CALLBACKS
    //

    //
    // MANDATORY FUNCTIONS FOR EACH CHILD TEMPLATES:
    //
    // define_columns
    // parse_table
    // index_status
    // index_action
    //

    // Define columns for the tables.
    function define_columns() {
        return [
            { data: 'checkbox' },
            { data: 'media' },
            { data: 'state' },
            { data: 'action' }
        ];
    }

    // Parses table parameters.
    // Some values in the cells of the table are required special processing,
    // e.g. they can be a JSON objects or a numeric status constant such
    // constant is required to replace with the status string.
    function parse_table(rows) {
        for (var i = 0; i < rows.length; i++) {
            // Media, Checkbox and Actions
            try {
                var media = JSON.parse(rows[i].cells[1].innerHTML);
                // Checkbox
                rows[i].cells[0].innerHTML = '<input type="checkbox" class="checkboxes" value="{media_id}"/>'
                    .replace("{media_id}", media.id);

                rows[i].cells[1].innerHTML = ' \
                    <h3 style="margin-top:0"> \
                        <span class="badge badge-info">#{media_id}</span> \
                         <a href="/media/{media_id}">{media_name}</a> \
                    </h3> \
                    <p class="text-info"> \
                        <i class="fa fa-user"></i> <a href="/media/mraid/?tab={ctab}&account={account_id}">{account_username}</a> \
                    </p> \
                    <p class="text-info"> \
                        <i class="fa fa-bullhorn"></i> <a href="/media/mraid/?tab={ctab}&campaign={campaign_id}">{campaign_name}</a> \
                    </p>'
                    .replace(/{media_id}/g, media.id)
                    .replace("{account_id}", media.account_id)
                    .replace("{media_name}", media.name)
                    .replace("{account_username}", media.account)
                    .replace("{campaign_name}", media.campaign)
                    .replace("{campaign_id}", media.campaign_id)
                    .replace(/{ctab}/g, Index.tab);

                    // Show URL if it exists.
                    var media_url_show = media.uri;
                    if (media_url_show && media_url_show.length > 25)
                        media_url_show = media_url_show.substr(0, 22) + "...";
                    if(media_url_show) {
                        rows[i].cells[1].innerHTML += ' \
                            <p class="text-info"> \
                                <i class="fa fa-external-link"></i> <a href="{media_uri}" target="_blank">{media_uri_show}</a> \
                            </p>'
                            .replace(/{media_uri}/g, media.uri)
                            .replace(/{media_uri_show}/g, media_url_show);
                    }

                    // Actions
                    var iAction = index_action();
                    var state = rows[i].cells[2].innerHTML;
                    state = JSON.parse(state);
                    if (state.id == ${Media.State.NONE}) {
                        rows[i].cells[iAction].innerHTML = '<a href="{mraid_path}" ><i class="fa fa-download fabutton tooltips" data-placement="top" data-original-title="Загрузить"></i></a>'.replace("{mraid_path}", media.mraid);
                        rows[i].cells[iAction].innerHTML += '<a onClick="entry_action(event, {action: \'active\', id: \'{media_id}\'})" ><i class="fa fa-check fabutton tooltips" data-placement="top" data-original-title="Активировать"></i></a>'.replace("{media_id}", media.id);
                        rows[i].cells[iAction].innerHTML += '<a onClick="entry_action(event, {action: \'disable\', id: \'{media_id}\'})" ><i class="fa fa-times fabutton tooltips" data-placement="top" data-original-title="Заблокировать"></i></a>'.replace("{media_id}", media.id);
                    } else if (state.id == ${Media.State.ACTIVE}) {
                        rows[i].cells[iAction].innerHTML = '<a href="{mraid_path}" ><i class="fa fa-download fabutton tooltips" data-placement="top" data-original-title="Загрузить"></i></a>'.replace("{mraid_path}", media.mraid);
                        rows[i].cells[iAction].innerHTML += '<a onClick="entry_action(event, {action: \'disable\', id: \'{media_id}\'})" ><i class="fa fa-times fabutton tooltips" data-placement="top" data-original-title="Заблокировать"></i></a>'.replace("{media_id}", media.id);
                        rows[i].cells[iAction].innerHTML += '<a onClick="entry_action(event, {action: \'stop\', id: \'{media_id}\'})" ><i class="fa fa-pause fabutton tooltips" data-placement="top" data-original-title="Приостановить"></i></a>'.replace("{media_id}", media.id);
                    } else if (state.id == ${Media.State.DISABLED}) {
                        rows[i].cells[iAction].innerHTML = '<a href="{mraid_path}" ><i class="fa fa-download fabutton tooltips" data-placement="top" data-original-title="Загрузить"></i></a>'.replace("{mraid_path}", media.mraid);
                        rows[i].cells[iAction].innerHTML += '<a onClick="entry_action(event, {action: \'disable\', id: \'{media_id}\'})" ><i class="fa fa-times fabutton tooltips" data-placement="top" data-original-title="Заблокировать"></i></a>'.replace("{media_id}", media.id);
                        rows[i].cells[iAction].innerHTML += '<a onClick="entry_action(event, {action: \'active\', id: \'{media_id}\'})" ><i class="fa fa-check fabutton tooltips" data-placement="top" data-original-title="Активировать"></i></a>'.replace("{media_id}", media.id);
                    } else  {
                        rows[i].cells[iAction].innerHTML = '<a onClick="entry_action(event, {action: \'active\', id: \'{media_id}\'})" ><i class="fa fa-check fabutton tooltips" data-placement="top" data-original-title="Активировать"></i></a>'.replace("{media_id}", media.id);
                    }
            } catch(e) { }

            // Status
            try {
                var stat = rows[i].cells[2].innerHTML;
                stat = JSON.parse(stat);
                var stat_class = "";
                if (stat.id == "${Media.State.ACTIVE}"){
                    stat_class = "badge-success";
                } else if (stat.id == "${Media.State.DISABLED}") {
                    stat_class = "badge-warning";
                } else if (stat.id == "${Media.State.NONE}") {
                    stat_class = "badge-info";
                } else {
                    stat_class = "badge-danger";
                }
                rows[i].cells[2].innerHTML = '<span class="badge {stat_class}">{stat_value}</span>'
                    .replace("{stat_value}", media_state[stat.id])
                    .replace("{stat_class}", stat_class);
            } catch(e) { }
        }
    }

    // Returns an index of cell with status.
    function index_status() { return 2; }

    // Returns an index of cell with actions.
    function index_action() { return 3; }

    //
    // SPECIFIC FUNCTIONS.
    //


</script>

${caller.body()}

</%def>


<!--DOCUMENT ON READY EXTEND-->
<!--The code below will be added to "document on ready" function.-->
<!--Therefore you should NOT put the code into "script tags.-->
<%def name="document_on_ready_extend()">

    media_state = {
        "${Media.State.NONE}": 'Новый',
        "${Media.State.ACTIVE}": 'Активный',
        "${Media.State.DISABLED}": 'На паузе',
        "${Media.State.SUSPENDED}": 'Заблокированный'
    }

${caller.body()}

</%def>