## -*- coding: utf-8 -*-

<%namespace name="bcrumb" file="/layout/bcrumb.mako" />

<%
    import json
%>

<%inherit file="/layout/main.mako" />

## Page JS
<%def name="page_js()">
    <script src="/global/static/plugins/moment-with-langs.min.js" type="text/javascript"></script>
</%def>
## END

## Page CSS
<%def name="page_css()">
    <link href="//netdna.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet">
    <style>
        .text-bold { font-weight: bold; }
        .error { padding: 2px 5px; }
        .text-help { font-style: oblique; color: #aaa; padding: 2px 5px; }
    </style>
</%def>
## END

<%def name="title()">Новости</%def>
<%def name="description()"></%def>

${bcrumb.h(self)}
${bcrumb.bc([{'name': u'Новости', 'href' : '/news/', 'icon' : 'bullhorn'}, ], [])}

<div class="row">
    <div class="col-md-12">
        <div class="portlet">
            <div class="portlet-title">
                <div class="caption">
                    Добавление новости
                </div>
            </div>
            <div class="portlet-body" id="portlet-render">
                <form class="form-horizontal" action="" method="post" role="form">
                    %for field in form:

                        %if field.name == "csrf_token":
                            ${field}
                        %else:
                            <div class="form-group ${'has-error' if field.errors else ''}">
                            ${field.label(class_="control-label col-md-3")}
                                <div class="col-sm-6 col-md-6">
                                    %if field.name == "message":
                                        ${field(class_="form-control", rows=10)}
                                    %else:
                                        ${field(class_="form-control")}
                                    %endif

                                    %if field.description:
                                        <small class="text-help">${field.description}</small>
                                    %endif

                                    %if field.errors:
                                        <ul class="text-danger text-bold error list-unstyled">
                                            %for error in field.errors:
                                                <li>${error}</li>
                                            %endfor
                                        </ul>
                                    %endif

                                </div>
                            </div>
                        %endif
                    %endfor
                    <div class="form-group">
                        <div class="col-md-offset-3 col-sm-12 col-md-6">
                            <button class="btn btn-success" type="submit">Сохранить</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(document).on("ready", function () {
        var currentTimezone = +(moment().format("ZZ")) * 36000;
        $("*[data-timestamp]").each(function () {
            var self = $(this);
            self.text(moment(self.data("timestamp") + currentTimezone).format("DD.MM.YYYY HH:mm:ss"))
        });

        $("#tags").select2({
            tags: ${json.dumps([t.name for t in g.tags])},
            tokenSeparators: [",", "  "]
        });
    });
</script>