## -*- coding: utf-8 -*-

<%namespace name="bcrumb" file="/layout/bcrumb.mako" />

<%
    import time
    from datetime import datetime
    from faan.web.app import account
    from faan.core.model.news import News
%>

<%inherit file="/layout/main.mako" />

## Page JS
<%def name="page_js()">
    <script src="/global/static/plugins/moment-with-langs.min.js" type="text/javascript"></script>
</%def>
## END

## Page CSS
<%def name="page_css()">
    <link href="//netdna.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet">
    <style>
        .list-inline li a { color: #eee; }
        .list-inline li:first-child { padding-left: 5px; }
        .text-bold { font-weight: bold; }
        .error { padding: 2px 5px; }
        .text-help { font-style: oblique; color: #aaa; }
        .as-block { display: block; }
    </style>
</%def>
## END

<%def name="title()">Новости</%def>
<%def name="description()"></%def>

${bcrumb.h(self)}

${bcrumb.bc([{'name': u'Новости', 'href': '/news/', 'icon': 'bullhorn'}, ], [])}

<div class="row">
    <div class="col-md-12">
        <div class="tags-cloud">
            <ul class="list-inline">
                <li>Теги:</li>
                <li class="label ${'label-primary' if not g.current_tag else 'label-default'}"
                    id="tag-item-0"><a href="?tag=all">Все</a></li>
                %for tag_id, tag_data in g.tags.iteritems():
                    <li class="label ${'label-primary' if (g.current_tag and g.current_tag == tag_id) else 'label-default'}" id="tag-item-${tag_id}"><a href="?tag=${tag_id}">${tag_data.name}</a></li>
                %endfor
            </ul>
        </div>
        <div class="portlet">
            <div class="portlet-title">
                <div class="caption">
                    Список новостей
                </div>
                <div class="actions">
                    <a href="new" class="btn btn-success btn-sm">
                        <i class="fa fa-plus"></i>
                        Создать
                    </a>
                </div>
            </div>
            <div class="portlet-body" id="portlet-render">
                <table class="table">
                    <thead>
                        <tr>
                            <th>Важность</th>
                            <th>Заголовок</th>
                            <th>Автор</th>
                            <th>Теги</th>
                            <th></th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        %for news_item in g.news_list:
                            <tr>
                                <td>
                                    <span class="label label-${g.priorities.get(news_item.priority)[1]}">${g.priorities.get(news_item.priority)[0]}</span>
                                </td>
                                <td>
                                    <a href="/news/${news_item.id}">${news_item.title}</a>
                                    <a href="#view"></a>
                                </td>
                                <td>
                                    <a href="/profile/${news_item.created_by}" target="_blank">${g.account_names.get(news_item.created_by)}</a>
                                    <small class="text-help as-block" data-timestamp="${news_item.ts_created*1000}"></small>
                                </td>
                                <td>
                                    <ul class="list-inline">
                                        %if news_item.tags:
                                            %for tag in news_item.tags:
                                                <li class="label label-default"><a href="?tag=${tag}">${g.tags.get(tag).name}</a></li>
                                            %endfor
                                        %else:
                                            <li>&mdash;</li>
                                        %endif
                                    </ul>
                                </td>
                                <td></td>
                                <td></td>
                            </tr>
                        %endfor
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(document).on("ready", function() {
        var currentTimezone = +(moment().format("ZZ")) * 36000;
        $("*[data-timestamp]").each(function() {
            var self = $(this);
            self.text(moment(self.data("timestamp") + currentTimezone).format("DD.MM.YYYY HH:mm:ss"))
        });
    });
</script>


