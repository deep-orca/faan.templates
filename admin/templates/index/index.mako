## -*- coding: utf-8 -*-

<%namespace name="bcrumb" file="/layout/bcrumb.mako" />

<%inherit file="/layout/main.mako" />

## Page JS
<%def name="page_js()">
    <script src="/global/static/plugins/moment-with-langs.min.js" type="text/javascript"></script>
    <script src="/global/static/plugins/bootstrap-daterangepicker/daterangepicker.js" type="text/javascript"></script>
    <script src="/global/static/plugins/flot/jquery.flot.js" type="text/javascript"></script>
    <script src="/global/static/plugins/flot/jquery.flot.resize.js" type="text/javascript"></script>
    <script src="/global/static/plugins/flot/jquery.flot.time.js" type="text/javascript"></script>
    <script src="/global/static/scripts/admin_dashboard.js" type="text/javascript"></script>
</%def>
## END

## Page CSS
<%def name="page_css()">
    <link href="/global/static/css/plugins.css" rel="stylesheet" type="text/css"/>
    <link href="/global/static/css/pages/dashboard.css" rel="stylesheet" type="text/css"/>
    <link href="/global/static/plugins/fullcalendar/fullcalendar/fullcalendar.css" rel="stylesheet" type="text/css"/>
    <link href="/global/static/plugins/bootstrap-daterangepicker/daterangepicker-bs3.css" rel="stylesheet" type="text/css"/>
</%def>
## END

<%def name="title()">Администратор</%def>
<%def name="description()">Главная</%def>

${bcrumb.h(self)}

${bcrumb.bc(
    [
        {
            'name': u'Администратор',
            'href' : '/',
            'icon' : 'rocket'
        },
        {
            'name': u'Главная',
            'href' : '/'
        }
    ],
    [
        {
            'type': 'datepicker',
            'title': u'Выберите дату'
        }
    ]
)}

<div class="clearfix">
</div>
<div class="row">
    <div class="col-md-6 col-sm-12">
        <div class="portlet">
            <div class="portlet-title" style="height:50px;">
                <div class="caption">
                    <i class="fa fa-bar-chart-o"></i><small>Динамика показов и реквестов</small>
                </div>
            </div>
            <div class="portlet-body">
                <div id="views_dynamics_daily_loading">
                    <img src="/global/static/img/loading.gif" alt="loading"/>
                </div>
                <div id="views_dynamics_daily_content" class="display-none">
                    <div id="views_dynamics_daily" class="chart">
                    </div>
                    <div class="legend_holder"></div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-6 col-sm-12">
        <div class="portlet">
            <div class="portlet-title" style="height:50px;">
                <div class="caption">
                    <i class="fa fa-bar-chart-o"></i><small>Средний eCPM и прибыль</small>
                </div>
            </div>
            <div class="portlet-body">
                <div id="views_dynamics_loading">
                    <img src="/global/static/img/loading.gif" alt="loading"/>
                </div>
                <div id="views_dynamics_content" class="display-none">
                    <div id="views_dynamics" class="chart">
                    </div>
                    <div class="legend_holder"></div>
                </div>
            </div>
        </div>
    </div>

    <div class="col-md-6 col-sm-12">
        <div class="portlet">
            <div class="portlet-title" style="height:50px;">
                <div class="caption">
                    <i class="fa fa-rocket"></i><small>TOP площадок по показам</small>
                </div>
                <div class="pull-right">
                    <input class="input-xsmall" name="filter_top_app_name" type="select" id="filter_top_app" value="${g.num_top_apps}" style="width:50px"/>
                </div>
            </div>
            <div class="portlet-body">
                <div id="top_apps_loading">
                    <img src="/global/static/img/loading.gif" alt="loading"/>
                </div>
                <div id="top_apps_content" class="display-none">
                    <div id="top_apps" class="chart">
                    </div>
                    <div class="legend_holder"></div>
                </div>
            </div>
        </div>
    </div>

    <div class="col-md-6 col-sm-12">
        <div class="portlet">
            <div class="portlet-title" style="height:50px;">
                <div class="caption">
                    <i class="fa fa-bullhorn"></i><small>TOP кампаний по показам</small>
                </div>
                <div class="pull-right">
                    <input class="input-xsmall" name="filter_top_campaign_name" type="select" id="filter_top_campaign" value="${g.num_top_cams}" style="width:50px"/>
                </div>
            </div>
            <div class="portlet-body">
                <div id="top_campaigns_loading">
                    <img src="/global/static/img/loading.gif" alt="loading"/>
                </div>
                <div id="top_campaigns_content" class="display-none">
                    <div id="top_campaigns" class="chart">
                    </div>
                    <div class="legend_holder"></div>
                </div>
            </div>
        </div>
    </div>

##    <div class="clearfix"></div>
##    <div class="col-md-6 col-sm-12">
##        <div class="portlet">
##            <div class="portlet-title">
##                <div class="caption">
##                    <i class="fa fa-bar-chart-o"></i><small>Динамика показов</small>
##                </div>
##            </div>
##            <div class="portlet-body">
##                <div id="views_dynamics_loading">
##                    <img src="/global/static/img/loading.gif" alt="loading"/>
##                </div>
##                <div id="views_dynamics_content" class="display-none">
##                    <div id="views_dynamics" class="chart">
##                    </div>
##                    <div class="legend_holder"></div>
##                </div>
##            </div>
##        </div>
##    </div>
</div>
<div class="clearfix"></div>
<div class="row">
    <div class="col-md-12 col-sm-12">
        <div class="portlet">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-table"></i><small>Данные</small>
                </div>
            </div>
            <div class="portlet-body">
                <div class="table-responsive">
                    <table id="apps_table" class="table table-hover">
                        <thead>
                            <tr>
                                <th>
                                     Дата
                                </th>
                                <th>
                                     Реквесты
                                </th>
                                <th>
                                     Показы
                                </th>
                                <th>
                                     Клики
                                </th>
                                <th>
                                     CPM
                                </th>
                                <th>
                                     CTR
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                            ## Here be dynamically created table
                        </tbody>
                        <tfoot>
                        <tr>
                            <th class="text-left">Всего</th>
                            <th class="text-left"></th>
                            <th class="text-left"></th>
                            <th class="text-left"></th>
                            <th class="text-left"></th>
                            <th class="text-left"></th>
                        </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>


<script type="text/javascript">
    jQuery(document).ready(function() {
        Index.init();
        Index.initDashboardDaterange();
        Index.initDashboard();
        Index.initFilters();
    });
</script>
