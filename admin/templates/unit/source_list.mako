## -*- coding: utf-8 -*-
<%namespace name="bcrumb" file="/layout/bcrumb.mako" />
<%namespace name="fabutton" file="/layout/fa-button.mako" />
<%inherit file="/layout/main.mako" />

<%def name="title()">Источники</%def>
<%def name="description()">Источники блока [${g.unit.id}] ${g.unit.name}</%def>
<%!
    import datetime
    from faan.core.model.pub.source import Source
    from faan.core.model.adv.media import Media
%>
${ bcrumb.h(self) }
${ bcrumb.bc([ { 'name' : u'Блоки',  'href' : '/unit/', 'icon' : 'cube' }
             , { 'name' : u"[%s] %s" % (g.unit.id, g.unit.name), 'href' : '/unit/%s/source' % (g.unit.id) }
             , { 'name' : u'Источники', 'href' : '/unit/%s/source' % (g.unit.id) }
             ]) }

<!-- BEGIN PAGE CONTENT-->
<div class="row">
    <div class="col-md-12">

    <div class="portlet">
        <div class="portlet-title">
            <div class="caption"><i class="fa fa-sitemap"></i>${self.description()}</div>
            <div class="actions">
                <div class="btn-group">
                    <a data-toggle="dropdown" class="btn btn-success dropdown-toggle " href="#">
                        <i class="fa fa-plus"></i>
                        Добавить новый
                        <i class="fa fa-angle-down"></i>
                    </a>
                    <ul class="dropdown-menu pull-right">
                        %for source_type in g.available_source_types:
                        <li>
                            <a href="/source/${g.unit.id}/new?type=${source_type[0]}">
                                Источник ${source_type[1]}
                            </a>
                        </li>
                        %endfor
                    </ul>
                </div>
            </div>
        </div>
        
        <div class="portlet-body">
            <table class="table table-striped table-bordered table-hover" id="units_table">
            <thead>
            <tr>
                <th style="width:15px;"><input type="checkbox" class="group-checkable" data-set="#units_table .checkboxes"/></th>
                <th>Название</th>
                <th class="text-center">Тип источника</th>
                <th class="text-center">Статус</th>
                <th class="text-center">Параметры</th>
                <th class="text-center">ECPM</th>
            </tr>
            </thead>
            <tbody>
            % for s in g.sources:
            <tr class="odd gradeX">                                                             <!-- TODO: add actual columns -->
                <td><input type="checkbox" class="checkboxes" value="${s.id}"/></td>
                <td>
                    <div class="row">
                        <div class="col-md-8">
                            
                            <h4 style="margin-top:0;">
                                <span class="badge badge-default">#${s.id}</span>
                                <a href="/source/${g.unit.id}/${s.id}">${s.name}</a>
                            </h4>
                            <p class="text-left">
                                <%
                                    today = datetime.date.today()
                                    monthago = today - datetime.timedelta(days=30)
                                %>
                                ${ fabutton.fab([{ 'text' : u'Активировать',  'href' : '/source/%d/%d/state/1'%(g.unit.id, s.id), 'icon' : 'play' }]) if s.state == 2 else fabutton.fab([{ 'text' : u'Остановить',  'href' : '/source/%d/%d/state/2'%(g.unit.id, s.id), 'icon' : 'pause' }])
                                }
                                ${ fabutton.fab([ 
                                    { 'text' : u'Редактировать',  'href' : '/source/%d/%d'%(g.unit.id, s.id), 'icon' : 'pencil' },
                                    { 'text' : u'Статистика',  'href' : '/stats/pub/days/?date=%s+-+%s&unit=%d&source=%d'%(unicode(monthago.strftime('%d.%m.%Y')), unicode(today.strftime('%d.%m.%Y')), g.unit.id, s.id), 'icon' : 'bar-chart-o' },
                                    {
                                        'text': u'Удалить',
                                        'href': '/source/%d/%d/delete' % (g.unit.id, s.id),
                                        'icon': 'trash-o',
                                        'data': {
                                            'aid': '%d' % g.unit.id,
                                            'zid': '%d' % s.id
                                        },
                                        'class': 'delete_source'
                                    }
                                ]) 
                                }
                            </p>
                        </div>
                        <div class="col-md-4 text-right">

                        </div>
                    </div>
                </td>

                <td>
                    %if s.type == Source.Type.VIDIGER:
                        VIDIGER
                    %elif s.type == Source.Type.CUSTOM:
                        CUSTOM
                    %elif s.type == Source.Type.ADMOB:
                        ADMOB
                    %elif s.type == Source.Type.ADFOX:
                        ADFOX
                    %elif s.type == Source.Type.BACKFILL:
                        BACKFILL
                    %endif
                </td>

                <td class="text-center">
                    ${fabutton.fas([{'status': s.state, 'type': 'unit'}])}
                </td>
                <td>
                    %for k, v in s.parameters.items():
                        %if k == 'media_types':
                            <b>${k}</b>:
                            %for media_type in v:
                                %if media_type == Media.Type.BANNER:
                                    BANNER,
                                %elif media_type == Media.Type.VIDEO:
                                    VIDEO,
                                %elif media_type == Media.Type.MRAID:
                                    MRAID
                                %endif
                            %endfor
                        %else:
                            <b>${k}</b>: ${v}<br>
                        %endif
                    %endfor
                </td>
                <td> ${s.ecpm} </td>
            </tr>
            % endfor
            </tbody>
            </table>
        </div>
    </div>
 
</div>
</div>

<div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog" style="width: 25%">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="myModalLabel">Удалить источники?</h4>
      </div>
      <div class="modal-body">
        <div class="pasta form-group">
            Действительно удалить источники <span id="del_list"></span> ?

        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
        <button type="button" id="delete" class="btn btn-danger group_actions">Удалить</button>
      </div>
    </div>
  </div>
</div>
<!-- END PAGE CONTENT-->

<script type="text/javascript">
    var deleteModal = $('#deleteModal');
    $('.delete_source').on('click', function() {
        var aid = $(this).data('aid'),
            zid = $(this).data('zid');
        $('#delete').one('click', function() {
            window.location = '/unit/' + aid + '/source/' + zid + '/delete';
        });
        if (zid) {
            deleteModal.modal();
            $('#del_list').html('').html(zid);
        }
    });

    $('#units_table').dataTable({
         "aoColumns": [
          { "bSortable": false, "bSearchable": false },
          { "bSearchable": true },
          { "bSortable": false, "bSearchable": false },
          { "bSearchable": false },
          { "bSearchable": false },
          { "bSearchable": false }
        ],
        "aLengthMenu": [
            [10, 20, 50, -1],
            [10, 20, 50, "Все"]  // change per page values here
        ],
        // set the initial value
        "iDisplayLength": 10,
        "sPaginationType": "bootstrap",
        "oLanguage": {
            "sLengthMenu": "_MENU_ блоков",
            "oPaginate": {
                "sPrevious": "Назад",
                "sNext": "Вперед"
            },
            "sSearch": "Поиск: ",
            "sInfo" : "Показано с _START_ по _END_ из _TOTAL_ блоков",
            "sInfoempty" : "У Вас пока не добавлено ни одного блока"
        },
        "aoColumnDefs": [{
                'bSortable': false,
                'aTargets': [0]
            }
        ]
    });

    jQuery('#units_table .group-checkable').change(function () {
        var set = jQuery(this).attr("data-set");
        var checked = jQuery(this).is(":checked");
        jQuery(set).each(function () {
            if (checked) {
                $(this).attr("checked", true);
            } else {
                $(this).attr("checked", false);
            }
        });
        jQuery.uniform.update(set);
    });

    jQuery('#units_table_wrapper .dataTables_filter input').addClass("form-control input-small"); // modify table search input
    jQuery('#units_table_wrapper .dataTables_length select').addClass("form-control input-xsmall"); // modify table per page dropdown
    jQuery('#units_table_wrapper .dataTables_length select').select2(); // initialize select2 dropdown
</script>
