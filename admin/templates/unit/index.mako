<%namespace name="bcrumb" file="/layout/bcrumb.mako" />
<%namespace name="fabutton" file="/layout/fa-button.mako" />
<%inherit file="/layout/main.mako" />

<%!
    from faan.admin.controllers.unit import Tabs
    from faan.core.model.pub.unit import Unit
%>

<%

    unit_type_dict = {
        Unit.Type.BANNER: u"Баннер",
        Unit.Type.INTERSTITIAL: u"Полноэкранный",
    }
    unit_type = {
        'name': 'unit_type',
        'header': u'Тип блока',
        'id': 'select_unit_type',
        'dict': unit_type_dict,
        'help_string': '',
        'data': unit_type_dict,
    }
    unit_size_dict = {
        "320x50": "320x50",
        "320x100": "320x100",
        "300x250": "300x250",
        "468x60": "468x60",
        "728x90": "728x90",
        "320x480": "320x480",
        "1024x768": "1024x768",
    }
    unit_size = {
        'name': 'unit_size',
        'header': u'Размер блока',
        'id': 'select_unit_size',
        'dict': unit_size_dict,
        'help_string': '',
        'data': unit_size_dict,
    }
    
    g.selects.append(unit_type)
    g.selects.append(unit_size)

    for s in g.selects:
        if s.get('name') == 'account':
            s.update({'header': u'Аккаунт'})
        if s.get('name') == 'application':
            s.update({'header': u'Приложение'})
%>

<%def name="title()">Блоки</%def>
<%def name="description()"></%def>

<%def name="page_css()">
<link href="/global/static/plugins/bootstrap-modal/css/bootstrap-modal-bs3patch.css" rel="stylesheet" type="text/css"/>
<link href="/global/static/plugins/bootstrap-modal/css/bootstrap-modal.css" rel="stylesheet" type="text/css"/>
<style>
.playbutton{
    font-size: 36px;
    position: absolute;
    margin-top: -50px;
    margin-left: -15px;
    color: #5AB5FD;
}
.playbutton:hover{
    color:#fff;
}

.text-muted:hover{
    color: #4889BD;
}
</style>
</%def>

<script type="text/javascript" src="/static/scripts/filters.js"></script>

${ bcrumb.h(self) }

<%def name="render_table(id)">
 <!--START TABLE-->
 <table class="table table-striped table-bordered table-hover" id="${id}">
 <thead>
 <tr>
     <th class="table-checkbox">
         <input type="checkbox" class="group-checkable" data-set="#${id} .checkboxes"/>
     </th>
     <th>Название</th>
     <th class="text-center">Тип блока</th>
     <th class="text-center">Статус</th>
     <th class="text-center">Действия</th>
 </tr>
 </thead>
 <tbody>

${caller.body()}

 </tbody>
 </table>
 <!--END TABLE-->
</%def>

<%def name="show_select_item(select)">
    <div class="form-group">
        <label for="${select['name']}" class="control-label col-md-3">${unicode(select['header'])}</label>
        <div class="col-md-3">
                <input name="${select['name']}" type="hidden" id="${select['id']}" style="width:324px"/>
        </div>
    </div>
</%def>

<%def name="show_filters(selects)">
    %for select in selects:
        ${self.show_select_item(select)}
    %endfor
    <!-- ==== SUBMIT ==== --->
    <div class="form-group">
    <label class="control-label col-md-3"></label>
        <div class="col-sm-1">
            <a href="#" id="apply_changes" class="btn btn-success">Применить</a>
        </div>
    </div>
</%def>

<!-- BEGIN PAGE CONTENT-->
<div class="row">
<div class="col-md-12">
    <div class="portlet">
        <div class="portlet-title">

        <div class="caption"><i class="fa fa-sitemap"></i>Блоки</div>
            <div class="actions">
                <div class="btn-group">
                    <a class="btn btn-info" href="#" data-toggle="dropdown">
                        <i class="fa fa-cogs"></i> Действие <i class="fa fa-angle-down"></i>
                    </a>
                    <ul class="dropdown-menu pull-right">
                        <li><a href="#group_action_approve" data-toggle="modal" id="disable" class="group_actions" ><i class="fa fa-times"></i> Остановить</a></li>
                        <li><a href="#group_action_approve" data-toggle="modal" id="active" class="group_actions" ><i class="fa fa-play"></i> Активировать</a></li>
                        <li><a href="#group_action_approve" data-toggle="modal" id="delete" class="group_actions" ><i class="fa fa-trash-o"></i> Удалить</a></li>
                    </ul>
                </div>
            </div>
        </div>

        <div class="portlet-body">

                <form method="GET" class="form-horizontal">
                    ${self.show_filters(g.selects)}
			    </form>

                <ul class="nav nav-pills">
                    %if getattr(g, 'tab', '') and g.tab == Tabs.NEW:
                    <li class="active">
                    %else:
                    <li class="">
                    %endif
                        <a href="#tab_2_1" data-toggle="tab" id="tab_${Tabs.NEW}">Новые</a>
                    </li>
                    %if getattr(g, 'tab', '') and g.tab == Tabs.ACTIVE:
                    <li class="active">
                    %else:
                    <li class="">
                    %endif
                        <a href="#tab_2_2" data-toggle="tab" id="tab_${Tabs.ACTIVE}">Активные</a>
                    </li>
                    %if getattr(g, 'tab', '') and g.tab == Tabs.BANNED:
                    <li class="active">
                    %else:
                    <li class="">
                    %endif
                        <a href="#tab_2_3" data-toggle="tab" id="tab_${Tabs.BANNED}">Остановленные</a>
                    </li>
                </ul>
                <div class="tab-content">
                    %if getattr(g, 'tab', '') and g.tab == Tabs.NEW:
                    <div class="tab-pane fade active in" id="tab_2_1">
                    %else:
                    <div class="tab-pane fade" id="tab_2_1">
                    %endif
                        <p>
                        <%self:render_table id="stats_table_${str(Tabs.NEW)}">

                        </%self:render_table>
                        </p>
                        <div class="row">
                            <div class="col-md-6 col-sm-12"></div>
                            <div class="col-md-6 col-sm-12">
                                <div id="log_table_filter_${Tabs.NEW}" class="dataTables_filter">
                                    <label>
                                    &nbsp
                                        <ul class="pagination">
                                            <li class="prev disabled"><a title="Prev" href="#"><i class="fa fa-angle-left"></i></a></li>
                                            <li class="next"><a title="Next" href="#"><i class="fa fa-angle-right"></i></a></li>
                                        </ul>
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>
                    %if getattr(g, 'tab', '') and g.tab == Tabs.ACTIVE:
                    <div class="tab-pane fade active in" id="tab_2_2">
                    %else:
                    <div class="tab-pane fade" id="tab_2_2">
                    %endif
                        <p>
                        <%self:render_table id="stats_table_${str(Tabs.ACTIVE)}">

                        </%self:render_table>
                        </p>
                        <div class="row">
                            <div class="col-md-6 col-sm-12"></div>
                            <div class="col-md-6 col-sm-12">
                                <div id="log_table_filter_${Tabs.ACTIVE}" class="dataTables_filter">
                                    <label>
                                    &nbsp
                                        <ul class="pagination">
                                            <li class="prev disabled"><a title="Prev" href="#"><i class="fa fa-angle-left"></i></a></li>
                                            <li class="next"><a title="Next" href="#"><i class="fa fa-angle-right"></i></a></li>
                                        </ul>
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>
                    %if getattr(g, 'tab', '') and g.tab == Tabs.BANNED:
                    <div class="tab-pane fade active in" id="tab_2_3">
                    %else:
                    <div class="tab-pane fade" id="tab_2_3">
                    %endif
                        <p>
                        <%self:render_table id="stats_table_${str(Tabs.BANNED)}">

                        </%self:render_table>
                        </p>
                        <div class="row">
                            <div class="col-md-6 col-sm-12"></div>
                            <div class="col-md-6 col-sm-12">
                                <div id="log_table_filter_${Tabs.BANNED}" class="dataTables_filter">
                                    <label>
                                    &nbsp
                                        <ul class="pagination">
                                            <li class="prev disabled"><a title="Prev" href="#"><i class="fa fa-angle-left"></i></a></li>
                                            <li class="next"><a title="Next" href="#"><i class="fa fa-angle-right"></i></a></li>
                                        </ul>
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
        </div>
    </div>
</div>
</div>
<!-- END PAGE CONTENT-->

<div id="group_action_approve" class="modal fade" tabindex="-1" data-width="760" style="top:10%">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
        <h4 class="modal-title"><span id="approve_header"></span></h4>
    </div>
    <form method="POST" class="form-horizontal">
        <div class="modal-body">
            <div class="row">
                <div class="col-md-10">
                    <p>
                        Вы действительно хотите <span id='approve_chose_action'></span> блоки <b><span id="approve_elements_list"></span></b> ?
                    </p>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" data-dismiss="modal" class="btn btn-default">Отменить</button>
            <a href="#" data-dismiss="modal" id="button_group_action_approve" class="btn btn-warning">Подтвердить</a>
        </div>
    </form>
</div>

<!-- SCRIPTS INDEX -->
<script type="text/javascript">
Index = function() {
    return {

        // Some table attributes
        data: null,
        total: +0,

        // Pagination options
        pag_size: +50,
        pag_start: 0,

        tab: null,
        mID: null,
        fmtype: null,

        init: function() {
            $("#apply_changes").on("click", function(e) {
                Index["loadData_" + Index.tab]()
            });

            %for tab in Tabs.tabs():
            $("#tab_${tab}").on("click", function(e) {
                Index.tab = "${tab}";
                Index.pag_size = + 50;
                Index.pag_start = + 0;
                Index.loadData_${tab}();
            });
            %endfor

            %if getattr(g, 'tab', ''):
                this.tab = "${g.tab}";
            %else:
                this.tab = "${Tabs.NEW}";
            %endif

            this.first = true;
        },

        %for tab in Tabs.tabs():
        loadData_${tab}: function(start, count, search) {
            try { $("#stats_table_${tab}").DataTable().destroy(); } catch (e) { }

            var parameters = {
                start: start ? start : 0,
                count: count ? count : Index.pag_size,
                search: search ? search : ""
            }

            var url = "/unit/ajax/table?tab=${tab}" + "&values=" + filterBox.getValues();

            for (var p in parameters)
                url += "&" + p + "=" + parameters[p];

            var c = [
                { data: 'checkbox' },
                { data: 'unit' },
                { data: 'unit_type' },
                { data: 'state' },
                { data: 'action' }
            ];

            $("#stats_table_${tab}").dataTable({
                processing: true,
                ajax: {
                    url: url,
                    dataSrc: function(response) {
                        Index.total = response.meta.total;
                        return response.data;
                    }
                },
                columns: c,
                lengthChange: true,
                "aLengthMenu": [
                    [25, 50, 100, -1],
                    [25, 50, 100, "All"] // change per page values here
                ],
                // set the initial value
                "iDisplayLength": 50,
                "sPaginationType": "bootstrap",
                "oLanguage": {
                    "sLengthMenu": "_MENU_ records",
                    "oPaginate": {
                        "sPrevious": "Prev",
                        "sNext": "Next"
                    }
                },
                "aoColumnDefs": [{
                        'bSortable': false,
                        'aTargets': [0]
                    }
                ],
                order: [],
                paging: false,
                bFilter: false,
                bInfo: false
            });

            $("#stats_table_${tab} .group-checkable").change(function () {
                var set = jQuery(this).attr("data-set");
                var checked = jQuery(this).is(":checked");
                jQuery(set).each(function () {
                    if (checked) {
                        $(this).attr("checked", true);
                        $(this).parents('tr').addClass("active");
                        $(this).parents('span').addClass("checked");
                    } else {
                        $(this).attr("checked", false);
                        $(this).parents('tr').removeClass("active");
                        $(this).parents('span').removeClass("checked");
                    }
                });
                jQuery.uniform.update(set);

            });

            // Add select items.
            $("#stats_table_${tab}_wrapper .row")[0].children[0].innerHTML = '<div class="dataTables_length" id="stats_table_${tab}_length"><label><select name="stats_table_${tab}_length" id="_stats_table_${tab}_length" aria-controls="stats_table_${tab}" class="form-control input-xsmall"><option value="50">50</option><option value="100">100</option><option value="150">150</option></select> records</label></div>'
            $('#stats_table_${tab}_wrapper .dataTables_length select').select2().on("select2-selecting", function(e) {
                Index.pag_size = + e.val;
                Index.pag_start = + 0;
                Index.loadData_${tab}(0, Index.pag_size);
                $('#stats_table_${tab}_wrapper .dataTables_length select').select2("val", e.val.toString());
            });

            //Add search item.
            $("#stats_table_${tab}_wrapper .row")[0].children[1].innerHTML = '<div id="stats_table_${Tabs.NEW}_filter" class="dataTables_filter"><label>Search:<input type="search" class="form-control input-medium" aria-controls="stats_table_${Tabs.NEW}"></label></div>'
            $('#stats_table_${tab}_wrapper .dataTables_filter input').on("change", function(){
                var text = $(this).val();
                Index.pag_start = + 0;
                Index.loadData_${tab}(Index.pag_start, Index.pag_size, text);
                $('#stats_table_${tab}_wrapper .dataTables_length select').select2("val", Index.pag_size);
                $('#stats_table_${tab}_wrapper .dataTables_filter input').val(text);
            });

            // Parsing parameters for the table.
            $("#stats_table_${tab}").on('order.dt',  function () {

                // Array of the table rows.
                var rows = $("#stats_table_${tab}").dataTable().fnGetNodes();

                if (rows.length < Index.pag_size)
                    $(".next").addClass("disabled");
                else
                    $(".next").removeClass("disabled");

                for (var i = 0; i < rows.length; i++) {
                    // Unit
                    try {
                        var unit = JSON.parse(rows[i].cells[1].innerHTML);
                        // Checkbox
                        rows[i].cells[0].innerHTML = '<input type="checkbox" class="checkboxes" value="{unit_id}"/>'.replace(/{unit_id}/g, unit.id);

                        rows[i].cells[1].innerHTML = ' \
                            <div class="col-md-8"> \
                                <h4 style="margin-top:0;"> \
                                    <span class="badge badge-default">#{unit_id}</span> \
                                    <a href="/unit/{unit_id}/source">{unit_name}</a> \
                                </h4> \
                                <p class="text-info"> \
                                    <i class="fa fa-user"></i> <a href="/unit/?tab={ctab}&account={acc_id}">{acc_name}</a> \
                                </p> \
                                <p class="text-info"> \
                                    <i class="fa fa-rocket"></i> <a href="/unit/?tab={ctab}&application={app_id}">{app_name}</a> \
                                </p> \
                            </div>'
                        .replace(/{unit_id}/g, unit.id)
                        .replace(/{unit_name}/g, unit.name)
                        .replace(/{app_id}/g, unit.app_id)
                        .replace(/{app_name}/g, unit.app_name)
                        .replace(/{acc_id}/g, unit.acc_id)
                        .replace(/{acc_name}/g, unit.acc_email)
                        .replace(/{ctab}/g, Index.tab);

                        // Unit type
                        var utype = rows[i].cells[2].innerHTML, utypeText = "";
                        switch(utype) {
                            case "${Unit.Type.BANNER}":
                                utypeText = "Банер";
                                // Add banner size.
                                rows[i].cells[1].innerHTML += ' \
                                    <div class="col-md-4 text-right"> \
                                        <p class="text-muted"> \
                                            <small><i class="fa fa-arrows-alt"></i>{unit_size}</small> \
                                        </p> \
                                    </div>'
                                    .replace(/{unit_size}/g, unit.size)
                                break;
                            case "${Unit.Type.INTERSTITIAL}":
                                utypeText = "Полноэкранная";
                                break;
                        }
                        rows[i].cells[2].innerHTML = utypeText;

                        //Actions
                        var stat = rows[i].cells[3].innerHTML;
                        if (stat == ${Unit.State.NONE}) {
                            rows[i].cells[4].innerHTML += '&nbsp <a onClick="doAction(event, {action: \'active\', id: \'{unit_id}\'})" ><i class="fa fa-check fabutton tooltips" data-placement="top" data-original-title="Активировать"></i></a>'.replace("{unit_id}", unit.id);
                            rows[i].cells[4].innerHTML += '&nbsp <a onClick="doAction(event, {action: \'disable\', id: \'{unit_id}\'})" ><i class="fa fa-times fabutton tooltips" data-placement="top" data-original-title="Остановить"></i></a>'.replace("{unit_id}", unit.id);
                            rows[i].cells[4].innerHTML += '&nbsp <a href="/unit/{app_id}/{unit_id}" ><i class="fa fa-pencil fabutton tooltips" data-placement="top" data-original-title="Редактировать"></i></a>'.replace("{unit_id}", unit.id).replace("{app_id}", unit.app_id);
                        } else if (stat == ${Unit.State.ACTIVE}) {
                            rows[i].cells[4].innerHTML = '<a onClick="doAction(event, {action: \'disable\', id: \'{unit_id}\'})" ><i class="fa fa-pause fabutton tooltips" data-placement="top" data-original-title="Остановить"></i></a>'.replace("{unit_id}", unit.id);
                            rows[i].cells[4].innerHTML += '&nbsp <a href="/unit/{app_id}/{unit_id}" ><i class="fa fa-pencil fabutton tooltips" data-placement="top" data-original-title="Редактировать"></i></a>'.replace("{unit_id}", unit.id).replace("{app_id}", unit.app_id);
                        } else if (stat == ${Unit.State.DISABLED}) {
                            rows[i].cells[4].innerHTML = '<a onClick="doAction(event, {action: \'active\', id: \'{unit_id}\'})" ><i class="fa fa-check fabutton tooltips" data-placement="top" data-original-title="Активировать"></i></a>'.replace("{unit_id}", unit.id);
                            rows[i].cells[4].innerHTML += '&nbsp <a href="/unit/{app_id}/{unit_id}" ><i class="fa fa-pencil fabutton tooltips" data-placement="top" data-original-title="Редактировать"></i></a>'.replace("{unit_id}", unit.id).replace("{app_id}", unit.app_id);
                        } else  {
                            rows[i].cells[4].innerHTML = '<a onClick="doAction(event, {action: \'active\', id: \'{unit_id}\'})" ><i class="fa fa-check fabutton tooltips" data-placement="top" data-original-title="Активировать"></i></a>'.replace("{unit_id}", unit.id);
                            rows[i].cells[4].innerHTML += '&nbsp <a href="/unit/{app_id}/{unit_id}" ><i class="fa fa-pencil fabutton tooltips" data-placement="top" data-original-title="Редактировать"></i></a>'.replace("{unit_id}", unit.id).replace("{app_id}", unit.app_id);
                        }

                    } catch(e) { }

                    // Status
                    var stat = rows[i].cells[3].innerHTML;
                    if (!isNaN(stat)) {
                        var stat_class = "badge-danger", stat_value = "Unknown";
                        switch(stat) {
                            case "${Unit.State.ACTIVE}":
                                stat_class = "badge-success";
                                stat_value = "Активный";
                                break;
                            case "${Unit.State.DISABLED}":
                                stat_class = "badge-warning";
                                stat_value = "остановленный";
                                break;
                            case "${Unit.State.NONE}":
                                stat_class = "badge-info";
                                stat_value = "Новый";
                                break;
                        }
                        rows[i].cells[3].innerHTML = '<span class="badge {stat_class}">{stat_value}</span>'
                            .replace(/{stat_value}/g, stat_value)
                            .replace(/{stat_class}/g, stat_class);
                    }
                }

                // Add summary to the table.
                var start = +Index.pag_start + 1, end;
                if (!Index.total)
                    start = end = 0;
                else if ((+Index.pag_start + Index.pag_size) > +Index.total)
                    end = Index.total;
                else
                    end = + Index.pag_start + Index.pag_size;
                $("#stats_table_${tab}_wrapper .row")[1].children[0].innerHTML = 'С {start} по {end}, всего {total}'
                    .replace("{total}", Index.total)
                    .replace("{start}", start)
                    .replace("{end}", end);
            });
        },
        %endfor

        // Reload the table. The pagination settings are dropped.
        reload_table: function(){
            Index["loadData_" + Index.tab]();
        }
    }
}();
</script>
<!-- SCRIPTS INDEX END-->

<script type="text/javascript">
    $(document).on("ready", function() {

        // Table pagination.
        $(".prev").on("click", function() {
            if ($(".prev").attr("class").indexOf("disabled") > 0)
               return;

            Index.pag_start -= Number(Index.pag_size);
            if (+Index.pag_start < 0) {
               Index.pag_start = +0;
               $(".prev").addClass("disabled");
               return;
            }
            $(".next").removeClass("disabled");

            Index["loadData_" + Index.tab](Index.pag_start, Index.pag_size);

        });
        $(".next").on("click", function() {
            if ($(".next").attr("class").indexOf("disabled") > 0)
               return;

            $(".prev").removeClass("disabled");

            Index.pag_start = Number(Index.pag_start) + Number(Index.pag_size);
            Index["loadData_" + Index.tab](Number(Index.pag_start), Number(Index.pag_size));
        });

        Index.init();
        // Init filters.
        %for select in g.selects:
            <%
                _name = 'account' if select['name'] in ('advertiser', 'publisher', ) else select['name']
                placeholder = getattr(g, str(_name) + '_name', select['header'])
            %>
            %if select.get("data"):
                filterBox.push({
                    id: "${select['id']}",
                    name: "${select['name']}",
                    header: "${select['header']}",
                    placeholder: "${placeholder}",
                    value: "${getattr(g, str(select['name']), '')}",
                    %if select.get("name") == "unit_size":
                    format: function(obj, value) {
                        try {
                            var size = value.split("x");
                            return "width:" + size[0] + ",height:" + size[1];
                        } catch(e) { return "width:,height:"; }
                    },
                    %endif
                    data: [
                        {"id": "", "text": "Все"},
                        %for i, (k, v) in enumerate(select.get('data').items()):
                            %if i != len(select.get('data')) - 1:
                            {"id": "${k}", "text": "${v}"},
                            %else:
                            {"id": "${k}", "text": "${v}"}
                            %endif
                        %endfor
                    ]
                });
            %else:
                filterBox.push({
                    id: "${select['id']}",
                    name: "${select['name']}",
                    header: "${select['header']}",
                    placeholder: "${placeholder}",
                    value: "${getattr(g, str(select['name']), '')}",
                    url: "/unit/ajax/select"
                });
            %endif
        %endfor

        %if getattr(g, 'tab', ''):
            Index.loadData_${g.tab}();
        %else:
            Index.loadData_${Tabs.NEW}();
        %endif
    });

<!--GROUP ACTIONS-->

    action_assistant = {
        objects: null,
        objects_ids: null,
        action: null
    };

    $('.group_actions').on('click', function(){
        var objects = $('.checkboxes:checked');
        var objects_ids = '';
        if (objects.length > 0){
            for (var i = 0; i < objects.length; i++){
                if (i == objects.length - 1){
                    objects_ids += objects[i].value;
                    continue;
                }
                objects_ids += objects[i].value + ',';
            }

            var group_checkbox = $("#stats_table_{tab} .group-checkable".replace("{tab}", Index.tab));
            group_checkbox[0].parentElement.className = "";

            action_assistant.action = this.id
            action_assistant.objects = objects
            action_assistant.objects_ids = objects_ids

            switch(this.id) {
                case 'active':
                    $('#approve_chose_action').html("активировать");
                    $('#approve_header').html("Активация блоков");
                    break;
                case 'disable':
                    $('#approve_chose_action').html("заблокировать");
                    $('#approve_header').html("Блокировка блоков");
                    break;
                case 'delete':
                    $('#approve_chose_action').html("удалить");
                    $('#approve_header').html("Удаление блоков");
                    break;
            }

            $('#approve_elements_list').html("[" + objects_ids + "]");
        }
    });

    $("#button_group_action_approve").on('click', function(){
        $.post('/unit/actions', {objects_list: action_assistant.objects_ids, action: action_assistant.action}, after_load);

        objects = action_assistant.objects

        for (var i = 0; i < objects.length; i++){
            tr = objects[i].parentElement.parentElement;
            tr.className = tr.className.replace("active", "");
            objects[i].parentElement.className = "";
            objects[i].checked = false;
            remove_tr(tr);
        }
    });

    function doAction(event, action_assistant) {
        var tr = event.target.parentElement.parentElement.parentElement;
        var action = action_assistant.action;
        var objects_ids = action_assistant.id;
        var previous_state = "" + action_assistant.state;

        $.post('/unit/actions', {objects_list: objects_ids, action: action}, after_load);

        tr.className = tr.className.replace("active", "");
        remove_tr(tr);
    }

    // That's used to reload the page if there are no rows in the table.
    function remove_tr(tr) {
        var reload = true;
        var trs = tr.parentElement.children;
        tr.innerHTML = "";

        for (var i = 0; i < trs.length; i++) {
            if (trs[i].innerHTML != "") {
                reload = false;
                break;
            }
        }
        if (reload)
            window.location.href = '/unit/?tab=' + Index.tab;
    }

    function after_load(data) { }
</script>


