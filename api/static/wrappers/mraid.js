function mraidIsReady() {
	mraid.removeEventListener("ready", mraidIsReady);
	main();
}

function doReadyCheck() {
	if (mraid.getState() == 'loading') {	
		mraid.addEventListener("ready", mraidIsReady);  
	} else { 	
		showMyAd();      
	}
}

var onNavigate = function() {
	console.log("> mraid > onNavigate");
	//window.location = params.uri;
	mraid.open(params.uri);
}

var onTerminate = function() {
	console.log("> mraid > onTerminate");
	mraid.close();
}