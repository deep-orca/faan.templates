log = function (msg) {
	if (typeof console === "undefined" || typeof console.log === "undefined") return;
	console.log(msg); 
}

queryParams = function(str) {
	var qparams = {}; 
	
	if(str) {
		var query_str_split = str.split("&");
		
		for(var param_str_idx in query_str_split) {
			var param_str = query_str_split[param_str_idx].split("=");
			qparams[param_str[0]] = param_str[1];
		}
	}
	return qparams;
}

if(typeof(params) == "undefined") {
	log("MRAID > BannerWrapper > Trying window.location query parameters");
	params = queryParams(window.location.href.split("?")[1]);
	log("d0");
}

if(typeof(adapter) == "undefined") {
	log("MRAID > BannerWrapper > Trying window.parent notification adapter");
	adapter = window.parent.vdgr.tmps[params.serial];
	log("d1");
}

log("+ael");

function ael(o, e, h, p) {
	if(o.addEventListener) {
		o.addEventListener(e, h, p);
	} else if ( o.attachEvent ) {
		o.attachEvent(e, h);
	} else {
		log("MARID > BannerWrapper > Error : cannot attach event");
	}
}

var imgLoaded = true;
var imgError  = false;
var wndLoaded = false;
var wndError  = false;

log("+notifyAdapter");

function notifyAdapter() {
	log(" > notifyAdapter");
	if(!wndLoaded || !imgLoaded)
		return;
	
	if(!adapter) 
		return;
	
	if(imgError || wndError)
		adapter.failed();
	else
		adapter.loaded();
}

log("+mraidLoad");

function mraidLoad() { 
	wndLoaded = true;
	notifyAdapter();
}	

log("+mraidError");

function mraidError() {
	wndLoaded = true;
	wndError = true;
	notifyAdapter();
}	

log("+click");

ael(document.body, "click", function() {
	log(" > document.body.click ev");
	if(adapter) adapter.clicked();
	return true;
}, false );	

//document.body.addEventListener("click", function() { 
//	if(adapter) adapter.clicked();
//	return true;
//}, false );	

log("+load");

ael(window, "load", function() { 
	log(" > window.load ev");
	if(adapter) adapter.loaded();
	return true;
}, false );

log("INIT COMPLETE");

//window.addEventListener('load', function() { 
//	if(adapter) adapter.loaded();
//	return true;
//}, false );

//document.write(params.body);
