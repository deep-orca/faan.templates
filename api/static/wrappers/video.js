
// ======================= SETUP CONTENT ======================= //

var ss  = document.createElement("link");								// Inject link to .css
ss.type = "text/css";
ss.rel  = "stylesheet";
ss.href = "http://api.vidiger.com/static/wrappers/video.css";
document.getElementsByTagName("head")[0].appendChild(ss);

document.write(															// Inject actual body
'<div id="vlayer">' + 
'	<video id="v" preload="none"></video>' + 
'	<div id="overlaybox">' +
'		<div id="overlayToggle"></div>' +
'		<img id="overlay" src="javascript:void(0)">' + 
'	</div>' + 
'	<div id="circle"></div>' + 
'	<div id="timerbox"><span id="timer"></span></div>' +
'	<div id="closebox">' +
'		<span id="closetext"><span id="closetext_anchor"></span></span>' +
'		<span id="closeintext"><span id="closecounter"></span></span>' +
'	</div>' +
'</div>' +
'<div id="eclayer">' + 
'	<img id="close" src="http://api.vidiger.com/global/static/img/endcard_close.png">' + 
'</div>'
);

// ================ INTEGRATE WITH CONTAINER =================== //
if(typeof(mraid) == "undefined" && typeof(testing) == "undefined") {
	
	queryParams = function(str) {
		var qparams = {}; 
		var query_str_split = str.split("&");
		
		for(var param_str_idx in query_str_split) {
			var param_str = query_str_split[param_str_idx].split("=");
			qparams[param_str[0]] = param_str[1];
		}
			
		return qparams;
	}
	
	if(typeof(params) == "undefined") {
		console.log("Video Wrapper > Trying window.location query parameters");
		params = queryParams(window.location.href.split("?")[1]);
	}
	
	if(typeof(adapter) == "undefined") {
		console.log("Video Wrapper > Trying window.parent notification adapter");
		adapter = window.parent.vdgr.tmps[params.serial];
	}
	
	var vidLoaded = false;
	var vidError  = false;
	var wndLoaded = false;
	var wndError  = false;
	
	function notifyAdapter() {
		if(!wndLoaded || !vidLoaded || !adapter)  return;
		
		if(vidError || wndError)
			adapter.failed();
		else
			adapter.loaded();
	}
	
	
	window.addEventListener("load", function() { 
		wndLoaded = true;
		notifyAdapter();
	}, false );	
	
	window.addEventListener("error", function() {
		wndLoaded = true;
		wndError = true;
		notifyAdapter();
	}, false );	
	
	/*
	document.getElementById('link').addEventListener("click", function() { 
		if(adapter) adapter.clicked();
		return true;
	}, false );	
	*/
	
	onTerminate = function() {
		console.log("> video > onTerminate");
	}
	
	onNavigate = function() {
		console.log("> video > onNavigate");
		var win = window.open(params.uri, '_blank');
		win.focus();
	}

}
// ========================= ORIGINAL ======================== //


var v 		 		= document.getElementById('v');

var eclayer  		= document.getElementById('eclayer');
var circle   		= document.getElementById('circle');
var timerbox 		= document.getElementById('timerbox');
var timer    		= document.getElementById('timer');

var close    		= document.getElementById('close');
var closetext 		= document.getElementById('closetext');
var closeintext		= document.getElementById('closeintext');
var closecounter 	= document.getElementById('closecounter');

var overlay  		= document.getElementById('overlay');
var olToggle 		= document.getElementById('overlayToggle');

// ================================= EVENTS ======================================

v.addEventListener('canplaythrough', function(event) {
	console.log(">>> canplaythrough");
	if (typeof(mraid) == "undefined" || mraid.isViewable()) {
		v.play();
	}
	console.log("<<< canplaythrough");
}, false);	

v.addEventListener('loadeddata', function() {
    console.log("** RECEIVED loadeddata **");
    
    if (typeof(mraid) == "undefined" || mraid.isViewable()) {
        v.play();
        
        setTimeout(function() { 
        	if (v.paused) {
        		console.log(">>> simulating click 1");
                v.play();
                
                if (v.paused) {
                	console.log(">>> simulating click 2");
                	simulate(v, "click");
                	v.play();
                }
        	};
        }, 1);
    }
}, false);

v.addEventListener('timeupdate', function(event) {
	timer.innerHTML = "" + parseInt(v.duration - v.currentTime);
}, false);

v.addEventListener('ended', function() {
	onEnded();
}, false);

v.addEventListener('playing', function() {
	console.log(">>> playing");
	initPostPlay();
	v.addEventListener('click', function() {
		onEnded();
		onNavigate();
		return false;
	}, false);
	console.log("<<< playing");
}, false);

overlay.addEventListener('click', function() {
	console.log(">>> overlay clicked");
	onNavigate();
	console.log("<<< overlay clicked");
	return false;
}, false);

overlayToggle.addEventListener('click', function() {
	hide(overlayToggle);
	show(overlay);
}, false);

eclayer.addEventListener('click', function() {
	onNavigate();
	return false;
}, false);
	
closetext.addEventListener('click', function(e) {
	console.log("> closetext > click");
	onTerminate();
	e.preventDefault();
	e.stopPropagation();
	return false;
}, false);	

close.addEventListener('click', function(e) {
	console.log("> close > click");
	onTerminate();
	e.preventDefault();
	e.stopPropagation();
	return false;
}, false);
	
// =============================== FUNCTIONS =================================

function hide() { for (var i = 0; i < arguments.length; i++) arguments[i].style.display = 'none';  }
function show() { for (var i = 0; i < arguments.length; i++) arguments[i].style.display = 'block'; }

function closeInUpdate() {
	closecounter.innerHTML = "" + parseInt(params.closable - v.currentTime);
	
	if(params.closable < v.currentTime) { 
		show(circle, timerbox, closetext);
		hide(closeintext);
		v.removeEventListener('timeupdate', closeInUpdate);
	}
}

function initPostPlay() {
	console.log(">>> initPostPlay");
	eclayer.style.backgroundImage = "url(" + params.endcard + ")";
	
	if(params.closable == 0) {
		show(closetext, circle, timerbox);
	} else if(params.closable < params.duration) {
		show(closeintext);
		v.addEventListener('timeupdate', closeInUpdate);
	} else {
		show(circle, timerbox);
	}
	
	if(typeof(params.overlay) != "undefined" && params.overlay) {
		overlay.src = params.overlay;
		setTimeout(function() { console.log(">>> SHOWING overlayToggle"); show(overlayToggle); }, 2000);
	}
	
	console.log("<<< initPostPlay");
}

function onEnded() {
	console.log("> onEnded");
	
	hide(circle, timerbox);
	show(eclayer);
	v.src = "";

}

// =============================== EXECUTION ==============================

console.log(">>> INITIAL");
v.src = params.video;
v.load();
console.log("<<< INITIAL");

// ============================== SORCERY ================================

function simulate(element, eventName) {
    var options = extend(defaultOptions, arguments[2] || {});
    var oEvent, eventType = null;

    for (var name in eventMatchers) 
        if (eventMatchers[name].test(eventName)) { eventType = name; break; }

    if (!eventType)
        throw new SyntaxError('Only HTMLEvents and MouseEvents interfaces are supported');

    if (document.createEvent) {
        oEvent = document.createEvent(eventType);
        if (eventType == 'HTMLEvents') {
            oEvent.initEvent(eventName, options.bubbles, options.cancelable);
        } else {
            oEvent.initMouseEvent(eventName, options.bubbles, options.cancelable, document.defaultView,
            options.button, options.pointerX, options.pointerY, options.pointerX, options.pointerY,
            options.ctrlKey, options.altKey, options.shiftKey, options.metaKey, options.button, element);
        }
        
        element.dispatchEvent(oEvent);
    } else {
        options.clientX = options.pointerX;
        options.clientY = options.pointerY;
        var evt = document.createEventObject();
        oEvent = extend(evt, options);
        element.fireEvent('on' + eventName, oEvent);
    }
    
    return element;
}

function extend(destination, source) {
    for (var property in source)
      destination[property] = source[property];
    return destination;
}

var eventMatchers = {
    'HTMLEvents': /^(?:load|unload|abort|error|select|change|submit|reset|focus|blur|resize|scroll)$/,
    'MouseEvents': /^(?:click|dblclick|mouse(?:down|up|over|move|out))$/
}
var defaultOptions = {
    pointerX: 0,
    pointerY: 0,
    button: 0,
    ctrlKey: false,
    altKey: false,
    shiftKey: false,
    metaKey: false,
    bubbles: true,
    cancelable: true
}
