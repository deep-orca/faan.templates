/*
document.write( "<style> " +
                    "html, body {" +
                    "	background-color: black;" +
                    "}" +
                    "" +
                    "html, body, img {" +
                    "	border	 : none;" +
                    "	margin	 : 0;" +
                    "	padding	 : 0;" +
                    "" +
                    "}"  +
                "</style>");
*/

log = function (msg) {
    if (typeof console === "undefined" || typeof console.log === "undefined") return;
    console.log(msg);
}

queryParams = function(str) {
    var qparams = {};
    var query_str_split = str.split("&");

    for(var param_str_idx in query_str_split) {
        var param_str = query_str_split[param_str_idx].split("=");
        qparams[param_str[0]] = param_str[1];
    }

    return qparams;
}

if(typeof(params) == "undefined") {
    log("BannerWrapper > Trying window.location query parameters");
    params = queryParams(window.location.href.split("?")[1]);
    log("BannerWrapper > qp done");
}

if(typeof(adapter) == "undefined") {
    log("BannerWrapper > Trying window.parent notification adapter");
    adapter = window.parent.vdgr.tmps[params.serial];
    log("BannerWrapper > ap done");
}

function ael(o, e, h, p) {
    if(o.addEventListener) {
        o.addEventListener(e, h, p);
    } else if ( o.attachEvent ) {
        o.attachEvent(e, h);
    } else {
        log("BannerWrapper > Error : cannot attach event");
    }
}


var imgLoaded = false;
var imgError  = false;
var wndLoaded = false;
var wndError  = false;

function notifyAdapter() {
    if(!wndLoaded || !imgLoaded)
        return;

    if(!adapter)
        return;

    if(imgError || wndError)
        adapter.failed();
    else
        adapter.loaded();
}

document.write(
        "<center>" +
            "<a id='link' href='" + params.uri + "' target='_blank'> " +
                "<img onerror='imgError = true; imgLoaded = true; notifyAdapter();' " +
                " onload='imgLoaded = true; notifyAdapter();' " +
                " src='" + params.image + "' />" +
            "</a>" +
        "</center>"
    );




ael(window, "load", function() { 
    wndLoaded = true;
    notifyAdapter();
}, false );	

ael(window, "error", function() {
    wndLoaded = true;
    wndError = true;
    notifyAdapter();
}, false );	

ael(document.getElementById('link'), "click", function() { 
    if(adapter) adapter.clicked();
    return true;
}, false );	

