(function () { 
	if(typeof(window.adn) == 'undefined') {
		var d = new Date();
		
		window.adn = {	'globals'		: { 'rendered' 	: [],
											'readyBinded': false,
											'day' 		: (d.getDay() + 6) % 7,
											'hour'		: d.getHours(),	
											'timestamp'	: Math.round(d.getTime() / 1000),
											'storageTTL': 60 * 5,
											'readyWait'	: 1,						// A counter to track how many items to wait for before the ready event fires. See #6781
											'isReady'	: false,					// Is the DOM ready to be used? Set to true once it occurs.
											'retries'	: 0
									  	},
						'browsers'		: { 'unknown' 	: 0,
											'chrome' 	: 1,
											'firefox' 	: 2,
											'opera' 	: 3,
											'msie' 		: 4,
											'safari' 	: 5,
											'iemobile'	: 6,
											'operamini' : 7
										},
										  
						'platforms'		: { 'unknown' 	: 0,
											'windows' 	: 1,
											'mac' 		: 2,
											'linux' 	: 3,
											'android' 	: 4,
											'ios' 		: 5,
											'bsd'		: 6,
											'winmobile' : 7
										},										  
									  	
					    
						'spots'			: {},
						'sites'			: {},
						'ads'			: {},
						
						'siteIds'		: [],
						'spotIds'		: [],
						'adIds'			: [],
						'pickedAds'		: [],
						
						'region'		: 1,
						'cookie'		: (typeof(navigator.cookieEnabled) != 'undefined' ? navigator.cookieEnabled : "true"),
						
						'browserN'		: 0,
						'browserV'		: 0,

						'platformN'		: 0,
						'platformV'		: 0,
						
						'applied'		: false,
						
						'referrer'		: true,

						'contains'		: function(haystack, needle) {
											for(var i = 0; i < haystack.length; i++)
												if(haystack[i] == needle)
													return i;
											
											return -1;
										},
						'storageLoad'	: function(name) {
											try {
												if(typeof(localStorage) == 'undefined')	return null; 
												var pair = localStorage.getItem(name);
												if(!pair) return null;
												pair = JSON.parse(pair); 
												if(pair.ts < window.adn.globals.timestamp - window.adn.globals.storageTTL) {
													localStorage.removeItem(name);
													return null;
												}
												
												return pair.value;
											} catch(e) {
												return null;
											}
										},
						'storageHas'	: function(name) {
											try {
												if(typeof(localStorage) == 'undefined')	return false;
												return localStorage.getItem(name) != null;
											} catch(e) {
												return false;
											}
										},										
						'storageSave'	: function(name, value) {
											try {
												if(typeof(localStorage) == 'undefined')	return false;
												var pair = { 'ts'	 : window.adn.globals.timestamp, 
														 	 'value' : value };
												
												pair = JSON.stringify(pair);
												localStorage.setItem(name, pair);
												return true;
											} catch(e) {
												return false;
											}
										},
						'xaclk'			: function(spot) {
											try {
												if(window.adn.spots[spot].response.ganalytics) {
													_gaq.push(['_trackPageview', '/' + window.adn.spots[spot].settings.ganalytics]);
												}
											} catch(e) { } 		
										},
						'fnGetDomain'	: function(url) { return url.split("/")[2]; },
						'getThisScript' : function () {
											var scripts = document.getElementsByTagName('script');
											return scripts[scripts.length - 1];	
										},
						'getCookie' 	: function (c_name) {
											var i, x, y, ARRcookies = document.cookie.split(";");
											for (i = 0; i < ARRcookies.length; i++) {
										  		x = ARRcookies[i].substr(0, ARRcookies[i].indexOf("="));
										  		y = ARRcookies[i].substr(ARRcookies[i].indexOf("=") + 1);
										  		x = x.replace(/^\s+|\s+$/g, "");
										  		if(x == c_name)
													return unescape(y);
											}
				
											return null;
										},
						'delCookie'		: function (name) {
											document.cookie = name + '=; path=/; expires=Thu, 01-Jan-70 00:00:01 GMT;';
										}, 
						'setCookie'		: function  (name, value, expires, path, domain, secure) {
										    document.cookie = name + "=" + escape(value) +
										      ((expires) 	? "; expires=" 	+ expires 	: "") +
										      ((path) 		? "; path=" 	+ path 		: "") +
										      ((domain) 	? "; domain=" 	+ domain 	: "") +
										      ((secure) 	? "; secure" 				: "");
										},						
						'doScrollCheck'	: function() {
											if (!window.adn.isReady) {
												try 	 { document.documentElement.doScroll("left"); 			}
												catch(e) { setTimeout( window.adn.doScrollCheck, 1 ); return; 	}	
												window.adn.ready();									
											}
										},
						'ieVer'		    : function() {
											return (navigator.appVersion.indexOf("MSIE") != -1) ? parseFloat(navigator.appVersion.split("MSIE")[1]) : 999;
										},
						'frameBody'		: function(spotId) {
											var dmn = window.adn.spots[spotId].domain;
											return (window.adn.ieVer() < 8 ? '' : '<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">') +
											'<html>' +
											'<head>' +
											'<meta http-equiv="content-type" content="text/html; charset=UTF-8">' +
											'<link rel="stylesheet" href="http://' + dmn + '/static/css/block.css" type="text/css" media="all" />' +
											'<!--[if lt IE 8]><link rel="stylesheet" type="text/css" href="http://' + dmn + '/static/css/block_ie.css" /><![endif]-->' +
											'<scr' + 'ipt type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.6.3/jquery.js"></scr' + 'ipt>' +
											'<scr' + 'ipt type="text/javascript" src="http://' + dmn + '/static/js/utils.js"></scr' + 'ipt>' +
											'<scr' + 'ipt type="text/javascript">' +
											'var domain = "' + dmn + '";' + 
											'var data = ""; var spotId = ' + spotId + ';' +
											'</scr' + 'ipt>' +
											'<scr' + 'ipt type="text/javascript" src="http://' + dmn + '/static/js/render.js"></scr' + 'ipt>' +
											'</head>' +
											'<body>' +
											'<div id="root">' +
											'	<span id="loading">Loading...</span>' +
											'</div>' +
											'</body>' +
											'</html>';											
										},
						'frameCreate'	: function(spotId) {
											var iframe = document.createElement('iframe');
											
										    iframe.id = iframe.name  = "adn_frame_" + spotId;
											iframe.scrolling 		 = 'no';
											iframe.frameBorder 		 = 0;
											iframe.marginheight 	 = 0;
											iframe.marginwidth 		 = 0;
											iframe.allowtransparency = 'true';
											iframe.vspace 			 = 0;
											iframe.hspace 			 = 0;
											
											return iframe;
										},
								
						'frameDoc'		: function(iframe) {
											if (iframe.contentDocument)
												return iframe.contentDocument;
											else if (iframe.contentWindow)
												return iframe.contentWindow.document;
											else if (window.frames[iframe.name]) 
												return window.frames[iframe.name].document;
											
											return null;
										},
						'holdReady' 	: function( hold ) {
											hold ? window.adn.readyWait++ : window.adn.ready( true );
										},
						'ready' 		: function( wait ) {												
											if ( (wait === true && !--window.adn.readyWait) || (wait !== true && !window.adn.isReady) ) {
												if ( !document.body )
													return setTimeout( window.adn.ready, 1 );
												
												window.adn.isReady = true;
												if ( wait !== true && --window.adn.readyWait > 0 )
													return;

												window.adn.activate();
											}
										},
						'bind'			: function () {
											if ( window.adn.globals.readyBinded )  		return;
											window.adn.globals.readyBinded = true;
											if ( document.readyState === "complete" ) 	return setTimeout( window.adn.ready, 1 );
											
											if ( document.addEventListener ) {
												document.addEventListener( "DOMContentLoaded", window.adn.DOMContentLoaded, false );	
												window.addEventListener( "load", window.adn.ready, false );					// A fallback to window.onload, that will always work
											} else if ( document.attachEvent ) {											// If IE event model is used
												document.attachEvent( "onreadystatechange", window.adn.DOMContentLoaded );	// ensure firing before onload, maybe late but safe also for iframes
												window.attachEvent( "onload", window.adn.ready );							// A fallback to window.onload, that will always work
										
												var toplevel = false;														// If IE and not a frame continually check to see if the document is ready
										
												try {
													toplevel = window.frameElement == null;
												} catch(e) {}
										
												if ( document.documentElement.doScroll && toplevel ) 
													window.adn.doScrollCheck();
											}
										},
						'attachJS'		: function(elem, src) {
											var js = document.createElement('script');
											js.src = src;
											elem.appendChild(js);
						  				},
						'rrPick'		: function(sid) {
											var xs 		= window.adn.sites[sid];
											var current = xs.adIds[xs.offset++ % xs.adIds.length];
													
											if(window.adn.contains(window.adn.pickedAds, current) == -1) {
												window.adn.pickedAds.push(current);
												return current;
											} 
											
											if(xs.dupsOnly) {
												return current;
											}
											
											for(var i = xs.offset; i <  xs.offset + xs.adIds.length - 1; i++) {
												var repl = xs.adIds[i % xs.adIds.length];
												if(window.adn.contains(window.adn.pickedAds, repl) != -1)
													continue;
												
												window.adn.pickedAds.push(repl);
												xs.adIds[(xs.offset - 1) % xs.adIds.length] = repl;
												xs.adIds[i % xs.adIds.length]			    = current;
												
												return repl;
											}
											
											xs.dupsOnly = true;
											return current;
						  				},							  				
						'rrPickAds'		: function(sid, count) {
											var res = [];
											while(res.length < count)
												res.push(window.adn.rrPick(sid));
							  				
							  				return res;
						  				},										
						'activate'		: function () {
											var nadq 			= [], cadq	 		= [];
											var cspotq 			= [], nspotq 		= [];
											var siteq			= [], redrawq		= [];
											
											for(var spotId in window.adn.spots) { 
												if(window.adn.spots[spotId].settings != null) {
													if(!window.adn.storageHas("adn_spot_settings_" + spotId))
														window.adn.storageSave("adn_spot_settings_" + spotId, 
																			   window.adn.spots[spotId].settings);
													cspotq.push("" + spotId);
												} else
													nspotq.push("" + spotId);
											}											
											
											var redraw = false;
											
											for(var adId in window.adn.ads) {
												if(!window.adn.storageHas("adn_ad_" + adId))
													window.adn.storageSave("adn_ad_" + adId, 
																		   window.adn.ads[spotId]);
												
												cadq.push(adId);			
											}
											
											for(var siteId in window.adn.sites) {
												siteq.push(siteId);	
												
												var xs 		= window.adn.sites[siteId];
												var sredraw = xs.offset >= xs.adIds.length;
												
												redrawq.push( sredraw ? "5" : "1" );
												
												redraw = redraw || sredraw;
												
												for(var adIdN in xs.adIds) {
													var adId = xs.adIds[adIdN];
													if(window.adn.contains(cadq, adId) == -1 && 
													   window.adn.contains(nadq, adId) == -1)
														nadq.push(adId);
												}
											}
											
											if(!redraw && !nadq.length && !nspotq.length) {
												window.adn.apply();
												return;
											}
											
											var lastSpotId = window.adn.spotIds[0];
											
											var src = 	"http://" + window.adn.spots[lastSpotId].domain + "/core/a/q.x" +
														"?s="	+ siteq.join("-") +
														"&rw="	+ redrawq.join("-") +
														
														"&cs="	+ cspotq.join("-") +
													  	"&ns="	+ nspotq.join("-") +

													  	"&ca="	+ cadq.join("-") +
													  	"&na="	+ nadq.join("-") +
													  	
														"&d="	+ window.adn.globals.day + 
														"&h="	+ window.adn.globals.hour +
														
														"&r="	+ window.adn.region +
														
														"&b="	+ ((parseInt(window.adn.browserN ) << 16) + parseInt(window.adn.browserV )) + 
														"&p="	+ ((parseInt(window.adn.platformN) << 16) + parseInt(window.adn.platformV))
														;
											
											if(!window.adn.globals.retries)
												window.adn.attachJS(window.adn.spots[lastSpotId].statics, src);
											else {
												var sid		= window.adn.spotIds[0];
												var bindel	= window.adn.spots[sid].statics;

												setTimeout(function() {
																var js = document.createElement('script');
																js.src = src;
																bindel.appendChild(js);  
														   }, 
														   window.adn.globals.retries * 250);
											}
											window.adn.globals.retries++;
										},

						  'apply'		: function() {
							  				var spotsq  = [], sitesq   = [], spotHitsq  = [];
							  				var adsq 	= [], adsitesq = [], campaignsq = [], adhitsq = [];
							  				
							  				var xadsq = {};
							  				
							  				for(var spotIdN in window.adn.spotIds) {
							  					var spotId 	= window.adn.spotIds[spotIdN];
							  					var spot 	= window.adn.spots[spotId];
							  					
							  					spot.ads 	= window.adn.rrPickAds(spot.site, spot.settings.h_count * spot.settings.v_count);
							  					
							  					// Stat count data
							  					
							  					spotsq.push(spotId);
							  					sitesq.push(spot.site);
							  					spotHitsq.push(spot.ads.length);
							  					
							  					for(var xadN in spot.ads) {
							  						var ad 	 = spot.ads[xadN];
							  						
							  						if(typeof(xadsq[ad]) == 'undefined')
							  							xadsq[ad] = {};
							  						
							  						if(typeof(xadsq[ad][spot.site]) == 'undefined')
							  							xadsq[ad][spot.site] = 0;
							  						
							  						xadsq[ad][spot.site]++;
							  					}
							  					
							  					// Attach iframe
							  					
												var el 		= document.createElement('div');
												var w 		= spot.settings.width;
												var h 		= spot.settings.height;
			
												el.id = 'adn_spot_' + spotId;
												el.style.cssText = 	"width: "  + w + "; "
																 +	"height: " + h + "; ";
												
												spot.root.parentNode.insertBefore(el, spot.root);	
												
												var iframe = window.adn.frameCreate(spotId);
												iframe.width	= w;
												iframe.height	= h;

												el.appendChild(iframe);
										
												var iframeDoc 	= window.adn.frameDoc(iframe);

												iframeDoc.open();
												iframeDoc.write(window.adn.frameBody(spotId));
												iframeDoc.close();								  					
							  				};
							  				
							  				for(var siteId in window.adn.sites) {
							  					var xs 		= window.adn.sites[siteId];
							  					var offKey 	= "adn_offset_" + siteId;
							  					var adsKey 	= "adn_ad_ids_" + siteId;
							  					
								  				if(xs.offset < xs.adIds.length) {
								  					if(!window.adn.storageSave(adsKey,  xs.adIds) || 
								  					   !window.adn.storageSave(offKey,  xs.offset)) 
								  					{
								  						window.adn.setCookie(adsKey, 	xs.adIds.join(" "));
								  						window.adn.setCookie(offKey, 	xs.offset);
								  					}
								  				} else {
								  					window.adn.storageSave(adsKey, []); 
								  					window.adn.storageSave(offKey, 0);
							  						window.adn.delCookie(adsKey);
							  						window.adn.delCookie(offKey);							  					
								  				}
							  				}
							  				
							  				// Invoke stat counter
							  				
							  				for(var adId in xadsq) {
							  					var camp = window.adn.ads[adId].campaign;
							  					
							  					for(var siteId in xadsq[adId]) {
							  						var adhits = xadsq[adId][siteId];
							  						
							  						adsq.push(adId); 
							  						adsitesq.push(siteId);
							  						campaignsq.push(camp);
							  						adhitsq.push(adhits);
							  					}
							  				}

							  				var lastSpotId = window.adn.spotIds[0];
							  				
											var src = 	"http://" + window.adn.spots[lastSpotId].domain + "/core/a/c.x" +
											"?sp="	+ spotsq.join("-") +
											"&st="	+ sitesq.join("-") +
											"&sh="	+ spotHitsq.join("-") +
											
											"&a="	+ adsq.join("-") +
											"&as="	+ adsitesq.join("-") +
											"&c="	+ campaignsq.join("-") +
											"&ah="	+ adhitsq.join("-") +
											
											"&r="	+ window.adn.region +
											
											"&cookie=" +
											
											"&b="	+ ((parseInt(window.adn.browserN ) << 16) + parseInt(window.adn.browserV )) + 
											"&p="	+ ((parseInt(window.adn.platformN) << 16) + parseInt(window.adn.platformV))
											;							  				
							  				
											var sid		= window.adn.spotIds[0];
											var bindel	= window.adn.spots[sid].statics;
											var js 		= document.createElement('script');
											js.src 		= src;
											bindel.appendChild(js);  

							  				
							  				
										}
										
					 };
		
		// ============================== GLOBAL INITIALISATION ===========================
		
		if ( document.addEventListener ) {						// Cleanup functions for the document ready method
			window.adn.DOMContentLoaded = function() {
				document.removeEventListener( "DOMContentLoaded", window.adn.DOMContentLoaded, false );
				window.adn.ready();
			};

		} else if ( document.attachEvent ) {
			window.adn.DOMContentLoaded = function() {
				if ( document.readyState === "complete" ) {
					document.detachEvent( "onreadystatechange", window.adn.DOMContentLoaded );
					window.adn.ready();
				}
			};
		};	
		
		
		
		var ua = navigator.userAgent;
		var n, v;
		
		if      (/Chrome/.test(ua))  	{	n = window.adn.browsers.chrome;  	v = parseInt(ua.split("Chrome/") 	[1].split(".")[0]);  	}
		else if (/IEMobile/.test(ua))	{	n = window.adn.browsers.iemobile;	v = parseInt(ua.split("IEMobile/") 	[1].split(".")[0]);  	}
		else if (/Firefox/.test(ua)) 	{	n = window.adn.browsers.firefox; 	v = parseInt(ua.split("Firefox/")	[1].split(".")[0]);  	}
		else if (/Opera Mini/.test(ua)) {	n = window.adn.browsers.opera; 		v = parseInt(ua.split("Version/")	[1].split(".")[0]);  	}
		else if (/Opera/.test(ua)) 	 	{	n = window.adn.browsers.opera; 		v = parseInt(ua.split("Version/")	[1].split(".")[0]);  	}
		else if (/MSIE/.test(ua))    	{	n = window.adn.browsers.msie; 		v = parseInt(ua.split("MSIE ")		[1].split(".")[0]);  	}
		else if (/Safari/.test(ua))  	{	n = window.adn.browsers.safari;  	v = parseInt(ua.split("Version/")	[1].split(".")[0]);  	}
		else						 	{	n = window.adn.browsers.unknown;  	v = 0;  													}
		
		
		window.adn.browserN = n;
		window.adn.browserV = v;		
		
		if      (/Windows NT/.test(ua)) {	n = window.adn.platforms.windows;  	v = ua	.split("Windows NT ") 	[1]
																					   	.split(";")[0]
																						.split(")")[0]
																						.split(" ")[0]
																						.split(".");
																				if(v.length < 2) v[1] = 0;
																				v = v[0] * 10 + v[1]; 										}
		else if (/IPhone OS/.test(ua))	{	n = window.adn.platforms.ios;		v = ua	.split("IPhone OS ") 	[1]
																						.split(";")[0]
																						.split(")")[0];  									
											if 		(v.indexOf(".") != -1)
											v = v.split(".");
											else if	(v.indexOf("_") != -1)
											v = v.split("_");
											else if	(v.indexOf(" ") != -1)
											v = v.split(" ");
											else 
											v = [0, 0];
											
											if(v.length < 1) v[0] = 0;
											if(v.length < 2) v[1] = 0;
											
											v = v[0] * 10 + v[1]; 																			}
		
		else if (/Mac OS X/.test(ua))	{	n = window.adn.platforms.mac;		v = ua	.split("Mac OS X ") 	[1]
																						.split(";")[0]
																						.split(")")[0];  									
											if 		(v.indexOf(".") != -1)
												v = v.split(".");
											else if	(v.indexOf("_") != -1)
												v = v.split("_");
											else if	(v.indexOf(" ") != -1)
												v = v.split(" ");
											else 
												v = [0, 0];
											
											if(v.length < 1) v[0] = 0;
											if(v.length < 2) v[1] = 0;
											
											v = v[0] * 10 + v[1]; 																			}
		else							{ 	n = window.adn.platforms.unknown;	v = 0;														}
		
		
		window.adn.platformN = n;
		window.adn.platformV = v;
	}
	
	// PER SPOT INITIALISATION =============================================================
	
	var el 		= window.adn.getThisScript();
	
	var ssIds   = el.src.split("?")[1];
	var siteId  = ssIds.split("/")[0];
	var spotId  = ssIds.split("/")[1];
	
	if(typeof(window.adn.sites[siteId]) == 'undefined') {
		window.adn.siteIds.push(siteId);
		
		var offKey = "adn_offset_" + siteId;
		var adsKey = "adn_ad_ids_" + siteId;
		
		var site = { "id"	  		: siteId,
					 "offset" 		: parseInt(window.adn.storageLoad(offKey)
							 				|| window.adn.getCookie(offKey)
							 				|| 0 ),
					 "adIds"  		:	window.adn.storageLoad(adsKey)
							    		|| ( window.adn.getCookie(adsKey) || "").split(" ", false),
					 "spots"  		: [],
					 "leftover"		: [],
					 "wipeAdIds"	: false,
					 "dupsOnly"		: false
					 
				   };
		
		window.adn.sites[siteId] = site;	
		
		for(var adId in window.adn.adIds) {
			var cad = window.adn.storageLoad("adn_ad_" + adId);
			if(cad != null)
				window.adn.ads[adId] = cad;
		};	
	}
	
	if(typeof(window.adn.spots[spotId]) == 'undefined') {
		window.adn.spotIds.push(spotId);
		window.adn.sites[siteId].spots.push(spotId);
		
		var jsRoot = el.src.substring(0, el.src.lastIndexOf("/") - 1);
		document.write("<div id='adn_spot_statics_" + spotId + "' style='display: none;'></div>");
		
		var spot = { "id"		: spotId,
					 "site"	    : siteId,
					 "jsRoot"	: jsRoot,
					 "domain"	: window.adn.fnGetDomain(jsRoot),
					 "root"		: el,
					 "statics"	: document.getElementById('adn_spot_statics_' + spotId),
					 "ads"		: [],
					 "settings" : window.adn.storageLoad("adn_spot_settings_" + spotId)
				   };
		
		window.adn.spots[spotId] = spot;		
		window.adn.bind();
	};
	
})();


























