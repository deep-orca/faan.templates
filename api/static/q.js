(function () { // ============ RESPONSIVE BRANCH ============== //
	if(typeof(window.vdgr) == 'undefined') {
	
		var JSON = JSON || {};
	
		JSON.stringify = JSON.stringify || function (obj) {										// implement JSON.stringify serialization (IE 8 in compatibility mode?)  
			var t = typeof (obj);
			if (t != "object" || obj === null) {
			    if (t == "string") obj = '"'+obj+'"';											// simple data type
			    return String(obj);
			} else {
			    var n, v, json = [], arr = (obj && obj.constructor == Array);				    // recurse array or object
	
			    for (n in obj) {
			        v = obj[n]; t = typeof(v);
	
			        if (t == "string") v = '"'+v+'"';
			        else if (t == "object" && v !== null) v = JSON.stringify(v);
	
			        json.push((arr ? "" : '"' + n + '":') + String(v));
			    }
	
			    return (arr ? "[" : "{") + String(json) + (arr ? "]" : "}");
			}
		};

		JSON.parse = JSON.parse || function (str) {												// implement JSON.parse de-serialization
			if (str === "") str = '""';
			eval("var p=" + str + ";");
			return p;
		};
				
		function Unit(el) { 
			this.root	 = el;
			this.params  = window.vdgr.scriptParams(el);
			this.id  	 = this.params['unit'];
			this.jsRoot  = el.src.substring(0, el.src.lastIndexOf("/") - 1);
			this.domain  = window.vdgr.fnGetDomain(this.jsRoot);
			this.view    = null;
			this.sandbox = null;
			this.adapter = null;
			this.cache	 = {};
			this.ad		 = {};
			
			this.loadTO 	= null;
			this.refreshTO  = null;
			
			delete this.params['unit'];
			
			this.counter = 0;
		};
		Unit.prototype = {
				 constructor: Unit,
				 initView	: function() {
					 			this.view 	 = document.createElement("div");
					 			this.view.id = 'vdgr_unit_view_' + this.id;
					 			this.view.style.display = "none";
					 			this.view.style.textAlign = "center";
					 			
					 			this.root.parentElement.insertBefore(this.view, this.root); //  insertAdjacentElement
				 			},		
				 ready		: function() {
					 			this.initView();
					 			this.query();
				 			},
				 refresh	: function() {
					 			this.query();
				 			},
				 query		: function() {
					 			window.vdgr.log('Unit > query');
					 			clearTimeout(this.refreshTO);
					 
					 			var bbox = { "w" : [], "h" : [] };
					 			if(document.defaultView) {
					 				bbox["w"].push( parseInt(document.defaultView.getComputedStyle(this.root.parentElement).getPropertyValue('max-width')) || 0);
					 				bbox["h"].push( parseInt(document.defaultView.getComputedStyle(this.root.parentElement).getPropertyValue('max-height')) || 0);
					 			}
					 			
					 			if(this.root.parentElement.currentStyle) {
					 				bbox["w"].push( parseInt(this.root.parentElement.currentStyle.maxWidth) || 0 );
					 				bbox["h"].push( parseInt(this.root.parentElement.currentStyle.maxHeight) || 0 );
					 			}
					 			
					 			bbox["w"].push(this.root.parentElement.clientWidth);
					 			bbox["w"].push(this.root.parentElement.offsetWidth);
					 			bbox["w"].push(this.root.parentElement.scrollWidth);
					 			
					 			bbox["h"].push(this.root.parentElement.clientHeight);
					 			bbox["h"].push(this.root.parentElement.offsetHeight);
					 			bbox["h"].push(this.root.parentElement.scrollHeight);			
					 			
					 			var uri = "http://" + this.domain + "/new/query/web" +	"?unit=" + this.id +
					 											 						"&dayhour=" + window.vdgr.getDayHour() +
								 											 			"&browser=" + window.vdgr.browserN  + ":" + window.vdgr.browserV + 
								 											 			//"&plaform=" + window.vdgr.platformN + ":" + window.vdgr.platformV + 
								 											 			window.vdgr.arrToQuery(window.vdgr.cache) +
								 											 			window.vdgr.arrToQuery(this.cache) +
								 											 			window.vdgr.arrToQuery(this.params) +
								 											 			//window.vdgr.keysToQuery("adapter", window.vdgr.adapters) +
								 											 			"&platform=android" +
								 											 			"&devclass=phone" +
								 											 			"&bbox=" + (Math.max.apply(Math, bbox["w"]) || 0) + "-" + (Math.max.apply(Math, bbox["h"]) || 0) +
								 											 			"";
					 			window.vdgr.attachJS(this.root, uri);
				 			},
				 render		: function() {
					 			window.vdgr.log('Unit > render');
					 			
					 			if(this.adapter != null) {
					 				this.adapter.invalidate();
					 				this.adapter = null;
					 			}
					 			
					 			if(this.ad.clear == "clear") {
					 				this.scheduleRefresh();
					 				return;
					 			}
					 			
					 			this.sandbox = document.createElement("div");
					 			this.sandbox.id = 'vdgr_unit_view_sanbox_' + this.id + "_" + (this.counter++);
					 			this.sandbox.style.display = "none";
					 			
					 			this.view.appendChild(this.sandbox);
			 				
					 			this.adapter = new this.ad.ad_class();
					 			var that = this;
					 			this.loadTO = setTimeout(function() { window.vdgr.log("Loading timed out."); that.onAdFailed(11); }, (this.ad.ad_timeout || 30) * 1000);
					 			this.adapter.load(this.sandbox, this, this.ad.ad_parameters);
				 			},
				 onAdLoaded : function() {
					 			window.vdgr.log('Unit > onAdLoaded');
					 
					 			if(this.adapter == null) return;
					 			clearTimeout(this.loadTO);
					 			
					 			for(var ci = 0; ci < this.view.children.length; ci++) 			// Clear view
					 				if(this.view.children[ci] != this.sandbox)
					 					this.view.removeChild(this.view.children[ci--]);
					 			
					 			this.view.style.display = "";
					 			this.sandbox.style.display = "";
					 			
					 			if(this.ad.impression_url) {
					 				window.vdgr.log('Unit > onAdLoaded # tracking impression');			// track impression
					 				window.vdgr.attachJS(this.root, this.ad.impression_url);
					 			}
					 			
					 			this.scheduleRefresh();
				 			},
			scheduleRefresh : function() {
								clearTimeout(this.refreshTO);
				
					 			if(this.ad.refresh_time > 0) {									// Schedule refresh
					 				var that = this;
					 				this.refreshTO = setTimeout(function() { window.vdgr.log("Refreshing...");  that.refresh(); }, this.ad.refresh_time * 1000);
					 			}
							},
				 onAdFailed	: function(code) {
					 			window.vdgr.log('Unit > onAdFailed');
					 			
					 			if(this.adapter == null) return;
					 			clearTimeout(this.loadTO);
					 			
					 			if(!this.ad.fail_url) {
					 				window.vdgr.log("Loading failed completely.");
					 				return;
					 			}
					 			
					 			window.vdgr.attachJS(this.root, this.ad.fail_url);
					 			this.ad.fail_url = null;
				 			},
			    onAdClicked : function() {
					 			window.vdgr.log('Unit > onAdClicked');
					 			
					 			if(this.ad.clickthrough_url) {
					 				window.vdgr.log('Unit > onAdClicked # tracking click');
					 				window.vdgr.attachJS(this.root, this.ad.clickthrough_url);
					 			}

					 			
					 			
				 			}
				 
		};
		
		function VidigerAdapter() {
			this.serial		= Math.floor(Math.random() * 100000000);
			this.alias 		= "vidiger";
			this.listener	= null;
			this.unit 		= null;
			this.params	 	= null; 
			this.view 		= null;
			this.root 		= null;
		};
		VidigerAdapter.prototype = {
				constructor	: VidigerAdapter,
				load		: function (root, l, params) {
								window.vdgr.log('VidigerAdapter > load');
								
								this.root 	  = root;
								this.listener = l;
								this.unit	  = l;
								this.params	  = params;
								
								this.params.serial = this.serial;
								this.params.unit   = this.unit.id;
								
								this.render();
							},
				invalidate	: function() {
								delete window.vdgr.tmps[this.serial];
								
								this.listener	= null;
								this.unit 		= null;
								this.params	 	= null; 
								this.view 		= null;
								this.serial 	= null;
								this.root 		= null;
							},
				frameCreate	: function(unitId) {
								var iframe = document.createElement('iframe');
								
							    iframe.id = iframe.name  = "vdgr_frame_" + unitId;
								iframe.scrolling 		 = 'no';
								iframe.frameBorder 		 = 0;
								iframe.marginheight 	 = 0;
								iframe.marginwidth 		 = 0;
								iframe.allowtransparency = 'true';
								iframe.vspace 			 = 0;
								iframe.hspace 			 = 0;
								
								return iframe;
							},
							
				frameBody	: function() {
								xb = ((typeof(this.params.body) != "undefined") ? this.params.body : "");
								this.params.body = "";
								
								
								
					
								s =     (window.vdgr.ieVer() <= 8 ? '<!DOCTYPE html>' : '<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">') +
										'<html>' +
										'<head>' +
										'<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />' +
										'<meta http-equiv="content-type" content="text/html; charset=UTF-8">' +
										'</head>' +
										'<body style="border: 0;margin: 0; padding : 0; width: 100%; height: 100%; ">' +
										'<scr' + 'ipt type="text/javascript">' +
										'var params = ' + JSON.stringify(this.params) + ";" +
										'</scr' + 'ipt>' + 
										'<scr' + 'ipt type="text/javascript" src="' + this.params.script + '"></scr' + 'ipt>' +
										xb +
										'</body>' +
										'</html>';
								
								return s;
							},
				frameDoc	: function(iframe) {
								if (iframe.contentDocument)
									return iframe.contentDocument;
								else if (iframe.contentWindow)
									return iframe.contentWindow.document;
								else if (window.frames[iframe.name]) 
									return window.frames[iframe.name].document;
								
								return null;
							},							
				render 		: function() {
								this.view = document.createElement('div');
								var w = this.params.banner_size[0], h = this.params.banner_size[1];
									
								this.view.id = 'vdgr_unit_container_' + this.unit.id;
								this.view.style.cssText = 	"width: "  + w + "; "
														+	"height: " + h + "; ";

								var iframe = this.frameCreate(this.unit.id);
								iframe.width	= w;
								iframe.height	= h;
								this.view.appendChild(iframe);
								
								this.root.appendChild(this.view);
						
								var iframeDoc 	= this.frameDoc(iframe);
						
								window.vdgr.tmps[this.serial] = this;
								
								iframeDoc.open();
								iframeDoc.write(this.frameBody());
								iframeDoc.close();
								
								
							},
				loaded		: function() {
								window.vdgr.log('VidigerAdapter > loaded');
								
								if(this.listener != null) this.listener.onAdLoaded(this.view);
							},
				failed		: function() {
								window.vdgr.log('VidigerAdapter > failed');
								
								if(this.listener != null) this.listener.onAdFailed(55);
							},							
				clicked		: function() { 
								window.vdgr.log('VidigerAdapter > clicked');
								
								if(this.listener != null) this.listener.onAdClicked();
							}
		};
		
		
		window.vdgr = {	'Unit'			: Unit,
						'globals'		: { 'rendered' 	: [],
											'timestamp'	: Math.round((new Date()).getTime() / 1000),
											'storageTTL': 60 * 5,
											'readyWait'	: 1,
											'isReady'	: false,
											'retries'	: 0
									  	},
						'browsers'		: { 'unknown' 	: 0,
											'chrome' 	: 1,
											'firefox' 	: 2,
											'opera' 	: 3,
											'msie' 		: 4,
											'safari' 	: 5,
											'iemobile'	: 6,
											'operamini' : 7
										},
						'platforms'		: { 'unknown' 	: 0,
											'windows' 	: 1,
											'mac' 		: 2,
											'linux' 	: 3,
											'android' 	: 4,
											'ios' 		: 5,
											'bsd'		: 6,
											'winmobile' : 7
										},

						'cache'			: {},
						'units'			: {},
						'tmps'			: {},
						'adapters' 		: { 'vidiger' : VidigerAdapter },
						
						'unitIds'		: [],
						
						'region'		: 1,
						'cookie'		: (typeof(navigator.cookieEnabled) != 'undefined' ? navigator.cookieEnabled : "true"),
						
						'browserN'		: 0,
						'browserV'		: 0,

						'platformN'		: 0,
						'platformV'		: 0,
						
						'applied'		: false,
						
						'referrer'		: true,

						'keysToQuery'	: function(n, a) {
							var result = "";
							
							for(var k in a)
								result += "&" + n + "=" + k;
			
							return result;
						},
						'ieVer'		    : function() {
											return (navigator.appVersion.indexOf("MSIE") != -1) ? parseFloat(navigator.appVersion.split("MSIE")[1]) : 999;
										},
						'arrToQuery'	: function(a) {
											var result = "";
											
											for(var k in a)
												result += "&" + k + "=" + a[k];
							
											return result;
										},
						'getDayHour'	: function() {
											var d = new Date();
											return "" + ((d.getDay() + 6) % 7) + ":" + d.getHours();	
										},
						'queryParams'	: function(str) {
											
											var qparams = {}; 
											
											if(str) {
												var query_str_split = str.split("&");
											
												for(var param_str_idx in query_str_split) {
													var param_str = query_str_split[param_str_idx].split("=");
													qparams[param_str[0]] = param_str[1];
												}
											}
												
											return qparams;
										},
						'scriptParams'	: function(s) {
											var params = window.vdgr.queryParams(s.src.split("?")[1]);
											var dataPrefix = "data-";
											
											for(var i = 0; i < s.attributes.length; i++) {
												var attr = s.attributes[i];
												
												if(attr.name.length > dataPrefix.length && window.vdgr.strStartsWith(attr.name, dataPrefix))
													params[attr.name.slice(dataPrefix.length)] = attr.value;
											}
											
											return params;
										},
						'contains'		: function(haystack, needle) {
											for(var i = 0; i < haystack.length; i++)
												if(haystack[i] == needle)
													return i;
											
											return -1;
										},
						'storageLoad'	: function(name) {
											try {
												if(typeof(localStorage) == 'undefined')	return null; 
												var pair = localStorage.getItem(name);
												if(!pair) return null;
												pair = JSON.parse(pair); 
												if(pair.ts < window.vdgr.globals.timestamp - window.vdgr.globals.storageTTL) {
													localStorage.removeItem(name);
													return null;
												}
												
												return pair.value;
											} catch(e) {
												return null;
											}
										},
						'storageHas'	: function(name) {
											try {
												if(typeof(localStorage) == 'undefined')	return false;
												return localStorage.getItem(name) != null;
											} catch(e) {
												return false;
											}
										},										
						'storageSave'	: function(name, value) {
											try {
												if(typeof(localStorage) == 'undefined')	return false;
												var pair = { 'ts'	 : window.vdgr.globals.timestamp, 
														 	 'value' : value };
												
												pair = JSON.stringify(pair);
												localStorage.setItem(name, pair);
												return true;
											} catch(e) {
												return false;
											}
										},
						'strStartsWith' : function (s, str){ return s && str && s.slice(0, str.length) == str; },
						'fnGetDomain'	: function(url) { return url.split("/")[2]; },
						'getThisScript' : function () {
											var scripts = document.getElementsByTagName('script');
											var xuri    = "http://api.vidiger.com/static/q.js";
											for(var i = 0; i < scripts.length; i++) {
												var s = scripts[i];
												
												if(window.vdgr.strStartsWith(s.src, xuri)) {
													var p = window.vdgr.scriptParams(s);

													if(p.unit && typeof(window.vdgr.units[p.unit]) == "undefined")
														return s;
												}
											}
											window.vdgr.log("ERROR > should never reach this point under regular conditions");
										},
						'getCookie' 	: function (c_name) {
											var i, x, y, ARRcookies = document.cookie.split(";");
											for (i = 0; i < ARRcookies.length; i++) {
										  		x = ARRcookies[i].substr(0, ARRcookies[i].indexOf("="));
										  		y = ARRcookies[i].substr(ARRcookies[i].indexOf("=") + 1);
										  		x = x.replace(/^\s+|\s+$/g, "");
										  		if(x == c_name)
													return unescape(y);
											}
				
											return null;
										},
						'delCookie'		: function (name) {
											document.cookie = name + '=; path=/; expires=Thu, 01-Jan-70 00:00:01 GMT;';
										}, 
						'setCookie'		: function  (name, value, expires, path, domain, secure) {
										    document.cookie = name + "=" + escape(value) +
										      ((expires) 	? "; expires=" 	+ expires 	: "") +
										      ((path) 		? "; path=" 	+ path 		: "") +
										      ((domain) 	? "; domain=" 	+ domain 	: "") +
										      ((secure) 	? "; secure" 				: "");
										},						
						'doScrollCheck'	: function(f) {
											return function() {
												if (!window.vdgr.globals.isReady) {
													try 	 { document.documentElement.doScroll("left"); 			}
													catch(e) { setTimeout( window.vdgr.doScrollCheck, 1 ); return; 	}	
													window.vdgr.readyCheckF(f)();									
												}
											}
										},

						'readyCheck' 	: function(f, wait) {
											if ( (wait && !--f.readyWait) || (!wait && !f.isReady) ) {
												if ( !document.body )
													return setTimeout( window.vdgr.readyCheckF(f), 1 );
												
												window.vdgr.globals.isReady = true;
												if ( !wait && --f.readyWait > 0 )
													return;
				
												f(); //window.vdgr.activate();
											}							
											
											/*
											if (wait !== true || --window.vdgr.globals.readyWait) return;
											if (wait === true || window.vdgr.globals.isReady) return;
											if ( !document.body ) return setTimeout(window.vdgr.readyCheckF(f), 1);
											
											window.vdgr.globals.isReady = true;
												
											if(wait === true || --window.vdgr.globals.readyWait < 0) f();
											*/												
										},
						'readyCheckF'	: function(f) {
											var rcf = function( wait ) { 
												window.vdgr.readyCheck(f, typeof(wait) != "undefined"); 
											};
											
											return rcf;
										},
						'DOMCL' 		: function(f) {
											if ( document.addEventListener ) {
												var handler = function() {
													document.removeEventListener( "DOMContentLoaded", handler, false );
													window.vdgr.readyCheckF(f)();
												};
											} else if ( document.attachEvent ) {
												var handler = function() {
													if ( document.readyState === "complete" ) {
														document.detachEvent( "onreadystatechange", handler );
														window.vdgr.readyCheckF(f)();
													}
												};
											}

											return handler;
										},
						'onReady'		: function(f) {
											f.readyWait = window.vdgr.globals.readyWait;
											
											if ( document.readyState === "complete" ) 	return setTimeout( window.vdgr.readyCheckF(f), 1 );
											
											if ( document.addEventListener ) {
												document.addEventListener( "DOMContentLoaded", window.vdgr.DOMCL(f), false );	
												window.addEventListener( "load", window.vdgr.readyCheckF(f), false );					// A fallback to window.onload, that will always work
											} else if ( document.attachEvent ) {														// If IE event model is used
												document.attachEvent( "onreadystatechange", window.vdgr.DOMCL(f) );						// ensure firing before onload, maybe late but safe also for iframes
												window.attachEvent( "onload", window.vdgr.readyCheckF(f) );								// A fallback to window.onload, that will always work
										
												var toplevel = false;																	// If IE and not a frame continually check to see if the document is ready
										
												try {
													toplevel = window.frameElement == null;
												} catch(e) {}
										
												if ( document.documentElement.doScroll && toplevel ) 
													window.vdgr.doScrollCheck();
											}
										},
						'attachJS'		: function(elem, src) {
											var js = document.createElement('script');
											js.src = src;
											
											elem.parentElement.insertBefore(js, elem); //  insertAdjacentElement
						  				},
						'log'			: function (msg) {
											if (typeof console === "undefined" || typeof console.log === "undefined") return;
											console.log(msg); 
										},
						'detectBrowser' : function () {
											var ua = navigator.userAgent.toLowerCase();
											var n, v;
											
											if      (/chrome/.test(ua))  	{	n = window.vdgr.browsers.chrome;  	v = parseInt(ua.split("chrome/") 	[1].split(".")[0]);  	}
											else if (/iemobile/.test(ua))	{	n = window.vdgr.browsers.iemobile;	v = parseInt(ua.split("iemobile/") 	[1].split(".")[0]);  	}
											else if (/firefox/.test(ua)) 	{	n = window.vdgr.browsers.firefox; 	v = parseInt(ua.split("firefox/")	[1].split(".")[0]);  	}
											else if (/opera mini/.test(ua)) {	n = window.vdgr.browsers.opera; 	v = parseInt(ua.split("version/")	[1].split(".")[0]);  	}
											else if (/opera/.test(ua)) 	 	{	n = window.vdgr.browsers.opera; 	v = parseInt(ua.split("version/")	[1].split(".")[0]);  	}
											else if (/msie/.test(ua))    	{	n = window.vdgr.browsers.msie; 		v = parseInt(ua.split("msie ")		[1].split(".")[0]);  	}
											else if (/safari/.test(ua))  	{	n = window.vdgr.browsers.safari;  	v = parseInt(ua.split("version/")	[1].split(".")[0]);  	}
											else						 	{	n = window.vdgr.browsers.unknown;  	v = 0;  													}
											
											window.vdgr.browserN = n;
											window.vdgr.browserV = v;		
										},
						'detectPlatform': function () {
											var ua = navigator.userAgent.toLowerCase();
											var n, v;
											
											n = window.vdgr.platforms.unknown;	v = 0;
											
											if      (/windows nt/.test(ua)) {	n = window.vdgr.platforms.windows; 	v = ua	.split("windows nt ") 	[1]
																														   	.split(";")[0]
																															.split(")")[0]
																															.split(" ")[0]
																															.split(".");
																													if(v.length < 2) v[1] = 0;
																														v = v[0] * 10 + v[1]; 										
											} else if (/iphone os/.test(ua)){	n = window.vdgr.platforms.ios;		v = ua	.split("iphone os ") 	[1]
																															.split(";")[0]
																															.split(")")[0];  									
																													if 		(v.indexOf(".") != -1)
																														v = v.split(".");
																													else if	(v.indexOf("_") != -1)
																														v = v.split("_");
																													else if	(v.indexOf(" ") != -1)
																														v = v.split(" ");
																													else 
																														v = [0, 0];
																													
																													if(v.length < 1) v[0] = 0;
																													if(v.length < 2) v[1] = 0;
																													
																													v = v[0] * 10 + v[1]; 											
											} else if (/mac os x/.test(ua))	{	n = window.vdgr.platforms.mac;		v = ua;
																													v = v.split("mac os x ")[1];
																													v = v.split(";")[0];
																													v = v.split(")")[0];  									
																													if 		(v.indexOf(".") != -1)
																													v = v.split(".");
																													else if	(v.indexOf("_") != -1)
																													v = v.split("_");
																													else if	(v.indexOf(" ") != -1)
																													v = v.split(" ");
																													else 
																													v = [0, 0];
																													
																													if(v.length < 1) v[0] = 0;
																													if(v.length < 2) v[1] = 0;
																													
																													v = v[0] * 10 + v[1]; 																			
											}
											
											window.vdgr.platformN = n;
											window.vdgr.platformV = v;

										}
										
		};
		
		// ============================== GLOBAL INITIALISATION ============================
		
		try { window.vdgr.detectBrowser(); } catch(e) { }
		try { window.vdgr.detectPlatform(); } catch(e) { }
		//window.vdgr.detectBrowser();
		//window.vdgr.detectPlatform();
	}
	
	// PER UNIT INITIALISATION =============================================================
	
	var unit = new window.vdgr.Unit(window.vdgr.getThisScript());
	
	if(typeof(window.vdgr.units[unit.id]) == 'undefined') {
		window.vdgr.unitIds.push(unit.id);
		window.vdgr.units[unit.id] = unit;	
		window.vdgr.onReady( function() { unit.ready(); } );
	};
	
})();


