var AWDebug = 0,
    AWGlobalConfig_MasterUrl = "//master.adwired.mobi/services",
    AWGlobalConfig_StaticUrl = "//static.adwired.mobi/content",
    AWapplication_id = "www.adwired.ru",
    AWbanners_list = [],
    AWchallenge_key = "none",
    AWtime_stamp = Math.round((new Date).getTime() / 1E3),
    AW_DMP_delta = 604800,
    AWUID_simulator = "*web*00000000000000000000",
    AWGlobalConfig_ReTime = 1E3,
    AWGlobalConfig_ReLimit = 5;

function AWgetJSON(c, b) {
    callback = "awQuery" + Math.floor(1E9 * Math.random()) + Math.floor(1E9 * Math.random());
    c += "&callback=" + callback;
    eval("window." + callback + " = " + b);
    var d = document.createElement("script");
    d.src = c;
    document.getElementsByTagName("head")[0].appendChild(d)
}

function AWdataPOST(c) {
    AWDebug && AWDebugOut("Start POST submit");
    var b = document.createElement("input");
    b.type = "hidden";
    b.name = c;
    b.value = "";
    c = document.createElement("iframe");
    c.id = "AWSubmitUserData-" + +Math.floor(1E9 * Math.random());
    c.name = c.id;
    var d = document.createElement("form");
    d.method = "POST";
    d.action = "https:" + AWGlobalConfig_MasterUrl + "/updateprofile?dev=" + AWUID_simulator + "&ver=1";
    d.target = c.id;
    var a = document.createElement("div");
    a.style.display = "none";
    AWDebug && AWDebugOut("Start appending");
    document.getElementsByTagName("body")[0].appendChild(a);
    a.appendChild(d);
    a.appendChild(c);
    d.appendChild(b);
    AWDebug && AWDebugOut(d.action);
    d.submit()
}

function AWDebugOut(c) {
    console.log(c)
}

function AWRemoveElement(c) {
    (c = document.getElementById(c)) && c.parentNode.removeChild(c)
}

function AWGetPlatform() {
    for (var c = "Auto", b = ["win", "android", "iphone", "ipad"], d = 0; d < b.length; d++) - 1 < navigator.userAgent.toLowerCase().indexOf(b[d]) && (c = b[d]);
    return c
}

function AWBannerIdToPath(c) {
    c = ("" + c).match(/\d/g);
    for (var b = "", d = 0; d < c.length; d += 1) b += c[d] + "/";
    return b
}

function AWUID_simulator_generate() {
    for (var c = "*web*", b = 0; 30 > b; b += 1) c += Math.round(9 * Math.random());
    return c
}

function isLocalStorageAvailable() {
    try {
        return "localStorage" in window && null !== window.localStorage
    } catch (c) {
        return !1
    }
}

function AWStatCollector(c, b, d) {
    AWgetJSON(AWGlobalConfig_MasterUrl + "/websubmit?vnd=" + navigator.platform + "&mdl=" + navigator.appName + "&udid=" + AWUID_simulator + "&awuid=" + AWUID_simulator + "&app=" + AWapplication_id + "&bid=" + c + "&pid=" + b + "&act=" + d, function(a) {
        AWDebug && AWDebugOut("Master : " + JSON.stringify(a))
    })
}

function AWBannerRefresh(c, b) {
    AWDebug && AWDebugOut("Start Banner JSON getting...");
    var d = AWGlobalConfig_MasterUrl + "/getjson?fn=banner-min&id=" + c;
    AWDebug && AWDebugOut("B Request : " + d);
    localStorage.setItem("TEMP_PIDS_ID_" + c, b);
    AWgetJSON(d, action = function(a) {
        AWDebug && AWDebugOut("Banner JS : " + JSON.stringify(a));
        var b = localStorage.getItem("TEMP_PIDS_ID_" + a.ID);
        localStorage.setItem(a.ID + "_ID", a.ID);
        localStorage.setItem(a.ID + "_Updated", a.Updated);
        localStorage.setItem(a.ID + "_Type", a.Type);
        localStorage.setItem(a.ID + "_pids", b);
        localStorage.setItem(a.ID + "_ViewsMax", a.ViewsMax);
        localStorage.setItem(a.ID + "_PageFrequency", a.PageFrequency);
        localStorage.setItem(a.ID + "_TimeFrequency", a.TimeFrequency);
        localStorage.setItem(a.ID + "_DirectInteraction", a.DirectInteraction);
        localStorage.setItem(a.ID + "_OffLine", a.OffLine);
        localStorage.setItem(a.ID + "_FastClick", a.FastClick);
        localStorage.setItem(a.ID + "_Advanced", a.Advanced);
        localStorage.setItem(a.ID + "_ConnectionType", a.ConnectionType);
        localStorage.setItem(a.ID + "_Keywords", a.Keywords);
        localStorage.setItem(a.ID + "_Image", a.ImageLists[0].src);
        localStorage.setItem(a.ID + "_ZeroCounter", a.ZeroCounter);
        localStorage.setItem(a.ID + "_Url_Link", a.Interactive.Url.Link);
        localStorage.setItem(a.ID + "_Url_Button", a.Interactive.Url.Button);
        localStorage.setItem(a.ID + "_Email_Address", a.Interactive.Email.Address);
        localStorage.setItem(a.ID + "_Email_Button", a.Interactive.Email.Button);
        localStorage.setItem(a.ID + "_Gps_Coordinates", a.Interactive.Gps.Coordinates);
        localStorage.setItem(a.ID + "_Gps_Button", a.Interactive.Gps.Button);
        localStorage.setItem(a.ID + "_Phone_Number", a.Interactive.Phone.Number);
        localStorage.setItem(a.ID + "_Phone_Button", a.Interactive.Phone.Button);
        localStorage.setItem(a.ID + "_AfterCloseType", a.AfterCloseType);
        localStorage.setItem(a.ID + "_OffAfterClose", a.OffAfterClose);
        localStorage.setItem(a.ID + "_OffForSecond", a.OffForSecond);
        localStorage.setItem(a.ID + "_IsCloseable", a.IsCloseable);
        localStorage.setItem(a.ID + "_CloseDelay", a.CloseDelay);
        localStorage.setItem(a.ID + "_AutocloseTime", a.AutocloseTime);
        localStorage.getItem(a.ID + "_lastshow") || localStorage.setItem(a.ID + "_lastshow", 0);
        localStorage.getItem(a.ID + "_views") || localStorage.setItem(a.ID + "_views", 0);
        localStorage.getItem(a.ID + "_closes") || localStorage.setItem(a.ID + "_closes", 0);
        AWDebug && AWDebugOut("Banner #" + localStorage.getItem(a.ID + "_ID") + " in Local Storage")
    })
}

function AWBannersFusion(c, b) {
    AWDebug && AWDebugOut("AWBannersFusion");
    for (var d = JSON.parse("[]"), a = 0; a < b.length; a += 1) d.push(b[a].id), (last_update = localStorage.getItem(b[a].id + "_Updated")) || (last_update = "0"), last_update != b[a].updated && AWBannerRefresh(b[a].id, b[a].pids);
    return JSON.stringify(d)
}

function AWUserDataUpdate(c) {
    AWDebug && AWDebugOut("AWUserDataUpdate START");
    var b = c.uage,
        d = c.umale;
    c = c.uarpu;
    var a = 10,
        e = 10;
    try {
        a = screen.width, e = screen.height
    } catch (h) {
        AWDebug && AWDebugOut("ERROR: " + String(h))
    }
    var f = AWGetPlatform(),
        q = navigator.appName,
        r = navigator.platform,
        l = !1,
        m = localStorage.getItem("AdWired_uage"),
        u = localStorage.getItem("AdWired_umale"),
        n = localStorage.getItem("AdWired_uarpu"),
        s = localStorage.getItem("AdWired_w"),
        k = localStorage.getItem("AdWired_h"),
        g = localStorage.getItem("AdWired_platform"),
        p = localStorage.getItem("AdWired_model"),
        t = localStorage.getItem("AdWired_vendor");
    m != b && "" != b && (localStorage.setItem("AdWired_uage", b), l = !0);
    u != d && "" != d && (localStorage.setItem("AdWired_umale", d), l = !0);
    n != c && "" != c && (localStorage.setItem("AdWired_uarpu", c), l = !0);
    s != a && "" != a && (localStorage.setItem("AdWired_w", a), l = !0);
    k != e && "" != e && (localStorage.setItem("AdWired_h", e), l = !0);
    g != f && "" != f && (localStorage.setItem("AdWired_platform", f), l = !0);
    p != q && "" != q && (localStorage.setItem("AdWired_model", q), l = !0);
    t != r && "" != r && (localStorage.setItem("AdWired_vendor", r), l = !0);
    m = localStorage.getItem("AdWired_dmp_lastsubmit");
    if ("" == m || "undefined" == m) m = "10";
    m = parseInt(m, 10);
    m > AWtime_stamp - AW_DMP_delta && (l = !0);
    AWDebug && AWDebugOut("AWUserDataUpdate: need_update - " + l);
    AWDebug && (l = !0);
    l && (AWdataPOST(JSON.stringify({
        uage: b,
        umale: d,
        uarpu: c,
        w: a,
        h: e,
        platform: f,
        mdl: q,
        vnd: r
    })), localStorage.setItem("AdWired_dmp_lastsubmit", AWtime_stamp))
}

function AWRefreshCore() {
    AWDebug && AWDebugOut("Start refresh-banners");
    var c = navigator.platform,
        b = navigator.appName,
        d = AWGetPlatform(),
        a = 10,
        e = 10;
    try {
        a = screen.width, e = screen.height
    } catch (h) {
        AWDebug && AWDebugOut("ERROR: " + String(h))
    }
    var f = AWGlobalConfig_MasterUrl + "/refresh-banners?js=1&app=" + AWapplication_id,
        f = f + ("&dev=" + AWUID_simulator),
        f = f + ("&awuid=" + AWUID_simulator),
        f = f + ("&ver=1&mdl=" + b) + ("&frw=1&vnd=" + c),
        f = f + ("&platform=" + d),
        f = f + ("&w=" + a),
        f = f + ("&h=" + e);
    AWDebug && AWDebugOut("Refresh-request: " + f);
    AWgetJSON(f,
        action = function(a) {
            AWDebug && AWDebugOut("Refresh-Data: " + JSON.stringify(a));
            localStorage.setItem("AWnext_refresh", a.NextRefresh);
            localStorage.setItem("AWchallenge_key", a.Key);
            localStorage.setItem("AWtime_delta", AWtime_stamp - a.CurrentTime);
            localStorage.setItem("AWbanners_list", AWBannersFusion(AWbanners_list, a.BannersList));
            AWDebug && AWDebugOut("Refresh-Data parsing complete")
        })
}

function AWRefresh() {
    AWDebug && AWDebugOut("Ready to refresh");
    if (isLocalStorageAvailable()) {
        AWUID_simulator = localStorage.getItem("AWUID_simulator");
        AWUID_simulator || (AWUID_simulator = AWUID_simulator_generate(), localStorage.setItem("AWUID_simulator", AWUID_simulator));
        AWDebug && AWDebugOut("AWUID: " + AWUID_simulator);
        try {
            AWbanners_list = JSON.parse(localStorage.getItem("AWbanners_list"))
        } catch (c) {
            AWbanners_list = JSON.parse("[]"), AWDebug && AWDebugOut("ERROR: no banner list in localStorage")
        }
        var b = localStorage.getItem("AWnext_refresh"),
            d = localStorage.getItem("AWlast_refresh");
        localStorage.getItem("AWtime_delta");
        localStorage.getItem("AWchallenge_key");
        b = b ? parseInt(b) : 0;
        d = d ? parseInt(d) : 0;
        AWbanners_list || (AWbanners_list = [])
    } else AWDebug && AWDebugOut("ERROR: NO localStorage");
    AWDebug && AWDebugOut("Ready for refresh");
    AWDebug && (b = 0);
    d + b < AWtime_stamp ? (AWDebug && AWDebugOut("RefreshCore Start"), AWRefreshCore(), localStorage.setItem("AWlast_refresh", AWtime_stamp)) : AWDebug && AWDebugOut("RefreshCore too early\u2026")
}

function AWGetBanner(c) {
    var b = 0,
        d = 0,
        a = "",
        e = "",
        h = "",
        f = "",
        q = "",
        r = "",
        l = "*",
        m = "",
        u = Math.round((new Date).getTime() / 1E3),
        n = "",
        s = "",
        k = "",
        g = "",
        p = 0,
        t = 0,
        v = [];
    AWbanners_list = JSON.parse(localStorage.getItem("AWbanners_list"));
    for (b = 0; b < AWbanners_list.length; b += 1) 
    	d = AWbanners_list[b],
    	g = "", 
    	s = localStorage.getItem(d + "_views"), 
    	k = localStorage.getItem(d + "_closes"),
    	n = localStorage.getItem(d + "_lastshow"), 
    	AWDebug && AWDebugOut("Banner #" + d + " views:  " + s), 
    	AWDebug && AWDebugOut("Banner #" + d + " closes:  " + k), 
    	AWDebug && AWDebugOut("Banner #" + d + " lastshow:  " + n), 
    	a = localStorage.getItem(d + "_ViewsMax"), 
    	e = localStorage.getItem(d + "_PageFrequency"), 
    	h = localStorage.getItem(d + "_TimeFrequency"), 
    	f = localStorage.getItem(d + "_AfterCloseType"), 
    	q = localStorage.getItem(d + "_OffAfterClose"), 
    	r = localStorage.getItem(d + "_OffForSecond"), 
    	l = localStorage.getItem(d + "_pids"), 
    	m = localStorage.getItem(d + "_Type"), 
    	s > a && 0 < a && (g = "VM"), 
    	0 != s % e && 0 < e && (g = "PF"), 
    	u - n < h && (g = !1), 
    	"off_after_close" == f && k > q && (g = "OAC"), 
    	"off_for_second" == f && u - n < r && (g = "OFS"), 
    	-1 == l.indexOf(c) && (g = "P"), 
    	AWDebug && AWDebugOut("Banner #" + d + " pids:  " + l + ". Look for " + c), 
    	"Fullscreen" == m && (a = document.referrer, e = document.domain, a && -1 < a.indexOf(e) && (g = "FR")), 
    	"" == g && (v.push(d), p = n), 
    	AWDebug && AWDebugOut("Banner #" + d + " IS " + g);
    	
    AWDebug && AWDebugOut("Final List: " + v.toString());
    for (b = 0; b < v.length; b += 1) 
    	d = v[b], 
    	n = localStorage.getItem(d + "_lastshow"), 
    	p >= n && (p = n, t = d), 
    	AWDebug && AWDebugOut("Banner #" + d + " lastshow : " + n + " / min : " + p);
    	
    localStorage.setItem(t + "_lastshow", u);
    s = localStorage.getItem(t + "_views");
    s = parseInt(s) + 1;
    localStorage.setItem(t + "_views", s);
    AWDebug && AWDebugOut("SHOW ME Banner #" + t);
    return t
}

function AWBannerClose(c, b, d) {
    b || (b = localStorage.getItem(d + "_closes"), b = parseInt(b) + 1, localStorage.setItem(d + "_closes", b), AWStatCollector(d, "", 2));
    document.getElementById(c).innerHTML = "";
    document.getElementById(c).style.display = "none"
}

function AWActionURL(c, b, d) {
    AWStatCollector(c, b, 4);
    setTimeout(function() {
        window.open(d, "_blank")
    }, 400)
}

function AWActionPHONE(c, b, d) {
    AWStatCollector(c, b, 3);
    setTimeout(function() {
        document.location = "tel:" + d
    }, 400)
}

function AWActionEMAIL(c, b, d) {
    AWStatCollector(c, b, 5);
    setTimeout(function() {
        document.location = "mailto:" + d
    }, 400)
}

function AWActionGPS(c, b, d) {
    AWStatCollector(c, b, 7);
    setTimeout(function() {
        window.open("https://maps.google.ru/maps?q=" + d, "_blank")
    }, 400)
}

function AWActionCANCEL(c, b, d, a, e) {
    AWStatCollector(c, b, 8);
    document.getElementById(e).style.height = a + "px";
    AWRemoveElement(d);
    AWRemoveElement("aw-action-bar-lightbox")
}

function AWAdvBannerCancel(c, b) {
    AWStatCollector(c, b, 8);
    AWRemoveElement("aw-advanced-box-" + c)
}

function AWShowInteractive(c, b, d) {
    AWDebug && AWDebugOut("Click action for Banner # " + c);
    var a = document.getElementById(d),
        e = 61,
        h = document.getElementById(d).offsetHeight,
        f = localStorage.getItem(c + "_Url_Link"),
        q = localStorage.getItem(c + "_Url_Button"),
        r = localStorage.getItem(c + "_Email_Address"),
        l = localStorage.getItem(c + "_Email_Button"),
        m = localStorage.getItem(c + "_Gps_Coordinates"),
        u = localStorage.getItem(c + "_Gps_Button"),
        n = localStorage.getItem(c + "_Phone_Number"),
        s = localStorage.getItem(c + "_Phone_Button"),
        k = localStorage.getItem(c +
            "_FastClick");
    if ("0" == localStorage.getItem(c + "_Advanced")) {
        var g = 0;
        "" != f && (g += 1);
        "" != n && (g += 1);
        "" != r && (g += 1);
        "" != m && (g += 1);
        "1" == k && 1 == g ? ("" != f && AWActionURL(c, b, f), "" != n && AWActionPHONE(c, b, n), "" != r && AWActionEMAIL(c, b, r), "" != m && AWActionGPS(c, b, m)) 
        				   : (
        						   k = document.createElement("div"), 
        						   k.className = "aw-action-bar-lightbox",
        						   k.id = "aw-action-bar-lightbox", 
        						   k.onclick = AWActionCANCEL(c, b, "aw-action-bar" + c, h, d), 
        						   a.appendChild(k), 
        						   k = document.createElement("div"), 
        						   k.className = "aw-action-bar", 
        						   k.id = "aw-action-bar" + c, 
        						   a.appendChild(k),
        						   a = null, 
        						   "" != f && (e += 61, a = document.createElement("div"), a.innerHTML = q, a.onclick = function() { AWActionURL(c, b, f) }, k.appendChild(a)), 
        						   "" != n && (e += 61, a = document.createElement("div"), a.innerHTML = s, a.onclick = function() { AWActionPHONE(c, b, n) }, k.appendChild(a)), 
        						   "" != r && (e += 61, a = document.createElement("div"), a.innerHTML = l, a.onclick = function() { AWActionEMAIL(c, b, r) }, k.appendChild(a)), 
        						   "" != m && (e += 61, a = document.createElement("div"), a.innerHTML = u, a.onclick = function() { AWActionGPS(c, b, m) }, k.appendChild(a)), 
        						   a = document.createElement("div"),
        						   a.innerHTML = "\u041e\u0442\u043c\u0435\u043d\u0438\u0442\u044c", 
        						   a.onclick = function() { WActionCANCEL(c, b, "aw-action-bar" + c, h, d) }, 
        						   k.appendChild(a), 
        						   h < e && (document.getElementById(d).style.height = e + "px"),
        						   e = document.getElementById("aw-action-bar" + c).getAttribute("class"), 
        						   document.getElementById("aw-action-bar" + c).setAttribute("class", e + " visible")
        				   )
    } else {
        var p = document.createElement("div");
        p.id = "aw-advanced-box-" + c;
        p.className = "aw-banner-box-full";
        p.style.height = window.innerHeight + "px";
        window.addEventListener("orientationchange",
            function() {
                p.style.height = window.innerHeight + "px"
            }, !1);
        window.addEventListener("scroll", function() {
            p.style.height = window.innerHeight + "px"
        }, !1);
        window.addEventListener("resize", function() {
            p.style.height = window.innerHeight + "px"
        }, !1);
        a.appendChild(p);
        e = document.createElement("iframe");
        e.id = "aw-advanced-frame-" + c;
        e.src = AWGlobalConfig_StaticUrl + "/AWBanners/" + AWBannerIdToPath(c) + "banner-adv.html?up=" + localStorage.getItem(c + "_Updated");
        e.className = "aw-banner-frame";
        e.scrolling = "no";
        e.border = "0";
        p.appendChild(e);
        e = document.createElement("div");
        e.id = "aw-close-button-id-" + c;
        e.className = "aw-action-close-bar-big";
        e.style.display = "block";
        e.onclick = function() {
            AWAdvBannerCancel(c, b)
        };
        p.appendChild(e)
    }
}

function AWBuildBanner(c, b, d, a) {
    AWDebug && AWDebugOut("Start set up Banner #" + b);
    var e = document.getElementById(c),
        h = localStorage.getItem(b + "_Type"),
        f = document.createElement("iframe"),
        q = "";
    f.id = "aw-banner-frame-" + b;
    f.src = AWGlobalConfig_StaticUrl + "/AWBanners/" + AWBannerIdToPath(b) + "banner-js.html?up=" + localStorage.getItem(b + "_Updated");
    f.className = "aw-banner-frame";
    f.scrolling = "no";
    f.border = "0";
    "Fullscreen" == h ? (q = "aw-action-close-bar-big", e.className = "aw-banner-box-full", e.style.height = window.innerHeight +
        "px", window.addEventListener("orientationchange", function() {
            e.style.height = window.innerHeight + "px"
        }, !1), window.addEventListener("scroll", function() {
            e.style.height = window.innerHeight + "px"
        }, !1), window.addEventListener("resize", function() {
            e.style.height = window.innerHeight + "px"
        }, !1)) : (q = "aw-action-close-bar", e.className = "aw-banner-box-std", e.style.width = a.width, e.style.height = a.height, e.style.position = a.inline ? "relative" : "fixed");
    e.appendChild(f);
    AWDebug && AWDebugOut("Banner #" + b + " type is " + h);
    a = localStorage.getItem(b +
        "_ZeroCounter");
    AWDebug && AWDebugOut("Banner #" + b + " zero counter: " + a);
    "undefined" != a && null != a && "" != a && (0 < a.indexOf("?") && (a = a + "&rnd=" + Math.floor(1E9 * Math.random())), h = document.createElement("div"), h.style.display = "none", e.appendChild(h), f = document.createElement("img"), f.src = a, f.style.width = "1px", f.style.height = "1px", h.appendChild(f));
    "0" == localStorage.getItem(b + "_DirectInteraction") && (a = document.createElement("div"), a.className = "aw-banner-white-screen", a.onclick = function() {
            AWShowInteractive(b, d, c)
        },
        e.appendChild(a));
    "1" == localStorage.getItem(b + "_IsCloseable") && (a = document.createElement("div"), a.className = q, a.id = "aw-close-button-id-" + c, a.onclick = function() {
        AWBannerClose(c, !1, b)
    }, e.appendChild(a), q = localStorage.getItem(b + "_CloseDelay"), a = localStorage.getItem(b + "_AutocloseTime"), setTimeout(function() {
        document.getElementById("aw-close-button-id-" + c).style.display = "block"
    }, 1E3 * q + 10), 0 < a && setTimeout(function() {
        AWBannerClose(c, !0, b)
    }, 1E3 * a + 10))
}

function AWReSetBanner(c, b, d) {
    try {
        AWRefresh()
    } catch (a) {
        AWDebug && AWDebugOut("ERROR:" + a)
    }
    var e = document.getElementById(c).getAttribute("data-place-id");
    d == AWGlobalConfig_ReLimit && (AWStatCollector(0, e, 0), AWUserDataUpdate(b));
    var h = AWGetBanner(e);
    0 == h ? 0 < d && setTimeout(function() {
        AWReSetBanner(c, b, d - 1)
    }, AWGlobalConfig_ReTime) : (AWStatCollector(h, e, 1), AWBuildBanner(c, h, e, b))
}

function AWSetBanner(c, b) {
    if ("undefined" == typeof b || null == b) b = {};
    "inline" in b || (b.inline = !0);
    "height" in b || (b.height = "50px");
    "width" in b || (b.width = "100%");
    "uage" in b || (b.uage = "");
    "umale" in b || (b.umale = "");
    "uarpu" in b || (b.uarpu = "");
    AWReSetBanner(c, b, AWGlobalConfig_ReLimit)
};