<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en" class="no-js">
<!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
<meta charset="utf-8"/>
<title>FAAN | Вход на сайт</title>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta content="width=device-width, initial-scale=1.0" name="viewport"/>
<meta content="" name="description"/>
<meta content="" name="author"/>
<meta name="MobileOptimized" content="320">
<!-- BEGIN GLOBAL MANDATORY STYLES -->
<link href="/global/static/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
<link href="/global/static/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
<link href="/global/static/plugins/uniform/css/uniform.default.css" rel="stylesheet" type="text/css"/>
<!-- END GLOBAL MANDATORY STYLES -->
<!-- BEGIN PAGE LEVEL STYLES -->
<link rel="stylesheet" type="text/css" href="/global/static/plugins/select2/select2_conquer.css"/>
<!-- END PAGE LEVEL SCRIPTS -->
<!-- BEGIN THEME STYLES -->
<link href="/global/static/css/style-conquer.css" rel="stylesheet" type="text/css"/>
<link href="/global/static/css/style.css" rel="stylesheet" type="text/css"/>
<link href="/global/static/css/style-responsive.css" rel="stylesheet" type="text/css"/>
<link href="/global/static/css/plugins.css" rel="stylesheet" type="text/css"/>
<link href="/global/static/css/themes/default.css" rel="stylesheet" type="text/css" id="style_color"/>
<link href="/global/static/css/pages/login.css" rel="stylesheet" type="text/css"/>
<link href="/global/static/css/custom.css" rel="stylesheet" type="text/css"/>
<!-- END THEME STYLES -->
<link rel="shortcut icon" href="favicon.ico"/>
</head>
<!-- BEGIN BODY -->
<body class="login">
<!-- BEGIN LOGO -->
<div class="logo">
	<img src="/global/static/img/logo_centered.png" alt=""/>
</div>
<!-- END LOGO -->
<!-- BEGIN LOGIN -->
<div class="content">
	<!-- BEGIN LOGIN FORM -->
	<form class="login-form" action="index.html" method="post">
		<h3 class="form-title">Войдите в Ваш аккаунт</h3>
		<div class="alert alert-error display-hide">
			<button class="close" data-close="alert"></button>
			<span>
				 Введите любое имя пользователя и пароль.
			</span>
		</div>
		<div class="form-group">
			<!--ie8, ie9 does not support html5 placeholder, so we just show field title for that-->
			<label class="control-label visible-ie8 visible-ie9">Адрес e-mail</label>
			<div class="input-icon">
				<i class="fa fa-envelope"></i>
				<input class="form-control placeholder-no-fix" type="text" autocomplete="off" placeholder="Адрес e-mail" name="email"/>
			</div>
		</div>
		<div class="form-group">
			<label class="control-label visible-ie8 visible-ie9">Пароль</label>
			<div class="input-icon">
				<i class="fa fa-lock"></i>
				<input class="form-control placeholder-no-fix" type="password" autocomplete="off" placeholder="Пароль" name="password"/>
			</div>
		</div>
		<div class="form-actions">
			<label class="checkbox">
			<input type="checkbox" name="remember" value="1"/> Запомнить меня </label>
			<button type="submit" class="btn btn-info pull-right">
			Войти </button>
		</div>
		<div class="forget-password">
			<h4>Забыли пароль ?</h4>
			<p>
				 нажмите <a href="javascript:;" id="forget-password">сюда</a>,
				чтобы восстановить пароль.
			</p>
		</div>
		<div class="create-account">
			<p>
				 У Вас нет аккаунта?&nbsp; <a href="javascript:;" id="register-btn">Создайте его</a>
			</p>
		</div>
	</form>
	<!-- END LOGIN FORM -->
	<!-- BEGIN FORGOT PASSWORD FORM -->
	<form class="forget-form" action="index.html" method="post">
		<h3>Забыли пароль ?</h3>
		<p>
			 Введите Ваш e-mail в поле ниже, чтобы восстановить пароль.
		</p>
		<div class="form-group">
			<div class="input-icon">
				<i class="fa fa-envelope"></i>
				<input class="form-control placeholder-no-fix" type="text" autocomplete="off" placeholder="Адрес e-mail" name="email"/>
			</div>
		</div>
		<div class="form-actions">
			<button type="button" id="back-btn" class="btn btn-default">
			<i class="m-icon-swapleft"></i> Назад </button>
			<button type="submit" class="btn btn-info pull-right">
			Восстановить пароль </button>
		</div>
	</form>
	<!-- END FORGOT PASSWORD FORM -->
	
	<!-- BEGIN REGISTRATION FORM -->
	<form class="register-form" action="index.html" method="post">
		<h3>Регистрация</h3>
		<p>
			 Я регистрируюсь как
		</p>
		<div class="form-group">
			<div class="btn-group regtype" data-toggle="buttons">
				<label class="btn btn-default usrtypebutton">
					<input type="radio" class="toggle">
					<i class="fa fa-mobile-phone usrtypeicon"></i>
					<div class="usrtype">
						 Вебмастер
					</div>
				</label>
				<label class="btn btn-default usrtypebutton">
				<input type="radio" class="toggle">
					<i class="fa fa-desktop usrtypeicon"></i>
					<div class="usrtype">
						 Рекламодатель
					</div>
				</label>
				<label class="btn btn-default usrtypebutton">
				<input type="radio" class="toggle">
					<i class="fa fa-group usrtypeicon"></i>
					<div class="usrtype">
						 Агентство
					</div>
				</label>
			</div>
			<input type="hidden" name="usertype" id="usertype">
		</div>
		
		<div class="form-group">
			<label class="control-label visible-ie8 visible-ie9">Полное имя</label>
			<div class="input-icon">
				<i class="fa fa-font"></i>
				<input class="form-control placeholder-no-fix" type="text" placeholder="Ваше имя и фамилия" name="fullname"/>
			</div>
		</div>
		<div class="form-group">
			<label class="control-label visible-ie8 visible-ie9">Компания</label>
			<div class="input-icon">
				<i class="fa fa-briefcase"></i>
				<input class="form-control placeholder-no-fix" type="text" placeholder="Компания" name="company"/>
			</div>
		</div>
		<div class="form-group">
			<!--ie8, ie9 does not support html5 placeholder, so we just show field title for that-->
			<label class="control-label visible-ie8 visible-ie9">Адрес e-mail</label>
			<div class="input-icon">
				<i class="fa fa-envelope"></i>
				<input class="form-control placeholder-no-fix" type="text" placeholder="Адрес e-mail" name="email"/>
			</div>
		</div>
		<div class="form-group">
			<label class="control-label visible-ie8 visible-ie9">Пароль</label>
			<div class="input-icon">
				<i class="fa fa-lock"></i>
				<input class="form-control placeholder-no-fix" type="password" autocomplete="off" id="register_password" placeholder="Пароль" name="password"/>
			</div>
		</div>
		<div class="form-group">
			<label class="control-label visible-ie8 visible-ie9">Пароль еще раз</label>
			<div class="controls">
				<div class="input-icon">
					<i class="fa fa-check"></i>
					<input class="form-control placeholder-no-fix" type="password" autocomplete="off" placeholder="Пароль еще раз" name="rpassword"/>
				</div>
			</div>
		</div>
		<div class="form-group">
			<label>
			<input type="checkbox" name="tnc"/> Я согласен с <a href="#">Условиями использования</a> и <a href="#">Политикой конфиденциальности</a>
			</label>
			<div id="register_tnc_error">
			</div>
		</div>
		<div class="form-actions">
			<button id="register-back-btn" type="button" class="btn btn-default">
			<i class="m-icon-swapleft"></i> Назад </button>
			<button type="submit" id="register-submit-btn" class="btn btn-info pull-right">
			Зарегистироваться <i class="m-icon-swapright m-icon-white"></i>
			</button>
		</div>
	</form>
	<!-- END REGISTRATION FORM -->
</div>
<!-- END LOGIN -->
<!-- BEGIN COPYRIGHT -->
<div class="copyright">
	 2014 &copy; FAAN. Все права защищены
</div>
<!-- END COPYRIGHT -->
<!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->
<!-- BEGIN CORE PLUGINS -->
<!--[if lt IE 9]>
<script src="/global/static/plugins/respond.min.js"></script>
<script src="/global/static/plugins/excanvas.min.js"></script>
<![endif]-->
<script src="/global/static/plugins/jquery-1.10.2.min.js" type="text/javascript"></script>
<script src="/global/static/plugins/jquery-migrate-1.2.1.min.js" type="text/javascript"></script>
<script src="/global/static/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<script src="/global/static/plugins/bootstrap-hover-dropdown/twitter-bootstrap-hover-dropdown.min.js" type="text/javascript"></script>
<script src="/global/static/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
<script src="/global/static/plugins/jquery.blockui.min.js" type="text/javascript"></script>
<script src="/global/static/plugins/jquery.cokie.min.js" type="text/javascript"></script>
<script src="/global/static/plugins/uniform/jquery.uniform.min.js" type="text/javascript"></script>
<!-- END CORE PLUGINS -->
<!-- BEGIN PAGE LEVEL PLUGINS -->
<script src="/global/static/plugins/jquery.validate.min.js" type="text/javascript"></script>
<script type="text/javascript" src="/global/static/plugins/select2/select2.min.js"></script>
<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="/global/static/scripts/app.js" type="text/javascript"></script>
<script src="/global/static/scripts/login.js" type="text/javascript"></script>
<!-- END PAGE LEVEL SCRIPTS -->
<script>
jQuery(document).ready(function() {     
  App.init();
  Login.init();
});
</script>
<!-- END JAVASCRIPTS -->
</body>
<!-- END BODY -->
</html>