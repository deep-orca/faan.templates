/**
 * Created by limeschnaps on 23.05.14.
 */

(function($) {
    $.fn.limetoggle = function() {
        var self = this;
        if (!self.data("limetoggle-state") || self.data("limetoggle-state") == "hidden" ) {
            self.text(self.data("limetoggle-label-show"));
            $(self.data("limetoggle-bind")).hide();
        } else {
            self.text(self.data("limetoggle-label-hide"));
        }

        self.on("click", function(e) {
            e.preventDefault();
            (self.text() == self.data("limetoggle-label-show")) ? self.text(self.data("limetoggle-label-hide")) : self.text(self.data("limetoggle-label-show"));
            $(self.data("limetoggle-bind")).slideToggle();
        });
        return this;
    };
})(jQuery);