/**
 * Created by limeschnaps on 23.05.14.
 */

(function($) {
    $.fn.templated = function(replacement) {
        var m = this.html();
        $.each(replacement, function(key, value) {
            var tplTag = "{"+key+"}";
            while (m.indexOf(tplTag) >= 0) { m = m.replace(tplTag, value); }
        });
        this.html(m);
        return this
    };

    /*
     * TODO: To do something with this
     */
    $.fn.loadList = function(callback) {
        var el = this;
        $.ajax({
            url: "/ajax/balance/list",
            type: "GET",
            data: el.data(),
            success: function(response) {
                callback(el, response);
            }
        });
        return self;
    };
})(jQuery);