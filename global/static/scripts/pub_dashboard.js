Index = function () {
    return {
        init: function (options) {
            var self = this;
            moment.lang('ru');
            App.addResponsiveHandler(function () {
                jQuery('.vmaps').each(function () {
                    var map = jQuery(this);
                    map.width(map.parent().width());
                });
            });
            self.options = {
                timezone: +(moment().format("ZZ")) * 36
            };
            $.extend(self.options, options);
        },
        initDashboardDaterange: function () {
            var self = this;

            var updateDatePlaceholders = function (from, to) {
                var dateString = from.format('MMMM D, YYYY') + ' - ' + to.format('MMMM D, YYYY');
                $(".date_placeholder").html(dateString);
                $("#dashboard-report-range span").html(dateString);
            };

            $('#dashboard-report-range').daterangepicker(
                {
                    opens: (App.isRTL() ? 'right' : 'left'),
                    startDate: self.options.dateFrom,
                    endDate: self.options.dateTo,
                    minDate: '01.01.2012',
                    maxDate: moment(self.options.dateTo).add("days", 1),
                    dateLimit: {
                        days: 60
                    },
                    showDropdowns: false,
                    showWeekNumbers: true,
                    timePicker: false,
                    timePickerIncrement: 1,
                    timePicker12Hour: false,
                    ranges: {
                        'Сегодня': [moment(), moment()],
                        'Вчера': [moment().subtract('days', 1), moment().subtract('days', 1)],
                        'Последние 7 дней': [moment().subtract('days', 6), moment()]
                    },
                    buttonClasses: ['btn', 'btn-small'],
                    applyClass: 'btn-success',
                    cancelClass: 'btn-default',
                    format: 'DD.MM.YYYY',
                    separator: ' to ',
                    locale: {
                        applyLabel: 'ОК',
                        cancelLabel: 'Отмена',
                        fromLabel: 'С',
                        toLabel: 'По',
                        customRangeLabel: 'Вручную',
                        daysOfWeek: ['Вс', 'Пн', 'Вт', 'Ср', 'Чт', 'Пт', 'Сб'],
                        monthNames: ['Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь', 'Июль', 'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь'],
                        firstDay: 1
                    }
                },
                function (start, end) {
                    updateDatePlaceholders(start, end);
                    var query = {date: start.format("DD.MM.YYYY") + ' - ' + end.format("DD.MM.YYYY")};
                    $("#apps_table").limeTables().fetch(query);
                    Index.fetch(query);
                }
            ).show();

            updateDatePlaceholders(moment().subtract('days', 6), moment());
        },

        initDashboard: function () {
            // TODO: Refactor this shit into plugin. Make it DRY!
            var self = this;
            var query = {
                today: self.options.dateTo.format('DD.MM.YYYY') + ' - ' + self.options.dateTo.format('DD.MM.YYYY'),
                week: self.options.dateFrom.format('DD.MM.YYYY') + ' - ' + self.options.dateTo.format('DD.MM.YYYY')
            };
            $.ajax({
                'url': '/pub/ajax/dashboard/graph/overview',
                'type': 'GET',
                'data': {
                    'date': query.today,
                    'timezone': Index.options.timezone
                },
                success: function (response) {
                    Index.drawCharts("#views_dynamics_daily", response.objects, response.meta);
                }
            });
            $.ajax({
                'url': '/pub/ajax/dashboard/graph/overview',
                'type': 'GET',
                'data': {
                    'date': query.week,
                    'timezone': Index.options.timezone
                },
                success: function (response) {
                    Index.drawCharts("#views_dynamics", response.objects, response.meta);
                }
            });

            $.ajax({
                'url': '/pub/ajax/dashboard/graph/revenue',
                'type': 'GET',
                'data': {
                    'date': query.today,
                    'timezone': Index.options.timezone
                },
                success: function (response) {
                    Index.drawCharts("#revenue_dynamics_daily", response.objects, response.meta);
                }
            });
            $.ajax({
                'url': '/pub/ajax/dashboard/graph/revenue',
                'type': 'GET',
                'data': {
                    'date': query.week,
                    'timezone': Index.options.timezone
                },
                success: function (response) {
                    Index.drawCharts("#revenue_dynamics", response.objects, response.meta);
                }
            });
        },

        fetch: function (query) {
            // TODO: Refactor this shit into plugin lime.chart. Make it DRY!
            if (query == null) {
                query = {}
            }
            var base = {timezone: Index.options.timezone};
            $.extend(query, base);
            $.ajax({
                url: "/pub/ajax/dashboard/graph/overview",
                data: query,
                type: "GET",
                success: function (response) {
                    Index.drawCharts("#views_dynamics", response.objects, response.meta);
                }
            });
            $.ajax({
                url: "/pub/ajax/dashboard/graph/revenue",
                data: query,
                type: "GET",
                success: function (response) {
                    Index.drawCharts("#revenue_dynamics", response.objects, response.meta);
                }
            });
        },

        initTables: function () {
            var table = $("#apps_table");
            table.limeTables({
                columns: function (response) {
                    return [
                        {
                            render: function(data, type, row) {
                                return "<a href='/pub/stat/date#?from="+ response.meta["date_from"] +"&to="+ response.meta["date_to"] +"&application=" + row["application"] + "'>"+ row["name"] +"</a>";
                            }
                        },
                        //{data: "raw", type: "num-fmt"},
                        {data: "impressions", type: "num-fmt"},
                        {data: "revenue", render: "currency", type: "num-fmt" }
                    ]
                },
                footer: {},
                dataTable: {
                    language: {
                        "oPaginate": {
                            "sPrevious": "Назад",
                            "sNext": "Вперед"
                        },
                        "sInfo": "",
                        "sInfoEmpty": "",
                        "sEmptyTable": "Нет данных для отображения"
                    },
                    searching: false,
                    lengthChange: false
                },
                ajax: {
                    url: "/pub/ajax/dashboard/table",
                    data: {
                        date: "{from} - {to}"
                            .replace("{from}", moment(Index.options.dateFrom).format('DD.MM.YYYY'))
                            .replace("{to}", moment(Index.options.dateTo).format('DD.MM.YYYY')),
                        timezone: Index.options.timezone
                    }
                }
            });
            table.limeTables().fetch();
        },

        drawCharts: function (el, data, options) {
            // TODO: Refactor this shit into plugin lime.chart. Make it DRY!
            if (!jQuery.plot) {
                return;
            }

            function showTooltip(title, x, y, contents) {
                $('<div id="tooltip" class="chart-tooltip"><div class="date">' + title + '<\/div><div class="label label-success" style="white-space:normal !important;">' + contents.label + ": " + contents.y + '<\/div><\/div>').css({
                    position: 'absolute',
                    display: 'none',
                    top: y - 100,
                    width: 140,
                    left: x - 70,
                    border: '0px solid #ccc',
                    padding: '2px 6px',
                    'background-color': '#fff'
                }).appendTo("body").fadeIn(200);
            }

            if ($(el).size() != 0) {

                $(el + '_loading').hide();
                $(el + '_content').show();

                $.plot($(el), data, {
                    legend: {
                        show: true,
                        container: $(el + '_content').find('.legend_holder')
                    },
                    series: {
                        lines: {
                            show: true,
                            lineWidth: 2,
                            fillColor: {
                                colors: [
                                    {
                                        opacity: 0.05
                                    },
                                    {
                                        opacity: 0.01
                                    }
                                ]
                            }
                        },
                        points: {
                            show: true
                        },
                        shadowSize: 2
                    },
                    grid: {
                        hoverable: true,
                        clickable: true,
                        tickColor: "#eee",
                        borderWidth: 0
                    },
                    colors: ["#d12610", "#37b7f3", "#52e136", "#ba2fae"],
                    xaxis: {
                        mode: "time",
                        ticks: 5
                    },
                    yaxis: {
                        mode: "categories",
                        ticks: 10,
                        tickDecimals: 0
                    }
                });

                var previousPoint = null;
                $(el).unbind("plothover");
                $(el).bind("plothover", function (event, pos, item) {
                    $("#x").text(pos.x.toFixed(2));
                    $("#y").text(pos.y.toFixed(2));
                    if (item) {
                        if (previousPoint != item.dataIndex) {
                            previousPoint = item.dataIndex;

                            $("#tooltip").remove();

                            var x = item.datapoint[0].toFixed(2),
                                y = item.datapoint[1].toFixed(2);

                            if (item.seriesIndex == 0 && item.series.label == "CPM") {
                                y = parseFloat(y / options.cpm_mult);
                            }

                            var dateFormat = "l HH:mm";
                            if (!options.hourly) {
                                dateFormat = "l";
                            }

                            showTooltip((moment(+x).subtract('hours', Index.options.timezone / 3600).format(dateFormat)), item.pageX, item.pageY, {'x': x, 'y': y, 'label': item.series.label});
                        }
                    } else {
                        $("#tooltip").remove();
                        previousPoint = null;
                    }
                });
            }
        }
    }
}();
