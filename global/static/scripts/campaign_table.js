var TableManaged = function () {

    return {

        //main function to initiate the module
        init: function () {
            
            if (!jQuery().dataTable) {
                return;
            }

var table = $('#campaigns_table').DataTable({
                "bAutoWidth": false , 
                 "aoColumns": [
                    { "bSortable": false, "bSearchable": false },   // Checkboxes
                    { "visible": false },                           // ID
                    {"sWidth": "35%"},                              // Name
                    { "visible": false },                           // Platform
                    { "bSortable": false, "bSearchable": false},    // Media counters
                    { "bSearchable": false },                       // Status
                  { "bSearchable": false },
                  { "bSearchable": false },
                  { "bSearchable": false },
                  { "bSearchable": false }
                ],
                "order": [[ 5, "asc" ]],
                "aLengthMenu": [
                    [15, 25, 50, -1],
                    [15, 25, 50, "Все"]  // change per page values here
                ],
                // set the initial value
                "iDisplayLength": 15,
                "sPaginationType": "bootstrap",
                "oLanguage": {
                    "sLengthMenu": "_MENU_ кампаний",
                    "oPaginate": {
                        "sPrevious": "Назад",
                        "sNext": "Вперед"
                    },
                    "sSearch": "Поиск: ",
                    "sInfo" : "Показано с _START_ по _END_ из _TOTAL_ кампаний",
                    "sInfoempty" : "У Вас пока не добавлено ни одного кампаний"
                },
                "aoColumnDefs": [{
                        'bSortable': false,
                        'aTargets': [0]
                    }
                ]
            });

            jQuery('#campaigns_table .group-checkable').change(function () {
                var set = jQuery(this).attr("data-set");
                var checked = jQuery(this).is(":checked");
                jQuery(set).each(function () {
                    if (checked) {
                        $(this).attr("checked", true);
                    } else {
                        $(this).attr("checked", false);
                    }
                });
                jQuery.uniform.update(set);
            });

            jQuery('#campaigns_table_wrapper .dataTables_filter input').addClass("form-control input-small"); // modify table search input
            jQuery('#campaigns_table_wrapper .dataTables_length select').addClass("form-control input-xsmall"); // modify table per page dropdown
            jQuery('#campaigns_table_wrapper .dataTables_length select').select2(); // initialize select2 dropdown
            $("#campaigns_table_length label").after('<div class="btn-group osfilter" data-toggle="buttons" style="padding-left: 50px;"> <label class="btn btn-default btn-sm filteros active" id=""> <input type="radio" class="toggle"> Все </label> <label class="btn btn-default btn-sm filteros" id="ios"> <input type="radio" class="toggle"> <i class="fa fa-apple"></i></label> <label class="btn btn-default btn-sm filteros" id="android"> <input type="radio" class="toggle"> <i class="fa fa-android"></i></label> <label class="btn btn-default btn-sm filteros" id="windows"> <input type="radio" class="toggle"> <i class="fa fa-windows"></i></label> </div>');
            $('.filteros').on('click', function(e) {
                    table.column(3).search($(this).attr('id')).draw();
            });
            //$(".osfilter").after('<div class="btn-group devclassfilter" data-toggle="buttons" style="padding-left: 350px;"> <label class="btn btn-default btn-sm filteros active" id=""> <input type="radio" class="toggle"> Все </label> <label class="btn btn-default btn-sm filteros" id="ios"> <input type="radio" class="toggle"> <i class="fa fa-apple"></i></label> <label class="btn btn-default btn-sm filteros" id="android"> <input type="radio" class="toggle"> <i class="fa fa-android"></i></label> <label class="btn btn-default btn-sm filteros" id="windows"> <input type="radio" class="toggle"> <i class="fa fa-windows"></i></label> </div>');
 		}

    };

}();