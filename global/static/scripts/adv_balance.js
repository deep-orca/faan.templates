$(document).ready(function () {
    $('.adv_balance').each(function () {
        $(this).validate({
            errorElement: 'span', //default input error message container
            errorClass: 'help-block', // default input error message class
            focusInvalid: false, // do not focus the last invalid input
            ignore: "",
            rules: {
                amount: {
                    min: 1000,
                    required: true,
                    number: true
                }
            },
            errorPlacement: function (error, element) { // render error placement for each input type
                var icon = $(element).parent('.input-icon').children('i');
                icon.removeClass('fa-check').addClass("fa-warning");
                icon.attr("data-original-title", error.text()).tooltip({'container': 'body'});
            },
            success: function (label, element) {
                var icon = $(element).parent('.input-icon').children('i');
                $(element).closest('.form-group').removeClass('has-error').addClass('has-success'); // set success class to the control group
                icon.removeClass("fa-warning").addClass("fa-check");
            },
            highlight: function (element) { // hightlight error inputs
                $(element)
                    .closest('.form-group').addClass('has-error'); // set error class to the control group
            },
            unhighlight: function (element) { // revert the change done by hightlight

            },
            invalidHandler: function (event, validator) { //display error alert on form submit
                $('.alert-danger', validator.currentForm).show();
                $('.alert-success', validator.currentForm).hide();
                App.scrollTo($('.alert-danger', validator.currentForm), -200);
            },
            submitHandler: function (form) {
                $('.alert-success', form).show();
                $('.alert-danger', form).hide();
                window.location.href = form.attr("action");
            }
        })
    });

    moment.lang("ru");

    var quickListFormatter = function(row) {
        return {
            date: moment(row["ts_spawn"]).format("L"),
            ps: function() {
                var psName;
                switch (row["ps"]) {
                    case 0:
                        psName = "Вручную";
                        break;
                    case 1:
                        psName = "WebMoney";
                        break;
                    case 2:
                        psName = "Yandex.Money";
                        break;
                    case 3:
                        psName = "QIWI";
                        break;
                    case 4:
                        psName = "PayPal";
                        break;
                    case 5:
                        psName = "Банковская карта";
                        break;
                    default:
                        break;
                }
                return psName;
            },
            amount: (+row["amount"] > 0 ? "+" : "&ndash;") + Math.abs(+row["amount"]),
            css: (+row["amount"] > 0 ? "success" : "danger")
        }
    };

    var quickListTemplate = "<li class='list-group-item'>{date} &rarr; {ps} <span class='badge badge-{css}'>{amount}</span></li>";

    var onLoad = function(el, response) {
        $(el).empty();
        $.each(response.objects, function() {
            var row = this;
            $(quickListTemplate).templated(quickListFormatter(row)).appendTo(el);
        });
    };

    $(".deposit-quickview a").limetoggle();
    $(".withdraw-quickview a").limetoggle();

    var loadData = function() {
        $(".deposit-quickview ul").loadList(onLoad);
        $(".withdraw-quickview ul").loadList(onLoad);
    }

    loadData();

//    var t = setInterval(loadData, 1000);

});