/**
 * Created by limeschnaps on 08.08.14.
 */


var module = angular.module('Media', ['ui.select2']);

module.controller('MediaController', function($scope, $http) {
    var self = this;

    self.select2Options = {
        matcher: function (term, text, opt) {
            return text.toUpperCase().indexOf(term.toUpperCase()) >= 0 || opt.parent("optgroup").attr("label").toUpperCase().indexOf(term.toUpperCase()) >= 0
        }
    };

//    id          = Column(BigInteger,        nullable=False, primary_key=True, autoincrement=True)     #@ReservedAssignment
//
//    name        = Column(Unicode(64),       nullable=False, default=u"", server_default=""    )
//    campaign    = Column(BigInteger,        nullable=False)
//
//    uri         = Column(Unicode(255),      nullable=False, default=u"", server_default=""    )
//    uri_target = Column(Integer, nullable=False, default=0, server_default="0")
//
//    video       = Column(Unicode(255),      nullable=False, default=u"", server_default=""    )
//    endcard = Column(Unicode(255), nullable=False, default=u"", server_default="")
//    overlay = Column(Unicode(255), nullable=False, default=u"", server_default="")
//    mraid = Column(Unicode(255), nullable=False, default=u"", server_default="")
//
//    video_left  = Column(Unicode(255),      nullable=False, default=u"", server_default=""    )
//    video_right = Column(Unicode(255),      nullable=False, default=u"", server_default=""    )
//
//    banner = Column(Unicode(255), nullable=False, default=u"", server_default="")
//    banner_size = Column(Integer, nullable=False, default=0, server_default="0")
//
//    phone_number = Column(Unicode(20), nullable=False, default=u"", server_default="")
//    action_click = Column(Integer, nullable=False, default=0, server_default="0")
//    action_end = Column(Integer, nullable=False, default=0, server_default="0")
//
//    duration    = Column(Integer,           nullable=False, default=0,   server_default="0"   )
//    closable    = Column(Integer,           nullable=False, default=0,   server_default="0"   )      # view-time to enable "close" button after
//
//    impressions = Column(Integer,           nullable=False, default=0,   server_default="0"   )
//    raw         = Column(Integer,           nullable=False, default=0,   server_default="0"   )
//    clicks      = Column(Integer,           nullable=False, default=0,   server_default="0"   )
//    cost        = Column(DECIMAL,           nullable=False, default=Decimal(0), server_default="0")
//    overlays    = Column(Integer, nullable=False, default=0)               # billed usage counter
//    endcards    = Column(Integer, nullable=False, default=0)               # billed usage counter
//    overlay_cost= Column(DECIMAL, nullable=False, default=Decimal(0))
//    endcard_cost= Column(DECIMAL, nullable=False, default=Decimal(0))
//
//    daily_limit = Column(Integer,           nullable=False, default=0,   server_default="0"   )
//    session_limit=Column(Integer,           nullable=False, default=0,   server_default="0"   )
//
//    type        = Column(Integer,           nullable=False, default=0,   server_default="0"   )      #@ReservedAssignment
//    state       = Column(Integer,           nullable=False, default=0,   server_default="0"   )      #@ReservedAssignment
//    flags       = Column(Integer,           nullable=False, default=0,   server_default="0"   )      #@ReservedAssignment

    self.media = window.media || {
        id: null,
        name: null,
        uri: null,
        uri_target: 1,
        video: null,
        endcard: '/global/static/img/default-endcard.png',
        overlay: null,
        mraid: null,
        banner: null,
        banner_size: null,
        type: -1,
        action_сlick: 0,
        action_end: 0,
        phone_number: null,
        duration: null,
        closable: 10,
        session_limit: 0,
        daily_limit: 0
    };

    // self.videoType = null;
    self.videoType = 0;

    self.isNew = function() { return window.media.id == 0 };

    self.setType = function(type) { self.media.type = type; };
    self.isType = function(type) {
        switch (type) {
            case 'any':
                return -1 != self.media.type;
                break;
            case 'none':
                return -1 == self.media.type;
                break;
            default:
                return type == self.media.type;
                break;
        }
    };

    self.setSize = function(size) { self.media.banner_size = size; };
    self.isSize = function(size) { return size == self.media.banner_size };

    self.setAction = function(action) { self.media.action_click = action; };
    self.isAction = function(action) { return action == self.media.action_click };

    self.setVideoType = function(videoType) { self.videoType = videoType; };
    self.isVideoType = function(videoType) { return videoType == self.videoType; }

    self.selectFile = function() {

    };

    self.videoSelect = {
        items: [],
        fetch: function () {
            var list = this;
            list.items = [];
            $http.get('/adv/ajax/videos')
                .success(function (data) {
                    list.items = data['objects'];
                });
        }
    };

    self.save = function() {
        console.log(self.media);
        $.ajax({
            type: 'post',
            data: self.media
        });
    }
});

module.directive('ionSlider', [function() {
    return {
        restrict: 'A',
        scope: {
            options: '=ionSlider',
            ngModel: '='
        },
        controller: function($scope, $element) {
            var defaults = {
                min: 0,
                max: 45,
                from: $scope.ngModel,
                type: 'single',
                step: 1,
                postfix: " сек.",
                prettify: false,
                hasGrid: true,
                onChange: function() {
                    $element.trigger('change');
                }
            };
            $element.ionRangeSlider($.extend(defaults, $scope.options));
        }
    }
}]);

module.directive('inputSpinner', [function() {
    return {
        restrict: 'A',
        scope: {
            options: '=inputSpinner',
            ngModel: '='
        },
        controller: function($scope, $element) {
            var increaseButton, decreaseButton;

            increaseButton = $('<div class="input-group-btn"><a class="btn btn-default"><i class="fa fa-plus"></i></a></div>');
            decreaseButton = $('<div class="input-group-btn"><a class="btn btn-default"><i class="fa fa-minus"></i></a></div>');

            increaseButton.on('click', function() {
                if ($scope.options.max != undefined && (+$scope.ngModel + ($scope.options.step || 1)) > $scope.options.max) { return }
                $element.val(+$scope.ngModel + ($scope.options.step || 1));
                $element.trigger('change');
            });
            decreaseButton.on('click', function() {
                if ($scope.options.min != undefined && (+$scope.ngModel - ($scope.options.step || 1)) < $scope.options.min) { return }
                $element.val(+$scope.ngModel - ($scope.options.step || 1));
                $element.trigger('change');
            });

            $element.on('change', function() {
                if ($scope.options.max != undefined && +$element.val() > $scope.options.max) { $element.val($scope.options.max) }
                if ($scope.options.min != undefined && +$element.val() < $scope.options.min) { $element.val($scope.options.min) }
            });

            $element.before(decreaseButton);
            $element.after(increaseButton);
        }
    }
}]);

module.directive('fileSelect', [function() {
    return {
        restrict: 'A',
        scope: {
            options: '=fileSelect',
            ngModel: '='
        },
        template: '<div class="preview text-center" style="margin-bottom: 10px;" ng-show="options.preview"></div>\
                   <div class="input-group">\
                       <input ng-model="ngModel" type="text" class="form-control" readonly style="cursor: pointer;" />\
                       <span class="input-group-btn">\
                           <a class="btn btn-default"><i class="fa fa-paperclip"></i> Выбрать файл</a>\
                       </span>\
                   </div>\
                   <input type="file" name="{{id}}" style="display:none;"/>',
        controller: function($scope, $element, $http, $log) {
            var input = $element.find('input[type="file"]'),
                button = $element.find('a.btn'),
                display = $element.find('input[type="text"]'),
                onClick = function() { input.trigger('click'); },
                onChange = function(evt) { display.val(evt.target.files[0].name); $scope.ngModel = evt.target.files[0].name; display.trigger('change') };

            $scope.id = $element.attr('id');

            var setPreview = function(fileURL) {
                $element.find('.preview').empty();
                if ($scope.options.type == 'video') {
                    var playerId = 'player_' + $scope.id;
                    var playerWrap = $('<div></div>').attr('id', playerId).appendTo($element.find('.preview'));
                    jwplayer(playerId).setup({ file: fileURL, height: 200, width: 340 });
                    $log.debug(playerWrap);
                    playerWrap.css({
                        margin: 'auto'
                    });
                }

                if ($scope.options.type == 'image' || !$scope.options.type) {
                    $('<img class="img-thumbnail" style="height:200px;" src="' + fileURL + '"/>').appendTo($element.find('.preview'));
                }
            };

            if ($scope.ngModel) {
                setPreview($scope.ngModel + ($scope.options.type == 'video' ? '.mp4' : ''))
            }

            input.fileupload({
                dataType: 'json',
                url: $scope.options.url,
                replaceFileInput: false,
                send: function () {
                    $log.debug('Send');
                    // TODO: Loader view
                },
                done: function (e, response) {
                    if (response.result.error == 0) {
                        var fileURL = '/global/upload/temp/' + response.result.name;

                        setPreview(fileURL);
                    }
                    if (response.result.error == 1) {

                    }
                }
            });

            button.on('click', onClick);
            display.on('click', onClick);
            display.attr('name', input.attr('id'));
            input.on('change', onChange);
        },
        coltrollerAs: 'input'
    }
}]);
