/**
 * Created by limeschnaps on 30.07.14.
 */

var module;

module = angular.module('Advertiser', ['ui.select2', 'ngBootstrap', 'ngTable']);

module.controller('TableController', function ($http, $scope, $location, $filter, ngTableParams) {
    var self, data, summary, summarize;

    self = this;
    data = [];

    summarize = function () {
        var result = {};
        var defaultSumFn = function (name) {
            return _.reduce(_.pluck(data, name), function (memo, value) {
                return memo + value
            }, 0);
        };

        var customSumFn = {
            ctr: function () {
                return result["clicks"] / result["impressions"] * 100;
            },
            cpm: function () {
                return result["cost"] / result["impressions"] * 1000;
            }//,
            //raw_impressions: function () {
            //    return result["impressions"] / result["raw"] * 100 ;
            //}
        };

        var names = _.reduce(data, function (memo, o) {
            return _.keys(o);
        }, []);

        _.each(_.difference(names, _.keys(customSumFn)), function (name) {
            result[name] = defaultSumFn(name);
        });
        _.each(_.intersection(names, _.keys(customSumFn)), function (name) {
            var sum = customSumFn[name]();
            result[name] = (sum == Infinity || isNaN(sum)) ? 0 : sum;
        });

        return result;
    };

    $scope.$watch('table.options.date', function () {
        self.filterData.from = self.options.date.startDate.format("DD.MM.YYYY");
        self.filterData.to = self.options.date.endDate.format("DD.MM.YYYY");
    });

    self.summary = [];

    self.filterData = angular.extend({
        campaign: null,
        application: null,
        media: null,
        unit: null,
        source: null,
        country: null,
        region: null,
        city: null,
        from: null,
        to: null,
        timezone: +(moment().format("ZZ")) * 36,
        force_hourly: false
    }, $location.search());

    self.setOptions = function () {
        self.options = {
            group: _.last(window.location.pathname.split("/")),
            date: {
                startDate: self.filterData.from ? moment(self.filterData.from, "DD.MM.YYYY") : moment(),
                endDate: self.filterData.to ? moment(self.filterData.to, "DD.MM.YYYY") : moment()
            }
        };
    };

    self.setFilterData = function(data) {
        self.filterData = $.extend(self.filterData, data);
    };

    self.setOptions();

    self.columnDate = function(data) {
        self.setFilterData(data);
        self.setOptions();
        $location.search(self.filterData);
        self.fetch();
    };

    self.columnCampaign = function(data) {
        return 'media#?' + $.param($.extend({}, self.filterData, data));
    };

    self.columnApplication = function (data) {
        return 'unit#?' + $.param($.extend({}, self.filterData, data));
    };

    self.columnUnit = function (data) {
        return 'source#?' + $.param($.extend({}, self.filterData, data));
    };

    self.columnCountry = function (data) {
        return 'region#?' + $.param($.extend({}, self.filterData, data));
    };

    self.columnRegion = function (data) {
        return 'city#?' + $.param($.extend({}, self.filterData, data));
    };

    self.fetch = function() {
        var type = window.location.pathname.split('/')[1];
        $http.get('/{type}/ajax/stat/{groupName}'.replace('{groupName}', self.options.group).replace('{type}', type), {params: self.filterData})
            .success(function (response) {
                $location.search(self.filterData);
                data = response['objects'];
                angular.extend(self.options, response['meta']);
                self.summary = summarize();
                self.config.reload();
            });
    };

    self.config = new ngTableParams({
        page: 1,
        count: 50,
        sorting: {
            'ts_spawn'      : 'asc',
            'campaign'      : 'asc',
            'application'   : 'asc',
            'media'         : 'asc',
            'unit'          : 'asc',
            'source'        : 'asc',
            'country'       : 'asc',
            'region'        : 'asc',
            'city'          : 'asc'
        }
    }, {
        data: data,
        total: 0,
        counts: [50, 100, 250],
        getData: function($defer, params) {
            var orderedData = params.sorting() ? $filter('orderBy')(data, params.orderBy()) : data;
            params.total(data.length);
            $defer.resolve(orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count()));
        }
    });

    self.campaignSelect = {
        items: [],
        init: function() { this.fetch(); },
        fetch: function () {
            var list = this;
            list.items = [];
            $http.get('/adv/ajax/campaign')
                .success(function (data) { list.items = data['objects']; });
        },
        change: function() { self.mediaSelect.fetch(); }
    };

    self.applicationSelect = {
        items: [],
        init: function() { this.fetch(); },
        fetch: function () {
            var list = this;
            list.items = [];
            $http.get('/pub/ajax/application')
                .success(function (data) { list.items = data['objects']; });
        },
        change: function() { self.unitSelect.fetch(); }
    };

    self.mediaSelect = {
        items: [],
        init: function () {
            this.fetch();
        },
        fetch: function () {
            var list = this;
            list.items = [];
            if (self.filterData.campaign) {
                $http.get('/adv/ajax/campaign/{campaignId}/media'.replace('{campaignId}', self.filterData.campaign))
                    .success(function (data) { list.items = data['objects']; });
            } else { list.items = []; }
        }
    };

    self.unitSelect = {
        items: [],
        init: function () {
            this.fetch();
        },
        fetch: function () {
            var list = this;
            list.items = [];
            if (self.filterData.application) {
                $http.get('/pub/ajax/application/{applicationId}/unit'.replace('{applicationId}', self.filterData.application))
                    .success(function (data) { list.items = data['objects']; });
            } else { list.items = []; }
        },
        change: function() { self.sourceSelect.fetch() }
    };

    self.sourceSelect = {
        items: [],
        init: function () {
            this.fetch();
        },
        fetch: function () {
            var list = this;
            list.items = [];
            if (self.filterData.unit) {
                $http.get('/pub/ajax/application/{applicationId}/unit/{unitId}/source'
                    .replace('{applicationId}', self.filterData.application)
                    .replace('{unitId}', self.filterData.unit))
                    .success(function (data) {
                        list.items = data['objects'];
                    });
            } else {
                list.items = [];
            }
        }
    };

    self.countrySelect = {
        items: [],
        init: function () {
            this.fetch();
        },
        fetch: function () {
            var list = this;
            list.items = [];
            $http.get('/any/ajax/country')
                .success(function (data) {
                    list.items = data['objects'];
                });
        },
        change: function () {
            self.regionSelect.fetch();
        }
    };

    self.regionSelect = {
        items: [],
        init: function () {
            this.fetch();
        },
        fetch: function () {
            var list = this;
            list.items = [];
            if (self.filterData.country) {
            $http.get('/any/ajax/country/{countryId}/region'.replace('{countryId}', self.filterData.country))
                .success(function (data) { list.items = data['objects'];
                });
            } else { list.items = []; }
        },
        change: function () {
            self.citySelect.fetch();
        }
    };

    self.citySelect = {
        items: [],
        init: function () {
            this.fetch();
        },
        fetch: function () {
            var list = this;
            list.items = [];
            if (self.filterData.region) {
                $http.get('/any/ajax/country/{countryId}/region/{regionId}/city'.replace('{countryId}', self.filterData.country).replace('{regionId}', self.filterData.region))
                    .success(function (data) { list.items = data['objects']; });
            } else { list.items = []; }
        }
    };

    self.datepickerConfig = {
        locale: {
            applyLabel: 'ОК',
            cancelLabel: 'Отмена',
            fromLabel: 'С',
            toLabel: 'По',
            customRangeLabel: 'Вручную',
            daysOfWeek: ['Вс', 'Пн', 'Вт', 'Ср', 'Чт', 'Пт', 'Сб'],
            monthNames: ['Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь', 'Июль', 'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь'],
            firstDay: 1
        }
    };

    self.control = {
        isActive: function (groups) { return _.contains(groups, self.options.group); }
    };

    self.applyFilter = function() {
        self.fetch();
    };

    self.fetch();
});

//module.directive('timestamp', [function() {
//    return {
//        restrict: 'A',
//        scope: {
//            config: '=timestamp'
//        },
//        template: "{{date}}",
//        controller: function($scope, $element) {
//
//            console.debug($scope.config['timestamp']);
//
//            $scope.date = moment(($scope.config['timestamp'] + +$scope.config['timezone']) * 1000)
//                .format($scope.config['hourly'] ? 'DD.MM.YYYY HH:mm' : 'DD.MM.YYYY')
//        }
//    }
//}]);

module.directive('uniformCheckbox', [function() {
    return {
        restrict: 'A',
        scope: {
            checked: '=ngModel'
        },
        controller: function($element, $scope) {
            $element.uniform();
            if ($scope.checked == true) $element.parent().addClass('checked');
        }
    }
}]);

module.filter('x_number', ['numberFilter', function(numberFilter) {
    return function(val, decimal) {
        if (decimal === undefined) decimal = 2;
        return numberFilter(val, decimal).split(',').join(' ');
    }
}]);

module.filter('x_date', [function() {
    return function(ts, tz, hourly) {
        if (hourly === undefined)
            hourly = false;

        return moment((ts) * 1000)
            .format(hourly ? 'DD.MM.YYYY HH:mm' : 'DD.MM.YYYY')
    }
}]);