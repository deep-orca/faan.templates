var TableManaged = function () {

    return {

        //main function to initiate the module
        init: function () {
            
            if (!jQuery().dataTable) {
                return;
            }

            $('#media_table').dataTable({
                "aoColumns": [
                    { "bSortable": false, "bSearchable": false },
                    { "bSearchable": true },
                    { "bSearchable": false },
                    { "bSearchable": false },
                    { "bSearchable": false }
//                  { "bSearchable": false },
//                  { "bSearchable": false },
//                  { "bSearchable": false },
//                  { "bSearchable": false }
                ],
                order: [[3, 'asc']],
                "aLengthMenu": [
                    [10, 20, 50, -1],
                    [10, 20, 50, "Все"]  // change per page values here
                ],
                // set the initial value
                "iDisplayLength": 10,
                "sPaginationType": "bootstrap",
                "oLanguage": {
                    "sLengthMenu": "_MENU_ медиа",
                    "oPaginate": {
                        "sPrevious": "Назад",
                        "sNext": "Вперед"
                    },
                    "sSearch": "Поиск: ",
                    "sInfo" : "Показано с _START_ по _END_ из _TOTAL_ медиа",
                    "sInfoempty" : "У Вас пока не добавлено ни одного медиа"
                },
                "aoColumnDefs": [{
                        'bSortable': false,
                        'aTargets': [0]
                    }
                ]
            });

            jQuery('#media_table .group-checkable').change(function () {
                var set = jQuery(this).attr("data-set");
                var checked = jQuery(this).is(":checked");
                jQuery(set).each(function () {
                    if (checked) {
                        $(this).attr("checked", true);
                    } else {
                        $(this).attr("checked", false);
                    }
                });
                jQuery.uniform.update(set);
            });

            jQuery('#media_table_wrapper .dataTables_filter input').addClass("form-control input-small"); // modify table search input
            jQuery('#media_table_wrapper .dataTables_length select').addClass("form-control input-xsmall"); // modify table per page dropdown
            jQuery('#media_table_wrapper .dataTables_length select').select2(); // initialize select2 dropdown
 		}

    };

}();