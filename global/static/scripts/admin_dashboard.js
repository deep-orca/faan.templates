/**
* Created by limeschnaps on 06.05.14.
*/

Index = function() {
    return {
        init: function () {
            moment.lang('ru');
            App.addResponsiveHandler(function () {
                jQuery('.vmaps').each(function () {
                    var map = jQuery(this);
                    map.width(map.parent().width());
                });
            });
        },
        initDashboardDaterange: function () {
            var updateDatePlaceholders = function(from, to) {
                var dateString = from.format('MMMM D, YYYY') + ' - ' + to.format('MMMM D, YYYY');
                $(".date_placeholder").html(dateString);
                $("#dashboard-report-range span").html(dateString);
            }

            $('#dashboard-report-range').daterangepicker(
                {
                    opens: (App.isRTL() ? 'right' : 'left'),
                    startDate: moment(),
                    endDate: moment(),
                    minDate: '01.01.2012',
                    maxDate: moment().add('days', 1).format('DD.MM.YYYY'),
                    dateLimit: {
                        days: 60
                    },
                    showDropdowns: false,
                    showWeekNumbers: true,
                    timePicker: false,
                    timePickerIncrement: 1,
                    timePicker12Hour: false,
                    ranges: {
                        'Сегодня': [moment(), moment()],
                        'Вчера': [moment().subtract('days', 1), moment().subtract('days', 1)],
                        'Последние 7 дней': [moment().subtract('days', 6), moment()]
                    },
                    buttonClasses: ['btn', 'btn-small'],
                    applyClass: 'btn-success',
                    cancelClass: 'btn-default',
                    format: 'DD.MM.YYYY',
                    separator: ' to ',
                    locale: {
                        applyLabel: 'ОК',
                        cancelLabel: 'Отмена',
                        fromLabel: 'С',
                        toLabel: 'По',
                        customRangeLabel: 'Вручную',
                        daysOfWeek: ['Вс', 'Пн', 'Вт', 'Ср', 'Чт', 'Пт', 'Сб'],
                        monthNames: ['Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь', 'Июль', 'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь'],
                        firstDay: 1
                    }
                },
                function (start, end) {
                    updateDatePlaceholders(start, end);
                    Index.start = start;
                    Index.end = end;
                    Index.loadData(Index.start, Index.end, Index.filters.numTopApp, Index.filters.numTopCam)
                }
            ).show();

            updateDatePlaceholders(moment(), moment());
        },

        initDashboard: function() {
            Index.start = Index.end = moment().format('M/D/YYYY');
            $.ajax({
                'url': '/ajax/dashboard',
                'type': 'GET',
                'data': {
                    'date': '{from} - {to}'.replace('{to}', moment().format('M/D/YYYY')).replace('{from}', moment().format('M/D/YYYY')),
                    'timezone': +(moment().format("ZZ")) * 36
                },
                success: function(response) {
                    var graphData1 = response.objects.graph_1,
                        graphData2 = response.objects.graph_2,
                        graphTopApps = response.objects.top_apps,
                        graphTopCamps = response.objects.top_campaigns,
                        tableData = response.objects.table;
                    Index.drawCharts("#views_dynamics", graphData2, response.meta);
                    Index.drawCharts("#views_dynamics_daily", graphData1, response.meta);
                    Index.drawCharts("#top_apps", graphTopApps, response.meta);
                    Index.drawCharts("#top_campaigns", graphTopCamps, response.meta);
                    Index.drawTable("#apps_table", tableData,
                        [
                            { data: "date", render: function(data, type, row) {
                                if (row["hourly"] == 0) {
                                    return moment(row["date"]).format('DD.MM.YYYY');
                                }
                                return moment(row["date"]).format('DD.MM.YYYY HH:mm');
                            } },
                            { data: "requests" },
                            { data: "impressions" },
                            { data: "clicks" },
                            { data: "cpm", render: function(data) { return data + " руб"} },
                            { data: "ctr", render: function(data) { return data + "%"} }
                        ]
                    );
                }
            });
        },

        initFilters: function() {
            Index.filters = {numTopCam: +10, numTopApp: +10};
            $("#filter_top_campaign").select2({
                placeholder: "10",
                data: [
                    {id: 10, text:'10'},
                    {id: 5, text:'5'},
                    {id: 3, text:'3'},
                    {id: 1, text:'1'},
                ]
            }).on("select2-selecting", function(e) {
                Index.filters.numTopCam = e.val;
                Index.loadData(Index.start, Index.end, Index.filters.numTopApp, Index.filters.numTopCam);
            });
            $("#filter_top_app").select2({
                placeholder: "10",
                data: [
                    {id: 10, text:'10'},
                    {id: 5, text:'5'},
                    {id: 3, text:'3'},
                    {id: 1, text:'1'},
                ]
            }).on("select2-selecting", function(e) {
                Index.filters.numTopApp = e.val;
                Index.loadData(Index.start, Index.end, Index.filters.numTopApp, Index.filters.numTopCam);
            });
        },

        loadData: function(start, end, filterApp, filterCam) {
            var d = "{start} - {end}"
                    .replace("{start}", moment(start).format('M/D/YYYY'))
                    .replace("{end}", moment(end).format('M/D/YYYY'));
            $.ajax({
                url: "/ajax/dashboard",
                data: {
                    date: d,
                    timezone: +(moment().format("ZZ")) * 36,
                    num_top_apps: filterApp || "",
                    num_top_cams: filterCam || ""
                },
                type: "GET",
                success: function(response) {
                    var graphData1 = response.objects.graph_1,
                        graphData2 = response.objects.graph_2,
                        graphTopApps = response.objects.top_apps,
                        graphTopCamps = response.objects.top_campaigns,
                        tableData = response.objects.table;
                    Index.drawCharts("#views_dynamics", graphData2, response.meta);
                    Index.drawCharts("#views_dynamics_daily", graphData1, response.meta);
                    Index.drawCharts("#top_apps", graphTopApps, response.meta);
                    Index.drawCharts("#top_campaigns", graphTopCamps, response.meta);
                    Index.drawTable("#apps_table", tableData,
                        [
                            { data: "date", render: function(data, type, row) {
                                if (!response.meta.hourly) {
                                    return moment(row["date"]).format('DD.MM.YYYY');
                                }
                                return moment(row["date"]).format('DD.MM.YYYY HH:mm');
                            } },
                            { data: "requests" },
                            { data: "impressions" },
                            { data: "clicks" },
                            { data: "cpm", render: function(data) { return data + " руб"} },
                            { data: "ctr", render: function(data) { return data + "%"} }
                        ]
                    );
                }
            });
        },

        drawTable: function (el, data, columns) {
            try { $(el).DataTable().destroy(); } catch (e) {}

            var lang = {
                "oPaginate": {
                    "sPrevious": "Назад",
                    "sNext": "Вперед"
                },
                "sInfo" : "",
                "sInfoEmpty": "",
                "sEmptyTable" : "Нет данных для отображения"
            }

            // Code below used to add thousand whitespace separator
            // --BEGIN
            $.each(columns, function() {
                var self = this, _render = null;
                var beforeRender = function(data, type, row) { try { return [data.toString().replace(/\B(?=(\d{3})+(?!\d))/g, " "), type, row] } catch(e) { return [data, type, row] }};
                if (self.hasOwnProperty("render")) { _render = self.render }
                self["render"] = function(data, type, row) {
                    var res = beforeRender(data, type, row), _data = res[0], _type = res[1], _row  = res[2];
                    if (_render) { return _render(_data, _type, _row); }
                    return _data;
                };
            });
            // --END

            var footer = function ( row, data, start, end, display ) {
                var api = this.api();
                if (start == end) {
                    for (var j = 1; j <= 5; j++)
                        $(api.column(j).footer()).html("");
                    return;
                }

                // Remove the formatting to get integer data for summation
                var intVal = function ( i ) {
                    return typeof i === 'string' ?
                        i.replace(/[' ']/g, '').replace(/[\$,]/g, '') * 1:
                        typeof i === 'number' ?
                            i : 0;
                };

                // Requests
                var requests = api.column(1).data().reduce( function (a, b) { return intVal(a) + intVal(b); });
                // Impressions
                var impression = api.column(2).data().reduce( function (a, b) { return intVal(a) + intVal(b); });
                // Clicks
                var clicks = api.column(3).data().reduce( function (a, b) { return intVal(a) + intVal(b); });
                // CPM
                var cpm = 0, revenue = 0.0
                for(var i = 0; i < data.length; i++) {
                    revenue += data[i].revenue;
                }
                try {
                    if (revenue > 0.0000001)
                        cpm = revenue / impression * 1000;
                } catch(e) { cpm = 0.0 }
                // CTR
                var ctr = 0;
                try {
                    if (impression != 0)
                        ctr = +clicks / +impression * 100;
                } catch(e) { }

                // Requests
                $(api.column(1).footer()).html(requests.toString().replace(/\B(?=(\d{3})+(?!\d))/g, " "));
                //Impressions
                $(api.column(2).footer()).html(impression.toString().replace(/\B(?=(\d{3})+(?!\d))/g, " "));
                //Clicks
                $(api.column(3).footer()).html(clicks.toString().replace(/\B(?=(\d{3})+(?!\d))/g, " "));
                // CPM
                $(api.column(4).footer()).html(cpm.toFixed(2).toString().replace(/\B(?=(\d{3})+(?!\d))/g, " ") + " руб");
                //CTR
                $(api.column(5).footer()).html(ctr.toFixed(2).toString().replace(/\B(?=(\d{3})+(?!\d))/g, " ") + "%");
            }

            $(el).dataTable({ data: data, columns: columns, language: lang, lengthChange: false, searching: false, order: [], paging: false, footerCallback: footer });
            var wrap = $(el).parent().parent();
            wrap.find('input, select').addClass("form-control input-xsmall");
            wrap.find('select').select2();
        },

        drawCharts: function (el, data, options) {
            if (!jQuery.plot) {
                return;
            }

            var heightArr = [];
            try{ chartsArr[++uid] = {}; } catch(e) { uid = 0; chartsArr = [{}]; }

            // Extend data with idx attributes.
            for (var i = 0; i < data.length; i++) {
                data[i].idx = i + 1;
                // Find out the highest point.
                try {
                    maxHeight = data[i].data[0][1];
                    for (var j = 1; j < data[i].data.length; j++)
                        if (maxHeight < data[i].data[j][1])
                            maxHeight = data[i].data[j][1];
                } catch(e) {
                    maxHeight = 0;
                }
                // Format entries of Height Array -> [state, value]
                heightArr[i] = [1, maxHeight]
            }

            function showTooltip(title, x, y, width, contents) {
                console.log(contents);
                $('<div id="tooltip" class="chart-tooltip"><div class="date">' + title + '<\/div><div class="label label-success">' + contents.y + '<\/div><\/div>').css({
                    position: 'absolute',
                    display: 'none',
                    top: y - 100,
                    width: width,
                    left: x - 40,
                    border: '0px solid #ccc',
                    padding: '2px 6px',
                    'background-color': '#fff'
                }).appendTo("body").fadeIn(200);
            }

            if ($(el).size() != 0) {
                var previousPoint2 = null;
                var plot_statistics = null;

                chartsArr[uid].onClick = function(seriesIdx) {
                    var previousPoint2 = plot_statistics.getData();
                    seriesIdx--;  // ***HERE***
                    previousPoint2[seriesIdx].points.show = // ***AND HERE***
                    previousPoint2[seriesIdx].lines.show = !previousPoint2[seriesIdx].lines.show;
                    plot_statistics.setData(previousPoint2);

                    heightArr[seriesIdx][0] = ! heightArr[seriesIdx][0];
                    // Find point with the max height.
                    for (var i = 0, maxHeight = 0; i < heightArr.length; i++)
                        if (heightArr[i][0] && heightArr[i][1] > maxHeight)
                            maxHeight = heightArr[i][1];

                    var axes = plot_statistics.getAxes();
                    if (maxHeight == 0)
                        axes.yaxis.options.max = 1;
                    else {
                        // Setup the maxHeight with corresponding offset.
                        var j, increment;
                        for(j = 10; j < maxHeight; j *= 10);
                        if (j == 10) {
                            increment = 1;
                            axes.yaxis.options.max = maxHeight + increment;
                        } else {
                            increment = j / 20;
                            axes.yaxis.options.max = (maxHeight + increment) - (maxHeight + increment) % increment;
                        }
                    }
                    plot_statistics.setupGrid();
                    plot_statistics.draw();
                }

                $(el+'_loading').hide();
                $(el+'_content').show();

                var _uid = uid.toString();

                plot_statistics = $.plot($(el), data, {
                    legend: {
                        show: true,
                        container: $(el+'_content').find('.legend_holder'),
                        labelFormatter: function(label, series) {
                            return '<a href="#" onClick="chartsArr['+ _uid + '].onClick('+series.idx+'); if ( ! $(this).attr(\'style\')) { $(this).attr(\'style\', \'color: #5A009D;\'); } else {$(this).attr(\'style\', \'\'); } return false;">'+label+'</a>';
                        }
                    },
                    series: {
                        lines: {
                            show: true,
                            lineWidth: 2,
                            fill: true,
                            fillColor: {
                                colors: [{
                                        opacity: 0.05
                                    }, {
                                        opacity: 0.01
                                    }
                                ]
                            }
                        },
                        points: {
                            show: true
                        },
                        shadowSize: 2
                    },
                    grid: {
                        hoverable: true,
                        clickable: true,
                        tickColor: "#eee",
                        borderWidth: 0
                    },
                    colors: ["#d12610", "#37b7f3", "#52e136", "#ba2fae"],
                    xaxis: {
                        mode: "time",
                        ticks: 5
                    },
                    yaxis: {
                        mode: "categories",
                        ticks: 10,
                        tickDecimals: 0
                    }
                });

                $(el).unbind("plothover");
                $(el).bind("plothover", function (event, pos, item) {
                    $("#x").text(pos.x.toFixed(2));
                    $("#y").text(pos.y.toFixed(2));
                    if (item) {
                        if (previousPoint2 != item.dataIndex) {
                            previousPoint2 = item.dataIndex;

                            $("#tooltip").remove();
                            var x = item.datapoint[0].toFixed(2), y;
                            var label = item.series.label, width;

                            if (label == "eCPM" || label == "Прибыль")
                                y = item.datapoint[1].toFixed(2).toString().replace(/\B(?=(\d{3})+(?!\d))/g, " ");
                            else
                                y = item.datapoint[1].toString().replace(/\B(?=(\d{3})+(?!\d))/g, " ");


                            var dateFormat = "l HH:mm";
                            if (!options.hourly) {
                                dateFormat = "l";
                            }

                            if (label.length <= '00.00.0000'.length)
                                width = 100;
                            else {
                                if (String.width === undefined) {
                                    String.prototype.width = function(font) {
                                        var f = font || '12px arial',
                                            o = $('<div>' + this + '</div>')
                                                  .css({'position': 'absolute', 'float': 'left',
                                                        'white-space': 'nowrap', 'visibility': 'hidden', 'font': f}).appendTo($('body')),
                                            w = o.width();
                                        o.remove();
                                        return w;
                                    }
                                }
                                //width = Math.trunc(label.length * 8.5);
                                width = label.width('14px arial');
                            }
                            var tz = +moment().format('ZZ') / 100;
                            showTooltip(label + "<br>" + (moment(+x).subtract('hours', tz).format(dateFormat)), item.pageX, item.pageY, width, {'x': x, 'y': y});
                        }
                    } else {
                        $("#tooltip").remove();
                        previousPoint2 = null;
                    }
                });
            }
        }
    }
}();