function add_input_error(input_id){
    var pdiv  = $(input_id).parent();
    pdiv.addClass('has-error');
    $('.xform-error',pdiv).show();
}
function remove_input_error(input_id){
    var pdiv  = $(input_id).parent();
    pdiv.removeClass('has-error');
    $('.xform-error',pdiv).hide();
}
function check_error(input_id){
    if ($(input_id).val() != ''){
        remove_input_error(input_id);
    }
}



$(".but1").on("click", function(e){
	var mname = $('#mediaName').val();
    var muri  = $('#mediaURI').val();
    if (mname != '' && muri != ''){
        remove_input_error('#mediaName');
        remove_input_error('#mediaURI');
        $("#collapse_1").collapse("hide");
	    $("#collapse_2").collapse("show");
    }
    else{
        if (mname == ''){
            add_input_error('#mediaName');
        }
        else{
            remove_input_error('#mediaName');
        }
        if (muri == ''){
            add_input_error('#mediaURI');
        }
        else{
            remove_input_error('#mediaURI');
        }
    }
})


$(".but2").on("click", function(){
    if ($('#video').val() != ''){
        $('.xv').hide();
        $("#collapse_2").collapse("hide");
	    $("#collapse_3").collapse("show");
    }
    else{
        $('.xv').show();
    }
})

$(".but3").on("click", function(){
	$("#collapse_3").collapse("hide");
	$("#collapse_4").collapse("show");
})

$(".but5").on("click", function(){
	$("#collapse_5").collapse("hide");
	$("#collapse_6").collapse("show");
})

$(".butb1").on("click", function(){
	$("#collapse_2").collapse("hide");
	$("#collapse_1").collapse("show");
})

$(".butb2").on("click", function(){
	$("#collapse_3").collapse("hide");
	$("#collapse_2").collapse("show");
})

$(".butb3").on("click", function(){
	$("#collapse_4").collapse("hide");
	$("#collapse_3").collapse("show");
})

$(".butb4").on("click", function(){
	$("#collapse_5").collapse("hide");
	$("#collapse_4").collapse("show");
})

$(".butb5").on("click", function(){
	$("#collapse_6").collapse("hide");
	$("#collapse_5").collapse("show");
})


$(".but4").on("click", function(){
	$("#collapse_4").collapse("hide");
	$("#collapse_5").collapse("show");
	var id = $("#id").val();
	var close = $("#closable").val();
	if (id=="None")
		ioninit(5);
	else 
		ioninit(close);
})

$(".a5").on("click", function(e){
	e.preventDefault();
	$("#collapse_5").collapse("show");
	var id = $("#id").val();
	var close = $("#closable").val();
	if (id=="None")
		ioninit(5);
	else 
		ioninit(close);
})






var upload_url = document.URL.split('/media')[0]+'/media/upload/';
//alert (upload_url);
	
	$(function () {
	    $('#data-video').fileupload({ dataType: 'json', url : upload_url + 'video', send : function(){$(".progress").addClass("spinner");}, done : function (e, data) {
			if(data.result.error) return alert("Ошибка!");
			if ($(".ma").length!=0)
				jwplayer().load({file:'/global/upload/temp/' + data.result.name}).play();
			else{
				$(".vidthumb").show();
				jwplayer("preview").setup({
                    file: '/global/upload/temp/' + data.result.name,
                    height: 180,
                    skin: 'bekle',
                    width: 320
                }).play();
			}
			//$("#new_video").attr('src', '/global/upload/temp/' + data.result.name);
			$("#video").val(data.result.name);
			$(".progress").removeClass("spinner");
	    }});

	    $('#data-endcard').fileupload({ dataType: 'json', url : upload_url + 'endcard', send : function(){$(".progress").addClass("spinner");}, done : function (e, data) {
			if(data.result.error) return alert("Ошибка!");
			$("#new_endcard").attr('src', '/global/upload/temp/' + data.result.name).show();
			$("#endcard").val(data.result.name);
			$(".progress").removeClass("spinner");
			$("#endcard_cost").html("0.5");
			summary();
	    }});
	    
	    $('#data-overlay').fileupload({ dataType: 'json', url : upload_url + 'overlay', send : function(){$(".progress").addClass("spinner");}, done : function (e, data) {
			if(data.result.error) return alert("Ошибка!");
			$("#olay").attr('src', '/global/upload/temp/' + data.result.name);
			if ($("#overlay").val()=='')		
				$(".olay").show();
			$("#overlay").val(data.result.name);
			$(".progress").removeClass("spinner");
			$("#overlay_cost").html("0.3");
			summary();
	    }});	    
	    
	   /* $("#video_reset").click(function() {
	    	$("#video").val('');
	    	$("#new_source").attr('src', 'x');
	    	$("#new_video").hide();
	    });
	    
	    $("#endcard_reset").click(function() {
	    	$("#endcard").val('');
	    	$("#new_endcard").attr('src', 'x');
	    	$("#new_endcard").hide();
	    });
	    
	    $("#overlay_reset").click(function() {
	    	$("#overlay").val('');
	    	$("#new_overlay").attr('src', 'x');
	    	$("#new_overlay").hide();
	    }); 	*/    
	});

$('#spinner1').spinner({min: 0});
$('#spinner2').spinner({min: 0});


//FIXED BLOCK
$(function() {
	var offset = $(".fixed").offset();
	var topPadding = 65;
//	$(window).scroll(function() {
//		if ($(window).scrollTop() > offset.top - 35) {
//			$(".fixed").stop().animate({marginTop: $(window).scrollTop() - offset.top + topPadding}, 200, 'linear');
//		}
//		else {$(".fixed").stop().animate({marginTop: 0}, 200, 'linear');};});

    $('#save_form_btn').click(function(){
        check_error('#mediaName');
        check_error('#mediaURI');
        var video_name = $('#video').val();
        if (video_name != ''){
            $('.xv').hide();
        }
        if ($('#mediaName').val() == ''){
            if (document.URL.split('media/')[1] == 'new'){
                $("#collapse_1").collapse("show");
                $("#collapse_6").collapse("hide");
            }
            else{
                $('body').animate({ scrollTop: 0 }, "slow");
            }
            add_input_error('#mediaName');
        }
        else if($('#mediaURI').val() == ''){
            if (document.URL.split('media/')[1] == 'new'){
                $("#collapse_1").collapse("show");
                $("#collapse_6").collapse("hide");
            }
            else{
                $('body').animate({ scrollTop: 0 }, "slow");
            }

            add_input_error('#mediaURI');

        }
        else if (video_name  == '' && document.URL.split('media/')[1] == 'new'){

            if (document.URL.split('media/')[1] == 'new'){
                $("#collapse_2").collapse("show");
                $("#collapse_6").collapse("hide");
            }
            else{
                $('body').animate({ scrollTop: 200 }, "slow");
            }

            $('.xv').show();
        }
        else{
            $('#add_media_form').submit()
        }

    });

});

function summary(){
	var sum = parseFloat($("#endcard_cost").text()) + parseFloat($("#overlay_cost").text()) + parseFloat($("#closable_cost").text());
	var sum_plus_campaign = parseFloat($("#campaign_cost").text()) + sum;
	$(".total_cost").text( sum_plus_campaign + " руб.");
	$("#cost").val(sum);
}

function ioninit(fromnum){
	$("#range_close").ionRangeSlider({
    	    min: 0,
    	    max: 45,
    	    from: fromnum,
    	    type: 'single',
    	    step: 1,
    	    postfix: " сек.",
    	    prettify: false,
    	    hasGrid: true,
    		onChange: function(obj) {
    		    $("#closable").val(obj.fromNumber);
    		    $("#closable_cost").html((obj.fromNumber*.3).toFixed(2));
    		   summary();
    		}
    	});
}
