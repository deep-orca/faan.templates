
function format(state) {
    if (!state.id) return state.text; // optgroup
    return "<img class='flag' src='../assets/img/flags/" + state.id.toLowerCase() + ".png'/>&nbsp;&nbsp;" + state.text;
}

$("#select2_sample4").select2({
    placeholder: "Select a Country",
    allowClear: true,
    formatResult: format,
    formatSelection: format,
    escapeMarkup: function (m) {
        return m;
    }
});



$(".apptypebutton").on( "click", function() {
	$("#apptype").val(this.id);
});