$.fn.serializeObject = function () {
    var o = {};
    var a = this.serializeArray();
    $.each(a, function () {
        if (o[this.name] !== undefined) {
            if (!o[this.name].push) {
                o[this.name] = [o[this.name]];
            }
            o[this.name].push(this.value || '');
        } else {
            o[this.name] = this.value || '';
        }
    });
    return o;
};

Index = function () {
    return {
        init: function (options) {
            var self = this;
            moment.lang('ru');

            App.addResponsiveHandler(function () {
                jQuery('.vmaps').each(function () {
                    var map = jQuery(this);
                    map.width(map.parent().width());
                });
            });

            var args = {};
            $.each(window.location.search.replace("?", "").split("&"), function () {
                var k, v;
                k = this.split("=")[0];
                v = this.split("=")[1];
                if (k == "date") {
                    v = _.contains(v, "+") ? v.split("+-+") : v.split(" - ");
                    args[k + "From"] = moment(v[0], "DD.MM.YYYY");
                    args[k + "To"] = moment(v[1], "DD.MM.YYYY");
                } else {
                    args[k] = +v;
                }
            });

            self.options = {
                application: null,
                zone: null,
                country: null,
                region: null,
                city: null,
                dateFrom: moment(),
                dateTo: moment(),
                group: _.last(window.location.pathname.split("/")),
                timezone: +(moment().format("ZZ")) * 36,
                dateRangePickerConf: function (from, to) {
                    return {
                        opens: (App.isRTL() ? 'right' : 'left'),
                        startDate: moment(from),
                        endDate: moment(to),
                        minDate: '01.01.2012',
                        maxDate: moment().add('days', 1).format('DD.MM.YYYY'),
                        // dateLimit: {
                        //     days: 60
                        // },
                        showDropdowns: false,
                        showWeekNumbers: true,
                        timePicker: false,
                        timePickerIncrement: 1,
                        timePicker12Hour: false,
                        buttonClasses: ['btn', 'btn-small'],
                        applyClass: 'btn-success',
                        cancelClass: 'btn-default',
                        format: 'DD.MM.YYYY',
                        separator: ' to ',
                        locale: {
                            applyLabel: 'ОК',
                            cancelLabel: 'Отмена',
                            fromLabel: 'С',
                            toLabel: 'По',
                            customRangeLabel: 'Вручную',
                            daysOfWeek: ['Вс', 'Пн', 'Вт', 'Ср', 'Чт', 'Пт', 'Сб'],
                            monthNames: ['Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь', 'Июль', 'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь'],
                            firstDay: 1
                        }
                    }
                }
            };

            $.extend(self.options, options)
        },

        initDaterange: function (el) {
            var self = this;
            var updateDatePlaceholders = function (from, to) {
                var dateString = from.format('DD.MM.YYYY') + ' - ' + to.format('DD.MM.YYYY');
                $(".date_placeholder").html(dateString);
                $(el).val(dateString);
            };
            $(el).daterangepicker(self.options.dateRangePickerConf(self.options.dateFrom, self.options.dateTo), function (start, end) {
                updateDatePlaceholders(start, end);
            }).show();

            updateDatePlaceholders(self.options.dateFrom, self.options.dateTo);
        },

        initSelect2: function () {
            var self = this;
            var format = function (item) {
                return item["name"]
            };

            $(".ajax-select2.zone").select2({
                placeholder: "Zone",
                ajax: {
                    url: function () {
                        return "/pub/ajax/application/" + self.options.application + "/zone"
                    },
                    type: "GET",
                    results: function (data) {
                        return {results: data.objects, text: 'name' }
                    },
                    data: function (term) {
                        return { q: term }
                    }
                },
                formatSelection: format,
                formatResult: format,
                initSelection: function (el, callback) {
                    $.ajax({
                        url: (function () {
                            return "/pub/ajax/application/" + self.options.application + "/zone"
                        })(),
                        type: "GET",
                        success: function (response) {
                            callback($.grep(response.objects, function (e) {
                                return e.id == self.options.zone
                            })[0]);
                        }
                    })
                }
            }).select2("enable", false);

            $(".ajax-select2.application").select2({
                placeholder: "Zone",
                ajax: {
                    url: "/pub/ajax/application",
                    type: "GET",
                    results: function (data) {
                        return {results: data.objects, text: 'name'}
                    },
                    data: function (term) {
                        return { q: term }
                    }
                },
                formatSelection: format,
                formatResult: format,
                initSelection: function (el, callback) {
                    $.ajax({
                        url: "/pub/ajax/application",
                        type: "GET",
                        success: function (response) {
                            callback($.grep(response.objects, function (e) {
                                return e.id == self.options.application
                            })[0]);
                        }
                    })
                }
            }).on("change", function (evt) {
                self.options.application = evt.val;
                if (self.options.application != null && self.options.application != '') $(".ajax-select2.zone").select2("enable", true).select2("val", null); else self.options.application = null;
            });

            $(".ajax-select2.city").select2({
                placeholder: "City",
                ajax: {
                    url: function () {
                        return "/adv/ajax/country/" + self.options.country + "/region/" + self.options.region + "/city"
                    },
                    type: "GET",
                    results: function (data) {
                        return {results: data.objects, text: 'name'}
                    },
                    data: function (term) {
                        return { q: term }
                    }
                },
                formatSelection: format,
                formatResult: format,
                initSelection: function (el, callback) {
                    $.ajax({
                        url: function () {
                            return "/adv/ajax/country/" + self.options.country + "/region/" + self.options.region + "/city"
                        },
                        type: "GET",
                        success: function (response) {
                            callback($.grep(response.objects, function (e) {
                                return e.id == self.options.city
                            })[0]);
                        }
                    })
                }
            }).select2("enable", false);

            $(".ajax-select2.region").select2({
                placeholder: "Region",
                ajax: {
                    url: function () {
                        return "/adv/ajax/country/" + self.options.country + "/region"
                    },
                    type: "GET",
                    results: function (data) {
                        return {results: data.objects, text: 'name'}
                    },
                    data: function (term) {
                        return { q: term }
                    }
                },
                formatSelection: format,
                formatResult: format,
                initSelection: function (el, callback) {
                    $.ajax({
                        url: function () {
                            return "/adv/ajax/country/" + self.options.country + "/region"
                        },
                        type: "GET",
                        success: function (response) {
                            callback($.grep(response.objects, function (e) {
                                return e.id == self.options.region
                            })[0]);
                        }
                    })
                }
            }).select2("enable", false).on("change", function (evt) {
                self.options.region = evt.val;
                if (self.options.region != null && self.options.region != '') {
                    $(".ajax-select2.city").select2("enable", true).select2("val", null);
                } else {
                    self.options.region = null;
                    $(".ajax-select2.city").select2("enable", false).select2("val", null);
                }
            });

            $(".ajax-select2.country").select2({
                placeholder: "Country",
                ajax: {
                    url: "/adv/ajax/country",
                    type: "GET",
                    results: function (data) {
                        return {results: data.objects, text: 'name'}
                    },
                    data: function (term) {
                        return { q: term }
                    }
                },
                formatSelection: format,
                formatResult: format,
                initSelection: function (el, callback) {
                    $.ajax({
                        url: "/adv/ajax/country",
                        type: "GET",
                        success: function (response) {
                            callback($.grep(response.objects, function (e) {
                                return e.id == self.options.country
                            })[0]);
                        }
                    })
                }
            }).on("change", function (evt) {
                self.options.country = evt.val;
                $(".ajax-select2.city").select2("enable", false).select2("val", null);
                if (self.options.country != null && self.options.country != '') {
                    $(".ajax-select2.region").select2("enable", true).select2("val", null);
                } else {
                    self.options.country = null;
                    $(".ajax-select2.region").select2("enable", false).select2("val", null);
                }
            });

            if (self.options.application) {
                $(".ajax-select2.application").select2("val", self.options.application);
                $(".ajax-select2.zone").select2("val", self.options.zone);
                $(".ajax-select2.zone").select2("enable", true);
            }

            if (self.options.country) {
                $(".ajax-select2.country").select2("val", self.options.country);
                $(".ajax-select2.region").select2("val", self.options.region).select2("enable", true);
            }

            if (self.options.region) {
                $(".ajax-select2.region").select2("val", self.options.region);
                $(".ajax-select2.city").select2("val", self.options.city).select2("enable", true);
            }
        },

        initTables: function () {
            var table = $("#applications_table");
            table.limeTables({
                columns: function (response) {
                    return [
                        {render: function (data, type, row) {
                            if (row["application_name"])
                                return row["application_name"];
                            if (row["zone_name"])
                                return row["zone_name"];
                            if (row["country_name"])
                                return row["country_name"];
                            if (row["region_name"])
                                return row["region_name"];
                            if (row["city_name"])
                                return row["city_name"];
                            if (row["ts_spawn"])
                                return moment(+(row["ts_spawn"])).format(response.meta.hourly ? "DD.MM.YYYY HH:mm" : "DD.MM.YYYY");
                            else
                                return "Error"
                        }},
                        {data: "raw", type: "num-fmt"},
                        {data: "impressions", type: "num-fmt"},
                        {data: "raw_impressions", render: "percent", type: "num-fmt"},
                        {data: "revenue", render: "currency", type: "num-fmt"},
                        {data: "cpm", render: "currency", type: "num-fmt"}
                    ]
                },
                dataTable: {
                    language: {
                        "oPaginate": {
                            "sPrevious": "Назад",
                            "sNext": "Вперед"
                        },
                        "sInfo": "Показаны с _START_ по _END_ из _TOTAL_ записей",
                        "sInfoEmpty": "",
                        "sEmptyTable": "Нет данных для отображения",
                        "lengthMenu": "_MENU_ записей"
                    },
                    searching: false,
                    order: [],
                    paging: true,
                    lengthChange: true,
                    lengthMenu: [
                        [50, 100, 250, -1],
                        [50, 100, 250, "Все"]
                    ]
                },
                ajax: {
                    url: "/pub/ajax/stat/{group}".replace("{group}", Index.options.group),
                    data: {
                        'date': "{start} - {end}"
                            .replace("{start}", moment(Index.options.dateFrom).format('DD.MM.YYYY'))
                            .replace("{end}", moment(Index.options.dateTo).format('DD.MM.YYYY')),
                        'timezone': Index.options.timezone
                    }
                },
                footer: [
                    { name: "cpm", sum: function (r) {
                        try {
                            return r["cost"] / r["raw"] * 1000;
                        } catch (e) {
                            return 0
                        }
                    }},
                    { name: "raw_impressions", sum: function (r) {
                        try {
                            return r["impressions"] / r["raw"] * 100;
                        } catch (e) {
                            return 0
                        }
                    }}
                ]
            });

            $("#apply_changes").on("click", function (evt) {
                evt.preventDefault();
                table.limeTables().fetch($("#stat_filter").serializeObject());
            });
            table.limeTables().fetch($("#stat_filter").serializeObject());
        }
    }
}();