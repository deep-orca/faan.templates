var Login = function () {

	var handleLogin = function() {
		$('.login-form').validate({
	            errorElement: 'span', //default input error message container
	            errorClass: 'help-block', // default input error message class
	            focusInvalid: false, // do not focus the last invalid input
	            rules: {
	                email: {
	                    required: true,
	                    email: true
	                },
	                password: {
	                    required: true
	                },
	                remember: {
	                    required: false
	                }
	            },

	            messages: {
	                email: {
	                    required: "Введите адрес e-mail."
	                },
	                password: {
	                    required: "Введите пароль."
	                }
	            },

	            invalidHandler: function (event, validator) { //display error alert on form submit
	                $('.alert-error', $('.login-form')).show();
	            },

	            highlight: function (element) { // hightlight error inputs
	                $(element)
	                    .closest('.form-group').addClass('has-error'); // set error class to the control group
	            },

	            success: function (label) {
	                label.closest('.form-group').removeClass('has-error');
	                label.remove();
	            },

	            errorPlacement: function (error, element) {
	                error.insertAfter(element.closest('.input-icon'));
	            },

	            submitHandler: function (form) {
	                form.submit();
	            }
	        });

	        $('.login-form input').keypress(function (e) {
	            if (e.which == 13) {
	                if ($('.login-form').validate().form()) {
	                    $('.login-form').submit();
	                }
	                return false;
	            }
	        });
	}

	var handleForgetPassword = function () {
		$('.forget-form').validate({
	            errorElement: 'span', //default input error message container
	            errorClass: 'help-block', // default input error message class
	            focusInvalid: false, // do not focus the last invalid input
	            ignore: "",
	            rules: {
	                email: {
	                    required: true,
	                    email: true
	                }
	            },

	            messages: {
	                email: {
	                    required: "Введите адрес e-mail."
	                }
	            },

	            invalidHandler: function (event, validator) { //display error alert on form submit   

	            },

	            highlight: function (element) { // hightlight error inputs
	                $(element)
	                    .closest('.form-group').addClass('has-error'); // set error class to the control group
	            },

	            success: function (label) {
	                label.closest('.form-group').removeClass('has-error');
	                label.remove();
	            },

	            errorPlacement: function (error, element) {
	                error.insertAfter(element.closest('.input-icon'));
	            },

	            submitHandler: function (form) {
	                form.submit();
	            }
	        });

	        $('.forget-form input').keypress(function (e) {
	            if (e.which == 13) {
	                if ($('.forget-form').validate().form()) {
	                    $('.forget-form').submit();
	                }
	                return false;
	            }
	        });

	        jQuery('#forget-password').click(function () {
	            jQuery('.login-form').hide();
	            jQuery('.forget-form').show();
	        });

	        jQuery('#back-btn').click(function () {
	            jQuery('.login-form').show();
	            jQuery('.forget-form').hide();
	        });

	}

	var handleRegister = function () {

		function format(state) {
            if (!state.id) return state.text; // optgroup
            return "<img class='flag' src='assets/img/flags/" + state.id.toLowerCase() + ".png'/>&nbsp;&nbsp;" + state.text;
        }


		$("#select2_sample4").select2({
		  	placeholder: '<i class="fa fa-map-marker"></i>&nbsp;Select a Country',
            allowClear: true,
            formatResult: format,
            formatSelection: format,
            escapeMarkup: function (m) {
                return m;
            }
        });


			$('#select2_sample4').change(function () {
//                $('.register-form').validate().element($(this)); //revalidate the chosen dropdown value and show error or success message for the input
            });

			$('.register-form input').keypress(function (e) {
	            if (e.which == 13) {
//	                if ($('.register-form').validate().form()) {
	                    $('.register-form').submit();
//	                }
	                return false;
	            }
	        });

	        jQuery('#register-btn').click(function () {
	        	if ($(".usrtypebutton").hasClass("active"))
	        		$( ".content" ).animate({width: "600px"}, 300);
	        	else
	        		$( ".content" ).animate({width: "460px"}, 300);
	            jQuery('.login-form').hide();
	            jQuery('.register-form').show();
	        });

	        jQuery('#register-back-btn').click(function () {
	        	if ($(".content").width()!="600px")
    				$( ".content" ).animate({width: "360px"}, 300);
	            $('.login-form').show();
	            $('.register-form').hide();
	        });
	}
    
    return {
        //main function to initiate the module
        init: function () {
        	
            handleLogin();
            handleForgetPassword();
            handleRegister();
	       
        }

    };

}();


var adv_rules = pub_rules = {
        fullname: {
            required: true
        },
        email: {
            required: true,
            email: true
        },
        icq:{
            number: true
        }//,
//        phone:{
//            number: true
//        }
//        password: {
//            required: true
//        },
//        rpassword: {
//            equalTo: "#register_password"
//        }
}

var agc_rules = {
        email: {
            required: true,
            email: true
        },
        fullname: {
            required: true
        },
        company: {
            required: true
        },
        icq:{
            number: true
        },
        phone:{
            number: true
        }

}

$(".usrtypebutton").on( "click", function() {
// 	var user_type = $(this).children()[0].name;
 	var user_type = $(this).data("group");
// 	var user_type = $(this).data("group");
    $("#usertype").val(user_type);
    if ($(".content").width()!="460px")
    	$( ".content" ).animate({width: "600px"}, 600, function() {

    		//$( ".usrtypelabel" ).css("font-size", "12px");
    		if ($('.register-fields').attr('id') != user_type && $('.register-fields').attr('id') != ""){
    		    $('.register-fields').slideUp();
    		    $('.register-fields').attr('id', user_type);
    		    show_register_inputs(user_type);
    		    $('.register-fields').slideDown();
    		}
    		else{
    		    $('.register-fields').attr('id', user_type);
    		    show_register_inputs(user_type);
    		    $('.register-fields').slideDown();
    		}
  		});
    	$( ".acctypeicon" ).animate({fontSize: "18px"}, 200);
    	$( ".usrtypelabel" ).animate({fontSize: "12px", margin: "0"}, 200);

    if ($('.register-fields').attr('id') != user_type && $('.register-fields').attr('id') != ""){
        $('.register-fields').slideUp();
        $('.register-fields').attr('id', user_type);
        show_register_inputs(user_type);
        $('.register-fields').slideDown();
    }
    else{
        $('.register-fields').attr('id', user_type);
        show_register_inputs(user_type);
        $('.register-fields').slideDown();
    }
});

var register_form = $('.register-form');//.validate({
//	            errorElement: 'span', //default input error message container
//	            errorClass: 'text-danger', // default input error message class
//	            focusInvalid: false, // do not focus the last invalid input
//	            ignore: "",
//
//	            messages: { // custom messages for radio buttons and checkboxes
//	            	usertype: {
//	                    required: "Выберите тип аккаунта"
//	                },
//	                fullname: {
//	                    required: "Введите Ваше имя и фамилию"
//	                },
//	            	email: {
//	                    required: "Введите адрес e-mail",
//	                    email: "Введите правильный e-mail адрес"
//	                }
//                    company:{
//                        required: "Введить название компании"
//                    },
//	                password: {
//	                    required: "Введите пароль"
//	                },
//	                rpassword: {
//	                    required: "Введите подтверждение пароля"
//	                },
//                    phone: {
//                        number: "Вводите только цифры"
//                    },
//                    icq:{
//                        number:"Вводите только цифры"
//                    },
//	                tnc: {
//	                    required: "Вы должны принять Правила, чтобы продолжить."
//	                }
//	            },
//
//	            invalidHandler: function (event, validator) { //display error alert on form submit
//
//	            },
//
//	            highlight: function (element) { // hightlight error inputs
//	                $(element)
//	                    .closest('.input-icon').addClass('has-error'); // set error class to the control group
//	            },
//
//	            success: function (label) {
//	                label.closest('.input-icon').removeClass('has-error');
//	                label.remove();
//	            },
//
//	            errorPlacement: function (error, element) {
//	                if (element.attr("name") == "tnc") { // insert checkbox errors after the container
//	                    error.insertAfter($('#register_tnc_error'));
//	                } else if (element.closest('.input-icon').size() === 1) {
//	                    error.insertAfter(element.closest('.input-icon'));
//	                } else {
//	                	error.insertAfter(element);
//	                }
//	            },
//
//	            submitHandler: function (form) {
//	                form.submit();
//	            }
//	        });

function show_register_inputs(u_type){
    try{
        register_form.resetForm();
        $('.register-form .form-group').removeClass('has-error');
    } catch(e) {}

    if (u_type == 1){
        $('[name=description]').attr('placeholder','Расскажите о себе. Какие приложения Вы хотите монетизировать?');
        $('#register-submit-btn').text('Подать заявку');
    } else if (u_type == 2){
        $('[name=description]').attr('placeholder','Расскажите о себе. Какие продукты Вы хотите рекламировать?');
        $('#register-submit-btn').text('Подать заявку');
    } else{
        $('[name=description]').attr('placeholder','');
        $('#register-submit-btn').text('Связаться с нами');
    }
}

//function addRules(rulesObj){
//    for (var item in rulesObj){
//       $('.register-form [name = '+item+']').rules('add',rulesObj[item]);
//    }
//}

//function removeRules(rulesObj){
//    for (var item in rulesObj){
//       $('.register-form [name = '+item+']').rules('remove');
//    }
//}


