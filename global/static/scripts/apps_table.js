var TableManaged = function () {

    return {

        //main function to initiate the module
        init: function () {
            
            if (!jQuery().dataTable) {
                return;
            }

var table = $('#applications_table').DataTable({
                 "aoColumns": [
                  { "bSortable": false, "bSearchable": false },
                  { "visible": false },
                  { "visible": false },
                  { "visible": true, "sortable": false },
                  { "bSearchable": true, "sWidth": "35%" },
                  { "bSearchable": true },
                  { "bSearchable": true },
                  { "bSearchable": false }
//                  { "bSearchable": false },
//                  { "bSearchable": false },
//                  { "bSearchable": false },
//                  { "bSearchable": false },
//                  { "bSearchable": false }
                ],
                "order": [[ 5, "asc" ]],
                "aLengthMenu": [
                    [10, 20, 50, -1],
                    [10, 20, 50, "Все"]  // change per page values here
                ],
                // set the initial value
                "iDisplayLength": 10,
                "sPaginationType": "bootstrap",
                "oLanguage": {
                    "sLengthMenu": "_MENU_ площадок",
                    "oPaginate": {
                        "sPrevious": "Назад",
                        "sNext": "Вперед"
                    },
                    "sSearch": "Поиск: ",
                    "sInfo" : "Показано с _START_ по _END_ из _TOTAL_ площадок",
                    "sInfoempty" : "У Вас пока не добавлено ни одной площадки"
                },
                "aoColumnDefs": [{
                        'bSortable': false,
                        'aTargets': [0]
                    }
                ]
            });

            jQuery('#applications_table .group-checkable').change(function () {
                var set = jQuery(this).attr("data-set");
                var checked = jQuery(this).is(":checked");
                jQuery(set).each(function () {
                    if (checked) {
                        $(this).attr("checked", true);
                    } else {
                        $(this).attr("checked", false);
                    }
                });
                jQuery.uniform.update(set);
            });

            jQuery('#applications_table_wrapper .dataTables_filter input').addClass("form-control input-small"); // modify table search input
            jQuery('#applications_table_wrapper .dataTables_length select').addClass("form-control input-xsmall"); // modify table per page dropdown
            jQuery('#applications_table_wrapper .dataTables_length select').select2(); // initialize select2 dropdown
            $("#applications_table_length label")
                .after('<div class="btn-group osfilter" style="padding-left: 50px;">' +
                    '<button class="btn btn-default btn-sm filteros active" id=""> ' +
                    'Все ' +
                    '</button> ' +

                    '<button class="btn btn-default btn-sm filteros" id="ios"> ' +
                    '<i class="fa fa-apple"></i>' +
                    '</button> ' +

                    '<button class="btn btn-default btn-sm filteros" id="android"> ' +
                    '<i class="fa fa-android"></i>' +
                    '</button> ' +

                    '<button class="btn btn-default btn-sm filteros" id="windows" disabled> ' +
                    '<i class="fa fa-windows"></i>' +
                    '</button> ' +

                    '<button class="btn btn-default btn-sm filteros" id="web"> ' +
                    '<i class="fa fa-globe"></i>' +
                    '</button> ' +

                    '</div>');
            $('.filteros').on('click', function(e) {
                $('.osfilter button.filteros').removeClass('active');
                $(this).addClass('active');
                table.column(2).search($(this).attr('id')).draw();
            });

 		}

    };

}();